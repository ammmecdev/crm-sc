<?php 
require_once '../../model/usuario.php';
require_once './model/modulo.php';
class UsuariosController{

	private $model;
	private $url;
	private $mensaje;
	private $error;

	public function __CONSTRUCT(){
		$this->model = new Usuario();
		$this->modelMod = new Modulo();
	}

	public function Index(){
		if(!empty($_SESSION['acceso'])){
			$page="./view/usuarios/usuarios.php";
			require_once '../../view/index.php';
		}
		else
			$this->Lockscreen();
	}

	public function Guardar(){
		try
		{
			$usuario= new Usuario();
			$usuario->idUsuario=$_REQUEST['idUsuario'];
			$usuario->nombreUsuario=$_REQUEST['nombreUsuario'];
			$usuario->usuario=$_REQUEST['usuario'];
			$usuario->email=$_REQUEST['email'];
			$password = $_REQUEST['pass'];
			$usuario->idEmpleado = $_REQUEST['idEmpleado'];
			$password = md5($password);
			$password = crc32($password);
			$password = crypt($password,"xtem");
			$usuario->pass= sha1($password);
			$usuario->token_password = $this->generateToken();
			if($usuario->idUsuario > 0){
				$this->model->Actualizar($usuario);
				echo "Se han actualizado correctamente los datos";
			}else{
				$idUsuario = $this->model->Registrar($usuario); 
				$res = $this->model->ObtenerPermisosPuesto($_POST['idPuesto']);
				foreach ($res as $r) {
					$this->model->InsertarPermisos($idUsuario, $r->tipo, $r->idRelacion, $r->idElement);
				}
				echo "Se ha registrado correctamente el usuario";
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}	
	}


	public function generateToken()
	{
		$gen = md5(uniqid(mt_rand(), false));	
		return $gen;
	}

	public function Desactivar(){
		try
		{
			$idUsuario=$_REQUEST['idUsuario'];
			$this->model->Desactivar($idUsuario);
			echo "Se ha desactivado correctamente el usuario";
		}
		catch(Exception $e)
		{
			echo "No se ha podido desactivar al usuario";
		}	
	}

	public function Activar(){
		try
		{
			$idUsuario=$_REQUEST['idUsuario'];
			$this->model->Activar($idUsuario);
			echo "Se ha activado correctamente el usuario";
		}
		catch(Exception $e)
		{
			echo "No se ha podido activar al usuario";
		}	
	}

	public function rowToArray($r){
		return array(
			$r->idUsuario, 
			$r->idNivel, 
			$r->nombreUsuario,
			$r->usuario,
			$r->email,
			$r->pass,
			$r->nivel,
			$r->idEmpleado,
			$r->modulo,
			$r->idModulo
		);
	}

	public function crearTablaActivos(){
		if (!empty($_REQUEST['vBusqueda']))
			$vBusqueda = $_REQUEST['vBusqueda'];
		else
			$vBusqueda = "";

		if ($vBusqueda == '')
			$resultado = $this->model->Listar();
		else
			$resultado = $this->model->ConsultaUsuariosA($vBusqueda);
		$permisoEdi = $this->consultaPermisos(1);	
		echo '
		<thead>
		<tr style="background-color: #FCF3CF;">
		<td></td>
		<td><strong>Nombre Completo</strong></td>
		<td align="center"><strong>Usuario</strong></td>
		<td align="center"><strong>Tipo de usuario</strong></td>
		<td align="center"><strong>Módulo</strong></td>';
		if ($permisoEdi == true) {
			echo '<td align="center"><strong>Editar</strong></td>';
		}
		echo '</tr>
		</thead>
		<tbody>';
		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="11" align="center"> <strong>No se encontraron usuarios</strong></td></tr>';
		} else {
			foreach($resultado as $r) :
				$array = $this->rowToArray($r);// se llena el arreglo con los datos del ciclo
				$datos = implode(",", $array);
				echo '
				<tr>
				<td align="center"><img src="../../assets/imagenes/'.$r->foto.'" class="img-circle" style="width: 40px; height: 40px;"/></td>
				<td>
				<table>
				<tr>
				<td><a href="" data-toggle="modal" data-target="#mUsuario" onclick="editarUsuario('; echo "'". $datos . "'"; echo')"> '.$r->nombreUsuario.'</a> <a href="" data-toggle="modal" data-target="#mPermisos" onclick="permisosUsuario('; echo $r->idUsuario ?>,<?php echo "'". $r->nombreUsuario . "'"; ?> ,<?php echo "'". $r->puesto . "'"; ?> ,<?php echo "'". $r->nivel . "'"; ?>, <?php echo "'" . $datos . "'"; echo ')"> <i class="fas fa-lock"></i></a></td>
				</tr>
				<tr>
				<td>'.$r->email.'</td>
				</tr>
				</table>
				</td>
				<td align="center">'.$r->usuario.'</td>
				<td align="center">'.$r->nivel.'</td>
				<td align="center">'.$r->modulo.'</td>';
				if ($permisoEdi == true) {
					echo '<td align="center">
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mUsuario" onclick="editarUsuario('; echo "'". $datos . "'"; echo')"><span class="glyphicon glyphicon-pencil"></span></button></td>';
				}
				echo '</tr>';
			endforeach;
		}
		echo '</tbody>
		</table>';
	}

	public function crearTablaDesactivados(){
		if (!empty($_REQUEST['vBusqueda']))
			$vBusqueda = $_REQUEST['vBusqueda'];
		else
			$vBusqueda = "";
		
		if ($vBusqueda == "")
			$resultado = $this->model->ListarInactivos();
		else
			$resultado = $this->model->ConsultaUsuariosI($vBusqueda);
		$permisoAct = $this->consultaPermisos(2);
		echo '
		<thead>
		<tr style="background-color: #FCF3CF;">
		<td style="width: 8%"></td>
		<td style="width: 30%"><strong>Nombre Completo</strong></td>
		<td style="width: 30%"><strong>Email</strong></td>
		<td style="width: 16%" align="center"><strong>Usuario</strong></td>';
		if ($permisoAct == true) {
			echo '<td style="width: 16%" align="center"><strong>Acción</strong></td>';
		}
		echo '</tr>
		</thead>
		<tbody>';
		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="11" align="center"> <strong>No se encontraron usuarios</strong></td></tr>';
		} else {
			foreach($resultado as $r):
				$array = $this->rowToArray($r);// se llena el arreglo con los datos del ciclo
				$datos = implode(",", $array);
				echo '
				<tr>
				<td align="center"><img src="../../assets/imagenes/'.$r->foto.' "class="img-circle" style="width: 40px; height: 40px;" /></td>
				<td>'.$r->nombreUsuario.'</td>
				<td>'.$r->email.'</td>
				<td align="center">'.$r->usuario.'</td>';
				if ($permisoAct == true) {
					echo '<td align="center"><button type="button" class="btn btn-link" onclick="myFunctionActivar('; ?><?php echo $r->idUsuario; ?><?php echo ')">Activar</button></td>';
				}
				echo '</tr>';
			endforeach;
		}
		echo '</tbody>
		</table>';
	}

	public function crearTablaSuspendidos()
	{
		if (!empty($_REQUEST['vBusqueda']))
			$vBusqueda = $_REQUEST['vBusqueda'];
		else
			$vBusqueda = "";
		
		if ($vBusqueda == "")
			$resultado = $this->model->ListarSuspendidos();
		else
			$resultado = $this->model->ListarSuspendidosB($vBusqueda);
		$permisoEdi = $this->consultaPermisos(1);
		echo '
		<thead>
		<tr style="background-color: #FCF3CF;">
		<td></td>
		<td><strong>Nombre Completo</strong></td>
		<td align="center"><strong>Usuario</strong></td>
		<td align="center"><strong>Tipo de usuario</strong></td>
		<td align="center"><strong>Módulo</strong></td>';
		if ($permisoEdi == true) {
			echo '<td align="center"><strong>Editar</strong></td>';
		}
		echo '</tr>
		</thead>
		<tbody>';
		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="11" align="center"> <strong>No se encontraron usuarios</strong></td></tr>';
		} else {
			foreach($resultado as $r) :
				$array = $this->rowToArray($r);// se llena el arreglo con los datos del ciclo
				$datos = implode(",", $array);
				echo '
				<tr>
				<td align="center"><img src="../../assets/imagenes/'.$r->foto.'" class="img-circle" style="width: 40px; height: 40px;"/></td>
				<td>
				<table>
				<tr>
				<td><a href="" data-toggle="modal" data-target="#mUsuario" onclick="editarUsuario('; echo "'". $datos . "'"; echo')"> '.$r->nombreUsuario.'</a> <a href="" data-toggle="modal" data-target="#mPermisos" onclick="permisosUsuario('; echo $r->idUsuario ?>,<?php echo "'". $r->nombreUsuario . "'"; ?> ,<?php echo "'". $r->puesto . "'"; ?> ,<?php echo "'". $r->nivel . "'"; ?>, <?php echo "'" . $datos . "'"; echo ')"> <i class="fas fa-lock"></i></a></td>
				</tr>
				<tr>
				<td>'.$r->email.'</td>
				</tr>
				</table>
				</td>
				<td align="center">'.$r->usuario.'</td>
				<td align="center">'.$r->nivel.'</td>
				<td align="center">'.$r->modulo.'</td>';
				if ($permisoEdi == true) {
					echo '<td align="center">
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mUsuario" onclick="editarUsuario('; echo "'". $datos . "'"; echo')"><span class="glyphicon glyphicon-pencil"></span></button></td>';
				}
				echo '</tr>';
			endforeach;
		}
		echo '</tbody>
		</table>';
	}

	public function consultaPermisos($val)
	{
		$permisos = $_SESSION['permisos'];
		if ($val == 1) {
			$permiso = "editar_usuarios";
		}else{
			$permiso = "activar_usuarios";
		}
		$retorno = false;
		foreach ($permisos as $p):
			if ($p == $permiso) {
				$retorno = true; 
			}
		endforeach;
		return $retorno;
	}

	public function ListarEmpleados()
	{
		header('Content-Type: application/json');
		$datos = array();
		$query = $this->model->ListarEmpleados();
		foreach ($query as $empleado):
			$row_array['idEmpleado']  = $empleado->idEmpleado;
			$row_array['nombreEmpleado']  = $empleado->nombre;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ObtenerDatosEmpleado()
	{
		header('Content-Type: application/json');
		$idEmpleado = $_POST['idEmpleado'];
		$datos = array();
		if ($idEmpleado == "") {
			$row_array['reset']  = "false";
		}else{
			$res = $this->model->ObtenerDatosEmpleado($idEmpleado);
			$row_array['email']  = $res->email;
			$row_array['modulo']  = $res->modulo;
			$row_array['idModulo']  = $res->idModulo;
			$row_array['tipo']  = $res->nivel;	
			$row_array['idPuesto'] = $res->idPuesto;
		}
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);	
	}
	
	public function ListarTiposUsuario()
	{
		header('Content-Type: application/json');
		$datos = array();
		$idModulo = $_POST['idModulo'];
		$res=$this->model->ListarTiposUsuario($idModulo);
		foreach ($res as $r) :
			$row_array['idTipoUM']  = $r->idNivelUM;
			$row_array['tipo']  = $r->nivel;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function verificarPuesto()
	{
		$idUsuario = $_POST['idUsuario'];
		$verificar=$this->model->verificarPuesto($idUsuario);
		if ($verificar != "")
			echo "ok";
		else
			echo "false";
	}

	//Metodo para acceso de administrador
	public function Lockscreen(){
		require_once '../../controller/lockscreen.controller.php';
		$controller = new LockscreenController;
		call_user_func( array( $controller, 'Index' ));
	}

	public function verificarUsuario()
	{
		$idUsuario = $_POST['idUsuario'];
		$usuario = $_POST['usuario'];
		$verificar=$this->model->verificarUsuario($usuario,$idUsuario);
		if ($verificar == "")
			echo "0";
		else
			echo "1";
	}

	//Metodo para exportar los usuarios del sistema
	public function Exportar(){
		$vBusqueda = $_REQUEST['vBusqueda'];
		echo ($vBusqueda);
		if ($vBusqueda == ''){
			$resultado = $this->model->ListarE();
		}else{
			$resultado = $this->model->ListarExp($vBusqueda);	
		}
		$resultado=$resultado->fetchAll(PDO::FETCH_OBJ);

		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');
		$objPHPExcel  = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Usuarios del sistema");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Usuarios del sistema");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(100);
		$objDrawing->setCoordinates('F1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));

		$styleAlingCenter = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);

		$estiloActivo = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '58D68D')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$estiloInactivo = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'EC7063')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$estiloSuspendido = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'F8C471')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

		$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->applyFromArray($estiloTituloColumnas);

		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'USUARIOS DEL SISTEMA');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Nombre completo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Usuario');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tipo de usuario');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Email');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Modulo al que corresponde');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Estado del usuario');
		$fila = 7; 

		foreach ($resultado as $r) :
			$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->applyFromArray($styleAlingCenter);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$fila)->applyFromArray($styleAlingCenter);

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->nombreUsuario);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->usuario);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->nivel);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->email);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->modulo);

			if ($r->activo==0){
				$r->activo="Desactivado";
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->activo);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->applyFromArray($estiloInactivo);
			}else if($r->activo==1){
				$r->activo="Activo";
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->activo);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->applyFromArray($estiloActivo);
			}else{
				$r->activo="Suspendido";
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->activo);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->applyFromArray($estiloSuspendido);
			}
			$fila++; 
		endforeach;

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Usuarios.csv"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');    
		
	}

	public function ActualizarPermisos()
	{
		try {
			
			$usuario = new Usuario();
			$idUsuario = $_POST['idUsuario'];
			$puesto = $_POST['puesto'];
			$nombreUsuario = $_POST['nombreUsuario'];
			$post=$_POST;
			$this->model->LimpiarPermisos($idUsuario);
			foreach ($post as $key => $value) {
				if($key!='idUsuario' && $key!='nombreUsuario' && $key!='puesto'){
					$idElement = $key;
					$valores = explode(',', $value);
					$tipo = $valores[0];
					$idRelacion = $valores[1];
					$this->model->InsertarPermisos($idUsuario, $tipo, $idRelacion, $idElement);
				}
			}
			echo "Se han actualizado correctamente los permisos del ". $puesto . " " . $nombreUsuario;	
		} catch (Exception $e) {
			echo "error";
		}
	}	

	public function ObtenerPermisos()
	{
		$idUsuario = $_POST['idUsuario'];
		$res = $this->model->ObtenerPermisos($idUsuario);
		$permisos=array();
		foreach ($res as $r) 
			array_push($permisos, $r->idElement);
		echo '<section class="panel default blue_title" style="margin-top: -10px; margin-bottom: -20px; box-shadow: 0 0px 3px 0 rgba(0,0,0,0.2);">
		<legend align="center" style="margin-bottom: -5px"><h3>Selección de permisos</h3></legend>
		<div class="panel-body">
		<ul class="treeview-black treeview" id="black">';
		foreach($this->modelMod->Listar() as $modulo):
			if($modulo->idModulo == 2 || $modulo->idModulo == 3 || $modulo->idModulo==6){
				echo '<li class="liPermisos1 expandable lastExpandable">
				<div class="hitarea expandable-hitarea lastExpandable-hitarea"></div>
				<!-- <div class="minimal single-row"> -->';
				$check=false;
				foreach ($permisos as $p)
					if ($p == $modulo->idElement) $check=true;
				echo '<input type="checkbox" id="'.$modulo->idElement.'" name="'.$modulo->idElement.'" value="modulo,'. $modulo->idModulo.'" class="modulos"'; if($check == true){ echo 'checked'; }  echo '>&nbsp;
				<label>'.$modulo->modulo.'</label>
				<!-- </div><ul style="display: none;"> -->
				<ul>';
				foreach ($this->modelMod->ListarSecciones($modulo->idModulo) as $seccion):
					echo '<li class="liPermisos2 lastExpandable">
					<div class="hitarea expandable-hitarea"></div>
					<!-- <div class="minimal single-row"> -->';
					$check=false;
					foreach ($permisos as $p)
						if ($p == $seccion->idElement) $check=true;
					echo '<input type="checkbox" id="'.$seccion->idElement.'" name="'.$seccion->idElement.'" value="seccion,'.$seccion->idSeccion.'" class="'.$modulo->idElement . ' seccion"'; if($check == true){ echo 'checked'; }  echo '>&nbsp;
					<font>'.$seccion->seccion.'</font>
					<!-- </div> -->
					<ul>';
					foreach($this->modelMod->ListarFunciones($seccion->idSeccion) as $fun):
						echo '<li class="liPermisos3">
						<!-- <div class="minimal single-row"> -->';
						$check=false;
						foreach ($permisos as $p)
							if ($p == $fun->idElement) $check=true;
						echo '<input type="checkbox" id="'.$fun->idElement.'" name="'.$fun->idElement.'" value="funcion,'.$fun->idFuncion.'" class="'.$modulo->idElement . ' ' . $seccion->idElement.' funcion"'; if($check == true){ echo 'checked'; }  echo '>&nbsp;
						<font>'.$fun->funcion.'</font>
						<!-- </div> -->
						</li>';
					endforeach; 
					echo '</ul>
					</li>';
				endforeach; 
				echo '</ul></li>';
			}
		endforeach; 
		echo '</ul></div></selection>';
		echo "<script type='text/javascript'>
		$('.modulos').click(function(){
			if (this.checked){
				$('.'+this.id).prop('checked', true);
			}else{
				$('.'+this.id).prop('checked', false);
			}
		});

		$('.seccion').click(function(){
			if (this.checked){
				$('.'+this.id).prop('checked', true);
				var res = $(this).attr('class').split(' ');
				mod=res[0];
				$('#'+mod).prop('checked', true);
			}else{
				$('.'+this.id).prop('checked', false);
			}
		});

		$('.funcion').click(function(){
			if (this.checked){
				var res = $(this).attr('class').split(' ');
				mod=res[0];
				sec=res[1];
				$('#'+mod).prop('checked', true);
				$('#'+sec).prop('checked', true);
			}
		});
		</script>
		<script src='../../assets/plugins/tree-view/jquery.treeview.js'></script>
		<script src='../../assets/plugins/tree-view/jquery.cookie.js'></script>
		<script type='text/javascript' src='../../assets/plugins/tree-view/demo.js'></script>";
	}

	public function ActualizaPermisosSesion()
	{
		$res = $this->model->ObtenerPermisos($_SESSION['idUsuario']);
		$permisos=array();
		foreach ($res as $re) 
			array_push($permisos, $re->idElement);
		$_SESSION['permisos'] = $permisos;
		echo json_encode($_SESSION['permisos'], JSON_FORCE_OBJECT);
	}
}
?>