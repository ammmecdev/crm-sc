<?php
require_once '../../model/usuario.php';
class LockscreenController{
	private $model;

	public function __CONSTRUCT(){
		$this->model = new Usuario();
	}

	public function Index(){
		$page = "../../view/login/locked.php";
		require_once '../../view/index.php';
		//require_once '../../view/login/locked.php';
	}
	
	public function VerificarAdminRH(){
		$usuario = new Usuario();
		$usuario->email = $_SESSION['email'];
		$password = $_POST['password'];
		$password=md5($password);
		$password=crc32($password);
		$password=crypt($password,"xtem");
		$usuario->pass=sha1($password);
		$consulta = $this->model->VerificarAdminRH($usuario);
		if($consulta != null){
			$get = $this->validarUsuario($consulta);
			if ($get == 'true') {
				echo 'ok';
				$_SESSION['acceso'] = "true";
				$_SESSION['tiempoAdm'] = time();
			}else{
				echo 'Acceso denegado';
			}
		}else{
			echo 'Acceso denegado';
		}
	}

	public function validarUsuario($consulta)
	{
		foreach ($consulta as $r):
			if ($r->email == $_SESSION['email'] && $r->idUsuario == $_SESSION['idUsuario']) {
				return 'true';
			}else{
				return 'false';
			}
		endforeach;
	}
}