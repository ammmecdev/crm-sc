<?php 

require_once 'model/usuario.php';

class LoginController{
	public $model;
	private $mensaje;
	private $verificacion;

	public function __CONSTRUCT(){
		$this->model = new Usuario();
	} 

	public function Index(){
		require_once 'view/login/index.php';
	}

	public function Lockscreen(){
		require_once '../../view/login/lockscreen.php';
	}

	public function Acceder(){
		header ('Location: ?c=negocios');
	}

	public function Verificar(){
		try {
			header('Content-Type: application/json');
			$usuario = new Usuario();
			$usuario->email = $_REQUEST['email'];
			$password = $_REQUEST['password'];
			$password = md5($password);
			$password = crc32($password);
			$password = crypt($password,"xtem");
			$usuario->pass= sha1($password);
			$array=array();
			$r=$this->model->Verificar($usuario);
			if($r!=null){
				if($r->activo==1){
					$permisosUser = $this->model->ObtenerPermisos($r->idUsuario);
					$permisos=array();
					foreach ($permisosUser as $per) 
						array_push($permisos, $per->idElement);
					$this->Session($r->nombreUsuario, $r->email, $r->usuario, $r->idUsuario, $r->foto, $r->idPuesto, $r->puesto, $r->idModulo, $r->modulo, $r->idNivel, $r->idEmpleado, $permisos);
					$row_array['mensaje']='ok';
				}else if($r->activo==0){
					$row_array['mensaje']="Acceso denegado";
				}else if($r->activo==2){
					$row_array['mensaje']="suspendida";
					$row_array['idUsuario']=$r->idUsuario;
					$row_array['token']=$r->token_password;
				} else {
					$row_array['mensaje'] = 'Acceso incorrecto';
				}
			} else {
				$row_array['mensaje'] = 'Acceso incorrecto';	
			}
			array_push($array, $row_array);
			echo json_encode($array, JSON_FORCE_OBJECT);	
		} catch (Exception $e) {
			header('Content-Type: application/json');
			$array=array();
			$row_array['mensaje']='Ha ocurrido un error inesperado, vuelve a intentarlo.';
			array_push($array, $row_array);
			echo json_encode($array, JSON_FORCE_OBJECT);	
			// die($e->getMessage());
		}	
	}

	public function Session
	(
		$nombreUsuario, 
		$email, 
		$usuario, 
		$idUsuario, 
		$foto, 
		$idPuesto, 
		$puesto, 
		$idModulo, 
		$modulo,
		$idNivel,
		$idEmpleado,
		$permisos
	)
	{
		$_SESSION['email'] = $email;
		$_SESSION['idUsuario']= $idUsuario;
		$_SESSION['usuario'] = $usuario;
		$_SESSION['nombreUsuario'] = $nombreUsuario;
		$_SESSION['seguridad'] = "ok";
		$_SESSION['idEmbudo'] = 4;
		$_SESSION['nombre'] = "Smart condition 2019";
		$_SESSION['logo'] = "logoammmec.png";
		$_SESSION['foto'] = $foto;
		$_SESSION['idPuesto'] = $idPuesto;
		$_SESSION['puesto'] = $puesto;
		$_SESSION['idModulo'] = $idModulo;
		$_SESSION['modulo'] = $modulo;
		$_SESSION['idNivel'] = $idNivel;
		$_SESSION['acceso'] = "";	
		$_SESSION['idEmpleado'] = $idEmpleado;
		$_SESSION['permisos'] = $permisos;
	}

	public function Logout()
	{
		session_destroy();
		unset($_SESSION['nombreUsuario']);
		unset($_SESSION['email']);
		unset($_SESSION['usuario']);
		unset($_SESSION['idTipo']);
		unset($_SESSION['seguridad']);
		unset($_SESSION['acceso']);
		unset($_SESSION['idEmpleado']);
		header ('Location: ./');
	}

	public function VerificarCorreo(){
		$email = $_POST['email'];
		$usuario = new Usuario;
		$usuario = $this->model->VerificarCorreo($email);
		if($usuario != null){
			if($usuario->activo != 0){
				$token = $this->generateToken();
				$this->model->GeneraTokenPass($usuario->idUsuario, $token);
				$url = 'http://'.$_SERVER['SERVER_NAME'].':81/erp-ammmec/?c=login&a=RestablecerPass&idUsuario='.$usuario->idUsuario.'&token='.$token;
				$asunto = "ERP AMMMEC - Recuperar password";
				$cuerpo = "Hola ".$usuario->nombreUsuario.".<br><br> Se ha solicitado un reinicio de password.<br><br>Para comenzar con su restauración <a href='".$url."' class='btn' type='button'>haga click aquí.</a><br><br>ERP-AMMMEC <br> Departamento de sistemas <br> Control de usuarios" ;
				if($this->enviarEmail($usuario->email, $usuario->nombreUsuario, $asunto, $cuerpo))
					echo 'Hemos enviado un correo electrónico a la cuenta '.$usuario->email.' para restablecer tu contraseña';
				else
					echo "error";
			}else{
				echo 'denegado';
			}
		}else{
			echo "inexistente";
		}
	}

	public function generateToken()
	{
		$gen = md5(uniqid(mt_rand(), false));	
		return $gen;
	}
	

	public function enviarEmail($email, $nombre, $asunto, $cuerpo){
		require './assets/plugins/PHPMailer/PHPMailerAutoload.php';
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPAuth = true;
		// $mail->SMTPDebug = 2;
		$mail->SMTPSecure = 'tls'; //Modificar
		$mail->Host = 'smtp.gmail.com'; //Modificar
		$mail->Port = 587; //Modificar
		
		$mail->Username = 'ammmec.crm@gmail.com'; //Modificar
		$mail->Password = 'Ammmec.2018'; //Modificar
		
		$mail->setFrom('ammmec.crm@gmail.com', 'CRM-AMMMEC'); //Modificar
		$mail->addAddress($email, $nombre);
		
		$mail->Subject = $asunto;
		$mail->Body    = $cuerpo;
		$mail->IsHTML(true);
		
		if($mail->send())
			return true;
		else
			return false;
	}

	public function RestablecerPass()
	{
		if(empty($_GET['idUsuario']))
			header('Location: index.php');
		if(empty($_GET['token']))
			header('Location: index.php');	
		$idUsuario = $_REQUEST['idUsuario'];
		$token = $_GET['token'];
		$res=$this->model->VerificaTokenPass($idUsuario, $token);
		if($res!=null){
			if($res->activo==2)
				$this->verificacion=1;
			else
				$this->verificacion=0;
		}
		else
			$this->verificacion=0;
		require_once 'view/login/recovery.php';
	}

	public function ActualizarPass()
	{
		try {
			if(!isset($_POST['idUsuario']))
				header('Location: index.php');
			$usuario = new Usuario();
			$usuario->idUsuario = $_POST['idUsuario'];
			$password = $_POST['password'];
			$password = md5($password);
			$password = crc32($password);
			$password = crypt($password,"xtem");
			$usuario->passnueva= sha1($password);
			$this->model->RestablecerPass($usuario);
			$this->verificacion=2;
			require_once 'view/login/recovery.php';
		} catch (Exception $e) {
			die($e->getMessage());	
		}
	}
}

?>
