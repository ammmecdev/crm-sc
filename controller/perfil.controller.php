<?php 
require_once '../../model/usuario.php';
require_once '../../modules/rh/model/empleado.php';

class PerfilController{

	private $model;
	private $modelEmpleado;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT(){
		$this->model = new Usuario();
		$this->modelEmpleado = new Empleado();

	}
	public function Index(){
		$perfil=true;
		$this->url="?c=Perfil";
		$page="../../view/configuracion/menu.php";
		$contenedor="../../view/configuracion/perfil.php";
		require_once '../../view/index.php';
	}
	public function Actualizar(){
		try
		{

			$usuario = new Usuario();
			// $usuario->idUsuario=$_REQUEST['idUsuario'];
			$usuario->idEmpleado=$_REQUEST['idEmpleado'];
			$_SESSION['nombreUsuario'] = $usuario->nombre = $_REQUEST['nombreUsuario'];
			$_SESSION['usuario'] = $usuario->usuario=$_REQUEST['usuario'];
			$_SESSION['email'] = $usuario->email=$_REQUEST['email'];
			$this->model->ActualizarPerfil($usuario);
			$this->model->ActualizarUsuario($usuario);
			echo "Se han actualizado correctamente los datos del usuario";
			//$this->Index();
		}
		catch(Exception $e)
		{
			echo "Se produjo un error al actualizar los datos del usuario";
		}   
	}
	public function ActualizarFoto(){
		try
		{
			if(!isset($_POST['idUsuario'])){
				$this->Index();
			}else{
				$usuario = new Usuario();
				$foto = $_FILES["foto"];
				if($foto['type']=="image/x-png" || $foto['type']=="image/png" || $foto['type']=="image/jpeg" || $foto['type']=="image/pjpeg")
				{
					$_SESSION['foto'] = $usuario->foto = $foto["name"];
					$usuario->idUsuario=$_REQUEST['idUsuario'];
					$archivo = $foto["tmp_name"];
					$carpeta = "../../assets/imagenes/";
					$src = $carpeta.$usuario->foto;
					move_uploaded_file($archivo, $src);
					$this->model->ActualizarPic($usuario);
					$this->mensaje="Se ha actualizado correctamente la foto de perfil";
					$this->Index();
				}else{
					$this->error=true;
					$this->mensaje="Formato de imagen invalido. Seleccione una imagen con los siguientes formatos  <strong> PNG </strong> ó <strong> JPG </strong>";
					$this->Index();
				}
			}
		}catch(Exception $e)
		{
			$this->mensaje="Se ha producido un error al actualizar la foto de perfil";
			$this->Index();
		}   
	}
	public function ActualizarPassword(){
		try
		{
			$usuario = new Usuario();
			$usuario->idUsuario = $_REQUEST['idUsuario'];
			$password = $_REQUEST['passnueva'];
			$password = md5($password);
			$password = crc32($password);
			$password = crypt($password,"xtem");
			$usuario->passnueva= sha1($password);
			$this->model->ActualizarPass($usuario);
			echo "Se han actualizado correctamente la contraseña del usuario";
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al actualizar la contraseña del usuario";
		}   
	}
	public function VerificaPassword(){
		$password = $_POST['password'];
		$password = md5($password);
		$password = crc32($password);
		$password = crypt($password,"xtem");
		$password= sha1($password);
		$usuario=$this->model->VerificarPass($password);
		if($usuario != null){
			try {
				echo 'ok';
			} catch (Exception $e) {

			}
		}else{
			echo "";
		}
	}
	
	public function comparaContra(){
		$password = $_REQUEST['passnueva'];
		$password2 = $_REQUEST['passconfirmar'];
		if($password != $password2){
			try {
				echo '<div style="color:red">Las contraseñas no coinciden</div>';
			} catch (Exception $e) {

			}
		}else{
			$this->ActualizarPassword();
		}
	}

	public function verificarEmail()
	{
		$idUsuario = $_POST['idUsuario'];
		$email = $_POST['email'];
		$verificar=$this->model->verificarEmail($email,$idUsuario);
		if ($verificar == "")
			echo "0";
		else
			echo "1";
	}
}