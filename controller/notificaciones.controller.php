<?php

require_once '../../model/notificacion.php';

class NotificacionesController{

	private $model;

	public function __CONSTRUCT(){
		$this->model = new Notificacion();
	}

	public function ListarNotificaciones()
	{
		foreach($this->model->Listar() as $n): 
			echo '  <li>
			<a href="<?php echo $n->ruta ?>" class="list-group-item list-group-item-info">
			<div class="row">
			<div class="col-lg-6">
			<i class="fas fa-dollar-sign"></i>&nbsp;<small>Departamento de Operaciones</small>
			</div>
			<div class="col-lg-6" align="right">
			<small style="font-size: 10px;"><i class="far fa-clock"></i> ' . $n->timestamp .' </small>
			</div>
			<div class="col-lg-12">
			<h4 style="margin-bottom: 0px; margin-top: 0px;">
			<small>
			' . $n->asunto . '
			</small>
			</h4>
			</div>
			</div>
			<small style="font-size: 10px;">CUENTA: FRE-0001-MCT</small>
			</a>
			</li> ';
		endforeach; 
	}

	public function CountNotification()
	{
		$idUsuario = $_SESSION['idUsuario'];
		$count = $this->model->CountNotification($idUsuario);
		echo $count;
	}

	public function MarcarLeida()
	{
		try {
			$idNotificacion = $_POST['idNotificacion'];
			$this->model->MarcarLeida($idNotificacion);
		} catch (Exception $e) {
			echo "Ha ocurrido un error al marcar notificacion como leida";
		}
	}
}
?>