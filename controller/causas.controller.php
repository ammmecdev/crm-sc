<?php
require_once '../../model/causaperdida.php';
class CausasController{
	private $model;
	private $url;

	public function __Construct(){
		$this->model = new CausaPerdida();
	}

	public function Index(){
		$this->url="?c=causas";
		$causasPerdida=true;
		$page = "../../view/configuracion/menu.php";
		$modales = "../../view/configuracion/modales/modalesCausas.php";
		$contenedor = "../../view/configuracion/causas.php";
		require_once '../../view/index.php';
	}

	public function ResultadoCausaPerdida(){
		header('Content-Type: application/json');
		$data = array();
		foreach ($this->model->Listar() as $r): 
			$r_array['idcausasPerdida']  = $r->idcausasPerdida;
			$r_array['nombreCausa']  = $r->nombreCausa;
			array_push($data, $r_array);
		endforeach;		
		echo json_encode($data, JSON_FORCE_OBJECT);
	}

	public function Guardar(){
		try
		{
			$causa = new CausaPerdida();
			$causa->idcausasPerdida = $_REQUEST['idcausasPerdida'];
			$causa->nombreCausa = $_REQUEST['nombreCausa'];
			if($causa->idcausasPerdida>0)
			{
				$this->model->Actualizar($causa);
				echo 'Se ha actulizado correctamente la causa';
			}else{
				$this->model->Registrar($causa);
				echo 'Se ha registrado correctamente la causa';
			}
		} 
		catch(Exception $e)
		{
			echo "error";
		}	
	}
	
	public function Eliminar()
	{
		try
		{
			$idcausasPerdida=$_REQUEST['idcausasPerdida'];
			$this->model->Eliminar($idcausasPerdida);
			echo "Se elimino correctamente la causa";
		}
		catch(Exception $e)
		{
			echo "error";
		}	
	}
}
?>