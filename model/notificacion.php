<?php
class Notificacion
{    
	public $idNotificacion;
	public $idUsuarioEmisor;
	public $idModuloEmisor;
	public $timestamp;
	public $leido;
	public $ruta;
	public $idEvento;
	public $idRelacion;
	public $idUsuarioReceptor;
	public $idModuloReceptor;
	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	public function Registrar(Notificacion $data)
	{
		$sql1 ="INSERT INTO notificaciones VALUES(?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql1)
		->execute(
			array(
				null,
				$data->idUsuarioEmisor,
				$data->idModuloEmisor,
				$data->timestamp,
				$data->leido,
				$data->ruta,
				$data->idEvento,
				$data->idRelacion
			)
		);
		return $this->pdo->lastInsertId(); 
	}

	public function RegistrarReceptores($idNotificacion, $idUsuarioReceptor, $idModuloReceptor)
	{
		$sql ="INSERT INTO receptores_notificaciones VALUES(?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idNotificacion,
				$idUsuarioReceptor,
				$idModuloReceptor
			)
		);
	}


	public function MarcarLeida($idNotificacion)
	{
		$sql ="UPDATE notificaciones SET leido=1 WHERE idNotificacion=?";
		$this->pdo->prepare($sql)
		->execute(
			array($idNotificacion)
		);
	}

	public function ObtenerUltimaNotificacion()
	{
		$stm = $this->pdo
		->prepare("SELECT * FROM notificaciones ORDER BY timestamp DESC LIMIT 1");
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	}


	public function CountNotification($idUsuario)
	{
		$qcn = $this->pdo->prepare("SELECT COUNT(*) FROM notificaciones n, receptores_notificaciones r, usuarios u WHERE r.idNotificacion = n.idNotificacion AND u.idUsuario = r.idUsuarioReceptor AND u.idUsuario = ? AND n.leido = 0");
		$qcn->execute(array($idUsuario));
		return $cn = implode($qcn->fetchAll(PDO::FETCH_COLUMN));
	}
}
?>