<?php 
class CausaPerdida
{
	public $idcausasPerdida;
	public $nombreCausa;
	
	private $pdo;
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	 //Metodo para registrar causa
	public function Registrar(CausaPerdida $data)
	{
		$sql ="INSERT INTO causaperdida VALUES(?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->nombreCausa
			)
		);
	}

	//Metodo para actualizar causa
	public function Actualizar(CausaPerdida $data)
	{
		$sql ="UPDATE causaperdida SET 
		nombreCausa=?
		WHERE idcausasPerdida=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombreCausa,
				$data->idcausasPerdida
			)
		);
	}

	//Metodo para eliminar causa
	public function Eliminar($idcausasPerdida)
	{
		$sql ="DELETE FROM causaperdida WHERE idcausasperdida=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idcausasPerdida
			)
		);
	}
	//Metodo para listar las causas de perdida
	public function Listar()
	{

		$stm = $this->pdo->prepare("SELECT * FROM causaperdida");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
}
?>