<?php
if (!isset($_SESSION['seguridad'])){
  header("Location: ./../..");
}

  try //Este metodo cierra la sesión de los usuarios que estan desactivados
  {
    $this->pdo = Database::StartUp(); 
    $idUsuario=$_SESSION['idUsuario'];
    $stm = $this->pdo->prepare("SELECT u.activo, e.email FROM usuarios as u, empleados as e WHERE u.idEmpleado = e.idEmpleado AND idUsuario = ?;");
    $stm->execute(array($idUsuario));
    $row=$stm->fetch(PDO::FETCH_OBJ);
    if($row->activo == 0 || $row->email != $_SESSION['email']){
      session_unset();
      session_destroy(); 
      echo  "<script type='text/javascript'> window.location='./'; </script>";
      exit();
    }
  }
  catch(Exception $e)
  {
    die($e->getMessage());
  }

  $this->pdo = Database::StartUp(); 
  $querydolar = $this->pdo->prepare("SELECT valorDolar FROM empresa WHERE idEmpresas = 1 ");
  $querydolar->execute();
  $dolar = implode($querydolar->fetchAll(PDO::FETCH_COLUMN));

  //CONSULTA PARA OBTENER EL CONTADOR DE NOTIFICACIONES
  //qCountNot = query contador de notificaciones
  $qContNot = $this->pdo->prepare("SELECT COUNT(*) FROM notificaciones n, receptores_notificaciones r, usuarios u WHERE r.idNotificacion = n.idNotificacion AND u.idUsuario = r.idUsuarioReceptor AND r.idUsuarioReceptor = ? AND n.leido = 0");
  $qContNot->execute(array($_SESSION['idUsuario']));
  $contNot = implode($qContNot->fetchAll(PDO::FETCH_COLUMN));

  $puestoSesion = $_SESSION['puesto'];
  $idNivel = $_SESSION['idNivel']; 
  $idModulo = $_SESSION['idModulo']; 

  $idUsuario = $_SESSION['idUsuario'];
  $idEmpleado = $_SESSION['idEmpleado'];
// echo  '<script type="text/javascript">alert("'.$_SESSION['idModulo'].'")</script>';


  $nameServer = $_SERVER['SERVER_NAME'];
//echo  '<script type="text/javascript">alert("'.$nivelPuesto.'")</script>';
  $server = $_SERVER['PHP_SELF'];
  $permisos = $_SESSION['permisos'];
  // print_r($permisos);

  ?>
