<?php
class Usuario
{    
	public $idUsuario;
	public $idNivel;
	public $nombreUsuario;
	public $usuario;
	public $email;
	public $pass;
	public $passnueva;
	public $foto;
	public $idEmpleado;
	public $token_password;
	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	//Metodo para listar todos los usuarios activos
	public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel AND u.activo = 1");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	public function ListarE()
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel");
		$stm->execute();
		return $stm;
	}
	public function ListarExp($vBusqueda)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel AND (nombreUsuario like '%$vBusqueda%')");
		$stm->execute();
		return $stm;
	}

	public function ConsultaUsuariosA($vBusqueda)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel AND (nombreUsuario like '%$vBusqueda%') AND u.activo = 1");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ConsultaUsuariosI($vBusqueda)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel AND (nombreUsuario like '%$vBusqueda%') AND u.activo = 0");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para listar todos los usuarios
	public function ListarInactivos()
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e WHERE u.idEmpleado = e.idEmpleado AND u.activo = 0");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarEmpleados()
	{
		$stm = $this->pdo->prepare("SELECT e.idEmpleado, e.nombre FROM empleados as e WHERE NOT EXISTS (SELECT NULL FROM usuarios as u WHERE e.idEmpleado = u.idEmpleado) AND e.status = 'activo'");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarSuspendidos()
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel AND u.activo = 2");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarSuspendidosB($vBusqueda)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p, modulos as m, niveles as t WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = t.idNivel AND (nombreUsuario like '%$vBusqueda%') AND u.activo = 2");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para contar los usuarios
	public function Contador()
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM usuarios");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}

    //Metodo para registrar usuarios
	public function Registrar(Usuario $data)
	{
		$sql ="INSERT INTO usuarios VALUES(?,?,?,?,?,'avatar.png',2,?,?,1)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->nombreUsuario,
				$data->usuario,
				$data->email,
				$data->pass,
				$data->idEmpleado,
				$data->token_password
			)
		);
		return $this->pdo->lastInsertId(); 
	}

	//Metodo para actualizar usuarios
	public function Actualizar(Usuario $data)
	{
		$sql ="UPDATE usuarios SET 
		nombreUsuario = ?,
		usuario = ?,
		email = ?,
		pass = ?,
		idEmpleado = ?,
		activo = 2,
		token_password = ?,
		password_request = 1
		WHERE idUsuario = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombreUsuario,
				$data->usuario,
				$data->email,
				$data->pass,
				$data->idEmpleado,
				$data->token_password,
				$data->idUsuario	
			)
		);
	}

	public function ActualizarPerfil(Usuario $data)
	{
		$sql ="UPDATE empleados SET 
		nombre=?,
		email=? 
		WHERE idEmpleado=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombre,
				$data->email,
				$data->idEmpleado
			)
		);
	}
	public function ActualizarUsuario(Usuario $data)
	{
		$sql ="UPDATE usuarios SET 
		usuario=?
		WHERE idEmpleado=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->usuario,
				$data->idEmpleado
			)
		);
	}

	//Metodo para actualizar perfil de usuarios
	public function ActualizarPic(Usuario $data)
	{
		$sql ="UPDATE usuarios SET 
		foto = ? 
		WHERE idUsuario = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->foto,
				$data->idUsuario
			)
		);
	}

	//Metodo para actualizar password de usuarios
	public function ActualizarPass(Usuario $data)
	{
		$sql ="UPDATE usuarios SET 
		pass=? 
		WHERE idUsuario=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->passnueva,
				$data->idUsuario
			)
		);
	}

	//Metodo para desactivar usuarios
	public function Desactivar($idUsuario)
	{
		$sql ="UPDATE usuarios SET activo=0 WHERE idUsuario=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idUsuario
			)
		);
	}

	//Metodo para desactivar usuarios
	public function Activar($idUsuario)
	{
		$sql ="UPDATE usuarios SET activo=1 WHERE idUsuario=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idUsuario
			)
		);
	}

	public function verificarPuesto($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios as u, empleados as e, puestos as p WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND u.idUsuario = ? ");
		$stm->execute(array($idUsuario));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	//Verificar usuarios existentes para dar acceso en login
	public function Verificar(Usuario $data)
	{
		$sql= $this->pdo->prepare("SELECT * FROM usuarios u, empleados e, puestos p, modulos m, niveles tu WHERE u.email=? AND u.pass=? AND u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = tu.idNivel");
		$resultado=$sql->execute(
			array(
				$data->email,
				$data->pass
			)
		);
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}

	//Verificar usuarios existentes para dar acceso en login
	public function VerificarCorreo($email)
	{
		$sql= $this->pdo->prepare("SELECT * FROM usuarios WHERE email = '$email'");
		$resultado=$sql->execute();
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}

	public function VerificarPass($password)
	{
		$sql= $this->pdo->prepare("SELECT * FROM usuarios WHERE pass = '$password'");
		$resultado=$sql->execute();
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}

	public function ObtenerDatosEmpleado($idEmpleado)
	{
		$sql= $this->pdo->prepare("SELECT e.email, m.idModulo, m.modulo, n.nivel, p.idPuesto FROM empleados e, puestos p, modulos m, niveles n WHERE e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = n.idNivel AND idEmpleado = ?");
		$resultado=$sql->execute(array($idEmpleado));
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}

	//Metodo para listar todos los usuarios
	public function ListarTiposUsuario($idModulo)
	{
		$stm = $this->pdo->prepare("SELECT tu.tipo, tum.idNivelUM FROM niveles tu, niveles_modulos tum, modulos m WHERE tu.idNivel=tum.idNivel AND tum.idModulo=m.idModulo AND m.idModulo = ?");
		$stm->execute(array($idModulo));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function VerificarAdminRH(Usuario $data)
	{
		$sql= $this->pdo->prepare("SELECT * FROM usuarios u, empleados e, puestos p WHERE u.idEmpleado = e.idEmpleado AND e.idPuesto = p.idPuesto AND u.email = ? AND u.pass = ? AND ((p.idNivel = 1 AND p.idModulo = 1) OR (p.idNivel = 2 AND p.idModulo = 1))");
		$resultado=$sql->execute(
			array(
				$data->email,
				$data->pass
			)
		);
		return $sql->fetchAll(PDO::FETCH_OBJ);
	}

	public function checar($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario = ? ");
		$stm->execute(array($idUsuario));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function verificarUsuario($usuario, $idUsuario)
	{
		if ($idUsuario == "")
			$sql = $this->pdo->prepare("SELECT * FROM usuarios WHERE usuario = '$usuario'");
		else
			$sql = $this->pdo->prepare("SELECT * FROM usuarios WHERE usuario = '$usuario' AND idUsuario <>  $idUsuario");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;	
	}

	public function GeneraTokenPass($idUsuario, $token)
	{
		$sql ="UPDATE usuarios SET activo=2, token_password=?, password_request=1 WHERE idUsuario = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$token,
				$idUsuario,
			)
		);
	}

	public function VerificaTokenPass($idUsuario, $token)
	{
		$sql = $this->pdo->prepare("SELECT activo FROM usuarios WHERE idUsuario = ? AND token_password = ? AND password_request = 1 LIMIT 1");
		$sql->execute(array($idUsuario, $token));
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}

	public function verificarEmail($email, $idUsuario)
	{
		if ($idUsuario == "")
			$sql = $this->pdo->prepare("SELECT * FROM usuarios WHERE email = '$email'");
		else
			$sql = $this->pdo->prepare("SELECT * FROM usuarios WHERE email = '$email' AND idUsuario <>  $idUsuario");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;	
	}

	//Metodo para actualizar password de usuarios
	public function RestablecerPass(Usuario $data)
	{
		$sql ="UPDATE usuarios SET 
		pass=?,
		token_password=null,
		password_request=0,
		activo = 1 
		WHERE idUsuario=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->passnueva,
				$data->idUsuario
			)
		);
	}

	public function LimpiarPermisos($idUsuario)
	{
		$sql ="DELETE FROM permisos_usuarios WHERE idUsuario=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idUsuario
			)
		);
	}

	public function InsertarPermisos($idUsuario, $tipo, $idRelacion, $idElement)
	{
		$sql ="INSERT INTO permisos_usuarios VALUES(?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idUsuario,	
				$tipo,
				$idRelacion,
				$idElement
			)
		);
	}

	public function ObtenerPermisos($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * FROM permisos_usuarios WHERE idUsuario=?");
		$stm->execute(array($idUsuario));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ObtenerPermisosPuesto($idPuesto)
	{
		$stm = $this->pdo->prepare("SELECT * FROM cat_permisos_puestos WHERE idPuesto=?");
		$stm->execute(array($idPuesto));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}


}