<?php
class Empleado
{
	public $idEmpleado;
	public $nombre;
	public $ubicacion;
	public $calle;
	public $numero;
	public $ciudad;
	public $estado;
	public $cp;
	public $pais;
	public $telefono;
	public $email;
	public $curp;
	public $rfc;
	public $nss;
	public $noEmpleado;
	public $idPuesto;
	public $sueldo;
	public $fsrOcho;
	public $fsrDoce;
	public $antiguedad;
	public $fechaInicio;
	public $sueldoOcho;
	public $sueldoDoce;
	public $status;
	private $pdo;    
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListarTA($bTexto, $bAlfabetica, $bStatus)
	{
		$stm = $this->pdo->prepare("SELECT * FROM empleados WHERE nombre LIKE '$bAlfabetica%' AND (curp LIKE '%$bTexto%' OR rfc LIKE '%$bTexto%' OR email LIKE '%$bTexto%' OR telefono LIKE '%$bTexto%' OR nss LIKE '%$bTexto%') AND status = '$bStatus'");
		$stm->execute();
		return $stm;
	}

	public function ListarT($bTexto, $bStatus)
	{
		$stm = $this->pdo->prepare("SELECT * FROM empleados WHERE (nombre LIKE '%$bTexto%' OR curp LIKE '%$bTexto%' OR rfc LIKE '%$bTexto%' OR email LIKE '%$bTexto%' OR telefono LIKE '%$bTexto%' OR nss LIKE '%$bTexto%') AND status = '$bStatus'");
		$stm->execute();
		return $stm;
	}

	public function ObtenerPuesto($idPuesto)
	{
		$stm = $this->pdo->prepare("SELECT puesto FROM puestos WHERE idPuesto = ?");
		$stm->execute(array($idPuesto));
		$nombre = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $nombre;
	}

	public function ObtenerDatosEmpleado($idEmpleado)
	{
		$sql= $this->pdo->prepare("SELECT  m.idModulo, m.modulo FROM empleados e, puestos p, modulos m WHERE e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND idEmpleado = ? ");
		$resultado=$sql->execute(array($idEmpleado));
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}

		//Metodo para verificar si hay datos duplicados en el NoEmpleado
	public function verificarNoEmpleado($NoEmpleado, $idEmpleado)
	{
		if ($idEmpleado == "")
			$sql = $this->pdo->prepare("SELECT * FROM empleados WHERE noEmpleado = $NoEmpleado");
		else
			$sql = $this->pdo->prepare("SELECT * FROM empleados WHERE noEmpleado = $NoEmpleado AND idEmpleado <>  $idEmpleado");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;					
	}

	public function verificarEmail($email, $idEmpleado)
	{
		if ($idEmpleado == "")
			$sql = $this->pdo->prepare("SELECT * FROM empleados WHERE email = '$email'");
		else
			$sql = $this->pdo->prepare("SELECT * FROM empleados WHERE email = '$email' AND idEmpleado <>  $idEmpleado");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;	
	}

	//Metodo para registrar actividades
	public function Registrar(Empleado $data)
	{
		$sql ="INSERT INTO empleados VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->nombre,
				$data->ubicacion,
				$data->calle,
				$data->numero,
				$data->ciudad,
				$data->estado,
				$data->cp,
				$data->pais,
				$data->telefono,
				$data->email,
				$data->curp,
				$data->rfc,
				$data->nss,
				$data->noEmpleado,
				$data->idPuesto,
				$data->sueldo,
				$data->fsrOcho,
				$data->fsrDoce,
				$data->antiguedad,
				$data->fechaInicio,
				$data->sueldoOcho,
				$data->sueldoDoce,
				'activo',
				null,
				null,
				null,
				null,
				null,
				null,
				null
			)
		);
		$this->RegistrarBitacora($this->pdo->lastInsertId(),'Insert', 'empleados');
	}

	//Metodo para actualizar actividades
	public function Actualizar(Empleado $data)
	{
		$sql ="UPDATE empleados SET 
		nombre=?,
		ubicacion=?,
		calle=?,
		numero=?,
		ciudad=?,
		estado=?,
		cp=?,
		pais=?,
		telefono=?,
		email=?,
		curp=?,
		rfc=?,
		nss=?,
		sueldo=?,
		fechaInicio=?,
		fsrOcho=?,
		fsrDoce=?,
		sueldoOcho=?,
		sueldoDoce=?,
		antiguedad=?,
		idPuesto=?
		WHERE idEmpleado=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombre,
				$data->ubicacion,
				$data->calle,
				$data->numero,
				$data->ciudad,
				$data->estado,
				$data->cp,
				$data->pais,
				$data->telefono,
				$data->email,
				$data->curp,
				$data->rfc,
				$data->nss,
				$data->sueldo,
				$data->fechaInicio,
				$data->fsrOcho,
				$data->fsrDoce,
				$data->sueldoOcho,
				$data->sueldoDoce,
				$data->antiguedad,
				$data->idPuesto,
				$data->idEmpleado
			)
		);

		$this->RegistrarBitacora($data->idEmpleado,'Update', 'empleados');
	}
	/*public function ActualizarEmp(Empleado $data)
	{
		$sql ="UPDATE empleados SET 
		nombre=?,
		email=?
		WHERE idEmpleado=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombre,
				$data->email,
				$data->idEmpleado
			)
		);
	}*/

	//Metodo para dar de baja un empleado
	public function Baja($idEmpleado)
	{
		$sql = "UPDATE empleados SET 
		status = 'Inactivo',
		idPuesto = ?
		WHERE idEmpleado = ?";
		$this->pdo->prepare($sql)
		->execute(array(null, $idEmpleado));
	}
	public function BajaUsuario($idEmpleado)
	{
		$sql = "UPDATE usuarios SET 
		activo = 0
		WHERE idEmpleado = ?";
		$this->pdo->prepare($sql)
		->execute(array($idEmpleado));
		$this->RegistrarBitacora($idEmpleado,'Baja Usuario', 'empleados');
	}

		//Metodo para reactivar un empleado
	public function Reactivar($fechaInicio, $idEmpleado, $idPuesto)
	{
		$sql = "UPDATE empleados SET
		idPuesto = ?,
		fechaInicio = ?,
		status = 'activo'
		WHERE idEmpleado = ? ";
		$this->pdo->prepare($sql)
		->execute(array($idPuesto, $fechaInicio, $idEmpleado));
		$this->RegistrarBitacora($idEmpleado,'Reactivar', 'empleados');
	}

	public function Obtener($idEmpleado)
	{
		$stm = $this->pdo
		->prepare("SELECT * FROM empleados, puestos WHERE idEmpleado = ? AND empleados.idPuesto = puestos.idPuesto");

		$stm->execute(array($idEmpleado));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function RegistrarBitacora($idRelacion, $accion, $table)
	{
		$sql ="INSERT INTO bitacoras VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$accion,
				$table,
				$idRelacion,
				$_SESSION['idUsuario'],
				date("Y-m-d H:i:s")
			)
		);
		return $this->pdo->lastInsertId();
	}

	


}