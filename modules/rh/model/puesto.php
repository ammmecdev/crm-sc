<?php
class Puesto
{
	public $idPuesto;
	public $noPersonas;
	public $puesto;
	public $ididNivel;
	public $idModulo;
	public $idNodo;
	public $parent;
	public $idRelacion;
	public $tipo;
	private $pdo;    
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT * FROM puestos, modulos, niveles WHERE puestos.idModulo = modulos.idModulo AND puestos.idNivel = niveles.idNivel ORDER BY modulos.modulo, niveles.nivel");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ); 
	}

	//Metodo para registrar puestos
	public function RegistrarPuesto(Puesto $data)
	{
		$sql ="INSERT INTO puestos VALUES(?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idPuesto,
				$data->puesto,
				$data->idModulo,
				$data->idNivel
			)
		);
	}

	//Metodo para actualizar 
	public function Actualizar(Puesto $data)
	{
		$sql ="UPDATE puestos SET 
		puesto=?,
		idNivel=?
		WHERE idPuesto=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->puesto,
				$data->idNivel,
				$data->idPuesto,
			)
		);
	}

	public function RegistrarNodo(Puesto $data)
	{
		$sql ="INSERT INTO nodos VALUES(?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->tipo,
				$data->idRelacion,
				$data->parent
			)
		);
	}	

	public function ActualizarNodo(Puesto $data)
	{
		$sql ="UPDATE nodos SET 
		parent=?
		WHERE idNodo=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->parent,
				$data->idNodo,
			)
		);
	}

	public function ObtenerNodo($idRelacion){
		$stm = $this->pdo->prepare("SELECT * FROM nodos WHERE idRelacion = ?");
		$stm->execute(array($idRelacion));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function ObtenerInfoNodo($id, $tipo){
		if($tipo=="modulo")
			$stm = $this->pdo->prepare("SELECT * FROM modulos WHERE idModulo = ?");
		else
			$stm = $this->pdo->prepare("SELECT * FROM puestos, niveles WHERE idPuesto = ? AND puestos.idNivel = niveles.idNivel");
		$stm->execute(array($id));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function ObtenerNodoPadre($id){
		$stm = $this->pdo->prepare("SELECT * FROM nodos WHERE idNodo = ?");
		$stm->execute(array($id));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function ObtenerPadre($id,$tipo){
		if($tipo=="modulo")
			$stm = $this->pdo->prepare("SELECT * FROM modulos WHERE idModulo = ?");
		else
			$stm = $this->pdo->prepare("SELECT * FROM puestos WHERE idPuesto = ?");
		$stm->execute(array($id));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function ObtenerOrganigrama()
	{
		$stm = $this->pdo->prepare("SELECT * FROM nodos");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ); 
	}

	public function ObtenerUltimoId()
	{
		$stm = $this->pdo->prepare("SELECT MAX(idPuesto)  AS idPuesto FROM puestos;");
		$stm->execute(array());
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function VerificaEnNodos($idPuesto)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) AS dependientes FROM nodos WHERE parent=?;");
		$stm->execute(array($idPuesto));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}

	public function ContarEmpleados($idPuesto)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) AS numeroEmpleados FROM empleados WHERE idPuesto=?;");
		$stm->execute(array($idPuesto));
		return $stm->fetch(PDO::FETCH_OBJ);	
	}
	
	//Metodo para dar de baja un puesto
	public function Baja($idPuesto)
	{
		$sql = "DELETE FROM puestos WHERE idPuesto = ?";
		$this->pdo->prepare($sql)->execute(array($idPuesto));
	}

	//Metodo para dar de baja un puesto
	public function EliminarNodo($idPuesto)
	{
		$sql = "DELETE FROM nodos WHERE idRelacion = ?";
		$this->pdo->prepare($sql)->execute(array($idPuesto));
	}

	public function ListarNiveles()
	{
		$stm = $this->pdo->prepare("SELECT * FROM niveles");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ); 
	}

	public function LimpiarPermisos($idPuesto)
	{
		$sql ="DELETE FROM cat_permisos_puestos WHERE idPuesto=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idPuesto
			)
		);
	}

	public function InsertarPermisos($idPuesto, $tipo, $idRelacion, $idElement)
	{
		$sql ="INSERT INTO cat_permisos_puestos VALUES(?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idPuesto,	
				$idElement,
				$idRelacion,
				$tipo
			)
		);
	}

	public function ObtenerPermisos($idPuesto)
	{
		$stm = $this->pdo->prepare("SELECT * FROM cat_permisos_puestos WHERE idPuesto=?");
		$stm->execute(array($idPuesto));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ObtenerNodosHijo($idNodo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM nodos WHERE parent=?");
		$stm->execute(array($idNodo));
		return $stm->fetchAll(PDO::FETCH_OBJ); 	
	}

	public function ActualizarModulo($idPuesto, $idModulo)
	{
		$sql ="UPDATE puestos SET 
		idModulo=?
		WHERE idPuesto=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idModulo,
				$idPuesto
			)
		);
	}	
}

