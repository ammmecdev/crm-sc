<?php
class Modulo
{
	public $idModulo;
	public $modulo;
	private $pdo;    
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT * FROM modulos");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ); 
	}

	//Metodo para actualizar 
	public function Actualizar(Modulo $data)
	{
		$sql ="UPDATE modulos SET 
		modulo=?
		WHERE idModulo=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->modulo,
				$data->idModulo
			)
		);
	}
	public function ListarModulos()
	{
		$stm = $this->pdo->prepare("SELECT m.idModulo, m.modulo FROM modulos as m  WHERE NOT EXISTS (SELECT NULL FROM nodos as n WHERE n.idRelacion = m.idModulo AND n.tipo = 'modulo')");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarSecciones($idModulo)
	{
		try {
			$stm = $this->pdo->prepare("SELECT * FROM cat_secciones WHERE idModulo=?");
			$stm->execute(array($idModulo));
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
		
	}

	public function ListarFunciones($idSeccion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM cat_funciones WHERE idSeccion=?");
		$stm->execute(array($idSeccion));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
}