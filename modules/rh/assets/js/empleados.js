$(document).ready(function() {
	consultas();

	$("input").dblclick(function() {
		var id_attr = "#" + $(this).attr("id");
		$(id_attr).removeAttr("readonly");
		$(id_attr).focus();
	});

	$(".double").blur(function() {
		var id_attr = "#" + $(this).attr("id");
		$(id_attr).prop('readonly', true);
	});

	$('#form-empleados').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					$("#mensaje").html(respuesta);
					$('#mRegistrado').modal({backdrop: 'static', keyboard: false})
				}
			});
			return false;
		},
		fields: {
			nombre: {
				validators: {
					regexp: {
						regexp: /^([a-zA-Z ÑñÁÉÍÓÚáéíóú])*$/,
						message: 'Sólo letras'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			email: {
				validators: {
					regexp: {
						regexp: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
						message: 'Ingrese un correo válido'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			telefono: {
				validators: {
					stringLength: {
						min: 7,
						max: 10,
						message: 'Mínimo 7 - Máximo 10 caracteres '
					},
					regexp: {
						regexp: /^[0-9]+$/,
						message: 'Sólo números'
					},
				}
			},
			fechaInicio: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			curp: {
				validators: {
					stringLength: {
						min: 18,
						max: 18,
						message: 'Mínimo 18 - Máximo 18 caracteres'
					},
					regexp: {
						regexp: /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
						message: 'La CURP no es válida'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			rfc: {
				validators: {
					stringLength: {
						min: 13,
						max: 13,
						message: 'Mínimo 13 - Máximo 13 caracteres'
					},
					regexp: {
						regexp: /([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})/,
						message: 'El RFC no es válido'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			nss: {
				validators: {
					regexp: {
						regexp: /^(\d{2})(\d{2})(\d{2})\d{5}$/,
						message: 'El NSS no es válido'
					},
					stringLength: {
						min: 11,
						max: 11,
						message: 'El NSS se compone de 11 dígitos'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			noEmpleado: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			sueldo: {
				validators: {
					regexp: {
						regexp: /^(([0-9]){1})?([0-9]+([.]?){1})?(([0-9]{2,2})+){1}?$/,
						message: 'Formato incorrecto'
						/*^((([0-9])+[,]){1})?([0-9]+([.]?){1})?(([0-9]{2,2})+){1}?$*/
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			idPuesto: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			fsrOcho: {
				validators: {
					regexp: {
						regexp: /^[0-9]{1}\.[0-9]{2}$/,
						message: 'Formato incorrecto'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			fsrDoce: {
				validators: {
					regexp: {
						regexp: /^[0-9]{1}\.[0-9]{2}$/,
						message: 'Formato incorrecto'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
		}
	})
});

var bAlfabetica='';
var bStatus='';

consultas = function ()
   { //Metodo de busqueda por ajax
   	var bTexto=$('#txtBuscar').val();
   	$.post("index.php?c=empleados&a=Consultas", {bTexto: bTexto, bAlfabetica: bAlfabetica, bStatus: bStatus}, function(mensaje) {
   		console.log(mensaje);
   		$("#resultadoBusqueda").html(mensaje[0].tabla);
   		if(mensaje[0].contador==1)
   			$("#contador").html("1 empleado");
   		else
   			$("#contador").html(mensaje[0].contador+" empleados");
   	});
   }

   function myFunctionEliminar()
   {
   	var idCliente = $('#txtIdCliente').val();
   	$('#txtIdClienteE').val(idCliente);
   }

   function filtrarPorLetra(busqueda)
   {
   	$("#txtBAlfabetica").val(busqueda);
   	bAlfabetica = busqueda;
   	switch(busqueda) {
   		case 'A':
   		desmarcar("a");
   		break;
   		case 'B':
   		desmarcar("b");
   		break;
   		case 'C':
   		desmarcar("c");
   		break;
   		case 'D':
   		desmarcar("d");
   		break;
   		case 'E':
   		desmarcar("e");
   		break;
   		case 'F':
   		desmarcar("f");
   		break;
   		case 'G':
   		desmarcar("g");
   		break;
   		case 'H':
   		desmarcar("h");
   		break;
   		case 'I':
   		desmarcar("i");
   		break;
   		case 'J':
   		desmarcar("j");
   		break;
   		case 'K':
   		desmarcar("k");
   		break;
   		case 'L':
   		desmarcar("l");
   		break;
   		case 'M':
   		desmarcar("m");
   		break;
   		case 'N':
   		desmarcar("n");
   		break;
   		case 'Ñ':
   		desmarcar("ñ");
   		break;
   		case 'O':
   		desmarcar("o");
   		break;
   		case 'P':
   		desmarcar("p");
   		break;
   		case 'Q':
   		desmarcar("q");
   		break;
   		case 'R':
   		desmarcar("r");
   		break;
   		case 'S':
   		desmarcar("s");
   		break;
   		case 'T':
   		desmarcar("t");
   		break;
   		case 'U':
   		desmarcar("u");
   		break;
   		case 'V':
   		desmarcar("v");
   		break;
   		case 'W':
   		desmarcar("w");
   		break;
   		case 'X':
   		desmarcar("x");
   		break;
   		case 'Y':
   		desmarcar("y");
   		break;
   		case 'Z':
   		desmarcar("z");
   		break;
   		default:
          //bAlfabetica='';
          desmarcar("todas");
          break;
      }
      consultas();
  }

  function desmarcar(letra){
  	var miarray = ["todas", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

  	for (var i = 0; i < miarray.length; i++) {
  		if (letra == miarray[i]) {
  			$("#btn-"+miarray[i]).addClass("btn-primary");
  			$("#btn-"+miarray[i]).removeClass("btn-default");
  		}else{
  			$("#btn-"+miarray[i]).removeClass("btn-primary");
  			$("#btn-"+miarray[i]).addClass("btn-default");
  		}
  	}
  }

  function filtrarPorStatus(status){
  	bStatus = status;
  	$("#txtBStatus").val(bStatus);
  	switch(status) {
  		case 'Activo':
  		$("#btn-activo").addClass("btn-primary");
  		$("#btn-activo").removeClass("btn-default");
  		$("#btn-inactivo").removeClass("btn-danger");
  		$("#btn-inactivo").addClass("btn-default");
  		break;
  		case 'Inactivo':
  		$("#btn-inactivo").addClass("btn-danger");
  		$("#btn-inactivo").removeClass("btn-default");
  		$("#btn-activo").removeClass("btn-primary");
  		$("#btn-activo").addClass("btn-default");
  		break;
  		default:
  		break;
  	}
  	consultas();
  }

  function bajaEmpleado(idEmpleado)
  {
  	$('#txtIdEmpleado').val(idEmpleado);
  }

  function activarEmpleado(idEmpleado) {
  	consultaPuestos();
  	$('#txtIdEmpleados').val(idEmpleado);
  	$('#txtFechaInicio').val("");
  	$('#selectPuesto').val("");
  }

  $('#form-baja').submit(function(event) {
  	$.ajax({
  		type: 'POST',
  		url: $(this).attr('action'),
  		data: $(this).serialize(),
  		success: function(respuesta){
  			$('#mBajaEmpleado').modal('toggle');
  			if (respuesta == 'true')
  				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>Se ha dado de baja correctamente el empleado</center></strong></div>');
  			else
  				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>Ha ocurrido un error al intentar dar debaja al empleado</center></strong></div>');
  			$('#mensajejs').show();
  			$('#mensajejs').delay(3500).hide(600);
  			consultas();
  		}
  	});
  	return false;
  });


  $('#form-reactivar').submit(function(event) {
  	$.ajax({
  		type: 'POST',
  		url: $(this).attr('action'),
  		data: $(this).serialize(),
  		success: function(respuesta){
  			// console.log(respuesta);
  			$('#mActEmpleado').modal('toggle');
  			if (respuesta == 'true')
  				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>Se ha reactivado correctamente el empleado</center></strong></div>');
  			else
  				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>Ha ocurrido un error al intentar reactivar al empleado</center></strong></div>');
  			$('#mensajejs').show();
  			$('#mensajejs').delay(3500).hide(600);
  			consultas();
  		}
  	});
  	return false;
  });

  $("#txtNoEmpleado").keyup(function() {
  	$('#alerta').hide();
  	var NoEmpleado = $('#txtNoEmpleado').val();
  	var idEmpleado = $('#txtIdEmpleado').val();
  	if (!isNaN(NoEmpleado)) {
  		$.post("index.php?c=empleados&a=verificarNoEmpleado", {NoEmpleado: NoEmpleado, idEmpleado:idEmpleado}, function(respuesta) {
  			if (respuesta == 0){
  				$( "#btnGuardar" ).prop("disabled", false);
  			}else if (respuesta == 1){
  				$('#alerta').show();
  				$("#alerta").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este número ya existe </p>');
  				$("#btnGuardar").prop("disabled", true);
  			}
  		})
  	}
  });

  $("#txtEmail").keyup(function() {
  	$('#alertaEmail').hide();
  	var email = $('#txtEmail').val();
  	var idEmpleado = $('#txtIdEmpleado').val();
  	$.post("index.php?c=empleados&a=verificarEmail", {email: email, idEmpleado:idEmpleado}, function(respuesta) {
  		console.log(respuesta);
  		if (respuesta == 0){
  			$( "#btnGuardar" ).prop("disabled", false);
  		}else if (respuesta == 1){
  			$('#alertaEmail').show();
  			$("#alertaEmail").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este correo electrónico ya existe </p>');
  			$("#btnGuardar").prop("disabled", true);
  		}
  	})
  });

  function consultaPuestos() {
  	$("#selectPuestoA").empty();
  	$.ajax({
  		url: "index.php?c=empleados&a=consultaPuestos",
  		type: "POST",
  		success: function(respuesta){
  			console.log(respuesta);
  			var selectPuesto = document.getElementById("selectPuestoA");
  			if (jQuery.isEmptyObject(respuesta)) {
  				selectPuesto.options[0] = new Option("No se encontraron puestos registrados","");
  			}else{
  				selectPuesto.options[0] = new Option("Asignar puesto","");
  				j=0;
  				for (var i in respuesta) {
  					selectPuesto.options[++j] = new Option(respuesta[i].puesto,respuesta[i].idPuesto);
  				}
  			}
  			$('#txtIdEmpleado').selectpicker('refresh');
  		}
  	});
  }

  function verificarEmpleado($idEmpleado) {
  	if ($idEmpleado == idEmpleadoSes) {
  		alert("si");
  	}else{
  		alert("no");
  	}
  }

  var placeSearch, autocomplete;
  var componentForm = {
  	locality: 'long_name',
  	administrative_area_level_1: 'short_name',

  };

  function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        	{types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
        	document.getElementById(component).value = '';
        	document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        	var addressType = place.address_components[i].types[0];
        	if (componentForm[addressType]) {
        		var val = place.address_components[i][componentForm[addressType]];
        		document.getElementById(addressType).value = val;
        	}
        }
    }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
      	if (navigator.geolocation) {
      		navigator.geolocation.getCurrentPosition(function(position) {
      			var geolocation = {
      				lat: position.coords.latitude,
      				lng: position.coords.longitude
      			};
      			var circle = new google.maps.Circle({
      				center: geolocation,
      				radius: position.coords.accuracy
      			});
      			autocomplete.setBounds(circle.getBounds());
      		});
      	}
      }

      //function activaCampos(arg) {

      //}

      //function updateCampos()
      //{

      //}
      function mayus(e) {
      	e.value = e.value.toUpperCase();
      }
