$(document).ready(function(){
    obtenerNiveles();
    $('#cp2').colorpicker();
    $('#cp4').colorpicker().on('changeColor', function(e) {
        $('#nodoPrueba')[0].style.backgroundColor = e.color.toString(
            'rgba');

    });

    $("#selectPadre").change(function() {
     var padre = $('#selectPadre').val(); 
     res = padre.split(",");
     $('#txtParent').val(res[0]);
     $('#txtIdModulo').val(res[1]);
 });

    $("#selectTipo").change(function() {
      tipoNodo = $('#selectTipo').val();
      if(tipoNodo=='puesto'){
         $('#divPuesto').show();
         $('#divModulo').hide();
         $('#txtPuesto').focus();
         $('#btnGuardar').show();
     }else if(tipoNodo=='modulo'){
         $('#divPuesto').hide();
         $('#divModulo').show();
         $('#btnGuardar').show();
     }else{
         $('#divPuesto').hide();
         $('#divModulo').hide();
         $('#btnGuardar').hide();
     }
 });
    $('#form-puesto').bootstrapValidator({
      feedbackIcons: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'glyphicon glyphicon-refresh'
     },
     submitHandler: function(validator, form, submitButton) {
            // Use Ajax to submit form data
            $.ajax({
            	type: 'POST',
            	url: form.attr('action'),
            	data: form.serialize(),
            	success: function(respuesta){
            		$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
            		$('#mPuesto').modal('toggle');
            		$('#mensajejs').show();
            		$('#mensajejs').delay(3500).hide(600);
            		crearOrganigrama();
            	}
            });    
            return false;
        },
        fields: {
        	idModulo2: {
        		validators: {
        			notEmpty: {
        				message: 'Campo Obligatorio'
        			}
        		}
        	},
        	puesto: {
        		validators: {
        			notEmpty: {
        				message: 'Campo Obligatorio'
        			}
        		}
        	},
        	nivel: {
        		validators: {
        			notEmpty: {
        				message: 'Campo Obligatorio'
        			}
        		}
        	},
        }
    });
    $('#mPuesto')
    .on('shown.bs.modal', function() {

    })
    .on('hidden.bs.modal', function () {
      $('#form-puesto').bootstrapValidator('resetForm', true);
  });

    $('#form-nivel').bootstrapValidator({
      feedbackIcons: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'glyphicon glyphicon-refresh'
     },
     submitHandler: function(validator, form, submitButton) {
            // Use Ajax to submit form data
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                success: function(respuesta){
                    $("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
                    $('#mNivel').modal('toggle');
                    $('#mensajejs').show();
                    $('#mensajejs').delay(3500).hide(600);
                    obtenerNiveles();
                }
            });    
            return false;
        },
        fields: {
            nombreNivel: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un nombre de nivel'
                    }
                }
            },
        }
    });
    $('#mNivel')
    .on('shown.bs.modal', function() {

    })
    .on('hidden.bs.modal', function () {
      $('#form-puesto').bootstrapValidator('resetForm', true);
  });

    $('#form-eliminar').submit(function(){
      $.ajax({
         type: 'POST',
         url: $(this).attr('action'),
         data: $(this).serialize(),
     }).done(function(respuesta){
         $('#mEliminar').modal('toggle');
         if(respuesta == "denegado1")
            $("#mensajejs").html('<div class="alert alert-danger alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>No se ha podido eliminar el puesto debido a que existen nodos dependientes de esta información. Reasigne la información ligada a el para poder dar la baja.</center></strong></div>');
        else if(respuesta == "denegado2")
            $("#mensajejs").html('<div class="alert alert-danger alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>No se ha podido eliminar el puesto debido a que existen empleados dependiendo de esta información. Reasigne la información ligada a el para poder dar la baja.</center></strong></div>');
        else{
            $("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
            crearOrganigrama();
        }
        $('#mensajejs').show();
        $('#mensajejs').delay(3500).hide(600);
    });
     return false;
 });
});

(function($) {

	crearOrganigrama();

	$.fn.orgChart = function(options) {
		var opts = $.extend({}, $.fn.orgChart.defaults, options);
		return new OrgChart($(this), opts);        
	}

	$.fn.orgChart.defaults = {
		data: [{id:1, name:'Root', parent: 0}],
		showControls: false,
		allowEdit: false,
		onAddNode: null,
		onDeleteNode: null,
		onClickNode: null,
		newNodeText: 'Add',
		exportButton: true,
		exportFilename: 'organigrama-ammmec',
		exportFileextension: 'pdf',
		chartClass: '',
	};

	function OrgChart($container, opts){
		var data = opts.data;
		var nodes = {};
		var rootNodes = [];
		this.opts = opts;
		this.$container = $container;
		var self = this;

		this.draw = function(){
			$container.empty().append(rootNodes[0].render(opts));
			$container.find('.node').click(function(){
				if(self.opts.onClickNode !== null){
					self.opts.onClickNode(nodes[$(this).attr('node-id')]);
				}
			});

			if(opts.allowEdit){
				$container.find('.node h2').click(function(e){
					var thisId = $(this).parent().attr('node-id');
					self.startEdit(thisId);
					e.stopPropagation();
				});
			}

             // append the export button
             if (this.opts.exportButton && !$container.find('.oc-export-btn').length) {
             	this.attachExportButton();
             }

            // add "add button" listener
            $container.find('.org-add-button').click(function(e){
            	var thisId = $(this).parent().attr('node-id');

            	if(self.opts.onAddNode !== null){
            		self.opts.onAddNode(nodes[thisId]);
            	}
            	else{
            		self.newNode(thisId);
            	}
            	e.stopPropagation();
            });

            $container.find('.org-del-button').click(function(e){
            	var thisId = $(this).parent().attr('node-id');

            	if(self.opts.onDeleteNode !== null){
            		self.opts.onDeleteNode(nodes[thisId]);
            	}
            	else{
            		self.deleteNode(thisId);
            	}
            	e.stopPropagation();
            });
        }

        this.attachExportButton = function () {
        	var that = this;
        	var $exportBtn = $('<button>', {
        		'class': 'oc-export-btn btn btn-primary' + (this.opts.chartClass !== '' ? ' ' + this.opts.chartClass : ''),
        		'text': 'Exportar',
        		'id': 'btnExportar',
        		'click': function(e) {
        			e.preventDefault();
        			that.export();
        		}

        	});
        	this.$container.append($exportBtn);
        }

        this.startEdit = function(id){
        	var inputElement = $('<input class="org-input" type="text" value="'+nodes[id].data.name+'"/>');
        	$container.find('div[node-id='+id+'] h2').replaceWith(inputElement);
        	var commitChange = function(){
        		var h2Element = $('<h2>'+nodes[id].data.name+'</h2>');
        		if(opts.allowEdit){
        			h2Element.click(function(){
        				self.startEdit(id);
        			})
        		}
        		inputElement.replaceWith(h2Element);
        	}  
        	inputElement.focus();
        	inputElement.keyup(function(event){
        		if(event.which == 13){
        			commitChange();
        		}
        		else{
        			nodes[id].data.name = inputElement.val();
        		}
        	});
        	inputElement.blur(function(event){
        		commitChange();
        	})
        }

        this.newNode = function(parentId){
        	var nextId = Object.keys(nodes).length;
        	while(nextId in nodes){
        		nextId++;
        	}
        	self.addNode({id: nextId, name: '', parent: parentId});
        }

        this.addNode = function(data){
        	var newNode = new Node(data);
        	nodes[data.id] = newNode;
        	nodes[data.parent].addChild(newNode);

        	self.draw();
        	self.startEdit(data.id);
        }

        this.deleteNode = function(id){
        	for(var i=0;i<nodes[id].children.length;i++){
        		self.deleteNode(nodes[id].children[i].data.id);
        	}
        	nodes[nodes[id].data.parent].removeChild(id);
        	delete nodes[id];
        	self.draw();
        }

        this.getData = function(){
        	var outData = [];
        	for(var i in nodes){
        		outData.push(nodes[i].data);
        	}
        	return outData;
        }

        this.export = function (exportFilename, exportFileextension) {
        	var that = this;
        	exportFilename = (typeof exportFilename !== 'undefined') ?  exportFilename : this.opts.exportFilename;
        	exportFileextension = (typeof exportFileextension !== 'undefined') ?  exportFileextension : this.opts.exportFileextension;
        	if ($(this).children('.spinner').length) {
        		return false;
        	}
        	var $container = this.$container;
        	var $mask = $container.find('.mask');

        	var sourceChart = $container.addClass('canvasContainer');
        	var flag = that.opts.direction === 'l2r' || that.opts.direction === 'r2l';
        	html2canvas(sourceChart, {
        		'width': flag ? sourceChart.clientHeight : sourceChart.clientWidth,
        		'height': flag ? sourceChart.clientWidth : sourceChart.clientHeight,
        		'onclone': function (cloneDoc) {
        			$(cloneDoc).find('.canvasContainer').css('overflow', 'visible')
        			.find('.orgchart:not(".hidden"):first').css('transform', '');
        		},
        		'onrendered': function (canvas) {
        			$container.find('.mask').addClass('hidden');
        			if (exportFileextension.toLowerCase() === 'pdf') {
        				var doc = {};
        				var docWidth = Math.floor(canvas.width * 0.2646);
        				var docHeight = Math.floor(canvas.height * 0.2646);
        				if (docWidth > docHeight) {
        					doc = new jsPDF('l', 'mm', [docWidth, docHeight]);
        				} else {
        					doc = new jsPDF('p', 'mm', [docHeight, docWidth]);
        				}
        				doc.addImage(canvas.toDataURL(), 'png', 0, 0);
        				doc.save(exportFilename + '.pdf');
        			} else {
        				var isWebkit = 'WebkitAppearance' in document.documentElement.style;
        				var isFf = !!window.sidebar;
        				var isEdge = navigator.appName === 'Microsoft Internet Explorer' || (navigator.appName === "Netscape" && navigator.appVersion.indexOf('Edge') > -1);

        				if ((!isWebkit && !isFf) || isEdge) {
        					window.navigator.msSaveBlob(canvas.msToBlob(), exportFilename + '.png');
        				} else {
        					var selector = '.oc-download-btn' + (that.opts.chartClass !== '' ? '.' + that.opts.chartClass : '');
        					if (!$container.find(selector).length) {
        						$container.append('<a class="oc-download-btn' + (that.opts.chartClass !== '' ? ' ' + that.opts.chartClass : '') + '"'
        							+ ' download="' + exportFilename + '.png"></a>');
        					}
        					$container.find(selector).attr('href', canvas.toDataURL())[0].click();
        				}
        			}
        		}
        	})
        	.then(function () {
        		$container.removeClass('canvasContainer');
        	}, function () {
        		$container.removeClass('canvasContainer');
        	});
        }

        // constructor
        for(var i in data){
        	var node = new Node(data[i]);
        	nodes[data[i].id] = node;
        }

        // generate parent child tree
        for(var i in nodes){
        	if(nodes[i].data.parent == 0){
        		rootNodes.push(nodes[i]);
        	}
        	else{
        		nodes[nodes[i].data.parent].addChild(nodes[i]);
        	}
        }

        // draw org chart
        $container.addClass('orgChart');
        self.draw();
    }

    function Node(data){
    	this.data = data;
    	this.children = [];
    	var self = this;

    	this.addChild = function(childNode){
    		this.children.push(childNode);
    	}

    	this.removeChild = function(id){
    		for(var i=0;i<self.children.length;i++){
    			if(self.children[i].data.id == id){
    				self.children.splice(i,1);
    				return;
    			}
    		}
    	}

    	this.render = function(opts){
    		var childLength = self.children.length,
    		mainTable;

    		mainTable = "<table cellpadding='0' cellspacing='0' border='0'>";
    		var nodeColspan = childLength>0?2*childLength:2;
    		mainTable += "<tr><td colspan='"+nodeColspan+"'>"+self.formatNode(opts)+"</td></tr>";

    		if(childLength > 0){
    			var downLineTable = "<table cellpadding='0' cellspacing='0' border='0'><tr class='lines x'><td class='line left half'></td><td class='line right half'></td></table>";
    			mainTable += "<tr class='lines'><td colspan='"+childLength*2+"'>"+downLineTable+'</td></tr>';

    			var linesCols = '';
    			for(var i=0;i<childLength;i++){
    				if(childLength==1){
                        linesCols += "<td class='line left half'></td>";    // keep vertical lines aligned if there's only 1 child
                    }
                    else if(i==0){
                        linesCols += "<td class='line left'></td>";     // the first cell doesn't have a line in the top
                    }
                    else{
                    	linesCols += "<td class='line left top'></td>";
                    }

                    if(childLength==1){
                    	linesCols += "<td class='line right half'></td>";
                    }
                    else if(i==childLength-1){
                    	linesCols += "<td class='line right'></td>";
                    }
                    else{
                    	linesCols += "<td class='line right top'></td>";
                    }
                }
                mainTable += "<tr class='lines v'>"+linesCols+"</tr>";

                mainTable += "<tr>";
                for(var i in self.children){
                	mainTable += "<td colspan='2'>"+self.children[i].render(opts)+"</td>";
                }
                mainTable += "</tr>";
            }
            mainTable += '</table>';
            return mainTable;
        }

        this.formatNode = function(opts){
        	var nameString = '',
        	descString = '';
        	if(typeof data.name !== 'undefined'){
        		nameString = '<h2>'+self.data.name+'</h2>';
        	}
        	if(typeof data.description !== 'undefined'){
        		descString = '<p>'+self.data.description+'</p>';
        	}
        	if(opts.showControls){
        		var btnAdd = "<button type='button' class='btn btn-primary btn-block org-add-button' data-toggle='modal' data-target='#mPuesto' ><i id='icon-control-add' class='fas fa-plus-circle'></i></button>";
        		var btnDel = "<button type='button' class='btn btn-danger btn-block org-del-button'><i id='icon-control-del' class='fas fa-minus-circle'></i></button>";
        		var buttonsHtml =btnAdd+btnDel;
        	} 
        	else{
        		buttonsHtml = '';
        	}
          return "<div class='node node"+self.data.nivel+"' node-id='"+this.data.id+"' style='background-color:"+this.data.back_color+"; color: "+this.data.color+ ";'>" + nameString + buttonsHtml + "</div>";
      }
  }
})(jQuery);

function crearOrganigrama()
{
	$.ajax({
		type:"POST",
		url:"?c=puestos&a=ObtenerOrganigrama",
		success : function(json){
			var testData = JSON.parse(json);
         org_chart = $('#orgChart').orgChart({
           data: testData,
           showControls: true,
           allowEdit: false,
           onAddNode: function(node){ 
              nuevoNodo(node.data.id, node.data.idModulo, node.data.name);          
              $('#mPuesto').modal('toggle');
                    //org_chart.newNode(node.data.id); 
                },
                onDeleteNode: function(node){
                	$('#txtIdPuestoE').val(node.data.idRelacion);
                	$('#mEliminar').modal('toggle');
                    // org_chart.deleteNode(node.data.id); 
                },
                onClickNode: function(node){
                	editarNodo(node.data.idRelacion, node.data.tipo, node.data.parent, node.data.id)
                	$('#mPuesto').modal('toggle'); // editar
                }
            });
     }
 });
}

function nuevoNodo(parent, idModulo, padre) 
{
	$('#divTipo').show();
    $('#nodoPadreTxt').show();
    $('#nodoPadreSelect').hide();
    $('#divModulo').hide();
    $('#divPuesto').hide();
    $('#txtIdNodo').val('0');
    $('#txtIdPuesto').val('0');
    $('#txtItem').val('');
    $('#txtPuesto').val('');
    $('#txtParent').val(parent);
    $('#txtIdModulo').val(idModulo);
    $('#btnPermisos').hide();
    $('#txtPadre').val(padre);
    selectModulos();

    var selectTipo = document.getElementById("selectTipo");
    selectTipo.options[0] = new Option("Seleccione el tipo de nodo" , "");
    selectTipo.options[1] = new Option("Puesto" , "puesto");
    selectTipo.options[2] = new Option("Módulo" , "modulo");

    var selectNivel = document.getElementById("selectNivel");
    selectNivel.options[0] = new Option("Seleccione el nivel del puesto" , "");

    selectNivel.options[1] = new Option("Nivel 2 - Subusuario" , 2);
    selectNivel.options[2] = new Option("Nivel 3 - Administrador" , 3);
    selectNivel.options[3] = new Option("Nivel 4 - Avanzado" , 4);
    selectNivel.options[4] = new Option("Nivel 5 - Regular" , 5);

    $('#btnEliminar').hide();
    $('#btnGuardar').val("Guardar");
    $('#btnGuardar').hide();
    $('#labTitulo').html("Nuevo nodo");
}

function selectModulos() {
	document.getElementById("selectIdModulo").options.length = 0;
	$.ajax({
		url: "?c=puestos&a=ListarModulos",
		type: "POST",
		success: function(respuesta){
			var selectModulos = document.getElementById("selectIdModulo");
			if (jQuery.isEmptyObject(respuesta)) {	
				selectModulos.options[0] = new Option("Ningún departamento disponible","");
			}else{
				selectModulos.options[0] = new Option("Seleccione el departamento del puesto","");
				j=0;
				for (var i in respuesta) {
					selectModulos.options[++j] = new Option(respuesta[i].modulo,respuesta[i].idModulo);
				}
			}

		}
	});
}

function editarNodo(idRelacion, tipo, parent, id) 
{
    if(parent==0){
        $('#mPuesto').modal('toggle');
    }
    $.ajax({
      type: 'POST',
      url: '?c=puestos&a=ObtenerInfoNodo',
      data: {idRelacion : idRelacion, tipo : tipo, parent : parent},
      success: function(res){
       $('#txtIdNodo').val(id);
       $('#txtParent').val(parent); 
       $('#btnGuardar').show(); 
       $('#divPuesto').show();
       $('#nodoPadreSelect').show();
       $('#nodoPadreTxt').hide();
       var selectPadre = document.getElementById('selectPadre');
       selectPadre.options.length = 0;
       selectPadre.options[0] = new Option(res[0].padre + " - " + res[0].tipoPadre, parent + "," + res[0].idModulo);
       $.get('?c=puestos&a=ObtenerNodos', function(nodos) {
        var o=0;
        for (var i in nodos)
            if(res[0].idModulo!=nodos[i].idModulo){
                if(id!=nodos[i].id)
                 selectPadre.options[++o] = new Option(nodos[i].name + " - " + nodos[i].tipo, nodos[i].id+ "," + nodos[i].idModulo);
         }
     });
       if(tipo=='puesto'){
        $('#divModulo').hide();
        var selectTipo = document.getElementById('selectTipo');
        selectTipo.options[0] = new Option('Puesto' , 'puesto');
        $('#selectTipo option:not(:selected)').attr('disabled',true);
        $('#txtIdPuesto').val(res[0].idPuesto);
        $('#txtPuesto').val(res[0].puesto);
        $('#txtItem').val(res[0].idPuesto);
        $('#txtIdModulo').val(res[0].idModulo);
        $('#btnPermisos').show();
        j=0;
        var selectNivel = document.getElementById("selectNivel");
        selectNivel.options.length =0;
        selectNivel.options[j++] = new Option("Nivel " + res[0].nivel, res[0].nivel);
        if(res[0].nivel != 1)
         selectNivel.options[j++] = new Option("Nivel 1 - Superusuario" , 1);
     if(res[0].nivel != 2)
         selectNivel.options[j++] = new Option("Nivel 2 - Subusuario" , 2);
     if(res[0].nivel != 3)
         selectNivel.options[j++] = new Option("Nivel 3 - Administrador" , 3);
     if(res[0].nivel != 4)
         selectNivel.options[j++] = new Option("Nivel 4 - Avanzado" , 4);
     if(res[0].nivel != 5)
         selectNivel.options[j++] = new Option("Nivel 5 - Regular" , 5);
 }else if(tipo=='modulo')
 {
    $('#txtIdPuesto').val('1');
    $('#divModulo').show();
    $('#divPuesto').hide();
    $('#btnPermisos').hide();
    var selectTipo = document.getElementById('selectTipo');
    selectTipo.options[0] = new Option('Módulo' , 'modulo');
    $('#selectTipo option:not(:selected)').attr('disabled',true);
    var selectIdModulo = document.getElementById("selectIdModulo");
    selectIdModulo.options.length =0;

    $.ajax({
     url: "?c=puestos&a=ListarModulos",
     type: "POST",
     success: function(respuesta){
      var selectModulos = document.getElementById("selectIdModulo");
      if (jQuery.isEmptyObject(respuesta)) {	
       selectModulos.options[0] = new Option("Ningún departamento disponible","");
   }else{
       selectModulos.options[0] = new Option(res[0].modulo,res[0].idModulo);
       j=0;
       for (var i in respuesta) {
        selectModulos.options[++j] = new Option(respuesta[i].modulo,respuesta[i].idModulo);
    }
}
}
});
}
$('#btnGuardar').val('Guardar');
$('#labTitulo').html("Editar nodo");
}
});
}

obtenerNiveles = function()
{
   $.ajax({
    url: "?c=puestos&a=ListarNiveles",
    type: "POST",
    success: function(respuesta){
        var niveles="";
        for (var i in respuesta) 
            niveles = niveles + "<div class='divInfo nodeInfo"+respuesta[i].idNivel+"' style='border-left: 10px solid "+respuesta[i].back_color+";'>"+respuesta[i].nivel+"</div>"
        nivelCero="<div class='divInfo nodeInfo' style='border-left: 10px solid #34495e;'>Nivel 0 - Módulos</div>";
        niveles = nivelCero + niveles;
        $('#divNiveles').html(niveles);
    }
});
}

function nuevoNivel() 
{
    $('#btnEliminar').hide();
    $('#labNivel').html("Nuevo nivel");
}

