$(document).ready(function(){
	consultas();
	consultaModulos();

	$('#form-eliminar').on('submit', function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(respuesta){
				if(respuesta == "denegado1")
					$("#mensajejs").html('<div class="alert alert-danger alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>No se ha podido eliminar el puesto debido a que existen nodos dependientes de esta información. Reasigne la información ligada a el para poder dar la baja.</center></strong></div>');
				else if(respuesta == "denegado2")
					$("#mensajejs").html('<div class="alert alert-danger alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>No se ha podido eliminar el puesto debido a que existen empleados dependiendo de esta información. Reasigne la información ligada a el para poder dar la baja.</center></strong></div>');
				else{
					$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
					consultas();
				}
				$('#mensajejs').show();
				$('#mPuesto').modal('toggle');
				$('#mEliminar').modal('toggle');
				$('#mensajejs').delay(3500).hide(600);
			}
		});      
		return false;
	});

	$('#form-puesto').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			$('#btnGuardar').prop('disabled', true);
			$('#btnGuardar').val('Procesando...');
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
					$('#mPuesto').modal('toggle');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					consultas();
				}
			});    
			return false;
		},
		fields: {
			puesto: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
		}
	});
	$('#mPuesto').on('hidden.bs.modal', function() {
		$('#form-puesto').bootstrapValidator('resetForm', true);
	});

	$('#form-modulo').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
            // Use Ajax to submit form data
            $.ajax({
            	type: 'POST',
            	url: form.attr('action'),
            	data: form.serialize(),
            	success: function(respuesta){
            		$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
            		$('#mModulo').modal('toggle');
            		$('#mensajejs').show();
            		$('#mensajejs').delay(3500).hide(600);
            		consultaModulos();
            	}
            });    
            return false;
        },
        fields: {
        	modulo: {
        		validators: {
        			notEmpty: {
        				message: 'Campo Obligatorio'
        			}
        		}
        	},
        }
    });
	$('#mModulo').on('hidden.bs.modal', function() {
		$('#form-modulo').bootstrapValidator('resetForm', true);
	});

});

  //Metodo de busqueda por ajax
  consultas = function (){ 
  	$.post("index.php?c=puestos&a=Consultas", {}, function(respuesta) {
  		$("#resultadoBusqueda").html(respuesta);
  	});   
  }

  consultaModulos = function(){
  	$.post("index.php?c=puestos&a=ConsultaModulos", {}, function(respuesta) {
  		$("#resultadoBusquedaModulos").html(respuesta);
  	});  
  }

  camposEditar = function()
  {
  	$('#labTitulo').html("Editar actividad");
  	$('#btnEliminar').show();
  }

  function eliminarPuesto()
  {
  	var idPuesto = $('#txtIdPuesto').val();
  	$('#txtIdPuestoE').val(idPuesto);  
  }

  function editarPuesto(idPuesto, puesto, nivel, idModulo) 
  {
  	$('#btnGuardar').prop('disabled', false);
  	$('#btnGuardar').val('Guardar');
  	$('#labTitulo').html("Editar puesto");
  	$('#txtIdPuesto').val(idPuesto);
  	$('#txtPuesto').val(puesto);
  	$('#txtIdModulo').val(idModulo);
  	j=0;
  	var selectNivel = document.getElementById("selectNivel");
  	selectNivel.options.length =0;
  	selectNivel.options[j++] = new Option("Nivel " + nivel, nivel);
  	if(nivel != 1)
  		selectNivel.options[j++] = new Option("Nivel 1 - Superusuario" , 1);
  	if(nivel != 2)
  		selectNivel.options[j++] = new Option("Nivel 2 - Subusuario" , 2);
  	if(nivel != 3)
  		selectNivel.options[j++] = new Option("Nivel 3 - Administrador" , 3);
  	if(nivel != 4)
  		selectNivel.options[j++] = new Option("Nivel 4 - Avanzado" , 4);
  	if(nivel != 5)
  		selectNivel.options[j++] = new Option("Nivel 5 - Regular" , 5);
  }


  function editarModulo(idModulo, modulo) 
  {
  	$('#labTituloModulo').html("Editar modulo");
  	$('#txtIdMod').val(idModulo);
  	$('#txtModulo').val(modulo);
  }

  permisosPuesto = function(idPuesto, puesto, nivel)
  {
  	$('#btnGuardarP').prop('disabled', false);
  	$('#btnGuardarP').val('Guardar');
  	$('#labPuesto').html(puesto);
  	$('#labNivel').html(nivel);
  	$('#txtIdPuestoP').val(idPuesto);
  	$('#txtPuestoP').val(puesto);
  	$.ajax({
  		type: 'POST',
  		url: '?c=puestos&a=ObtenerPermisos',
  		data: {'idPuesto':idPuesto},
  		success: function(respuesta){
  			$("#div_permisos").html(respuesta);
  		}
  	});
  }

  $('#form_permisos').submit(function(){
  	$('#btnGuardarP').prop('disabled', true);
  	$('#btnGuardarP').val('Procesando...');
  	$.ajax({
  		type: 'POST',
  		url: $(this).attr('action'),
  		data: $(this).serialize(),
  		success: function(respuesta){
  			$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
  			$('#mPermisos').modal('toggle');
  			$('#mensajejs').show();
  			$('#mensajejs').delay(3500).hide(600);
  		}
  	});
  	return false;
  });
