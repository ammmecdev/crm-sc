$(document).ready(function() {
	var idModulo;

	$("#txtIdEmpleado").change(function(){
		idEmpleado=$('#txtIdEmpleado').val();
		$.ajax({
			url: '?c=usuarios&a=ObtenerDatosEmpleado&g=true',
			type: 'POST',
			data: {'idEmpleado':idEmpleado},
			success : function (res) {
				console.log(res);
				if (res[0].reset == "false") {
					desactivarCampos();
					$('#txtUsuario').val("");
					$('#txtPass').val("");
				}else{
					$('#divModulo').show();
					$('#txtModulo').val(res[0].modulo);
					$('#txtIdTipoUsuario').val(res[0].tipo);
					idModulo = res[0].idModulo;
					$('#divTipoUsuario').show();
					$('#divEmail').show();
					$('#txtEmail').val(res[0].email);
					$('#txtUsuario').focus();
					$('#txtIdPuesto').val(res[0].idPuesto);
				}
			}
		})
	});

	$('#form_usuarios').bootstrapValidator({
		excluded: [':disabled', ':hidden', ':not(:visible)'],
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			$('#btnGuardar').prop('disabled', true);
			$('#btnGuardar').val('Procesando...');
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta) {
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta+'</center></strong></div>'); 
					$('#mUsuario').modal('toggle');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					consultas();
					verSuspendido();
				}    
			}); 
			return false;
		},
		fields: {
			idEmpleado: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			modulo: {
				excluded: 'false',
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				},
			},
			idTipo: {
				excluded: 'false',
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				},
			},
			email: {
				excluded: 'false',
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				},
			},
			usuario: {
				validators: {
					stringLength: {
						min: 3,
						max: 15,
						message: 'Mínimo 3 - Máximo 15 letras'
					},
					regexp: {
						regexp: /^([a-zA-Z ÑñÁÉÍÓÚáéíóú])+$/,
						message: 'Sólo letras'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			pass: {
				validators: {
					regexp: {
						regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
						message: '- Al menos una letra mayúscula (A - Z)<br>- Al menos una letra minúscula (a - z)<br>- Al menos un dígito (0 - 9)<br>- Al menos un caracter especial ( ? = . * ? [ # ? ! @ $ % ^ & * - ] )<br>- Mínimo 8 de longitud'
					},
					notEmpty: {
						message: 'Ingrese una contraseña temporal'
					}
				}
			},
		}
	});
	$('#mUsuario')
	.on('shown.bs.modal', function() {

	})
	.on('hidden.bs.modal', function () {
		$('#form_usuarios').bootstrapValidator('resetForm', true);
	});
});

$('#form-desactivar').submit(function() {
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function(respuesta) {
			$('#mEliminar').modal('toggle');
			$('#mUsuario').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);  
			verDesactivados();
			consultas();
		}    
	})        
	return false;
}); 

obtenerNombre = function(){
	var nombreUsuario = $('#txtIdEmpleado option:selected').html();
	$('#txtNombreUsuario').val(nombreUsuario);
}

$("#txtUsuario").keyup(function() {
	$('#alertaUsuario').hide();
	var usuario = $('#txtUsuario').val();
	var idUsuario = $('#txtIdUsuario').val();
	$.post("?c=usuarios&a=verificarUsuario&g=true", {usuario:usuario, idUsuario:idUsuario}, function(respuesta) {
		if (respuesta == 0){
			$("#btnGuardar").prop("disabled", false);
		}else if (respuesta == 1){
			$('#alertaUsuario').show();
			$("#alertaUsuario").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este usuario ya existe </p>');
			$("#btnGuardar").prop("disabled", true);
		}
	})
});

function nuevoUsuario() {
	$('#labTitulo').html("Añadir usuario");
	$('#alertaUsuario').hide();
	$('#txtIdUsuario').val("");
	$('#alertaUsuario').html("");
	$('#txtNombreUsuario').val(""); 
	$('#txtIdTipoUsuario').val("");
	$('#txtUsuario').val("");
	$('#txtPass').val("");
	$('#txtEmail').val("");
	$('#txtUsuario').val("");
	$('#txtPass').val("");
	desactivarCampos();
	$('#btnGuardar').prop('disabled', false);
	$('#btnGuardar').val('Guardar');
	$("#txtIdEmpleado").empty();
	$.ajax({
		url: "?c=usuarios&a=ListarEmpleados&g=true",
		type: "POST",
		success: function(respuesta){
			// console.log(respuesta);
			var selectNombreUsuario = document.getElementById("txtIdEmpleado");
			if (jQuery.isEmptyObject(respuesta)) {	
				selectNombreUsuario.options[0] = new Option("Ningún empleado disponible","");
			}else{
				selectNombreUsuario.options[0] = new Option("Seleccione al empleado","");
				j=0;
				for (var i in respuesta) {
					selectNombreUsuario.options[++j] = new Option(respuesta[i].nombreEmpleado,respuesta[i].idEmpleado);
				}
			}	
			$('#txtIdEmpleado').selectpicker('refresh');
		}
	});
}

function desactivarCampos()
{
	$('#divTipoUsuario').hide();
	$('#divModulo').hide();
	$('#divEmail').hide();
	$('#btnEliminar').hide();
}

function editarUsuario(datos) {;
	var datos = datos.split(",");
	var idUsuario = datos[0];
	var idTipo = datos[1];
	var nombreUsuario = datos[2];
	var usuario = datos[3];
	var email = datos[4];
	var pass = datos [5];
	var tipo = datos[6];
	var idEmpleado = datos[7];
	var modulo = datos[8];
	var idModulo = datos[9];

	$('#divTipoUsuario').show();
	$('#divModulo').show();
	$('#divEmail').show();
	$('#btnEliminar').hide();
	$('#alertaUsuario').hide();
	$('#labTitulo').html("Editar usuario");
	$('#btnGuardar').prop('disabled', false);
	$('#btnGuardar').val('Guardar');
	$('#txtIdUsuario').val(idUsuario);
	$('#txtNombreUsuario').val(nombreUsuario); 
	$('#txtUsuario').val(usuario);
	$('#txtEmail').val(email);
	$('#txtIdTipoUsuario').val(tipo);
	$('#txtModulo').val(modulo);
	$('#txtPass').val("");
	desBtnEliminar(idUsuario);

  	//-- SELECT NOMBRE USUARIO --
  	$('#txtIdEmpleado').empty();
  	var selectNombreUsuario = document.getElementById("txtIdEmpleado");
  	selectNombreUsuario.options[0] = new Option(nombreUsuario,idEmpleado);
  	$('#txtIdEmpleado').selectpicker('refresh');
  }

  function myFunctionActivar(idUsuario){
  	$.post("?c=usuarios&a=verificarPuesto&g=true", {idUsuario: idUsuario}, function(validacion) {
  		if (validacion == "ok") {
  			datos = {"idUsuario":idUsuario};
  			$.ajax({
  				url: "index.php?c=usuarios&a=Activar&g=true",
  				type: "POST",
  				data: datos,
  				success: function(respuesta) {
  					verActivos();
  					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta+'</center></strong></div>');
  					$('#mensajejs').show();
  					$('#mensajejs').delay(3500).hide(600);  
  				}
  			});
  		}else{
  			verDesactivados();
  			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>No ha sido posible activar el usuario debido a que se encuentra desactivado en la sección de empleados</center></strong></div>');
  			$('#mensajejs').show();
  			$('#mensajejs').delay(3500).hide(600);
  		}
  	});
  }

  function desBtnEliminar(idUsuario){
  	if (idUsuarioSes == idUsuario) {
  		$('#btnEliminar').hide();
  	}else{
  		$('#btnEliminar').show();
  	}
  }
  
  function desactivarUsuario(){
  	var idUsuario = $('#txtIdUsuario').val();
  	$('#txtIdUsuarioE').val(idUsuario);  
  }

  function pintarTablaActivos(vBusqueda){
  	$.post("?c=usuarios&a=crearTablaActivos&g=true", {vBusqueda: vBusqueda}, function(tabla) {
  		$("#tabla-activos").html(tabla); 
  	});
  }

  function pintarTablaDesactivados(vBusqueda){
  	$.post("?c=usuarios&a=crearTablaDesactivados&g=true", {vBusqueda: vBusqueda}, function(tabla) {
  		$("#tabla-desactivos").html(tabla); 
  	});
  }

  function pintarTablaSuspendidos(vBusqueda) {
  	$.post("?c=usuarios&a=crearTablaSuspendidos&g=true", {vBusqueda: vBusqueda}, function(tabla) {
  		$("#tabla-suspendido").html(tabla); 
  	});
  }

  function consultas()
  {
  	var vBusqueda = $('#txtBuscar').val();
  	$.post("index.php?c=usuarios&a=Exportar&g=true", {vBusqueda: vBusqueda}, function(resultado) {
  	});

  	pintarTablaActivos(vBusqueda);
  	pintarTablaDesactivados(vBusqueda);
  	pintarTablaSuspendidos(vBusqueda);
  }

  window.onload=function(){
  	verActivos();
  	// consultas();
  }

  function verActivos(){
  	$("#liActivado").addClass("active");
  	$("#liDesactivado").removeClass("active");
  	$('#liSuspendido').removeClass("active");
  	$("#tabla-desactivos").hide();
  	$("#tabla-suspendido").hide();
  	$("#tabla-activos").show();
  	switchColor('A');
  	consultas();
  }

  function verDesactivados(){
  	$('#liDesactivado').addClass("active");
  	$('#liActivado').removeClass("active");
  	$('#liSuspendido').removeClass("active");
  	$("#tabla-activos").hide();
  	$("#tabla-suspendido").hide();
  	$("#tabla-desactivos").show();
  	switchColor('B');
  	consultas();
  }

  function verSuspendido() {
  	$('#liSuspendido').addClass("active");
  	$('#liActivado').removeClass("active");
  	$('#liDesactivado').removeClass("active");
  	$("#tabla-suspendido").show();
  	$("#tabla-activos").hide();
  	$("#tabla-desactivos").hide();
  	switchColor('C');
  	consultas();
  }

  function switchColor(option){
  	switch(option) {
  		case 'A':
  		$("#liActivado").addClass("btn-primary");
  		$("#liActivado").removeClass("btn-default");

  		$("#liDesactivado").removeClass("btn-primary");
  		$("#liDesactivado").addClass("btn-default");

  		$("#liSuspendido").removeClass("btn-primary");
  		$("#liSuspendido").addClass("btn-default");
  		break;
  		case 'B':
  		$("#liDesactivado").addClass("btn-primary");
  		$("#liDesactivado").removeClass("btn-default");

  		$("#liActivado").removeClass("btn-primary");
  		$("#liActivado").addClass("btn-default");

  		$("#liSuspendido").removeClass("btn-primary");
  		$("#liSuspendido").addClass("btn-default");

  		break;
  		case 'C':
  		$("#liSuspendido").addClass("btn-primary");
  		$("#liSuspendido").removeClass("btn-default");

  		$("#liActivado").removeClass("btn-primary");
  		$("#liActivado").addClass("btn-default");

  		$("#liDesactivado").removeClass("btn-primary");
  		$("#liDesactivado").addClass("btn-default");
  		break;
  		default:
  		break;
  	}
  }