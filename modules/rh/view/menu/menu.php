<?php 
$permisoEmp = $permisoPue = $permisoUsu = false;
foreach ($permisos as $p):
	switch ($p) {
		case 'sec_empleados':
		$permisoEmp=true;
		break;
		case 'sec_puestos':
		$permisoPue=true;
		break;
		case 'sec_usuarios':
		$permisoUsu=true;
		break;
		default:
		break;
	}
endforeach;
?>
<ul class="nav navbar-nav">
	<?php if ($permisoEmp == true) { ?>
		<li class="admin1"><a <?php if(isset($empleados)){ ?> id="seleccion" <?php } ?> href="?c=empleados"><i class="fas fa-address-card" style="font-size: 18px;"></i> Empleados</label><span class="sr-only">(current)</span></a></li>
	<?php } ?>
	<?php if ($permisoPue == true) { ?>
		<li class="admin1"><a <?php if(isset($puestos) && $permisoPue==true){ ?> id="seleccion" <?php } ?> href="?c=puestos"><i class="fas fa-briefcase" style="font-size: 18px;"></i></i> Puestos</label><span class="sr-only">(current)</span></a></li>
	<?php } ?>
	<?php if ($permisoUsu == true){ ?>
		<li class="admin1"><a <?php if(isset($usuarios) && $permisoUsu==true){ ?> id="seleccion" <?php } ?> href="?c=usuarios&g=true"><i class="fas fa fa-users" style="font-size: 18px;"></i> Usuarios</label><span class="sr-only">(current)</span></a></li>
	<?php } ?>
</ul>
