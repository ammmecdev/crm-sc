 <!--Inicio modal de añadir modulo -->
 <div class="modal fade" id="mModulo" role="dialog">
 	<div class="modal-dialog">
 		<form  method="post" id="form-modulo" action="index.php?c=puestos&a=GuardarModulo" role="form" enctype="multipart/form-data">
 			<!-- Modal content-->
 			<div class="modal-content">
 				<div class="modal-header bg-success">
 					<button type="button" class="close" data-dismiss="modal">&times;</button>
 					<h4 class="modal-title"><strong> <label id="labTituloModulo"></label></strong></h4>
 				</div>
 				<!-- Cuerpo -->
 				<div class="modal-body">
 					<input name="idModulo" class="field form-control" id="txtIdMod" type="hidden">
 					<div class="form-group">
 						<h5>Nombre del módulo</h5>
 						<div class="input-group">
 							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
 							<input type="text" class="form-control" aria-describedby="basic-addon2" name="modulo" id="txtModulo">
 						</div>
 					</div>  
 					<!-- fin cuerpo --> 
 					<div class="modal-footer" >
 						<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
 							<div class="col-xs-3 col-lg-3" align="left" style="padding-left: 0px">
 								
 							</div>
 							<div class="col-xs-9 col-lg-9" align="right" style="padding-right: 0px">
 								<input type="submit" class="btn btn-success" id="btnGuardar" value="Guardar">
 								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
 							</div> 
 						</div>
 					</div>
 				</div>
 			</div>
 		</form>
 	</div>  
 </div>    
  <!--fin modal de añadir puesto -->