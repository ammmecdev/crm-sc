  <link href="./assets/css/organigrama.css" media="all" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="./assets/js/html2canvas.min.js"></script>
  <script type="text/javascript" src="./assets/js/jspdf.min.js"></script>
  <script src="./assets/js/organigrama.js"></script>

  <div style="padding-top: 55px;">
  	<div class="modal-header btn-primary">
  		<a href="?c=puestos" class="close">&times;</a>
  		<h3 style="margin-bottom: -5px; margin-left: 7px; margin-top: -5px;" >Organigrama empresarial</h3>
  	</div>

  	<div class="row">
  		<div class="col-md-12" align="left">
  			<div class="panel-header" id="divNivel">
  				<a class="btn btn-default" role="button" data-toggle="collapse" href="#collapseNom" aria-expanded="false" aria-controls="collapseNom" style="  width: 100px;">
  					<i class="fas fa-sitemap"></i>
  					Niveles
  				</a>
  				<!-- <button type='button' class='btn btn-primary btn-block org-add-butto' data-toggle='modal' data-target='#mNivel' onclick="nuevoNivel()"><i id='icon-control-add' class='fas fa-plus-circle'></i></button> -->
  			</div><br><br>
  			<div class="collapse" id="collapseNom">
  				<div class="well">
  					<div class="panel-body" id="divNiveles" class="collapse">

  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  	<div id="orgChartContainer">
  		<div id="orgChart"></div>
  	</div>
  </div>
  <!--Inicio modal de añadir puesto -->
  <div class="modal fade" id="mPuesto" role="dialog">
  	<div class="modal-dialog">
  		<form  method="post" id="form-puesto" action="index.php?c=puestos&a=Guardar" role="form" enctype="multipart/form-data">
  			<!-- Modal content-->
  			<div class="modal-content">
  				<div class="modal-header bg-primary">
  					<button type="button" class="close" data-dismiss="modal">&times;</button>
  					<h4 class="modal-title"><strong> <label id="labTitulo"></label></strong></h4>
  				</div>
  				<!-- Cuerpo -->
  				<div class="modal-body">
  					<input class="form-control" name="idNodo" id="txtIdNodo" value="0" type="hidden">
  					<div id="divTipo">
  						<div class="form-group" id="nodoPadreTxt">
  							<h5>Nodo padre</h5>
  							<div class="input-group">
  								<span class="input-group-addon" id="basic-addon2"><i class="material-icons">&#xE86E;</i></span>
  								<input type="text" class="form-control" aria-describedby="basic-addon2" name="padre" id="txtPadre" readonly>
  							</div>
  						</div>
  						<div class="form-group" id="nodoPadreSelect">
  							<h5>Nodo padre</h5>
  							<div class="input-group">
  								<span class="input-group-addon" id="basic-addon2"><i class="material-icons">&#xE86E;</i></span>
  								<select name="padreEdit" aria-describedby="basic-addon2" id="selectPadre" class="form-control">
  								</select>
  							</div>
  						</div>
  						<div class="form-group">
  							<h5>Tipo de nodo</h5>
  							<select name="tipo" id="selectTipo" class="form-control" required>
  							</select>
  						</div>
  					</div>
  					<div id="divModulo">
  						<div class="form-group">
  							<h5>Seleccione el modulo</h5>
  							<div class="input-group">
  								<span class="input-group-addon" id="basic-addon2"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
  								<select name="idModulo2" id="selectIdModulo" class="form-control">
  								</select>
  							</div>
  						</div> 
  					</div>
  					<div id="divPuesto">
  						<input class="form-control" name="idPuesto" id="txtIdPuesto" value="0" type="hidden">
  						<input name="idModulo" class="field form-control" id="txtIdModulo" type="hidden">
  						<div class="form-group">
  							<h5>Nombre del puesto</h5>
  							<div class="input-group">
  								<span class="input-group-addon" id="basic-addon2"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
  								<input type="text" class="form-control" aria-describedby="basic-addon2" name="puesto" id="txtPuesto">
  							</div>
  						</div>  
  						<div class="form-group">
  							<h5>Nivel</h5>
  							<div class="input-group">
  								<span class="input-group-addon"><i class="fas fa-boxes"></i></span>
  								<select name="nivel" id="selectNivel" class="form-control">

  								</select>
  							</div>
  						</div>
  					</div>

  					<input name="parent" class="field form-control" id="txtParent" type="hidden">

  					<!-- fin cuerpo --> 
  					<div class="modal-footer" >
  						<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
  							<div class="col-xs-3 col-lg-3" align="left" style="padding-left: 0px">
  								<button type="button" id="btnPermisos" class="btn btn-primary" data-toggle="modal" data-target="#mPermisos" onclick="permisosPuesto()"><i class="fas fa-lock"></i> Permisos</button>
  							</div>
  							<div class="col-xs-9 col-lg-9" align="right" style="padding-right: 0px">
  								<input type="submit" class="btn btn-success" id="btnGuardar">
  								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
  							</div> 
  						</div>
  					</div>
  				</div>
  			</div>
  		</form>
  	</div>  
  </div>    
  <!--fin modal de añadir puesto -->

  <!--Inicio de Modal permisos usuario-->
  <div class="modal fade" id="mPermisos" role="dialog">
  	<div class="modal-dialog">
  		<form  method="post" action="?c=puestos&a=ActualizarPermisos" id="form_permisos" role="form">
  			<input type="hidden" name="puesto" id="txtPuestoP">
  			<input type="hidden" name="idPuesto" id="txtIdPuestoP">


  			<!-- Modal content-->
  			<div class="modal-content">
  				<div class="modal-header bg-primary">
  					<button type="button" class="close" data-dismiss="modal">&times;</button>
  					<h4 class="modal-title"><strong><label>Permisos del puesto</label></strong></h4>
  				</div>
  				<!-- Cuerpo -->
  				<div class="modal-body">  
  					<div class="well">
  						<fieldset style="margin-top: -10px;">
  							<legend align="center">Información del puesto</legend>
  							<div class="example-content-widget" style="margin-top: -10px; margin-bottom: -10px;" align="center">
  								<h5 id="labPuesto"></h5>
  								<h5 id="labNivel"></h5>
  							</div>	
  						</fieldset>
  					</div>
  					<div class="content-permisos" id="div_permisos">

  					</div>
  				</div>

  				<!-- fin cuerpo --> 
  				<div class="modal-footer">
  					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
  						<div class="col-lg-3" align="left" style="padding-left: 0px">
  							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Desactivar usuario" id="btnEliminar" onclick="desactivarUsuario()"><i class="fas fa-ban"></i></a>
  						</div>
  						<div class="col-lg-9" align="right" style="padding-right: 0px">
  							<input type="submit" class="btn btn-success" value="Guardar" id="btnGuardarP">
  							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
  						</div>
  					</div>
  				</div>
  			</div>
  		</form>
  	</div>  
  </div>    
  <!--fin modal de permisos usuario-->

  <!--Inicio modal de nivel -->
  <div class="modal fade" id="mNivel" role="dialog">
  	<div class="modal-dialog">
  		<form  method="post" id="form-nivel" action="index.php?c=puestos&a=GuardarNivel" role="form" enctype="multipart/form-data">
  			<!-- Modal content-->
  			<div class="modal-content">
  				<div class="modal-header bg-primary">
  					<button type="button" class="close" data-dismiss="modal">&times;</button>
  					<h4 class="modal-title"><strong><label id="labNivel"></label></strong></h4>
  				</div>
  				<!-- Cuerpo -->
  				<div class="modal-body">
  					<input class="form-control" name="idNivel" id="txtIdNodo" value="0" type="hidden">
  					<div class="form-group">
  						<h5>Nombre del nivel</h5>
  						<div class="input-group">
  							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
  							<input type="text" class="form-control" aria-describedby="basic-addon2" name="nombreNivel" id="txtNivel">
  						</div>
  					</div>  
  					<div class="form-group">
  						<h5>Color de nodo</h5>
  						<div id="cp2" class="input-group colorpicker-component">
  							<span class="input-group-addon"><i></i></span>
  							<input  id="cp4" type="text" value="#fff" class="form-control" />
  						</div>
  					</div><!--/form-group-->
  					<div class="form-group">
  						<div class="example">
  							<div class="example-title">Nodo ejemplo</div>

  							<div class="example-content well">
  								<div class="example-content-widget" align="center">
  									<div class='node' id="nodoPrueba" >Nodo ejemplo</div>
  								</div>
  							</div>
  						</div>
  					</div>
  					<!-- fin cuerpo --> 
  					<div class="modal-footer" >
  						<div class="col-xs-12 col-lg-12" align="right" style="padding-right: 0px">
  							<input type="submit" class="btn btn-success" value="Guardar">
  							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
  						</div> 
  					</div>
  				</div>
  			</div>
  		</form>
  	</div>  
  </div>    
  <!--fin modal de nivel -->
  <script type="text/javascript">
  	permisosPuesto = function()
  	{
  		var idPuesto = $('#txtIdPuesto').val();
  		var puesto = $('#txtPuesto').val();
  		var nivel = $('#selectNivel option:selected').html();
  		$('#btnGuardarP').prop('disabled', false);
  		$('#btnGuardarP').val('Guardar');
  		$('#labPuesto').html(puesto);
  		$('#labNivel').html(nivel);
  		$('#txtIdPuestoP').val(idPuesto);
  		$('#txtPuestoP').val(puesto);
  		$.ajax({
  			type: 'POST',
  			url: '?c=puestos&a=ObtenerPermisos',
  			data: {'idPuesto':idPuesto},
  			success: function(respuesta){
  				$("#div_permisos").html(respuesta);
  			}
  		});
  	}

  	$(document).ready(function(){

  		$('#form_permisos').submit(function(){
  			$('#btnGuardarP').prop('disabled', true);
  			$('#btnGuardarP').val('Procesando...');
  			$.ajax({
  				type: 'POST',
  				url: $(this).attr('action'),
  				data: $(this).serialize(),
  				success: function(respuesta){
  					$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
  					$('#mPermisos').modal('toggle');
  					$('#mPuesto').modal('toggle');
  					$('#mensajejs').show();
  					$('#mensajejs').delay(3500).hide(600);
  				}
  			});
  			return false;
  		});

  	});

  </script>
  <?php include 'view/puestos/modales/mEliminarPuesto.php'; ?>

