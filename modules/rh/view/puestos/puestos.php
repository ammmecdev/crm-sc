<style>
div.horizontal{
	overflow: auto;
	white-space: nowrap;
}
div.horizontal button{
	display: inline-block;
	text-align: center;
	text-decoration: none;
	margin-left: 0px;
	margin-right: 0px;
}
</style>
<?php 
$permisoOrg = false;
foreach ($permisos as $p):
	if ($p == 'acceder_organigrama') {
		$permisoOrg=true;
	}
endforeach;
?>
<div style="padding-top: 72px;">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4">
					
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4" align="center">
					<h2 style="margin-top: 0px;">Puestos y Módulos</h2>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4"  align="right">
					<?php if ($permisoOrg == true) { ?>
					<div class="btn-group">
						<a href="?c=puestos&a=Organigrama" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Mostrar Organigrama"><i class="fas fa-sitemap"></i> Organigrama </a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->

<!--Inicio de Modal permisos usuario-->
<div class="modal fade" id="mPermisos" role="dialog">
	<div class="modal-dialog">
		<form  method="post" action="?c=puestos&a=ActualizarPermisos" id="form_permisos" role="form">
			<input type="hidden" name="puesto" id="txtPuestoP">
			<input type="hidden" name="idPuesto" id="txtIdPuestoP">


			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label>Permisos del puesto</label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">  
					<div class="well">
						<fieldset style="margin-top: -10px;">
							<legend align="center">Información del puesto</legend>
							<div class="example-content-widget" style="margin-top: -10px; margin-bottom: -10px;" align="center">
								<h5 id="labPuesto"></h5>
								<h5 id="labNivel"></h5>
							</div>	
						</fieldset>
					</div>
					<div class="content-permisos" id="div_permisos">

					</div>
				</div>

				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-lg-3" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Desactivar usuario" id="btnEliminar" onclick="desactivarUsuario()"><i class="fas fa-ban"></i></a>
						</div>
						<div class="col-lg-9" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" value="Guardar" id="btnGuardarP">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de permisos usuario-->

<div class="panel panel-default" style="margin-bottom: 0px;">
	<div class="row">
		<div class="col-xs-12 col-lg-8">
			<div class="table-responsive" id="resultadoBusqueda"></div>
		</div>
		<div class="col-xs-12 col-lg-4">
			<div class="table-responsive" id="resultadoBusquedaModulos">
			</div>
		</div>
	</div>
</div>

<?php include 'view/puestos/modales/mPuesto.php'; ?>
<?php include 'view/puestos/modales/mModulo.php'; ?>
<?php include 'view/puestos/modales/mEliminarPuesto.php'; ?>

<script src="assets/js/puestos.js"></script>


