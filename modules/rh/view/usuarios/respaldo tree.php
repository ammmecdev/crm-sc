<ul class="treeview-black treeview" id="black">
	<?php foreach($this->modelMod->Listar() as $modulo): ?>
		<li class="liPermisos1 expandable lastExpandable">
			<div class="hitarea expandable-hitarea lastExpandable-hitarea"></div>
			<!-- <div class="minimal single-row"> -->
				<input type="checkbox" id="<?php echo $modulo->idElement; ?>" name="<?php echo $modulo->idElement ?>" value="<?php echo "modulo,". $modulo->idModulo?>" class="modulos">&nbsp;
				<label><?php echo $modulo->modulo ?> </label>
				<!-- </div> -->
				<ul style="display: none;">
					<?php foreach ($this->modelMod->ListarSecciones($modulo->idModulo) as $seccion): ?>
						<li class="expandable liPermisos2 lastExpandable">
							<div class="hitarea expandable-hitarea"></div>
							<!-- <div class="minimal single-row"> -->
								<input type="checkbox" id="<?php echo $seccion->idElement; ?>" name="<?php echo $seccion->idElement ?>" value="<?php echo "seccion,". $seccion->idSeccion?>" class="<?php echo $modulo->idElement . ' seccion'; ?>">&nbsp;
								<font><?php echo $seccion->seccion ?></font>
								<!-- </div> -->
								<ul>
									<?php foreach($this->modelMod->ListarFunciones($seccion->idSeccion) as $fun): ?>
										<li class="liPermisos3">
											<!-- <div class="minimal single-row"> -->
												<input type="checkbox" id="<?php echo $fun->idElement; ?>" name="<?php echo $fun->idElement ?>" value="<?php echo "funcion,". $fun->idFuncion?>" class="<?php echo $modulo->idElement . ' ' . $seccion->idElement ?>">&nbsp;
												<font><?php echo $fun->funcion; ?></font>
												<!-- </div> -->
											</li>
										<?php endforeach; ?>
									</ul>
								</li>
							<?php endforeach; ?>
						</ul>
					</li>
				<?php endforeach; ?>
			</ul>