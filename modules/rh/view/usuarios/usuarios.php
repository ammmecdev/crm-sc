<?php
//Comprobamos si esta definida la sesión 'tiempo'.
if(isset($_SESSION['tiempoAdm'])) {
    //Tiempo en segundos para dar vida a la sesión.
    $inactivo = 300;// 60 segundos * 10 = 600 segundos == 10 minutos en este caso.
    //Calculamos tiempo de vida inactivo.
    $vida_session = time() - $_SESSION['tiempoAdm'];
    //Compraración para redirigir página, si la vida de sesión sea mayor a el tiempo insertado en inactivo.
    if($vida_session > $inactivo){
    	unset($_SESSION['acceso']);
    	//echo  "<script type='text/javascript'> alert('Sesión cerrada por inactividad'); </script>";
    	echo  "<script type='text/javascript'> window.location='?c=usuarios&g=true'; </script>";
    	exit();
    }
    else {
    	$_SESSION['tiempoAdm'] = time(); // si no ha caducado la sesion, actualizamos
    }
} else {
    //Activamos sesion tiempo.
	$_SESSION['tiempoAdm'] = time();
}

// print_r($_SESSION['permisos']);
?>
<style type="text/css">
li.liPermisos1{
	font-size: 15px;
}
li.liPermisos2{
	font-size: 15px;
	padding-left: 20px;
}
li.liPermisos3{
	font-size: 13px;
	margin-left: 25px;
}
div.minimal{
	margin-left: 5px;
}
</style>
<?php 
$permisoReg = $permisoExp = $permisoDes = false;
foreach ($permisos as $p):
	switch ($p) {
		case 'registrar_usuarios':
		$permisoReg = true;
		break;
		case 'exportar_usuarios':
		$permisoExp = true;
		break;
		case 'desactivar_usuarios':
		$permisoDes = true;
		break;
		default:
		break;
	}
endforeach;
?>
<!--Encabezado del contenido-->
<div style="padding-top: 72px;">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<?php if ($permisoReg == true) { ?>
						<div class="btn-group">
							<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mUsuario" onclick="nuevoUsuario()"><i class="fas fa-address-card"></i> Registrar usuario </button>
						</div>
					<?php } ?>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<center><h3 style="margin-top: 0px;">Usuarios</h3></center>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
					<form  method="POST" action="?c=usuarios&a=Exportar&g=true" enctype="multipart/form-data" id="form-exportar">
						<div class="btn-group">
							<div class="form-group">
								<input type="text" name="vBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultas();">
							</div> 
						</div>
						<?php if ($permisoExp == true) { ?>
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar usuarios del sistema"><i class="fas fa-download"></i></button>
							</div>
						<?php } ?>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
					<div class="btn-group btn-group-justified" role="group" aria-label="...">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-sm btn-default" id="liActivado" data-toggle="tooltip" title="Activados" data-placement="bottom" onclick="verActivos();">
								<span class="hidden-xs">Activados</span> <i class="fas fa-user-check"></i>
							</button>
						</div>
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-sm btn-default" id="liDesactivado" data-toggle="tooltip" title="Desactivados" data-placement="bottom" onclick="verDesactivados();">
								<span class="hidden-xs">Desactivados</span> <i class="fas fa-user-slash"></i>
							</button>
						</div>
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-sm btn-default" id="liSuspendido" data-toggle="tooltip" title="Suspendidos" data-placement="bottom" onclick="verSuspendido();">
								<span class="hidden-xs">Suspendidos</span> <i class="fas fa-user-clock"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>  
</div>
<!--Fin de Encabezado-->

<div style="overflow-x: auto;" id="tbl" >
	<table class="table table-bordered table-hover" id="tabla-activos" style="margin-bottom: 0px;">
		<!--Aqui se imprime la tabla de usuarios activos --> 
	</table>
	<table class="table table-bordered table-hover" id="tabla-desactivos" style="margin-bottom: 0px;">
		<!--Aqui se imprime la tabla de usuarios desactivados--> 
	</table>
	<table class="table table-bordered table-hover" id="tabla-suspendido" style="margin-bottom: 0px;">
		<!--Aqui se imprime la tabla de usuarios desactivados--> 
	</table>
</div>

<!--Inicio de Modal añadir usuario-->
<div class="modal fade" id="mUsuario" role="dialog">
	<div class="modal-dialog">
		<form  method="post" action="?c=usuarios&a=Guardar&g=true" id="form_usuarios" role="form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo"></label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">  
					<div class="form-group">
						<input type="hidden" name="idUsuario" id="txtIdUsuario">
						<input type="hidden" name="idPuesto" id="txtIdPuesto">
						<input type="hidden" name="nombreUsuario" id="txtNombreUsuario">
					</div>

					<div class="form-group">
						<h5>Nombre de usuario</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-user"></span></span>
							<select class="form-control selectpicker" data-live-search="true" aria-describedby="basic-addon2" name="idEmpleado" id="txtIdEmpleado" onchange="obtenerNombre();">
								<option value="">Seleccione al empleado</option>
							</select>
						</div>
					</div>

					<div class="form-group" id="divModulo">
						<h5>Módulo</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-puzzle-piece"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon2" name="modulo" id="txtModulo" data-bv-excluded="false" readonly>
						</div>
					</div>

					<div class="form-group" id="divTipoUsuario">
						<h5>Tipo de Usuario</h5>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-align-justify"></span></span>
							<input type="text" class="form-control" name="idTipo" id="txtIdTipoUsuario" data-bv-excluded="false" readonly>
						</div>
					</div>

					<div class="form-group" id="divEmail">
						<h5>Correo Electrónico</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-envelope"></span></span>
							<input type="text" class="form-control" aria-describedby="basic-addon2" name="email" id="txtEmail" placeholder="Correo Electrónico" data-bv-excluded="false" readonly>
						</div>
					</div>
					<div class="form-group">
						<h5>Usuario</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-user"></span></span>
							<input type="text" class="form-control" aria-describedby="basic-addon2" name="usuario" id="txtUsuario" placeholder="Ingrese su usuario">
						</div>
					</div>

					<div class="form-group" align="justify">
						<h5>Contraseña temporal</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-time"></span></span>
							<input type="password" class="form-control" aria-describedby="basic-addon2" name="pass" id="txtPass" placeholder="Contraseña temporal">
						</div>
					</div>
				</div>
				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-lg-3" align="left" style="padding-left: 0px">
							<?php if ($permisoDes == true): ?>
								<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Desactivar usuario" id="btnEliminar" onclick="desactivarUsuario()"><i class="fas fa-ban"></i></a>
							<?php endif ?>
						</div>
						<div class="col-lg-9" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" value="Guardar" id="btnGuardar">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de añadir usuario-->

<!--Inicio de Modal permisos usuario-->
<div class="modal fade" id="mPermisos" role="dialog">
	<div class="modal-dialog">
		<form  method="post" action="?c=usuarios&a=ActualizarPermisos&g=true" id="form_permisos" role="form">
			<input type="hidden" name="idUsuario" id="txtIdUsuarioP">
			<input type="hidden" name="nombreUsuario" id="txtNombreUsuarioP">
			<input type="hidden" name="puesto" id="txtPuestoP">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-primary">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label>Permisos de usuario</label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">  
					<div class="well">
						<fieldset style="margin-top: -10px;">
							<legend align="center">Información del usuario</legend>
							<div class="example-content-widget" style="margin-top: -10px; margin-bottom: -10px;" align="center">
								<h5 id="labUsuario"></h5>
								<h5 id="labPuesto"></h5>
								<h5 id="labNivel"></h5>
							</div>	
						</fieldset>
					</div>
					<div class="content-permisos" id="div_permisos">
					</div>
				</div>

				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-lg-3" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Desactivar usuario" id="btnEliminar" onclick="desactivarUsuario()"><i class="fas fa-ban"></i></a>
						</div>
						<div class="col-lg-9" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" value="Guardar" id="btnGuardarP">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de permisos usuario-->

<!--Inicio modal de eliminar usuario -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  id="form-desactivar" method="post" action="?c=usuarios&a=Desactivar&g=true" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Desactivar usuario</strong></h4>
				</div>
				<div class="modal-body" style="margin-bottom: -30px;">  
					<!-- Cuerpo --> 
					<p style="font-size: 19px;">¿Está seguro que desea desactivar el usuario?</p>
					<input type="text" id="txtIdUsuarioE" name="idUsuario" hidden>
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Desactivar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     


<script src="assets/js/usuarios.js"></script>

<script type="text/javascript">
	permisosUsuario = function(idUsuario, nombreUsuario, puesto, nivel, datos)
	{
		$('#btnGuardarP').prop('disabled', false);
		$('#btnGuardarP').val('Guardar');
		$('#txtIdUsuarioP').val(idUsuario);
		$('#labUsuario').html(nombreUsuario);
		$('#labPuesto').html(puesto);
		$('#labNivel').html(nivel);
		$('#txtNombreUsuarioP').val(nombreUsuario);
		$('#txtPuestoP').val(puesto);
		$.ajax({
			type: 'POST',
			url: '?c=usuarios&a=ObtenerPermisos&g=true',
			data: {'idUsuario':idUsuario},
			success: function(respuesta){
				$("#div_permisos").html(respuesta);
			}
		});
	}

	$('#form_permisos').submit(function(){
		$('#btnGuardarP').prop('disabled', true);
		$('#btnGuardarP').val('Procesando ...');
		var idUsuario = $("#txtIdUsuarioP").val();
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(respuesta){
				$("#mensajejs").html('<div class="alert alert-success alert-bottom alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
				$('#mPermisos').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
			}
		});
		socket.emit( 'actualiza_permisos', { 
			idUsuario : idUsuario
		});
		return false;
	});

</script>
