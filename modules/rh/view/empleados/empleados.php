<style>
	div.horizontal{
		overflow: auto;
		white-space: nowrap;
	}
	div.horizontal button{
		display: inline-block;
		text-align: center;
		text-decoration: none;
		margin-left: 0px;
		margin-right: 0px;
	}
	.linkblack {
		color: #1A5276;
	}
</style>
<?php 
$permisoReg = $permisoExp = false;
foreach ($permisos as $p):
	switch ($p) {
		case 'registrar_empleado':
		$permisoReg=true;
		break;
		case 'exportar_empleado':
		$permisoExp=true;
		break;
		default:
		break;
	}
endforeach;
?>
<div style="padding-top: 72px;">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<?php if ($permisoReg == true) { ?>
						<div class="btn-group">
							<a href="?c=empleados&a=Crud" class="btn btn-success btn-sm"><i class="fas fa-address-card"></i> Registrar empleado </a>
						</div>
						<?php } ?>
						<br><br>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="center">
						<div class="btn-group">

							<h5><strong id="contador"></strong></h5>

						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=empleados&a=Exportar">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="bAlfabetica" class="form-control" id="txtBAlfabetica">
									<input type="hidden" name="bStatus" class="form-control" id="txtBStatus">
									<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultas();">
								</div> 
							</div>
							<?php if ($permisoExp == true) { ?>
								<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
									<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
								</div>
								<?php } ?>
							</form>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-lg-9" style="margin-bottom: 5px;">
							<div class="horizontal" role="group" aria-label="...">
								<button type="button" class="btn btn-sm btn-primary" onclick="filtrarPorLetra('')" id="btn-todas">Todas</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('A')" id="btn-a">A</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('B')" id="btn-b">B</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('C')" id="btn-c">C</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('D')" id="btn-d">D</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('E')" id="btn-e">E</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('F')" id="btn-f">F</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('G')" id="btn-g">G</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('H')" id="btn-h">H</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('I')" id="btn-i">I</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('J')" id="btn-j">J</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('K')" id="btn-k">K</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('L')" id="btn-l">L</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('M')" id="btn-m">M</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('N')" id="btn-n">N</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('Ñ')" id="btn-ñ">Ñ</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('O')" id="btn-o">O</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('P')" id="btn-p">P</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('Q')" id="btn-q">Q</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('R')" id="btn-r">R</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('S')" id="btn-s">S</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('T')" id="btn-t">T</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('U')" id="btn-u">U</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('V')" id="btn-v">V</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('W')" id="btn-w">W</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('X')" id="btn-x">X</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('Y')" id="btn-y">Y</button>
								<button type="button" class="btn btn-sm btn-default" onclick="filtrarPorLetra('Z')" id="btn-z">Z</button>
							</div>
						</div>
						<div class="col-xs-12 col-lg-3" align="right">
							<div class="btn-group" role="group" aria-label="Mostrar usuarios">
								<button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Empleados Activos" onclick="filtrarPorStatus('Activo')" id="btn-activo"><i class="fas fa-user-check"></i></button>
								<button type="button" class="btn btn-sm btn-default" data-toggle="tooltip" title="Empleados Inactivos" onclick="filtrarPorStatus('Inactivo')" id="btn-inactivo"><i class="fas fa-user-slash"></i></button>
							</div>
						</div>
					</div>    
				</div>
			</div>  
		</div>
		<!--Fin de Encabezado-->

		<!--Inicio del Contenedor-->
		<div style="overflow-x:auto;" id="tbl">
			<div> 

				<div id="resultadoBusqueda">
					<!--Aqui se vera a tabla de resultados-->
				</div> 
			</div>
		</div>

		<!--Inicio modal de confirmar para dar de baja al empleado -->
		<div class="modal fade" id="mBajaEmpleado" role="dialog">   
			<div class="modal-dialog">
				<form  method="post" action="index.php?c=empleados&a=Baja" id="form-baja">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header" style="background-color: #FCF3CF;">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title"><strong>Desactivar empleado</strong></h4>
						</div>
						<div class="modal-body">  
							<!-- Cuerpo --> 
							<p style="font-size: 18px; text-align: left;">¿Está seguro que desea dar de baja al empleado?</p>
							<input type="hidden" class="form-control" name="idEmpleado" id="txtIdEmpleado">
							<!-- fin cuerpo --> 
						</div>
						<div class="modal-footer">
							<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
								<input type="submit" class="btn btn-danger" value="Aceptar">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>     
		<!--fin modal de confirmar para dar de baja el empleado -->

		<!--Inicio modal de confirmar para dar de baja al empleado -->
		<div class="modal fade" id="mActEmpleado" role="dialog">   
			<div class="modal-dialog">
				<form  method="post" action="index.php?c=empleados&a=Reactivar" id="form-reactivar">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header" style="background-color: #FCF3CF;">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title"><strong>Activar empleado</strong></h4>
						</div>
						<div class="modal-body">  
							<!-- Cuerpo --> 
							<input type="hidden" class="form-control" name="idEmpleado" id="txtIdEmpleados">

							<div class="form-group">
								<h5><strong>Fecha de inicio:</strong></h5>
								<input name="fechaInicio" id="txtFechaInicio" type="date" class="form-control" required="">
							</div>

							<div class="form-group">
								<h5><strong>Puesto:</strong></h5>
								<div class="input-group">
									<span class="input-group-addon"><i class="fas fa-bars"></i></span>
									<select name="idPuesto" id="selectPuestoA" class="form-control" required>
										<option value="">Asignar puesto</option>
									</select>
								</div>
							</div>

							<p class="help-block" style="font-size: 13px; text-align: justify;">Para reactivar el empleado es necesario establecer una nueva fecha de inicio y asignar un nuevo puesto al empleado.</p>
							<!-- fin cuerpo --> 
						</div>
						<div class="modal-footer">
							<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
								<input type="submit" class="btn btn-success" value="Aceptar">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>     
		<!--fin modal de confirmar para dar de baja el empleado -->

		<script src="assets/js/empleados.js"></script>