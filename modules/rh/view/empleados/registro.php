<div style="padding-top: 72px;">
	<div class="container">
		<nav class="navbar bg-success" style="margin-bottom: 0px;">
			<div class="container-fluid">
				<div class="navbar-header">
					<h3><strong><?php echo $empleado->idEmpleado != null ? $empleado->nombre : 'Registrar empleado'; ?></strong></h3>
				</div>
			</div>
		</nav>
		<div class="container-fluid" style="background-color: white;"> 
			<div class="row">
				<div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
					<br>
					<form action="?c=empleados&a=Guardar" method="post" class="form-horizontal" id="form-empleados">
						<fieldset>

							<!--Titulo-->
							<div class="col-xs-12 col-lg-12">
								<legend>Datos Generales</legend>
								<p style="font-family: Helvetica;"></p>
							</div>
							<!--Titulo-->

							<!--Row-->
							<input type="hidden" class="form-control" aria-describedby="basic-addon2" name="idEmpleado" id="txtIdEmpleado" required value="<?php echo $empleado->idEmpleado != null ? $empleado->idEmpleado : 0 ?>" >
							<!--Row-->

							<!--Primer Columna-->
							<div class="col-xs-12 col-lg-6">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="nombreEmpleado">Nombre Completo</label>
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon2">
												<i class="fas fa-user" style="font-size: 16px;"></i>
											</span>
											<input autofocus type="text" class="form-control double" placeholder="Nombre completo del empleado" aria-describedby="basic-addon2" name="nombre" id="txtNomEmpleado" value="<?php echo $empleado->idEmpleado != null ? $empleado->nombre : '' ?>"  <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?> style="cursor:pointer;">
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Ubicación</label>
										<div class="input-group">
											<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-location-on" style="font-size: 17px;"></i></span>
											<input name="ubicacion" id="autocomplete" placeholder="Domicilio" onFocus="geolocate()" type="text" class="form-control double"  value="<?php echo $empleado->idEmpleado != null ? $empleado->ubicacion : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>          
								</div>
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Domicilio</label>
										<div class="row">
											<div class="col-xs-5 col-md-4" style="margin-bottom: 5px;">
												<div class="input-group">
													<span class="input-group-addon"><i class="fas fa-hashtag"></i></span>
													<input name="numero" class="field form-control double" id="street_number" placeholder="Número" value="<?php echo $empleado->idEmpleado != null ? $empleado->numero : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
												</div>
											</div>
											<div class="col-xs-12 col-md-8">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon2"><i class="fas fa-road"></i></span>
													<input name="calle" class="field form-control double" id="route" placeholder="Calle" value="<?php echo $empleado->idEmpleado != null ? $empleado->calle : '' ?>"  <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
												</div>
											</div>
										</div>
									</div>         
								</div>
								<div class="col-xs-12 col-lg-5">
									<div class="form-group">
										<label for="">Ciudad</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="mdi mdi-location-city mdi-lg"></i></span>
											<input name="ciudad" class="field form-control double" id="locality" placeholder="Ciudad" value="<?php echo $empleado->idEmpleado != null ? $empleado->ciudad : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>              
								</div>
								<div class="col-lg-1"></div>
								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label for="">Estado</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-map-signs"></i></span>
											<input name="estado" class="field form-control double" id="administrative_area_level_1" placeholder="Estado" value="<?php echo $empleado->idEmpleado != null ? $empleado->estado : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-lg-5">
									<div class="form-group">
										<label for="">Código postal</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-home"></i></span>
											<input name="cp" class="field form-control double" id="postal_code" placeholder="Código Postal" value="<?php echo $empleado->idEmpleado != null ? $empleado->cp : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
								<div class="col-lg-1"></div>
								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label for="">País</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-globe"></i></span>
											<input name="pais" class="field form-control double" id="country" placeholder="Pais" value="<?php echo $empleado->idEmpleado != null ? $empleado->pais : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
							</div>
							<!--Primer Columna-->

							<!--Segunda Columna-->
							<div class="col-xs-12 col-lg-6">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Correo Electrónico</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="mdi mdi-mail-outline mdi-lg"></i></span>
											<input name="email" class="field form-control double" id="txtEmail" placeholder="Correo Electrónico" value="<?php echo $empleado->idEmpleado != null ? $empleado->email : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
										<div id="alertaEmail"></div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-lg-3">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Teléfono</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-phone"></i></span>
											<input  type="tel" name="telefono" class="field form-control double" id="txtTelefono" placeholder="Teléfono" value="<?php echo $empleado->idEmpleado != null ? $empleado->telefono : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-lg-3">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Fecha de Inicio</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
											<input name="fechaInicio" id="txtFechaInicio" type="date" class="form-control double" value="<?php echo $empleado->idEmpleado != null ? $empleado->fechaInicio : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-lg-6">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Clave Única de Registro de Población (CURP)</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="far fa-id-card"></i></span>
											<input name="curp" class="field form-control double" id="txtCurp" placeholder="CURP" oninput="mayus(this)" value="<?php echo $empleado->idEmpleado != null ? $empleado->curp : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-lg-6">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Registro Federal de Contribuyentes (RFC)</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-address-card"></i></span>
											<input name="rfc" class="field form-control double" id="txtRfc" placeholder="RFC" oninput="mayus(this)" value="<?php echo $empleado->idEmpleado != null ? $empleado->rfc : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-lg-6">
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Número de Seguro Social (NSS)</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="far fa-window-maximize"></i></span>
											<input name="nss" class="field form-control double" id="txtNss" placeholder="NSS" value="<?php echo $empleado->idEmpleado != null ? $empleado->nss : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
							</div>
							<!--Segunda Columna-->
						</fieldset>

						<!--Segunda Parte-->
						<fieldset>
							<!--Encabezado Segunda Parte-->
							<div class="col-xs-12 col-lg-12">
								<legend>Datos Empresariales</legend>
							</div>
							<!--Encabezado Segunda Parte-->

							<!--Primer Columna-->
							<div class="col-xs-12 col-lg-6">
								<div class="col-xs-7 col-lg-5">
									<div class="form-group">
										<label for="">No. de Empleado</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-sort-numeric-down"></i></span>
											<input name="noEmpleado" id="txtNoEmpleado" type="text" class="form-control double" placeholder="No. de empleado"value="<?php echo $empleado->idEmpleado != null ? $empleado->noEmpleado : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
										<div id="alerta"></div>
									</div>
								</div>
								<div class="col-lg-1"></div>
								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label for="">Sueldo base</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-dollar-sign"></i></span>
											<input name="sueldo" id="txtSueldo" type="text" class="form-control double" placeholder="Sueldo base" data-toggle="tooltip" title="Formato 000.00 / 000" value="<?php echo $empleado->idEmpleado != null ? $empleado->sueldo : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-lg-12">
									<div class="form-group">
										<label for="">Puesto</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fas fa-bars"></i></span>
											<select name="idPuesto" id="selectPuesto" class="form-control double">
												<?php if($empleado->idEmpleado==null){ ?>
													<option value="">Asignar puesto</option>
													<?php } if($empleado->idEmpleado!=null){ ?>
														<option value="<?php echo $empleado->idPuesto?>">
															<?php echo $empleado->puesto; ?>
														</option>
														<?php } foreach ($this->modelPuesto->Listar() as $puesto):
														if($puesto->idPuesto!=$empleado->idPuesto){ ?>
															<option value="<?php echo $puesto->idPuesto; ?>">
																<?php echo $puesto->puesto; ?>
															</option>
															<?php } endforeach; ?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<!--Primer Columna-->

										<!--Segunda Columna-->
										<div class="col-xs-12 col-lg-6">
											<div class="col-xs-12 col-lg-5">
												<div class="form-group">
													<label for="">Factor (8 horas)</label>
													<div class="input-group">
														<span class="input-group-addon"><i class="far fa-clock"></i></span>
														<input name="fsrOcho" id="fsrOcho" type="text" class="form-control double" placeholder="8 horas" data-toggle="tooltip" title="Formato 0.00" data-placement="top" value="<?php echo $empleado->idEmpleado != null ? $empleado->fsrOcho : '' ?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>                    
													</div>                  
												</div>
											</div>
											<div class="col-lg-2"></div>
											<div class="col-xs-12 col-lg-5">
												<div class="form-group">
													<label for="">Factor (12 horas)</label>
													<div class="input-group">
														<span class="input-group-addon"><i class="fas fa-clock"></i></span>
														<input name="fsrDoce" type="text" id="fsrDoce" class="form-control double" placeholder="12 horas" data-toggle="tooltip" title="Formato 0.00" data-placement="top" value="<?php echo $empleado->idEmpleado != null ? $empleado->fsrDoce : ''?>" <?php if ($empleado->idEmpleado != null) echo 'readonly'; ?>>                    
													</div>                  
												</div>
											</div>
										</div>
										<!--Segunda Columna-->
									</fieldset>
									<!--Segunda Parte-->

									<!--Pie de Formulario-->
									<div class="col-xs-12 col-lg-12" align="right" style="margin-bottom: 5px;">
										<hr style="margin-top: 3px; margin-bottom: 15px;">
										<a href="?c=empleados" type="button" class="btn btn-sm btn-default">Cancelar</a>
										<button type="submit" class="btn btn-sm btn-success" id="btnGuardar">Guardar</button>
									</div>
									<!--Pie de Formulario-->
								</form>
							</div>
						</div>
					</div>
				</div> 
			</div>

			<!--Inicio modal de usuario registrado-->
			<div class="modal fade" id="mRegistrado" role="dialog">   
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header bg-success">
							<p style="font-size: 18px; margin-bottom: -4px"><i class="fas fa-info-circle"></i> <strong> Aviso </strong></p>
						</div>
						<div class="modal-body" style="margin-bottom: -30px;">  
							<p style="font-size: 20px;" id="mensaje"></p>
							<!-- fin cuerpo --> 
						</div>
						<div class="modal-footer">
							<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
								<?php if(!isset($_GET['idEmpleado'])){ ?><a href="?c=empleados&a=Crud" type="button" class="btn btn-default">Registrar otro empleado</a><?php } ?>    
								<a href="?c=empleados" type="button" class="btn btn-success">Terminar</a>    
							</div>
							<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
						</div>
					</div>
				</div>
			</div>     
			<!--fin modal de usuario registrado-->

			<script src="./assets/js/empleados.js"></script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfZw3DDsyLMfVWyhKt9JojWcay2bVd1GA&libraries=places&callback=initAutocomplete"
			async defer></script>
