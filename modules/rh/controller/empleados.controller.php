<?php 
require_once 'model/empleado.php';
require_once 'model/puesto.php';

class EmpleadosController{

	private $pdo;
	private $mensaje;
	private $error;
	private $tabla;

	public function __CONSTRUCT()
	{
		date_default_timezone_set("America/Mexico_City");
		$this->model = new Empleado();
		$this->modelPuesto = new Puesto();
	}

	public function Index()
	{
		$empleados=true;
		$page="view/empleados/empleados.php";
		require_once '../../view/index.php';
	}

	public function Prueba()
	{
		echo $_POST['prueba'];
	}

	public function Antiguedad($fecha)
	{
		if (!empty($fecha)) {
    		$fecha= strtotime($fecha); // convierte la fecha de formato mm/dd/yyyy a marca de tiempo
    		$dia=date("d",$fecha); // día del mes en número
    		$mes=date("m",$fecha); // número del mes de 01 a 12
    		$ano=date("Y",$fecha);

    		$diaactual=date("d",time());
    		$mesactual=date("m",time());
    		$anoactual=date("Y",time());

    		$fecha1=mktime(0,0,0,$mesactual,$diaactual,$anoactual);
    		$fecha2=mktime(0,0,0,$mes,$dia,$ano);

    		$diferencia=$fecha1-$fecha2;
    		$dias=((( $diferencia / 60 ) / 60 ) / 24);
    		$dias=floor($dias);
    		$formato = $this->obtenerFormato($dias);
    		return $formato;
    	}else{
    		return "Sin fecha de inicio";
    	}
    }
    
    public function obtenerFormato($dias)
    {
    	$etqdia = $etqmes = $etqano = '';
    	$anocal = floor($dias / 365.2);
    	$mescal = floor(($dias - floor($dias / 365.2) * 365.2) / 30.42);
    	$diacal = floor($dias - (floor($dias / 365.2) * 365.2 + floor(($dias - floor($dias / 365.2) * 365.2) / 30.42) * 30.42));

    	if ($diacal == 1)
    		$etqdia = "día";
    	else
    		$etqdia = "días";
    	if ($mescal == 1)
    		$etqmes = "mes";
    	else
    		$etqmes = "meses";
    	if ($anocal == 1)
    		$etqano = "año";
    	else
    		$etqano = "años";
    	return $anocal." ".$etqano.", ".$mescal." ".$etqmes." y ".$diacal." ".$etqdia;
    }

    public function Consultas()
    {
    	
    	header('Content-Type: application/json');
    	$bTexto = $_REQUEST['bTexto'];
    	$bAlfabetica = $_REQUEST['bAlfabetica'];
    	$bStatus = $_REQUEST['bStatus'];
    	$permisos = $_SESSION['permisos'];
    	$baccion = '';
    	if ($bStatus == '')
    		$bStatus = 'Activo';
    	if($bAlfabetica == '')
    		$stm=$this->model->ListarT($bTexto,$bStatus);
    	else
    		$stm=$this->model->ListarTA($bTexto, $bAlfabetica, $bStatus);
    	if ($bStatus == 'Activo')
    		$baccion = 'activo';
    	else
    		$baccion = 'Activo';

    	$permisoEdi = $permisoBaj = false;
    	foreach ($permisos as $p):
    		switch ($p) {
    			case 'editar_empleado':
    			$permisoEdi=true;
    			break;
    			case 'baja_empleado':
    			$permisoBaj=true;
    			break;
    			default:
    			break;
    		}
    	endforeach;
    	$resultado=$stm->fetchAll(PDO::FETCH_OBJ);
    	$this->CrearTabla($resultado, $baccion, $permisoEdi, $permisoBaj);
    	$array=array();
    	$row_array['contador']=$stm->rowCount();
    	$row_array['tabla']=$this->tabla;
    	array_push($array, $row_array);
    	echo json_encode($array, JSON_FORCE_OBJECT);
    }

    public function CrearTabla($resultado, $baccion, $permisoEdi, $permisoBaj)
    {
    	$idEmpleadoSes = $_SESSION['idEmpleado'];
    	$t = '';
    	$t = $t . '<table class="table table-bordered table-hover" style="margin-bottom: 0px;">
    	<thead>
    	<tr style="background-color: #6c757dc9; color:white">';
    	($permisoBaj == true) ? $t=$t.'<td></td>' : $t=$t.'';
    	$t=$t.'<td><strong>Nombre</strong></td>
    	<td><strong>Teléfono</strong></td>
    	<td><strong>Email</strong></td>
    	<td><strong>Curp</strong></td>
    	<td><strong>RFC</strong></td>
    	<td><strong>NSS</strong></td>
    	<td><strong>Puesto</strong></td>
    	<td style="white-space: nowrap;"><strong>Sueldo 8 horas</strong></td>
    	<td style="white-space: nowrap;"><strong>Sueldo 12 horas</strong></td>
    	<td style="white-space: nowrap;"><strong>Fecha de inicio</strong></td>
    	<td><strong>Antigüedad</strong></td>';
    	($baccion == 'activo' && $permisoEdi == true) ? $t=$t.'<td><strong>Editar</strong></td>' : $t=$t.'';
    	$t=$t.'</tr>
    	</thead>';
    	if($resultado==null){
    		$t=$t.'<tr><td class="alert-danger" colspan="13" align="center"> <strong>No se encontraron coincidencias en la búsqueda </strong></td></tr>';
    	}
    	foreach ($resultado as $empleado):
    		$antiguedad = $this->Antiguedad($empleado->fechaInicio);
    		$t=$t. '<tr>';
    		if ($permisoBaj == true) {
    			$t=$t.'<td align="center"><a href="#" data-toggle="modal" data-toggle="tooltip"';
    			if ($baccion == 'activo') {
    				$t=$t.'data-target="#mBajaEmpleado" onclick="bajaEmpleado('.$empleado->idEmpleado.');" title="Desactivar empleado"';
    				if ($empleado->idEmpleado == $idEmpleadoSes) {
    					$t=$t.'class="btn btn-primary btn-xs" disabled><i class="fas fa-user"></i>';
    				}else{
    					$t=$t.'class="btn btn-danger btn-xs"><i class="fas fa-ban"></i>';
    				}
    			}else{
    				$t=$t.'class="btn btn-success btn-xs" data-target="#mActEmpleado" onclick="activarEmpleado('.$empleado->idEmpleado.');" title="Activar empleado"><i class="fas fa-arrow-up"></i>';
    			}

    			$t=$t.'</a></td>';
    		}
    		$t=$t.'<td style="white-space: nowrap;">';
    		($baccion == 'activo' && $permisoBaj == true) ? $t=$t.'<a class="linkblack" href="?c=empleados&a=Crud&idEmpleado='. $empleado->idEmpleado .'" data-toggle="tooltip" title="Presione sobre el nombre para editar el empleado">' . $empleado->nombre . '</a>' :  $t=$t.''. $empleado->nombre . '';
    		$t=$t.'</td>
    		<td>' . $empleado->telefono .'</td>
    		<td>' . $empleado->email . '</td>
    		<td>' . $empleado->curp . '</td>
    		<td>' . $empleado->rfc . '</td>
    		<td>' . $empleado->nss . '</td>';
    		$puesto = $this->ObtenerPuesto($empleado->idPuesto);
    		$t=$t.'<td style="white-space: nowrap;">'.$puesto.'</td>
    		<td>
    		<table class="table-condensed table-hover" style="margin-bottom: 0px;">
    		<thead>
    		<tr>
    		<td><strong>$</strong></td>
    		<td></td>
    		<td align="right">'.number_format($empleado->sueldoOcho, 2, '.', ',').'</td>
    		</tr>
    		</thead>
    		</table>
    		</td>
    		<td>
    		<table class="table-condensed table-hover" style="margin-bottom: 0px;">
    		<thead>
    		<tr>
    		<td><strong>$</strong></td>
    		<td></td>
    		<td align="right">'.number_format($empleado->sueldoDoce, 2, '.', ',').'</td>
    		</tr>
    		</thead>
    		</table>
    		</td>
    		<td style="white-space: nowrap;" align="center">' . $empleado->fechaInicio . '</td>
    		<td style="white-space: nowrap;">' . $antiguedad . '</td>'; 
    		($baccion == 'activo' && $permisoEdi == true) ? $t=$t.'<td style="white-space: nowrap;" align="center"><a class="btn btn-primary btn-xs" href="?c=empleados&a=Crud&idEmpleado='. $empleado->idEmpleado .'" data-toggle="tooltip" title="Editar empleado"><span class="glyphicon glyphicon-pencil"></span></a></td>' : $t=$t.'';
    		$t=$t.'</tr>';
    	endforeach;
    	$t = $t . '</table>';
    	$this->tabla=$t;
    }

    public function Guardar()
    {
    	try {
    		$empleado = new Empleado();
    		$empleado->idEmpleado = $_REQUEST['idEmpleado'];
    		$empleado->nombre = $_REQUEST['nombre'];
    		$empleado->ubicacion = $_REQUEST['ubicacion'];
    		$empleado->calle = $_REQUEST['calle'];
    		$empleado->numero = $_REQUEST['numero'];
    		$empleado->ciudad = $_REQUEST['ciudad'];
    		$empleado->estado = $_REQUEST['estado'];
    		$empleado->cp = $_REQUEST['cp'];
    		$empleado->pais = $_REQUEST['pais'];
    		$empleado->telefono = $_REQUEST['telefono'];
    		$empleado->email = $_REQUEST['email'];
    		$empleado->curp = $_REQUEST['curp'];
    		$empleado->rfc = $_REQUEST['rfc'];
    		$empleado->nss = $_REQUEST['nss'];
    		$empleado->noEmpleado = $_REQUEST['noEmpleado'];
    		$empleado->idPuesto = $_REQUEST['idPuesto'];
    		$empleado->sueldo = $_REQUEST['sueldo'];
    		$empleado->fsrOcho = $_REQUEST['fsrOcho'];
    		$empleado->fsrDoce = $_REQUEST['fsrDoce'];
    		$empleado->antiguedad = "1 día";
    		$empleado->fechaInicio = $_REQUEST['fechaInicio'];
    		$empleado->sueldoOcho = $empleado->sueldo * $empleado->fsrOcho;
    		$empleado->sueldoDoce = $empleado->sueldo * $empleado->fsrDoce;
    		if($empleado->idEmpleado > 0){
    			if ($empleado->idEmpleado == $_SESSION['idEmpleado'])
    				$_SESSION['email'] = $empleado->email;
    			$this->model->Actualizar($empleado);
    			echo 'Se han actualizado correctamente los datos del empleado';
    		}else{
    			$this->model->Registrar($empleado);
    			echo 'Se han registrado correctamente los datos del empleado';
    		}
    	} catch (Exception $e) {
    		die($e->getMessage());
    	}
    }

    public function Crud()
    {
    	$empleado = new Empleado();
    	if (isset($_REQUEST['idEmpleado']))
    	{
    		$empleado->idEmpleado = $_REQUEST['idEmpleado'];
    		$empleado=$this->model->Obtener($empleado->idEmpleado);
    	}
    	$empleados=true;
    	$page="view/empleados/registro.php";
    	require_once '../../view/index.php';
    }

    public function Baja()
    {
    	try {
    		$idEmpleado = $_REQUEST['idEmpleado'];
    		$this->model->Baja($idEmpleado);
    		$this->model->BajaUsuario($idEmpleado);
    		echo 'true';
    	} catch (Exception $e) {
    		echo 'false';
    	}
    }

    public function Reactivar()
    {
    	try {
    		$idEmpleado = $_REQUEST['idEmpleado'];
    		$fechaInicio = $_REQUEST['fechaInicio'];
    		$idPuesto = $_REQUEST['idPuesto'];
    		$this->model->Reactivar($fechaInicio, $idEmpleado, $idPuesto);
    		echo 'true';
    	} catch (Exception $e) {
    		echo 'false';
    	}
    }

    public function ObtenerPuesto($idPuesto)
    {
    	$nombre = $this->model->ObtenerPuesto($idPuesto);
    	return $nombre;
    }

    public function ObtenerDatosEmpleado()
    {
    	header('Content-Type: application/json');
    	$idEmpleado = $_POST['idEmpleado'];
    	$datos = array();
    	$res = $this->model->ObtenerDatosEmpleado($idEmpleado);
    	$row_array['modulo']  = $res->modulo;
    	$row_array['idModulo']  = $res->idModulo;   
    	array_push($datos, $row_array);
    	echo json_encode($datos, JSON_FORCE_OBJECT);    
    }

    public function verificarNoEmpleado()
    {
    	$idEmpleado = $_POST['idEmpleado'];
    	$NoEmpleado = $_POST['NoEmpleado'];
    	$verificar=$this->model->verificarNoEmpleado($NoEmpleado,$idEmpleado);
    	if ($verificar == "")
    		echo "0";
    	else
    		echo "1";
    }

    public function verificarEmail()
    {
    	$idEmpleado = $_POST['idEmpleado'];
    	$email = $_POST['email'];
    	$verificar=$this->model->verificarEmail($email,$idEmpleado);
    	if ($verificar == "")
    		echo "0";
    	else
    		echo "1";
    }

    public function consultaPuestos()
    {
    	header('Content-Type: application/json');
    	$datos = array();
    	$query = $this->modelPuesto->Listar();
    	foreach ($query as $p):
    		$row_array['idPuesto']  = $p->idPuesto;
    		$row_array['puesto']  = $p->puesto;
    		array_push($datos, $row_array);
    	endforeach;     
    	echo json_encode($datos, JSON_FORCE_OBJECT);
    }

    public function Exportar(){
    	$bTexto = $_REQUEST['valorBusqueda'];
    	$bAlfabetica = $_REQUEST['bAlfabetica'];
    	$bStatus = $_REQUEST['bStatus'];
    	if ($bStatus == '')
    		$bStatus = 'Activo';

    	if($bAlfabetica == '')
    		$stm=$this->model->ListarT($bTexto,$bStatus);
    	else
    		$stm=$this->model->ListarTA($bTexto, $bAlfabetica, $bStatus);

    	$resultado = $stm->fetchAll(PDO::FETCH_OBJ);

    	require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
            //Logotipo
    	$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

            //Objeto de PHPExcel
    	$objPHPExcel  = new PHPExcel();

            //Propiedades de Documento
    	$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Equipos");

            //Establecemos la pestaña activa y nombre a la pestaña
    	$objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->setTitle("Equipos");
    	$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    	$objDrawing->setName('Logotipo');
    	$objDrawing->setDescription('Logotipo');
    	$objDrawing->setImageResource($gdImage);
    	$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
    	$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    	$objDrawing->setHeight(100);
    	$objDrawing->setCoordinates('H1');
    	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

    	$estilo = array(
    		'font' => array(
    			'name'      => 'Calibri',
    			'bold'      => true,
    			'italic'    => false,
    			'strike'    => false,
    			'size' =>11
    		),
    		'fill' => array(
    			'type'  => PHPExcel_Style_Fill::FILL_SOLID
    		),
    		'borders' => array(
    			'allborders' => array(
    				'style' => PHPExcel_Style_Border::BORDER_NONE
    			)
    		),
    		'alignment' => array(
    			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
    		)
    	);

    	$estiloTituloReporte = array(
    		'font' => array(
    			'name'      => 'Calibri',
    			'bold'      => true,
    			'italic'    => false,
    			'strike'    => false,
    			'size' =>20,
    			'color' => array(
    				'rgb' => 'E31B23'
    			)
    		),
    		'fill' => array(
    			'type'  => PHPExcel_Style_Fill::FILL_SOLID
    		),
    		'borders' => array(
    			'allborders' => array(
    				'style' => PHPExcel_Style_Border::BORDER_NONE
    			)
    		),
    		'alignment' => array(
    			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
    		)
    	);

    	$estiloTituloColumnas = array(
    		'font' => array(
    			'name'  => 'Calibri',
    			'bold'  => false,
    			'size' =>12,
    			'color' => array(
    				'rgb' => 'FFFFFF'
    			)
    		),
    		'fill' => array(
    			'type' => PHPExcel_Style_Fill::FILL_SOLID,
    			'color' => array('rgb' => 'E31B23')
    		),
    		'borders' => array(
    			'allborders' => array(
    				'style' => PHPExcel_Style_Border::BORDER_THIN
    			)
    		),
    		'alignment' =>  array(
    			'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
    		)
    	);

    	$estiloActivo = array(
    		'fill' => array(
    			'type' => PHPExcel_Style_Fill::FILL_SOLID,
    			'color' => array('rgb' => '58D68D')
    		),
    		'borders' => array(
    			'allborders' => array(
    				'style' => PHPExcel_Style_Border::BORDER_THIN
    			)
    		)
    	);

    	$estiloInactivo = array(
    		'fill' => array(
    			'type' => PHPExcel_Style_Fill::FILL_SOLID,
    			'color' => array('rgb' => 'EC7063')
    		),
    		'borders' => array(
    			'allborders' => array(
    				'style' => PHPExcel_Style_Border::BORDER_THIN
    			)
    		)
    	);

    	$styleAling = array(
    		'alignment' => array(
    			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    		)
    	);

    	$styleAlingCenter = array(
    		'alignment' => array(
    			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    		)
    	);

    	$estiloInformacion = new PHPExcel_Style();
    	$estiloInformacion->applyFromArray( array(
    		'font' => array(
    			'name'  => 'Calibri',
    			'size' =>12,
    			'color' => array(
    				'rgb' => '000000'
    			)
    		),
    		'fill' => array(
    			'type'  => PHPExcel_Style_Fill::FILL_SOLID
    		),
    		'borders' => array(
    			'allborders' => array(
    				'style' => PHPExcel_Style_Border::BORDER_THIN
    			)
    		),
    		'alignment' =>  array(
    			'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    			'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
    		)
    	));
    	date_default_timezone_set("America/Mexico_City");
    	$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

    	$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
    	$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($estilo);
    	$objPHPExcel->getActiveSheet()->getStyle('A6:T6')->applyFromArray($estiloTituloReporte);
    	$objPHPExcel->getActiveSheet()->getStyle('A6:T6')->applyFromArray($estiloTituloColumnas);

    	$objPHPExcel->getActiveSheet()->setCellValue('F3', 'FECHA: '.$A);
    	$objPHPExcel->getActiveSheet()->setCellValue('B3', 'REPORTE DE EMPLEADOS');
    	$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    	$objPHPExcel->getActiveSheet()->setCellValue('A6', '');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    	$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Nombre');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(75);
    	$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Ubicación');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    	$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Calle');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    	$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Ciudad');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Estado');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    	$objPHPExcel->getActiveSheet()->setCellValue('G6', 'CP');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    	$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Pais');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
    	$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Teléfono'); 
    	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(35);
    	$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Email');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
    	$objPHPExcel->getActiveSheet()->setCellValue('K6', 'CURP');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('L6', 'NSS');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(40);
    	$objPHPExcel->getActiveSheet()->setCellValue('M6', 'Puesto');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('N6', 'Sueldo');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('O6', 'Factor (8 hrs)');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('P6', 'Factor (12 hrs)');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('Q6', 'Sueldo (8 hrs)');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('R6', 'Sueldo (12 hrs)');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
    	$objPHPExcel->getActiveSheet()->setCellValue('S6', 'Fecha Inicio');
    	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
    	$objPHPExcel->getActiveSheet()->setCellValue('T6', 'Antigüedad');

            //Establecemos en que fila inciara a imprimir los datos
    	$fila = 7; 

            //Recorremos los resultados de la consulta y los imprimimos
    	foreach ($resultado as $r) :
    		$antiguedad = $this->Antiguedad($r->fechaInicio);
    		$objPHPExcel->getActiveSheet()->getStyle('S'.$fila)->applyFromArray($styleAlingCenter);
    		$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->applyFromArray($styleAling);
    		$objPHPExcel->getActiveSheet()->getStyle('G'.$fila.':H'.$fila)->applyFromArray($styleAlingCenter);
    		$objPHPExcel->getActiveSheet()->getStyle('K'.$fila.':L'.$fila)->applyFromArray($styleAlingCenter);
    		$objPHPExcel->getActiveSheet()->getStyle('Q'.$fila.':R'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    		$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    		$puesto = $this->ObtenerPuesto($r->idPuesto);
    		if ($bStatus == 'Activo')
    			$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->applyFromArray($estiloActivo);
    		else
    			$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->applyFromArray($estiloInactivo);

    		$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, "");
    		$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->nombre);
    		$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->ubicacion);
    		$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->calle);
    		$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->ciudad);
    		$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->estado);
    		$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r->cp);
    		$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $r->pais);
    		$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $r->telefono);
    		$objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $r->email);
    		$objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $r->curp);
    		$objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $r->nss);
    		$objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $puesto);
    		$objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $r->sueldo);
    		$objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $r->fsrOcho);
    		$objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $r->fsrDoce);
    		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $r->sueldoOcho);
    		$objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $r->sueldoDoce);
    		$objPHPExcel->getActiveSheet()->setCellValue('S'.$fila, $r->fechaInicio);
    		$objPHPExcel->getActiveSheet()->setCellValue('T'.$fila, $antiguedad);
    		$fila++; 
    	endforeach;

    	header("Content-Type: application/vnd.ms-excel");
    	header('Content-Disposition: attachment;filename="Empleados.csv"');
    	header('Cache-Control: max-age=0');

    	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    	ob_end_clean();
    	$objWriter->save('php://output');    
    }   
}