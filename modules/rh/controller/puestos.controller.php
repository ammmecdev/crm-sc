<?php 
require_once 'model/puesto.php';
require_once 'model/modulo.php';

class PuestosController{

	private $pdo;
	private $mensaje;
	private $error;
	private $tabla;

	public function __CONSTRUCT()
	{
		$this->modelPuestos = new Puesto();
		$this->modelModulo = new Modulo();
	}

	public function Index()
	{
		$puestos=true;
		$page="view/puestos/puestos.php";
		require_once '../../view/index.php';
	}

	public function Organigrama()
	{
		$puestos=true;
		$page="view/puestos/organigrama.php";
		require_once '../../view/index.php';
	}

	public function Consultas()
	{
		$editar_puesto = $this->consultaPermisos(2);
		echo'  
		<table class="table table-bordered table-hover" id="tbl">
		<thead>
		<tr>
		<td colspan="6" align="center"><strong>Puestos</strong></td>
		</tr>
		<tr style="background-color: #6c757dc9; color:white">
		<td align="center"><strong>Item</strong></td>
		<td align="center"><strong>No. Personas</strong></td>
		<td><strong>Puesto</strong></td>
		<td><strong>Módulo</strong></td>
		<td><strong>Nivel</strong></td>';
		if ($editar_puesto == true) {	
			echo '<td align="center"><strong>Editar</strong></td>';
		}		
		echo '</tr>
		</thead>';
		$n=0;
		foreach ($this->modelPuestos->Listar() as $puesto): 
			$empleados=$this->modelPuestos->ContarEmpleados($puesto->idPuesto);
			echo'<tr>
			<td align="center"><strong>' . ++$n . '</strong></td>
			<td align="center">' . $empleados->numeroEmpleados . '</td>
			<td><a style="color:#020202ba" href="#" data-toggle="modal" data-target="#mPuesto" onclick="editarPuesto(' . $puesto->idPuesto; ?> , <?php echo  "'" . $puesto->puesto . "'"; ?> , <?php echo $puesto->idNivel; ?> , <?php echo $puesto->idModulo . ')"> ' . $puesto->puesto . '</a> 
			<a href="" style="color:#d9534f" data-toggle="modal" data-target="#mPermisos" onclick="permisosPuesto('; echo $puesto->idPuesto ?>,<?php echo "'". $puesto->puesto . "'"; ?> ,<?php echo "'". $puesto->nivel . "'"; echo ')"> <i class="fas fa-lock"></i></a></td>
			<td>' . $puesto->modulo . '</td>
			<td>Nivel ' . $puesto->idNivel . '</td>';
			if ($editar_puesto == true) { echo '<td align="center"><a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mPuesto" onclick="editarPuesto(' . $puesto->idPuesto; ?> , <?php echo  "'" . $puesto->puesto . "'"; ?> , <?php echo $puesto->idNivel; ?> , <?php echo $puesto->idModulo . ')"> <span class="glyphicon glyphicon-pencil"></span></a></td>'; }
			echo '</tr>';
		endforeach;
		echo '</table>';
	}

	public function ConsultaModulos()
	{
		$editar_modulo = $this->consultaPermisos(1);
		echo'  
		<table class="table table-bordered table-hover">
		<thead>
		<tr>
		<td colspan="5" align="center"><strong>Módulos</strong></td>
		</tr>
		<tr  style="background-color: #6c757dc9; color:white">
		<td align="center" style="width: 20%;"><strong>Item</strong></td>
		<td><strong>Módulo</strong></td>';
		if ($editar_modulo == true) {
			echo '<td align="center"><strong>Editar</strong></td>';
		}
		echo '</tr>
		</thead>
		<tbody> ';
		foreach($this->modelModulo->Listar() as $modulo): 
			echo '<tr>
			<td align="center"><strong>'.$modulo->idModulo.'</strong></td>
			<td>' . $modulo->modulo . '</td>';

			if ($editar_modulo == true) { echo '<td align="center"><a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mModulo" onclick="editarModulo(' . $modulo->idModulo; ?> , <?php echo  "'" . $modulo->modulo ."'"; ?> <?php echo ')"> <span class="glyphicon glyphicon-pencil"></span></a></td>'; }
			echo '</tr>';
		endforeach; 
		echo '</tbody>
		</table>';
	}

	public function consultaPermisos($val)
	{
		$permisos = $_SESSION['permisos'];
		$retorno = false;
		if ($val == 1) {
			$permiso = "editar_modulos";
		}else{
			$permiso = "editar_puestos";
		}
		foreach ($permisos as $p):
			if ($p == $permiso) {
				$retorno = true; 
			}
		endforeach;
		return $retorno;
	}

	public function ListarPuestos(){
		try {
			header('Content-Type: application/json');
			$array=array();
			foreach ($this->modelPuestos->Listar() as $puesto):
				$row_array['puesto']=$puesto->puesto;
				$row_array['idPuesto']=$puesto->idPuesto;
				array_push($array, $row_array);
			endforeach;
			echo json_encode($array, JSON_FORCE_OBJECT);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Guardar()
	{
		try {
			$puesto = new Puesto();

			$puesto->tipo = $_POST['tipo'];
			$puesto->parent = $_POST['parent'];

			if($_POST['tipo']=='puesto')
			{
				$idPuesto = $_POST['idPuesto'];
				$puesto->puesto = $_POST['puesto'];
				$puesto->idModulo = $_POST['idModulo'];
				$puesto->idNivel = $_POST['nivel'];
				if($idPuesto>0){
					$puesto->idPuesto = $idPuesto;
					$this->modelPuestos->Actualizar($puesto);
					$nodo = $this->modelPuestos->ObtenerNodo($puesto->idPuesto);
					$puesto->idNodo = $nodo->idNodo;
					$this->modelPuestos->ActualizarNodo($puesto);
					$this->ActualizarModulo($puesto->idNodo, $puesto->idModulo, $puesto->tipo);
					echo 'Se han actualizado correctamente los datos del nodo';
				}else{
					$lastId = $this->modelPuestos->ObtenerUltimoId();
					$puesto->idPuesto = (int)$lastId->idPuesto+1;
					$this->modelPuestos->RegistrarPuesto($puesto);
					$puesto->idRelacion = $puesto->idPuesto;
					$this->modelPuestos->RegistrarNodo($puesto);
					echo 'Se han registrado correctamente los datos del nodo';
				}
			}else if($_POST['tipo']=='modulo'){
				$puesto->idNivel = 0;
				$puesto->idRelacion = $_POST['idModulo2'];
				$puesto->idNodo = $_POST['idNodo'];
				if($_POST['idNodo'] > 0){
					$this->modelPuestos->ActualizarNodo($puesto);
					echo 'Se han actualizado correctamente los datos del nodo';
				}else{
					$this->modelPuestos->RegistrarNodo($puesto);
					echo 'Se han registrado correctamente los datos del nodo';
				}
			}
		} catch (Exception $e) {
			// die($e->getMessage());
			echo 'Se ha producido un error al intentar guardar los datos del nodo';
		}
	}

	public function ActualizarModulo($idNodo, $idModulo, $tipo)
	{
		$array=array();
		array_push($array, $idNodo);
		do{
			$array2=$this->ObtNodosHijoActModPadre($array[0], $idModulo);
			$array = array_merge($array, $array2);
			unset($array[0]);
			$array=array_values($array);
		}while(!empty($array));
	}

	public function ObtNodosHijoActModPadre($idNodo, $idModulo){
		$array=array();
		$nodoPadre=$this->modelPuestos->ObtenerNodoPadre($idNodo);
		if($nodoPadre->tipo=='puesto'){
			$this->modelPuestos->ActualizarModulo($nodoPadre->idRelacion, $idModulo);
			foreach ($this->modelPuestos->ObtenerNodosHijo($idNodo) as $r):
				array_push($array, $r->idNodo);
			endforeach;
		}
		return $array;
	}

	public function GuardarPuesto()
	{
		try {
			$puesto = new Puesto();
			$puesto->idPuesto = $_POST['idPuesto'];
			$puesto->puesto = $_POST['puesto'];
			$puesto->idModulo = $_POST['idModulo'];
			$puesto->idNivel = $_POST['nivel'];
			$this->modelPuestos->Actualizar($puesto);
			echo 'Se han actualizado correctamente los datos del puesto';
		} catch (Exception $e) {
			echo 'Se ha producido un error al intentar guardar los datos del nodo';
		}
	}

	public function GuardarModulo()
	{
		try {
			$modulo = new Modulo();
			$modulo->idModulo = $_POST['idModulo'];
			$modulo->modulo = $_POST['modulo'];
			$this->modelModulo->Actualizar($modulo);
			echo 'Se han actualizado correctamente los datos del módulo';
		} catch (Exception $e) {
			echo 'Se ha producido un error al intentar guardar los datos del módulo';
		}
	}

	public function Baja()
	{
		try {
			$idPuesto=$_REQUEST['idPuesto'];
			$nodo=$this->modelPuestos->ObtenerNodo($idPuesto);
			$nodo->idNodo;
			$r=$this->modelPuestos->VerificaEnNodos($nodo->idNodo);
			if($r->dependientes>0){
				echo 'denegado1';
			}else{
				$this->modelPuestos->Baja($idPuesto);
				$this->modelPuestos->EliminarNodo($idPuesto);
				echo 'Se ha dado de baja correctamente el puesto';
			}
		} catch (Exception $e) {
			echo 'denegado2';
		}
	}

	public function ObtenerOrganigrama()
	{
		try {
			$dataTest=array();
			$organigrama=$this->modelPuestos->ObtenerOrganigrama();
			foreach ($organigrama as $r):
				$nodo=$this->modelPuestos->ObtenerInfoNodo($r->idRelacion, $r->tipo);
				switch ($r->tipo) {
					case 'modulo':
					$name=$nodo->modulo;
					$row_array['nivel'] = 0;
					$row_array['idModulo'] = $nodo->idModulo;
					$row_array['back_color'] = "#34495e";
					$row_array['color'] = "#fff";
					break;
					case 'puesto':
					$name=$nodo->puesto;
					$row_array['nivel'] = $nodo->idNivel;
					$row_array['idModulo'] = $nodo->idModulo;
					$row_array['color'] = $nodo->color;
					$row_array['back_color'] = $nodo->back_color;
					break;
				}
				$row_array['id'] = $r->idNodo;
				$row_array['name'] = $name;
				$row_array['parent'] = $r->parent; 
				$row_array['tipo'] = $r->tipo;
				$row_array['idRelacion'] = $r->idRelacion;
				array_push($dataTest,$row_array);
			endforeach;
			echo json_encode($dataTest);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function ObtenerNodos()
	{
		try {
			header('Content-Type: application/json');
			$dataTest=array();
			$nodos=$this->modelPuestos->ObtenerOrganigrama();
			foreach ($nodos as $r):
				$nodo=$this->modelPuestos->ObtenerInfoNodo($r->idRelacion, $r->tipo);
				switch ($r->tipo) {
					case 'modulo':
					$name=$nodo->modulo;
					$row_array['idModulo'] = $nodo->idModulo;
					break;
					case 'puesto':
					$name=$nodo->puesto;
					$row_array['idModulo'] = $nodo->idModulo;
					break;
				}
				$row_array['id'] = $r->idNodo;
				$row_array['name'] = $name; 
				$row_array['tipo'] = $r->tipo;
				array_push($dataTest,$row_array);
			endforeach;
			echo json_encode($dataTest, JSON_FORCE_OBJECT);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function ObtenerInfoNodo()
	{
		try {
			header('Content-Type: application/json');
			$dataNode = array();
			$idRelacion = $_POST['idRelacion'];
			$tipo = $_POST['tipo'];
			$parent = $_POST['parent'];
			$info = $this->modelPuestos->ObtenerInfoNodo($idRelacion, $tipo);
			$nodoPadre = $this->modelPuestos->ObtenerNodoPadre($parent);
			$padre = $this->modelPuestos->ObtenerPadre($nodoPadre->idRelacion, $nodoPadre->tipo);

			if($nodoPadre->tipo == 'puesto'){
				$row_array['padre'] = $padre->puesto;
				$row_array['tipoPadre'] = 'puesto';
			}

			else if($nodoPadre->tipo == 'modulo'){
				$row_array['padre'] = $padre->modulo;
				$row_array['tipoPadre'] = 'modulo';
			}

			if($tipo == 'puesto'){
				$row_array['nivel'] = $info->idNivel;
				$row_array['idModulo'] = $info->idModulo;
				$row_array['idPuesto'] = $info->idPuesto;
				$row_array['puesto'] = $info->puesto;
				
			}else if($tipo == 'modulo')
			{
				$row_array['idModulo'] = $info->idModulo;
				$row_array['modulo'] = $info->modulo;
			}
			array_push($dataNode, $row_array);
			echo json_encode($dataNode, JSON_FORCE_OBJECT);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function ListarModulos()
	{
		header('Content-Type: application/json');
		$datos = array();
		$res=$this->modelModulo->ListarModulos();
		foreach ($res as $r) :
			$row_array['idModulo']  = $r->idModulo;
			$row_array['modulo']  = $r->modulo;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ListarNiveles()
	{
		header('Content-Type: application/json');
		$datos = array();
		$res=$this->modelPuestos->ListarNiveles();
		foreach ($res as $r) :
			$row_array['idNivel']  = $r->idNivel;
			$row_array['nivel']  = $r->nivel;
			$row_array['back_color']  = $r->back_color;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ObtenerPermisos()
	{
		$idPuesto = $_POST['idPuesto'];
		$res = $this->modelPuestos->ObtenerPermisos($idPuesto);
		$permisos=array();
		foreach ($res as $r) 
			array_push($permisos, $r->idElement);
		echo '<section class="panel default blue_title" style="margin-top: -10px; margin-bottom: -20px; box-shadow: 0 0px 3px 0 rgba(0,0,0,0.2);">
		<legend align="center" style="margin-bottom: -5px"><h3>Selección de permisos</h3></legend>
		<div class="panel-body">
		<ul class="treeview-black treeview" id="black">';
		foreach($this->modelModulo->Listar() as $modulo):
			if($modulo->idModulo == 2 || $modulo->idModulo == 3 || $modulo->idModulo==6){
				echo '<li class="liPermisos1 expandable lastExpandable">
				<div class="hitarea expandable-hitarea lastExpandable-hitarea"></div>
				<!-- <div class="minimal single-row"> -->';
				$check=false;
				foreach ($permisos as $p)
					if ($p == $modulo->idElement) $check=true;
				echo '<input type="checkbox" id="'.$modulo->idElement.'" name="'.$modulo->idElement.'" value="modulo,'. $modulo->idModulo.'" class="modulos"'; if($check == true){ echo 'checked'; }  echo '>&nbsp;
				<label>'.$modulo->modulo.'</label>
				<!-- </div><ul style="display: none;"> -->
				<ul>';
				foreach ($this->modelModulo->ListarSecciones($modulo->idModulo) as $seccion):
					echo '<li class="liPermisos2 lastExpandable">
					<div class="hitarea expandable-hitarea"></div>
					<!-- <div class="minimal single-row"> -->';
					$check=false;
					foreach ($permisos as $p)
						if ($p == $seccion->idElement) $check=true;
					echo '<input type="checkbox" id="'.$seccion->idElement.'" name="'.$seccion->idElement.'" value="seccion,'.$seccion->idSeccion.'" class="'.$modulo->idElement . ' seccion"'; if($check == true){ echo 'checked'; }  echo '>&nbsp;
					<font>'.$seccion->seccion.'</font>
					<!-- </div> -->
					<ul>';
					foreach($this->modelModulo->ListarFunciones($seccion->idSeccion) as $fun):
						echo '<li class="liPermisos3">
						<!-- <div class="minimal single-row"> -->';
						$check=false;
						foreach ($permisos as $p)
							if ($p == $fun->idElement) $check=true;
						echo '<input type="checkbox" id="'.$fun->idElement.'" name="'.$fun->idElement.'" value="funcion,'.$fun->idFuncion.'" class="'.$modulo->idElement . ' ' . $seccion->idElement.' funcion"'; if($check == true){ echo 'checked'; }  echo '>&nbsp;
						<font>'.$fun->funcion.'</font>
						<!-- </div> -->
						</li>';
					endforeach; 
					echo '</ul>
					</li>';
				endforeach; 
				echo '</ul>
				</li>';
			}
		endforeach; 
		echo '</ul></div></selection>';
		echo "<script type='text/javascript'>
		$('.modulos').click(function(){
			if (this.checked){
				$('.'+this.id).prop('checked', true);
			}else{
				$('.'+this.id).prop('checked', false);
			}
		});

		$('.seccion').click(function(){
			if (this.checked){
				$('.'+this.id).prop('checked', true);
				var res = $(this).attr('class').split(' ');
				mod=res[0];
				$('#'+mod).prop('checked', true);
			}else{
				$('.'+this.id).prop('checked', false);
			}
		});

		$('.funcion').click(function(){
			if (this.checked){
				var res = $(this).attr('class').split(' ');
				mod=res[0];
				sec=res[1];
				$('#'+mod).prop('checked', true);
				$('#'+sec).prop('checked', true);
			}
		});
		</script>
		<script src='../../assets/plugins/tree-view/jquery.treeview.js'></script>
		<script src='../../assets/plugins/tree-view/jquery.cookie.js'></script>
		<script type='text/javascript' src='../../assets/plugins/tree-view/demo.js'></script>";
	}

	public function ActualizarPermisos()
	{
		try {
			$idPuesto = $_POST['idPuesto'];
			$puesto = $_POST['puesto'];
			$post=$_POST;
			$this->modelPuestos->LimpiarPermisos($idPuesto);
			foreach ($post as $key => $value) {
				if($key!='idPuesto' && $key!='puesto'){
					$idElement = $key;
					$valores = explode(',', $value);
					$tipo = $valores[0];
					$idRelacion = $valores[1];
					$this->modelPuestos->InsertarPermisos($idPuesto, $tipo, $idRelacion, $idElement);
				}
			}
			echo "Se han actualizado correctamente los permisos del ". $puesto;	
		} catch (Exception $e) {
			echo "error";
		}
	}	
}

?>