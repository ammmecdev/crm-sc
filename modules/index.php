<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Módulos SC</title>
	<link rel="icon" href="assets/imagenes/isotipo.ico">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">



	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> 	

	<link rel="stylesheet" href="assets/css/AdminLTE.min.css">

	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script> 

</head>

<style>
body{

	/*background-image: url("assets/imagenes/modulos2.jpg");*/
	background-color: rgba(9, 85, 104, 1) !important;
	background-position: center;   
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
	height: 100vh;

}
.small-box{
	background:-webkit-gradient(linear, 25% 0%, 10% 91%, from(#E0F7FA), to(#9E9E9E));

	border-top-left-radius:5px;
	border-top-right-radius:5px;
	border-bottom-left-radius:5px;
	border-bottom-right-radius:5px;

	-moz-box-shadow: 4px 4px 16px #000000;
	-webkit-box-shadow: 4px 4px 16px #000000;
	box-shadow: 4px 4px 16px #000000;
}

.sombra {
	-moz-box-shadow: 4px 4px 16px #000000;
	-webkit-box-shadow: 4px 4px 16px #000000;
	box-shadow: 4px 4px 16px #000000;
}

.disabled{
	opacity: 0.5;
	cursor: not-allowed
}

.navbar {
	background-color: transparent;
	border: transparent;
}

.navbar-inverse {
	background-image: linear-gradient(to bottom,transparent 0,transparent 100%);
	filter: 
}

footer {
	position: fixed;
	height: 100px;
	bottom: 0;
	width: 100%;
}

.btn-primary{
	background-color: rgba(9, 85, 104, 1) !important;
}
.btn-secondary{
	background-color: rgba(87, 96, 102, 1) !important;
}
.btn-info{
	background-color: rgba(58, 163, 213, 1) !important;
}
.bg-primary, {
	background-color: rgba(9, 85, 104, 1) !important;
}
.text-primary{
	color: rgba(9, 85, 104, 1) !important;
}
.text-secondary{
	color: rgba(87, 96, 102, 1) !important;
}
.text-white{
	color:#fff;
}

</style>

<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#modal-collapse" aria-expanded="false" style="margin-top: 8px;">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="#" class="navbar-text" data-toggle="dropdown" id="admin8" style="padding-bottom: 9px;">
					<img src="assets/imagenes/<?php echo $_SESSION['foto']?>" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;" />
					<span>&nbsp;</span><?php echo $_SESSION['nombreUsuario']; ?>

				</a>
			</div>
			<div class="collapse navbar-collapse" id="modal-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="?c=login&a=Logout&g=true" style="margin-top: 8px;"><i class="glyphicon glyphicon-log-out"></i> Cerrar Sesión</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container" align="center">
		<section class="content">
			<center><h1 class="text-primary" style="margin-top: -20px;">CRM SMART CONDITION</h1>
				<img src="assets/imagenes/sc-blanco.png" alt="" >
				<h1 style="margin-bottom: 60px; margin-top: 35px;" class="text-white"> Módulos del Sistema</h1>
			</center>
			<div class="row">
				<?php 
				$permisos = $_SESSION['permisos'];
				$permiso = false;
				foreach ($permisos as $p):
					if ($p == 'mod_ventas') $permiso=true; 
				endforeach;
				?>
				<div class="col-lg-12 col-xs-12">
					
				</div>
				<!-- ./col -->
				
				<div class="col-lg-12">
					<br>
				</div>
			</div>
			<div class="row">

				<div class="col-xs-12 col-lg-5 col-lg-offset-1" style="margin-top: 20px;">
					<!-- small box -->
					<a <?php if ($permiso == true){ ?>  href="./modules/ventas" <?php } ?> class="mall-box-footer">
						<div class="small-box">
							<div <?php if($permiso==true){ ?> class="panel panel-default" <?php } else { ?> class="panel panel-default disabled" <?php } ?>>
								<div class="panel-heading">
									<h3 class="panel-title" style="margin-top: 20px; margin-bottom: 0px;"><strong>Ventas</strong></h3>
								</div>
								<div class="panel-body">
									<div class="icon" style="margin-top: 20px; margin-right: 10px;">
										<span class="glyphicon glyphicon-tags"></span>
									</div>									
								</div>              
							</div>
						</div>
					</a>
				</div>
				<?php 
				$permiso = false;
				foreach ($permisos as $p):
					if ($p == 'mod_ventas') $permiso=true; 
				endforeach;
				?>
				<div class="col-lg-5 col-xs-12 " style="margin-top: 20px;">
					<a <?php if ($permiso == true){ ?>  href="#" <?php } ?> class="mall-box-footer">
						<div class="small-box">
							<div <?php if($permiso==true){ ?> class="panel panel-default disabled" <?php } else { ?> class="panel panel-default disabled" <?php } ?>>
								<div class="panel-heading">
									<h3 class="panel-title" style="margin-top: 20px; margin-bottom: 0px;"><strong>Asset Care</strong></h3>
								</div>
								<div class="panel-body">
									<div class="inner">
									</div>
									<div class="icon" style="margin-top: 20px;">
										<span class="glyphicon glyphicon-transfer"></span>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<!-- ./col -->
			<!-- /.row -->
		</section>
	</div>
</body>
<!-- Footer -->
<footer class="fixed-bottom">

	<div class="container text-center">

		<div class="row">
			<div class="col-lg-12">
				<span class="help-block" align="center" style="margin-top: 0px;">&#9400;&nbsp;Copyright:<!-- <i class="far fa-copyright" style="font-size: 9px;"></i> --> 2019</span>
				<span class="help-block" align="center" style="margin-top: 0px;">SMART CONDITION S.A. DE C.V.</span>
			</div>
		</div>
	</div>

</footer>
<!-- Footer -->
</html>