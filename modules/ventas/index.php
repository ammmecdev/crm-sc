<?php 
require_once '../../model/database.php';
session_start();
if (!isset($_REQUEST['c']) && isset($_SESSION['seguridad'])){
	header("Location: index.php?c=ventas"); 
}
$controller = 'login';

if (!isset($_REQUEST['c'])) {
	require_once "../../controller/login.controller.php";
	$var= new LoginController;
	$var->Index();

}else if(isset($_REQUEST['g'])){

	$controller = $_REQUEST['c'];
	$accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'Index';
	require_once "../../controller/$controller.controller.php";
	$controller = ucwords($controller) . 'Controller';
	$controller = new $controller;
	call_user_func( array( $controller, $accion) );

}else{

	$controller = $_REQUEST['c'];
	$accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'Index';
	require_once "controller/$controller.controller.php";
	$controller = ucwords($controller) . 'Controller';
	$controller = new $controller;
	call_user_func( array( $controller, $accion) );
}

?>