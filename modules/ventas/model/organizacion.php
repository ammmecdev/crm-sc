<?php
class Organizacion
{
	public $idOrganizacion;
	public $nombreOrganizacion;
	public $direccion;
	public $paginaWeb;
	public $telefono;
	public $clave;
	public $idSector;
	public $localidadEstado;
	public $extensionTelefonica;
	public $tipoTelefono;
	public $rfc;
	private $pdo;    
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para listar datos
	public function ListarO()
	{
			$stm = $this->pdo->prepare("SELECT * FROM organizaciones,sector_industrial where organizaciones.idSector = sector_industrial.idSector");
			$stm->execute();			
			return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar datos
	public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT * FROM organizaciones,usuarios,sector_industrial WHERE organizaciones.idUsuario = usuarios.idUsuario and organizaciones.idSector = sector_industrial.idSector");
		$stm->execute();			
		return $stm;
	}

	public function listarClientes($inf, $sup)
	{
		$stm = $this->pdo->prepare("SELECT * FROM organizaciones,usuarios,sector_industrial WHERE organizaciones.idUsuario = usuarios.idUsuario and organizaciones.idSector = sector_industrial.idSector ORDER by organizaciones.idOrganizacion ASC LIMIT $inf, $sup");
		$stm->execute();			
		return $stm;
	}
//Metodo para listar todos las organizaciones para el select
	public function ListarSectorIndustrial()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM sector_industrial");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//Metodo para contar las personas que estn en una organizacion 
	public function ContadorPersonas($idOrganizacion)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT COUNT(*) FROM organizaciones, personas WHERE organizaciones.idOrganizacion = personas.idOrganizacion AND organizaciones.idOrganizacion = $idOrganizacion ");
			$stm->execute();



			return $cont=implode($stm->fetchAll(PDO::FETCH_COLUMN));
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
//Metodo para contar los negocios cerrados 
	public function ContadorNegocios($idOrganizacion,$status)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT COUNT(*) FROM organizaciones, negocios WHERE organizaciones.idOrganizacion = negocios.idOrganizacion AND organizaciones.idOrganizacion = $idOrganizacion AND negocios.status = $status");
			$stm->execute();

			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));

			return $cont;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}



//Metodo para obtener la fecha proxima de una actividad por negocio 
	public function ContadorFechaProxima($idOrganizacion)
	{
		try
		{
			date_default_timezone_set("America/Mexico_City");
			$A= date("Y")."-".date("m")."-".date("d"); //->No obitiene la fecha real
			$stm = $this->pdo->prepare("SELECT fechaActividad FROM actividades WHERE idOrganizacion=$idOrganizacion AND fechaActividad >='$A' ORDER BY fechaActividad ASC ");
			$stm->execute();
			$resultado = $stm->fetchAll();

			if (!empty($resultado)) 
				return $datos = $resultado[0][0];				
			else
				return null;			
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para contar las organizaciones
	public function Contador()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT COUNT(*) FROM organizaciones ");
			$stm->execute();
			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;	
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para verificar si hay datos duplicados nombre de Organizacion con nombre
	public function VerificarNombre($nombreOrganizacion,$id)
	{
		if ($id==null) {
			$sql = $this->pdo->prepare("SELECT * FROM organizaciones WHERE nombreOrganizacion='$nombreOrganizacion'");
		}else{
			$sql = $this->pdo->prepare("SELECT * FROM organizaciones WHERE nombreOrganizacion='$nombreOrganizacion' and idOrganizacion!=$id");
		}
		$sql->execute();
		$valor = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $valor;					

	}
	//Metodo para verificar si hay datos duplicados nombre de Organizacion con  calve
	public function VerificarClave($clave,$id)
	{
		if ($id==null) {
			$sql = $this->pdo->prepare("SELECT * FROM organizaciones WHERE clave='$clave'");
		}else{
			$sql = $this->pdo->prepare("SELECT * FROM organizaciones WHERE clave='$clave' and idOrganizacion!=$id");
		}
		$sql->execute();
		$valor = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $valor;					


	}
	//Metodo para verificar el usuario para poder asi eliminar la organizacion
	public function VerificarUsuario($email,$pass)
	{
		try 
		{
			$sql = $this->pdo->prepare("SELECT idTipo FROM usuarios WHERE email = '$email' AND pass = '$pass' LIMIT 1");
			$sql->execute();
			$valor = implode($sql->fetchAll(PDO::FETCH_COLUMN));
			return $valor;

		} 
		catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	//Metodo para registrar
	public function Registrar(Organizacion $data)
	{
		try 
		{
			$sql = "INSERT INTO organizaciones 
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			$this->pdo->prepare($sql)
			->execute(
				array(
					null,
					$data->idUsuario,
					$data->nombreOrganizacion, 
					$data->direccion, 
					$data->paginaWeb,
					$data->telefono,
					$data->clave,
					$data->idSector,
					$data->localidadEstado,
					$data->extensionTelefonica,
					$data->tipoTelefono,
					$data->rfc
				)
			);

			

		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Organizacion $data)
	{
		try 
		{
			$sql = "UPDATE organizaciones SET 
			idUsuario=?,
			nombreOrganizacion = ?,
			direccion = ?,
			paginaWeb = ?,
			telefono = ?,
			clave = ?,
			idSector = ?,
			localidadEstado = ?,
			extensionTelefonica = ?,
			tipoTelefono = ?,
			rfc = ?  
			WHERE idOrganizacion = ? ";

			$this->pdo->prepare($sql)
			->execute(
				array(
					$data->idUsuario,
					$data->nombreOrganizacion, 
					$data->direccion, 
					$data->paginaWeb,
					$data->telefono,
					$data->clave,
					$data->idSector,
					$data->localidadEstado,
					$data->extensionTelefonica,
					$data->tipoTelefono,
					$data->rfc,
					$data->idOrganizacion
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
	//Metdo para eliminar organizaciones
	public function Eliminar($idOrganizacion)
	{

		$sql ="DELETE FROM organizaciones WHERE idOrganizacion=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idOrganizacion
			)
		);

	}
	//Metodo para filtrar las organisaciones por letras de abesedario
	public function ConsultaOrganizaciones($bTexto, $bAlfabetica)
	{
		try
		{	
			$stm = $this->pdo->prepare("SELECT * FROM organizaciones,usuarios,sector_industrial WHERE organizaciones.idUsuario = usuarios.idUsuario and organizaciones.idSector = sector_industrial.idSector and nombreOrganizacion like '$bAlfabetica%' and (nombreOrganizacion like '%$bTexto%' or clave like '%$bTexto%' or direccion like '%$bTexto%' or nombreSector like '%$bTexto%' or rfc like '%$bTexto%');");
			$stm->execute();

			return $stm;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para filtrar las organisaciones por letras de abesedario
	public function ConsultaOrganizacionesParaExportar($bTexto, $bAlfabetica)
	{


		$stm = $this->pdo->prepare("SELECT * FROM organizaciones,usuarios,sector_industrial WHERE organizaciones.idUsuario = usuarios.idUsuario and organizaciones.idSector = sector_industrial.idSector and nombreOrganizacion like '$bAlfabetica%' and (nombreOrganizacion like '%$bTexto%' or clave like '%$bTexto%' or direccion like '%$bTexto%' or nombreSector like '%$bTexto%' or rfc like '%$bTexto%');");
		$stm->execute();			
		return $stm->fetchAll(PDO::FETCH_OBJ);
		print_r($stm);
	}
	//Metodo para el id de Una organizacion por el nombre de la Organizacion
	//Creado por: Ivan
	public function ObtenerId($nombreOrganizacion){
		$sql = $this->pdo->prepare("SELECT idOrganizacion FROM organizaciones WHERE nombreOrganizacion = '$nombreOrganizacion'");

		$sql->execute();
		$valor = implode($sql->fetchAll(PDO::FETCH_COLUMN));			
		return $valor; 
	}

	public function clientes()
	{
		$stm = $this->pdo->prepare("SELECT count(*) as totalRegistros FROM organizaciones");
		$stm->execute();
		return $stm->fetch(PDO::FETCH_COLUMN);
	}
}