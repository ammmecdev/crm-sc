<?php 
class Etapa
{

	private $pdo;

	public $idEtapa;
	public $idEmbudo;
	public $nombreEtapa;
	public $contenido;
	public $probabilidad;
	public $inactividad;
	public $consecutivo;
	public $orden;


	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

		//Metodo para listar los embudos para posicionar el registro
	public function VerificarNumOrden(Etapa $data)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT orden FROM etapasventas WHERE idEmbudo = ? ORDER BY orden DESC LIMIT 1");
			$stm->execute(
				array(
					$data->idEmbudo
				)
			);

			return $cont=implode($stm->fetchAll(PDO::FETCH_COLUMN));
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

		    //Metodo para registrar ETAPAS
	public function Registrar(Etapa $data)
	{
		$sql ="INSERT INTO etapasventas VALUES(?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idEmbudo,
				$data->nombreEtapa,
				$data->contenido,
				$data->probabilidad,
				$data->inactividad,
				$data->consecutivo,
				$data->orden
			)
		);

	}
	//Metodo para actualizar ETAPAS
	public function Actualizar(Etapa $data)
	{
		$sql ="UPDATE etapasventas SET 
		nombreEtapa = ?,
		probabilidad = ?,
		inactividad = ?
		WHERE idEtapa = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombreEtapa,
				$data->probabilidad,
				$data->inactividad,
				$data->idEtapa
			)
		);
	}
	//Metodo para eliminar etapas
	public function Eliminar($idEtapa)
	{
		$sql ="DELETE FROM etapasventas WHERE idEtapa = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEtapa
			)
		);
	}
}
?>