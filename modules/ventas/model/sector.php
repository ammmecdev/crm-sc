<?php
class Sector
{    
	public $idSector;
	public $claveSector;
	public $nombreSector;
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT * FROM sector_industrial");

		$stm->execute();

		return $stm;
	}
	//Metodo para contar las personas
	public function Contador()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT COUNT(*) FROM sector_industrial");
			$stm->execute();
			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;	
		}
		catch(Exception $e)
		{
			die($e->getMessage());
			$this->error=true;
			$this->mensaje="Se ha producido un error";
		}
	}
	//Metodo para Registrar
	public function Registrar(Sector $data)
	{
		$sql ="INSERT INTO sector_industrial VALUES(?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->nombreSector,
				$data->claveSector
				)
			);
	}
	

	public function Actualizar(Sector $data)
	{
		$sql = "UPDATE  sector_industrial SET nombreSector=?,
		claveSector=?
		WHERE  idSector=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombreSector,
				$data->claveSector,
				$data->idSector
				)
			);
	}
	//Metdo para eliminar
	public function Eliminar($idSector)
	{
		$sql ="DELETE FROM sector_industrial WHERE idSector=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idSector
				)
			);
	}
	public function ConsultaSectores($valorBusqueda, $bAlfabetica)
	{
		
		$stm = $this->pdo->prepare("SELECT * FROM sector_industrial WHERE nombreSector LIKE '$bAlfabetica%' and (nombreSector LIKE '%$valorBusqueda%' or claveSector like '%$valorBusqueda%');");
		$stm->execute();
		
		return $stm;
	}
	//Metodo para verificar si hay datos duplicados nombre de Organizacion con  calve
	public function VerificarClave($clave,$id)
	{
		if ($id==null) {
			$sql = $this->pdo->prepare("SELECT * FROM sector_industrial WHERE claveSector='$clave'");
		}else{
			$sql = $this->pdo->prepare("SELECT * FROM sector_industrial WHERE claveSector='$clave' and idSector!=$id");
		}
		$sql->execute();
		$valor = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $valor;
	}
	}//fin de la clase
?>