<?php 
class Embudo
{
	public $nombre;
	public $perdido;
	public $ganado;
	public $idUsuario;
	public $idEmbudo;
	private $pdo;
	
	public $nombreEtapa;
	public $idEtapa;
	public $orden;
	public $contenido;


	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	// ------------------------------------------------------------------------------------------------

	public function ListarEtapas($idEmbudo)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM etapasventas WHERE etapasventas.idEmbudo = $idEmbudo ORDER BY orden ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
// ------------------------------------------------------------------------------------------------

	//Metodo para listar los embudos
	public function Listar()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM embudos");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}



	//Metodo para listar los embudos para posicionar el registro
	public function ListarEmbudo()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM embudos ORDER BY idEmbudo DESC LIMIT 1");
			$stm->execute();

			return $cont=implode($stm->fetchAll(PDO::FETCH_COLUMN));
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Metodo para verificar si ya existe el nombre del EMBUDO
	public function VerificarRegistro(Embudo $data)
	{
		try
		{
			$sql= $this->pdo->prepare("SELECT * FROM embudos WHERE nombre = ? ");
			$resultado=$sql->execute(
				array(
					$data->nombre
				)
			);
			return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	    //Metodo para registrar EMBUDOS
	public function Registrar(Embudo $data)
	{
		$sql ="INSERT INTO embudos VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idUsuario,
				$data->nombre,
				$data->perdido,
				$data->ganado,
				$data->logo
			)
		);

	}
		//Metodo para actualizar EMBUDOS
	public function Actualizar(Embudo $data)
	{
		$sql ="UPDATE embudos SET 
		nombre = ?
		WHERE idEmbudo = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombre,
				$data->idEmbudo
			)
		);
	}

		//Metodo para eliminar embudo
	public function Eliminar($idEmbudo)
	{
		$sql ="DELETE FROM embudos WHERE idEmbudo = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEmbudo
			)
		);
	}
	//Metodo para contar las etapas de cada embudo
	public function ContadorEtapas($idEmbudo)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT COUNT(*) FROM etapasventas WHERE etapasventas.idEmbudo = $idEmbudo");
			$stm->execute();

			return $cont=implode($stm->fetchAll(PDO::FETCH_COLUMN));
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para reordenar las etapas con el id de la etapa.
	function Reordenar($idEtapa, $orden) 
	{
		$sql ="UPDATE etapasventas SET 
		orden = $orden
		WHERE idEtapa = $idEtapa";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$orden->orden,
				$idEtapa->idEtapa
			)
		);
		if ($sql) return true;
		return false;
	}

	//Metodo para ordenar los negocios.
	function OrdenarNegocios($idEtapa,$contenido) 
	{
		
		$sql ="UPDATE etapasventas SET 
		contenido = '$contenido'
		WHERE idEtapa = $idEtapa";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$contenido,
				$idEtapa
			)
		); 
	}
	//Metodo para ordenar el negocio actualizado en el contenido de etapas.
	function NegocioContenido($idEtapa,$idNegocio) 
	{
		
		try
		{
			$stm = $this->pdo->prepare("SELECT contenido FROM etapasventas WHERE idEtapa=$idEtapa");
			$stm->execute();
			$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			if ($valor==null) {
				$Resultado=$idNegocio;
			}else{
				$Resultado=$valor.','.$idNegocio++;	
			}
			$sql = "UPDATE etapasventas  SET contenido = '$Resultado'
			WHERE idEtapa = $idEtapa";
			$this->pdo->prepare($sql)
			->execute();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para traer en forma de array los ids de los negocios del contenido de la etapa.
	function ArregloDeNegociosAntiguo($idEtapa) 
	{
		$stm = $this->pdo->prepare("SELECT contenido FROM etapasventas WHERE idEtapa=$idEtapa;");
		$stm->execute();
		$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $valor;
	}
	//Metodo para traer en forma de cadena los ids de los negocios del contenido de la etapa.
	function CadenaDeNegociosNuevo($idEtapa) 
	{
		$stm = $this->pdo->prepare("SELECT contenido FROM etapasventas WHERE idEtapa=$idEtapa;");
		$stm->execute();
		$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $valor;
	}
	//Metodo para reordenar los negocios 
	function ReordenarContenido($idEmbudo,$idEtapa,$contenido) 
	{
		$sql ="UPDATE etapasventas SET contenido = '$contenido' WHERE idEtapa = $idEtapa";
		$this->pdo->prepare($sql)
		->execute();

	}
	function CambiaLogo($idEmbudo, $logo){
		$sql ="UPDATE embudos SET 
		logo = ?
		WHERE idEmbudo = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$logo,
				$idEmbudo
			)
		);
	}

	function ObtenerLogo($idEmbudo){
		$sql= $this->pdo->prepare("SELECT logo FROM embudos WHERE idEmbudo = $idEmbudo");
		$resultado=$sql->execute();
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	}
} 
?>