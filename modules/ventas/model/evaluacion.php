<?php
class Evaluacion
{  
	private $idEvaluacion;
	private $fechaEvaluacion;
	private $idContacto;
	private $idOrganizacion;
	private $observaciones;
	private $promedio;
	private $pdo;

	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}
	public function ListarDatosEvaluacion($idEvaluacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM evaluacion e INNER JOIN personas p ON p.idCliente = e.idContacto INNER JOIN organizaciones o ON o.idOrganizacion = e.idOrganizacion WHERE idEvaluacion = ?");
		$stm->execute(array($idEvaluacion));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	public function ListarDatosAspectos()
	{
		$stm = $this->pdo->prepare("SELECT aspecto, valor from aspectos, evaluacion, aspectos_evaluacion where aspectos.idAspecto = aspectos_evaluacion.idAspecto and aspectos_evaluacion.idEvuacion = evaluacion.idEvaluacion");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ConsultaEvaluaciones($bTexto,$mes,$anio){

		if($bTexto!="" || $mes !="" || $anio != ""){
			$sql="SELECT * FROM evaluacion e INNER JOIN personas p ON p.idCliente = e.idContacto INNER JOIN organizaciones o ON o.idOrganizacion = e.idOrganizacion
				WHERE (p.nombrePersona like '%$bTexto%' OR o.nombreOrganizacion like '%$bTexto%' OR e.fechaEvaluacion like '%$bTexto%') ";
				
			if($mes!=""){
				$sql.=" AND MONTH(e.fechaEvaluacion) = $mes ";
			}
			if($anio !=""){
				$sql.=" AND YEAR(e.fechaEvaluacion) = $anio ";
			}
			
		}else{
			$sql="SELECT * FROM evaluacion e INNER JOIN personas p ON p.idCliente = e.idContacto INNER JOIN organizaciones o ON o.idOrganizacion = e.idOrganizacion";
		}
		
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

	public function ConsultaAspectosEvaluaciones($idEvaluacion){
		$stm=$this->pdo->prepare("SELECT aspecto,valor,observaciones FROM evaluacion e INNER JOIN aspectos_evaluacion ae ON e.idEvaluacion = ? AND ae.idEvuacion = e.idEvaluacion
				INNER JOIN aspectos a ON a.idAspecto = ae.idAspecto");
		$stm->execute(array($idEvaluacion));
		return $stm;
		
	}

	public function Registrar(Evaluacion $data)
	{
		$sql ="INSERT INTO evaluacion VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->getFechaEvaluacion(),
				$data->getIdContacto(),
				$data->getIdOrganizacion(),
				$data->getObservaciones(),
				$data->getPromedio()
			)
		);

		$sql="SELECT idEvaluacion FROM evaluacion e ORDER BY idEvaluacion DESC LIMIT 1";
		
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function Actualizar(Evaluacion $data)
	{
		$sql ="UPDATE evaluacion SET
			fechaEvaluacion = ?,
			idContacto = ?,
			idOrganizacion = ?,
			observaciones = ?,
			promedio = ?
			WHERE idEvaluacion = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->getFechaEvaluacion(),
				$data->getIdContacto(),
				$data->getIdOrganizacion(),
				$data->getObservaciones(),
				$data->getPromedio(),
				$data->getIdEvaluacion(),
			)
		);
	}

	public function RegistrarAspecto($valor,$idAspecto, $idEvaluacion){
		$sql ="INSERT INTO aspectos_evaluacion VALUES(?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idAspecto,
				$idEvaluacion,
				$valor,
			)
		);
	}

	public function ActualizarAspecto($valor,$idAspecto, $idEvaluacion){
		$sql ="UPDATE  aspectos_evaluacion SET
			valor = ?
			WHERE idEvuacion = ? AND idAspecto = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$valor,
				$idEvaluacion,
				$idAspecto,
				
			)
		);
	}

	public function Eliminar($idEvaluacion){
		
		$sql ="DELETE FROM evaluacion WHERE idEvaluacion=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEvaluacion
			)
		);
	}

	public function EliminarAspectos($idEvaluacion){
		
		$sql ="DELETE FROM aspectos_evaluacion WHERE idEvuacion=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEvaluacion
			)
		);
	}

	

	public function getIdEvaluacion(){
		return $this->idEvaluacion;
	}

	public function getFechaEvaluacion(){
		return $this->fechaEvaluacion;
	}

	public function getIdContacto(){
		return $this->idContacto;
	}

	public function getIdOrganizacion(){
		return $this->idOrganizacion;
	}

	public function getObservaciones(){
		return $this->observaciones;
	}

	public function getPromedio(){
		return $this->promedio;
	}

	public function setIdEvaluacion($idEva){
		$this->idEvaluacion= $idEva;
	}

	public function setFechaEvaluacion($FechaEva){
		$this->fechaEvaluacion = $FechaEva;
	}

	public function setIdContacto($idCont){
		$this->idContacto = $idCont;
	}

	public function setIdOrganizacion($idOrga){
		$this->idOrganizacion = $idOrga;
	}

	public function setObservaciones($observa){
		$this->observaciones = $observa;
	}

	public function setPromedio($promedio){
		$this->promedio = $promedio;
	}
}