<?php
class Costo
{    
	public $idCostoOperativo;
	public $fechaEmision;
	public $fechaEntrega;
	public $requisicion;
	public $idNegocio;
	public $idUsuario;
	public $costoOperativo;
	public $tiempoEntrega;
	public $condicionesPago;
	public $observaciones;
	public $idContacto;
	public $status;

	public $idSerCO;
	public $clave;
	public $descripcion;
	public $unidad;
	public $cantidad;
	public $precioUnitario;
	public $importe;

	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	//Metodo para listar las solicitudes de costos
	public function ListarSolicitudes($bTexto)
	{
		$stm = $this->pdo->prepare("SELECT  n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idCostoOperativo, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, pc.nombrePersona, og.nombreOrganizacion FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og WHERE co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion AND (tituloNegocio like '%$bTexto%' OR fechaEmision like '%$bTexto%' OR fechaEntrega like '%$bTexto%' OR requisicion like '%$bTexto%') ORDER BY co.idCostoOperativo DESC");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function RegistrarSolicitud(Costo $data)
	{
		$sql ="INSERT INTO costo_operativo VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->fechaEmision,
				$data->fechaEntrega,
				$data->requisicion,
				$data->idNegocio,
				null,
				null,
				"",
				"",
				"",
				$data->idContacto,
				"Pendiente"
			)
		);
		return $this->pdo->lastInsertId(); 
	}

	//Metodo para actualizar solicitud de costo operativo
	public function ActualizarSolicitud(Costo $data)
	{
		$sql ="UPDATE costo_operativo SET 
		fechaEmision=?,
		fechaEntrega=?,
		requisicion=?,
		idNegocio=?,
		idContacto=?
		WHERE idCostoOperativo=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->fechaEmision,
				$data->fechaEntrega,
				$data->requisicion,
				$data->idNegocio,
				$data->idContacto,
				$data->idCostoOperativo
			)
		);
	}

	//Metodo para eliminar solicitud
	public function Eliminar($idCostoOperativo)
	{
		$sql ="DELETE FROM costo_opetativo WHERE idCostoOperativo = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCostoOperativo
			)
		);
	}


	//Metodo para registrar los servicios que se van agregando al carrito de servicios
	public function AgregarServicio(Costo $data)
	{
		$sql ="INSERT INTO servicios_costo_operativo VALUES(?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idCostoOperativo,
				$data->clave,
				$data->descripcion,
				$data->unidad,
				$data->cantidad,
				$data->precioUnitario,
				$data->importe
			)
		);
	}

	public function ObtenerServicio($idSerCO)
	{
		$stm = $this->pdo
		->prepare("SELECT * FROM servicios_costo_operativo WHERE idSerCO = ?");
		$stm->execute(array($idSerCO));
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ActualizarServicio(Costo $data)
	{
		$sql ="UPDATE servicios_costo_operativo SET 
		clave=?,
		descripcion=?,
		unidad=?,
		cantidad=?,
		precioUnitario=?,	
		importe=?
		WHERE idSerCO=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->clave,
				$data->descripcion,
				$data->unidad,
				$data->cantidad,
				$data->precioUnitario,
				$data->importe,
				$data->idSerCO
			)
		);
	}

	//Metodo para eliminar solicitud
	public function EliminarServicio($idCostoOperativo, $idSerCO)
	{
		$sql ="DELETE FROM servicios_costo_operativo WHERE idCostoOperativo=? AND idSerCO=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCostoOperativo,
				$idSerCO
			)
		);
	}

	public function ConsultarProyectoClientePorNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT tituloNegocio, nombreOrganizacion FROM negocios AS n, organizaciones AS o WHERE n.idOrganizacion = o.idOrganizacion AND idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ConsultarContactosNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT idCliente, nombrePersona FROM negocios AS n, organizaciones AS o, personas AS p WHERE n.idOrganizacion = o.idOrganizacion AND n.idOrganizacion = o.idOrganizacion AND idNegocio = $idNegocio");
		$stm->execute();
		return $stm;
	}

		//Metodo para listar personas por negocio
	public function ListarPersonasPorNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas, negocios, pertenece  WHERE personas.idCliente = pertenece.idCliente AND pertenece.idNegocio = negocios.idNegocio AND negocios.idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarCarrito($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM servicios_costo_operativo AS sco, costo_operativo AS co WHERE sco.idCostoOperativo=co.idCostoOperativo AND co.idCostoOperativo = ? order by sco.clave ASC");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ConsultaDatos($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM costo_operativo, negocios, organizaciones, usuarios WHERE idCostoOperativo = ? AND costo_operativo.idNegocio=negocios.idNegocio AND costo_operativo.idContacto=organizaciones.idOrganizacion AND costo_operativo.idUsuario=usuarios.idUsuario");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar las solicitudes de costos
	public function ListarDatosA($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT  n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, co.tiempoEntrega, co.condicionesPago, co.observaciones, pc.nombrePersona, og.nombreOrganizacion FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og WHERE co.idCostoOperativo = ? AND co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}
	//Metodo para listar las solicitudes de costos
	public function ListarDatosCosto($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT  n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, co.tiempoEntrega, co.condicionesPago, co.observaciones, pc.nombrePersona, og.nombreOrganizacion, og.direccion, og.localidadEstado, us.nombreUsuario FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og, usuarios as us WHERE co.idCostoOperativo = ? AND co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion AND co.idUsuario=us.idUsuario");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	//Metodo para listar los servicios de cada uno de los costos operativos
	public function ListarSer($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT  n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, pc.nombrePersona, og.nombreOrganizacion FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og WHERE co.idCostoOperativo = ? AND co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function ListarPendiente($bTexto)
	{
		$stm = $this->pdo->prepare("SELECT n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idCostoOperativo, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, pc.nombrePersona, og.nombreOrganizacion FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og WHERE co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion AND (tituloNegocio like '%$bTexto%' OR fechaEmision like '%$bTexto%' OR fechaEntrega like '%$bTexto%' OR requisicion like '%$bTexto%') AND (estado = 'Pendiente' or estado = 'Atendido')");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function ListarRechazado($bTexto)
	{
		$stm = $this->pdo->prepare("SELECT n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idCostoOperativo, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, pc.nombrePersona, og.nombreOrganizacion FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og WHERE co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion AND (tituloNegocio like '%$bTexto%' OR fechaEmision like '%$bTexto%' OR fechaEntrega like '%$bTexto%' OR requisicion like '%$bTexto%') AND estado = 'Rechazado'");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function ListarAprobado($bTexto)
	{
		$stm = $this->pdo->prepare("SELECT n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, co.idCostoOperativo, co.idNegocio, co.idContacto, co.fechaEmision, co.fechaEntrega, co.costoOperativo, co.requisicion, co.estado, pc.nombrePersona, og.nombreOrganizacion FROM costo_operativo as co, negocios as n, personas as pc, organizaciones as og WHERE co.idNegocio = n.idNegocio AND n.idCliente = pc.idCliente AND n.idOrganizacion = og.idOrganizacion AND (tituloNegocio like '%$bTexto%' OR fechaEmision like '%$bTexto%' OR fechaEntrega like '%$bTexto%' OR requisicion like '%$bTexto%') AND estado = 'Aprobado'");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function servicios($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT sc.idSerCO, sc.clave, sc.descripcion, sc.unidad, sc.cantidad FROM costo_operativo as co, servicios_costo_operativo as sc WHERE co.idCostoOperativo = sc.idCostoOperativo AND sc.idCostoOperativo = $idCostoOperativo");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function consultaServicio($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM  servicios_costo_operativo  WHERE idCostoOperativo = $idCostoOperativo");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

		public function ListarNivelTresOperaciones()
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios u, empleados e, puestos p, modulos m WHERE u.idEmpleado = e.idEmpleado AND  e.idPuesto = p.idPuesto AND p.idModulo = m.idModulo AND p.idNivel = 3 AND m.idModulo=2");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function consultaServicio2($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM  servicios_costo_operativo  WHERE idCostoOperativo = ?");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}
	public function ObtenerUsuario()
	{
		$stm = $this->pdo
		->prepare("SELECT nombreUsuario FROM usuarios, costo_operativo WHERE costo_operativo.idUsuario = usuarios.idUsuario");
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	}

}