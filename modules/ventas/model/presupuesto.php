<?php
class Presupuesto
{    
	public $idPresupuestoGeneral;
	public $nombrePresupuesto;
	public $periodo;
	public $totalAnual;
	public $idUsuario;
	public $idPresupuesto;
	public $idNegocio;
	public $descripcion;
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Metodo para listar datos
	public function Listar($idUsuario, $idEmbudo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM presupuesto_general, usuarios WHERE presupuesto_general.idUsuario = usuarios.idUsuario AND presupuesto_general.idUsuario = $idUsuario AND presupuesto_general.idEmbudo = $idEmbudo ORDER BY periodo ASC");
		$stm->execute();			
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//metodo para verificar si ya existe un presupuesto en un determinado periodo
	public function verificar($periodo, $idUsuario, $idEmbudo){
		$stm = $this->pdo->prepare("SELECT * FROM presupuesto_general WHERE presupuesto_general.periodo = $periodo AND presupuesto_general.idUsuario = $idUsuario AND presupuesto_general.idEmbudo = $idEmbudo");
		$stm->execute();
		return $stm;
	}
	//metodo para verificar si ya existe un presupuesto en un determinado periodo
	public function consulta($idPresupuestoGeneral){
		$idEmbudo = $_SESSION['idEmbudo'];
		$stm = $this->pdo->prepare("SELECT * FROM presupuesto_general WHERE presupuesto_general.idPresupuestoGeneral = $idPresupuestoGeneral AND presupuesto_general.idEmbudo = $idEmbudo");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para registrar presupuestos generales
	public function Registrar(Presupuesto $data)
	{
		$sql ="INSERT INTO presupuesto_general VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->nombrePresupuesto,
				$data->periodo,
				$data->totalAnual,
				$data->idUsuario,
				$data->idEmbudo
			)
		);
	}

		//Metodo para registrar NEGOCIOS AL presupuestos 
	public function RegistrarPN(Presupuesto $data)
	{
		$sql ="INSERT INTO presupuestos VALUES(?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idPresupuestoGeneral,
				$data->idNegocio,
				$data->descripcion,
				$data->totalAnual
			)
		);
	}

	public function Actualizar(Presupuesto $data)
	{
		$sql = "UPDATE  presupuesto_general SET
		nombrePresupuesto = ?,
		periodo = ?
		WHERE  idPresupuestoGeneral = ? ";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->nombrePresupuesto,
				$data->periodo,
				$data->idPresupuestoGeneral	
			)
		);
	}

	public function ActualizarPN(Presupuesto $data)
	{
		$sql = "UPDATE  presupuestos SET
		idNegocio = ?,
		descripcion = ?
		WHERE  idPresupuesto= ? ";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idNegocio,
				$data->descripcion,
				$data->idPresupuesto	
			)
		);
	}


	public function Eliminar($idPG)
	{
		$sql ="DELETE FROM presupuesto_general WHERE idPresupuestoGeneral = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idPG
			)
		);
	}
	

	public function EliminarPN($idPN)
	{
		$sql ="DELETE FROM presupuestos WHERE idPresupuesto = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idPN
			)
		);
	}
		//Metodo para listar los embudos
	public function ListarEmbudos()
	{
		$stm = $this->pdo->prepare("SELECT * FROM embudos");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

			//Metodo para listar los negocios
	public function ListarNegocios($idEmbudo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios WHERE negocios.idEmbudo = $idEmbudo AND negocios.estimacion <> 1");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

				//Metodo para listar los presupuestos
	public function listarPresupuestos($idEmbudo, $idPresupuestoGeneral, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT presupuestos.idPresupuesto, usuarios.nombreUsuario, organizaciones.nombreOrganizacion, negocios.idNegocio, negocios.tituloNegocio, negocios.claveServicio, equipo.nombreEquipo, presupuestos.descripcion, presupuesto_general.periodo, presupuestos.totalAnual FROM presupuestos, presupuesto_general, negocios, equipo, usuarios, organizaciones WHERE presupuestos.idPresupuestoGeneral = presupuesto_general.idPresupuestoGeneral AND presupuestos.idNegocio = negocios.idNegocio AND negocios.idEquipo = equipo.idEquipo AND presupuesto_general.idUsuario = usuarios.idUsuario AND negocios.idOrganizacion = organizaciones.idOrganizacion AND presupuesto_general.idEmbudo = $idEmbudo AND presupuesto_general.idPresupuestoGeneral = $idPresupuestoGeneral AND presupuesto_general.idUsuario = $idUsuario");

		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function listarPresupuestosFiltro($idEmbudo, $idPresupuestoGeneral, $bTexto, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT presupuestos.idPresupuesto, usuarios.nombreUsuario, organizaciones.nombreOrganizacion, negocios.idNegocio, negocios.tituloNegocio, negocios.claveServicio, equipo.nombreEquipo, presupuestos.descripcion, presupuesto_general.periodo, presupuestos.totalAnual FROM presupuestos, presupuesto_general, negocios, equipo, usuarios, organizaciones WHERE presupuestos.idPresupuestoGeneral = presupuesto_general.idPresupuestoGeneral AND presupuestos.idNegocio = negocios.idNegocio AND negocios.idEquipo = equipo.idEquipo AND presupuesto_general.idUsuario = usuarios.idUsuario AND negocios.idOrganizacion = organizaciones.idOrganizacion AND presupuesto_general.idEmbudo = $idEmbudo AND presupuesto_general.idPresupuestoGeneral = $idPresupuestoGeneral AND presupuesto_general.idUsuario = $idUsuario  AND (nombreUsuario like '%$bTexto%' OR nombreOrganizacion like '%$bTexto%' OR claveServicio like '%$bTexto%' OR nombreEquipo  like '%$bTexto%' OR descripcion like '%$bTexto%' OR tituloNegocio like '%$bTexto%') ");

		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

				//Metodo para listar los negocios
	public function listarDatosNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT negocios.claveServicio, equipo.nombreEquipo, organizaciones.nombreOrganizacion FROM negocios, equipo, organizaciones WHERE negocios.idEquipo = equipo.idEquipo AND negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = ? ");
		$stm->execute(array($idNegocio));

		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	//Metodo para listar el periodo de los presupuestos
	public function ListarPeriodo($idEmbudo, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idPresupuestoGeneral, periodo FROM presupuesto_general as pg, usuarios as us WHERE pg.idUsuario = us.idUsuario AND pg.idEmbudo = ? AND pg.idUsuario = ? ORDER BY pg.periodo ASC");
		$stm->execute(array($idEmbudo, $idUsuario));

		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function ActualizarMes($id,$value,$idPresupuesto){
		$sql = "UPDATE  presupuestos_meses SET
		totalMensual = $value
		WHERE  idPresupuesto = $idPresupuesto AND idMes = $id";
		$this->pdo->prepare($sql)
		->execute(
			array()
		);
	}

	public function ConsultaTotalesMensuales($idPresupuesto)
	{
		$stm = $this->pdo->prepare("SELECT * FROM presupuestos as p, presupuestos_meses as pm, meses as m WHERE p.idPresupuesto=pm.idPresupuesto AND pm.idMes=m.idMes AND pm.idPresupuesto=?");
		$stm->execute(array($idPresupuesto));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function listarDatosPG($idPresupuestoGeneral){
		$stm = $this->pdo->prepare("SELECT nombrePresupuesto FROM presupuesto_general WHERE idPresupuestoGeneral = ? ");
		$stm->execute(array($idPresupuestoGeneral));

		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function ObtenerValorMaximo($idEmbudo, $idPresupuestoGeneral){
		$stm = $this->pdo->prepare("SELECT MAX(presupuestos.totalAnual) AS 'maximo' FROM presupuestos, presupuesto_general WHERE presupuestos.idPresupuestoGeneral = presupuesto_general.idPresupuestoGeneral AND presupuesto_general.idEmbudo = ? AND presupuesto_general.idPresupuestoGeneral = ?");
		$stm->execute(array($idEmbudo, $idPresupuestoGeneral));
		$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $valor;
	}

	public function VerificarPresupuesto($periodo, $idUsuario, $idPresupuestoGeneral)
	{
		if ($idPresupuestoGeneral == "")
			$sql = $this->pdo->prepare("SELECT * FROM presupuesto_general WHERE periodo = $periodo AND idUsuario = $idUsuario");
		else
			$sql = $this->pdo->prepare("SELECT * FROM presupuesto_general WHERE periodo = $periodo AND idUsuario = $idUsuario AND idPresupuestoGeneral <> $idPresupuestoGeneral");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;		
	}

	public function obtenerClave($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, fechaCierre, valorNegocio, tipoMoneda FROM negocios WHERE idNegocio = ? ");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);	
	}

	public function obtenerEstimaciones($claveOrganizacion, $claveConsecutivo, $claveServicio)
	{
		$stm = $this->pdo->prepare("SELECT fechaCierre, valorNegocio, tipoMoneda FROM negocios WHERE claveOrganizacion  = ? AND claveConsecutivo = ? AND claveServicio = ? AND estimacion <> 0");
		$stm->execute(array($claveOrganizacion, $claveConsecutivo, $claveServicio));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

}//fin de la clase
?>