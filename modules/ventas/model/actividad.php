<?php
class Actividad
{    
	public $idActividad;
	public $nombreActividad;
	public $idUsuario;
	public $idNegocio;
	public $idOrganizacion;
	public $tipo;
	public $duracion;
	public $notas;
	public $fechaCompletado;
	public $fechaActividad;
	public $fechaFin;
	public $horaInicio;
	public $horaFin;
	public $timestamp;
	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	//Metodo para listar todos los negocios para el select
	public function ListarNegocios()
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para listar todos las organizaciones para el select
	public function ListarOrganizaciones()
	{
		$stm = $this->pdo->prepare("SELECT * FROM organizaciones");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarPersonas()
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ConsultaActividades($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios u, negocios n, organizaciones o WHERE a.idUsuario = u.idUsuario AND a.idNegocio=n.idNegocio AND o.idOrganizacion = a.idOrganizacion AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario ORDER BY idActividad DESC;";
			break;

			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios u, organizaciones o WHERE a.idUsuario = u.idUsuario AND o.idOrganizacion = a.idOrganizacion AND tipo like '%$bActividad%' AND a.idNegocio is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario ORDER BY idActividad DESC;";
			break;

			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios u WHERE a.idUsuario = u.idUsuario  AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario ORDER BY idActividad DESC;";
			break;
		}
		
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

	//Metodo para listar las actividades
	public function ListarPlaneado($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=0 AND fechaActividad > CURDATE() AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=0 AND a.idNegocio is null AND fechaActividad > CURDATE() AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND a.completado=0 AND fechaActividad > CURDATE() AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}


	//Metodo para listar las actividades
	public function ListarVencido($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=0 AND  fechaActividad < CURDATE() AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=0 AND  fechaActividad < CURDATE() AND a.idNegocio is null AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND a.completado=0 AND  fechaActividad < CURDATE() AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

	//Metodo para listar las actividades completadas
	public function ListarCompletado($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=1 AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=1 AND tipo like '%$bActividad%' AND a.idNegocio is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND a.completado=1 AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

	//Metodo para listar las actividades de hoy
	public function ListarHoy($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND fechaActividad=CURDATE() AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario;";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND fechaActividad=CURDATE() AND tipo like '%$bActividad%' AND a.idNegocio is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario;";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND fechaActividad=CURDATE() AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario;";
			break;	
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

		//Metodo para listar las actividades de mañana
	public function ListarTomorrow($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND fechaActividad=CURDATE() + INTERVAL 1 DAY AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND fechaActividad=CURDATE() + INTERVAL 1 DAY AND tipo like '%$bActividad%' AND a.idNegocio is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND fechaActividad=CURDATE() + INTERVAL 1 DAY AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;	
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

		//Metodo para listar las actividades de esta semana
	public function ListarEstaSemana($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND YEARweek(a.fechaActividad) = YEARweek(CURRENT_date) AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario;";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND YEARweek(a.fechaActividad) = YEARweek(CURRENT_date) AND tipo like '%$bActividad%' AND a.idNegocio is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario;";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND YEARweek(a.fechaActividad) = YEARweek(CURRENT_date) AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario;";
			break;	
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

	//Metodo para listar las actividades de la proxima semana
	public function ListarProximaSemana($bTexto, $bActividad, $tAct)
	{
		$idUsuario=$_SESSION['idUsuario'];
		switch ($tAct) {
			case 'completas':
			$sql="SELECT * FROM actividades a, usuarios, negocios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND YEARweek(a.fechaActividad) = YEARweek(CURRENT_date + INTERVAL 7 DAY) AND tipo like '%$bActividad%' AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR tituloNegocio LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'leads':
			$sql="SELECT * FROM actividades a, usuarios, organizaciones WHERE a.idUsuario = usuarios.idUsuario AND organizaciones.idOrganizacion = a.idOrganizacion AND YEARweek(a.fechaActividad) = YEARweek(CURRENT_date + INTERVAL 7 DAY) AND tipo like '%$bActividad%' AND a.idNegocio is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR nombreOrganizacion LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%' OR nombrePersona LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;
			case 'internas':
			$sql="SELECT * FROM actividades a, usuarios WHERE a.idUsuario = usuarios.idUsuario AND YEARweek(a.fechaActividad) = YEARweek(CURRENT_date + INTERVAL 7 DAY) AND tipo like '%$bActividad%' AND a.idNegocio is null and a.idOrganizacion is null AND (tipo LIKE '%$bTexto%' OR nombreUsuario LIKE '%$bTexto%' OR notas LIKE '%$bTexto%' OR fechaActividad LIKE '%$bTexto%') AND a.idUsuario=$idUsuario";
			break;	
		}
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		return $stm;
	}

	//Metodo para validar que tiempo de una actividad no se empalme con otra
	public function ValidarActividad($fechaActividad, $horaInicio, $horaFin)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades a WHERE a.fechaActividad = ? AND idUsuario = ? AND ((? BETWEEN a.horaInicio AND a.horaFin) OR (? BETWEEN a.horaInicio AND a.horaFin) OR (a.horaInicio BETWEEN ? AND ?) OR (a.horaFin BETWEEN ? AND ?));");
		$stm->execute(array(
			$fechaActividad, 
			$_SESSION['idUsuario'], 
			$horaInicio, 
			$horaFin,
			$horaInicio, 
			$horaFin,
			$horaInicio, 
			$horaFin));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

    //Metodo para registrar actividades
	public function Registrar(Actividad $data)
	{
		$sql ="INSERT INTO actividades VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idUsuario,
				$data->idNegocio,
				$data->idOrganizacion,
				$data->tipo,
				$data->notas,
				$data->duracion,
				0,
				$data->fechaActividad,
				$data->horaInicio,
				$data->horaFin,
				$data->fechaCompletado,
				$data->nombrePersona,
				$data->timestamp
			)
		);
	}

	//Metodo para actualizar actividades
	public function Actualizar(Actividad $data)
	{
		$sql ="UPDATE actividades SET 
		idNegocio=?,
		idOrganizacion=?,
		tipo=?,
		notas=?,
		duracion=?,	
		fechaActividad=?,
		horaInicio=?,
		horaFin=?,  
		nombrePersona=?
		WHERE idActividad=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idNegocio,
				$data->idOrganizacion,
				$data->tipo,
				$data->notas,
				$data->duracion,
				$data->fechaActividad,
				$data->horaInicio,
				$data->horaFin,
				$data->nombrePersona,
				$data->idActividad
			)
		);
	}

	//Metodo para actualizar completa de actividad
	public function CambiaEstado($idActividad, $fechaCompletado, $completo)
	{
		$sql ="UPDATE actividades SET completado=?, fechaCompletado=? WHERE idActividad=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$completo,
				$fechaCompletado,
				$idActividad
			)
		);
	}

	//Metodo para eliminar actividades
	public function Eliminar($idActividad)
	{
		$sql ="DELETE FROM actividades WHERE idActividad=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idActividad
			)
		);
	}

	//Metodo para listar personas por negocio
	public function ListarPersonasPorNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT personas.nombrePersona, personas.idCliente FROM personas, negocios, pertenece  WHERE personas.idCliente = pertenece.idCliente AND pertenece.idNegocio = negocios.idNegocio AND negocios.idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ListarParticipantesPorNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT personas.idCliente, personas.nombrePersona FROM participantes, personas WHERE participantes.idCliente = personas.idCliente AND participantes.idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para listar personas por organizacion
	public function ListarPersonasPorOrganizacion($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT personas.nombrePersona FROM personas, organizaciones  WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idOrganizacion = ?");
		$stm->execute(array($idOrganizacion));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

   //Metodo para listar organizaciones por negocio
	public function ObtenerOrganizacionPorNegocio($idNegocio)
	{
		$stm = $this->pdo
		->prepare("SELECT * FROM organizaciones, negocios  WHERE organizaciones.idOrganizacion = negocios.idOrganizacion AND negocios.idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
	} 

	//Metodo pra consultar el nomre del negocio con su id
	public function ConsultaNombreOrganizacion($idOrganizacion)
	{
		$sql= $this->pdo->prepare("SELECT nombreOrganizacion FROM organizaciones where idOrganizacion=?");
		$resultado=$sql->execute(array($idOrganizacion));
		return $sql->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
	} 

   //Metodo para listar organizaciones por negocio
	public function ListarNegociosPorOrganizacion($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT negocios.tituloNegocio, negocios.idNegocio FROM organizaciones, negocios  WHERE organizaciones.idOrganizacion = negocios.idOrganizacion AND organizaciones.idOrganizacion = ?");

		$stm->execute(array($idOrganizacion));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}


	//Metodo para exportar actividades entre intervalos de fechas y periodo
	public function ExportarPorFechaCompleta($fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades a, negocios, organizaciones, usuarios WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=1 AND a.fechaActividad BETWEEN ? AND ?");
		$stm->execute(array($fechaInicio, $fechaFin));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	public function ExportarPorFechaVencida($fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades a, negocios, organizaciones, usuarios WHERE a.idUsuario = usuarios.idUsuario AND a.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = a.idOrganizacion AND a.completado=0 AND fechaActividad < CURDATE() AND a.fechaActividad BETWEEN ? AND ?");
		$stm->execute(array($fechaInicio, $fechaFin));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
}