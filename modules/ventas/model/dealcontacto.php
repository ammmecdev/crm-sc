<?php 
class DealContacto{
	public $idCliente;
	public $status;
	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}
	//Metodo para eliminar las notas del negocio
	public function EliminarArchivo($idContenido)
	{
		$stm = $this->pdo->prepare("DELETE FROM contenidos WHERE idContenido=?");
		$stm->execute(array($idContenido
		));
	}
	//Metodo para consultar el valor del dolar desde la tabla de empresa
	public function ConsultaValorDolar()
	{
		$stm = $this->pdo->prepare("SELECT valorDolar FROM empresa WHERE idEmpresas = 1 ");
		$stm->execute(); 
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}
	//Metodo para obtener el conatdor de actividades
	public function ContadorActividades($nombrePersona)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM actividades WHERE nombrePersona = '$nombrePersona';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}

	//Metodo para obtener el conatdor de notas
	public function ContadorNotas($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM contenidos WHERE idCliente = ?");
		$stm->execute(array($idCliente));
		return implode($stm->fetchAll(PDO::FETCH_COLUMN));
	}

	//Metodo para obtener el conatdor de negocios
	public function ContadorNegocios($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE idCliente = ?");
		$stm->execute(array($idCliente));
		return implode($stm->fetchAll(PDO::FETCH_COLUMN));
	}

	//Metodo para desvincular usuarios del negocio
	public function EliminarSeguidores($idCliente,$idUsuario)
	{
		$stm = $this->pdo->prepare("DELETE FROM segidorescontacto WHERE idCliente=? and idUsuario=?");
		$stm->execute(array($idCliente,
			$idUsuario));
	}
	//Metodo parar traer la informacion de los usuarios
	public function ConsultaUsuarios($idUsuario)
	{
		if ($idUsuario==null) {
			return 0;
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario=$idUsuario;");
			$stm->execute();
			$A=$stm->fetchAll();
			return $A[0];
		}
	}
	//Metodo para consultar valores en la vista detalles chava
	public function Detalles($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT p.idCliente, p.honorifico, p.nombrePersona, p.idOrganizacion, p.telefono as telefonoPersona, p.extension, p.tipoTelefono as tipoTelefonoPersona, p.email, p.puesto, o.nombreOrganizacion, o.clave, o.telefono, o.extensionTelefonica, o.tipoTelefono, o.direccion, o.localidadEstado, o.rfc, o.paginaWeb, u.idUsuario, u.nombreUsuario, u.foto  FROM personas as p,organizaciones as o,usuarios as u WHERE p.idOrganizacion=o.idOrganizacion and p.idUsuario=u.idUsuario and p.idCliente = ?");
		$stm->execute(array($idCliente));
		$Resultado=$stm->fetchAll();
		return $Resultado[0];
	}
	 //Metodo Para agregar segidores a un negocio
	public function AgregarSeguidores($idCliente,$idUsuario)
	{
		$sql ="INSERT INTO segidorescontacto VALUES(?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCliente,
				$idUsuario
			)
		);
	}
	//Metodo para obtener el idUsuario de un negocio
	public function ObtnenIdUsuario($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario from segidorescontacto WHERE idCliente = '$idCliente';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
		//Metodo para tarer los negocios de una organaizacion
	public function ListarNegocios($idCliente)
	{

		$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario  and  negocios.idCliente=? ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo;
			");
		$stm->execute(array($idCliente));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	public function ListarNegociosAbiertos($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and negocios.idUsuario=usuarios.idUsuario and personas.idCliente=? and negocios.status=0");

		$stm->execute(array($idCliente));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar negocios abiertos por organizacion
	public function ListarNegociosGanados($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and negocios.idUsuario=usuarios.idUsuario and personas.idCliente=? and negocios.status=1");

		$stm->execute(array($idCliente));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar negocios abiertos por organizacion
	public function ListarNegociosPerdidos($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and negocios.idUsuario=usuarios.idUsuario and personas.idCliente=? and negocios.status=2");

		$stm->execute(array($idCliente));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para buscar el valor de los negocios cerrados
	public function ConsultaValorNegociosCerrados($idCliente,$valor)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio,tipoMoneda FROM negocios WHERE idCliente=? and status=?");
		$stm->execute(array($idCliente,$valor));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para registrar notas y archivos
	public function RegistrarNota($idUsuario,$idCliente,$notas,$FechaAc,$nombre,$size)
	{
		$sql ="INSERT INTO contenidos VALUES(?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idUsuario,
				$idCliente,
				null,
				null,
				$notas,
				$nombre,
				$size,
				$FechaAc
			)
		);
	}
	//Metodo para tarer el numero de personal de un contacto
	public function ContadorSeguidores($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT ((SELECT COUNT(*) FROM personas WHERE idCliente=$idCliente) + (SELECT COUNT(*) FROM segidorescontacto WHERE idCliente=$idCliente)) as total;");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}
	//Metodo para traer un arreglo de los usuarios que tengan actividades registradas
	public function ArregloDeSeguidoresAct($nombrePersona,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario FROM actividades WHERE nombrePersona='$nombrePersona' and idUsuario !='$idUsuario';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	//Metodo para obtener el los seguidores de un negocio
	public function ObtnenSeguidores($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * from usuarios WHERE idUsuario = '$idUsuario';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0];
	}
	//Metodo para obtener el numero de actividades echas por cada usuario
	public function ActividadesUsuarios($nombrePersona,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM actividades WHERE nombrePersona = '$nombrePersona' and idUsuario=$idUsuario;");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para listar los usuarios para el select
	public function ListarUsuarios($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario !=? ");
		$stm->execute(array(
			$idUsuario));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para traer un arreglo de los usuarios vinculadas a dicho contacto
	public function ArregloDeSeguidores($idCliente,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario FROM segidorescontacto WHERE idCliente=? and idUsuario != ?;");
		$stm->execute(array($idCliente, $idUsuario));
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	//Metodo para obtener las actividades
	public function Actividades($nombrePersona)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades, usuarios, negocios, organizaciones WHERE actividades.idUsuario = usuarios.idUsuario AND actividades.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = actividades.idOrganizacion AND actividades.nombrePersona='$nombrePersona' ORDER BY actividades.idActividad DESC;");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ObtenerActividad($idActividad)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades as a, negocios as n, organizaciones as o, usuarios as u  WHERE a.idNegocio = n.idNegocio AND a.idUsuario = u.idUsuario AND a.idOrganizacion = o.idOrganizacion AND a.idActividad = ? ORDER BY a.idActividad DESC");
		$stm->execute(array($idActividad));
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ObtenerNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario  and  negocios.idNegocio=? ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo;
			");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	//Metodo para obtener las notas de un cliente
	public function ContenidosNotas($idContenido)
	{
		$stm = $this->pdo->prepare("SELECT * from contenidos, usuarios WHERE  contenidos.idUsuario=usuarios.idUsuario and contenidos.idContenido=?;");
		$stm->execute(array($idContenido));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	//Metodo para trer el nombre del mes
	public function NombreMes($idMes)
	{
		$stm = $this->pdo->prepare("SELECT mes FROM meses WHERE idMes = ?");
		$stm->execute(array($idMes));

		$A=$stm->fetchAll();
		return $A[0][0];
	}

	public function ObtnenPersonas($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * from personas WHERE idCliente = '$idCliente';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0];
	}

	public function ObtenerInfoBitacora($tabla, $campo ,$id, $res)
	{
		$query="SELECT " . $res . " FROM " . $tabla . " WHERE ". $campo . " = " . $id;
		$stm = $this->pdo->prepare($query);
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ListarBitacoras()
	{
		$stm = $this->pdo->prepare("SELECT * FROM bitacoras  WHERE accion='Insert' ORDER BY timestamp;");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

}
?>
