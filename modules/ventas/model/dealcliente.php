<?php 
class DealCliente
{
	public $idOrganizacion;
	public $status;
	private $pdo;
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para eliminar las notas del negocio
	public function EliminarArchivo($idContenido)
	{
		$stm = $this->pdo->prepare("DELETE FROM contenidos WHERE idContenido=?");
		$stm->execute(array($idContenido
		));
	}
//Metodo para consultar valores en la vista detalles chava
	public function Detalles($idOrganizacion)
	{

		$stm = $this->pdo->prepare("SELECT * FROM organizaciones, usuarios WHERE organizaciones.idUsuario=usuarios.idUsuario and organizaciones.idOrganizacion = '$idOrganizacion'");
		$stm->execute();
		$Resultado=$stm->fetchAll();
		return $Resultado[0];
	}

	//Metodo para agregar notas a la organizacion
		//Metodo para agregar notas al negocio
	public function RegistrarNota($idUsuario,$idOrganizacion,$notas,$FechaAc,$nombre,$size)
	{
		$sql ="INSERT INTO contenidos VALUES(?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idUsuario,
				null,
				$idOrganizacion,
				null,
				$notas,
				$nombre,
				$size,
				$FechaAc
			)
		);
	}
	//Metodo para obtener el idPersonas de una organizacion
	public function ObtnenIdPersonas($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT idCliente from personas WHERE idOrganizacion = '$idOrganizacion';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	public function ObtnenPersonas($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * from personas WHERE idCliente = '$idCliente';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0];
	}
	//Metodo para obtener el conatdor de actividades
	public function ContadorActividades($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) as contador FROM actividades as a, negocios as n, organizaciones as o  WHERE a.idNegocio = n.idNegocio AND a.idOrganizacion = o.idOrganizacion AND a.idOrganizacion = ?");
		$stm->execute(array($idOrganizacion));
		return implode($stm->fetchAll(PDO::FETCH_COLUMN));
	}

	//Metodo para obtener el conatdor de notas
	public function ContadorNotas($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM contenidos WHERE idOrganizacion = ?");
		$stm->execute(array($idOrganizacion));
		return implode($stm->fetchAll(PDO::FETCH_COLUMN));
	}

	//Metodo para obtener el conatdor de negocios
	public function ContadorNegocios($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE idOrganizacion = ?");
		$stm->execute(array($idOrganizacion));
		return implode($stm->fetchAll(PDO::FETCH_COLUMN));
	}

	//Metodo para listar personas por organizacion
	public function ListarPersonasPorOrganizacion($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT personas.nombrePersona FROM personas, organizaciones  WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idOrganizacion = ?");

		$stm->execute(array($idOrganizacion));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	// Metodo para listar personas por organizacion
	public function ListarPersonas($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT p.idCliente, p.idOrganizacion, p.honorifico, p.nombrePersona, p.extension, p.telefono, p.tipoTelefono, p.email, o.nombreOrganizacion, p.puesto, u.nombreUsuario FROM personas as p, organizaciones as o, usuarios as u WHERE p.idOrganizacion = o.idOrganizacion AND p.idUsuario = u.idUsuario AND p.idOrganizacion = ?");
		$stm->execute(array($idOrganizacion));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar negocios abiertos por organizacion
	public function ListarNegociosAbiertos($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and negocios.idUsuario=usuarios.idUsuario and organizaciones.idOrganizacion=? and negocios.status=0");
		$stm->execute(array($idOrganizacion));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar negocios abiertos por organizacion
	public function ListarNegociosGanados($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and negocios.idUsuario=usuarios.idUsuario and organizaciones.idOrganizacion=? and negocios.status=1");

		$stm->execute(array($idOrganizacion));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para consultar el valor del dolar desde la tabla de empresa
	public function ConsultaValorDolar()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT valorDolar FROM empresa WHERE idEmpresas = 1 ");
			$stm->execute(); 

			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;	
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
		//Metodo para listar negocios abiertos por organizacion
	public function ListarNegociosPerdidos($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and negocios.idUsuario=usuarios.idUsuario and organizaciones.idOrganizacion=? and negocios.status=2");

		$stm->execute(array($idOrganizacion));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para buscar el valor de los negocios cerrados
	public function ConsultaValorNegociosCerrados($idOrganizacion,$valor)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda FROM negocios WHERE idOrganizacion=? and status=?");
		$stm->execute(array($idOrganizacion,$valor));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para tarer los negocios de una organaizacion
	public function ListarNegocios($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario  and  negocios.idOrganizacion=? ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo;
			");
		$stm->execute(array($idOrganizacion));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para tarer los negocios de una organaizacion
	public function Negocios($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario  and  negocios.idNegocio=? ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo;
			");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	//Metodo para obtener las actividades
	public function Actividades($idActividad)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades as a, negocios as n, organizaciones as o, usuarios as u  WHERE a.idNegocio = n.idNegocio AND a.idUsuario = u.idUsuario AND a.idOrganizacion = o.idOrganizacion AND a.idActividad = ? ORDER BY a.idActividad DESC");
		$stm->execute(array($idActividad));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	public function DetallesActividades($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades as a, negocios as n, organizaciones as o, usuarios as u  WHERE a.idNegocio = n.idNegocio AND a.idUsuario = u.idUsuario AND a.idOrganizacion = o.idOrganizacion AND a.idOrganizacion = ? ORDER BY a.idActividad DESC");
		$stm->execute(array($idOrganizacion));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para tarer el numero de personal de un cliente
	public function ContadorSeguidores($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT ((SELECT COUNT(*) FROM organizaciones WHERE idOrganizacion=$idOrganizacion) + (SELECT COUNT(*) FROM segidorescliente WHERE idOrganizacion=$idOrganizacion)) as total;");
		$stm->execute();
		$cont = $stm->fetch(PDO::FETCH_COLUMN);
		return $cont;	
	}
	//Metodo para traer un arreglo de los usuarios que tengan actividades registradas
	public function ArregloDeSeguidoresAct($idOrganizacion, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario FROM actividades WHERE idOrganizacion = ? AND idUsuario != ?");
		$stm->execute(array($idOrganizacion, $idUsuario));
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	//Metodo para traer un arreglo de los usuarios vinculadas a dicho contacto
	public function ArregloDeSeguidores($idOrganizacion,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario FROM segidorescliente WHERE idOrganizacion='$idOrganizacion' and idUsuario !='$idUsuario';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	 //Metodo Para agregar segidores a un negocio
	public function AgregarSeguidores($idOrganizacion,$idUsuario)
	{
		$sql ="INSERT INTO segidorescliente VALUES(?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idOrganizacion,
				$idUsuario
			)
		);
	}
	//Metodo para desvincular usuarios del negocio
	public function EliminarSeguidores($idOrganizacion,$idUsuario)
	{
		$stm = $this->pdo->prepare("DELETE FROM segidorescliente WHERE idOrganizacion=? and idUsuario=?");
		$stm->execute(array($idOrganizacion,
			$idUsuario));
	}
		//Metodo parar traer la informacion de los usuarios
	public function ConsultaUsuarios($idUsuario)
	{
		if ($idUsuario==null) {
			return 0;
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario=$idUsuario;");
			$stm->execute();
			$A=$stm->fetchAll();
			return $A[0];
		}
	}
	//Metodo para obtener el idUsuario de un negocio
	public function ObtnenIdUsuario($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario from segidorescliente WHERE idOrganizacion = '$idOrganizacion';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
		//Metodo para listar los usuarios para el select
	public function ListarUsuarios($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario !='$idUsuario';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para obtener el los seguidores de un negocio
	public function ObtnenSeguidores($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * from usuarios WHERE idUsuario = '$idUsuario';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0];
	}
	//Metodo para obtener el numero de actividades echas por cada usuario
	public function ActividadesUsuarios($idOrganizacion,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM actividades, usuarios, negocios, organizaciones WHERE actividades.idUsuario = usuarios.idUsuario AND actividades.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = actividades.idOrganizacion AND actividades.idOrganizacion= '$idOrganizacion' and usuarios.idUsuario=$idUsuario;");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para obtener las notas de una organizacion
	public function ContenidosNotas($idContenido)
	{
		$stm = $this->pdo->prepare("SELECT * from contenidos, usuarios WHERE  contenidos.idUsuario=usuarios.idUsuario and contenidos.idContenido=?;");
		$stm->execute(array($idContenido));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	//Metodo para trer el nombre del mes
	public function NombreMes($idMes)
	{
		$stm = $this->pdo->prepare("SELECT mes FROM meses WHERE idMes = ?");
		$stm->execute(array($idMes));

		$A=$stm->fetchAll();
		return $A[0][0];
	}

	public function ObtenerInfoBitacora($tabla, $campo ,$id)
	{
		$query="SELECT idOrganizacion FROM " . $tabla . " WHERE ". $campo . " = " . $id;
		$stm = $this->pdo->prepare($query);
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	public function ListarBitacoras()
	{
		$stm = $this->pdo->prepare("SELECT * FROM bitacoras  WHERE accion='Insert' ORDER BY timestamp;");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
}
?>