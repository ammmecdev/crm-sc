<?php
class Negocio
{    
	public $idNegocio;
	public $idUsuario;
	public $idEtapa;
	public $idOrganizacion;
	public $idCliente;
	public $idEmbudo;
	public $idEquipo;
	public $claveOrganizacion;
	public $claveConsecutivo;
	public $claveServicio;
	public $tituloNegocio;
	public $valorNegocio;
	public $tipoMoneda;
	public $fechaGanadoPerdido;
	public $fechaCierre;
	public $contadorActivo;
	public $status;
	public $ponderacion;
	public $xCambio;
	public $idUnidadNegocio;

	private $pdo;
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	
		//Metodo para listar.
	public function Listar()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function ListarSeg()
	{
		$stm = $this->pdo->prepare("SELECT nombreOrganizacion, claveOrganizacion, claveConsecutivo, claveServicio from negocios, organizaciones where organizaciones.idOrganizacion = negocios.idOrganizacion");

		$stm->execute();
		return $stm;
	}
	//Metodo para listar los negocios creados
	public function ListarEtapas($idEmbudo)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM etapasventas, embudos WHERE etapasventas.idEmbudo = embudos.idEmbudo AND etapasventas.idEmbudo = '$idEmbudo' ORDER BY orden");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function CambiaEmbudo($idEmbudo, $idNegocio)
	{
		$sql = "UPDATE negocios SET 
		idEmbudo=?
		where idNegocio=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEmbudo,
				$idNegocio
			)
		);
	}

//Metodo para actualizar la fecha de cierre
	public function ActualizaCierreN($fechaCierre,$idNegocio)
	{
		$sql = "UPDATE negocios SET 
		fechaCierre=?
		where idNegocio=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$fechaCierre,
				$idNegocio
			)
		);
	}
	//Metodo para actualizar clave del negocio
	public function ActualizaClaveNegocio(Negocio $negocio)
	{
		$sql = "UPDATE negocios SET 
		claveOrganizacion=?,
		claveConsecutivo=?,
		claveServicio=?
		where idNegocio=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$negocio->claveOrganizacion,
				$negocio->claveConsecutivo,
				$negocio->claveServicio,
				$negocio->idNegocio,
			)
		);
	}

	//Metodo para listar las actividades Por negocio vencidas
	public function ActividadesNegocioVencidas($idNegocio)
	{
		try
		{
			date_default_timezone_set("America/Mexico_City");
			$hoy = date("Y-m-d"); 
			$stm = $this->pdo->prepare("SELECT * FROM actividades,usuarios WHERE idNegocio='$idNegocio' and actividades.idUsuario=usuarios.idUsuario AND fechaActividad < '$hoy' and completado=0  ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para consultar todos los negocios y tarer su mes y su idNegocio
	public function ConsultaNegociosXMes($idEmbudo,$filtro)
	{
		try
		{
			
			$stm = $this->pdo->prepare("SELECT idNegocio,SUBSTRING(fechaCierre,6,2) as meses,SUBSTRING(fechaCierre,1,4) as Año FROM negocios WHERE idEmbudo=$idEmbudo and status=$filtro ORDER BY meses ASC");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);

		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para tarer los negocios de cada mes
	public function ConsultaNegociosTrueM($idNegocio,$filtro)
	{
		try
		{
			
			$stm = $this->pdo->prepare("SELECT etapasventas.inactividad,negocios.idNegocio,negocios.tituloNegocio,negocios.claveOrganizacion,negocios.claveConsecutivo,negocios.claveServicio,negocios.contadorActivo,organizaciones.nombreOrganizacion,personas.nombrePersona,organizaciones.idOrganizacion,personas.idCliente,negocios.valorNegocio,negocios.tipoMoneda FROM negocios,personas,organizaciones,etapasventas WHERE negocios.idEtapa=etapasventas.idEtapa and negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion and idNegocio=$idNegocio and status=$filtro;
				");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);

		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para listar las actividades Por negocio planeadas
	public function ActividadesNegocioPlaneada($idNegocio)
	{
		try
		{
			date_default_timezone_set("America/Mexico_City");
			$hoy = date("Y-m-d"); 
			$stm = $this->pdo->prepare("SELECT * FROM actividades,usuarios WHERE idNegocio='$idNegocio' and actividades.idUsuario=usuarios.idUsuario AND fechaActividad >= '$hoy' and completado=0  ");
			
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	/*Metodo para buscar por filtrado chava*/
	public function ConsultaPorValor($valor,$idEmbudo,$filtro)
	{
		$stm = $this->pdo->prepare("SELECT idNegocio from negocios,organizaciones where negocios.idOrganizacion=organizaciones.idOrganizacion and idEmbudo = ? and status= ? and (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or negocios.tituloNegocio like '%$valor%' or organizaciones.nombreOrganizacion like '%$valor%')");
		$stm->execute(array($idEmbudo, $filtro));
		return $stm->fetchAll();
	}
	//Metodo para buscar el valor de los negocios ganados por mes En Pronostico chava
	public function ConsultaNegociosGanadosXMes($Mes,$Año,$valorIdEmbudo)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio,tipoMoneda FROM negocios WHERE MONTH(fechaCierre) = $Mes and YEAR(fechaCierre) = $Año and idEmbudo=$valorIdEmbudo and status=1");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);		
	}
	//Metodo para buscar el presupuesto por mes En Pronostico chava
	public function ConsultaNegociosPresupuestoXMes($Mes,$Año,$valorIdEmbudo)
	{
		$stm = $this->pdo->prepare("SELECT idMes,monto FROM presupuesto_general,pregen_meses WHERE presupuesto_general.idPresupuestoGeneral =pregen_meses.idPresupuestoGeneral and presupuesto_general.periodo=$Año and pregen_meses.idMes=$Mes and idEmbudo=$valorIdEmbudo");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);		
	}
	//Metodo para buscar por filtrado en la vista pronostico chava
	public function ConsultaPorValorPronostico($valor,$idEmbudo,$filtro)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT idNegocio,SUBSTRING(fechaCierre,6,2) as meses,SUBSTRING(fechaCierre,1,4) as Año from negocios,organizaciones where negocios.idOrganizacion=organizaciones.idOrganizacion  and idEmbudo=$idEmbudo and status=$filtro  and (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or negocios.tituloNegocio like '%$valor%' or organizaciones.nombreOrganizacion like '%$valor%')");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para obtener la probabilidad de las etapas chava
	public function ProbabilidadEtapa($idEtapa)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT probabilidad from etapasventas where idEtapa=$idEtapa;");
			$stm->execute();
			$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $valor;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para obtener el valor de los negocios en cada etapa chava
	public function ValoresNeg($idEtapa,$filtro,$valor)
	{
		try
		{

			$stm = $this->pdo->prepare("SELECT valorNegocio,tipoMoneda from negocios,organizaciones where negocios.idOrganizacion=organizaciones.idOrganizacion and idEtapa=$idEtapa and status=$filtro and (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or negocios.tituloNegocio like '%$valor%' or organizaciones.nombreOrganizacion like '%$valor%');");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para ver si hay algo en los embudos chava
	public function VerificarContenido($valor)
	{
		try
		{

			$stm = $this->pdo->prepare("SELECT * FROM negocios where idEmbudo=$valor");
			$stm->execute();
			return $stm->fetchAll();

		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para verificar el id de la etapa chava
	public function VerificarIdEtapa($idNegocio)
	{
		try
		{

			$stm = $this->pdo->prepare("SELECT idEtapa FROM negocios where idNegocio=?");
			$stm->execute(array(
				$idNegocio)
		);
			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;	

		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para actualizar el contador para cambiar los estados del negocio Blanco, Amarillo y Rojo chava
	public function ActualizarContadorActivo($idNegocio)
	{
		try
		{
			date_default_timezone_set("America/Mexico_City");
			$hoy = date("Y-m-d H:i:s"); 
			$stm = $this->pdo->prepare("UPDATE negocios SET contadorActivo='$hoy' WHERE idNegocio = $idNegocio");
			$stm->execute();


		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para consultar los negocios de cada etapa de venta
	public function ConsultaNegociosEmbudo($idEmbudo,$idEtapa,$idNegocio,$filtro)
	{
		try
		{

			$stm = $this->pdo->prepare("SELECT negocios.idNegocio,negocios.tituloNegocio,negocios.claveOrganizacion,negocios.claveConsecutivo,negocios.claveServicio,negocios.contadorActivo,organizaciones.nombreOrganizacion,personas.nombrePersona,organizaciones.idOrganizacion,personas.idCliente,negocios.valorNegocio,negocios.tipoMoneda FROM negocios,personas,organizaciones WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND idEmbudo=$idEmbudo  AND idEtapa=$idEtapa and idNegocio=$idNegocio and status=$filtro;");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para consultar valores en la vista detalles chava
	public function NegocioDetalles($idNegocio)
	{
		try
		{

			$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas WHERE negocios.idCliente=personas.idCliente AND negocios.idOrganizacion=organizaciones.idOrganizacion AND negocios.idEtapa=etapasventas.idEtapa and idNegocio=$idNegocio");
			$stm->execute();
			$Resultado=$stm->fetchAll();
			return $Resultado[0];
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para actualizar el nogocio en la etapa que esta
	public function ActualizarEtapaNegocio($idEtapa,$idNegocio)
	{

		$sql ="UPDATE negocios SET 
		idEtapa = ?
		WHERE idNegocio = ?";

		$A= $this->pdo->prepare($sql)
		->execute(
			array(
				$idEtapa,
				$idNegocio
			)
		); 

	}
	//Metodo para obtener el contenido de las eapasventas en forma de arreglo
	public function ContenidoEtapas($idEtapa)
	{
		$stm = $this->pdo->prepare("SELECT contenido FROM etapasventas WHERE idEtapa=$idEtapa;");
		$stm->execute();
		$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $valor;
	}
	//Metodo para listar los embudos
	public function ListarEmbudos()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM embudos ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para contar los negocios creados.
	public function Contador($idEmbudo, $valor, $filtro)
	{
		$sql1="SELECT COUNT(*) FROM negocios WHERE negocios.idEmbudo = $idEmbudo";
		$sql2=" and (claveOrganizacion like '%$valor%' or tituloNegocio like '%$valor%' or claveServicio like '%$valor%')";
		$sql3=" and negocios.status = $filtro";
		if ($valor == null && $filtro == null) $sql=$sql1;
		else if ($valor != null &&  $filtro == null) $sql=$sql1.$sql2;
		else $sql=$sql1.$sql2.$sql3;
		$stm = $this->pdo->prepare($sql);
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}

	//Metodo para sumar el valor del negocio. 
	public function ContadorValor($idEmbudo, $valor, $filtro)
	{
		if ($valor == null && $filtro == null) {
			$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE negocios.idEmbudo = $idEmbudo");

		}elseif ($valor != null &&  $filtro == null) {
			$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE negocios.idEmbudo = $idEmbudo AND (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or tituloNegocio like '%$valor%')");
		}else{
			$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE negocios.idEmbudo = $idEmbudo AND (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or tituloNegocio like '%$valor%') AND negocios.status = $filtro");
		}

		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function ContadorPonderacion($idEmbudo, $valor, $filtro, $dolar){

		if ($valor == null && $filtro == null) {
			$stm = $this->pdo->prepare("SELECT negocios.valorNegocio, negocios.tipoMoneda, negocios.status, etapasventas.nombreEtapa, etapasventas.probabilidad FROM negocios, etapasventas WHERE negocios.idEmbudo = $idEmbudo AND negocios.idEtapa = etapasventas.idEtapa");
		}elseif ($valor != null &&  $filtro == null) {
			$stm = $this->pdo->prepare("SELECT negocios.valorNegocio, negocios.tipoMoneda, negocios.status, etapasventas.nombreEtapa, etapasventas.probabilidad FROM negocios, etapasventas WHERE negocios.idEmbudo = $idEmbudo AND negocios.idEtapa = etapasventas.idEtapa AND (claveOrganizacion like '%$valor%' or tituloNegocio like '%$valor%' or claveServicio like '%$valor%')");
		}else{
			$stm = $this->pdo->prepare("SELECT negocios.valorNegocio, negocios.tipoMoneda, negocios.status, etapasventas.nombreEtapa, etapasventas.probabilidad FROM negocios, etapasventas WHERE negocios.idEmbudo = $idEmbudo AND negocios.idEtapa = etapasventas.idEtapa AND (claveOrganizacion like '%$valor%' or tituloNegocio like '%$valor%' or claveServicio like '%$valor%') AND negocios.status = $filtro");
		}
		$stm->execute();
		$arreglo = $stm->fetchAll(PDO::FETCH_OBJ); 

		$sumaPonderacion = 0;
		$total = 0;
		$valor = 0;

		foreach ($arreglo as $r):
			if ($r->tipoMoneda == "USD") {
				$valor = $r->valorNegocio * $dolar;
				$total = ($valor * $r->probabilidad) / 100;
			}else{
				$total = ($r->valorNegocio * $r->probabilidad) / 100;
			}
			$sumaPonderacion = $sumaPonderacion + $total;
		endforeach;

		return $sumaPonderacion;
	}

	//Metodo para consultar la clave de la organizacion que se encuentra en el negocio 
	public function ConsultaClaveOrganizacion($idOrganizacion)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT clave FROM organizaciones WHERE idOrganizacion = $idOrganizacion");
			$stm->execute(); 

			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;	
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Metodo para consultar el numero consecutivo segun la clave de la organizacion 
	public function ConsultaConsecutivo($claveOrga)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT claveConsecutivo from negocios WHERE claveOrganizacion = '$claveOrga' order by claveConsecutivo DESC limit 1");
			$stm->execute(); 

			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

    //Metodo para registrar negocios
	public function Registrar(Negocio $data)
	{
		$sql ="INSERT INTO negocios VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idUsuario,
				$data->idEtapa,
				$data->idOrganizacion,
				$data->idCliente,
				$data->idEmbudo,
				$data->idEquipo,
				$data->tituloNegocio,
				$data->claveOrganizacion,
				$data->claveConsecutivo,
				$data->claveServicio,
				$data->valorNegocio,
				$data->tipoMoneda,
				null,
				$data->fechaCierre,
				$data->contadorActivo,
				$data->fechaCreado,
				$data->status,
				$data->ponderacion,
				$data->xCambio,
				$data->estimacion,
				$data->idUnidadNegocio
			)
		);
		$con = $this->pdo->prepare("SELECT idNegocio from negocios order by idNegocio DESC limit 1");
		$con->execute();
		$valor = implode($con->fetchAll(PDO::FETCH_COLUMN));
		return $valor;

	}
	//Metodo para concatenar el necio a la etapa de venta
	public function ActualizaEtapaContenido($idOld,$data)
	{		
		try
		{
			$stm = $this->pdo->prepare("SELECT contenido FROM etapasventas WHERE idEtapa=$data->idEtapa and idEmbudo=$data->idEmbudo");
			$stm->execute();
			$valor = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			if ($valor==null) {
				$Resultado=$idOld;
			}else{
				$Resultado=$valor.','.$idOld++;	
			}
			$sql = "UPDATE etapasventas  SET contenido = '$Resultado'
			WHERE idEtapa = ?  and idEmbudo=?";
			$this->pdo->prepare($sql)
			->execute(
				array(
					$data->idEtapa,
					$data->idEmbudo
				)
			);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
		//Metodo para actualizar negocios
	public function Actualizar(Negocio $data)
	{
		$sql = "UPDATE negocios SET 
		idEtapa = ?,
		idOrganizacion = ?,
		idCliente = ?,
		idEmbudo = ?,
		idEquipo = ?,
		tituloNegocio = ?,
		claveOrganizacion = ?,
		claveConsecutivo = ?,
		claveServicio = ?,
		valorNegocio = ?,
		tipoMoneda = ?,
		fechaCierre = ?,
		ponderacion = ?,
		xCambio = ?,
		idUnidadNegocio = ?
		WHERE idNegocio = ? ";

		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idEtapa,
				$data->idOrganizacion,
				$data->idCliente,
				$data->idEmbudo,
				$data->idEquipo,
				$data->tituloNegocio,
				$data->claveOrganizacion,
				$data->claveConsecutivo,
				$data->claveServicio,
				$data->valorNegocio,
				$data->tipoMoneda,
				$data->fechaCierre,
				$data->ponderacion,
				$data->xCambio,
				$data->idUnidadNegocio,
				$data->idNegocio
			)
		);
	}

	public function CambiarClave(Negocio $data)
	{
		$sql = "UPDATE negocios SET 
		claveOrganizacion = ?,
		claveConsecutivo = ?,
		claveServicio = ?
		WHERE idNegocio = ? ";

		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->claveOrganizacion,
				$data->claveConsecutivo,
				$data->claveServicio,
				$data->idNegocio
			)
		);
	}

	public function verificarConsecutivo($claveOrganizacion, $claveConsecutivo, $claveServicio, $idNegocio)
	{
		$sql = $this->pdo->prepare("SELECT * FROM negocios WHERE claveConsecutivo = '$claveConsecutivo' AND claveOrganizacion = '$claveOrganizacion' AND idNegocio <> $idNegocio");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;					
	}

	/*Metodo para eliminar negocios*/
	public function Eliminar($idNegocio)
	{
		$sql ="DELETE FROM negocios WHERE idNegocio = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idNegocio
			)
		);
	}
	//Metodo para consultar negocios segun el embudo seleccionado.
	public function ConsultaNegocios($valorIdEmbudo, $filtro)
	{
		if($filtro != ''){
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario AND negocios.status = '$filtro' AND negocios.idEmbudo = '$valorIdEmbudo' ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo");
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario AND negocios.idEmbudo = '$valorIdEmbudo' ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo");
		}	
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function NegociosLista($valorIdEmbudo, $filtro, $inf, $sup)
	{
		if($filtro != ''){
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios, unidad_negocio WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario AND negocios.idUnidadNegocio = unidad_negocio.idUnidadNegocio AND negocios.status = '$filtro' AND negocios.idEmbudo = '$valorIdEmbudo' ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo, negocios.idNegocio LIMIT $inf, $sup");
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios, unidad_negocio WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario AND negocios.idUnidadNegocio = unidad_negocio.idUnidadNegocio AND negocios.idEmbudo = '$valorIdEmbudo' ORDER BY negocios.claveOrganizacion, negocios.claveConsecutivo, negocios.idNegocio LIMIT $inf, $sup");
		}	
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para buscar negocios. 
	public function ConsultaNegociosValor($idEmbudo, $valor, $filtro)
	{
		if($filtro != null){
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios, unidad_negocio WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario AND negocios.idUnidadNegocio = unidad_negocio.idUnidadNegocio AND negocios.idEmbudo = '$idEmbudo' AND negocios.status = '$filtro' AND (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or negocios.tituloNegocio like '%$valor%' or negocios.claveServicio like '%$valor%' or etapasventas.nombreEtapa like '%$valor%' or equipo.nombreEquipo like '%$valor%' or organizaciones.nombreOrganizacion like '%$valor%' or personas.nombrePersona like '%$valor%' or sector_industrial.nombreSector like '%$valor%' or usuarios.nombreUsuario like '%$valor%' or unidad_negocio.unidadNegocio like '%$valor%')");
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM negocios, organizaciones, pertenece, etapasventas, personas, embudos, equipo, sector_industrial, usuarios, unidad_negocio WHERE negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idNegocio = pertenece.idNegocio AND pertenece.idCliente = personas.idCliente AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idEmbudo = embudos.idEmbudo AND negocios.idEquipo = equipo.idEquipo AND organizaciones.idSector = sector_industrial.idSector AND negocios.idUsuario = usuarios.idUsuario AND negocios.idUnidadNegocio = unidad_negocio.idUnidadNegocio AND negocios.idEmbudo = '$idEmbudo' AND (concat(negocios.claveOrganizacion,'-',negocios.claveConsecutivo,'-',negocios.claveServicio) like '%$valor%' or negocios.tituloNegocio like '%$valor%' or negocios.claveServicio like '%$valor%' or etapasventas.nombreEtapa like '%$valor%' or equipo.nombreEquipo like '%$valor%' or organizaciones.nombreOrganizacion like '%$valor%' or personas.nombrePersona like '%$valor%' or sector_industrial.nombreSector like '%$valor%' or usuarios.nombreUsuario like '%$valor%' or unidad_negocio.unidadNegocio like '%$valor%')");
		}
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	//Metodo para listar organizaciones por persona
	public function listarOrganizacionesPorPersona($idCliente)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT organizaciones.idOrganizacion, organizaciones.nombreOrganizacion, organizaciones.clave FROM personas, organizaciones WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idCliente = ? ");

			$stm->execute(array($idCliente));

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	} 

	public function ListarO()
	{
			$stm = $this->pdo->prepare("SELECT * FROM organizaciones,sector_industrial where organizaciones.idSector = sector_industrial.idSector");
			$stm->execute();			
			return $stm->fetchAll(PDO::FETCH_OBJ);
	}


//Metodo para saber si hay alguna activdad registrada en el negocio
	public function FechaActividadNegocio($idNegocio)
	{
		try
		{

			date_default_timezone_set("America/Mexico_City");
      $A= date("Y")."-".date("m")."-".date("d"); //->No obitiene la fecha real
      $stm = $this->pdo->prepare("SELECT fechaActividad FROM actividades WHERE idNegocio=$idNegocio AND completado=0 AND fechaActividad <='$A' ORDER BY `actividades`.`fechaActividad` ASC ");
      $stm->execute();
      $resultado = $stm->fetchAll();

      if (!empty($resultado)) {
      	return $datos = $resultado[0][0];  
      }else{
      	$stm = $this->pdo->prepare("SELECT fechaActividad FROM actividades WHERE idNegocio=$idNegocio AND completado=0 ORDER BY `actividades`.`fechaActividad` DESC ");
      	$stm->execute();
      	$resultado = $stm->fetchAll();  
      	if (!empty($resultado)) {
      		return $datos = $resultado[0][0]; 
      	}else{
      		return null;
      	}

      } 
  }
  catch(Exception $e)
  {
  	die($e->getMessage());
  }
}

  //Metodo para comprobar el estado de la activdad

public function EstadoActividad($idNegocio)
{
	try
	{
		$stm = $this->pdo->prepare("SELECT completado FROM actividades WHERE idNegocio = ? ORDER BY `actividades`.`completado` ASC   ");
		$stm->execute(array($idNegocio));

		$A=$stm->fetchAll();
		return $A[0][0];
	}
	catch(Exception $e)
	{
		die($e->getMessage());
	}
}

		//Metodo para listar organizaciones por persona
public function listarPersonasPorOrganizacion($idOrganizacion)
{
	try
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas, organizaciones WHERE personas.idOrganizacion = ? AND personas.idOrganizacion = organizaciones.idOrganizacion ");

		$stm->execute(array($idOrganizacion));

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	catch(Exception $e)
	{
		die($e->getMessage());
	}
}

public function listarPersonas()
{
	$stm = $this->pdo->prepare("SELECT * FROM personas ");

	$stm->execute();

	return $stm->fetchAll(PDO::FETCH_OBJ);
}

//Metodo para actualizar el valor del dolar al consultar desde el index
public function ActualizarValorDolar($dolar)
{
	$sql = "UPDATE empresa SET 
	valorDolar = ?
	WHERE idEmpresas = 1";
	$this->pdo->prepare($sql)
	->execute(array($dolar));
}

public function actualizarValor($idNegocio, $valorNegocio)
{
	$select = $this->pdo->prepare("SELECT valorNegocio FROM negocios WHERE idNegocio = ?");
	$select->execute(array($idNegocio));
	$valor = implode($select->fetchAll(PDO::FETCH_COLUMN));
	$resultado = ($valor - $valorNegocio);
	$sql = "UPDATE negocios SET 
	valorNegocio = ?
	WHERE idNegocio = ?";
	$this->pdo->prepare($sql)
	->execute(array($resultado, $idNegocio));
}

//Metodo para buscar los negocios por mes y tarer valores especificos para sacar el valor estimado
public function ConsultaNegociosEstimadoXMes($mes,$año,$idEmbudo)
{
	$stm = $this->pdo->prepare("SELECT etapasventas.probabilidad,negocios.valorNegocio,negocios.tipoMoneda FROM negocios,etapasventas WHERE etapasventas.idEtapa=negocios.idEtapa and MONTH(fechaCierre)=$mes and status=0 and negocios.idEmbudo=$idEmbudo and YEAR(fechaCierre)=$año;");

	$stm->execute();

	return $stm->fetchAll(PDO::FETCH_OBJ);
}

	//Metodo para consultar el valor del dolar desde la tabla de empresa
public function ConsultaValorDolar()
{
	try
	{
		$stm = $this->pdo->prepare("SELECT valorDolar FROM empresa WHERE idEmpresas = 1 ");
		$stm->execute(); 

		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}
	catch(Exception $e)
	{
		die($e->getMessage());
	}
}


public function RegistrarPresupuesto($idNegocio, $data){

	$sql ="INSERT INTO presupuestos VALUES(?,?,?,?,?)";
	$this->pdo->prepare($sql)
	->execute(
		array(
			null,
			$data->idPresupuesto,
			$idNegocio,
			$data->descripcion,
			$data->totalAnual
		)
	);
}

public function totalNegocios($filtro, $valorIdEmbudo)
{
	$stm = $this->pdo->prepare("SELECT count(*) as totalRegistros FROM negocios WHERE status = ? AND idEmbudo = ?");
	$stm->execute(array($filtro, $valorIdEmbudo));
	return $stm->fetch(PDO::FETCH_COLUMN);
}

public function listarNegociosConCotizacion(){
	$stm = $this->pdo->prepare("SELECT * FROM negocios n INNER JOIN costo_operativo co ON co.idNegocio = n.idNegocio INNER JOIN cotizacion c ON c.idCostoOperativo = co.idCostoOperativo WHERE c.estado = ? AND co.estado = ?");
	$stm->execute(array('Aprobado','Aprobado'));

	return $stm->fetchAll(PDO::FETCH_OBJ);
}

public function listarNegocioConCotizacion($idNegocio){
	$stm = $this->pdo->prepare("SELECT n.idNegocio,n.claveOrganizacion, n.claveConsecutivo, n.claveServicio, n.tituloNegocio, n.idCliente, co.fechaEntrega, c.idCotizacion FROM negocios n INNER JOIN costo_operativo co ON co.idNegocio = n.idNegocio INNER JOIN cotizacion c ON c.idCostoOperativo = co.idCostoOperativo WHERE n.idNegocio = ? AND c.estado = ? AND co.estado = ?");
	$stm->execute(array($idNegocio,'Aprobado','Aprobado'));

	return $stm->fetchAll(PDO::FETCH_OBJ);
}

public function obtEstimaciones($claveOrganizacion, $claveConsecutivo, $claveServicio)
{
	$stm = $this->pdo->prepare("SELECT idNegocio FROM negocios WHERE claveOrganizacion = ? AND claveConsecutivo = ? AND claveServicio = ? AND estimacion <> 0");
	$stm->execute(array($claveOrganizacion, $claveConsecutivo, $claveServicio));
	return $stm->fetchAll(PDO::FETCH_OBJ);
}

public function actualizarEstimacion(Negocio $data, $dataId)
{
	foreach ($dataId as $r):
		$sql = "UPDATE negocios SET 
		idOrganizacion = ?,
		idCliente = ?,
		claveOrganizacion = ?,
		claveConsecutivo = ?,
		claveServicio = ?,
		tipoMoneda = ?,
		ponderacion = ?
		WHERE idNegocio = ? ";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idOrganizacion,
				$data->idCliente,
				$data->claveOrganizacion,
				$data->claveConsecutivo,
				$data->claveServicio,
				$data->tipoMoneda,
				$data->ponderacion,
				$r->idNegocio
			)
		);
	endforeach;	
}

public function consultarClave($idNegocio)
{
	$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio FROM negocios WHERE idNegocio = ? ");
	$stm->execute(array($idNegocio));
	return $stm->fetch(PDO::FETCH_OBJ,PDO::FETCH_ASSOC);
}

	//Metodo para listar las unidades de negocio
	public function ListarUnidadNegocio()
	{
			$stm = $this->pdo->prepare("SELECT * FROM unidad_negocio");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
	}


} //final de la clase 
?>