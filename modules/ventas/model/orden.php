<?php
class Orden
{  
	private $idCotizacion;
	private $idCostoOperativo;
	private $idOrdenTrabajo;
	private $fecha;
	private $noPedido;
	private $descripcion;
	private $observaciones;
	private $estado;
	private $pdo;

	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}
	public function ListarDatosOrden($idOrdenTrabajo)
	{
		$stm = $this->pdo->prepare("SELECT fecha, noPedido, descripcion, observaciones FROM orden_trabajo WHERE idOrdenTrabajo = ?");
		$stm->execute(array($idOrdenTrabajo));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	public function ListarDatosOrdenTodo()
	{
		$stm = $this->pdo->prepare("SELECT * from orden_trabajo, cotizacion, costo_operativo WHERE  orden_trabajo.idCotizacion=cotizacion.idCotizacion AND cotizacion.idCostoOperativo = costo_operativo.idCostoOperativo;");
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	} 

	public function ListarDatosOrdenTodoNegocio($bTexto)
	{
		$stm = null;
		if($bTexto != ""){
			$stm = $this->pdo->prepare("SELECT n.claveOrganizacion,n.claveConsecutivo,n.claveServicio, ot.fecha, n.tituloNegocio, ot.noPedido, p.nombrePersona, co.tiempoEntrega, co.fechaEntrega, c.idCotizacion, co.idCostoOperativo, ot.idOrdenTrabajo,
				ot.observaciones,ot.descripcion, n.idCliente, n.idNegocio, ot.estado
				FROM orden_trabajo ot INNER JOIN cotizacion c ON c.idCotizacion = ot.idCotizacion
					INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
					INNER JOIN negocios n ON n.idNegocio = co.idNegocio
					INNER JOIN personas p ON p.idCliente = n.idCliente
					WHERE (n.tituloNegocio like '%$bTexto%' OR co.fechaEntrega like '%$bTexto%' OR (n.claveOrganizacion +' '+n.claveConsecutivo+' '+n.claveServicio) like '%$bTexto%' OR ot.noPedido like '%$bTexto%' OR p.nombrePersona like '%$bTexto%' OR co.fechaEntrega like '%$bTexto%' OR ot.estado like '%$bTexto%'
					OR ot.fecha like '%$bTexto%');");
		}else{
			$stm = $this->pdo->prepare("SELECT n.claveOrganizacion,n.claveConsecutivo,n.claveServicio, ot.fecha, n.tituloNegocio, ot.noPedido, p.nombrePersona, co.tiempoEntrega, co.fechaEntrega, c.idCotizacion, co.idCostoOperativo, ot.idOrdenTrabajo,
				ot.observaciones,ot.descripcion, n.idCliente, n.idNegocio, ot.estado
				FROM orden_trabajo ot INNER JOIN cotizacion c ON c.idCotizacion = ot.idCotizacion
					INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
					INNER JOIN negocios n ON n.idNegocio = co.idNegocio
					INNER JOIN personas p ON p.idCliente = n.idCliente");
		}
		$stm->execute();
		
		return $stm->fetchAll(PDO::FETCH_OBJ);
	} 

	public function Registrar($orden){
		$sql ="INSERT INTO orden_trabajo VALUES(?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$orden->getIdCotizacion(),
				$orden->getFecha(),
				$orden->getNoPedido(),
				$orden->getDescripcion(),
				$orden->getObservaciones(),
				"Pendiente"
			)
		);
	}

	public function Actualizar($orden){
		$sql = "UPDATE orden_trabajo SET 
			fecha = ?,
			noPedido = ?,
			descripcion = ?,
			observaciones = ?
			WHERE idOrdenTrabajo = ?";

		$this->pdo->prepare($sql)
		->execute(
			array(
				$orden->getFecha(),
				$orden->getNoPedido(),
				$orden->getDescripcion(),
				$orden->getObservaciones(),
				$orden->getIdOrdenTrabajo()
			)
		);
	}

	public function Eliminar($idOrdenTrabajo){
		$sql = "DELETE FROM orden_trabajo WHERE idOrdenTrabajo = ?";

		$this->pdo->prepare($sql)
		->execute(
			array(
				$idOrdenTrabajo
			));
	}


	//GETTERS
	public function getIdCotizacion(){
		return $this->idCotizacion;
	}

	public function getIdCostoOperativo(){
		return $this->idCostoOperativo;
	}

	public function getIdOrdenTrabajo(){
		return $this->idOrdenTrabajo;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function getNoPedido(){
		return $this->noPedido;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function getObservaciones(){
		return $this->observaciones;
	}

	public function getEstado(){
		return $this->estado;
	}


	//SETTERS
	public function setIdCotizacion($idCotizacion){
		$this->idCotizacion = $idCotizacion;
	}

	public function setIdCostoOperativo($idCostoOperativo){
		$this->idCostoOperativo = $idCostoOperativo;
	}

	public function setIdOrdenTrabajo($idOrdenTrabajo){
		$this->idOrdenTrabajo = $idOrdenTrabajo;
	}

	public function setFecha($fecha){
		$this->fecha = $fecha;
	}

	public function setNoPedido($noPedido){
		$this->noPedido = $noPedido;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}

	public function setObservaciones($observaciones){
		$this->observaciones = $observaciones;
	}

	public function setEstado($estado){
		$this->estado = $estado;
	}
}