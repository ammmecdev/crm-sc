<?php
class Cotizacion
{  
	private $idCotizacion;
	private $idCostoOperativo;
	private $claveCotizacion;
	private $fechaCotizacion;
	private $atencion;
	private $factor;
	private $subTotal;
	private $iva;
	private $total;
	private $vigencia;
	private $estado;
	private $direccion;
	private $localidad;

	private $tituloNegocio;
	private $nombreOrganizacion;
	private $nombreUsuario;
	private $claveNegocio;
	private $pdo;

	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	public function listarCostoOperativo()
	{
		$stm = $this->pdo->prepare("SELECT co.idCostoOperativo, concat(ne.claveOrganizacion,'-', ne.claveConsecutivo,'-',ne.claveServicio) as claveNegocio FROM costo_operativo as co, negocios as ne WHERE co.idNegocio = ne.idNegocio AND co.estado = 'Aprobado'");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function datosCostoOperativo($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT ne.tituloNegocio, org.nombreOrganizacion,  co.requisicion, us.nombreUsuario FROM costo_operativo as co, negocios as ne, organizaciones as org, usuarios as us WHERE co.idNegocio = ne.idNegocio AND ne.idOrganizacion = org.idOrganizacion AND co.idUsuario = us.idUsuario AND co.idCostoOperativo = ?");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}
	public function ObtenerCotizacion($idCotizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM cotizacion WHERE idCotizacion = ?");
		$stm->execute(array($idCotizacion));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	public function ObtenerDatosCotizacion($idCotizacion)
	{
		$stm = $this->pdo->prepare("SELECT subtotal, iva, total, vigencia FROM cotizacion WHERE idCotizacion = ?");
		$stm->execute(array($idCotizacion));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	public function Listar($bTexto)
	{
		$stm = $this->pdo->prepare("SELECT claveCotizacion, fechaCotizacion FROM cotizacion");
		$stm->execute();
		return $stm;
	}

	public function ListarPendiente($bTexto){
		if($bTexto!= ""){
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE (c.claveCotizacion like '%$bTexto%' OR 2 like '%$bTexto%' OR c.fechaCotizacion like '%$bTexto%'
				OR co.requisicion like '%$bTexto%' OR u.nombreUsuario like '%$bTexto%' OR c.total like '%$bTexto%'
				) AND c.estado = ? AND co.estado = ?");

		}else{
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE c.estado = ? AND co.estado = ?");
		}
		$stm->execute(array('Pendiente','Aprobado'));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarRechazado($bTexto){
		if($bTexto!= ""){
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE (c.claveCotizacion like '%$bTexto%' OR 2 like '%$bTexto%' OR c.fechaCotizacion like '%$bTexto%'
				OR co.requisicion like '%$bTexto%' OR u.nombreUsuario like '%$bTexto%' OR c.total like '%$bTexto%'
				) AND c.estado = ? AND co.estado = ?");

		}else{
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE c.estado = ? AND co.estado = ?");
		}
		$stm->execute(array('Rechazado','Aprobado'));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarAprobado($bTexto){
		if($bTexto!= ""){
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE (c.claveCotizacion like '%$bTexto%' OR 2 like '%$bTexto%' OR c.fechaCotizacion like '%$bTexto%'
				OR co.requisicion like '%$bTexto%' OR u.nombreUsuario like '%$bTexto%' OR c.total like '%$bTexto%'
				) AND c.estado = ? AND co.estado = ?");

		}else{
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE c.estado = ? AND co.estado = ?");
		}
		$stm->execute(array('Aprobado','Aprobado'));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarTodas($bTexto){
		if($bTexto!= ""){
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE (c.claveCotizacion like '%$bTexto%' OR 2 like '%$bTexto%' OR c.fechaCotizacion like '%$bTexto%'
				OR co.requisicion like '%$bTexto%' OR u.nombreUsuario like '%$bTexto%' OR c.total like '%$bTexto%'
				) AND  co.estado = ?");

		}else{
			$stm = $this->pdo->prepare("SELECT  c.claveCotizacion, concat(n.claveOrganizacion,'-', n.claveConsecutivo,'-',n.claveServicio) as cuenta, 
			c.fechaCotizacion, co.requisicion,  u.nombreUsuario, c.total, c.idCotizacion, co.idCostoOperativo, c.direccion, c.localidad, c.iva, c.factor, c.total,c.subtotal,
			c.estado
				FROM cotizacion c INNER JOIN costo_operativo co ON co.idCostoOperativo = c.idCostoOperativo
								INNER JOIN negocios n ON n.idNegocio = co.idNegocio
								LEFT JOIN usuarios u ON u.idUsuario = co.idUsuario
				WHERE  co.estado = ?");
		}
		$stm->execute(array('Aprobado'));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function Guardar(){
		$sql = "INSERT INTO cotizacion VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$stm = $this->pdo->prepare($sql);
		$stm->execute(
			array(
				null,
				$this->getIdCostoOperativo(),
				$this->getClaveCotizacion(),
				$this->getFechaCotizacion(),
				$this->getAtencion(), //atencion
				$this->getFactor(),
				$this->getSubTotal(),
				$this->getIva(),
				$this->getTotal(),
				null, //vigencia
				$this->getEstado(),
				$this->getDireccion(),
				$this->getLocalidad()
			)
		);
	}

	public function Actualizar(){
		$sql = "UPDATE cotizacion SET 
			idCostoOperativo = ?,
			claveCotizacion = ?,
			fechaCotizacion = ?,
			atencion = ?,
			factor = ?,
			subtotal = ?,
			iva = ?,
			total = ?,
			vigencia = null,
			direccion = ?,
			localidad = ?
			WHERE idCotizacion = ?";
		$stm = $this->pdo->prepare($sql);
		$stm->execute(
			array(
				$this->getIdCostoOperativo(),
				$this->getClaveCotizacion(),
				$this->getFechaCotizacion(),
				$this->getAtencion(), //atencion
				$this->getFactor(),
				$this->getSubTotal(),
				$this->getIva(),
				$this->getTotal(),
				$this->getDireccion(),
				$this->getLocalidad(),
				$this->getIdCotizacion()
			)
		);
	}

	public function Eliminar($idCotizacion){
		$sql="DELETE FROM cotizacion WHERE idCotizacion = ?";
		$stm = $this->pdo->prepare($sql);
		$stm->execute(array($idCotizacion));

	}

	public function CambiarEstado($idCotizacion,$estado){
		$sql = "UPDATE cotizacion SET 
			estado = ?
			WHERE idCotizacion = ?";
		$stm = $this->pdo->prepare($sql);
		$stm->execute(
			array(
				$estado,
				$idCotizacion
			)
		);

	}

	public function getIdCotizacion(){
		return $this->idCotizacion;
	}

	public function getIdCostoOperativo(){
		return $this->idCostoOperativo;
	}
	public function getClaveCotizacion(){
		return $this->claveCotizacion;
	}
	public function getFechaCotizacion(){
		return $this->fechaCotizacion;
	}
	public function getAtencion(){
		return $this->atencion;
	}
	public function getFactor(){
		return $this->factor;
	}
	public function getSubTotal(){
		return $this->subTotal;
	}
	public function getIva(){
		return $this->iva;
	}
	public function getTotal(){
		return $this->total;
	}
	public function getVigencia(){
		return $this->vigencia;
	}
	public function getEstado(){
		return $this->estado;
	}

	public function getDireccion(){
		return $this->direccion;
	}
	public function getLocalidad(){
		return $this->localidad;
	}

	public function setIdCotizacion($idCot){
		 $this->idCotizacion = $idCot;
	}

	public function setIdCostoOperativo($idCosto){
		$this->idCostoOperativo = $idCosto;
	}
	public function setClaveCotizacion($clave){
		$this->claveCotizacion = $clave;
	}
	public function setFechaCotizacion($fecha){
		$this->fechaCotizacion = $fecha;
	}
	public function setAtencion($atencion){
		$this->atencion = $atencion;
	}
	public function setFactor($factor){
		$this->factor = $factor;
	}
	public function setSubTotal($subtotal){
		$this->subTotal = $subtotal;
	}
	public function setIva($iva){
		$this->iva = $iva;
	}
	public function setTotal($total){
		$this->total = $total;
	}
	public function setVigencia($vigencia){
		$this->vigencia = $vigencia;
	}
	public function setEstado($estado){
		$this->estado = $estado;
	}

	public function setDireccion($dir){
		$this->direccion = $dir;
	}
	public function setLocalidad($loc){
		$this->localidad = $loc;
	}

	
}
?>