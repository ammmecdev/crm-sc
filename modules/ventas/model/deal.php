<?php 
class Deal
{
	public $idNegocio;
	public $status;
	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     

	}
		//Metodo para consultar valores en la vista detalles
	public function NegocioDetalles($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT * FROM negocios,personas,organizaciones,etapasventas,usuarios, unidad_negocio,equipo WHERE negocios.idCliente = personas.idCliente AND negocios.idOrganizacion = organizaciones.idOrganizacion AND negocios.idEtapa = etapasventas.idEtapa AND negocios.idUnidadNegocio = unidad_negocio.idUnidadNegocio AND negocios.idEquipo = equipo.idEquipo AND negocios.idUsuario = usuarios.idUsuario and idNegocio = $idNegocio");
		$stm->execute();
		$Resultado=$stm->fetchAll();
		if ($Resultado==null) {
			header("Location: index.php?c=ventas"); 
		}else{
			return $Resultado[0];
		}
	}
	//Método para obtener los datos correctos del contacto
	public function ObtenerContacto($idCliente)
	{	
		$stm = $this->pdo
		->prepare("SELECT nombrePersona, telefono, email, honorifico, puesto FROM personas WHERE idCliente = ?");
		$stm->execute(array($idCliente));
		return $stm->fetch(PDO::FETCH_OBJ);
	}

 	public function ObtenerEmbudo($idNegocio)
 	{
 		$stm = $this->pdo
		->prepare("SELECT embudos.nombre, embudos.idEmbudo FROM embudos, negocios WHERE embudos.idEmbudo=negocios.idEmbudo AND idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
 	}

	//Metodo para cambiar la etapa del negocio
	public function CambiaEatapa($idEtapa,$idNegocio)
	{
		$sql ="UPDATE negocios SET 
		idEtapa=? WHERE idNegocio=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEtapa,
				$idNegocio
			)
		);
	}
	//Metodo para listar las personas dependiendo de la organizacion para el select
	public function ListarPersona($idOrganizacion, $idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas WHERE idOrganizacion=? and idCliente != ?");
		$stm->execute(array($idOrganizacion, $idCliente));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar los usuarios para el select
	public function ListarUsuarios($idOrganizacion,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario !='$idUsuario';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para Listar Personas vinculadas al negocio
	public function ListarPersonaVinculadas($idOrganizacion)
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas WHERE idOrganizacion='$idOrganizacion';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para Listar causas
	public function ListarCausas()
	{
		$stm = $this->pdo->prepare("SELECT * FROM causaperdida;");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para traer un arreglo de las personas vinculadas a dicho negocio
	public function ArregloDePersonas($idNegocio,$idCliente)
	{
		$stm = $this->pdo->prepare("SELECT idCliente FROM participantes WHERE idNegocio='$idNegocio' and idCliente !='$idCliente';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	//Metodo para traer un arreglo de los usuarios que tengan actividades registradas
	public function ArregloDeSeguidoresAct($idNegocio,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario FROM actividades WHERE idNegocio='$idNegocio' and idUsuario !='$idUsuario';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	//Metodo para traer un arreglo de los usuarios vinculadas a dicho negocio
	public function ArregloDeSeguidores($idNegocio,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario FROM seguidores WHERE idNegocio='$idNegocio' and idUsuario !='$idUsuario';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_COLUMN);
	}
	//Metodo para cambiar el estado del negocio
	public function CambiaStaus($status,$idNegocio,$FechaAc)
	{
		$sql ="UPDATE negocios SET 
		status=?,fechaGanadoPerdido=? WHERE idNegocio=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$status,
				$FechaAc,
				$idNegocio
			)
		);
	}
	//Metodo para reabrir un negocio perdido
	public function ReabrirPerdido($idNegocio)
	{
		$sql ="DELETE FROM negocios_perdidos WHERE idNegocio=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idNegocio
			)
		);
	}
	//Metodo para guardar un cambio de un negocio
	public function RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario)
	{
		$sql ="INSERT INTO registro_cambios VALUES (?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$accion,
				$campo1,
				$campo2,
				$FechaAc,
				$idNegocio,
				$idUsuario

			)
		);

	}
	//Metodo para guardar el negocio perdido negocio
	public function NegocioPerdido($idNegocio,$razonPerdido,$comentarios)
	{
		$sql ="INSERT INTO negocios_perdidos 
		VALUES (?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idNegocio,
				$razonPerdido,
				$comentarios
			)
		);
	}
	//Metodo para trer el nombre del mes
	public function NombreMes($idMes)
	{
		$stm = $this->pdo->prepare("SELECT mes FROM meses WHERE idMes = ?");
		$stm->execute(array($idMes));

		$A=$stm->fetchAll();
		return $A[0][0];

	}
	//Metodo para trer el nombre del usuario
	public function NombreUsuario($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT nombreUsuario FROM usuarios WHERE idUsuario = ?");
		$stm->execute(array($idUsuario));

		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para trer el nombre del cliente o participante
	public function NombreParticipantes($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT nombrePersona FROM personas WHERE idCliente = ?");
		$stm->execute(array($idCliente));

		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para trer el nombre de la etapa de venta
	public function NombreEtapa($idEtapa)
	{
		$stm = $this->pdo->prepare("SELECT nombreEtapa FROM etapasventas WHERE idEtapa = ?");
		$stm->execute(array($idEtapa));
		$A=$stm->fetchAll();
		return $A[0][0];
	}

	//Metodo para trer el nombre del embudo de venta
	public function NombreEmbudo($idEmbudo)
	{
		$stm = $this->pdo->prepare("SELECT nombre FROM embudos WHERE idEmbudo = ?");
		$stm->execute(array($idEmbudo));
		$A=$stm->fetchAll();
		return $A[0][0];
	}

	//Metodo para tarer el numero de personal a un negocio
	public function ContadorPersonas($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT ((SELECT COUNT(*) FROM pertenece WHERE idNegocio=$idNegocio) + (SELECT COUNT(*) FROM participantes WHERE idNegocio=$idNegocio)) as total;");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}
	//Metodo para tarer el numero de personal a un negocio
	public function ContadorSeguidores($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT ((SELECT COUNT(*) FROM negocios WHERE idNegocio=$idNegocio) + (SELECT COUNT(*) FROM seguidores WHERE idNegocio=$idNegocio)) as total;");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;	
	}
	//Metodo para obtener el idPersonas de un negocio
	public function ObtnenIdPersonas($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT idCliente from participantes WHERE idNegocio = ?;");
		$stm->execute(array($idNegocio));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para obtener el idUsuario de un negocio
	public function ObtnenIdUsuario($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT idUsuario from seguidores WHERE idNegocio = '$idNegocio';");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	public function ObtnenPersonas($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * from personas WHERE idCliente = '$idCliente';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0];
	}
	//Metodo para obtener el los seguidores de un negocio
	public function ObtnenSeguidores($idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT * from usuarios WHERE idUsuario = '$idUsuario';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0];
	}
	//Metodo para eliminar la fecha de cierre del negocio
	public function EliminaFechaCierre($idCliente)
	{
		$stm = $this->pdo->prepare("UPDATE negocios SET fechaCierre = null WHERE idNegocio = ?");
		$stm->execute(array($idCliente));
	}
	//Metodo para agregar una fecha de cierre del negocio
	public function RegistrarFechaCierre($fechaCierre,$idCliente)
	{
		$stm = $this->pdo->prepare("UPDATE negocios SET fechaCierre = ? WHERE idNegocio = ?");
		$stm->execute(array($fechaCierre,
			$idCliente));
	}
	 //Metodo Para agregar participantes a un negocio
	public function AgregarParticipantes($idNegocio, $idCliente)
	{
		$sql ="INSERT INTO participantes VALUES(?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idNegocio,
				$idCliente
			)
		);
	}

	//Metodo Para agregar segidores a un negocio
	public function AgregarSeguidores($idNegocio,$idCliente)
	{
		$sql ="INSERT INTO seguidores VALUES(?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idNegocio,
				$idCliente
			)
		);
		// $this->RegistrarBitacora($this->pdo->lastInsertId(),'Insert', 'seguidores');
	}

	//Metodo para desvincular contactos del negocio
	public function EliminarParticipantes($idNegocio,$idCliente)
	{
		// $sql= $this->pdo->prepare("SELECT idParticipante FROM participantes where idNegocio=? AND idCliente=?");
		// $resultado = $sql->execute(array($idNegocio, $idCliente));
		// $idParticipante = implode($sql->fetchAll(PDO::FETCH_COLUMN));

		// $idBitacora = $this->RegistrarBitacora($idParticipante,'Delete','participantes');

		// $this->RegistrarNotasBitacoras($idBitacora, 'idNegocio', $idNegocio);
		// $this->RegistrarNotasBitacoras($idBitacora, 'idCliente', $idCliente);

		$stm = $this->pdo->prepare("DELETE FROM participantes WHERE idNegocio=? and idCliente=?");
		$stm->execute(array($idNegocio,$idCliente));
	}

	//Metodo para desvincular usuarios del negocio
	public function EliminarSeguidores($idNegocio, $idUsuario)
	{
		// $sql= $this->pdo->prepare("SELECT idSeguidor FROM seguidores where idNegocio=? AND idUsuario=?");
		// $resultado = $sql->execute(array($idNegocio, $idUsuario));
		// $idSeguidor = implode($sql->fetchAll(PDO::FETCH_COLUMN));

		// $idBitacora = $this->RegistrarBitacora($idSeguidor,'Delete','seguidores');

		// $this->RegistrarNotasBitacoras($idBitacora, 'idNegocio', $idNegocio);
		// $this->RegistrarNotasBitacoras($idBitacora, 'idUsuario', $idUsuario);

		$stm = $this->pdo->prepare("DELETE FROM seguidores WHERE idNegocio=? and idUsuario=?");
		$stm->execute(array($idNegocio, $idUsuario));
	}

	public function RegistrarNotasBitacoras($idBitacora, $nota1, $nota2)
	{
		$sql ="INSERT INTO notas_bitacoras VALUES(?,?,?)";
		$this->pdo->prepare($sql)->execute(
			array(
				$idBitacora,
				$nota1,
				$nota2
			)
		);
	}

 	//Metodo Para registrar bitacoras de movimientos.
	public function RegistrarBitacora($idRelacion, $accion, $table)
	{
		$sql ="INSERT INTO bitacoras VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$accion,
				$table,
				$idRelacion,
				$_SESSION['idUsuario'],
				date("Y-m-d H:i:s")
			)
		);
		return $this->pdo->lastInsertId();
	}

	 //Metodo Para tarer la fecha de cierre de un negocio
	public function TraerFechaC($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT fechaCierre FROM negocios WHERE idNegocio = ?");
		$stm->execute(array($idNegocio));

		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para agregar notas al negocio
	public function RegistrarNota($idUsuario,$idNegocio,$idCliente,$idOrganizacion,$notas,$FechaAc,$nombre,$size)
	{
		$sql ="INSERT INTO contenidos VALUES(?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$idUsuario,
				null,
				null,
				$idNegocio,
				$notas,
				$nombre,
				$size,
				$FechaAc
			)
		);
	}

	//Metodo para actualizar las notas del negocio
	public function ActualizarNota($idContenido,$idUsuario,$notas,$FechaAc)
	{
		$stm = $this->pdo->prepare("UPDATE contenidos SET notas = ?, idUsuario = ? , fechaSubido = ?  WHERE idContenido = ?");
		$stm->execute(array($notas,
			$idUsuario,
			$FechaAc,
			$idContenido
		));
	}
	//Metodo para eliminar las notas del negocio
	public function EliminarNota($idContenido)
	{
		$stm = $this->pdo->prepare("DELETE FROM contenidos WHERE idContenido=?");
		$stm->execute(array($idContenido
		));
	}
	//Metodo para eliminar las notas del negocio
	public function EliminarArchivo($idContenido)
	{
		$stm = $this->pdo->prepare("DELETE FROM contenidos WHERE idContenido=?");
		$stm->execute(array($idContenido
		));
	}
	//Metodo para obtener el conatdor de notas
	public function ContadorNotas($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM contenidos WHERE idNegocio = '$idNegocio' and notas is not null;");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para obtener el conatdor de archivos
	public function ContadorArchivos($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM contenidos WHERE idNegocio = '$idNegocio' and archivos is not null;");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para obtener el conatdor de cambios
	public function ContadorCambios($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM registro_cambios WHERE idNegocio = '$idNegocio';");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para obtener el conatdor de actividades
	public function ContadorActividades($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM actividades WHERE idNegocio = ?");
		$stm->execute(array($idNegocio));
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo para obtener las actividades
	public function Actividades($idActividad)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades, usuarios, negocios, organizaciones WHERE actividades.idUsuario = usuarios.idUsuario AND actividades.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = actividades.idOrganizacion AND actividades.idActividad=? ORDER BY actividades.idActividad DESC;");
		$stm->execute(array($idActividad));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
		//Metodo para obtener las actividades
	public function ObtenerActividades($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT * FROM actividades, usuarios, negocios, organizaciones WHERE actividades.idUsuario = usuarios.idUsuario AND actividades.idNegocio=negocios.idNegocio AND organizaciones.idOrganizacion = actividades.idOrganizacion AND negocios.idNegocio = ? ORDER BY actividades.idActividad DESC;");
		$stm->execute(array($idNegocio));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para obtener el contenido de un negocio
	public function Contenidos($idContenido)
	{
		$stm = $this->pdo->prepare("SELECT * from contenidos,usuarios WHERE  contenidos.idUsuario=usuarios.idUsuario and idOrganizacion is null and idCliente is null AND contenidos.idContenido=? ORDER BY contenidos.idContenido DESC;");
		$stm->execute(array($idContenido));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
		//Metodo para obtener el contenido de un negocio
	public function Cambios($idRegistro)
	{
		$stm = $this->pdo->prepare("SELECT * FROM registro_cambios,usuarios WHERE registro_cambios.idUsuario=usuarios.idUsuario and idRegistro = ? ORDER BY registro_cambios.idRegistro DESC;");
		$stm->execute(array($idRegistro));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
	//Metodo para obtener el numero de actividades echas por cada usuario
	public function ActividadesUsuarios($idNegocio,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM actividades WHERE idNegocio = '$idNegocio' and idUsuario=$idUsuario;");
		$stm->execute();
		$A=$stm->fetchAll();
		return $A[0][0];
	}
	//Metodo parar traer la informacion de los clientes
	public function ConsultaClientes($idCliente)
	{
		if ($idCliente==null) {
			return 0;
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM personas, organizaciones, usuarios WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idUsuario = usuarios.idUsuario and idCliente=$idCliente;");
			$stm->execute();
			$A=$stm->fetchAll();
			return $A[0];
		}
	}
	//Metodo parar traer la informacion de los usuarios
	public function ConsultaUsuarios($idUsuario)
	{
		if ($idUsuario==null) {
			return 0;
		}else{
			$stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE idUsuario=?;");
			$stm->execute(array($idUsuario));
			$A=$stm->fetchAll();
			return $A[0];
		}
	}

	public function ListarBitacoras()
	{
		$stm = $this->pdo->prepare("SELECT * FROM bitacoras WHERE accion='Insert' ORDER BY timestamp;");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ObtenerInfoBitacora($tabla, $campo ,$id)
	{
		$query="SELECT idNegocio FROM " . $tabla . " WHERE ". $campo . " = " . $id;
		$stm = $this->pdo->prepare($query);
		$stm->execute();
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ObtenerStatusNegocio($idNegocio)
	{
		$query="SELECT status FROM negocios WHERE idNegocio=?";
		$stm = $this->pdo->prepare($query);
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);
	}
}
?>