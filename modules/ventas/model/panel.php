<?php
class Panel
{ 
	private $pdo;
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Metodo para obtener el contador de los negocios perdidos.
	public function ListarNegociosPerdidos($idEmbudo,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE status = 2 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}

		//Metodo para obtener el contador de los negocios perdidos con un rango de fechas.
	public function ListarNegociosPerdidosRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE status = 2 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
	
		//Metodo para obtener el contador de los negocios ganados.
	public function ListarNegociosGanados($idEmbudo, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE status = 1 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
			//Metodo para obtener el contador de los negocios ganados por rango de fechas.
	public function ListarNegociosGanadosRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE status = 1 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
	
		//Metodo para obtener el contador de los negocios nuevos.
	public function ListarNegociosNuevos($idEmbudo, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}

	public function ListarNegociosNuevosRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT COUNT(*) FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
	
		//Metodo para obtener el valor de los negocios ganados 
	public function ValorNegociosGanados($idEmbudo, $dolar, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);

	}
			//Metodo para obtener el valor de los negocios ganados por rango de fechas
	public function ValorNegociosGanadosRango($idEmbudo, $dolar, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	

		//Metodo para obtener el valor de los negocios perdidos 
	public function ValorNegociosPerdidos($idEmbudo, $dolar, $idUsuario)
	{

		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND negocios.status = 2 ");
		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);

	}
			//Metodo para obtener el valor de los negocios perdidos  por rango de fechas
	public function ValorNegociosPerdidosRango($idEmbudo, $dolar, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado) AND negocios.status = 2 ");
		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

			//Metodo para obtener el valor de los negocios perdidos 
	public function ValorNegociosNuevos($idEmbudo, $dolar,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
				//Metodo para obtener el valor de los negocios perdidos 
	public function ValorNegociosNuevosRango($idEmbudo, $dolar, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda, status FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute(); 
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	

	//Metodo para listar las actividades añadidas y el contador de cada una.
	public function ListarActividadesA($idEmbudo, $idUsuario){

		$stm = $this->pdo->prepare("SELECT tipo, COUNT(*) As 'Contador' FROM `actividades`, `negocios`, `embudos` WHERE actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') GROUP BY tipo ORDER BY Contador DESC");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);

	}
		//Metodo para listar las actividades añadidas y el contador de cada una por rango de fechas. 
	public function ListarActividadesARango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin){

		$stm = $this->pdo->prepare("SELECT tipo, COUNT(*) As 'Contador' FROM `actividades`, `negocios`, `embudos` WHERE actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') AND (actividades.timestamp >= '$fechaInicio') AND ('$fechaFin' >= actividades.timestamp) GROUP BY tipo ORDER BY Contador DESC");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);

	}
	

		//Metodo para obtener el contador maximo para las actividades añadidas.
	public function ObtenerMaximoAA($idEmbudo, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT MAX(counted) AS 'maximo' FROM ( SELECT COUNT(*) AS counted FROM actividades, negocios, embudos WHERE actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') GROUP BY tipo ) AS counts");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
			//Metodo para obtener el contador maximo para las actividades añadidas por rango de fechas.
	public function ObtenerMaximoAARango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT MAX(counted) AS 'maximo' FROM ( SELECT COUNT(*) AS counted FROM actividades, negocios, embudos WHERE actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') AND (actividades.timestamp >= '$fechaInicio') AND ('$fechaFin' >= actividades.timestamp) GROUP BY tipo ) AS counts");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
	

		//Metodo para listar las actividades completadas y el contador de cada una.
	public function ListarActividadesC($idEmbudo,$idUsuario){
		$stm = $this->pdo->prepare("SELECT tipo, COUNT(*) As 'Contador' FROM `actividades`, `negocios`, `embudos` WHERE completado = 1 AND actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') GROUP BY tipo ORDER BY Contador DESC");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
			//Metodo para listar las actividades completadas y el contador de cada una por rango.
	public function ListarActividadesCRango($idEmbudo,$idUsuario,$fechaInicio, $fechaFin){
		$stm = $this->pdo->prepare("SELECT tipo, COUNT(*) As 'Contador' FROM `actividades`, `negocios`, `embudos` WHERE completado = 1 AND actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') AND (actividades.fechaCompletado >= '$fechaInicio') AND ('$fechaFin' >= actividades.fechaCompletado) GROUP BY tipo ORDER BY Contador DESC");
		$stm->execute();

		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

			//Metodo para obtener el contador maximo para las actividades completadas.
	public function ObtenerMaximoAC($idEmbudo,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT MAX(counted) AS 'maximo' FROM ( SELECT COUNT(*) AS counted FROM actividades, negocios, embudos WHERE completado = 1 AND actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') GROUP BY tipo ) AS counts");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}
				//Metodo para obtener el contador maximo para las actividades completadas por rango de fechas.
	public function ObtenerMaximoACRango($idEmbudo,$idUsuario,$fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT MAX(counted) AS 'maximo' FROM ( SELECT COUNT(*) AS counted FROM actividades, negocios, embudos WHERE completado = 1 AND actividades.idNegocio = negocios.idNegocio AND negocios.idEmbudo = embudos.idEmbudo AND embudos.idEmbudo = $idEmbudo AND (actividades.idUsuario like '%$idUsuario%') AND (actividades.fechaCompletado >= '$fechaInicio') AND ('$fechaFin' >= actividades.fechaCompletado) GROUP BY tipo ) AS counts");
		$stm->execute();
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}

	//Metodo para listar los usuarios de forma descendente segun sea el número de negocios iniciados.
	public function NegociosIniciadorPor($idEmbudo,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT foto, nombreUsuario, COUNT(*) AS 'contador' FROM `negocios`, `usuarios` WHERE negocios.idUsuario = usuarios.idUsuario AND negocios.idEmbudo = $idEmbudo AND (negocios.idUsuario like '%$idUsuario%') group by nombreUsuario, foto ORDER BY contador DESC");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
		//Metodo para listar los usuarios de forma descendente segun sea el número de negocios iniciados y el rango de fecha establecidas en el input.
	public function NegociosIniciadorPorRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT foto, nombreUsuario, COUNT(*) AS 'contador' FROM `negocios`, `usuarios` WHERE negocios.idUsuario = usuarios.idUsuario AND negocios.idEmbudo = $idEmbudo AND (negocios.idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado) group by nombreUsuario, foto ORDER BY contador DESC");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

		//Metodo para listar los usuarios de forma descendente segun sea el número de negocios ganados.
	public function NegociosGanadosPor($idEmbudo,$idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT negocios.idUsuario, foto, nombreUsuario, COUNT(*) AS 'contador' FROM `negocios`, `usuarios` WHERE negocios.idUsuario = usuarios.idUsuario AND negocios.idEmbudo = $idEmbudo AND negocios.status = 1 AND (negocios.idUsuario like '%$idUsuario%') group by nombreUsuario, foto, idUsuario ORDER BY contador DESC");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
			//Metodo para listar los usuarios de forma descendente segun sea el número de negocios ganados y el renago de fechas establecido.
	public function NegociosGanadosPorRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT negocios.idUsuario, foto, nombreUsuario, COUNT(*) AS 'contador' FROM `negocios`, `usuarios` WHERE negocios.idUsuario = usuarios.idUsuario AND negocios.idEmbudo = $idEmbudo AND negocios.status = 1 AND (negocios.idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado) group by nombreUsuario, foto, idUsuario ORDER BY contador DESC");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

		//Metodo para obtener el valor de negocio segun el embudo y el usuario seleccionado.
	public function valorNegociosGanadosPorUsuario($idUsuario, $idEmbudo)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda FROM negocios WHERE idEmbudo = $idEmbudo AND idUsuario = $idUsuario AND status = 1 ");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
			//Metodo para obtener el valor de negocio segun el embudo y el usuario seleccionado y el rango de fechas establecidas en el input.
	public function valorNegociosGanadosPorUsuarioRango($idUsuario, $idEmbudo, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT valorNegocio, tipoMoneda FROM negocios WHERE idEmbudo = $idEmbudo AND idUsuario = $idUsuario AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado) AND status = 1 ");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

		//Metodo para listar los embudos
	public function ListarEmbudos()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM embudos ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Consultas para mostrar los negocios Iniciador en el modal de panel ...................................
	public function NegociosIniciados($idEmbudo,$idUsuario){
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute();
		$query = $stm->fetchAll(PDO::FETCH_OBJ);
		return $query;
	}

	public function NegociosIniciadosPorRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin){
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda FROM negocios WHERE idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute();
		$query = $stm->fetchAll(PDO::FETCH_OBJ);
		return $query;
	}
	// ........................................................................................................
		//Consultas para mostrar los negocios Iniciador en el modal de panel ...................................
	public function NegociosPerdidos($idEmbudo,$idUsuario){
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda, razonPerdido  FROM negocios, negocios_perdidos WHERE status = 2 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND negocios.idNegocio = negocios_perdidos.idNegocio");
		$stm->execute();
		$query = $stm->fetchAll(PDO::FETCH_OBJ);
		return $query;
	}

	public function NegociosPerdidosPorRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin){
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda, razonPerdido FROM negocios, negocios_perdidos WHERE status = 2 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado) AND negocios.idNegocio = negocios_perdidos.idNegocio");
		$stm->execute();
		$query = $stm->fetchAll(PDO::FETCH_OBJ);
		return $query;
	}
	// ........................................................................................................
	//Metodo para obtener el contador de los negocios ganados.................................................
	public function NegociosGanados($idEmbudo, $idUsuario)
	{
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda FROM negocios WHERE status = 1 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%')");
		$stm->execute();
		$query = $stm->fetchAll(PDO::FETCH_OBJ);
		return $query;
	}
			//Metodo para obtener el contador de los negocios ganados por rango de fechas.
	public function ListarNegociosGanadosPorRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin)
	{
		$stm = $this->pdo->prepare("SELECT claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda FROM negocios WHERE status = 1 AND idEmbudo = $idEmbudo AND (idUsuario like '%$idUsuario%') AND (negocios.fechaCreado >= '$fechaInicio') AND ('$fechaFin' >= negocios.fechaCreado)");
		$stm->execute();
		$query = $stm->fetchAll(PDO::FETCH_OBJ);
		return $query;
	}
	
}
?>