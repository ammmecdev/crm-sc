<?php
class Persona
{
	private $pdo;
	public $idCliente;
	public $idOrganizacion;
	public $idUsuario;
	public $nombrePersona;
	public $telefono;
	public $extension;
	public $tipoTelefono;
	public $email;
	public $honorifico;
	public $puesto;
	public $nombreOrganizacion;
	
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para listar
	public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT idCliente, nombreUsuario, personas.honorifico, personas.nombrePersona, organizaciones.idOrganizacion, organizaciones.nombreOrganizacion, personas.telefono, personas.extension, personas.tipoTelefono, personas.email, personas.puesto FROM personas, organizaciones, usuarios WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idUsuario = usuarios.idUsuario");
		$stm->execute();
		return $stm;
	}

	//Metodo para la paginación
	public function ListarContactos($inf, $sup)
	{
		$stm = $this->pdo->prepare("SELECT idCliente, nombreUsuario, personas.honorifico, personas.nombrePersona, organizaciones.idOrganizacion, organizaciones.nombreOrganizacion, personas.telefono, personas.extension, personas.tipoTelefono, personas.email, personas.puesto FROM personas, organizaciones, usuarios WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idUsuario = usuarios.idUsuario ORDER by personas.nombrePersona ASC LIMIT $inf, $sup");
		$stm->execute();
		return $stm;
	}

	public function Listar2()
	{
		$stm = $this->pdo->prepare("SELECT idCliente, nombreUsuario, personas.honorifico, personas.nombrePersona, organizaciones.idOrganizacion, organizaciones.nombreOrganizacion, personas.telefono, personas.extension, personas.tipoTelefono, personas.email, personas.puesto FROM personas, organizaciones, usuarios WHERE personas.idOrganizacion = organizaciones.idOrganizacion AND personas.idUsuario = usuarios.idUsuario");

		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	//Metodo para listar todos las organizaciones para el select
	public function ListarOrganizaciones()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM organizaciones");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	//Metodo para listar todos las personas para el select
	public function ListarPersonas()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM personas");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListarPersonasDeOrganizacion($idOrganizacion)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM personas WHERE idOrganizacion = ?");
			$stm->execute(array($idOrganizacion));
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Metodo para contar las personas
	public function Contador()
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT COUNT(*) FROM personas");
			$stm->execute();
			$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
			return $cont;	
		}
		catch(Exception $e)
		{
			die($e->getMessage());
			$this->error=true;
			$this->mensaje="Se ha producido un error";
		}
	}
	//Metodo para Registrar
	public function Registrar(Persona $data)
	{
		//echo 'entro';
		$sql ="INSERT INTO personas VALUES(?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idOrganizacion,
				$data->idUsuario,
				$data->nombrePersona,
				$data->telefono,
				$data->extension,
				$data->tipoTelefono,
				$data->email,
				$data->honorifico,
				$data->puesto	
			)
		);

		$this->RegistrarBitacora($this->pdo->lastInsertId(),'Insert', 'personas');
	}
	
	public function Actualizar(Persona $data)
	{
		$sql = "UPDATE  personas SET idOrganizacion=?,
		nombrePersona=?,
		telefono=?,
		extension=?,
		tipoTelefono=?,
		email=?,
		honorifico=?,
		puesto=?
		WHERE  idCliente=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idOrganizacion,
				$data->nombrePersona,
				$data->telefono,
				$data->extension,
				$data->tipoTelefono,
				$data->email,
				$data->honorifico,
				$data->puesto,
				$data->idCliente	
			)
		);

		$this->RegistrarBitacora($data->idCliente,'Update', 'personas');
	}
	//Metdo para eliminar
	public function Eliminar($idCliente)
	{
		$idBitacora = $this->RegistrarBitacora($idCliente,'delete','personas');
		$this->RegistrarNotasBitacora($idBitacora,$idCliente);

		$sql ="DELETE FROM personas WHERE idCliente=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCliente
			)
		);
	}
	public function ConsultaPersonas($valorBusqueda, $bAlfabetica)
	{
		$stm = $this->pdo->prepare("SELECT idCliente, personas.honorifico, personas.nombrePersona, organizaciones.idOrganizacion, organizaciones.nombreOrganizacion, personas.telefono, personas.extension, personas.tipoTelefono, personas.email, personas.puesto, nombreUsuario FROM personas, organizaciones, usuarios WHERE organizaciones.idUsuario = usuarios.idUsuario and personas.idOrganizacion = organizaciones.idOrganizacion and nombrePersona LIKE '$bAlfabetica%' and (nombrePersona LIKE '%$valorBusqueda%' or nombreOrganizacion like '%$valorBusqueda%' or personas.telefono like '%$valorBusqueda%' or extension like '%$valorBusqueda%' or personas.email like '%$valorBusqueda%' or puesto like '%$valorBusqueda%'  or usuarios.nombreUsuario like '%$valorBusqueda%');");
		$stm->execute();
		return $stm;
	}
	

	//Metodo para obtener el Id de un Cliente
	public function Obtener($idCliente)
	{
		try 
		{
			$stm = $this->pdo
			->prepare("SELECT * FROM personas WHERE idCliente = ?");

			$stm->execute(array($idCliente));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function contactos()
	{
		$stm = $this->pdo->prepare("SELECT count(*) as totalRegistros FROM personas");
		$stm->execute();
		return $stm->fetch(PDO::FETCH_COLUMN);
	}


 //Metodo Para registrar en la bitacora de las acciones
	public function RegistrarBitacora($idRelacion, $accion, $table)
	{
		$sql ="INSERT INTO bitacoras VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$accion,
				$table,
				$idRelacion,
				$_SESSION['idUsuario'],
				date("Y-m-d H:i:s")
			)
		);
		return $this->pdo->lastInsertId();
	}

	public function RegistrarNotasBitacora($idBitacora,$idCliente){
		$stm = $this->pdo->prepare("SELECT personas.nombrePersona, organizaciones.nombreOrganizacion FROM personas INNER JOIN organizaciones ON personas.idOrganizacion = organizaciones.idOrganizacion WHERE personas.idCliente = ?");
		$stm->execute(array($idCliente));
		$result = $stm->fetch(PDO::FETCH_OBJ);

		$sql = "INSERT INTO notas_bitacoras	VALUES (?,?,?)";
		$this->pdo->prepare($sql)
			->execute(
				array(
					$idBitacora,
					"Nombre de la Persona",
					$result->nombrePersona,
				)
			
		);

		$this->pdo->prepare($sql)
			->execute(
				array(
					$idBitacora,
					"Nombre de la Organización",
					$result->nombreOrganizacion,
				)
			
		);
	}
}