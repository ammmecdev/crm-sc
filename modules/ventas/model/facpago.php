<?php
class Facpago
{
	public $idFactura;
	public $idCotizacion;
	public $noFactura;
	public $fechaFactura;
	public $ruta;
	public $idPago;
	public $fechaPago;
	public $fechaRecibo;
	public $noPago;
	public $pdo;

	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}
	public function RegistrarFactura(Facpago $data)
	{
		$sql ="INSERT INTO facturacion VALUES(?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idCotizacion,
				$data->noFactura,
				$data->fechaFactura,
				$data->ruta
			)
		);
	}
	public function RegistrarPago(Facpago $data)
	{
		$sql ="INSERT INTO pago VALUES(?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idCotizacion,
				$data->fechaPago,
				$data->fechaRecibo,
				$data->ruta,
				$data->noPago
			)
		);
	}
	public function ListarNegocios(){
		$stm = $this->pdo->prepare("SELECT negocios.idNegocio, tituloNegocio  from cotizacion, costo_operativo, negocios where negocios.idNegocio = costo_operativo.idNegocio and costo_operativo.idCostoOperativo = cotizacion.idCostoOperativo");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
	public function ListarFactura(){
		$stm = $this->pdo->prepare("SELECT *  from facturacion");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}
	public function ListarPago(){
		$stm = $this->pdo->prepare("SELECT *  from pago");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}
	public function ActualizarFactura(Facpago $data)
	{
		$sql ="UPDATE facturacion SET 
		idCotizacion=?,
		noFactura=?,
		fechaFactura=?,
		ruta=?
		WHERE idFactura=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idCotizacion,
				$data->noFactura,
				$data->fechaFactura,
				$data->ruta,
				$data->idFactura
			)
		);
	}
		public function ActualizarPago(Facpago $data)
	{
		$sql ="UPDATE pago SET 
		idCotizacion=?,
		fechaPago=?,
		fechaRecibo=?,
		ruta=?,
		noPago=?
		WHERE idPago=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->idCotizacion,
				$data->fechaPago,
				$data->fechaRecibo,
				$data->ruta,
				$data->noPago,
				$data->idPago
			)
		);
	}	
}
?>