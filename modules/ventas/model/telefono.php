<?php
class Telefono
{    
	public $idTelefono;
	public $extension;
	public $telefono;
	public $tipoTelefono;
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListarTelefonos($idCliente)
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas, telefonos WHERE personas.idCliente = telefonos.idCliente AND personas.idCliente = ?");
		$stm->execute(array($idCliente));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function AgregarTelefono(Telefono $data)
	{
		$sql ="INSERT INTO telefonos VALUES(?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->telefono,
				$data->extension,
				$data->tipoTelefono,
				$data->idCliente
			)
		);
	}

	public function ActualizarTelefono(Telefono $data)
	{
		$sql ="UPDATE telefonos SET 
		telefono=?,
		extension=?,
		tipoTelefono=?,
		idCliente=?
		WHERE idTelefono=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->telefono,
				$data->extension,
				$data->tipoTelefono,
				$data->idCliente,
				$data->idTelefono
			)
		);
	}

	public function EliminarTelefono($idCliente, $idTelefono)
	{
		$sql ="DELETE FROM telefonos WHERE idCliente = ? AND idTelefono = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCliente,
				$idTelefono
			)
		);
	}
}
?>
