<?php
class Costo
{    
	public $idCostoOperativo;
	public $fechaEmision;
	public $fechaEntrega;
	public $requisicion;
	public $idNegocio;
	public $idUsuario;
	public $costoOperativo;
	public $tiempoEntrega;
	public $condicionesPago;
	public $observaciones;
	public $idContacto;
	public $status;

	public $idSerCO;
	public $clave;
	public $descripcion;
	public $unidad;
	public $cantidad;
	public $precioUnitario;
	public $importe;

	private $pdo;
	
	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	//Metodo para listar las solicitudes de costos
	public function ListarSolicitudes($bTexto)
	{
		$stm = $this->pdo->prepare("SELECT * FROM costo_operativo as co, negocios as n WHERE co.idNegocio = n.idNegocio");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);	
	}

	public function RegistrarSolicitud(Costo $data)
	{
		$sql ="INSERT INTO costo_operativo VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->fechaEmision,
				$data->fechaEntrega,
				$data->requisicion,
				$data->idNegocio,
				null,
				null,
				"",
				"",
				"",
				$data->idContacto,
				"pendiente"
			)
		);
		return $this->pdo->lastInsertId(); 
	}

	//Metodo para actualizar solicitud de costo operativo
	public function ActualizarSolicitud(Costo $data)
	{
		$sql ="UPDATE costo_operativo SET 
		fechaEmision=?,
		fechaEntrega=?,
		requisicion=?,
		idNegocio=?,
		idContacto=?
		WHERE idCostoOperativo=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->fechaEmision,
				$data->fechaEntrega,
				$data->requisicion,
				$data->idNegocio,
				$data->idContacto,
				$data->idCostoOperativo
			)
		);
	}

	//Metodo para eliminar solicitud
	public function Eliminar($idCostoOperativo)
	{
		$sql ="DELETE FROM costo_opetativo WHERE idCostoOperativo=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCostoOperativo
			)
		);
	}


	//Metodo para registrar los servicios que se van agregando al carrito de servicios
	public function AgregarServicio(Costo $data)
	{
		$sql ="INSERT INTO servicios_costo_operativo VALUES(?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->idCostoOperativo,
				$data->clave,
				$data->descripcion,
				$data->unidad,
				$data->cantidad,
				null,
				null
			)
		);
	}

	public function ObtenerServicio($idSerCO)
	{
		$stm = $this->pdo
		->prepare("SELECT * FROM servicios_costo_operativo WHERE idSerCO = ?");
		$stm->execute(array($idSerCO));
		return $stm->fetch(PDO::FETCH_OBJ);
	}

	public function ActualizarServicio(Costo $data)
	{
		$sql ="UPDATE servicios_costo_operativo SET 
		clave=?,
		descripcion=?,
		unidad=?,
		cantidad=?
		WHERE idSerCO=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->clave,
				$data->descripcion,
				$data->unidad,
				$data->cantidad,
				$data->idSerCO
			)
		);
	}

	//Metodo para eliminar solicitud
	public function EliminarServicio($idCostoOperativo, $idSerCO)
	{
		$sql ="DELETE FROM servicios_costo_operativo WHERE idCostoOperativo=? AND idSerCO=?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idCostoOperativo,
				$idSerCO
			)
		);
	}

	public function ConsultarProyectoClientePorNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT tituloNegocio, nombreOrganizacion FROM negocios AS n, organizaciones AS o WHERE n.idOrganizacion = o.idOrganizacion AND idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetch(PDO::FETCH_OBJ);

	}

	public function ConsultarContactosNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT idCliente, nombrePersona FROM negocios AS n, organizaciones AS o, personas AS p WHERE n.idOrganizacion = o.idOrganizacion AND n.idOrganizacion = o.idOrganizacion AND idNegocio = $idNegocio");
		$stm->execute();
		return $stm;
	}

		//Metodo para listar personas por negocio
	public function ListarPersonasPorNegocio($idNegocio)
	{
		$stm = $this->pdo->prepare("SELECT * FROM personas, negocios, pertenece  WHERE personas.idCliente = pertenece.idCliente AND pertenece.idNegocio = negocios.idNegocio AND negocios.idNegocio = ?");
		$stm->execute(array($idNegocio));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}

	public function ListarCarrito($idCostoOperativo)
	{
		$stm = $this->pdo->prepare("SELECT * FROM servicios_costo_operativo AS sco, costo_operativo AS co WHERE sco.idCostoOperativo=co.idCostoOperativo AND co.idCostoOperativo = ?");
		$stm->execute(array($idCostoOperativo));
		return $stm->fetchAll(PDO::FETCH_OBJ);
	}
}