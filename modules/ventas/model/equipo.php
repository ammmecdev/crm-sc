<?php
class Equipo
{
	public $idEquipo;
	public $clave;
	public $nombreEquipo;
	private $pdo;

	public function __CONSTRUCT()
	{
		$this->pdo = Database::StartUp();     
	}

	//Metodo para listar datos
	public function Listar()
	{
		$stm = $this->pdo->prepare("SELECT * FROM equipo");
		$stm->execute();			
		return $stm;
	}

		//Metodo para listar datos
	public function consulta($bTexto, $bAlfabetica)
	{
		$stm = $this->pdo->prepare("SELECT * FROM equipo WHERE nombreEquipo like '$bAlfabetica%' AND (clave like '%$bTexto%' OR nombreEquipo like '%$bTexto%') ");
		$stm->execute();			
		return $stm;
	}

		    //Metodo para registrar equipos
	public function Registrar(Equipo $data)
	{
		$sql ="INSERT INTO equipo VALUES(?,?,?)";
		$this->pdo->prepare($sql)
		->execute(
			array(
				null,
				$data->nombreEquipo,  
				$data->clave,	          
			)
		);
	}

	public function Actualizar(Equipo $data)
	{
		$sql = "UPDATE equipo SET
		clave = ?,
		nombreEquipo = ?
		WHERE  idEquipo = ? ";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$data->clave,
				$data->nombreEquipo,
				$data->idEquipo	
			)
		);
	}

	public function Eliminar($idEquipo)
	{
		$sql ="DELETE FROM equipo WHERE idEquipo = ?";
		$this->pdo->prepare($sql)
		->execute(
			array(
				$idEquipo
			)
		);
	}

	public function verificarClave($clave, $idEquipo)
	{
		if ($idEquipo == "")
			$sql = $this->pdo->prepare("SELECT * FROM equipo WHERE clave = '$clave'");
		else
			$sql = $this->pdo->prepare("SELECT * FROM equipo WHERE clave = '$clave' AND idEquipo <> $idEquipo");
		$sql->execute();
		$result = implode($sql->fetchAll(PDO::FETCH_COLUMN));	
		return $result;					
	}

	public function consecutivo() //Metodo para traer el consecutivo mayor de la tabla de equipos
	{
		$stm = $this->pdo->prepare("SELECT consecutivo from equipo order by consecutivo DESC limit 1");
		$stm->execute(); 
		$cont = implode($stm->fetchAll(PDO::FETCH_COLUMN));
		return $cont;
	}


}//fin de la clase

?>