<?php
require_once 'model/dealcliente.php';

class DealclienteController {
	
	public function __CONSTRUCT()
	{
		try{
			$this->model = new DealCliente();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function Index(){
		$page="view/deal/dealcliente.php";
		$contenedor="view/dealcliente/contenedor.php";
		require_once '../../view/index.php';
	}

	public function Consultaas2(){
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d");
		$idOrganizacion=$_REQUEST['idOrganizacion'];
		$valorTipoTarea = isset($_REQUEST['valor']) ? $_REQUEST['valor'] : '';
		$Resultado=$this->model->Detalles($idOrganizacion);

		$idEmbudo=$_REQUEST['idEmbudo'];

		$NegociosGanados=$this->model->ListarNegociosGanados($idOrganizacion);
		$NegociosPerdidos=$this->model->ListarNegociosPerdidos($idOrganizacion);
		$NegociosCerrados=((count($NegociosGanados))+(count($NegociosPerdidos)));

		($NegociosGanados!=null) ? $PorcietoG=((count($NegociosGanados)*100)/$NegociosCerrados):$PorcietoG=0;

		($NegociosPerdidos!=null) ? $PorcietoP=((count($NegociosPerdidos)*100)/$NegociosCerrados):$PorcietoP=0;
		$SumaGanados=1;
		$SumaPerdidos=2;
		$SumaGanados=$this->model->ConsultaValorNegociosCerrados($idOrganizacion,$SumaGanados);
		$SumaPerdidos=$this->model->ConsultaValorNegociosCerrados($idOrganizacion,$SumaPerdidos);



		$arrayLineaTiempo=array();
		$contenidoAct=$this->model->Actividades($idOrganizacion);
		foreach ($contenidoAct as $r):
			$row_array["tipov"] = 1;
			$row_array['idActividad']=$r->idActividad;
			$row_array['fechaCompletado']=$r->fechaCompletado;
			$row_array['fecha']=$r->fechaActividad;
			$row_array['tipo']=$r->tipo;
			$row_array['notas']=$r->notas;
			$row_array['duracion']=$r->duracion;
			$row_array['completado']=$r->completado;
			$row_array['horaInicio']=$r->horaInicio;
			$row_array['idOrganizacion']=$r->idOrganizacion;
			$row_array['horaFin']=$r->fechaActividad;
			$row_array['nombrePersona']=$r->nombrePersona;
			$row_array['tituloNegocio']=$r->tituloNegocio;
			$row_array['idNegocio']=$r->idNegocio;
			$row_array['nombreOrganizacion']=$r->nombreOrganizacion;
			$row_array['nombreUsuario']=$r->nombreUsuario;
			array_push($arrayLineaTiempo, $row_array);
		endforeach;	 

		$contenidoNotas=$this->model->ContenidosNotas($idOrganizacion);
		foreach ($contenidoNotas as $r):
			$row_arrayN["tipov"] = 2;
			$row_arrayN["notas"] = $r->notas;
			$row_arrayN["idContenido"] = $r->idContenido;
			$row_arrayN["fecha"] = $r->fechaSubido;
			$row_arrayN["nombreUsuario"] = $r->nombreUsuario;
			array_push($arrayLineaTiempo, $row_arrayN);
		endforeach;	

		$contenidoArchivos=$this->model->ContenidosArchivos($idOrganizacion);
		foreach ($contenidoArchivos as $r):
			$row_arrayA["tipov"] = 3;
			$row_arrayA["archivos"] = $r->archivos;
			$row_arrayA["size"] = $r->size;
			$row_arrayA["idContenido"] = $r->idContenido;
			$row_arrayA["fecha"] = $r->fechaSubido;
			$row_arrayA["nombreUsuario"] = $r->nombreUsuario;
			array_push($arrayLineaTiempo, $row_arrayA);
		endforeach;	

		$contenidoNegociosCreados=$this->model->ListarNegocios($idOrganizacion);
		foreach ($contenidoNegociosCreados as $r):

			if (isset($r->fechaGanadoPerdido)) {
				$row_arrayGP["tipov"] = 5;
				$row_arrayGP["fecha"] = $r->fechaGanadoPerdido;
				$row_arrayGP["idNegocio"] = $r->idNegocio;
				$row_arrayGP["status"] = $r->status;
				$row_arrayGP["tituloNegocio"] = $r->tituloNegocio;
				$row_arrayGP["claveOrganizacion"] = $r->claveOrganizacion;
				$row_arrayGP["claveConsecutivo"] = $r->claveConsecutivo;
				$row_arrayGP["claveServicio"] = $r->claveServicio;
				$row_arrayGP["valorNegocio"] = $r->valorNegocio;
				$row_arrayGP["tipoMoneda"] = $r->tipoMoneda;
				$row_arrayGP["nombreUsuario"] = $r->nombreUsuario;
				array_push($arrayLineaTiempo, $row_arrayGP);
			}
			$row_array["tipov"] = 5;
			$row_array["fecha"] = $r->fechaCreado;
			$row_array["status"] = 0;
			$row_array["idNegocio"] = $r->idNegocio;
			$row_array["tituloNegocio"] = $r->tituloNegocio;
			$row_array["claveOrganizacion"] = $r->claveOrganizacion;
			$row_array["claveConsecutivo"] = $r->claveConsecutivo;
			$row_array["claveServicio"] = $r->claveServicio;
			$row_array["valorNegocio"] = $r->valorNegocio;
			$row_array["tipoMoneda"] = $r->tipoMoneda;
			$row_array["nombreUsuario"] = $r->nombreUsuario;
			array_push($arrayLineaTiempo, $row_array);
		endforeach;	


		function cmp($a, $b) {
			if ($a['fecha'] == $b['fecha']) {
				return 0;
			}

			return ($a['fecha'] < $b['fecha']) ? -1 : 1;
		}
		uasort($arrayLineaTiempo, 'cmp');
		$arrayLineaTiempo = array_reverse($arrayLineaTiempo);
		echo '
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="timeline-centered">
		<div style="text-align: center;">
		<span class="label label-default">Planeadas</span>
		</div>';
		if ($valorTipoTarea==0 || $valorTipoTarea==1) {
			foreach ($arrayLineaTiempo as $r  => $value):
				if ($value['fecha']>=$FechaAc && $value['tipov']==1) {
					echo '
					<!--Actividades-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-primary">
					<i class="fas fa-calendar-alt"></i>
					</div>
					<div class="timeline-label">
					<div class="row">
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
					<div class="btn-group">
					<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#mTerminar" onclick="cambiaEstado('.$value['idActividad'].','.$value['completado'].',';
					echo "'".$value['fechaCompletado']."'".')">';
					if ($value['completado']==0) {
						echo '<i class="far fa-circle" style="font-size: 16px;"></i>';
					}else{
						echo '<i class="fas fa-check-circle" style="font-size: 16px; color: green;"></i>';
					}
					echo '
					</button>
					</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-9 col-lg-10">
					<h2 style="padding-top: 4px;"><a>';
					if ($value['tipo']=="Llamada") {
						echo '<i class="mdi mdi-phone-in-talk"></i> ';
					}elseif ($value['tipo']=="Reunión") {
						echo '<i class="mdi mdi-group mdi-lg"></i> ';
					}elseif ($value['tipo']=="Tarea") {
						echo '<i class="far fa-clock" style="font-size: 15px;"></i> ';
					}elseif ($value['tipo']=="Plazo") {
						echo '<i class="fas fa-hourglass-half" style="font-size: 14px;"></i> ';
					}elseif ($value['tipo']=="Email") {
						echo '<i class="fas fa-envelope" style="font-size: 16px;"></i> ';
					}elseif ($value['tipo']=="Comida") {
						echo '<i class="fas fa-utensils" style="font-size: 14px;"></i> ';
					}else{
						echo '<i class="fab fa-whatsapp" style="font-size: 16px;"></i> ';
					}
					echo $value['tipo'].'</a> <span style="font-size: 14px;">';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
					echo ' - '.$value['horaInicio'].'</span>
					</h2>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
					<button type="button" class="btn btn-link dropdown-toggle" id="dropdownAct" data-toggle="dropdown" >
					<i class="far fa-edit" style="font-size: 16px;"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownAct">
					<li role="presentation" id="ya"><a data-toggle="tab" href="#actividad" onclick="editarActividad('."'".$value['idActividad'].",".$value['tipo']."".",".$value['notas']."".",".$value['fecha'].",".$value['duracion']."".",".$value['horaInicio'].",".$value['idOrganizacion'].",".$value['nombreOrganizacion'].",".$value['idNegocio'].",".$value['tituloNegocio']."".",".$value['nombrePersona']."'".');">Editar</a></li>
					<li><a href="#" data-toggle="modal" data-target="#mEliminarA" onclick="eliminarActividad('.$value['idActividad'].'); return false">Eliminar</a></li>
					</ul>
					</div>
					</div>
					<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<i class="fas fa-user"></i>
					<p">'.$value['nombrePersona'].'</button>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<i class="mdi mdi-business mdi-lg"></i>
					<p">'.$value['nombreOrganizacion'].'</p>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 hidden-xs hidden-sm" align="right">
					<p class="help-block">'.$value['nombreUsuario'].'</p>
					</div>
					</div>
					</div>

					<div class="row">
					<div class="col-lg-12">
					<hr style="margin-top: 3px; margin-bottom: 10px;">
					</div>
					<div class="col-lg-12">
					<div class="form-group">
					<textarea name="" class="form-control" id="txtNotas" readonly>'.$value['notas'].'</textarea>
					</div>
					</div>
					</div>
					</div>
					</div>
					</article>
					<!--Actividades-->';
				}
			endforeach;
		}

		echo '<div style="text-align: center;">
		<span class="label label-default">Pasadas</span>
		</div>';
		foreach ($arrayLineaTiempo as $r  => $value):
			if ($valorTipoTarea==0 || $valorTipoTarea==2) {
				if ($value['tipov']==2) {
					echo '<!--Notas-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-success">
					<i class="glyphicon glyphicon-comment"></i>
					</div>
					<div class="timeline-label">
					<div class="row">
					<div class="col-xs-2 col-lg-1" align="center" style="padding-right: 0px;">
					<i class="mdi mdi-business mdi-lg"></i>
					</div>
					<div class="col-xs-8 col-lg-8" style="padding-left: 0px;">
					<h2><a>'.$Resultado['nombreOrganizacion'].'</a></h2>
					</div>
					<div class="col-xs-2 col-lg-3" align="right">
					<button type="button" class="btn btn-link dropdown-toggle" id="dropdownNota" data-toggle="dropdown" >
					<i class="far fa-edit" style="font-size: 16px;"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownNota">
					<li><a href="#" onclick="quitarReadOnly(';echo "'txtNotas".$value['idContenido']."'"; echo '); return false">Editar</a></li>
					<li><a href="#" onclick="eliminarNota('.$value['idContenido'].'); return false">Eliminar</a></li>
					</ul>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12">
					<div class="col-lg-3">
					<p class="help-block">';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
					echo '</p>
					</div>
					<div class="col-lg-9" align="right">
					<p class="help-block">'.$value['nombreUsuario'].'</p>
					</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12">
					<hr style="margin-top: 3px; margin-bottom: 10px;">
					</div>
					<div class="col-lg-12">
					<div class="form-group">
					<textarea name="" class="form-control" id="txtNotas'.$value['idContenido'].'" readonly>'.$value['notas'].'</textarea>
					</div>
					</div>						
					</div>
					</div>
					</div>
					</article>
					<!--Notas-->';
				}
			} 

			if ($valorTipoTarea==0 || $valorTipoTarea==1) {
				if ($value['tipov']==1) {
					if ($value['fecha']<$FechaAc) {
						echo '<!--Actividades-->
						<article class="timeline-entry">
						<div class="timeline-entry-inner">
						<div class="timeline-icon bg-primary">
						<i class="fas fa-calendar-alt"></i>
						</div>
						<div class="timeline-label">
						<div class="row">
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
						<div class="btn-group">
						<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#mTerminar" onclick="cambiaEstado('.$value['idActividad'].','.$value['completado'].',';
						echo "'".$value['fechaCompletado']."'".')">';
						if ($value['completado']==0) {
							echo '<i class="far fa-circle" style="font-size: 16px;"></i>';
						}else{
							echo '<i class="fas fa-check-circle" style="font-size: 16px; color: green;"></i>';
						}
						echo '</button>
						</div>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-9 col-lg-10">
						<h2 style="padding-top: 4px;"><a>';
						if ($value['tipo']=="Llamada") {
							echo '<i class="mdi mdi-phone-in-talk"></i> ';
						}elseif ($value['tipo']=="Reunión") {
							echo '<i class="mdi mdi-group mdi-lg"></i> ';
						}elseif ($value['tipo']=="Tarea") {
							echo '<i class="far fa-clock" style="font-size: 15px;"></i> ';
						}elseif ($value['tipo']=="Plazo") {
							echo '<i class="fas fa-hourglass-half" style="font-size: 14px;"></i> ';
						}elseif ($value['tipo']=="Email") {
							echo '<i class="fas fa-envelope" style="font-size: 16px;"></i> ';
						}elseif ($value['tipo']=="Comida") {
							echo '<i class="fas fa-utensils" style="font-size: 14px;"></i> ';
						}else{
							echo '<i class="fab fa-whatsapp" style="font-size: 16px;"></i> ';
						}
						echo $value['tipo'].'</a> <span style="font-size: 14px;">';
						$date = substr($value['fecha'], 5, 2);
						$NomMes=$this->model->NombreMes($date);
						echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
						echo ' - '.$value['horaInicio'].'</span>
						</h2>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
						<button type="button" class="btn btn-link dropdown-toggle" id="dropdownAct" data-toggle="dropdown" >
						<i class="far fa-edit" style="font-size: 16px;"></i>
						</button>
						<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownAct">
						<li role="presentation" id="ya2"><a data-toggle="tab" href="#actividad" onclick="editarActividad('."'".$value['idActividad'].",".$value['tipo']."".",".$value['notas']."".",".$value['fecha'].",".$value['duracion']."".",".$value['horaInicio'].",".$value['idOrganizacion'].",".$value['nombreOrganizacion'].",".$value['idNegocio'].",".$value['tituloNegocio']."".",".$value['nombrePersona']."'".');">Editar</a></li>
						<li><a href="#" data-toggle="modal" data-target="#mEliminarA" onclick="eliminarActividad('.$value['idActividad'].'); return false">Eliminar</a></li>
						</ul>
						</div>
						</div>
						<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<i class="fas fa-user"></i>
						<p">'.$value['nombrePersona'].'</buttop>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<i class="mdi mdi-business mdi-lg"></i>
						<p">'.$value['nombreOrganizacion'].'</p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 hidden-xs hidden-sm" align="right">
						<p class="help-block">'.$value['nombreUsuario'].'</p>
						</div>
						</div>
						</div>
						<div class="row">
						<div class="col-lg-12">
						<hr style="margin-top: 3px; margin-bottom: 10px;">
						</div>
						<div class="col-lg-12">
						<div class="form-group">
						<textarea name="" class="form-control" id="txtNotas" readonly>'.$value['notas'].'</textarea>
						</div>
						</div>
						</div>
						</div>
						</div>
						</article>
						<!--Actividades-->';
					}
				}
			}
			if ($valorTipoTarea==0 || $valorTipoTarea==5) {
				if ($value['tipov']==5) {
					echo '<!--Negocios-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-danger">
					<i class="fas fa-dollar-sign"></i>
					</div>
					<div class="timeline-label">
					<h2>';
					if ($value['status']==2) {
						echo '<a style="color:red">Negocio perdido: </a>';
					}elseif ($value['status']==1) {
						echo '<a style="color:green">Negocio ganado: </a>';
					}else{
						echo 'Negocio creado: ';
					}



					echo '<a href="?c=deal&idNegocio='.$value['idNegocio'].'"><span>'.$value['claveOrganizacion'].'-'.$value['claveConsecutivo'].'-'.$value['claveServicio'].'</span></a></h2>
					<hr style="margin-top: 0px; ">
					<p>'.$value['tituloNegocio'].' $'.$value['valorNegocio'].$value['tipoMoneda']."<br>";
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4).' - '.$value['nombreUsuario'];
					echo '</p>
					</div>
					</div>
					</article>
					<!--Negocios-->';
				}
			}
			echo '<!--Cambios
			<article class="timeline-entry">
			<div class="timeline-entry-inner">
			<div class="timeline-icon bg-default">
			<i class="fas fa-exchange-alt"></i>
			</div>
			<div class="timeline-label">
			<h2><a>dfg</a><a>dfgdf</a> <i class="fas fa-arrow-right"></i> <a>dfgdf</a></h2>
			<hr style="margin-top: 0px; ">
			<p></p>
			</div>
			</div>
			</article>
			Cambios-->

			<!--Correos
			<article class="timeline-entry">
			<div class="timeline-entry-inner">
			<div class="timeline-icon bg-info">
			<i class="fas fa-envelope"></i>
			</div>
			<div class="timeline-label">
			<h2><a href="#">Arlind Nushi</a> <span>checked in at</span> <a href="#">Laborator</a></h2>
			<hr style="margin-top: 0px; ">
			</div>
			</div>
			</article>
			Correos-->';

			if ($valorTipoTarea==0 || $valorTipoTarea==3) {
				if ($value['tipov']==3) {
					echo '<!--Archivos-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-warning">
					<i class="fas fa-paperclip"></i>
					</div>
					<div class="timeline-label">
					<div class="row">
					<div class="col-lg-1" align="center">
					<i class="far fa-file-pdf" style="font-size: 30px; color: red;"></i>
					</div>
					<div class="col-lg-8">
					<h2><a href="assets/files/archivosClientes/'.$value['archivos'].'" download="'.$value['archivos'].'">'.$value['archivos'].'</a></h2>
					</div>
					<div class="col-lg-3" align="right">
					<button type="button" class="btn btn-link dropdown-toggle" id="dropdownNota" data-toggle="dropdown" >
					<i class="far fa-edit" style="font-size: 16px;"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownNota">
					<li><a href="#" onclick="eliminarArchivo('.$value['idContenido'].",'".$value['archivos']."'".'); return false">Eliminar</a></li>
					</ul>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-4" align="center">
					<p class="help-block">';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
					echo'</p>
					</div>
					<div class="col-lg-4" align="center">
					<p class="help-block">'.$value['nombreUsuario'].'</p>
					</div>
					<div class="col-lg-4" align="center">
					<p class="help-block">'; 
					echo round(($value['size'] / 1024), 2)." Kb";
					echo '</p>
					</div>
					</div>
					</div>
					</div>
					</article>
					<!--Archivos-->';
				}
			}

			echo '<!--Cambios
			<article class="timeline-entry">
			<div class="timeline-entry-inner">
			<div class="timeline-icon bg-default">
			<i class="fas fa-exchange-alt"></i>
			</div>
			<div class="timeline-label">
			<h2><a>Negocio creado el</a></h2>
			<hr style="margin-top: 0px; ">
			<p></p>
			</div>
			</div>
			</article>
			Cambios-->
			';
		endforeach;
		echo '<!--POR DEFAULT-->
		<article class="timeline-entry begin">
		<div class="timeline-entry-inner">
		<div class="timeline-icon">
		<i class="fas fa-ellipsis-h"></i>
		</div>
		</div>
		</article>
		<!--POR DEFAULT-->
		</div>
		</div>
		<!--Columna Derecha-->

		<!--Modal Seguidores-->
		<div class="modal fade" id="mSeguidores" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header bg-success">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel"><strong>Agregar Seguidores</strong></h4>
		</div>
		<div class="modal-body">
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="form-group">
		<div class="input-group">
		<span class="input-group-addon"><i class="fas fa-search"></i></span>
		<select name="" id="" class="form-control selectpicker" data-live-search="true">
		<option value="">Seleccione un usuario</option>
		<option value="">Bruno Padilla Guerrero</option>
		<option value="">Alejandro Castro Saucedo</option>
		</select>
		</div>
		</div>
		</div>
		</div>
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-success">Guardar</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
		</div>
		</div>
		</div>
		<!--Modal Seguidores-->


		<script>

		tipoActividad = function ($tipo)
		{
			$("#txtTipo").val($tipo);
		} 
		function deshabilitaConfirmado(){
			$("#txtConfirmado").val("false");
			$("#divCruzadas").hide();
			$("#btnGuardar").val("Validar y guardar");
		}


		$( "#checkTiempo" ).on( "click", function() {
			if( $(this).is(":checked") ){
				$("#divTiempo").show();
				$("#txtHora").focus();
				$("#divCruzadas").show();
				$("#txtConfirmado").val("false");
			} else {
				$("#divTiempo").hide();
				$("#divCruzadas").html("");
				$("#divCruzadas").hide();
				$("#btnGuardar").val("Guardar");
			}
		});
		$('."'".'#form-actividades'."'".').submit(function() {
			$.ajax({
				type: '."'".'POST'."'".',
				url: $(this).attr('."'".'action'."'".'),
				data: $(this).serialize(),
				success: function(respuesta){
					console.log(respuesta);
					if(respuesta[0].mensaje == "cruzada"){
						$('."'".'#divCruzadas'."'".').show();
						$("#cruzadas").html("<label style='."'".'color:red;'."'".'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='."'".'color:blue; margin-bottom-80px;'."'".'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='."'".'right'."'".' style='."'".'color:blue'."'".'>¿Desea guardar de todas formas?</p>");
						$('."'".'#txtConfirmado'."'".').val('."'".'true'."'".');
						$('."'".'#btnGuardar'."'".').val('."'".'Confirmar'."'".');
					}else{
						$('."'".'#mActividades'."'".').modal('."'".'toggle'."'".');
						$("#mensajejs").html('."'".'<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'."'".'+respuesta[0].mensaje+'."'".'</center></strong></div>'."'".');
						$('."'".'#mensajejs'."'".').show();
						$('."'".'#mensajejs'."'".').delay(1000).hide(600);
						consulta();
					}
				}
			})      
			return false;
		});

		$('."'".'#form-negocios-eliminar'."'".').submit(function() {
			$.ajax({
				type: '."'".'POST'."'".',
				url: $(this).attr('."'".'action'."'".'),
				data: $(this).serialize(),
				success: function(respuesta) {
            //console.log(respuesta);
					$('."'".'#mNegocios'."'".').modal('."'".'toggle'."'".');
					$('."'".'#mEliminar'."'".').modal('."'".'toggle'."'".');
					$("#mensajejs").html('."'".'<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'."'".'+respuesta+'."'".'</center></strong></div>'."'".');
					$('."'".'#mensajejs'."'".').show();
					$('."'".'#mensajejs'."'".').delay(1000).hide(600); 
					consulta(); 
				}    
			})        
			return false;
		});  
		</script>';		
	}

	public function Consultas(){
		header('Content-Type: application/json');
		$idOrganizacion = $_GET['idOrganizacion'];
		$valor = $_GET['valor'];
		$org = $this->model->Detalles($idOrganizacion);
		$bitacoras = $this->model->ListarBitacoras();
		$arrayLineaTiempo = array();
		foreach($bitacoras as $b):

			if($b->tabla=='actividades' && ($valor==0 || $valor==1))
			{
				$res = $this->model->ObtenerInfoBitacora('actividades', 'idActividad', $b->idRelacion);
				if ($res != null){
					if($res->idOrganizacion==$idOrganizacion)
					{
						$r=$this->model->Actividades($b->idRelacion);
						$row_array["tipov"] = 1;
						$row_array['idActividad']=$r->idActividad;
						$row_array['fechaCompletado']=$r->fechaCompletado;
						$row_array['fecha']=$r->fechaActividad;
						$row_array['tipo']=$r->tipo;
						$row_array['notas']=$r->notas;
						$row_array['duracion']=$r->duracion;
						$row_array['completado']=$r->completado;
						if($r->horaInicio != null ){
							$row_array['horaInicio']=$r->horaInicio;
							$row_array['horaInicioF']=date("g:i a",strtotime($r->horaInicio));
							$row_array['horaFinF']=date("g:i a",strtotime($r->horaFin));
						}
						else{
							$row_array['horaInicio']="";
							$row_array['horaInicioF']="No defida";
							$row_array['horaFinF']="";
						}
						$row_array['idOrganizacion']=$r->idOrganizacion;
						$row_array['nombrePersona']=$r->nombrePersona;
						$row_array['tituloNegocio']=$r->tituloNegocio;
						$row_array['idNegocio']=$r->idNegocio;
						$row_array['nombreOrganizacion']=$r->nombreOrganizacion;
						$row_array['nombreUsuario']=$r->nombreUsuario;
						$row_array['nombrePersona']=$r->nombrePersona;
						$row_array['timestamp']=$b->timestamp;
						array_push($arrayLineaTiempo, $row_array);
						unset($row_array);
					}
				}
			}	 
			if ($b->tabla=='contenidos' && ($valor==0 || $valor==2))
			{
				$res = $this->model->ObtenerInfoBitacora('contenidos', 'idContenido', $b->idRelacion);
				if ($res != null){
					if($res->idOrganizacion==$idOrganizacion){
						$r=$this->model->ContenidosNotas($b->idRelacion);
						$row_array["tipov"] = 2;
						$row_array["notas"] = $r->notas;
						$row_array["archivos"] = $r->archivos;
						$row_array["size"] = $r->size;
						$row_array["idContenido"] = $r->idContenido;
						$row_array["fecha"] = $r->fechaSubido;
						$row_array["nombreUsuario"] = $r->nombreUsuario;
						array_push($arrayLineaTiempo, $row_array);
						unset($row_array);
					}
				}
			}
			if ($b->tabla=='negocios' && ($valor==0 || $valor==5))
			{
				$res = $this->model->ObtenerInfoBitacora('negocios', 'idNegocio', $b->idRelacion);
				if ($res != null){
					if($res->idOrganizacion==$idOrganizacion){
						$r=$this->model->Negocios($b->idRelacion);
						if (isset($r->fechaGanadoPerdido)) {
							$row_array["tipov"] = 5;
							$row_array["fecha"] = $r->fechaGanadoPerdido;
							$row_array["idNegocio"] = $r->idNegocio;
							$row_array["status"] = $r->status;
							$row_array["tituloNegocio"] = $r->tituloNegocio;
							$row_array["claveOrganizacion"] = $r->claveOrganizacion;
							$row_array["claveConsecutivo"] = $r->claveConsecutivo;
							$row_array["claveServicio"] = $r->claveServicio;
							$row_array["valorNegocio"] = $r->valorNegocio;
							$row_array["tipoMoneda"] = $r->tipoMoneda;
							$row_array["nombreUsuario"] = $r->nombreUsuario;
						}else{
							$row_array["tipov"] = 5;
							$row_array["fecha"] = $r->fechaCreado;
							$row_array["status"] = 0;
							$row_array["idNegocio"] = $r->idNegocio;
							$row_array["tituloNegocio"] = $r->tituloNegocio;
							$row_array["claveOrganizacion"] = $r->claveOrganizacion;
							$row_array["claveConsecutivo"] = $r->claveConsecutivo;
							$row_array["claveServicio"] = $r->claveServicio;
							$row_array["valorNegocio"] = $r->valorNegocio;
							$row_array["tipoMoneda"] = $r->tipoMoneda;
							$row_array["nombreUsuario"] = $r->nombreUsuario;
						}
						array_push($arrayLineaTiempo, $row_array);
						unset($row_array);
					}
				}
			}
		endforeach;
		echo json_encode($arrayLineaTiempo, JSON_FORCE_OBJECT);
	}

//Metodo para eliminar archivos del negocio
	public function EliminarArchivo(){
		$idContenido = $_REQUEST['id'];
		$url = "assets/files/archivosClientes/";
		$url .= $_REQUEST['nombre'];
		unlink($url); 
		$this->model->EliminarArchivo($idContenido);
	}

//Metodo para ver los seguidores de contacto
	public function ListarSeguidores(){
		$array=json_decode($_REQUEST['array']);
		$idOrganizacion = $_REQUEST['idOrganizacion'];
		$idUsuario = $_REQUEST['idUsuario'];
		array_push($array,$idUsuario);
		$array = array_reverse($array);
		echo '<table class="table table-bordered table-hover" id="tbl">
		<thead>
		<tr style="background-color: #FCF3CF;">
		<td align="center"><strong>Eliminar</strong></td>
		<td><strong>Nombre Completo</strong></td>
		<td><strong>Correo eléctronico</strong></td>
		</tr>
		</thead>
		<tbody>';
		foreach($array as $r):
			$resultadoFor=$this->model->ConsultaUsuarios($r);
			if ($resultadoFor==0) {
			}else{
				echo '<tr>
				<td align="center">';
				if ($idUsuario!=$resultadoFor['idUsuario']) {
					echo '<a href="#" data-toggle="modal" data-target="#mEliminarS" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mPersona" onclick="myFunctionEliminaUsuario(';echo $idOrganizacion.",".$resultadoFor['idUsuario']; echo')"> <span class="glyphicon glyphicon-trash"></span></a>';
				}
				echo '</td>
				<td>'.$resultadoFor['nombreUsuario'].'</td>
				<td>'.$resultadoFor['email'].'</td>
				</tr>';
			}
		endforeach;
		echo '</tbody>
		</table>'; 
	}

//Metodo para desvincular a los usuarios
	public function EliminarSeguidores(){
		$idOrganizacion = $_REQUEST['idOrganizacion'];
		$idUsuario = $_REQUEST['idUsuario'];
		$this->model->EliminarSeguidores($idOrganizacion,$idUsuario);
/*$accion="Seguidores";
$campo1="-1";
$campo2=$this->model->NombreUsuario($idUsuario);
date_default_timezone_set("America/Mexico_City");
$FechaAc= date("Y-m-d H:i:s");
$idUsuario=$_SESSION['idUsuario'];
$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);*/
}

//Metodo Para agregar segidores a un contacto
public function AgregarSeguidores(){
	$idOrganizacion = $_REQUEST['idOrganizacion'];
	$idUsuario = $_REQUEST['idUsuario'];
	$this->model->AgregarSeguidores($idOrganizacion,$idUsuario);
/*$accion="Seguidores";
$campo1="+1";
$campo2=$this->model->NombreUsuario($idUsuario);
date_default_timezone_set("America/Mexico_City");
$FechaAc= date("Y-m-d H:i:s");
$idUsuario=$_SESSION['idUsuario'];
$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);*/
}

//Metodo para agregar notas al negocio
public function RegistrarNota(){
	$idUsuario=$_SESSION['idUsuario'];
	$idOrganizacion = $_REQUEST['idOrganizacion'];
	$notas = $_REQUEST['notas'];
	date_default_timezone_set("America/Mexico_City");
	$FechaAc= date("Y-m-d H:i:s");
	if(isset($_FILES['archivo']))
	{
		$archivo = $_FILES['archivo'];
		$nombre=$_FILES['archivo']['name'];
		$info = new SplFileInfo($nombre);
		$info=$info->getExtension();
		if ($_FILES['archivo']['size'] > 1500000 or $_FILES['archivo']['size']==0) {
			echo "Peso";
		}elseif ($info=='pdf') {
			$no_permitidas= array ("-","á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
			$permitidas= array ("","a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
			$nombre = str_replace($no_permitidas, $permitidas ,$nombre);
			$dir_subida = './assets/files/archivosClientes/';
			$fichero_subido = $dir_subida . basename($nombre);
			if(move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido))
			{
				$this->model->RegistrarNota($idUsuario,$idOrganizacion,$notas,$FechaAc,$nombre,$_FILES['archivo']['size']);
				echo "Se ha registrado correctamente la nota";
			}else{
				echo "Error al subir archivo";
			}
		}else{
			echo "Otro";
		}
	}else{
		$this->model->RegistrarNota($idUsuario,$idOrganizacion,$notas,$FechaAc,null,null);
		echo "Se ha registrado correctamente la nota";
	}
}

//Metodo Para tarer el nombre del negocio Chava
public function NombreContacto(){
	$idOrganizacion = $_REQUEST['idOrganizacion'];
	$Resultado=$this->model->Detalles($idOrganizacion);
	echo '<h5><span class="mdi mdi-business mdi-1x"></span>'.$Resultado['nombreOrganizacion'].'</h5>';

}
//Metodo para Listar usuarios
public function ListarUsuarios(){
	$idUsuario=$_REQUEST['idUsuario'];
	$array=json_decode($_REQUEST['array']);
	header('Content-Type: application/json');
	$datos = array();
	foreach ($this->model->ListarUsuarios($idUsuario) as $usuarios):
		if (in_array($usuarios->idUsuario, $array)) {
		}else{
			$row_array['idUsuario']  = $usuarios->idUsuario;
			$row_array['nombreUsuario']  = $usuarios->nombreUsuario;
			array_push($datos, $row_array);
		}
	endforeach;     
	echo json_encode($datos, JSON_FORCE_OBJECT);
}
//Metodo para listar todas las personas para el select
public function ListarPersonasPorOrganizacion()
{
	header('Content-Type: application/json');
	$idOrganizacion=$_REQUEST['idOrganizacion'];
	$datos = array();
	foreach ($this->model->ListarPersonasPorOrganizacion($idOrganizacion) as $persona):
		$row_array['nombrePersona']  = $persona->nombrePersona;
		array_push($datos, $row_array);
	endforeach;		
	echo json_encode($datos, JSON_FORCE_OBJECT);
}
//Metodo para listar todas las personas para el select
public function ListarPersonas()
{
	$idOrganizacion=$_REQUEST['idOrganizacion'];
	echo '<table class="table table-bordered table-hover" id="tbl">
	<thead>
	<tr style="background-color: #FCF3CF;">
	<td align="center"><strong>Eliminar</strong></td>
	<td><strong>Nombre Completo</strong></td>
	<td><strong>Teléfono</strong></td>
	<td align="center" style="white-space: nowrap;"><strong>Tipo de<br><small>Teléfono</small></strong></td>
	<td><strong>Correo Electrónico</strong></td>
	<td><strong>Puesto</strong></td>
	<td><strong>Responsable</strong></td>
	</tr>
	</thead>
	<tbody>';
	foreach ($this->model->ListarPersonas($idOrganizacion) as $r) {
		echo '<tr>
		<td align="center"><a href="#" data-toggle="modal" data-target="#mEliminarContacto" class="btn btn-danger btn-xs" onclick="eliminarContacto('.$r->idCliente.')"> <span class="glyphicon glyphicon-trash"></span></a>
		</td>
		<td style="white-space: nowrap;">'.$r->honorifico .''." ".''. $r->nombrePersona.'</td>
		<td style="white-space: nowrap;">(<strong>'.$r->extension.'</strong>) '.$r->telefono.'</td>
		<td align="center">'.$r->tipoTelefono.'</td>
		<td>'.$r->email.'</td>
		<td style="white-space: nowrap;">'.$r->puesto.'</td>
		<td style="white-space: nowrap;">'.$r->nombreUsuario.'</td>
		</tr>';
	}
	echo '</tbody>
	</table>';
}
//Metodo para listar todas los negocios para el select
public function ListarNegocios()
{
	$idOrganizacion=$_REQUEST['idOrganizacion'];
	echo '<table class="table table-bordered table-hover" style="margin-bottom: 0px;">
	<thead>
	<tr style="background-color: #FCF3CF;">
	<td align="center"><strong>Eliminar</strong></td>
	<td><strong>Clave</strong></td>
	<td><strong>Titulo</strong></td>
	<td><strong>Valor de negocio</strong></td>
	<td align="center"><strong>Etapa de Venta</strong></td>
	<td align="center"><strong>Fecha de cierre</strong></td>
	<td><strong>Equipo</strong></td>
	<td><strong>Contacto</strong></td>
	<td style="white-space: nowrap"><strong>Sector Industrial</strong></td>
	<td><strong>Ponderación</strong></td>
	<td><strong>Responsable</strong></td>
	</tr>
	</thead>
	<tbody>';
	foreach ($this->model->ListarNegocios($idOrganizacion) as $r) {
		if ($r->status==1) {
			echo '<tr class="success">';
		}elseif ($r->status==2) {
			echo '<tr class="danger">';
		}else{
			echo '<tr class="">';
		}
		echo '
		<td align="center"><a href="#" data-toggle="modal" data-target="#mEliminar" class="btn btn-danger btn-xs" onclick="myFunctionEliminaregocio('.$r->idNegocio.','; echo "'".$r->nombreOrganizacion."'"; echo ')"> <span class="glyphicon glyphicon-trash"></span></a></td>
		<td style="white-space: nowrap">'. $r->claveOrganizacion .'-'.$r->claveConsecutivo.'-'.$r->claveServicio.'</td>
		<td style="white-space: nowrap">'. $r->tituloNegocio .'</td>
		<td style="white-space: nowrap">'. number_format($r->valorNegocio, 2, '.', ',').' / '.$r->tipoMoneda.'</td>
		<td align="center">'. $r->nombreEtapa .'</td>
		<td style="white-space: nowrap">'. $r->fechaCierre .'</td>
		<td>'. $r->nombreEquipo .'</td>
		<td style="white-space: nowrap">'. $r->nombrePersona .'</td>
		<td style="white-space: nowrap">'. $r->nombreSector .'</td>
		<td align="center">'. $r->ponderacion .'</td>
		<td style="white-space: nowrap">'. $r->nombreUsuario .'</td>
		</tr>';
	}
	echo '</tbody></table>';

}
//Metodo para subir archivos
public function SubirArchivo(){
	$idUsuario=$_SESSION['idUsuario'];
	$idOrganizacion = $_REQUEST['idOrganizacion'];
	date_default_timezone_set("America/Mexico_City");
	$FechaAc= date("Y-m-d H:i:s");
	$nombre=($_FILES['archivo']['name']);
	$info = new SplFileInfo($nombre);
	$info=$info->getExtension();
	if ($_FILES['archivo']['size'] > 1500000 or $_FILES['archivo']['size']==0) {
		echo "Peso";
	}elseif ($info=='pdf') {
		$no_permitidas= array ("-","á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("","a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$nombre = str_replace($no_permitidas, $permitidas ,$nombre);
		$dir_subida = 'assets/files/archivosClientes/';
		$fichero_subido = $dir_subida . basename($nombre);
		move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido);
		$this->model->SubirArchivo($idUsuario,$idNegocio,$idCliente,$idOrganizacion,$nombre,$_FILES['archivo']['size'],$FechaAc);
	}else{
		echo "Otro";
	}
}

public function mostrarContactos()
{
	header('Content-Type: application/json');
	$idOrganizacion = $_GET['idOrganizacion'];
	$datos = array();
	foreach ($this->model->ObtnenIdPersonas($idOrganizacion) as $persona):
		$obtPersonas=$this->model->ObtnenPersonas($persona->idCliente);
		$row_array['idCliente']  = $obtPersonas['idCliente'];
		$row_array['nombrePersona']  = $obtPersonas['nombrePersona'];
		array_push($datos, $row_array);
	endforeach;		
	echo json_encode($datos, JSON_FORCE_OBJECT);
}

public function mostrarNegocios()
{
	header('Content-Type: application/json');
	$idOrganizacion = $_GET['idOrganizacion'];
	$data = array();
	foreach ($this->model->ListarNegociosAbiertos($idOrganizacion) as $negocio): 
		$r_array['idNegocio']  = $negocio->idNegocio;
		$r_array['tituloNegocio']  = $negocio->tituloNegocio;
		$r_array['claveOrganizacion']  = $negocio->claveOrganizacion;
		$r_array['claveConsecutivo']  = $negocio->claveConsecutivo;
		$r_array['claveServicio']  = $negocio->claveServicio;
		$r_array['valorNegocio']  = number_format($negocio->valorNegocio, 2, '.', ',');
		$r_array['tipoMoneda']  = $negocio->tipoMoneda;
		$r_array['nombreOrganizacion']  = $negocio->nombreOrganizacion;
		array_push($data, $r_array);
	endforeach;		
	echo json_encode($data, JSON_FORCE_OBJECT);
}

public function mostrarDetallesNegocio()
{
	$idOrganizacion = $_GET['idOrganizacion'];
	$NegociosGanados=count($this->model->ListarNegociosGanados($idOrganizacion));
	$NegociosPerdidos=count($this->model->ListarNegociosPerdidos($idOrganizacion));
	$NegociosCerrados=($NegociosGanados + $NegociosPerdidos);

	($NegociosGanados!=null) ? $PorcietoG=(($NegociosGanados * 100) / $NegociosCerrados):$PorcietoG=0;
	($NegociosPerdidos!=null) ? $PorcietoP=(($NegociosPerdidos * 100) / $NegociosCerrados):$PorcietoP=0;

	$SumaGanados=0;
	$SumaPerdidos=0;
	$Ganados=$this->model->ConsultaValorNegociosCerrados($idOrganizacion, 1);
	$Perdidos=$this->model->ConsultaValorNegociosCerrados($idOrganizacion, 2);
	$valDolar = $this->model->ConsultaValorDolar();

	foreach ($Ganados as $row):
		if ($row->tipoMoneda == 'USD')
			$SumaGanados = ($SumaGanados + ($valDolar * $row->valorNegocio));
		else
			$SumaGanados = ($SumaGanados + $row->valorNegocio);
	endforeach;

	foreach ($Perdidos as $row):
		if ($row->tipoMoneda == 'USD')
			$SumaPerdidos = ($SumaPerdidos + ($valDolar * $row->valorNegocio));
		else
			$SumaPerdidos = ($SumaPerdidos + $row->valorNegocio);
	endforeach;

	header('Content-Type: application/json');
	$data = array(); 
	$r_array['NegociosGanados']  = $NegociosGanados;
	$r_array['NegociosPerdidos']  = $NegociosPerdidos;
	$r_array['NegociosCerrados']  = $NegociosCerrados;
	$r_array['PorcietoG']  = $PorcietoG;
	$r_array['PorcietoP']  = $PorcietoP;
	$r_array['SumaGanados']  = number_format($SumaGanados, 2, '.', ',');
	$r_array['SumaPerdidos']  = number_format($SumaPerdidos, 2, '.', ',');
	array_push($data, $r_array);
	echo json_encode($data, JSON_FORCE_OBJECT);
}

public function mostrarDetallesActividad()
{
	$idOrganizacion = $_GET['idOrganizacion'];
	$countActividades=$this->model->ContadorActividades($idOrganizacion);
	$actividades=$this->model->DetallesActividades($idOrganizacion);
	$Llamada=$Reunión=$Tarea=$Plazo=$Email=$Comida=$WhatsApp=0;
	foreach ($actividades as $r):
		switch ($r->tipo) {
			case "Llamada":
			$Llamada++;
			break;
			case "Reunion":
			$Reunión++;
			break;
			case "Tarea":
			$Tarea++;
			break;
			case "Plazo":
			$Plazo++;
			break;
			case "Email":
			$Email++;
			break;
			case "Comida":
			$Comida++;
			break;
			case "WhatsApp":
			$WhatsApp++;
			break;
		}
	endforeach;

	$array = array(
		"Llamada" => $Llamada,
		"Reunión" => $Reunión,
		"Tarea" => $Tarea,
		"Plazo" => $Plazo,
		"Email" => $Email,
		"Comida" => $Comida,
		"WhatsApp" => $WhatsApp
	); 
	$sumaArray=array_sum($array);

	header('Content-Type: application/json');
	$data = array(); 
	$r_array['Llamada']  = $Llamada;
	$r_array['Reunión']  = $Reunión;
	$r_array['Tarea']  = $Tarea;
	$r_array['Plazo']  = $Plazo;
	$r_array['Email']  = $Email;
	$r_array['Comida']  = $Comida;
	$r_array['WhatsApp']  = $WhatsApp;
	array_push($data, $r_array);
	$row_array['sumaArray']  = $sumaArray;
	array_push($data, $row_array);
	echo json_encode($data, JSON_FORCE_OBJECT);
}

public function mostrarDetallesUserActivo()
{
	$idOrganizacion = $_GET['idOrganizacion'];
	$idUsuario = $_GET['idUsuario'];
	$arregloSAct = $this->model->ArregloDeSeguidoresAct($idOrganizacion, $idUsuario);
	/*Esta consulta me devuelve los ids de los usuarios que han registrado actividades sin devolver el id principal*/
	$arregloNuevoS = array_unique($arregloSAct); /*se eliminan los ids duplicados*/			
	array_push($arregloNuevoS, $idUsuario);/*Se Agrega el id del usuario al arreglo*/

	foreach ($arregloNuevoS as $id):
		$resSeguidores=$this->model->ObtnenSeguidores($id);
		$arrayUsuario[$resSeguidores['nombreUsuario']] = $id; /*Obtiene el nombre del id de usuario*/
		foreach ($arrayUsuario as $idU) {
			$contR=$this->model->ActividadesUsuarios($idOrganizacion,$idU);
			$arrayUsuarioN[$resSeguidores['nombreUsuario']] = $contR;
		}
	endforeach; 

	$sumaArray = array_sum($arrayUsuarioN);
	header('Content-Type: application/json');
	$data = array(); 
	$r_array['array']  = $arrayUsuarioN;
	array_push($data, $r_array);
	$row_array['sumaArray']  = $sumaArray;
	array_push($data, $row_array);
	echo json_encode($data, JSON_FORCE_OBJECT);
}

public function mostrarSeguidores()
{
	$idOrganizacion = $_GET['idOrganizacion'];
	$idUsuario = $_GET['idUsuario'];
	$countS= (int) $this->model->ContadorSeguidores($idOrganizacion);
	$arregloS=$this->model->ArregloDeSeguidores($idOrganizacion,$idUsuario);
	/*Esta consulta me devuelve los ids de los usuarios que estan relacionados con la organizacion y sean diferente a idUsuario enviado como parametro de la tabla de seguidoresCliente resultado*/
	$arregloS = implode(",", $arregloS); 
	header('Content-Type: application/json');
	$data = array();
	$seguidores = array();

	foreach ($this->model->ObtnenIdUsuario($idOrganizacion) as $r):
		$resSeguidores=$this->model->ObtnenSeguidores($r->idUsuario);
		$r_nom['nombreUsuario']  = $resSeguidores['nombreUsuario'];
		$r_nom['foto']  = $resSeguidores['foto'];
		array_push($seguidores, $r_nom);
	endforeach; 

	$r_array['array']  = $arregloS;
	array_push($data, $r_array);
	$r_count['countS']  = $countS;
	array_push($data, $r_count);
	array_push($data, $seguidores);
	echo json_encode($data, JSON_FORCE_OBJECT);
}

public function Contadores()
{
	header('Content-Type: application/json');
	$idOrganizacion = $_GET['idOrganizacion'];
	$contArray = array();
	$row_array['countActividades'] = $this->model->ContadorActividades($idOrganizacion);
	$row_array['countNotas'] = $this->model->ContadorNotas($idOrganizacion);
	$row_array['countNegocios'] = $this->model->ContadorNegocios($idOrganizacion);
	$row_array['countTotal'] = intval($row_array['countActividades']) + intval($row_array['countNotas']) + intval($row_array['countNegocios']);
	array_push($contArray, $row_array);
	echo json_encode($contArray, JSON_FORCE_OBJECT);
}

}
?>