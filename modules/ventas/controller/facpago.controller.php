<?php
require_once 'model/facpago.php';

class FacpagoController{
	public $model;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function Index(){
		$sector=true;
		$this->url="?c=facpago";
		$page = "view/page_not/page.php";
		//$page="view/procesos/facpago.php";
		require_once '../../view/index.php';
	}
	public function __CONSTRUCT()
	{
		try{
			$this->model = new Facpago();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function GuardarFactura(){
		try
		{
			header('Content-Type: application/json');
			$factura = new Facpago();
			$factura->idFactura=$_REQUEST['idFactura'];
			$factura->idCotizacion=$_REQUEST['selectCotizacion'];
			$factura->noFactura=$_REQUEST['noFactura'];
			$factura->fechaFactura=$_REQUEST['fechaFactura'];
			$ruta = $_FILES["factura"];
			$factura->ruta = $ruta["name"];
			$archivo = $ruta["tmp_name"];
			$carpeta = "../../assets/imagenes/";
			$src = $carpeta.$factura->ruta;
			move_uploaded_file($archivo, $src);
			if($factura->idFactura>0){
				$this->model->ActualizarFactura($factura);
				echo "Se han actualizado correctamente los datos de la factura de la factura";
			}else{
				$this->model->RegistrarFactura($factura);
				echo "Se ha registrado correctamente la factura de la factura";
			}
		}
		catch(Exception $e)
		{
			echo "error";
			echo $e->getMessage();
		}
	}
	public function listarNegocios1(){
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarNegocios() as $negocios):
			$row_array['idNegocio']  = $negocios->idNegocio;
			$row_array['tituloNegocio']  = $negocios->tituloNegocio;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

public function GuardarPago(){
		try
		{
			header('Content-Type: application/json');
			$pago = new Facpago();
			$pago->idPago=$_REQUEST['idPago'];
			$pago->idCotizacion=$_REQUEST['selectCotizacionPago'];
			$pago->fechaPago=$_REQUEST['fechaPago'];
			$pago->fechaRecibo=$_REQUEST['fechaRecibo'];
			$ruta = $_FILES["pago"];
			$pago->ruta = $ruta["name"];
			$archivo = $ruta["tmp_name"];
			$carpeta = "../../assets/imagenes/";
			$src = $carpeta.$pago->ruta;
			move_uploaded_file($archivo, $src);
			if($pago->idPago>0){
				$this->model->ActualizarPago($pago);
				echo "Se han actualizado correctamente los datos del recibo de pago";
			}else{
				$this->model->RegistrarPago($pago);
				echo "Se ha registrado correctamente el recibo de pago";
			}
		}
		catch(Exception $e)
		{
			echo "error";
			// echo $e->getMessage();
		}
	}
}
?>
