<?php 
require_once 'model/negocio.php';
require_once 'model/deal.php';
require_once 'model/embudo.php';

class DealController{

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Deal();
			$this->modelN = new Negocio();
			$this->modelEmbudo = new Embudo();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		date_default_timezone_set("America/Mexico_City");
		$page="view/deal/deal.php";
		$contenedor="view/deal/contenedor.php";
		$idNegocio= $_GET['idNegocio'];
		$idEmbudo = $_SESSION['idEmbudo'];
		$resultado = $this->model->NegocioDetalles($idNegocio);
		$contacto = $this->model->ObtenerContacto($resultado['idCliente']);
		$embudo = $this->model->ObtenerEmbudo($idNegocio);
		if($resultado['claveOrganizacion']==null)
			$claveOrganizacion=0;
		else
			$claveOrganizacion=1;
		require_once '../../view/index.php';
	}

	public function CambiaEmbudo()
	{
		$idNegocio = $_POST['idNegocio'];
		$idEmbudo = $_POST['idEmbudo'];
		$idOldEmbudo = $_POST['idOldEmbudo'];
		$idEtapa = $_POST['idEtapa'];
		$idOldEtapa = $_POST['idOldEtapa'];
		if(!empty($_POST['claveOrganizacion'])){
			echo 'entra';
			$this->ActualizaClaveNegocio($_POST['claveOrganizacion'],$_POST['claveConsecutivo'],$_POST['claveServicio'], $idNegocio);
		}
		$accion="Embudo";
		$this->modelN->CambiaEmbudo($idEmbudo, $idNegocio);
		$this->CambiaEtapa2($idEtapa, $idNegocio, $idOldEtapa, $idEmbudo, $idOldEmbudo);
	}

	public function ActualizaClaveNegocio($claveOrganizacion,$claveConsecutivo,$claveServicio,$idNegocio)
	{
		$negocio = new Negocio();
		$negocio->claveOrganizacion = $claveOrganizacion;
		$negocio->claveConsecutivo = $claveConsecutivo;
		$negocio->claveServicio = $claveServicio;	
		$negocio->idNegocio = $idNegocio;
		$this->modelN->ActualizaClaveNegocio($negocio);
	}

	public function CambiaEtapa2($idEtapa, $idNegocio, $idOldEtapa, $idEmbudo, $idOldEmbudo)
	{
		$ArrayCont=$this->modelN->ContenidoEtapas($idOldEtapa);
		$valor2 = explode(",", $ArrayCont);
		$borrar = array($idNegocio);
		$vector_nuevo = array_diff($valor2, $borrar);
		$cont=implode(',',$vector_nuevo);

		$this->modelEmbudo->OrdenarNegocios($idOldEtapa,$cont);
		$this->modelEmbudo->NegocioContenido($idEtapa,$idNegocio);
		$this->modelN->ActualizarContadorActivo($idNegocio);

		$Resultado=$this->model->CambiaEatapa($idEtapa,$idNegocio);
		$accion="Embudo";

		$embudoNew=$this->model->NombreEmbudo($idOldEmbudo);
		$embudoOld=$this->model->NombreEmbudo($idEmbudo);

		$etapaNew=$this->model->NombreEtapa($idEtapa);
		$etapaOld=$this->model->NombreEtapa($idOldEtapa);

		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];

		$campo1 = $embudoOld  . " (" . $etapaOld . ")";
		$campo2 = $embudoNew  . " (" . $etapaNew . ")";

		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);
	}

	public function ListarEtapas()
	{
		header('Content-Type: application/json');
		$etapas=$this->modelN->ListarEtapas($_GET['idEmbudo']);
		$data = array(); 
		foreach ($etapas as $etapa):
			$r_array['idEtapa']  = $etapa->idEtapa;
			$r_array['nombreEtapa']  = $etapa->nombreEtapa;
			array_push($data, $r_array);	
		endforeach;
		echo json_encode($data, JSON_FORCE_OBJECT);
	}

	public function Consulta2(){
		date_default_timezone_set("America/Mexico_City");
		$idNegocio=$_REQUEST['idNegocio'];
		$valorTipoTarea = isset($_REQUEST['valor']) ? $_REQUEST['valor'] : '';
		$resultado=$this->model->NegocioDetalles($idNegocio);
		$date = date_create($resultado['fechaCierre']);
		$FechaAc= date("Y-m-d");
		$contadorActivo = substr($resultado['fechaCreado'], 0, 10);
		$datetime1 = new DateTime($contadorActivo);
		$datetime2 = new DateTime($FechaAc);
		$interval = $datetime1->diff($datetime2);
		$arrayLineaTiempo=array();
		$contenido=$this->model->Actividades($idNegocio);

		foreach ($contenido as $r):
			$row_array["tipov"] = 1;
			$row_array['idActividad']=$r->idActividad;
			$row_array['fechaCompletado']=$r->fechaCompletado;
			$row_array['fecha']=$r->fechaActividad;
			$row_array['tipo']=$r->tipo;
			$row_array['notas']=$r->notas;
			$row_array['duracion']=$r->duracion;
			$row_array['completado']=$r->completado;
			$row_array['horaInicio']=$r->horaInicio;
			$row_array['horaFin']=$r->horaFin;
			$row_array['idOrganizacion']=$r->idOrganizacion;
			$row_array['nombrePersona']=$r->nombrePersona;
			$row_array['tituloNegocio']=$r->tituloNegocio;
			$row_array['idNegocio']=$r->idNegocio;
			$row_array['nombreOrganizacion']=$r->nombreOrganizacion;
			$row_array['nombreUsuario']=$r->nombreUsuario;
			$row_array['nombrePersona']=$r->nombrePersona;
			array_push($arrayLineaTiempo, $row_array);
		endforeach;	 

		$contenido=$this->model->Contenidos($idNegocio);
		foreach ($contenido as $r):
			if ($r->archivos==null) {
				$row_arrayCo["tipov"] = 2;
				$row_arrayCo["notas"] = $r->notas;
			}else{
				$row_arrayCo["tipov"] = 3;
				$row_arrayCo["archivos"] = $r->archivos;
				$row_arrayCo["size"] = $r->size;
			}
			$row_arrayCo["idContenido"] = $r->idContenido;
			$row_arrayCo["fecha"] = $r->fechaSubido;
			$row_arrayCo["nombreUsuario"] = $r->nombreUsuario;
			array_push($arrayLineaTiempo, $row_arrayCo);
		endforeach;	
		$contenido=$this->model->Cambios($idNegocio);
		foreach ($contenido as $r):
			$row_arrayCa["tipov"] = 4;
			$row_arrayCa["fecha"] = $r->fechaRegistroCambio;
			$row_arrayCa["accion"] = $r->accion;
			$row_arrayCa["campo1"] = $r->campo1;
			$row_arrayCa["campo2"] = $r->campo2;
			$row_arrayCa["nombreUsuario"] = $r->nombreUsuario;
			array_push($arrayLineaTiempo, $row_arrayCa);
		endforeach;
		function cmp($a, $b) {
			if ($a['fecha'] == $b['fecha']) {
				return 0;
			}
			return ($a['fecha'] < $b['fecha']) ? -1 : 1;
		}
		uasort($arrayLineaTiempo, 'cmp');
		print_r($arrayLineaTiempo);
		$arrayLineaTiempo = array_reverse($arrayLineaTiempo);



		echo ' 
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="timeline-centered">
		<div style="text-align: center;">
		<span class="label label-default">Planeadas</span>
		</div>';
		if ($valorTipoTarea==0 || $valorTipoTarea==1) {
			foreach ($arrayLineaTiempo as $r  => $value):
				if ($value['fecha']>=$FechaAc && $value['tipov']==1) {
					echo '<!--Actividades-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-primary">
					<i class="fas fa-calendar-alt"></i>
					</div>
					<div class="timeline-label">
					<div class="row">
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
					<div class="btn-group">
					<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#mTerminar" onclick="cambiaEstado('.$value['idActividad'].','.$value['completado'].',';
					echo "'".$value['fechaCompletado']."'".')">';
					if ($value['completado']==0) {
						echo '<i class="far fa-circle" style="font-size: 16px;"></i>';
					}else{
						echo '<i class="fas fa-check-circle" style="font-size: 16px; color: green;"></i>';
					}
					echo '</button>
					</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-9 col-lg-10">
					<h2 style="padding-top: 4px;"><a>';
					if ($value['tipo']=="Llamada") {
						echo '<i class="mdi mdi-phone-in-talk"></i> ';
					}elseif ($value['tipo']=="Reunión") {
						echo '<i class="mdi mdi-group mdi-lg"></i> ';
					}elseif ($value['tipo']=="Tarea") {
						echo '<i class="far fa-clock" style="font-size: 15px;"></i> ';
					}elseif ($value['tipo']=="Plazo") {
						echo '<i class="fas fa-hourglass-half" style="font-size: 14px;"></i> ';
					}elseif ($value['tipo']=="Email") {
						echo '<i class="fas fa-envelope" style="font-size: 16px;"></i> ';
					}elseif ($value['tipo']=="Comida") {
						echo '<i class="fas fa-utensils" style="font-size: 14px;"></i> ';
					}else{
						echo '<i class="fab fa-whatsapp" style="font-size: 16px;"></i> ';
					}
					echo $value['tipo'].'</a> <span style="font-size: 14px;">';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
					if ($value['horaInicio']!=null) {
						echo '  '.$value['horaInicio'].' - '.$value['horaFin'];
					}
					echo '</span>
					</h2>
					</div>
					<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
					<button type="button" class="btn-link dropdown-toggle" id="dropdownAct" data-toggle="dropdown" >
					<i class="far fa-edit" style="font-size: 16px;"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownAct">
					<li role="presentation" id="ya"><a data-toggle="tab" href="#actividad" onclick="editarActividad('."'".$value['idActividad'].",".$value['tipo']."".",".$value['notas']."".",".$value['fecha'].",".$value['duracion']."".",".$value['horaInicio'].",".$value['idOrganizacion'].",".$value['nombreOrganizacion'].",".$value['idNegocio'].",".$value['tituloNegocio']."".",".$value['nombrePersona']."'".');">Editar</a></li>
					<li><a href="#" data-toggle="modal" data-target="#mEliminarA" onclick="eliminarActividad('.$value['idActividad'].'); return false">Eliminar</a></li>
					</ul>
					</div>
					</div>
					<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<i class="fas fa-user"></i>
					<p">'.$value['nombrePersona'].'</p>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					<i class="mdi mdi-business mdi-lg"></i>
					<p">'.$value['nombreOrganizacion'].'</p>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-xs hidden-sm" align="right">
					<p class="help-block">'.$value['nombreUsuario'].'</p>
					</div>
					</div>
					</div>

					<div class="row">
					<div class="col-lg-12">
					<hr style="margin-top: 3px; margin-bottom: 10px;">
					</div>
					<div class="col-lg-12">
					<div class="form-group">
					<textarea name="" class="form-control" id="txtNotas" readonly>'.$value['notas'].'</textarea>
					</div>
					</div>
					</div>
					</div>
					</div>
					</article>
					<!--Actividades-->';
				}
			endforeach;
		}

		echo '<div style="text-align: center;">
		<span class="label label-default">Pasadas</span>
		</div>';
		foreach ($arrayLineaTiempo as $r  => $value):
			if ($valorTipoTarea==0 || $valorTipoTarea==2) {
				if ($value['tipov']==2) {
					echo '<!--Notas-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-success">
					<i class="glyphicon glyphicon-comment"></i>
					</div>
					<div class="timeline-label">
					<div class="row">
					<div class="col-xs-2 col-lg-1" align="center" style="padding-right: 0px;">
					<i class="fas fa-dollar-sign" style="font-size: 16px;"></i>
					</div>
					<div class="col-xs-8 col-lg-8" style="padding-left: 0px;">
					<h2><a><span>'.$resultado['claveOrganizacion'].'-'.$resultado['claveConsecutivo'].'-'.$resultado['claveServicio'].'</span> '.$resultado['tituloNegocio'].'</a></h2>
					</div>
					<div class="col-xs-2 col-lg-3" align="right">
					<button type="button" class="btn btn-link dropdown-toggle" id="dropdownNota" data-toggle="dropdown" >
					<i class="far fa-edit" style="font-size: 16px;"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownNota">
					<li><a href="#" onclick="quitarReadOnly(';echo "'txtNotas".$value['idContenido']."'"; echo '); return false">Editar</a></li>
					<li><a href="#" onclick="eliminarNota('.$value['idContenido'].'); return false">Eliminar</a></li>
					</ul>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12">
					<div class="col-lg-3">
					<p class="help-block">';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
					echo '</p>
					</div>
					<div class="col-lg-9" align="right">
					<p class="help-block">'.$value['nombreUsuario'].'</p>
					</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12">
					<hr style="margin-top: 3px; margin-bottom: 10px;">
					</div>
					<div class="col-lg-12">
					<div class="form-group">
					<textarea name="" class="form-control" id="txtNotas'.$value['idContenido'].'" readonly>'.$value['notas'].'</textarea>
					</div>
					</div>						
					</div>
					</div>
					</div>
					</article>
					<!--Notas-->';
				}
			} 
			if ($valorTipoTarea==0 || $valorTipoTarea==1) {
				if ($value['tipov']==1) {
					if ($value['fecha']<$FechaAc) {
						echo '<!--Actividades-->
						<article class="timeline-entry">
						<div class="timeline-entry-inner">
						<div class="timeline-icon bg-primary">
						<i class="fas fa-calendar-alt"></i>
						</div>
						<div class="timeline-label">
						<div class="row">
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-1">
						<div class="btn-group">
						<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#mTerminar" onclick="cambiaEstado('.$value['idActividad'].','.$value['completado'].',';
						echo "'".$value['fechaCompletado']."'".')">';
						if ($value['completado']==0) {
							echo '<i class="far fa-circle" style="font-size: 16px;"></i>';
						}else{
							echo '<i class="fas fa-check-circle" style="font-size: 16px; color: green;"></i>';
						}
						echo '</button>
						</div>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-9 col-lg-10">
						<h2 style="padding-top: 4px;"><a>';
						if ($value['tipo']=="Llamada") {
							echo '<i class="mdi mdi-phone-in-talk"></i> ';
						}elseif ($value['tipo']=="Reunión") {
							echo '<i class="mdi mdi-group mdi-lg"></i> ';
						}elseif ($value['tipo']=="Tarea") {
							echo '<i class="far fa-clock" style="font-size: 15px;"></i> ';
						}elseif ($value['tipo']=="Plazo") {
							echo '<i class="fas fa-hourglass-half" style="font-size: 14px;"></i> ';
						}elseif ($value['tipo']=="Email") {
							echo '<i class="fas fa-envelope" style="font-size: 16px;"></i> ';
						}elseif ($value['tipo']=="Comida") {
							echo '<i class="fas fa-utensils" style="font-size: 14px;"></i> ';
						}else{
							echo '<i class="fab fa-whatsapp" style="font-size: 16px;"></i> ';
						}
						echo $value['tipo'].'</a> <span style="font-size: 14px;">';
						$date = substr($value['fecha'], 5, 2);
						$NomMes=$this->model->NombreMes($date);
						echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
						if ($value['horaInicio']!=null) {
							echo '  '.$value['horaInicio'].' - '.$value['horaFin'];
						}
						echo '</span>
						</h2>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
						<button type="button" class="btn-link dropdown-toggle" id="dropdownAct" data-toggle="dropdown">
						<i class="far fa-edit" style="font-size: 16px;"></i>
						</button>
						<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownAct">
						<li role="presentation" id="ya2"><a data-toggle="tab" href="#actividad" onclick="editarActividad('."'".$value['idActividad'].",".$value['tipo']."".",".$value['notas']."".",".$value['fecha'].",".$value['duracion']."".",".$value['horaInicio'].",".$value['idOrganizacion'].",".$value['nombreOrganizacion'].",".$value['idNegocio'].",".$value['tituloNegocio']."".",".$value['nombrePersona']."'".');">Editar</a></li>
						<li><a href="#" data-toggle="modal" data-target="#mEliminarA" onclick="eliminarActividad('.$value['idActividad'].'); return false">Eliminar</a></li>
						</ul>
						</div>
						</div>
						<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<i class="fas fa-user"></i>
						<p">'.$value['nombrePersona'].'</p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<i class="mdi mdi-business mdi-lg"></i>
						<p">'.$value['nombreOrganizacion'].'</p>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-xs hidden-sm" align="right">
						<p class="help-block">'.$value['nombreUsuario'].'</p>
						</div>
						</div>
						</div>
						<div class="row">
						<div class="col-lg-12">
						<hr style="margin-top: 3px; margin-bottom: 10px;">
						</div>
						<div class="col-lg-12">
						<div class="form-group">
						<textarea name="" class="form-control" id="txtNotas" readonly>'.$value['notas'].'</textarea>
						</div>
						</div>
						</div>
						</div>
						</div>
						</article>
						<!--Actividades-->';
					}
				}
			}
			echo '<!--Negocios
			<article class="timeline-entry">
			<div class="timeline-entry-inner">
			<div class="timeline-icon bg-danger">
			<i class="fas fa-dollar-sign"></i>
			</div>
			<div class="timeline-label">
			<h2><a href="#">Job Meeting</a></h2>
			<hr style="margin-top: 0px; ">
			<p>You have a meeting at <strong>Laborator Office</strong> Today.</p>
			</div>
			</div>
			</article>
			Negocios-->';
			if ($valorTipoTarea==0 || $valorTipoTarea==4) {
				if ($value['tipov']==4) {
					echo '<!--Cambios-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-default">
					<i class="fas fa-exchange-alt"></i>
					</div>
					<div class="timeline-label">
					<h2><a>'.$value['accion'].": ".'</a><a '; 
					if ($value['campo1']=="Ganado" || $value['campo1']=="+1") {
						echo 'style="color:green"';
					}elseif($value['campo1']=="Perdido" || $value['campo1']=="-1"){
						echo 'style="color:red"';
					}
					echo '>'.$value['campo1'].'</a> <i class="fas fa-arrow-right"></i> <a '; 
					if ($value['campo2']=="Ganado") {
						echo 'style="color:green"';
					}elseif($value['campo2']=="Perdido"){
						echo 'style="color:red"';
					}
					echo '> '.$value['campo2'].'</a></h2>
					<hr style="margin-top: 0px; ">
					<p>';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4).' - '.$value['nombreUsuario'];
					echo 
					'</p>
					</div>
					</div>
					</article>
					<!--Cambios-->';
				}
			}
			echo '<!--Correos
			<article class="timeline-entry">
			<div class="timeline-entry-inner">
			<div class="timeline-icon bg-info">
			<i class="fas fa-envelope"></i>
			</div>
			<div class="timeline-label">
			<h2><a href="#">Arlind Nushi</a> <span>checked in at</span> <a href="#">Laborator</a></h2>
			<hr style="margin-top: 0px; ">
			</div>
			</div>
			</article>
			Correos-->';
			if ($valorTipoTarea==0 || $valorTipoTarea==3) {
				if ($value['tipov']==3) {
					echo '<!--Archivos-->
					<article class="timeline-entry">
					<div class="timeline-entry-inner">
					<div class="timeline-icon bg-warning">
					<i class="fas fa-paperclip"></i>
					</div>
					<div class="timeline-label">
					<div class="row">
					<div class="col-lg-1" align="center">
					<i class="far fa-file-pdf" style="font-size: 30px; color: red;"></i>
					</div>
					<div class="col-lg-8">
					<h2><a href="assets/files/archivosNegocios/'.$value['archivos'].'" download="'.$value['archivos'].'">'.$value['archivos'].'</a></h2>
					</div>
					<div class="col-lg-3" align="right">
					<button type="button" class="btn btn-link dropdown-toggle" id="dropdownNota" data-toggle="dropdown" >
					<i class="far fa-edit" style="font-size: 16px;"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownNota">
					<li><a href="#" onclick="eliminarArchivo('.$value['idContenido'].",'".$value['archivos']."'".'); return false">Eliminar</a></li>
					</ul>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-4" align="center">
					<p class="help-block">';
					$date = substr($value['fecha'], 5, 2);
					$NomMes=$this->model->NombreMes($date);
					echo substr($value['fecha'], 8, 2)." de ".$NomMes." de ".substr($value['fecha'], 0, 4);
					echo'</p>
					</div>
					<div class="col-lg-4" align="center">
					<p class="help-block">'.$value['nombreUsuario'].'</p>
					</div>
					<div class="col-lg-4" align="center">
					<p class="help-block">'; 
					echo round(($value['size'] / 1024), 2)." Kb";
					echo '</p>
					</div>
					</div>
					</div>
					</div>
					</article>
					<!--Archivos-->';
				}
			}
		endforeach;
		if ($valorTipoTarea==0 || $valorTipoTarea==4) {
			echo '<!--Cambios-->
			<article class="timeline-entry">
			<div class="timeline-entry-inner">
			<div class="timeline-icon bg-default">
			<i class="fas fa-exchange-alt"></i>
			</div>
			<div class="timeline-label">
			<h2><a>Negocio creado el</a></h2>
			<hr style="margin-top: 0px; ">
			<p>';
			$date = substr($resultado['fechaCreado'], 5, 2);
			$NomMes=$this->model->NombreMes($date);
			echo substr($resultado['fechaCreado'], 8, 2)." de ".$NomMes." de ".substr($resultado['fechaCreado'], 0, 4).' - '.$resultado['nombreUsuario'];
			echo '</p>
			</div>
			</div>
			</article>
			<!--Cambios-->';
		}
		echo '<!--POR DEFAULT-->
		<article class="timeline-entry begin">
		<div class="timeline-entry-inner">
		<div class="timeline-icon">
		<i class="fas fa-ellipsis-h"></i>
		</div>
		</div>
		</article>
		<!--POR DEFAULT-->
		</div>
		</div>';
	}

	public function Consulta(){
		header('Content-Type: application/json');
		$idNegocio = $_GET['idNegocio'];
		$valor = $_GET['valor'];
		$negocio = $this->model->NegocioDetalles($idNegocio);
		$bitacoras = $this->model->ListarBitacoras();
		$arrayLineaTiempo = array();
		foreach($bitacoras as $b):

			if($b->tabla=='actividades' && ($valor==0 || $valor==1))
			{
				$res = $this->model->ObtenerInfoBitacora('actividades', 'idActividad', $b->idRelacion);
				if ($res != null){
					if($res->idNegocio==$idNegocio)
					{
						$r=$this->model->Actividades($b->idRelacion);
						$row_array["tipov"] = 1;
						$row_array['idActividad']=$r->idActividad;
						$row_array['fechaCompletado']=$r->fechaCompletado;
						$row_array['fecha']=$r->fechaActividad;
						$row_array['tipo']=$r->tipo;
						$row_array['notas']=$r->notas;
						$row_array['duracion']=$r->duracion;
						$row_array['completado']=$r->completado;
						if($r->horaInicio != null ){
							$row_array['horaInicio']=$r->horaInicio;
							$row_array['horaInicioF']=date("g:i a",strtotime($r->horaInicio));
							$row_array['horaFinF']=date("g:i a",strtotime($r->horaFin));
						}
						else{
							$row_array['horaInicio']="";
							$row_array['horaInicioF']="No defida";
							$row_array['horaFinF']="";
						}
						$row_array['idOrganizacion']=$r->idOrganizacion;
						$row_array['nombrePersona']=$r->nombrePersona;
						$row_array['tituloNegocio']=$r->tituloNegocio;
						$row_array['idNegocio']=$r->idNegocio;
						$row_array['nombreOrganizacion']=$r->nombreOrganizacion;
						$row_array['nombreUsuario']=$r->nombreUsuario;
						$row_array['nombrePersona']=$r->nombrePersona;
						$row_array['timestamp']=$b->timestamp;
						array_push($arrayLineaTiempo, $row_array);
						unset($row_array);
					}
				}
			}	 
			if ($b->tabla=='contenidos' && ($valor==0 || $valor==2))
			{
				$res = $this->model->ObtenerInfoBitacora('contenidos', 'idContenido', $b->idRelacion);
				if ($res != null){
					if($res->idNegocio==$idNegocio){
						$r=$this->model->Contenidos($b->idRelacion);
						$row_array["tipov"] = 2;
						$row_array["notas"] = $r->notas;
						$row_array["archivos"] = $r->archivos;
						$row_array["size"] = $r->size;
						$row_array["idContenido"] = $r->idContenido;
						$row_array["fecha"] = $r->fechaSubido;
						$row_array["nombreUsuario"] = $r->nombreUsuario;
						$row_array["claveOrganizacion"] = $negocio['claveOrganizacion'];
						$row_array["claveConsecutivo"] = $negocio['claveConsecutivo'];
						$row_array["claveServicio"] = $negocio['claveServicio'];
						$row_array["tituloNegocio"] = $negocio['tituloNegocio'];
						array_push($arrayLineaTiempo, $row_array);
						unset($row_array);
					}
				}
			}
			if ($b->tabla=='registro_cambios' && ($valor==0 || $valor==4))
			{
				$res = $this->model->ObtenerInfoBitacora('registro_cambios', 'idRegistro', $b->idRelacion);
				if ($res != null){
					if($res->idNegocio==$idNegocio){
						$r=$this->model->Cambios($b->idRelacion);
						$row_array["tipov"] = 4;
						$row_array["fecha"] = $r->fechaRegistroCambio;
						$row_array["accion"] = $r->accion;
						$row_array["campo1"] = $r->campo1;
						$row_array["campo2"] = $r->campo2;
						$row_array["nombreUsuario"] = $r->nombreUsuario;
						array_push($arrayLineaTiempo, $row_array);
						unset($row_array);
					}
				}
			}
		endforeach;
		echo json_encode($arrayLineaTiempo, JSON_FORCE_OBJECT);
	}


//Metodo para reabrir un negocio perdido
	public function ReabrirPerdido(){
		$valor = 0;
		$FechaAc=null;
		$idNegocio = $_REQUEST['idNegocio'];
		$this->model->ReabrirPerdido($idNegocio);
		$this->model->CambiaStaus($valor,$idNegocio,$FechaAc);
		$campo1="Perdido";
		$campo2="Abierto";
		$accion="Estado";
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);

	}

//Metodo para cambiar el status del negocio
	public function CambiaStatus(){
		date_default_timezone_set("America/Mexico_City");
		$FechaAc=date("Y-m-d H:i:s");
		$valor = $_REQUEST['valor'];
		$idNegocio = $_REQUEST['idNegocio'];
		$Resultado=$this->model->CambiaStaus($valor,$idNegocio,$FechaAc);
		if ($valor==2) {
			$razonPerdido = $_REQUEST['razonPerdido'];
			$comentarios = $_REQUEST['comentarios'];
			$this->model->NegocioPerdido($idNegocio,$razonPerdido,$comentarios);
		}
		if ($valor==1) {
			$campo1="Abierto";
			$campo2="Ganado";
		}elseif ($valor==2) {
			$campo1="Abierto";
			$campo2="Perdido";
		}else{
			$campo1="Ganado";
			$campo2="Abierto";
		}
		$accion="Estado";
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);


	}
//Metodo para ver las personas de contacto
	public function VerPersonas(){
		$array=json_decode($_REQUEST['array']);
		$idNegocio = $_REQUEST['idNegocio'];
		$idCliente = $_REQUEST['idCliente'];
		array_push($array,$idCliente);
		$array = array_reverse($array);
		echo '<table class="table table-bordered table-hover id=" tbl"="">
		<thead>
		<tr style="background-color: #FCF3CF;">
		</td><td align="center"><strong>Acción</strong></td>
		<td><strong>Nombre Completo</strong>
		</td><td><strong>Cliente</strong>
		</td><td><strong>Teléfono</strong>
		</td><td align="center"><strong>Extensión<br><small>Telefónica</small></strong>
		</td><td align="center" style="white-space: nowrap;"><strong>Tipo de<br><small>Teléfono</small></strong>
		</td><td><strong>Correo Electrónico</strong>
		</td><td><strong>Puesto</strong>
		</td><td><strong>Responsable</strong>
		</tr>
		</thead>
		<tbody>';
		foreach($array as $r):
			$resultado=$this->model->ConsultaClientes($r);
			if ($resultado==0) {
			}else{
				echo '<tr><td align="center">';
				if ($idCliente!=$resultado['idCliente']) {
					echo '<a href="#" data-toggle="modal" data-target="#mEliminar" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mPersona" onclick="myFunctionEliminaPersona(';echo $idNegocio.",".$resultado['idCliente']; echo')"> <span class="glyphicon glyphicon-trash"></span>';
				}
				echo'</a></td>
				<td style="white-space: nowrap;">'.$resultado['honorifico']." ".$resultado['nombrePersona'].'</td>
				<td style="white-space: nowrap;">'.$resultado['nombreOrganizacion'].'</td>
				<td>'.$resultado['telefono'].'</td>
				<td align="center">'.$resultado['extension'].'</td>
				<td align="center">'.$resultado['tipoTelefono'].'</td>
				<td>'.$resultado['email'].'</td>
				<td style="white-space: nowrap;">'.$resultado['puesto'].'</td>  
				<td style="white-space: nowrap;">'.$resultado['nombreUsuario'].'</td>
				</tr>';
			}
		endforeach;
		echo '</tbody>
		</table>';
	}
//Metodo para ver los seguidores de contacto
	public function VerSeguidores(){
		$array=json_decode($_REQUEST['array']);
		$idNegocio = $_REQUEST['idNegocio'];
		$idUsuario = $_REQUEST['idUsuario'];
		array_push($array,$idUsuario);
		$array = array_reverse($array);
		echo '<table class="table table-bordered table-hover">
		<thead>
		<tr style="background-color: #FCF3CF;">
		</td><td align="center"><strong>Acción</strong></td>
		<td><strong>Nombre Completo</strong>
		</td><td><strong>Correo Electrónico</strong>
		</tr>
		</thead>
		<tbody>';
		foreach($array as $r):
			$resultado=$this->model->ConsultaUsuarios($r);
			if ($resultado==0) {
			}else{
				echo '<tr>
				<td align="center">';
				if ($idUsuario!=$resultado['idUsuario']) {
					echo '<a href="#" data-toggle="modal" data-target="#mEliminarS" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mPersona" onclick="myFunctionEliminaUsuario(' . $idNegocio . " , " . $resultado['idUsuario']; echo')"> <span class="glyphicon glyphicon-trash"></span></a>';
				}
				echo '</td>
				<td>'.$resultado['nombreUsuario'].'</td>
				<td>'.$resultado['email'].'</td>
				</tr>';
			}
		endforeach;
		echo '</tbody>
		</table>'; 
	}
//Metodo Para tarer el nombre del negocio Chava
	public function NombreNegocio(){
		$idNegocio = $_REQUEST['idNegocio'];
		$Resultado=$this->model->NegocioDetalles($idNegocio);
		echo '<h5><span class="glyphicon glyphicon-usd"></span>'." ".$Resultado['tituloNegocio'].'</h5>';

	}
//Metodo Para agregar participantes a un negocio
	public function AgregarParticipantes(){
		$idNegocio = $_POST['idNegocio'];
		$idCliente = $_POST['idPersona'];
		$this->model->AgregarParticipantes($idNegocio, $idCliente);
		$accion="Participantes";
		$campo1="+1";
		$campo2=$this->model->NombreParticipantes($idCliente);
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);
	}
//Metodo Para agregar segidores a un negocio
	public function AgregarSeguidores(){
		$idNegocio = $_REQUEST['idNegocio'];
		$idUsuario = $_REQUEST['idUsuario'];
		$this->model->AgregarSeguidores($idNegocio,$idUsuario);
		$accion="Seguidores";
		$campo1="+1";
		$campo2=$this->model->NombreUsuario($idUsuario);
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);


	}
//Metodo para cambiar la etapa del negocio chava
	public function CambiaEatapa(){
		$idEtapa = $_REQUEST['idEtapa'];
		$idNegocio = $_REQUEST['idNegocio'];
		$idOldEtapa = $_REQUEST['idOldEtapa'];
		$ArrayCont=$this->modelN->ContenidoEtapas($idOldEtapa);
		$valor2 = explode(",", $ArrayCont);
		$borrar = array($idNegocio);
		$vector_nuevo = array_diff($valor2, $borrar);
		$cont=implode(',',$vector_nuevo);
		$this->modelEmbudo->OrdenarNegocios($idOldEtapa,$cont);
		$this->modelEmbudo->NegocioContenido($idEtapa,$idNegocio);
		$this->modelN->ActualizarContadorActivo($idNegocio);
		$Resultado=$this->model->CambiaEatapa($idEtapa,$idNegocio);
		$accion="Etapa";
		$campo1=$this->model->NombreEtapa($idOldEtapa);
		$campo2=$this->model->NombreEtapa($idEtapa);
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);

	}
//Metodo para eliminar la fecha de cierre del negocio
	public function EliminaFechaCierre(){
		$idNegocio = $_REQUEST['idNegocio'];
		$campo1=$this->model->TraerFechaC($idNegocio);
		$this->model->eliminaFechaCierre($idNegocio);
		$accion="Fecha de cierre prevista";
		$campo2="Vacio";
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);
	}
//Metodo para agregar una fecha de cierre del negocio
	public function RegistrarFechaCierre(){
		$idNegocio = $_REQUEST['idNegocio'];
		$fechaCierre = $_REQUEST['fechaCierre'];
		if ($fechaCierre!=null) {
			$campo2=$this->model->TraerFechaC($idNegocio);
			if ($campo2==null) {
				$campo2="Vacio";
			}
			$this->model->RegistrarFechaCierre($fechaCierre,$idNegocio);
			$accion="Fecha de cierre prevista";
			date_default_timezone_set("America/Mexico_City");
			$FechaAc= date("Y-m-d H:i:s");
			$idUsuario=$_SESSION['idUsuario'];
			$this->model->RegistroCambios($accion,$campo2,$fechaCierre,$FechaAc,$idNegocio,$idUsuario);
			echo "Ok";
		}
	}

	public function ObtenerMes()
	{
		$numeroMes = $_GET['numero'];
		$nombreMes = $this->model->NombreMes($numeroMes);
		echo $nombreMes;
	}

	//Metodo para agregar notas al negocio
	public function RegistrarNota(){
		$idUsuario=$_SESSION['idUsuario'];
		$idNegocio = $_REQUEST['idNegocio'];
		$idCliente = $_REQUEST['idCliente'];
		$idOrganizacion = $_REQUEST['idOrganizacion'];
		$notas = $_REQUEST['notas'];
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		if(isset($_FILES['archivo']))
		{
			$archivo = $_FILES['archivo'];
			$nombre=$_FILES['archivo']['name'];
			$info = new SplFileInfo($nombre);
			$info=$info->getExtension();
			if ($_FILES['archivo']['size'] > 1500000 or $_FILES['archivo']['size']==0) {
				echo "Peso";
			}elseif ($info=='pdf') {
				$no_permitidas= array ("-","á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
				$permitidas= array ("","a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
				$nombre = str_replace($no_permitidas, $permitidas ,$nombre);
				$dir_subida = './assets/files/archivosNegocios/';
				$fichero_subido = $dir_subida . basename($nombre);
				if(move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido))
				{
					$this->model->RegistrarNota($idUsuario,$idNegocio,$idCliente,$idOrganizacion,$notas,$FechaAc,$nombre,$_FILES['archivo']['size']);
					echo "Se ha registrado correctamente la nota";
				}else{
					echo "Error al subir archivo";
				}
			}else{
				echo "Otro";
			}
		}else{
			$this->model->RegistrarNota($idUsuario,$idNegocio,$idCliente,$idOrganizacion,$notas,$FechaAc,null,null);
			echo "Se ha registrado correctamente la nota";
		}
	}

	//Metodo para actualizar las notas del negocio
	public function ActualizarNota(){
		$idUsuario=$_SESSION['idUsuario'];
		$idContenido = $_REQUEST['id'];
		$notas = $_REQUEST['contenido'];
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d");
		echo $idContenido;
		$this->model->ActualizarNota($idContenido,$idUsuario,$notas,$FechaAc);
	}
	//Metodo para eliminar las notas del negocio
	public function EliminarNota(){
		$idContenido = $_REQUEST['id'];
		$this->model->EliminarNota($idContenido);
	}
	//Metodo para eliminar archivos del negocio
	public function EliminarArchivo(){
		$idContenido = $_REQUEST['id'];
		$url = "assets/files/archivosNegocios/";
		$url .= $_REQUEST['nombre'];
		unlink($url);  
		$this->model->EliminarArchivo($idContenido);
	}
//Metodo para desvincular a los contactos
	public function EliminarParticipantes(){
		$idNegocio = $_REQUEST['idNegocio'];
		$idCliente = $_REQUEST['idCliente'];
		$this->model->EliminarParticipantes($idNegocio,$idCliente);
		$accion="Participantes";
		$campo1="-1";
		$campo2=$this->model->NombreParticipantes($idCliente);
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);

	}
//Metodo para desvincular a los usuarios
	public function EliminarSeguidores(){
		$idNegocio = $_POST['idNegocio'];
		$idUsuario = $_POST['idUsuario'];
		$this->model->EliminarSeguidores($idNegocio,$idUsuario);
		$accion="Seguidores";
		$campo1="-1";
		$campo2=$this->model->NombreUsuario($idUsuario);
		date_default_timezone_set("America/Mexico_City");
		$FechaAc= date("Y-m-d H:i:s");
		$idUsuario=$_SESSION['idUsuario'];
		$this->model->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$idNegocio,$idUsuario);
	}

//Metodo para Listar Personas
	public function ListarPersona(){
		$idOrganizacion = $_POST['idOrganizacion'];
		$idCliente=$_POST['idCliente'];
		$array=json_decode($_POST['array']);
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarPersona($idOrganizacion,$idCliente) as $personas):
			if (in_array($personas->idCliente, $array)) {
			}else{
				$row_array['idCliente']  = $personas->idCliente;
				$row_array['nombrePersona']  = $personas->nombrePersona;
				array_push($datos, $row_array);
			}
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ListarParticipantes()
	{
		$idNegocio = $_GET['idNegocio'];
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ObtnenIdPersonas($idNegocio) as $r): 
			$personas = $this->model->ObtnenPersonas($r->idCliente);
			$row_array['idCliente'] = $personas['idCliente']; 
			$row_array['nombrePersona'] = $personas['nombrePersona']; 
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ObtenerArregloP()
	{
		header('Content-Type: application/json');
		$datos = array();
		$idNegocio = $_GET['idNegocio'];
		$idCliente = $_GET['idCliente'];
		$arregloP=$this->model->ArregloDePersonas($idNegocio,$idCliente);
		$arregloP = implode(",", $arregloP);
		$countP=$this->model->ContadorPersonas($idNegocio);
		$row_array['arregloP'] = $arregloP; 
		$row_array['countP'] = $countP; 
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

//Metodo para Listar usuarios
	public function ListarUsuarios()
	{
		$idOrganizacion = $_REQUEST['idOrganizacion'];
		$idUsuario=$_REQUEST['idUsuario'];
		$array=json_decode($_REQUEST['array']);
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarUsuarios($idOrganizacion,$idUsuario) as $usuarios):
			if (in_array($usuarios->idUsuario, $array)) {
			}else{
				$row_array['idUsuario']  = $usuarios->idUsuario;
				$row_array['nombreUsuario']  = $usuarios->nombreUsuario;
				array_push($datos, $row_array);
			}
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ListarSeguidores()
	{
		$idNegocio = $_GET['idNegocio'];
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ObtnenIdUsuario($idNegocio) as $r) :
			$seguidores=$this->model->ObtnenSeguidores($r->idUsuario);
			$row_array['idUsuario'] = $seguidores['idUsuario']; 
			$row_array['nombreUsuario'] = $seguidores['nombreUsuario']; 
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ObtenerArregloS()
	{
		header('Content-Type: application/json');
		$datos = array();
		$idNegocio = $_GET['idNegocio'];
		$idUsuario = $_GET['idUsuario'];
		$arregloS = $this->model->ArregloDeSeguidores($idNegocio,$idUsuario);
		$arregloS = implode(",", $arregloS);
		$countS=$this->model->ContadorSeguidores($idNegocio);
		$row_array['arregloS'] = $arregloS; 
		$row_array['countS'] = $countS; 
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

//Metodo para Listar Personas vinculadas al negocio
	public function ListarPersonaVinculadas()
	{
		$idOrganizacion = $_REQUEST['idOrganizacion'];
		$array=json_decode($_REQUEST['array']);
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarPersonaVinculadas($idOrganizacion) as $personas):
			if (in_array($personas->idCliente, $array)) {

				$row_array['nombrePersona']  = $personas->nombrePersona;
				array_push($datos, $row_array);
			}else{
			}
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

//Metodo para Listar las causa
	public function ListarCausas()
	{
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarCausas() as $r):
			$row_array['nombreCausa']  = $r->nombreCausa;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	/*Upload Files*/
	public function UploadFiles(){
		$ds = DIRECTORY_SEPARATOR;

//Ruta dónde se guardan los archivos
		$storeFolder = 'docs';

		if (!file_exists($storeFolder)) {
			mkdir($storeFolder, 0777, true);
		}

		if (!empty($_FILES)) {

			$tempFile = $_FILES['file']['tmp_name'];             

			$targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;

			$targetFile =  $targetPath. $_FILES['file']['name']; 

			$upload_success = move_uploaded_file($tempFile,$targetFile); 

			/* TEST */

			$success_message = array( 
				'name' => $_FILES['file']['name'],
				'filesize' => $_FILES['file']['size']
			);

			if( $upload_success ) {
				return json_encode($success_message);
			} else {
				return Response::json('error', 400);
			}

		} else {                                                          
			$result  = array();

			$files = scandir($storeFolder);                 
			if ( false!==$files ) {
				foreach ( $files as $file ) {
					if ( '.'!=$file && '..' !=$file && strpos($file, '.') !== false) {       
						$obj['name'] = $file;
						$obj['size'] = filesize($storeFolder.$ds.$file);
						$result[] = $obj;
					}
				}
			}

			header('Content-type: text/json');             
			header('Content-type: application/json');
			echo json_encode($result);
		}
	}

	public function Contadores()
	{
		header('Content-Type: application/json');
		$idNegocio = $_GET['idNegocio'];
		$contArray = array();
		$row_array['countActividades'] = $this->model->ContadorActividades($idNegocio);
		$row_array['countNotas'] = $this->model->ContadorNotas($idNegocio);
		$row_array['countCambios'] = $this->model->ContadorCambios($idNegocio);
		$row_array['countRegCambios'] = intval($row_array['countCambios']) + 1;  
		$row_array['countTotal'] = intval($row_array['countActividades']) + intval($row_array['countNotas']) + intval($row_array['countRegCambios']);
		array_push($contArray, $row_array);
		echo json_encode($contArray, JSON_FORCE_OBJECT);
	}

	public function ObtenerStatusNegocio()
	{
		$idNegocio = $_GET['idNegocio'];
		$negocio = $this->model->ObtenerStatusNegocio($idNegocio);
		echo $negocio->status;
	}

	public function mostrarDetallesActividad()
	{
		$idNegocio = $_GET['idNegocio'];
		$countActividades=$this->model->ContadorActividades($idNegocio);
		$actividades=$this->model->ObtenerActividades($idNegocio);
		$Llamada=$Reunión=$Tarea=$Plazo=$Email=$Comida=$WhatsApp=0;
		foreach ($actividades as $r):
			switch ($r->tipo) {
				case "Llamada":
				$Llamada++;
				break;
				case "Reunion":
				$Reunión++;
				break;
				case "Tarea":
				$Tarea++;
				break;
				case "Plazo":
				$Plazo++;
				break;
				case "Email":
				$Email++;
				break;
				case "Comida":
				$Comida++;
				break;
				case "WhatsApp":
				$WhatsApp++;
				break;
			}
		endforeach;

		$array = array(
			"Llamada" => $Llamada,
			"Reunión" => $Reunión,
			"Tarea" => $Tarea,
			"Plazo" => $Plazo,
			"Email" => $Email,
			"Comida" => $Comida,
			"WhatsApp" => $WhatsApp
		); 
		$sumaArray=array_sum($array);

		header('Content-Type: application/json');
		$data = array(); 
		$r_array['Llamada']  = $Llamada;
		$r_array['Reunión']  = $Reunión;
		$r_array['Tarea']  = $Tarea;
		$r_array['Plazo']  = $Plazo;
		$r_array['Email']  = $Email;
		$r_array['Comida']  = $Comida;
		$r_array['WhatsApp']  = $WhatsApp;
		array_push($data, $r_array);
		$row_array['sumaArray']  = $sumaArray;
		array_push($data, $row_array);
		echo json_encode($data, JSON_FORCE_OBJECT);
	}

	public function mostrarDetallesUserActivo()
	{
		$idNegocio = $_GET['idNegocio'];
		$idUsuario = $_GET['idUsuario'];
		$arregloSAct = $this->model->ArregloDeSeguidoresAct($idNegocio, $idUsuario);
		/*Esta consulta me devuelve los ids de los usuarios que han registrado actividades sin devolver el id principal*/
		$arregloNuevoS = array_unique($arregloSAct); /*se eliminan los ids duplicados*/			
		array_push($arregloNuevoS, $idUsuario);/*Se Agrega el id del usuario al arreglo*/

		foreach ($arregloNuevoS as $id):
			$resSeguidores=$this->model->ObtnenSeguidores($id);
			$arrayUsuario[$resSeguidores['nombreUsuario']] = $id; /*Obtiene el nombre del id de usuario*/
			foreach ($arrayUsuario as $idU) {
				$contR=$this->model->ActividadesUsuarios($idNegocio,$idU);
				$arrayUsuarioN[$resSeguidores['nombreUsuario']] = $contR;
			}
		endforeach; 

		$sumaArray = array_sum($arrayUsuarioN);
		header('Content-Type: application/json');
		$data = array(); 
		$r_array['array']  = $arrayUsuarioN;
		array_push($data, $r_array);
		$row_array['sumaArray']  = $sumaArray;
		array_push($data, $row_array);
		echo json_encode($data, JSON_FORCE_OBJECT);
	}

	public function mostrarSeguidores()
	{
		$idNegocio = $_GET['idNegocio'];
		$idUsuario = $_GET['idUsuario'];
		$countS= (int) $this->model->ContadorSeguidores($idNegocio);
		$arregloS=$this->model->ArregloDeSeguidores($idNegocio, $idUsuario);
		/*Esta consulta me devuelve los ids de los usuarios que estan relacionados con la organizacion y sean diferente a idUsuario enviado como parametro de la tabla de seguidoresCliente resultado*/
		$arregloS = implode(",", $arregloS); 
		header('Content-Type: application/json');
		$data = array();
		$seguidores = array();
		
		foreach ($this->model->ObtnenIdUsuario($idNegocio) as $r):
			$resSeguidores=$this->model->ObtnenSeguidores($r->idUsuario);
			$r_nom['nombreUsuario']  = $resSeguidores['nombreUsuario'];
			$r_nom['foto']  = $resSeguidores['foto'];
			array_push($seguidores, $r_nom);
		endforeach; 

		$r_array['array']  = $arregloS;
		array_push($data, $r_array);
		$r_count['countS']  = $countS;
		array_push($data, $r_count);
		array_push($data, $seguidores);
		echo json_encode($data, JSON_FORCE_OBJECT);
	}
}
?>