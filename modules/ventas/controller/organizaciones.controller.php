<?php 

require_once 'model/organizacion.php';

class OrganizacionesController{
	private $model;
	private $url;
	private $pdo;
	public $mensaje;
	private $error;

	public function __CONSTRUCT(){
		$this->model = new Organizacion();
	}
	public function Index(){
		$clientes=true;
		$cliente=true;
		$page="view/organizaciones/organizaciones.php";
		require_once '../../view/index.php';
	}

	public function Guardar(){
		try
		{
			$organizacion = new Organizacion();
			$organizacion->idOrganizacion=$_POST['idOrganizacion'];
			$organizacion->idUsuario=$_SESSION['idUsuario'];
			$organizacion->nombreOrganizacion=$_POST['nombreOrganizacion'];
			$organizacion->direccion=$_POST['direccion'];
			$organizacion->paginaWeb=$_POST['paginaWeb'];
			$organizacion->telefono=$_POST['telefono'];
			$organizacion->clave=$_POST['clave'];
			$organizacion->localidadEstado=$_POST['localidadEstado'];
			$organizacion->extensionTelefonica=$_POST['extensionTelefonica'];
			$organizacion->tipoTelefono=$_POST['tipoTelefono'];
			$organizacion->idSector=$_POST['idSector'];
			$organizacion->rfc=$_POST['rfc'];
			$nombreOrganizacion=$_POST['nombreOrganizacion'];
			$clave=$_POST['clave'];
			if($organizacion->idOrganizacion>0){
				$this->model->Actualizar($organizacion);
				echo "Se ha actualizado correctamente el cliente";               
			}else{
				$this->model->Registrar($organizacion);
				echo "Se ha registrado correctamente el cliente";
			}
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al guardar la actividad";
		}

	}
	public function VerificaNombre(){
		$NomOrganizacion=$_POST['txtNomOrganizacion'];
		$IdOrganizacion=$_POST['txtIdOrganizacion'];
		$verificarNom=$this->model->VerificarNombre($NomOrganizacion,$IdOrganizacion);
		if ($verificarNom==null) {
			echo "0";
		}else{
			echo "1";
		}
	}
	public function VerificarClave(){
		$Clave=$_POST['txtClave'];
		$IdOrganizacion=$_POST['txtIdOrganizacion'];
		$verificarCla=$this->model->VerificarClave($Clave,$IdOrganizacion);
		if ($verificarCla==null) {
			echo "0";
		}else{
			echo "1";
		}
	}

	/* Metodo para listar todas las organizaciones para el select */
	public function ListarSectorIndustrial(){
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarSectorIndustrial() as $sector_industrial):
			$row_array['idSector']  = $sector_industrial->idSector;
			$row_array['nombreSector']  = $sector_industrial->nombreSector;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}
	public function Eliminar(){
		try {
			$idOrganizacion=$_POST['idOrganizacion'];
			$this->model->Eliminar($idOrganizacion);

		} catch (Exception $e) {
			echo "false";
		}
	}

	public function Contador(){
		$bTexto = $_POST['bTexto'];
		$bAlfabetica = $_POST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->listar();
		}else{
			$stm = $this->model->ConsultaOrganizaciones($bTexto, $bAlfabetica);
		}
		echo $stm->rowCount();
	}

	/* Metodo para realizar las consultas de acuerdo al valor de busqueda */
	public function Consultas(){
		$bTexto = $_POST['bTexto'];
		$bAlfabetica = $_POST['bAlfabetica'];

		$cantRegistros = $_POST['registros'];
		$pagina=(int)(!isset($_POST['pagina'])) ? 1 : $_POST['pagina']; 
		$inicio = (($pagina - 1) * $cantRegistros);

		if ($bTexto ==  '' && $bAlfabetica == ''){
			$tfoot = "true";
			$stm = $this->model->listarClientes($inicio, $cantRegistros);
		}else{
			$tfoot = "false";
			$stm = $this->model->ConsultaOrganizaciones($bTexto, $bAlfabetica);
		}
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$totalRegistros = $this->model->clientes();
		$this->crearTabla($resultado, $cantRegistros, $inicio, $totalRegistros, $pagina, $tfoot);
	}

	public function CrearTabla($resultado, $CantRegistros, $inicio, $totalRegistros, $pagina, $tfoot){
		$totalPaginas = ceil($totalRegistros / $CantRegistros);
		$val = 0;

		/* Operacion matematica para boton siguiente y atras */ 
		$IncrimentNum =(($pagina +1)<=$totalPaginas)?($pagina +1):1;
		$DecrementNum =(($pagina -1))<1?1:($pagina -1);

		echo '<table class="table table table-hover" id="tbl">
		<thead>
		<tr style="background-color: rgba(106, 115, 123, 1);color: white;">
		<td align="center" style="width: 200px;">Editar</td>
		<td style="white-space: nowrap;">Nombre Completo</td>
		<td align="center">Clave</td>
		<td style="white-space: nowrap;">Dirección</td>
		<td align="center" style="width: 200px;">Localidad/Estado</td>
		<td align="center">Teléfono</td>
		<td align="center">Extensión<br><small>telefónica</small></td>
		<td align="center" style="white-space: nowrap;">Tipo de<br><small>teléfono</small></td>
		<td align="center">Contactos</td>
		<td align="center">Negocios<br><small>ganados</small></td>
		<td align="center">Negocios<br><small>perdidos</small></td>
		<td align="center">Negocios<br><small>abiertos</small></td>
		<td align="center">Negocios<br><small>cerrados</small></td>
		<td align="center" style="white-space: nowrap;">Fecha<br><small>próxima</small></td>
		<td align="center">Pagina web</td>
		<td align="center" style="white-space: nowrap;">Sector industrial</td>
		<td style="width: 200px;">Responsable</td>
		<td align="center" style="width: 200px;">RFC</td>
		</tr>
		</thead>
		<tbody>';
		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="18" align="center"> <strong>No se encontraron clientes</strong></td></tr>';
		}
		foreach ($resultado as $r) :
			echo '
			<tr>
			<td align="center"><a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mOrganizacion" onclick="myFunctionEditar('.$r->idOrganizacion; ?> , <?php echo  "'".$r->nombreOrganizacion."'"; ?> , <?php echo  "'".$r->clave."'"; ?>, <?php echo  "'".$r->direccion."'"; ?> , <?php echo  "'".$r->localidadEstado."'"; ?> , <?php echo  "'".$r->paginaWeb."'"; ?> , <?php echo  "'".$r->telefono."'"; ?> , <?php echo  "'".$r->extensionTelefonica."'"; ?> , <?php echo  "'".$r->tipoTelefono."'"; ?> , <?php echo  "'".$r->nombreSector."'"; ?> , <?php echo  "'".$r->idSector."'";?> , <?php echo  "'".$r->rfc."'"; echo ')"> <span class="glyphicon glyphicon-pencil"></span></a></td>
			<td style="white-space: nowrap;"><a href="?c=dealcliente&idOrganizacion='.$r->idOrganizacion.'">'. $r->nombreOrganizacion .'</a></td>
			<td align="center">'. $r->clave .'</td>
			<td style="white-space: nowrap;">'. $r->direccion .'</td>
			<td align="center" style="white-space: nowrap;">'. $r->localidadEstado .'</td>
			<td style="white-space: nowrap;">'. $r->telefono .'</td>
			<td align="center">'. $r->extensionTelefonica .'</td>
			<td style="white-space: nowrap;">'. $r->tipoTelefono .'</td>
			<td align="center">'. $this->model->ContadorPersonas( "'".$r->idOrganizacion."'" ) .'</td>';

			$negociosGanados=$this->model->ContadorNegocios( "'".$r->idOrganizacion."'",1 );
			$negociosPerdidos=$this->model->ContadorNegocios( "'".$r->idOrganizacion."'",2 );
			$negociosAbiertos=$this->model->ContadorNegocios( "'".$r->idOrganizacion."'",0 );
			$negociosCerrados=$negociosGanados+$negociosPerdidos; 
			$fechaProxima=$this->model->ContadorFechaProxima( "'".$r->idOrganizacion."'" );

			echo '
			<td align="center" class="bg-success" >'.$negociosGanados.'</td>
			<td align="center" class="bg-danger">'.$negociosPerdidos.'</td>
			<td align="center" class="active">'.$negociosAbiertos.'</td>
			<td align="center" class="bg-info">'.$negociosCerrados.'</td>
			<td style="white-space: nowrap;">'.$fechaProxima.'</td>
			<td>'. $r->paginaWeb .'</td>
			<td>'. $r->nombreSector .'</td>
			<td style="white-space: nowrap;">'. $r->nombreUsuario .'</td>
			<td style="white-space: nowrap;">'. $r->rfc .'</td>
			</tr>';
		endforeach;
		echo '</tbody>';
		if ($tfoot == "true") {
			echo '
			<tfoot>
			<tr>
			<td colspan="14"><p class="help-block">Mostrando '.$inicio.' a '.($CantRegistros + $inicio).' de '.$totalRegistros.' registros</p></td>
			<td colspan="4" align="right">
			<nav id="navPaginacion">';
			echo'
			<ul class="pagination" style="margin: 0px;">
			<li id="previous" onclick="paginador('.$DecrementNum.');"><a><span aria-hidden="true">&laquo;</span></a></li>';
			/* Se resta y suma con el numero de pag actual con la cantidad de numeros a mostrar */
			$tope = 5;
			$Desde=$pagina-(ceil($tope / 2) - 1);
			$Hasta=$pagina+(ceil($tope / 2) - 1);
			/* Se valida */
			$Desde=($Desde<1)?1: $Desde;
			$Hasta=($Hasta<$tope)?$tope:$Hasta;
			/* Se muestra los numeros de paginas */
			for($i=$Desde; $i<=$Hasta;$i++){
				/* Se valida la paginacion total de registros */
				if($i<=$totalPaginas){
					/* Validamos la pag activo */
					if($i==$pagina){
						echo '<li id="li'.$i.'" class="active" onclick="paginador('.$i.');"><a>'.$i.' <span class="sr-only">(current)</span></a></li>';
					}else {
						echo '<li id="li'.$i.'" onclick="paginador('.$i.');"><a>'.$i.' <span class="sr-only">(current)</span></a></li>';
					}     		
				}
			}

			echo '
			<li id="next" onclick="paginador('.$IncrimentNum.');"><a><span aria-hidden="true">&raquo;</span></a></li>
			</ul>
			</nav>
			</td>
			<tr>
			</tfoot>';
		}
		echo '</table>';
	}

	public function obtenerRegistros()
	{
		$totalRegistros = $this->model->clientes();
		echo $totalRegistros;
	}

	public function VerificaEx(){
		$bTexto = $_POST['bTexto'];
		$bAlfabetica = $_POST['bAlfabetica'];
		try {
			if ($bTexto ==  '' && $bAlfabetica == ''){
				$stm = $this->model->listar();
			}else{
				$stm = $this->model->ConsultaOrganizaciones($bTexto, $bAlfabetica);
			}
			$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
			if($resultado != null){
				echo "false";
			}else{
				echo "true";   
			} 
		} catch (Exception $e) {
			echo 'Algo salio mal. Intentelo de nuevo';
		} 
	}

	public function Exportar(){

		$bTexto = $_POST['buscar'];
		$bAlfabetica = $_POST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$resultado = $this->model->listarO();

		}else{
			$resultado = $this->model->ConsultaOrganizacionesParaExportar($bTexto, $bAlfabetica);
		}
		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
            //Logotipo
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

            //Objeto de PHPExcel
		$objPHPExcel  = new PHPExcel();

            //Propiedades de Documento
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Clientes");

            //Establecemos la pestaña activa y nombre a la pestaña
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Clientes");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(115);
		$objDrawing->setCoordinates('J1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

		$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($estilo);
  //  $objPHPExcel->getActiveSheet()->getStyle('A6:J6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:J6')->applyFromArray($estiloTituloColumnas);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'REPORTE DE CLIENTES');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Nombre Completo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Clave');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Dirección');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Localidad / Estado');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Página web');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Teléfono');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Extensión');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Tipo de teléfono');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Sector industrial');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
		$objPHPExcel->getActiveSheet()->setCellValue('J6', 'RFC');

            //Establecemos en que fila inciara a imprimir los datos
		$fila = 7; 

            //Recorremos los resultados de la consulta y los imprimimos
		foreach ($resultado as $r) :
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->nombreOrganizacion);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->clave);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->direccion);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->localidadEstado);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->paginaWeb);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->telefono);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r->extensionTelefonica);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $r->tipoTelefono);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $r->nombreSector);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $r->rfc);
			$fila++; 
		endforeach;

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Clientes.csv"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');

	}   

    //Metodo para importar oganizaciones
    //Se realiza la lectura de un archivo de Excel y se importan los datos
    //a la tabla Organizaciones, considerando unicamente los atributos
    //que incluye el archivo y dejando los demas atributos en nulo
    //Creado por: Ivan
	public function ImportarOrganizaciones(){

		if(isset($_FILES['file']['name'])){
			try{
                //Se obtiene el nombre del archivo a importar
				$filename = $_FILES['file']['name'];
				$chk_ext = explode(".",$filename);

				if(strtolower(end($chk_ext)) == "xls" || strtolower(end($chk_ext)) == "xlsx") {
                    //si el archivo tiene extension .xls, o xlsx lo abrimos para subir los datos

					$filename = $_FILES['file']['tmp_name'];
                    //Cargamos las librerias necesarias para PHPExcel
					require_once('assets/Classes/PHPExcel.php');
					require_once('assets/Classes/PHPExcel/Reader/Excel2007.php');


                    //Cargamos la hoja de Excel
					$objReader = PHPExcel_IOFactory::createReaderForFile($filename);
					$objReader->setReadDataOnly(true);
					$objPHPExcel = $objReader->load($filename);
                    //Asignamos la primer hoja activa
					$objPHPExcel->setActiveSheetIndex(0);   

                    //El xls original, solo incliuye 3 atributos:
                    //  Id
                    //  Nombre
                    //  Direccion
                    //
                    //Se tendra que traer el ID del sector de la base de datos
                    //Los demas atributos quedaran sin valores.
                    //Obtenemos la letra de la ultima columna usada en la hoja
					$columnas = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
                    //echo "Columnas: $columnas <br>";

                    //Obtenemos el numero de filas con datos
					$filas = $objPHPExcel->getActiveSheet()->getHighestRow();
                    //echo "Filas: $filas <br>";

                    //Obtenemos los datos de Excel de 1 al numero de filas
					for ($i=2; $i <=$filas; $i++){

                        //Obtenemos el Nombre de la Columna B
						$nombre = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
                        //Obtenemos la Direccion de la Columna C


						if($objPHPExcel->getActiveSheet()->cellExists('C'.$i)) {
							$direccion = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
						} else {
                           // La celda no existe o no tiene valor
							$direccion=null; 
						}

                        //Preparamos los datos para insertarlos
						$organizacion = new Organizacion();


						$organizacion->idUsuario=1;

						$organizacion->nombreOrganizacion=$nombre;
						$organizacion->direccion = $direccion;
						$organizacion->paginaWeb= null;
						$organizacion->telefono=null;
						$organizacion->clave=null;
						$organizacion->idSector=1;
						$organizacion->localidadEstado= null;
						$organizacion->extensionTelefonica=null;
						$organizacion->tipoTelefono=null;
						$organizacion->rfc=null;                      

                        //Registramos la persona
						$this->model->Registrar($organizacion);
					}
				}
				else{
                     //si aparece esto es posible que el archivo no tenga el formato de excel adecuado 
					echo "Archivo en fornato incorrecto!";
				}
			}
			catch(Exceptio $e){
				echo $e;
				echo "error";
			}
		}
	}   

}

?>