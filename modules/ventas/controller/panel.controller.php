<?php 
require_once 'model/panel.php';
require_once 'model/negocio.php';
require_once '../../model/usuario.php';


class PanelController{

	private $contenedor;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Panel();
			$this->modelNegocio = new Negocio();
			$this->modelUsuarios = new Usuario();

		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$estadisticas=true;
		$panel=true;
		$page = "view/estadisticas/panel.php";
		require_once '../../view/index.php';
	}

	//Metodo para listar los embudos
	public function ListarEmbudos(){
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarEmbudos() as $e):
			$row_array['idEmbudo']  = $e->idEmbudo;
			$row_array['nombre']  = $e->nombre;
			array_push($datos, $row_array);
		endforeach;   
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

		//Metodo para listar los usuarios
	public function listarUsuarios(){
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->modelUsuarios->Listar() as $e):
			$row_array['idUsuario']  = $e->idUsuario;
			$row_array['nombreUsuario']  = $e->nombreUsuario;
			array_push($datos, $row_array);
		endforeach;   
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}
	

	//Metodo para imprimir las barras de las actividades añadidas.
	public function barraActividadesA(){
		try {
			$idEmbudo=$_REQUEST['valorIdEmbudo'];
			$idUsuario=$_REQUEST['idUsuario'];

			$fechaInicio=$_REQUEST['FechaInicio'];
			$fechaFin=$_REQUEST['FechaFin'];

			if ($fechaInicio != "" && $fechaFin != "") {
				$consulta=$this->model->ListarActividadesARango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin);
				$maximo=$this->model->ObtenerMaximoAARango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin);
			}else{
				$consulta=$this->model->ListarActividadesA($idEmbudo,$idUsuario);
				$maximo=$this->model->ObtenerMaximoAA($idEmbudo,$idUsuario);
			}
			foreach ($consulta as $e):
				switch ($e->tipo) {
					case "Llamada":
					echo '<i class="mdi mdi-phone-in-talk mdi-lg"></i>&nbsp ';
					break;
					case "Comida":
					echo '<i class="fas fa-utensils" style="font-size: 13px;"></i>&nbsp ';
					break;
					case "Email":
					echo '<i class="mdi mdi-email mdi-lg"></i>&nbsp ';
					break;
					case "Plazo":
					echo '<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>&nbsp ';
					break;
					case "Reunion":
					echo '<i class="mdi mdi-group mdi-lg"></i>&nbsp ';
					break;
					case "Tarea":
					echo '<i class="far fa-clock" style="font-size: 14px;"></i>&nbsp ';
					break;
					case "WhatsApp":
					echo '<i class="fab fa-whatsapp" style="font-size: 15px;"></i>&nbsp ';
					break;
				}

				echo'<font size="2"><strong> ' .$e->Contador. ' </strong> '.$e->tipo.' </font>
				<div class="progress" style="height: 8px">';

				//Regla de tres para sacar el porcentaje:
				$x = $e->Contador * 100;
				$porcentaje = $x / $maximo;

				echo '<div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: '.$porcentaje.'%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100%"></div></div>';
			endforeach;
			
		} catch (Exception $e) {
			
		}
	}

	//Metodo para imprimir las barras de las actividades completadas
	public function barraActividadesC(){
		try {
			$idEmbudo=$_REQUEST['valorIdEmbudo'];
			$idUsuario=$_REQUEST['idUsuario'];

			$fechaInicio=$_REQUEST['FechaInicio'];
			$fechaFin=$_REQUEST['FechaFin'];

			if ($fechaInicio != "" && $fechaFin != "") {
				$consulta=$this->model->ListarActividadesCRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin);
				$maximo=$this->model->ObtenerMaximoACRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin);
			}else{
				$consulta=$this->model->ListarActividadesC($idEmbudo, $idUsuario);
				$maximo=$this->model->ObtenerMaximoAC($idEmbudo, $idUsuario);
			}

			foreach ($consulta as $e):
				switch ($e->tipo) {
					case "Llamada":
					echo '<i class="mdi mdi-phone-in-talk mdi-lg"></i>&nbsp ';
					break;
					case "Comida":
					echo '<i class="fas fa-utensils" style="font-size: 13px;"></i>&nbsp ';
					break;
					case "Email":
					echo '<i class="mdi mdi-email mdi-lg"></i>&nbsp ';
					break;
					case "Plazo":
					echo '<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>&nbsp ';
					break;
					case "Reunion":
					echo '<i class="mdi mdi-group mdi-lg"></i>&nbsp ';
					break;
					case "Tarea":
					echo '<i class="far fa-clock" style="font-size: 14px;"></i>&nbsp ';
					break;
					case "WhatsApp":
					echo '<i class="fab fa-whatsapp" style="font-size: 15px;"></i>&nbsp ';
					break;
				}

				echo'<font size="2"><strong> ' .$e->Contador. ' </strong> '.$e->tipo.' </font>
				<div class="progress" style="height: 8px">';

				//Regla de tres para sacar el porcentaje:
				$x = $e->Contador * 100;
				$porcentaje = $x / $maximo;

				echo '<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: '.$porcentaje.'%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100%"></div></div>';
			endforeach;
			
		} catch (Exception $e) {
			
		}
	}

	public function NegociosIniciadosPor(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];

		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];

		if ($fechaInicio != "" && $fechaFin != "") {
			$consultaUsuarios=$this->model->NegociosIniciadorPorRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin);
		}else{
			$consultaUsuarios=$this->model->NegociosIniciadorPor($idEmbudo,$idUsuario);
		}
		
		foreach ($consultaUsuarios as $r):
			echo '<div class="col-xs-12 col-lg-12">
			<div class="col-xs-2 col-lg-2">
			<img src="../../assets/imagenes/'.$r->foto.'" class="img-circle" alt="Usuario" style="min-width: 37px; min-height: 37px; max-width: 37px; max-height: 37px"/>
			</div>
			<div class="col-xs-10 col-lg-10">
			<p style="margin: 0 0 5px;"><strong><font> '.$r->nombreUsuario.'</font></strong></p>
			<p>'.$r->contador.' '; if($r->contador == 1){echo 'Negocio';}else{echo 'Negocios';} echo '</p>
			</div>
			</div>';
		endforeach;
	}

	public function NegociosGanadosPor(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];

		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];
		$dolar=$this->modelNegocio->ConsultaValorDolar();

		if ($fechaInicio != "" && $fechaFin != "") {
			$consultaUsuarios=$this->model->NegociosGanadosPorRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin);
		}else{
			$consultaUsuarios=$this->model->NegociosGanadosPor($idEmbudo,$idUsuario);
		}

		foreach ($consultaUsuarios as $r):
			$temp = 0;
			$suma = 0;

			if ($fechaInicio != "" && $fechaFin != "") {
				$valorNegociosGanados=$this->model->valorNegociosGanadosPorUsuarioRango($r->idUsuario,$idEmbudo,$fechaInicio,$fechaFin);
			}else{
				$valorNegociosGanados=$this->model->valorNegociosGanadosPorUsuario($r->idUsuario,$idEmbudo);
			}

			echo '<div class="col-xs-12 col-lg-12">
			<div class="col-xs-2 col-lg-2" style="padding-top: 5px;">
			<img src="../../assets/imagenes/'.$r->foto.'" class="img-circle" alt="Usuario" style="min-width: 37px; min-height: 37px; max-width: 37px; max-height: 37px"/>
			</div>
			<div class="col-xs-10 col-lg-10">
			<p style="margin: 0 0 5px;"><strong><font> '.$r->nombreUsuario.'</font></strong></p>
			<p class="Contador'.$r->idUsuario.'"></p>
			</div>
			<hr>
			</div>';

			foreach ($valorNegociosGanados as $row):
				if ($row->tipoMoneda == 'USD') {
					$temp = $row->valorNegocio * $dolar;
					$suma = $suma + $temp;
				}else if ($row->tipoMoneda == 'MXN'){
					$suma = $suma + $row->valorNegocio;
				}
			endforeach;

			echo'
			<style>
			.Contador'.$r->idUsuario.':before {
				content:" '.$r->contador.' Negocios "; 
				padding:0 auto;
			} 
			.Contador'.$r->idUsuario.':hover:before {
				content:"$'.number_format($suma, 2, '.', ',').' MXN";  
				padding:0 auto; 
			}
			</style>';
		endforeach;
	}

	public function NegociosGanados(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];

		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];
		$tipoMoneda = $_REQUEST['tipoMoneda'];

		$dolar=$this->modelNegocio->ConsultaValorDolar();

		echo '<div class="row">
		<div class="col-lg-12">
		<div class="col-xs-12 col-lg-12">
		<h4 class="estadisticas">Negocios ganados</h4>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="col-xs-12 col-lg-12">';

		if ($fechaInicio != "" && $fechaFin != "") {
			$contador = $this->model->ListarNegociosGanadosRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin);
			$resultado = $this->model->ValorNegociosGanadosRango($idEmbudo, $dolar, $idUsuario, $fechaInicio, $fechaFin);
		}else{
			$contador = $this->model->ListarNegociosGanados($idEmbudo, $idUsuario);
			$resultado = $this->model->ValorNegociosGanados($idEmbudo, $dolar, $idUsuario);
		}

		$temp = 0;
		$valorGanado = 0;

		foreach ($resultado as $r):
			if ($r->status == 1){
				if($tipoMoneda == 'USD'){
					if ($r->tipoMoneda == 'MXN') {
						$temp = $r->valorNegocio / $dolar;
						$valorGanado = $valorGanado + $temp;
					}else if ($r->tipoMoneda == 'USD'){
						$valorGanado = $valorGanado + $r->valorNegocio;
					}
				}else{
					if ($r->tipoMoneda == 'USD') {
						$temp = $r->valorNegocio * $dolar;
						$valorGanado = $valorGanado + $temp;
					}else if ($r->tipoMoneda == 'MXN'){
						$valorGanado = $valorGanado + $r->valorNegocio;
					}
				}
			}
		endforeach;

		if($tipoMoneda =="VR") 
			$tipoMoneda ="MXN";

		echo'<a href="" class="ganados" data-toggle="modal" data-target="#myModal" onclick="contenidoModal('.'1'.')"><font size="5">'.$contador.'</font></a>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="col-xs-6 col-lg-6">';

		echo '<p><font size="4"><strong>$ '.number_format($valorGanado, 2, '.', ',').'<span> '.$tipoMoneda.'</span></strong></font></p>
		</div>
		<div class="col-xs-6 col-lg-6" align="right">
		<p class="help-block" style="margin-top: 0px;"><i class="fas fa-sort-up" style="color: green; font-size: 25px;"></i></p>
		</div>
		</div>
		</div>';
	}

	public function NegociosPerdidos(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];

		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];
		$tipoMoneda = $_REQUEST['tipoMoneda'];

		$dolar=$this->modelNegocio->ConsultaValorDolar();

		echo '<div class="row">
		<div class="col-lg-12">
		<div class="col-xs-12 col-lg-12">
		<h4 class="estadisticas">Negocios perdidos</h4>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="col-xs-12 col-lg-12">';

		if ($fechaInicio != "" && $fechaFin != "") {
			$contador = $this->model->ListarNegociosPerdidosRango($idEmbudo,$idUsuario,$fechaInicio,$fechaFin);
			$resultado = $this->model->ValorNegociosPerdidosRango($idEmbudo,$dolar,$idUsuario,$fechaInicio,$fechaFin);

		}else{
			$contador = $this->model->ListarNegociosPerdidos($idEmbudo,$idUsuario);
			$resultado = $this->model->ValorNegociosPerdidos($idEmbudo,$dolar,$idUsuario); 		
		}

		$temp = 0;
		$valorPerdido = 0;

		foreach ($resultado as $r):
			if ($r->status == 2){
				if($tipoMoneda == 'USD'){
					if ($r->tipoMoneda == 'MXN') {
						$temp = $r->valorNegocio / $dolar;
						$valorPerdido = $valorPerdido + $temp;
					}else if ($r->tipoMoneda == 'USD'){
						$valorPerdido = $valorPerdido + $r->valorNegocio;
					}
				}else{
					if ($r->tipoMoneda == 'USD') {
						$temp = $r->valorNegocio * $dolar;
						$valorPerdido = $valorPerdido + $temp;
					}else if ($r->tipoMoneda == 'MXN'){
						$valorPerdido = $valorPerdido + $r->valorNegocio;
					}
				}
			}
		endforeach;

		if($tipoMoneda =="VR") 
			$tipoMoneda ="MXN";

		echo'
		<a href="" class="perdidos" data-toggle="modal" data-target="#myModal" onclick="contenidoModal('.'2'.')"><font size="5">'.$contador.'</font></a>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="col-xs-6 col-lg-6">';

		echo '
		<p><font size="4"><strong>$ '.number_format($valorPerdido, 2, '.', ',').'<span> '.$tipoMoneda.'</span></strong></font></p>
		</div>
		<div class="col-xs-6 col-lg-6" align="right">
		<p class="help-block" style="margin-top: 0px;"><i class="fas fa-sort-down" style="color: red; font-size: 25px;"></i></p>
		</div>
		</div>
		</div>';
	}

	public function NuevosNegocios(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];

		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];
		$tipoMoneda = $_REQUEST['tipoMoneda'];


		$dolar=$this->modelNegocio->ConsultaValorDolar();

		echo '<div class="row">
		<div class="col-lg-12">
		<div class="col-xs-12 col-lg-12">
		<h4 class="estadisticas">Negocios iniciados</h4>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="col-xs-12 col-lg-12">';

		if ($fechaInicio != "" && $fechaFin != "") {
			$contador = $this->model->ListarNegociosNuevosRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin);
			$resultado = $this->model->ValorNegociosNuevosRango($idEmbudo, $dolar, $idUsuario, $fechaInicio, $fechaFin);
		}else{
			$contador = $this->model->ListarNegociosNuevos($idEmbudo,$idUsuario);
			$resultado = $this->model->ValorNegociosNuevos($idEmbudo, $dolar, $idUsuario);
		}

		$temp = 0;
		$valorNuevo = 0;

		foreach ($resultado as $r):
			if($tipoMoneda == 'USD'){
				if ($r->tipoMoneda == 'MXN') {
					$temp = $r->valorNegocio / $dolar;
					$valorNuevo = $valorNuevo + $temp;
				}else if ($r->tipoMoneda == 'USD'){
					$valorNuevo = $valorNuevo + $r->valorNegocio;
				}
			}else{
				if ($r->tipoMoneda == 'USD') {
					$temp = $r->valorNegocio * $dolar;
					$valorNuevo = $valorNuevo + $temp;
				}else if ($r->tipoMoneda == 'MXN'){
					$valorNuevo = $valorNuevo + $r->valorNegocio;
				}
			}
		endforeach;

		if($tipoMoneda =="VR") 
			$tipoMoneda ="MXN";

		echo'<a href="" class="abiertos" data-toggle="modal" data-target="#myModal" onclick="contenidoModal('.'3'.')"><font size="5">'.$contador.'</font></a>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-xs-12 col-lg-12">
		<div class="col-xs-6 col-lg-10">';

		echo'
		<p><font size="4"><strong>$ '.number_format($valorNuevo, 2, '.', ',').'<span> '.$tipoMoneda.'</span></strong></font></p>
		</div>
		<div class="col-xs-6 col-lg-2" align="right">
		<p class="help-block" style="margin-top: 0px;"><i class="fas fa-caret-left" style="color: blue; font-size: 25px;"></i></p>
		</div>
		</div>
		</div>';
	}

	public function TablaIniciados(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];
		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];
		$tipoMoneda = $_REQUEST['tipoMoneda'];
		$dollar = $_REQUEST['dollar'];


		if ($fechaInicio != "" && $fechaFin != "") {
			$array = $this->model->NegociosIniciadosPorRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin);
		}else{
			$array = $this->model->NegociosIniciados($idEmbudo,$idUsuario);
		}

		echo '<table class="table table-hover table-striped" style="margin-bottom: 0px;">
		<thead>
		<tr style="background-color: #1F618D; color: #FFF">
		<td><strong>Clave</strong></td>
		<td><strong>Titulo</strong></td>
		<td><strong>Valor del negocio</strong></td>
		</tr>
		</thead>';

		foreach ($array as $r ):

			switch ($tipoMoneda) {
				case 'MXN':
				if ($r->tipoMoneda=='USD'){
					$r->valorNegocio = $r->valorNegocio * $dollar;
					$r->tipoMoneda='MXN';
				}
				break;

				case 'USD':
				if ($r->tipoMoneda=='MXN'){
					$r->valorNegocio = $r->valorNegocio / $dollar;
					$r->tipoMoneda='USD';
				}
				break;
			}

			$r->valorNegocio = number_format($r->valorNegocio, 2, '.', ',');

			echo '
			<tr>
			<td>'.$r->claveOrganizacion.'-'.$r->claveConsecutivo.'-'.$r->claveServicio.'</td>
			<td style="white-space: nowrap;">'.$r->tituloNegocio.'</td>
			<td align="right">$'.$r->valorNegocio.' '.$r->tipoMoneda.'</td>
			</tr>';
		endforeach;
		echo '
		</table>';

	}
	public function TablaPerdidos(){
		$idEmbudo = $_REQUEST['valorIdEmbudo'];
		$idUsuario = $_REQUEST['idUsuario'];
		$fechaInicio = $_REQUEST['FechaInicio'];
		$fechaFin = $_REQUEST['FechaFin'];
		$tipoMoneda = $_REQUEST['tipoMoneda'];
		$dollar = $_REQUEST['dollar'];

		if ($fechaInicio != "" && $fechaFin != "") {
			$array = $this->model->NegociosPerdidosPorRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin);
		}else{
			$array = $this->model->NegociosPerdidos($idEmbudo,$idUsuario);
		}

		echo '<table class="table table-hover table-striped" style="margin-bottom: 0px;">
		<thead>
		<tr style="background-color: #B03A2E; color: #FFF">
		<td><strong>Clave</strong></td>
		<td><strong>Titulo</strong></td>
		<td><strong>Valor del negocio</strong></td>
		<td><strong>Razón</strong></td>
		</tr>
		</thead>';

		foreach ($array as $r ):

			switch ($tipoMoneda) {
				case 'MXN':
				if ($r->tipoMoneda=='USD'){
					$r->valorNegocio = $r->valorNegocio * $dollar;
					$r->tipoMoneda='MXN';
				}
				break;

				case 'USD':
				if ($r->tipoMoneda=='MXN'){
					$r->valorNegocio = $r->valorNegocio / $dollar;
					$r->tipoMoneda='USD';
				}
				break;
			}

			$r->valorNegocio = number_format($r->valorNegocio, 2, '.', ',');

			echo '
			<tr>
			<td>'.$r->claveOrganizacion.'-'.$r->claveConsecutivo.'-'.$r->claveServicio.'</td>
			<td style="white-space: nowrap;">'.$r->tituloNegocio.'</td>
			<td align="right">$ '.$r->valorNegocio.' '.$r->tipoMoneda.'</td>
			<td>'.$r->razonPerdido.'</td>
			</tr>';
		endforeach;
		echo '
		</table>';
	}

	public function TablaGanados(){
		$idEmbudo=$_REQUEST['valorIdEmbudo'];
		$idUsuario=$_REQUEST['idUsuario'];
		$fechaInicio=$_REQUEST['FechaInicio'];
		$fechaFin=$_REQUEST['FechaFin'];
		$tipoMoneda = $_REQUEST['tipoMoneda'];
		$dollar = $_REQUEST['dollar'];

		if ($fechaInicio != "" && $fechaFin != "") {
			$array = $this->model->ListarNegociosGanadosPorRango($idEmbudo, $idUsuario, $fechaInicio, $fechaFin);
		}else{
			$array = $this->model->NegociosGanados($idEmbudo,$idUsuario);
		}

		echo '<table class="table table-hover table-striped" style="margin-bottom: 0px;">
		<thead>
		<tr style="background-color: #148F77; color: #FFF">
		<td><strong>Clave</strong></td>
		<td><strong>Titulo</strong></td>
		<td><strong>Valor del negocio</strong></td>
		</tr>
		</thead>';

		foreach ($array as $r ):

			switch ($tipoMoneda) {
				case 'MXN':
				if ($r->tipoMoneda=='USD'){
					$r->valorNegocio = $r->valorNegocio * $dollar;
					$r->tipoMoneda='MXN';
				}
				break;

				case 'USD':
				if ($r->tipoMoneda=='MXN'){
					$r->valorNegocio = $r->valorNegocio / $dollar;
					$r->tipoMoneda='USD';
				}
				break;
			}

			$r->valorNegocio = number_format($r->valorNegocio, 2, '.', ',');

			echo '
			<tr>
			<td>'.$r->claveOrganizacion.'-'.$r->claveConsecutivo.'-'.$r->claveServicio.'</td>
			<td style="white-space: nowrap;">'.$r->tituloNegocio.'</td>
			<td align="right">$'.$r->valorNegocio.' '.$r->tipoMoneda.'</td>
			</tr>';
		endforeach;
		echo '
		</table>';
	}



}
?>
