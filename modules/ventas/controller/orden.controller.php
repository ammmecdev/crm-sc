<?php

require_once './model/orden.php';

class OrdenController{

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Orden();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$orden = true;

		$this->url="?c=orden";
		$page = "view/page_not/page.php";
		//$page="view/procesos/orden.php";
		require_once '../../view/index.php';
	}

	public function Guardar(){
		try
		{
			$respuesta = array();
			$orden = new Orden();
			$orden->setIdCotizacion($_POST['idCotizacion']);
			$orden->setFecha($_POST['fechaRealizacion']);
			$orden->setNoPedido($_POST['numeroPedido']);
			$orden->setObservaciones($_POST['txtTexto']);
			$orden->setDescripcion($_POST['txtNotas']);

			if($_POST['idOrdenTrabajo']== '0'){ //Guardar
				$ordenObj = $this->model->Registrar($orden);

				$row_array['mensaje']='Se ha guardado correctamente la Orden de Trabajo';
				array_push($respuesta, $row_array);

				echo json_encode($respuesta, JSON_FORCE_OBJECT);
			}else{ //Actualizar
				$orden->setIdOrdenTrabajo($_POST['idOrdenTrabajo']);
				$ordenObj = $this->model->Actualizar($orden);

				$row_array['mensaje']='Se ha modificado correctamente la Orden de Trabajo';
				array_push($respuesta, $row_array);

				echo json_encode($respuesta, JSON_FORCE_OBJECT);
			}
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function switchFiltro( $bTexto)
	{

		$resultado=$this->model->ListarDatosOrdenTodoNegocio($bTexto);

		return $resultado;
	}

	public function Consultas(){
		$bTexto=$_POST['bTexto'];

		$resultado = $this->switchFiltro( $bTexto);
		//$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$accion = $_POST['accion'];

		switch($accion){
			case 'tabla':
				echo json_encode($resultado);
				break;
		}
	}

	public function Eliminar(){
		try{
			$idOrdenTrabajo = $_POST['idOrdenEliminar'];
			$this->model->Eliminar($idOrdenTrabajo);

			echo "Se eliminó correctamente la orden de Trabajo";

		}catch(Exception $e){
			echo $e->getMessage();
			echo "No se pudo eliminar la orden de trabajo;";
		}
	}

}
?>
