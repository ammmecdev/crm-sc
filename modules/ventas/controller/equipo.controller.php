<?php 
require_once 'model/equipo.php';

class EquipoController{

	private $model;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT(){
		$this->model = new Equipo();
	}

	public function Index(){
		$productos=true;
		$equipo=true;
		$page="view/equipos/equipos.php";
		require_once '../../view/index.php';
	}

	  // Metodo para registrar equipos
	public function Guardar(){
		try
		{
			$equipo= new Equipo();
			$equipo->idEquipo=$_REQUEST['equipo'];
			$equipo->clave=$_REQUEST['clave'];
			$equipo->nombreEquipo=$_REQUEST['nombreEquipo'];
			if($equipo->idEquipo > 0){
				$this->model->Actualizar($equipo);
				echo "Se han actualizado correctamente los datos";
			}else{
				$this->model->Registrar($equipo);
				echo "Se ha registrado correctamente el equipo";
			}
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Eliminar(){
		try
		{
			$idEquipo=$_REQUEST['idEquipo'];
			$this->model->Eliminar($idEquipo);
			echo "Se elimino correctamente el equipo";
		}
		catch(Exception $e)
		{
			// $this->error=true;
			echo "Se ha producido un error al eliminar el equipo";
		} 
	}

	public function rowToArray($r){
		return array(
			$r->idEquipo, 
			$r->clave, 
			$r->nombreEquipo,
		);
	}
	
	public function Contador(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->listar();
		}else{
			$stm = $this->model->consulta($bTexto, $bAlfabetica);
		}
		echo $stm->rowCount();
	}


	public function Consultas(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->listar();
		}else{
			$stm = $this->model->consulta($bTexto, $bAlfabetica);
			//$resultado = '';
		}
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$this->crearTabla($resultado);
	}

	  // Metodo para crear la tabla de contenido de equipo
	public function crearTabla($resultado){
		echo '<table class="table table table-hover">
		<thead>
		<tr style="background-color: rgba(106, 115, 123, 1);color: white;">
		<td align="center" width="10%">Acción</td>
		<td align="center" width="10%">Clave</td>
		<td width="80%" style="padding-left:90px;">Nombre del equipo</td>
		</tr>
		</thead>';

		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="9" align="center"> <strong>No se encontraron equipos </strong></td></tr>';
		}else{
			foreach ($resultado as $r) :
				
				echo '
				<tr>
				<td align="center"><a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mEquipo" onclick="myFunctionEditar('; echo $r->idEquipo; echo ",'". $r->clave . "',"; echo "'" . $r->nombreEquipo . "'"; echo')"><span class="glyphicon glyphicon-pencil"></span></a></td>
				<td align="center">'. $r->clave.'</td>
				<td style="padding-left:90px;">'. $r->nombreEquipo.'</td>
				</tr>'; 
			endforeach;
		}
		echo'
		</table>';
	}

	public function verificarClave()
	{
		$clave = $_POST['clave'];
		$idEquipo = $_POST['idEquipo'];
		$verificar = $this->model->verificarClave($clave, $idEquipo);
		if ($verificar == "")
			echo "0";
		else
			echo "1";
	}

	//Metodo para consultar el numero consecutivo
	public function Consecutivo(){
		$numConsecutivo=$this->model->consecutivo();
		if($numConsecutivo == null){
			$val = 1;
			echo str_pad($val, 4, "0", STR_PAD_LEFT);
		}else{
			$sumConsecutivo = $numConsecutivo + 1;
			echo str_pad($sumConsecutivo, 4, "0", STR_PAD_LEFT);
		}
	}

	public function verExportar()
	{
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->listar();
		}else{
			$stm = $this->model->consulta($bTexto, $bAlfabetica);
		}
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		if (!empty($resultado))
			echo "true";
		else
			echo "false";
	}

	public function Exportar(){
		$bTexto = $_REQUEST['buscar'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->listar();
		}else{
			$stm = $this->model->consulta($bTexto, $bAlfabetica);
		}
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);


		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
            //Logotipo
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

            //Objeto de PHPExcel
		$objPHPExcel  = new PHPExcel();

            //Propiedades de Documento
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Equipos");

            //Establecemos la pestaña activa y nombre a la pestaña
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Equipos");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(100);
		$objDrawing->setCoordinates('H1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$styleAling = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);

		$styleAlingCenter = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

		$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('A6:H6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:H6')->applyFromArray($estiloTituloColumnas);

		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'REPORTE DE EQUIPOS');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Clave');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Consecutivo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Nombre del Equipo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Características');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Dimensiones');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'No. de Serie');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Modelo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(24);
		$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Medidas'); 
            //Establecemos en que fila inciara a imprimir los datos
		$fila = 7; 

            //Recorremos los resultados de la consulta y los imprimimos
		foreach ($resultado as $r) :
			$objPHPExcel->getActiveSheet()->getStyle('E'.$fila.':H'.$fila)->applyFromArray($styleAling);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$fila)->applyFromArray($styleAlingCenter);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$fila)->getNumberFormat()->setFormatCode('0000');
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->clave);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->consecutivo);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->nombreEquipo);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->caracteristicas);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->dimensiones);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->noSerie);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r->modelo);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $r->medidas);
			$fila++; 
		endforeach;

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Equipos.csv"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');    
	}   
	
} //fin de la clase

?>