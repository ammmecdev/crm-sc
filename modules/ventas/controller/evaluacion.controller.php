<?php

require_once './model/evaluacion.php';
require_once './model/evaluacion.php';
class EvaluacionController{

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Evaluacion();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$evaluacion=true;
		$this->url="?c=evaluacion";
		$page = "view/page_not/page.php";
		//$page="view/procesos/evaluacion.php";
		require_once '../../view/index.php';
	}

	public function Guardar(){
		try
		{
			$respuesta = array();
			$evaluacion = new Evaluacion();
			$evaluacion->setIdContacto($_POST['selectPersonas']);
			$evaluacion->setFechaEvaluacion($_POST['dateFechaEvaluacion']);
			$evaluacion->setIdOrganizacion($_POST['selectOrganizaciones']);
			$evaluacion->setObservaciones($_POST['txtNotas']);
			$evaluacion->setPromedio(floatval($_POST['promediotxt']));

			if($_POST['idEvaluacion']== '0'){ //Guardar
				$evaluacionObj = $this->model->Registrar($evaluacion);
				$idEvaluacion = $evaluacionObj->idEvaluacion;

				$this->model->RegistrarAspecto($_POST['optUno'],1,$idEvaluacion);
				$this->model->RegistrarAspecto($_POST['optDos'],2,$idEvaluacion);
				$this->model->RegistrarAspecto($_POST['optTres'],3,$idEvaluacion);
				$this->model->RegistrarAspecto($_POST['optCuatro'],4,$idEvaluacion);
				$this->model->RegistrarAspecto($_POST['optCinco'],5,$idEvaluacion);
				$this->model->RegistrarAspecto($_POST['optSeis'],6,$idEvaluacion);


				$row_array['mensaje']='Se ha guardado correctamente la Evaluación del Servicio';
				array_push($respuesta, $row_array);

				echo json_encode($respuesta, JSON_FORCE_OBJECT);
			}else{ //Actualizar
				$evaluacion->setIdEvaluacion($_POST['idEvaluacion']);
				$this->model->Actualizar($evaluacion);

				$this->model->ActualizarAspecto($_POST['optUno'],1,$evaluacion->getIdEvaluacion());
				$this->model->ActualizarAspecto($_POST['optDos'],2,$evaluacion->getIdEvaluacion());
				$this->model->ActualizarAspecto($_POST['optTres'],3,$evaluacion->getIdEvaluacion());
				$this->model->ActualizarAspecto($_POST['optCuatro'],4,$evaluacion->getIdEvaluacion());
				$this->model->ActualizarAspecto($_POST['optCinco'],5,$evaluacion->getIdEvaluacion());
				$this->model->ActualizarAspecto($_POST['optSeis'],6,$evaluacion->getIdEvaluacion());

				$row_array['mensaje']='Se ha actualizado correctamente la Evaluación del Servicio';
				array_push($respuesta, $row_array);

				echo json_encode($respuesta, JSON_FORCE_OBJECT);
			}
		}
		catch(Exception $e)
		{
			$respuesta = array();
			if($_POST['idEvaluacion']== '0'){ //Guardar
				$row_array['mensaje']='No se pudo guardar correctamente la Evaluación del Servicio';
			}else{
				$row_array['mensaje']='No se pudo actualizar correctamente la Evaluación del Servicio';
			}
			array_push($respuesta, $row_array);

			echo json_encode($respuesta, JSON_FORCE_OBJECT);
		}
	}

	public function Eliminar(){
		try{
			$idEvaluacion = $_POST['idEvaluacionEliminar'];
			$this->model->EliminarAspectos($idEvaluacion);
			$this->model->Eliminar($idEvaluacion);
			echo "Se eliminó correctamente la Evaluación";
		}catch(Exception $e){
			echo "Se ha producido un error al eliminar la Evaluación";
		}
	}



	//Metodo para realizar las consultas de acuerdo al valor de busqueda
	public function Consultas()
	{
		$bTexto=$_POST['bTexto'];
		$mes=$_POST['mes'];
		$anio=$_POST['anio'];

		$stm=$this->model->ConsultaEvaluaciones($bTexto,$mes,$anio);
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$accion=$_POST['accion'];
		switch ($accion) {
			//Crear tabla
			case 'tabla':
			echo json_encode($resultado);
			//$this->CrearTabla($resultado);
			break;
			//Exportar
			case 'exportar':
			$titulo='TODAS LAS EVALUACIONES';
			$pie='Actividades.csv';
			$this->CrearArchivo($resultado,$titulo,$pie);
			break;
			//Contador
			case 'contador':
			echo $stm->rowCount();
			break;
		}
	}


	public function AspectosEvaluacion(){
		$idEvaluacion = $_POST['idEvaluacion'];
		$stm = $this->model->ConsultaAspectosEvaluaciones($idEvaluacion);
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		echo json_encode($resultado);
	}

	public function Exportar(){

		$bTexto = $_POST['vBusqueda'];
		$mes = $_POST['selectMes'];
		$anio = $_POST['selectAnio'];
	/*	$bAlfabetica = $_POST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$resultado = $this->model->listarO();

		}else{
			$resultado = $this->model->ConsultaOrganizacionesParaExportar($bTexto, $bAlfabetica);
		}*/
		$stm = $this->model->ConsultaEvaluaciones($bTexto,$mes,$anio);
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
            //Logotipo
	//	$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

            //Objeto de PHPExcel
		$objPHPExcel  = new PHPExcel();

            //Propiedades de Documento
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Evaluaciones");

            //Establecemos la pestaña activa y nombre a la pestaña
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Evaluaciones");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Evaluaciones');
		$objDrawing->setDescription('Evaluaciones');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(115);
		$objDrawing->setCoordinates('J1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s");

		$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($estilo);
  //  $objPHPExcel->getActiveSheet()->getStyle('A6:J6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->applyFromArray($estiloTituloColumnas);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'REPORTE DE LAS EVALUACIONES');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Contacto');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Cliente');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Fecha');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Promedio');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Observaciones');
		/*$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Teléfono');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Extensión');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Tipo de teléfono');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Sector industrial');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
		$objPHPExcel->getActiveSheet()->setCellValue('J6', 'RFC');*/

            //Establecemos en que fila inciara a imprimir los datos
		$fila = 7;

            //Recorremos los resultados de la consulta y los imprimimos
		foreach ($resultado as $r) :
			$nombrePersona = $r->honorifico . ' ' . $r->nombrePersona;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila,  $nombrePersona);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->nombreOrganizacion);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->fechaEvaluacion);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->promedio);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->observaciones);
			$fila++;
		endforeach;

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Evaluaciones.csv"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');

	}

}
?>
