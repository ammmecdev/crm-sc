<?php
require_once 'model/cotizacion.php';
require_once 'model/costo.php';
require_once 'model/negocio.php';

class CotizacionController{

	public $idCotizacion;
	public $idCostoOperativo;

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Cotizacion();
			$this->modelCO = new Costo();
			$this->modelNegocios = new Negocio();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index()
	{
		$cotizacion=true;
		$this->url="?c=cotizacion";
		//$page = "view/page_not/page.php";
		$page="view/procesos/cotizacion.php";
		require_once '../../view/index.php';
	}

	public function Guardar(){
		$respuesta = array();
		$cotizacion = new Cotizacion();
		$cotizacion->setIdCostoOperativo($_POST['idCostoOperativo']);
		$cotizacion->setClaveCotizacion($_POST['claveCotizacion']);
		$cotizacion->setFechaCotizacion($_POST['fecha']);
		$cotizacion->setDireccion($_POST['direccion']);
		$cotizacion->setLocalidad($_POST['localidad']);
		if($_POST['total']!=""){
			$cotizacion->setTotal(trim(str_replace(",","",$_POST['total'])));
		}

		if($_POST['subTotal']!=""){
			$cotizacion->setSubTotal(trim(str_replace(",","",$_POST['subTotal'])));
		}

		if($_POST['iva']!=""){
			$cotizacion->setIva($_POST['iva']);
		}else{
			$cotizacion->setIva(0);
		}

		if($_POST['factor']!=""){
			$cotizacion->setFactor($_POST['factor']);
		}else{
			$cotizacion->setFactor(0);
		}

		$cotizacion->setAtencion($_POST['atencion']);
		$cotizacion->setEstado("Pendiente");

		try{
			if($_POST['idCotizacion'] == 0){
				$cotizacion->Guardar();
				$row_array['mensaje']='Se ha guardado correctamente la Cotización';
				array_push($respuesta, $row_array);

				echo json_encode($respuesta, JSON_FORCE_OBJECT);
			}else{
				$cotizacion->setIdCotizacion($_POST['idCotizacion']);
				$cotizacion->Actualizar();
				$row_array['mensaje']='Se ha actualizado correctamente la Cotización';
				array_push($respuesta, $row_array);

				echo json_encode($respuesta, JSON_FORCE_OBJECT);

			}
		}catch(Exception $e){
			echo $e->getMessage();
			if($_POST['idCotizacion']== '0'){ //Guardar
				$row_array['mensaje']='No se pudo guardar correctamente la Cotización';
			}else{
				$row_array['mensaje']='No se pudo actualizar correctamente la Cotización';
			}
			array_push($respuesta, $row_array);

			echo json_encode($respuesta, JSON_FORCE_OBJECT);
		}
	}

	public function Eliminar(){
		try{
			$idCotizacion = $_POST['idCotizacionEliminar'];
			$this->model->Eliminar($idCotizacion);
			echo "Se eliminó correctamente la Cotización";
		}catch(Exception $e){
			echo "Se ha producido un error al eliminar la Cotización";
		}
	}

	public function switchFiltro($bPeriodo, $bTexto)
	{
		switch ($bPeriodo) {
			case 1:
			$resultado=$this->model->ListarPendiente($bTexto);
			break;

			case 2:
			$resultado=$this->model->ListarRechazado($bTexto);
			break;

			case 3:
			$resultado=$this->model->ListarAprobado($bTexto);
			break;

			case 4:
			$resultado=$this->model->ListarTodas($bTexto);
			break;
		}
		return $resultado;
	}

	public function consultas()
	{
		$bTexto = $_POST['btexto'];
		if(isset($_POST['bPeriodo']))
			$bPeriodo=$_POST['bPeriodo'];
		$resultado = $this->switchFiltro($bPeriodo, $bTexto);
		$accion=$_POST['accion'];
		switch ($accion) {
			case 'tabla':
			echo json_encode($resultado);
			break;
			case 'exportar':
			$titulo='TODAS LAS SOLICITUDES';
			$pie='cotizacion.csv';
			$this->CrearArchivo($resultado,$titulo,$pie);
			break;
		}
	}

	public function listarCostoOperativo()
	{ //Metodo para listar la cuenta (CO) en el modal nueva cotizacion
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->listarCostoOperativo() as $co):
			$row_array['idCostoOperativo']  = $co->idCostoOperativo;
			$row_array['claveNegocio']  = $co->claveNegocio;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function datosCostoOperativo()
	{ // Metodo para obtener los datos del CO seleccionado
		header('Content-Type: application/json');
		$idCostoOperativo=$_POST['idCostoOperativo'];
		$datos = array();
		foreach ($this->model->datosCostoOperativo($idCostoOperativo) as $co):
			$row_array['tituloNegocio']  = $co->tituloNegocio;
			$row_array['nombreOrganizacion']  = $co->nombreOrganizacion;
			$row_array['requisicion']  = $co->requisicion;
			$row_array['nombreUsuario']  = $co->nombreUsuario;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function crearTablaServicios()
	{ //Metodo para crear la tabla de servicios según el CO seleccionado
		$subTotal = 0;
		$idCostoOperativo=$_POST['idCostoOperativo'];
		$servicios = $this->modelCO->ListarCarrito($idCostoOperativo);
		echo json_encode($servicios, JSON_FORCE_OBJECT);
	}

	public function listarNegocioConCotizacion(){
		try{
			if(isset($_POST['idNegocio'])){
				header('Content-Type: application/json');
				$datos = array();
				foreach ($this->modelNegocios->listarNegocioConCotizacion($_POST['idNegocio']) as $negocio):
					$row_array['idNegocio']  = $negocio->idNegocio;
					$row_array['claveOrganizacion']  = $negocio->claveOrganizacion;
					$row_array['claveConsecutivo']  = $negocio->claveConsecutivo;
					$row_array['claveServicio']  = $negocio->claveServicio;
					$row_array['tituloNegocio']  = $negocio->tituloNegocio;
					$row_array['idCliente']  = $negocio->idCliente;
					$row_array['fechaEntrega']  = $negocio->fechaEntrega;
					$row_array['idCotizacion']  = $negocio->idCotizacion;
					array_push($datos, $row_array);
				endforeach;
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}else{
				header('Content-Type: application/json');
				$datos = array();
				foreach ($this->modelNegocios->listarNegociosConCotizacion() as $negocio):
					$row_array['idNegocio']  = $negocio->idNegocio;
					$row_array['claveOrganizacion']  = $negocio->claveOrganizacion;
					$row_array['claveConsecutivo']  = $negocio->claveConsecutivo;
					$row_array['claveServicio']  = $negocio->claveServicio;
					array_push($datos, $row_array);
				endforeach;
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}
		}catch(Exception $e){
			die($e->getMessage());
		}

	}

	public function CambiarEstado(){
		$idCotizacion = $_POST['idCotizacionCambiarEstado'];
		$estado = $_POST['selectEstadoCotizacion'];

		try{
			$this->model->CambiarEstado($idCotizacion,$estado);
			echo "Se cambio el estado correctamente de la Cotización";
		}catch(Exception $e){
			echo "Se ha producido un error al cambiar el estado de la Cotización";
		}
	}
}
