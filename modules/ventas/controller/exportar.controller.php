<?php 

require_once 'model/actividad.php';
require_once 'model/negocio.php';
require_once 'model/persona.php';
require_once 'model/organizacion.php';

class ExportarController{

	private $model;
	private $url;
	private $modelNegocios;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Actividad();
			$this->modelNegocio = new Negocio();
			$this->modelPersona = new Persona();
			$this->modelOrganizacion = new Organizacion();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$exportar=true;
		$page="../../view/configuracion/menu.php";
		$contenedor="../../view/configuracion/exportar.php";
		require_once '../../view/index.php';
	}

	    //Metodo para exportar actividades con filtrado en fechas
	public function ExportarActividades()
	{
		if(!$_POST['periodo']==""){
			$valorBusqueda=$_REQUEST['periodo'];
			switch ($valorBusqueda) {
				case 1:
				$resultado=$this->model->ListarVencido();
				break;
				case 2:
				$resultado=$this->model->ListarCompletado();
				break;
			}
		}else{
			$valorBusqueda=$_POST['valorBusqueda'];
			if (!$valorBusqueda=="")
				$resultado=$this->model->ConsultaActividades($valorBusqueda);
			else
				$resultado=$this->model->Listar();	
		}
				//ob_start();
		require 'assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';

			//Logotipo
		$gdImage = imagecreatefrompng('assets/imagenes/logoammmec.png');

			//Objeto de PHPExcel
		$objPHPExcel  = new PHPExcel();

			//Propiedades de Documento
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Actividades");

			//Establecemos la pestaña activa y nombre a la pestaña
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Actividades");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(100);
		$objDrawing->setCoordinates('G1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
   			//Formato que obtiene la fecha y hora actual
		date_default_timezone_set("America/Mexico_City");
            $A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); //->No obitiene la fecha real


            $objPHPExcel->getActiveSheet()->getStyle('B3:C3')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray($estilo);
            $objPHPExcel->getActiveSheet()->getStyle('A6:G6')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('A6:G6')->applyFromArray($estiloTituloColumnas);

            $objPHPExcel->getActiveSheet()->setCellValue('E3', 'FECHA: '.$A);
            $objPHPExcel->getActiveSheet()->setCellValue('B3', 'REPORTE DE ACTIVIDADES');
            $objPHPExcel->getActiveSheet()->mergeCells('B3:C3');

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Actividad');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('B6', 'Propietario');
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $objPHPExcel->getActiveSheet()->setCellValue('C6', 'Negocio');
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->setCellValue('D6', 'Organización');
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('E6', 'Fecha y Hora');
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('F6', 'Duración');
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('G6', 'Notas');

			//Establecemos en que fila inciara a imprimir los datos
            $fila = 7; 

			//Recorremos los resultados de la consulta y los imprimimos
            foreach ($resultado as $r) :
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->tipo);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->usuario);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->tituloNegocio);
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->nombreOrganizacion);
            	$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->fechaHora);
            	$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->duracion);
            	$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r->notas);
            	$fila++; 
            endforeach;

            header("Content-Type: application/vnd.ms-excel");
            header('Content-Disposition: attachment;filename="Actividades.csv"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');

        }
    }