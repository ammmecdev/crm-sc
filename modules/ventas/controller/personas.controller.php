<?php 
require_once 'model/persona.php';
require_once 'model/organizacion.php';
require_once 'model/telefono.php';

class PersonasController{

	private $model;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT(){
		date_default_timezone_set("America/Mexico_City");
		$this->model = new Persona();
		$this->modelOrganizacion = new Organizacion();
		$this->modelTelefono = new Telefono();
	}
	public function Index(){
		$clientes=true;
		$contacto=true;
		$this->url="?c=personas";
		$page="view/personas/personas.php";
		require_once '../../view/index.php';
	}
	public function Guardar(){
		try
		{
			$persona= new Persona();
			$persona->idCliente=$_REQUEST['idCliente'];
			$persona->idOrganizacion=$_REQUEST['idOrganizacion'];
			$persona->idUsuario=$_SESSION['idUsuario'];
			$persona->nombrePersona=$_REQUEST['nombrePersona'];
			$persona->telefono=$_REQUEST['telefono'];
			$persona->extension=$_REQUEST['extension'];
			$persona->tipoTelefono=$_REQUEST['tipoTelefono'];
			$persona->email=$_REQUEST['email'];
			$persona->honorifico=$_REQUEST['honorifico'];
			$persona->puesto=$_REQUEST['puesto'];

			if($persona->idCliente>0){
				$this->model->Actualizar($persona);
				echo "Se han actualizado correctamente los datos de la persona de contacto";
			}else{
				$this->model->Registrar($persona);
				echo "Se ha registrado correctamente la persona de contacto";
			}      
		}
		catch(Exception $e)
		{
			echo "error";
		}   
	}
	/*Metodo para listar todas las organizaciones para el select*/
	public function ListarOrganizaciones(){
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarOrganizaciones() as $organizacion):
			$row_array['idOrganizacion']  = $organizacion->idOrganizacion;
			$row_array['nombreOrganizacion']  = $organizacion->nombreOrganizacion;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ListarPersonas(){
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarPersonas() as $persona):
			$row_array['idCliente']  = $persona->idCliente;
			$row_array['nombrePersona']  = $persona->honorifico." ".$persona->nombrePersona;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ListarPersona(){
		header('Content-Type: application/json');
		$datos = array();
		$idPersona=$_POST['idPersona'];
		$datos = $this->model->Obtener($idPersona);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function ListarPersonasDeOrganizacion(){
		header('Content-Type: application/json');
		$idOrganizacion = $_POST['idOrganizacion'];
		$datos = array();
		foreach ($this->model->ListarPersonasDeOrganizacion($idOrganizacion) as $persona):
			$row_array['idCliente']  = $persona->idCliente;
			$row_array['nombrePersona']  = $persona->honorifico." ".$persona->nombrePersona;
			array_push($datos, $row_array);
		endforeach;     
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function Eliminar(){
		try
		{
			$idCliente=$_REQUEST['idCliente'];
			$this->model->Eliminar($idCliente);
			echo "Se elimino correctamente la Persona de contacto";
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
			echo "error";

		}   
	}
	public function Contador(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->Listar();
		}else{
			$stm = $this->model->ConsultaPersonas($bTexto, $bAlfabetica);
		}
		echo $stm->rowCount();
	}

	/*Metodo para realizar las consultas de acuerdo al valor de busqueda*/
	public function Consultas(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];

		$cantRegistros = $_REQUEST['registros'];
		$pagina=(int)(!isset($_REQUEST['pagina'])) ? 1 : $_REQUEST['pagina']; 
		$inicio = (($pagina - 1) * $cantRegistros);

		if ($bTexto ==  '' && $bAlfabetica == ''){
			$tfoot = "true";
			$stm = $this->model->ListarContactos($inicio, $cantRegistros);
		}else{
			$tfoot = "false";
			$stm = $this->model->ConsultaPersonas($bTexto, $bAlfabetica);
		}
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$totalRegistros = $this->model->contactos();
		$this->crearTabla($resultado, $cantRegistros, $inicio, $totalRegistros, $pagina, $tfoot);
	}


	public function CrearTabla($resultado, $CantRegistros, $inicio, $totalRegistros, $pagina, $tfoot){
		$totalPaginas = ceil($totalRegistros / $CantRegistros);
		$val = 0;

		/* Operacion matematica para boton siguiente y atras */ 
		$IncrimentNum =(($pagina +1)<=$totalPaginas)?($pagina +1):1;
		$DecrementNum =(($pagina -1))<1?1:($pagina -1);

		echo '
		<table id="tbl" class="table table table-hover" style="width:100%;">
		<thead>
		<tr style="background-color: rgba(106, 115, 123, 1);color: white;">
		<td align="center">Editar</td>
		<td>Nombre Completo</th>
		<td>Cliente</th>
		<td>Correo Electrónico</th>
		<td>Puesto</th>
		<td>Responsable</th>
		</tr>
		</thead>
		<tbody>';

		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="11" align="center"> <strong>No se encontraron personas de contacto</strong></td></tr>';
		} else {
			foreach ($resultado as $r) :
				echo '
				<tr>
				<td align="center">
				<div class="btn-group dropdown">
				<button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-ellipsis-h"></i>
				</button>

				<ul class="dropdown-menu" style="text-aling: rigth;text-align-last: left;">
				<li>
				<a class="lista" href="#" data-toggle="modal" data-target="#mPersona" onclick="myFunctionEditarPersona('.$r->idCliente; ?>,<?php echo  "'".$r->honorifico."'"; ?>,<?php echo  "'".$r->nombrePersona."'"; ?>,<?php echo  $r->idOrganizacion; ?>,<?php echo  "'".$r->nombreOrganizacion."'"; ?>,<?php echo  "'".$r->telefono."'"; ?>,<?php echo  "'".$r->extension."'"; ?>,<?php echo  "'".$r->tipoTelefono."'"; ?>,<?php echo  "'".$r->email."'"; ?>,<?php echo  "'".$r->puesto."'"; ?>,<?php echo ')"><i class="fas fa-pen" style="margin-right: 20px; font-size: 17px;"></i>Editar</a>
				<a class="lista" href="#" data-toggle="modal" data-target="#mAgenda" onclick="MostrarTelefonos('.$r->idCliente; ?>,<?php echo "'".$r->honorifico."'";?>,<?php echo "'".$r->nombrePersona."'"; ?>,<?php echo "'".$r->extension."'"; ?>,<?php echo "'".$r->telefono."'"; ?>,<?php echo "'".$r->tipoTelefono."'"; ?><?php echo ')"><i class="fas fa-address-card" style="margin-right: 15px; font-size: 17px;"></i> Agenda</a>
				</li>
				</ul>
				</div>
				</td>

				<td style="white-space: nowrap;"><a href="?c=dealcontacto&idCliente='.$r->idCliente.'">'.$r->honorifico .''." ".''. $r->nombrePersona.'</a></td>
				<td style="white-space: nowrap;"><a href="?c=dealcliente&idOrganizacion='.$r->idOrganizacion.'">'. $r->nombreOrganizacion.'</a></td>
				<td style="white-space: nowrap;">'. $r->email.'</td>
				<td style="white-space: nowrap;">'. $r->puesto.'</td>
				<td style="white-space: nowrap;">'. $r->nombreUsuario.'</td>
				</tr>';
			endforeach;
		}
		echo '</tbody>';
		if ($tfoot == "true") {
			echo '
			<tfoot>
			<tr>
			<td colspan="5"><p class="help-block">Mostrando '.$inicio.' a '.($CantRegistros + $inicio).' de '.$totalRegistros.' registros</p></td>
			<td colspan="4" align="right">
			<nav id="navPaginacion">';
			echo'
			<ul class="pagination" style="margin: 0px;">
			<li id="previous" onclick="paginador('.$DecrementNum.');"><a><span aria-hidden="true">&laquo;</span></a></li>';
			/* Se resta y suma con el numero de pag actual con la cantidad de numeros a mostrar */
			$tope = 5;
			$Desde=$pagina-(ceil($tope / 2) - 1);
			$Hasta=$pagina+(ceil($tope / 2) - 1);
			/* Se valida */
			$Desde=($Desde<1)?1: $Desde;
			$Hasta=($Hasta<$tope)?$tope:$Hasta;
			/* Se muestra los numeros de paginas */
			for($i=$Desde; $i<=$Hasta;$i++){
				/* Se valida la paginacion total de registros */
				if($i<=$totalPaginas){
					/* Validamos la pag activo */
					if($i==$pagina){
						echo '<li id="li'.$i.'" class="active" onclick="paginador('.$i.');"><a>'.$i.' <span class="sr-only">(current)</span></a></li>';
					}else {
						echo '<li id="li'.$i.'" onclick="paginador('.$i.');"><a>'.$i.' <span class="sr-only">(current)</span></a></li>';
					}     		
				}
			}

			echo '
			<li id="next" onclick="paginador('.$IncrimentNum.');"><a><span aria-hidden="true">&raquo;</span></a></li>
			</ul>
			</nav>
			</td>
			<tr>
			</tfoot>';
		}
		echo '</table>';
	}

	public function obtenerRegistros()
	{
		$totalRegistros = $this->model->contactos();
		echo $totalRegistros;
	}

	public function VerificaEx(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		try {
			if ($bTexto ==  '' && $bAlfabetica == ''){
				$stm = $this->model->Listar();
			}else{
				$stm = $this->model->ConsultaPersonas($bTexto, $bAlfabetica);
			}
			$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
			if($resultado!=null){
				echo "false";
			}else{
				echo "true";   
			} 
		} catch (Exception $e) {
			echo 'Algo salio mal. Intentelo de nuevo';
		} 
	}



	public function CarritoTelefonos($idCliente)
	{
		$idCliente = $_REQUEST['idCliente'];
		$resultado=$this->modelTelefono->ListarTelefonos($idCliente);
		echo'
		<table class="table table-bordered">
		<thead>
		<tr align="center">
		<td style="width: 25px;"><strong>Extensión</strong></td>
		<td style="width: 25px;"><strong>Teléfono</strong></td>
		<td style="width: 25px;"><strong>Tipo de Teléfono</strong></td>
		<td style="width: 25px;"><strong>Acciones</strong></td>
		</tr>
		</thead>
		<tbody>';
		foreach($resultado as $r):
			echo'<tr>
			<td align="center">' . $r->extension . '</td>
			<td align="center">' . $r->telefono . '</td>
			<td align="center">' . $r->tipoTelefono . '</td>
			<td align="center">
			<button type="button" class="btn btn-primary btn-xs" onclick="editarTelefono(' . $r->idTelefono; ?>,<?php echo  "'".$r->extension."'"; ?>,<?php echo  "'".$r->telefono."'"; ?>,<?php echo  "'".$r->tipoTelefono."'"; ?><?php echo ')"><i class="fas fa-pen-square"></i></button>
			<button type="button" class="btn btn-danger btn-xs" onclick="quitarTelefono(' . $r->idTelefono . ')"><i class="fas fa-minus-circle"></i></button>
			</td>
			</tr>';
		endforeach;
		echo'
		</tbody>
		</table>';		
	}

	public function MostrarTelefonos()
	{
		$idCliente = $_REQUEST['idCliente'];
		if (isset($idCliente))
			$this->CarritoTelefonos($idCliente);
		else
			echo "Ha ocurrido un error al intentar mostrar los teléfonos";
	}

	public function AgregarTelefono()
	{
		try {
			$telefono = new Telefono();
			$telefono->idTelefono = $_REQUEST['idTelefono'];
			$telefono->extension = $_REQUEST['extension'];
			$telefono->telefono = $_REQUEST['telefono'];
			$telefono->tipoTelefono = $_REQUEST['tipoTelefono'];
			$telefono->idCliente = $_REQUEST['idCliente'];
			
			if($telefono->idTelefono > 0)
				$this->modelTelefono->ActualizarTelefono($telefono);
			else
				$this->modelTelefono->AgregarTelefono($telefono);
			$this->CarritoTelefonos($telefono->idCliente);
		} catch (Exception $e) {
			echo 'Ha ocurrido un error al intentar añadir el télefono.';
		}
	}

	public function QuitarTelefono()
	{
		try {
			$idTelefono = $_REQUEST['idTelefono'];
			$idCliente = $_REQUEST['idCliente'];
			$this->modelTelefono->EliminarTelefono($idCliente, $idTelefono);
			$this->CarritoTelefonos($idCliente);
		} catch (Exception $e) {
			die($e->getMessage());
			echo 'Ha ocurrido un error al quitar el télefono';
		}
	}


	public function Exportar(){
		$valorBusqueda = $_REQUEST['valorBusqueda'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];

		if ($valorBusqueda== '' && $bAlfabetica == ''){
			$resultado=$this->model->Listar(); 
		}else{
			$resultado=$this->model->ConsultaPersonas($valorBusqueda, $bAlfabetica);
		}
		$resultado=$resultado->fetchAll(PDO::FETCH_OBJ);

		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

		$objPHPExcel  = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Contactos");

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Contactos");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(100);
		$objDrawing->setCoordinates('I1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

		$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->applyFromArray($estiloTituloColumnas);

		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'REPORTE DE PERSONAS');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Honorífico');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Nombre Completo');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Organización');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Teléfono');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Extensión telefónica');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Tipo Teléfono');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Email');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Responsable'); 
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Puesto');
		$fila = 7; 

		foreach ($resultado as $r) :
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->honorifico);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->nombrePersona);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->nombreOrganizacion);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->telefono);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->extension);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->tipoTelefono);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r->email);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $r->nombreUsuario);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $r->puesto);
			$fila++; 
		endforeach;

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Contactos.csv"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');    
	}   

              //Metodo para importar personas
    //Previamente se deben importar las organizaciones
    //Se realiza la lectura de un archivo de Excel y se importan los datos
    //a la tabla Personas, considerando unicamente los atributos
    //que incluye el archivo y dejando los demas atributos en nulo
    //Creado por: Ivan
	public function ImportarPersonas(){

		if(isset($_FILES['file']['name'])){
			try{
                ///Se obtiene el nombre del archivo a importar
				$filename = $_FILES['file']['name'];
				$chk_ext = explode(".",$filename);

				if(strtolower(end($chk_ext)) == "xls" || strtolower(end($chk_ext)) == "xlsx") {
                    //si el archivo tiene extension .xls, o xlsx lo abrimos para subir los datos

					$filename = $_FILES['file']['tmp_name'];
                    //Cargamos las librerias necesarias para PHPExcel
					require_once('assets/Classes/PHPExcel.php');
					require_once('assets/Classes/PHPExcel/Reader/Excel2007.php');


                    //Cargamos la hoja de Excel
					$objReader = PHPExcel_IOFactory::createReaderForFile($filename);
					$objReader->setReadDataOnly(true);
					$objPHPExcel = $objReader->load($filename);
                    //Asignamos la primer hoja activa
					$objPHPExcel->setActiveSheetIndex(0);   

                    //El xls original, solo incliuye 3 atributos:
                    //  NombreCompleto
                    //  Organizacion
                    //  Correo electronico
                    //
                    //Se tendra que traer el ID de la organizacion de la base de datos
                    //Los demas atributos quedaran sin valores.

                    //Obtenemos la letra de la ultima columna usada en la hoja
					$columnas = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
                    //echo "Columnas: $columnas <br>";

                    //Obtenemos el numero de filas con datos
					$filas = $objPHPExcel->getActiveSheet()->getHighestRow();
                    //echo "Filas: $filas <br>";

                    //Obtenemos los datos de Excel de 1 al numero de filas
					for ($i=2; $i <=$filas; $i++){

                        //Obtenemos el nombre de la Columna B
						$nombre=$objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();

                        //Checamos si existe valor en la celda de Organizacion en la Columna C
						if($objPHPExcel->getActiveSheet()->cellExists('C'.$i)) {

                            //Obtenemos el nombre de la organización
							$nombreOrganizacion = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();

                            //Encontrar el ID de la orgaizacion basandonos en el nombre obtenido
							$organizacionId =$this->organizacion->ObtenerId($nombreOrganizacion);
							if ($organizacionId==null){
                                //Error al obtener el ID de la organizacion
							}

						} else {
                           // La celda no existe o no tiene valor
							$organizacionId=null; 
                           //Lanzar error al importar, ya que las personas deben estar asociadas a una organizcion
						}                        

                        //Checamos si existe valor en la celda de email en la Columna D
						if($objPHPExcel->getActiveSheet()->cellExists('C'.$i)) {
							$email = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
						} else {
                           // La celda no existe o no tiene valor
							$email=null; 
						}                        

                        //Preparamos los datos para insertarlos

						$persona= new Persona();
						$persona->idOrganizacion=$organizacionId; 
						$persona->idUsuario=1;
						$persona->nombrePersona=$nombre;
						$persona->telefono=null;
						$persona->extension=null;
						$persona->tipoTelefono=null;
						$persona->email=$email;
						$persona->honorifico=null;
						$persona->puesto=null;

                        //Registramos la persona
						$this->model->Registrar($persona);
					} 
					echo "Importación exitosa!";
				}
				else{
					echo "Archivo en fornato incorrecto!";
				}
			}
			catch(Exceptio $e){
				echo $e;
				echo "error";
			}
		}
	}
}
?>