<?php 

require_once 'model/actividad.php';
require_once 'model/negocio.php';
require_once 'model/persona.php';
require_once 'model/organizacion.php';

class ActividadesController{

	private $model;
	private $url;
	private $modelNegocios;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Actividad();
			$this->modelNegocio = new Negocio();
			$this->modelPersona = new Persona();
			$this->modelOrganizacion = new Organizacion();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$actividades=true;
		$this->url="?c=actividades";
		$page="view/actividades/actividades.php";
		require_once '../../view/index.php';
	}

	public function Guardar(){
		try
		{
			header('Content-Type: application/json');
			$actividad = new Actividad();
			$datos = array();
			/*Datos que todo tipo de actividad ocupa*/
			$actividad->idActividad = $_POST['idActividad'];
			$actividad->idUsuario = $_SESSION['idUsuario'];
			$actividad->tipo = $_POST['tipo'];	
			$actividad->notas = $_POST['notas'];
			$actividad->nombrePersona = $_POST['nombrePersona'];	
			$actividad->timestamp = date("Y-m-d H:i:s");
			$fechaActividad = $actividad->fechaActividad = $_POST['fecha'];
			$validacion="";
			/*-----------*/
			/*Si se habilito la duración se realizará el proceso de validacion para actividades cruzadas*/

			if(isset($_POST['checkTiempo']))
			{
				$duracion = $_POST['duracion'];
				if ($duracion=="") $duracion=0;
				$tipoDuracion = $_POST['tipoDuracion'];
				$actividad->duracion = $duracion." ".$tipoDuracion; 
				$horaInicio = $actividad->horaInicio = $_POST['hora'] ;
				$segundos_horaInicio=strtotime($horaInicio);
				if($tipoDuracion=='minutos')
				{
					if($duracion=='1')
						$tipoDuracion="minuto";
					$tiempo_en_segundos = $duracion * 60;
				} else
				{
					if($duracion=='1')
						$tipoDuracion="hora";
					$tiempo_en_segundos = $duracion * 3600;
				}
				$horaFin = $actividad->horaFin = date("H:i",$segundos_horaInicio + $tiempo_en_segundos).':00'; 
				$cruzadas = $this->model->ValidarActividad($fechaActividad,$horaInicio,$horaFin);
				if($cruzadas!=null && $_POST['confirmado']!='true'){	
					$encabezado='
					<table class="table table-hover" style="margin-bottom=-80px; margin-top:-15px; border: 0">
					<tr>
					<th>Act.</th>
					<th>Fecha</th>
					<th>Inicio</th>
					<th>Fin</th>
					</tr>';
					$body='';
					$n=0;
					foreach ($cruzadas as $cruzada){
						if($cruzada->idActividad!=$actividad->idActividad){
							$body=$body.
							'<tr>
							<td>'.$cruzada->tipo.'</td>
							<td>'.$cruzada->fechaActividad.'</td>
							<td>'.date("g:i a",strtotime($cruzada->horaInicio)).'</td>
							<td>'.date("g:i a",strtotime($cruzada->horaFin)).'</td>
							</tr>';
							$n++;
						}
					}
					$fin='</table>';
					if($n>0){
						$row_array['mensaje']='cruzada';
						$row_array['tabla']=$encabezado.$body.$fin;
						array_push($datos, $row_array);
						$validacion='cruzada';	
					}				
				}
			}

			if($validacion=="")
			{
				if($actividad->idActividad>0)
				{
					
					if($_POST['actividad']=='interna'){
						$actividad->idNegocio = null;
						$actividad->idOrganizacion = null;
					}else{
						$actividad->idOrganizacion = $_POST['idOrganizacion'];
						/*Si el radio fultro es por cliente y no se selecciono negocio, el negocio será null*/
						if($_POST['idNegocio']=='')
							$actividad->idNegocio = null;
						else
							$actividad->idNegocio = $_POST['idNegocio'];	
					}
					$this->model->Actualizar($actividad);
					$row_array['mensaje']='Se ha actulizado correctamente la actividad';
				}else{
					/*Si es una actividad interna, los identificadores foraneos serán nulos*/
					if($_POST['radioFiltros']=='interna'){
						$actividad->idNegocio = null;
						$actividad->idOrganizacion = null;
					}else{
						$actividad->idOrganizacion = $_POST['idOrganizacion'];
						/*Si el radio fultro es por cliente y no se selecciono negocio, el negocio será null*/
						if($_POST['idNegocio']=='')
							$actividad->idNegocio = null;
						else
							$actividad->idNegocio = $_POST['idNegocio'];				
					}
					$this->model->Registrar($actividad);
					$row_array['mensaje']='Se ha registrado correctamente la actividad';
				}
				array_push($datos, $row_array);
			}
			echo json_encode($datos, JSON_FORCE_OBJECT);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
			$datos = array();
			$row_array['mensaje']='Se ha producido un error al intentar guardar la actividad';
			array_push($datos, $row_array);
			echo json_encode($datos, JSON_FORCE_OBJECT);
		}	
	}

	public function Eliminar()
	{
		try
		{
			$idActividad=$_POST['idActividad'];
			$this->model->Eliminar($idActividad);
			echo "Se elimino correctamente la actividad";
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al eliminar la actividad";
		}	
	}

	//Metodo para listar todos los negocios para el select
	public function ListarNegocios()
	{
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarNegocios() as $negocio):
			$row_array['idNegocio']  = $negocio->idNegocio;
			$row_array['tituloNegocio']  = $negocio->tituloNegocio;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	//Metodo para listar todas las personas para el select
	public function ListarPersonasPorOrganizacion()
	{
		header('Content-Type: application/json');
		$idOrganizacion=$_POST['idOrganizacion'];
		$datos = array();
		foreach ($this->model->ListarPersonasPorOrganizacion($idOrganizacion) as $persona):
			$row_array['nombrePersona']  = $persona->nombrePersona;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function	ConsultaNombreOrganizacion()
	{
		$organizacion=$this->model->ConsultaNombreOrganizacion($_POST['idOrganizacion']);
		echo $organizacion->nombreOrganizacion;

	}

	//Metodo para listar todas las organizaciones para el select
	public function ListarOrganizaciones()
	{
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->model->ListarOrganizaciones() as $organizacion):
			$row_array['idOrganizacion']  = $organizacion->idOrganizacion;
			$row_array['nombreOrganizacion']  = $organizacion->nombreOrganizacion;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}


	public function ListarNegociosPorOrganizacion()
	{
		header('Content-Type: application/json');
		$idOrganizacion=$_POST['idOrganizacion'];
		$datos = array();
		foreach ($this->model->ListarNegociosPorOrganizacion($idOrganizacion) as $negocio):
			$row_array['idNegocio']  = $negocio->idNegocio;
			$row_array['tituloNegocio']  = $negocio->tituloNegocio;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	//Metodo para listar personas en base al negocio
	public function ListarPersonasPorNegocio()
	{
		header('Content-Type: application/json');
		$idNegocio=$_POST['idNegocio'];
		$datos = array();
		$personas = $this->model->ListarPersonasPorNegocio($idNegocio);
		$row_array['idCliente']  = $personas->idCliente;
		$row_array['nombrePersona']  = $personas->nombrePersona;
		array_push($datos, $row_array);	
		foreach ($this->model->ListarParticipantesPorNegocio($idNegocio) as $persona):
			$row_array['idCliente']  = $persona->idCliente;
			$row_array['nombrePersona']  = $persona->nombrePersona;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	//Metodo para listar organizaciones en base al negocio
	public function ObtenerOrganizacionPorNegocio()
	{
		header('Content-Type: application/json');
		$idNegocio = $_POST['idNegocio'];
		$datos = array();
		$organizacion = $this->model->ObtenerOrganizacionPorNegocio($idNegocio);
		$row_array['idOrganizacion']  = $organizacion->idOrganizacion;
		$row_array['nombreOrganizacion']  = $organizacion->nombreOrganizacion;
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	//Metodo para cambiar el completo con checkbox
	public function CambiaEstado()
	{
		try {
			$idActividad=$_POST['idActividad'];
			$fechaCompletado=$_POST['fechaCompletado'];
			$completado=$_POST['completado'];
			if($completado==1){
				$this->model->CambiaEstado($idActividad,null,0);
			}
			else{
				$this->model->CambiaEstado($idActividad,$fechaCompletado,1);
			}
			echo $this->mensaje="Se ha actualizado correctamente el estado de actividad";
		} catch (Exception $e) {
			echo $this->mensaje="No se pudo actualizar el estado de actividad";
		}
	}

	public function rowToArray($r){
		return array(
			$r->idActividad, 
			$r->tipo, 
			$r->notas,
			$r->fechaActividad,
			$r->duracion,
			$r->horaInicio,
			$r->idOrganizacion,
			$r->nombreOrganizacion,
			$r->idNegocio,
			$r->tituloNegocio,
			$r->nombrePersona
		);
	}

	public function CrearTabla($resultado, $tAct)
	{
		echo '  
		<div class="porlets-content">
		<div class="table-responsive">
		<table class="display table table table-hover" id="dynamic-table">
		<thead>
		<tr style="background-color: rgba(106, 115, 123, 1);color: white;">
		<td align="center">Acción</td>
		<td>Actividad</td>
		<td>Descripción</td>';
		if($tAct!='leads' && $tAct!='internas')
			echo '<td>Negocio</td>';
		if($tAct!='internas')
			echo '<td>Cliente</td>';
		echo '
		<td>Fecha</td>
		<td>Hora</td>';
		if($tAct!='internas')
			echo'<td>Contacto</td>';
		echo '
		</tr>
		</thead>';
		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="10" align="center"> <strong>No se encontraron actividades </strong></td></tr>';
		}
		foreach ($resultado as $r) :
			if (!isset($r->tituloNegocio)){
				$r->tituloNegocio='';
			}
			if (!isset($r->nombreOrganizacion)){
				$r->idOrganizacion='';
				$r->nombreOrganizacion='';
			}
			$array = $this->rowToArray($r);
			$datos = implode(",", $array);
			echo ' 
			<tr>
			<td align="center" style="white-space: nowrap;">
			<button type="button" data-toggle="modal" data-target="#mTerminar"'; 
			if($r->completado==0)
			{ 
				echo 'onclick="cambiaEstado('.$r->idActividad.',0,null)" class="btn btn-xs btn-default"'; 
			}else
			{ 
				echo 'onclick="cambiaEstado('.$r->idActividad.',1,'; echo "'".$r->fechaCompletado."'"; echo ')" class="btn btn-xs btn-success"';  
			} 
			echo 'id="checkEstado"><span class="glyphicon glyphicon-check"></span></button>';
			echo '&nbsp';
			if($r->completado==0)
			{ 
				echo '<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mActividades" onclick="editarActividad(';
				echo "'". $datos . "'"; 
				echo')"> <span class="glyphicon glyphicon-pencil"></span></button></td>'; 
			}else
			{ 
				echo '<button href="#" class="btn btn-primary btn-xs disabled"><span class="glyphicon glyphicon-pencil"></span></button></td>'; 
			} 
			echo '<td>'. $r->tipo .' </td>
			<td>'. $r->notas .'</td>';

			if($tAct!='leads' && $tAct!='internas')
				echo '<td>'. $r->tituloNegocio .'</td>';
			if($tAct!='internas')
				echo '<td>'. $r->nombreOrganizacion .'</td>';
			echo '<td>'. $r->fechaActividad .'</td>';
			if ($r->horaInicio!=null)
				echo '<td>'. date("g:i a",strtotime($r->horaInicio)) .'<strong> - </strong>'. date("g:i a",strtotime($r->horaFin)) .'</td>';
			else
				echo '<td>Indefinido</td>';
			if($tAct!='internas')
				echo '<td>'. $r->nombrePersona .'</td>';
			echo '</tr>';
			
		endforeach;
		echo '</table></div></div>';
	}

	public function switchFiltro($bPeriodo, $bTexto, $bActividad, $tAct)
	{
		switch ($bPeriodo) {
			case 1:
			$resultado=$this->model->ListarPlaneado($bTexto, $bActividad, $tAct);
			break;

			case 2:
			$resultado=$this->model->ListarVencido($bTexto, $bActividad, $tAct);
			break;

			case 3:
			$resultado=$this->model->ListarHoy($bTexto, $bActividad, $tAct);
			break;

			case 4:
			$resultado=$this->model->ListarTomorrow($bTexto, $bActividad, $tAct);
			break;

			case 5:
			$resultado=$this->model->ListarEstaSemana($bTexto, $bActividad, $tAct);
			break;

			case 6:
			$resultado=$this->model->ListarProximaSemana($bTexto, $bActividad, $tAct);
			break;

			case 7:
			$resultado=$this->model->ListarCompletado($bTexto, $bActividad, $tAct);
			break;

			case 8:
			$resultado=$this->model->ListarLeads($bTexto, $bActividad, $tAct);
			break;
		}
		return $resultado;
	}

	//Metodo para realizar las consultas de acuerdo al valor de busqueda
	public function Consultas()
	{
		$tAct = $_POST['tAct'];
		$bTexto=$_POST['bTexto'];
		$bActividad=$_POST['bActividad'];
		if(isset($_POST['bPeriodo']))
			$bPeriodo=$_POST['bPeriodo'];
		else
			$bPeriodo=null;
		if(!isset($_POST['periodos'])){
			$stm=$this->model->ConsultaActividades($bTexto, $bActividad, $tAct);
		}else{
			$stm = $this->switchFiltro($bPeriodo, $bTexto, $bActividad, $tAct);
		}
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$accion=$_POST['accion'];
		switch ($accion) {
			//Crear tabla
			case 'tabla':
			$this->CrearTabla($resultado, $tAct);	
			break;
			//Exportar
			case 'exportar':
			$titulo='TODAS LAS ACTIVIDADES';
			$pie='Actividades.csv';
			$this->CrearArchivo($resultado,$titulo,$pie);
			break;
			//Contador
			case 'contador':
			echo $stm->rowCount();	
			break;
		}
	}

	//Metodo para exportar actividades con filtrado en fechas
	public function ExportarPorFechas()
	{
		$periodo=$_POST['periodo'];
		$fechaInicio=$_POST['fechaInicio'];
		$fechaFin=$_POST['fechaFin'];
		$titulo='';
		$pie='';
		if($periodo == 'vencimiento'){
			$resultado=$this->model->ExportarPorFechaVencida($fechaInicio, $fechaFin);
			$titulo='ACTIVIDADES VENCIDAS';	
			$pie='Actividades vencidas.csv';
			$this->CrearArchivo($resultado,$titulo,$pie);		
		}else{
			$resultado=$this->model->ExportarPorFechaCompleta($fechaInicio, $fechaFin);
			$titulo='ACTIVIDADES COMPLETAS';
			$pie='Actividades completas.csv';
			$this->CrearArchivo($resultado,$titulo,$pie);			
		}
	}

	//De aqui para abajo si
	public function CrearArchivo($resultado,$titulo,$pie)
	{
				//ob_start();

		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';

			//Logotipo
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

			//Objeto de PHPExcel
		$objPHPExcel  = new PHPExcel();

			//Propiedades de Documento
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Actividades");

			//Establecemos la pestaña activa y nombre a la pestaña
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Actividades");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(100);
		$objDrawing->setCoordinates('G1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>20,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
    		//Formato que obtiene la fecha y hora actual
		date_default_timezone_set("America/Mexico_City");
            $A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); //->No obitiene la fecha real


            $objPHPExcel->getActiveSheet()->getStyle('B3:C3')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray($estilo);
            $objPHPExcel->getActiveSheet()->getStyle('A6:G6')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('A6:G6')->applyFromArray($estiloTituloColumnas);

            $objPHPExcel->getActiveSheet()->setCellValue('E3', 'FECHA: '.$A);
            $objPHPExcel->getActiveSheet()->setCellValue('B3', $titulo);
            $objPHPExcel->getActiveSheet()->mergeCells('B3:C3');

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Actividad');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
            $objPHPExcel->getActiveSheet()->setCellValue('B6', 'Propietario');
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
            $objPHPExcel->getActiveSheet()->setCellValue('C6', 'Negocio');
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $objPHPExcel->getActiveSheet()->setCellValue('D6', 'Organización');
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('E6', 'Fecha');
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('F6', 'Duración');
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->setCellValue('G6', 'Notas');

			//Establecemos en que fila inciara a imprimir los datos
            $fila = 7; 

			//Recorremos los resultados de la consulta y los imprimimos
            foreach ($resultado as $r) :
            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->tipo);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->usuario);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->tituloNegocio);
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->nombreOrganizacion);
            	$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r->fechaActividad);
            	$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r->duracion);
            	$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r->notas);
            	$fila++; 
            endforeach;

            header("Content-Type: application/vnd.ms-excel");
            header('Content-Disposition: attachment;filename='.$pie);
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');

        }
    }