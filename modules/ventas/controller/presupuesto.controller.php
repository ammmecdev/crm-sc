<?php
require_once 'model/presupuesto.php';
require_once 'model/negocio.php';

class PresupuestoController{

	private $contenedor;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Presupuesto();
			$this->modelNegocio = new Negocio();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$procesos=true;
		$presupuesto=true;
		$this->url="?c=presupuesto";
		$page="view/presupuestos/presupuestos.php";
		require_once '../../view/index.php';
	}

	 // Metodo para registrar negocios
	public function Guardar(){
		try
		{
			$presupuesto= new Presupuesto();
			$presupuesto->idPresupuestoGeneral=$_REQUEST['idPresupuestoGeneral'];
			$presupuesto->nombrePresupuesto=$_REQUEST['nombrePresupuesto'];
			$presupuesto->periodo=$_REQUEST['periodo'];
			$presupuesto->totalAnual=0;
			$presupuesto->idUsuario=$_SESSION['idUsuario'];
			$presupuesto->idEmbudo=$_SESSION['idEmbudo'];

			if($presupuesto->idPresupuestoGeneral > 0){
				$this->model->Actualizar($presupuesto);
				echo "actualizar";
			}else{	
				$this->model->Registrar($presupuesto);
				echo "Se ha registrado correctamente el presupuesto";
			}	
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al registrar el presupuesto";
		} 
	}

	public function rowToArrayPG($r){
		return array(
			$r->idPresupuestoGeneral, 
			$r->nombrePresupuesto, 
			$r->periodo,
			$r->totalAnual,
			$r->idUsuario,
			$r->idEmbudo,
			$r->nombreUsuario
		);
	}

	public function ConsultaGeneral(){
		$idUsuario = $_SESSION['idUsuario'];
		$idEmbudo = $_SESSION['idEmbudo'];
		$resultado = $this->model->Listar($idUsuario, $idEmbudo);
		echo '<table class="table table-bordered table-hover" style="margin-bottom: 0px;">
		<thead>
		<tr style="background-color: rgb(106, 115, 123); color: #FFF">
		<td>Nombre</td>
		<td align="center">Periodo</td>
		<td align="center">Total anual</td>
		<td>Responsable</strong></td>
		<td align="center">Editar</td>
		<td align="center">Eliminar</td>
		</tr>
		</thead>';
		foreach ($resultado as $r ):
			$array = $this->rowToArrayPG($r);/* se llena el arreglo con los datos del ciclo */
			$datos = implode(",", $array);
			echo '
			<tr>
			<td>'.$r->nombrePresupuesto.'</td>
			<td align="center">'.$r->periodo.'</td>
			<td align="center">$'.number_format($r->totalAnual, 2, '.', ',').'</td>
			<td>'.$r->nombreUsuario.'</td>
			<td align="center" width="5%"><a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#mPresupuestoGeneral" onclick="FunctionEditarPG('; echo "'". $datos . "'"; echo')"><span class="glyphicon glyphicon-pencil"></span></a></td>
			<td align="center" width="5%"><a href="" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#mEliminarPG" onclick="FunctionEliminarPG('; echo "'". $datos . "'"; echo')"><i class="fas fa-trash-alt" style="font-size: 14px;"></i></a></td>
			</tr>';
		endforeach;
		echo '
		</table>';
	}

	public function Eliminar(){
		try
		{
			$idPG=$_REQUEST['idPresupuestoGeneral'];
			$this->model->Eliminar($idPG);
			echo "Se elimino correctamente el presupuesto";
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al eliminar el presupuesto";
		} 
	}

	public function EliminarPN(){
		try
		{
			$idPN=$_REQUEST['idPresupuestoN'];
			$this->model->EliminarPN($idPN);
			echo "Se elimino correctamente el negocio del presupuesto";
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al eliminar el negocio del presupuesto";
		} 
	}

	public function GuardarNPresupuesto(){
		try
		{
			$presupuesto= new Presupuesto();
			$presupuesto->idPresupuesto=$_REQUEST['idPresupuesto'];
			$presupuesto->idPresupuestoGeneral=$_REQUEST['idPresupuestoGeneral'];
			$presupuesto->idNegocio=$_REQUEST['idNegocio'];
			$presupuesto->descripcion=$_REQUEST['descripcion'];
			$presupuesto->totalAnual=0;

			if($presupuesto->idPresupuesto > 0){
				$this->model->ActualizarPN($presupuesto);
				echo "Se han actualizado correctamente los datos";	
			}else{
				$this->model->RegistrarPN($presupuesto);
				echo "Se ha registrado correctamente el presupuesto";
			}	
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		} 
	}

	public function PrepararDatos($resultado,$Mes)
	{
		$datos=array();
		foreach ($resultado as $r):
			$row_array['idPresupuesto']=$r->idPresupuesto;
			$row_array['idNegocio']=$r->idNegocio;
			$row_array['tituloNegocio']=$r->tituloNegocio;
			$row_array['nombreUsuario']=$r->nombreUsuario;
			$row_array['nombreOrganizacion']=$r->nombreOrganizacion;
			$row_array['claveServicio']=$r->claveServicio;
			$row_array['nombreEquipo']=$r->nombreEquipo;
			$row_array['descripcion']=$r->descripcion;
			$row_array['periodo']=$r->periodo;
			$row_array['totalAnual']=$r->totalAnual;
			foreach($this->model->ConsultaTotalesMensuales($r->idPresupuesto) as $p):
				$row_array[$p->mes]=$p->totalMensual;
			endforeach;	    
			array_push($datos, $row_array);
		endforeach;

		$posicion = 0;
		if ($Mes != null) {
			foreach ($datos as $row):
				if ($row[$Mes] == 0) {
					unset($datos[$posicion]);
				}
				$posicion = $posicion + 1;
			endforeach;
			return $datos;
		}else{
			return $datos;	
		}
	}


	public function pintarTabla(){
		$bTexto = $_REQUEST['bTexto'];
		$Mes = $_REQUEST['Mes'];
		$idUsuario = $_SESSION['idUsuario'];
		$idEmbudo = $_REQUEST['idEmbudo'];
		$status = $_REQUEST['status'];
		$idPresupuestoGeneral = $_REQUEST['idPresupuestoGeneral'];
		$maximo=$this->model->ObtenerValorMaximo($idEmbudo,$idPresupuestoGeneral);

		if ($idPresupuestoGeneral == null || $status == "false") {
			echo '
			<div class="alert" role="alert" style="background-color: rgba(106, 115, 123, 1);color: white;">
			<p align="center" style="font-size: 18px;">¡Importante! <br> <small>Seleccione un presupuesto para visualizar su contenido.</small></p>
			</div>
			<div style="padding-top: 30px;">
			<img src="../../assets/imagenes/sc.png" alt="AMMMEC" class="center-block imag" style="width: 300px;">
			<h2 align="center"><span style="color: rgba(159, 161, 164)">PRESUPUESTO</span</h2>
			</div>
			';
		}else{
			if ($bTexto == null)
				$resultado = $this->model->listarPresupuestos($idEmbudo,$idPresupuestoGeneral,$idUsuario);
			else
				$resultado = $this->model->listarPresupuestosFiltro($idEmbudo,$idPresupuestoGeneral,$bTexto,$idUsuario);

			$datos=$this->PrepararDatos($resultado,$Mes);
			$query=$this->model->listarDatosPG($idPresupuestoGeneral);
			// print_r($datos);
			echo '<table class="table table-hover" id="tbl" style="margin-bottom: 0px;">
			<thead>
			<tr style="padding-top: 14px; background: " align="center">';
			foreach ($query as $r):
				echo'
				<td colspan="7" align="left" style="padding-top: 14px;"><p style="color: #; margin: 0px;"><font size="4">'.$r->nombrePresupuesto.'</font></p></td>';
			endforeach;

			echo'
			<td align="right" style="padding-top: 14px;"><p style="color: #2980B9; margin: 0px;"><font size="2"><strong>TOTALES: </strong></font></p></td>';

			$sumaTotal = $Ene = $Feb = $Mar = $Abr = $May = $Jun = $Jul = $Ago = $Sep = $Oct = $Nov = $Dic = 0;

			foreach ($datos as $row):
				$sumaTotal = $sumaTotal + $row["totalAnual"]; 
				$Ene = $Ene + $row["Enero"];
				$Feb = $Feb + $row["Febrero"];
				$Mar = $Mar + $row["Marzo"];
				$Abr = $Abr + $row["Abril"];
				$May = $May + $row["Mayo"];
				$Jun = $Jun + $row["Junio"];
				$Jul = $Jul + $row["Julio"];
				$Ago = $Ago + $row["Agosto"];
				$Sep = $Sep + $row["Septiembre"];
				$Oct = $Oct + $row["Octubre"];
				$Nov = $Nov + $row["Noviembre"];
				$Dic = $Dic + $row["Diciembre"];
			endforeach;

			echo '
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($sumaTotal, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Ene, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Feb, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Mar, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Abr, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($May, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Jun, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Jul, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Ago, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Sep, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Oct, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Nov, 2, '.', ',').'</strong></td>
			<td style="padding-top: 14px; background-color: #EAEDED; white-space: nowrap;"><strong>$ '.number_format($Dic, 2, '.', ',').'</strong></td>
			</tr>
			<tr style="background-color: rgb(106, 115, 123); color: #FFF">
			<td align="center">Acción</td>
			<td>Responsable</td>
			<td align="center">Negocio</td>
			<td align="center">Cliente</td>
			<td align="center">Servicio</td>
			<td align="center">Equipo</td>
			<td align="center">Descripción</td>
			<td align="center">Año de<br><small>presupuesto</small></td>
			<td align="center" style="white-space: nowrap;">Total anual por<br><small>servicio</small></td>
			<td>Enero</td>
			<td>Febrero</td>
			<td>Marzo</td>
			<td>Abril</td>
			<td>Mayo</td>
			<td>Junio</td>
			<td>Julio</td>
			<td>Agosto</td>
			<td>Septiembre</td>
			<td>Octubre</td>
			<td>Noviembre</td>
			<td>Diciembre</td>
			</tr>
			</thead>
			<tbody>';
			if($datos==null){
				echo '<tr><td class="alert-danger" colspan="21" align="center"><strong> No se encontraron negocios en el presupuesto </strong></td></tr>';
			}else{
				foreach ($datos as $r) :
					echo'
					<tr>
					<td align="center" style="white-space: nowrap;">

					<div class="btn-group dropdown">
					<button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-ellipsis-h"></i>
					</button>

					<ul class="dropdown-menu" style="text-aling: rigth;text-align-last: left;">
					<li><a href="" data-toggle="modal" data-target="#mPresupuestoNegocio" onclick="FunctionEditarPN('; ?><?php echo "'". $r["idPresupuesto"] . "'"; ?>,<?php echo "'". $r["idNegocio"] . "'"; ?>,<?php echo "'". $r["tituloNegocio"] . "'"; ?>,<?php echo "'". $r["descripcion"] . "'"; ?><?php echo')"><span class="glyphicon glyphicon-pencil"></span> Editar</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="" data-toggle="modal" data-target="#mDetalles" onclick="FunctionDetalles('; ?><?php echo "'". $r["claveServicio"] . "'"; ?>,<?php echo "'". $r["periodo"] . "'"; ?>,<?php echo "'". $r["totalAnual"] . "'"; ?>,<?php echo "'". $r["idNegocio"] . "'"; ?>,<?php echo "'". $r["tituloNegocio"] . "'"; ?>,<?php echo "'". $r["Enero"] . "'"; ?>,<?php echo "'". $r["Febrero"] . "'"; ?>,<?php echo "'". $r["Marzo"] . "'"; ?>,<?php echo "'". $r["Abril"] . "'"; ?>,<?php echo "'". $r["Mayo"] . "'"; ?>,<?php echo "'". $r["Junio"] . "'"; ?>,<?php echo "'". $r["Julio"] . "'"; ?>,<?php echo "'". $r["Agosto"] . "'"; ?>,<?php echo "'". $r["Septiembre"] . "'"; ?>,<?php echo "'". $r["Octubre"] . "'"; ?>,<?php echo "'". $r["Noviembre"] . "'"; ?>,<?php echo "'". $r["Diciembre"] . "'"; ?><?php echo')"><span class="glyphicon glyphicon-list-alt"></span> Detalles</a></li>
					</ul>
					</div>

					</td>
					<td align="center" style="white-space: nowrap;">'.$r["nombreUsuario"].'</td>
					<td align="center" style="white-space: nowrap;">'.$r["tituloNegocio"].'</td>
					<td align="center" style="white-space: nowrap;">'.$r["nombreOrganizacion"].'</td>
					<td align="center" style="white-space: nowrap;">'.$r["claveServicio"].'</td>
					<td align="center" style="white-space: nowrap;">'.$r["nombreEquipo"].'</td>
					<td align="center">'.$r["descripcion"].'</td>
					<td align="center" style="white-space: nowrap;">'.$r["periodo"].'</td>
					<td align="left" style="white-space: nowrap; background-color: #EAEDED">

					<strong>$ '.number_format($r["totalAnual"], 2, '.', ',').'</strong>';

					$x = $r["totalAnual"] * 100;
					if ($maximo == 0) {
						$porcentaje = $x / 1;
					}else{
						$porcentaje = $x / $maximo;
					}

					echo'
					<div class="progress" style="height: 8px">
					<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: '.$porcentaje.'%">
					</div>
					</div>
					</td>';

					$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

					$cont = 1;
					foreach ($meses as $e ):
						if ($r[$e] > 0 ){
							echo'
							<td align="center" style="white-space: nowrap; background-color: #D5F5E3"><a class="meses" href="" id="'.$e.$r["idPresupuesto"].'" data-type="text" data-pk="'.$cont.'"><strong>$ '.number_format($r[$e], 2, '.', ',').'</strong></a></td>';
							$cont = $cont + 1;
						}else{
							echo'
							<td align="center" style="white-space: nowrap;"><a class="meses" href="" id="'.$e.$r["idPresupuesto"].'" data-type="text" data-pk="'.$cont.'">$ '.number_format($r[$e], 2, '.', ',').'</a></td>';
							$cont = $cont + 1;
						}
					endforeach;

					echo'
					</tr>';
				endforeach;
			}
			echo'
			<tfoot>
			<tr><td colspan="21" style="padding-top: 60px;"></td></tr>
			</tfoot>

			</tbody>
			</table>';
		}
		echo'
		<script>

		$(document).ready(function(){
			var idEmbudo = $("#selectIdEmbudo").val();
			var idPresupuestoGeneral = $("#selectAnio").val();
			var Mes = $("#selectMes").val();

			$.fn.editable.defaults.mode = "popup";

			$.post("index.php?c=presupuesto&a=ConsultaPresupuesto", { idEmbudo: idEmbudo, idPresupuestoGeneral: idPresupuestoGeneral, Mes: Mes }, function(respuesta) {
				var content = JSON.parse(respuesta);

				var myArray = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

				for (var i in content) {
					for (var j = 0; j < 12; j++) {
						$("#"+myArray[j]+content[i].idPresupuesto).editable({
							type: "text",
							url: "index.php?c=presupuesto&a=ActualizarMes&idPresupuesto="+content[i].idPresupuesto,
							value: $("#"+myArray[j]+content[i].idPresupuesto).val(),
							success: function() {
								consultaOnchange();
							}
							})
						}
					}
					});
					});
					</script>';
				}

				public function verificaEx()
				{
					$bTexto = $_REQUEST['bTexto'];
					$idUsuario = $_SESSION['idUsuario'];
					$Mes = $_REQUEST['Mes'];
					$idEmbudo = $_REQUEST['idEmbudo'];
					$idPresupuestoGeneral = $_REQUEST['idPresupuestoGeneral'];
					if ($bTexto == null) 
						$resultado = $this->model->listarPresupuestos($idEmbudo,$idPresupuestoGeneral,$idUsuario);
					else
						$resultado = $this->model->listarPresupuestosFiltro($idEmbudo,$idPresupuestoGeneral,$bTexto,$idUsuario);
					$datos=$this->PrepararDatos($resultado,$Mes);

					if($datos != null)
						echo "false";
					else
						echo "true"; 
				}

				public function ActualizarMes(){
					$idMes=$_REQUEST['pk'];
					$valor=$_REQUEST['value'];
					$idPresupuesto=$_REQUEST['idPresupuesto'];
					$this->model->ActualizarMes($idMes,$valor,$idPresupuesto);
				}


				public function ConsultaPresupuesto(){
					$idUsuario = $_SESSION['idUsuario'];
					$idEmbudo=$_REQUEST['idEmbudo'];
					$Mes=$_REQUEST['Mes'];
					$idPresupuestoGeneral=$_REQUEST['idPresupuestoGeneral'];
					$resultado = $this->model->listarPresupuestos($idEmbudo, $idPresupuestoGeneral,$idUsuario);
					$datos=$this->PrepararDatos($resultado, $Mes);
					echo json_encode($datos, JSON_FORCE_OBJECT);
				}

				/*Metodo para listar los embudos*/
				public function ListarEmbudos(){
					header('Content-Type: application/json');
					$datos = array();
					foreach ($this->model->ListarEmbudos() as $e):
						$row_array['idEmbudo']  = $e->idEmbudo;
						$row_array['nombre']  = $e->nombre;
						array_push($datos, $row_array);
					endforeach;   
					echo json_encode($datos, JSON_FORCE_OBJECT);
				}

				/*Metodo para listar los negocios*/
				public function ListarNegocios(){
					$idEmbudo = $_REQUEST['idEmbudo'];
					header('Content-Type: application/json');
					$datos = array();
					foreach ($this->model->ListarNegocios($idEmbudo) as $e):
						$row_array['idNegocio']  = $e->idNegocio;
						$row_array['tituloNegocio']  = $e->tituloNegocio;
						array_push($datos, $row_array);
					endforeach;   
					echo json_encode($datos, JSON_FORCE_OBJECT);
				}

				/*Metodo para listar campos del negocio seleccionado*/
				public function listarDatosNegocio(){
					$idNegocio = $_REQUEST['idNegocio'];
					header('Content-Type: application/json');
					$datos = array();
					foreach ($this->model->listarDatosNegocio($idNegocio) as $e):
						$row_array['nombreEquipo']  = $e->nombreEquipo;
						$row_array['claveServicio']  = $e->claveServicio;
						$row_array['nombreOrganizacion']  = $e->nombreOrganizacion;
						array_push($datos, $row_array);
					endforeach;   
					echo json_encode($datos, JSON_FORCE_OBJECT);
				}

				/*Metodo para listar los presupuestos y mostrar su periodo*/
				public function ListarPeriodo(){
					$idEmbudo = $_REQUEST['idEmbudo'];
					$idUsuario = $_SESSION['idUsuario'];
					header('Content-Type: application/json');
					$datos = array();
					foreach ($this->model->ListarPeriodo($idEmbudo, $idUsuario) as $e):
						$row_array['idPresupuestoGeneral']  = $e->idPresupuestoGeneral;
						$row_array['periodo']  = $e->periodo;
						array_push($datos, $row_array);
					endforeach;   
					echo json_encode($datos, JSON_FORCE_OBJECT);
				}

				public function CambiarEmbudo(){
					$idEmbudo=$_REQUEST['idEmbudo'];
					$nombre = $_REQUEST['nombre'];
					$_SESSION['idEmbudo'] = $idEmbudo;
					$_SESSION['nombre'] = $nombre;
				}

				public function ListarPresupuestos(){
					foreach ($this->model->listar($_SESSION['idUsuario'], $_SESSION['idEmbudo']) as $r):
						echo '<option class="btn-default" value="1800">'.$r->nombrePresupuesto.' / '.$r->periodo.'</option>';
					endforeach; 
				}

				public function VerificarPresupuesto()
				{
					$periodo = $_POST['periodo'];
					$idPresupuestoGeneral = $_POST['idPresupuestoGeneral'];
					$idUsuario = $_SESSION['idUsuario'];
					$verificar = $this->model->VerificarPresupuesto($periodo, $idUsuario, $idPresupuestoGeneral);
					if ($verificar == "")
						echo "0";
					else
						echo "1";
				}

				public function obtenerClave()
				{
					$idNegocio = $_GET['idNegocio'];
					$vdolar = $this->modelNegocio->ConsultaValorDolar();
					$neg = $this->model->obtenerClave($idNegocio);
				
					if ($neg->tipoMoneda == "USD")
						$neg->valorNegocio = ($neg->valorNegocio * $vdolar);
					
					if (!empty($neg->claveOrganizacion))
						$estimaconesJSON = $this->consultarEstimaciones($neg->claveOrganizacion, $neg->claveConsecutivo, $neg->claveServicio, $neg->fechaCierre, $neg->valorNegocio, $vdolar);
					else
						$estimaconesJSON = "null";
					
					if ($estimaconesJSON != "null"){
						echo $estimaconesJSON;
					}else{
						header('Content-Type: application/json');
						$negocioJSON = array();
						$r_array['fechaCierre'] = $neg->fechaCierre;
						$r_array['valorNegocio'] = $neg->valorNegocio;
						array_push($negocioJSON, $r_array);
						echo json_encode($negocioJSON, JSON_FORCE_OBJECT);
					}
				}

				public function consultarEstimaciones($claveOrganizacion, $claveConsecutivo, $claveServicio, $fechaCierre, $valorNegocio, $vdolar)
				{
					$estimaciones = $this->model->obtenerEstimaciones($claveOrganizacion, $claveConsecutivo, $claveServicio);
					if (!empty($estimaciones)) {
						header('Content-Type: application/json');
						$datos = array();
						$r_array['fechaCierre'] = $fechaCierre;
						$r_array['valorNegocio'] = $valorNegocio;
						array_push($datos, $r_array);
						foreach ($estimaciones as $row):
							$row_array['fechaCierre']  = $row->fechaCierre;
							if ($row->tipoMoneda == "USD") {
								$vpesos = ($row->valorNegocio * $vdolar);
								$row_array['valorNegocio']  = $vpesos;
							}else{
								$row_array['valorNegocio']  = $row->valorNegocio;
							}
							array_push($datos, $row_array);
						endforeach;   
						echo json_encode($datos, JSON_FORCE_OBJECT);
					}else{
						return "null";
					}
					
				}

				public function Exportar(){
					$idPresupuestoGeneral=$_POST['idPG'];
					$bTexto=$_POST['buscar'];
					$idUsuario = $_SESSION['idUsuario'];
					$idEmbudo=$_REQUEST['idEmbudo'];
					$Mes = $_REQUEST['idMes'];
					echo $Mes;

					if ($idPresupuestoGeneral=="") {
						echo "Seleccione un presupuesto";
					}

					if($bTexto==""){
						$resultado = $this->model->listarPresupuestos($idEmbudo, $idPresupuestoGeneral, $idUsuario);		
					}
					else{
						$resultado=$this->model->listarPresupuestosFiltro($idEmbudo, $idPresupuestoGeneral, $bTexto, $idUsuario);
					}

					$datos=$this->PrepararDatos($resultado,$Mes);

					require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';

                                 //Logotipo
					$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

                                //Objeto de PHPExcel
					$objPHPExcel  = new PHPExcel();

                                //Propiedades de Documento
					$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Presupuesto");

                                //Establecemos la pestaña activa y nombre a la pestaña
					$objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->setTitle("Presupuesto");
					$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
					$objDrawing->setName('Logotipo');
					$objDrawing->setDescription('Logotipo');
					$objDrawing->setImageResource($gdImage);
					$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
					$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
					$objDrawing->setHeight(110);
					$objDrawing->setCoordinates('Q1');
					$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

					$estilo = array(
						'font' => array(
							'name'      => 'Calibri',
							'bold'      => true,
							'italic'    => false,
							'strike'    => false,
							'size' =>11
						),
						'fill' => array(
							'type'  => PHPExcel_Style_Fill::FILL_SOLID
						),
						'borders' => array(
							'allborders' => array(
							)
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
						)
					);

					$estiloTituloReporte = array(
						'font' => array(
							'name'      => 'Calibri',
							'bold'      => true,
							'italic'    => false,
							'strike'    => false,
							'size' =>30,
							'color' => array(
								'rgb' => 'E31B23'
							)
						),
						'fill' => array(
							'type'  => PHPExcel_Style_Fill::FILL_SOLID
						),
						'borders' => array(
							'allborders' => array(
							)
						),
						'alignment' => array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
						)
					);

					$estiloTituloColumnas = array(
						'font' => array(
							'name'  => 'Calibri',
							'bold'  => false,
							'size' =>12,
							'color' => array(
								'rgb' => 'FFFFFF'
							)
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'E31B23')
						),
						'borders' => array(
							'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						),
						'alignment' =>  array(
							'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
						)
					);
					$estiloTituloColumnasTotales = array(
						'font' => array(
							'name'  => 'Calibri',
							'bold'  => false,
							'size' =>12,
							'color' => array(
								'rgb' => '17202A'
							)
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'CCD1D1')
						),
						'borders' => array(
							'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						),
						'alignment' =>  array(
							'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
						)
					);
					$estiloTituloColumnasTotal = array(
						'font' => array(
							'name'  => 'Calibri',
							'bold'  => true,
							'size' =>12,
							'color' => array(
								'rgb' => '17202A'
							)
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => 'CCD1D1')
						),
						'borders' => array(
							'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						),
						'alignment' =>  array(
							'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
						)
					);
					$estiloTituloColumnasValor = array(
						'font' => array(
							'name'  => 'Calibri',
							'bold'  => false,
							'size' =>12,
							'color' => array(
								'rgb' => '17202A'
							)
						),
						'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb' => '04B404')
						),
						'borders' => array(
							'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
							)
						)
					);


					$estiloInformacion = new PHPExcel_Style();
					$estiloInformacion->applyFromArray( array(
						'font' => array(
							'name'  => 'Calibri',
							'size' =>12,
							'color' => array(
								'rgb' => '000000'
							)
						),
						'fill' => array(
							'type'  => PHPExcel_Style_Fill::FILL_SOLID
						),
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Borders::DIAGONAL_DOWN
							)
						),
						'alignment' =>  array(
							'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
						)
					));
					date_default_timezone_set("America/Mexico_City");
					$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

					$sumaTotal = $Ene = $Feb = $Mar = $Abr = $May = $Jun = $Jul = $Ago = $Sep = $Oct = $Nov = $Dic = 0;

					foreach ($datos as $row):
						$sumaTotal = $sumaTotal + $row["totalAnual"]; 
						$Ene = $Ene + $row["Enero"];
						$Feb = $Feb + $row["Febrero"];
						$Mar = $Mar + $row["Marzo"];
						$Abr = $Abr + $row["Abril"];
						$May = $May + $row["Mayo"];
						$Jun = $Jun + $row["Junio"];
						$Jul = $Jul + $row["Julio"];
						$Ago = $Ago + $row["Agosto"];
						$Sep = $Sep + $row["Septiembre"];
						$Oct = $Oct + $row["Octubre"];
						$Nov = $Nov + $row["Noviembre"];
						$Dic = $Dic + $row["Diciembre"];
					endforeach;

					$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
					$objPHPExcel->getActiveSheet()->getStyle('M3')->applyFromArray($estilo);
					$objPHPExcel->getActiveSheet()->getStyle('A6:R6')->applyFromArray($estiloTituloReporte);
					$objPHPExcel->getActiveSheet()->getStyle('A6:R6')->applyFromArray($estiloTituloColumnas);
					$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($estiloTituloColumnasTotal);
					$objPHPExcel->getActiveSheet()->getStyle('F5:R5')->applyFromArray($estiloTituloColumnasTotales);
					$objPHPExcel->getActiveSheet()->getStyle('F5:R5')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

					$objPHPExcel->getActiveSheet()->setCellValue('M3', 'FECHA: '.$A);
					$objPHPExcel->getActiveSheet()->setCellValue('B3', 'PRESUPUESTO');
					$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
					$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Responsable');
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
					$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Cliente');
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
					$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Servicio');
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
					$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Equipo');
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
					$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Descripción');
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
					$objPHPExcel->getActiveSheet()->setCellValue('E5', 'TOTALES');
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
					$objPHPExcel->getActiveSheet()->setCellValue('F5', $sumaTotal);
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
					$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Total anual por servicio');
					$objPHPExcel->getActiveSheet()->setCellValue('G5', $Ene);
					$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Enero');
					$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('H5', $Feb);
					$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Febrero'); 
					$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('I5', $Mar);
					$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Marzo');
					$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('J5', $Abr);
					$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Abril');
					$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('K5', $May);
					$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Mayo'); 
					$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('L5', $Jun);
					$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Junio');
					$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('M5', $Jul);
					$objPHPExcel->getActiveSheet()->setCellValue('M6', 'Julio');
					$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('N5', $Ago);
					$objPHPExcel->getActiveSheet()->setCellValue('N6', 'Agosto'); 
					$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('O5', $Sep);
					$objPHPExcel->getActiveSheet()->setCellValue('O6', 'Septiembre');
					$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('P5', $Oct);
					$objPHPExcel->getActiveSheet()->setCellValue('P6', 'Octubre');
					$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('Q5', $Nov);
					$objPHPExcel->getActiveSheet()->setCellValue('Q6', 'Noviembre'); 
					$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(13);
					$objPHPExcel->getActiveSheet()->setCellValue('R5', $Dic);
					$objPHPExcel->getActiveSheet()->setCellValue('R6', 'Diciembre');
            //Establecemos en que fila inciara a imprimir los datos
					$fila = 7; 

            //Recorremos los resultados de la consulta y los imprimimos
					foreach ($datos as $r) :
						$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r["nombreUsuario"]);
						$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r["nombreOrganizacion"]);
						$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r["claveServicio"]);
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r["nombreEquipo"]);
						$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $r["descripcion"]);
						$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $r["totalAnual"]);
						$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('M'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('P'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('Q'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->getStyle('R'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


						$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $r["Enero"]);
						if($r['Enero']>0)
							$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $r["Febrero"]);
						if($r['Febrero']>0)
							$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $r["Marzo"]);
						if($r['Marzo']>0)
							$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $r["Abril"]);
						if($r['Abril']>0)
							$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $r["Mayo"]);
						if($r['Mayo']>0)
							$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $r["Junio"]);
						if($r['Junio']>0)
							$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $r["Julio"]);
						if($r['Julio']>0)
							$objPHPExcel->getActiveSheet()->getStyle('M'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $r["Agosto"]);
						if($r['Agosto']>0)
							$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('O'.$fila, $r["Septiembre"]);
						if($r['Septiembre']>0)
							$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('P'.$fila, $r["Octubre"]);
						if($r['Octubre']>0)
							$objPHPExcel->getActiveSheet()->getStyle('P'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('Q'.$fila, $r["Noviembre"]);
						if($r['Noviembre']>0)
							$objPHPExcel->getActiveSheet()->getStyle('Q'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$objPHPExcel->getActiveSheet()->setCellValue('R'.$fila, $r["Diciembre"]);
						if($r['Diciembre']>0)
							$objPHPExcel->getActiveSheet()->getStyle('R'.$fila)->applyFromArray($estiloTituloColumnasValor);
						$fila++; 
					endforeach;

					header("Content-Type: application/vnd.ms-excel");
					header('Content-Disposition: attachment;filename="Presupuesto.csv"');
					header('Cache-Control: max-age=0');

					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					ob_end_clean();
					$objWriter->save('php://output');    

				}
			}
			?>