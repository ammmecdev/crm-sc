<?php 
require_once 'model/embudo.php';

class ConfiguracionController{
	private $modelEmbudos;
	private $mensajeI;
	private $errorI;
	private $warningI;
	private $url;

	public function __Construct(){
		$this->modelEmbudos = new Embudo();
	}

	public function Index(){
		$this->url="?c=configuracion";
		$page = "view/configuracion/menu.php";
		$contenedor = "view/configuracion/ajustes.php";
		require_once '../../view/index.php';
	}

		
	public function Guardar(){
		if (!isset($_POST['idEmbudo']))
			header('Location: ./?c=configuracion');		
		if(!empty($_FILES['logo']['name'])){
			$idEmbudo = $_POST['idEmbudo'];
			$logo = $_FILES['logo']['name'];
			$destino = "./assets/imagenes/".$logo;
			if(copy($_FILES['logo']['tmp_name'], $destino)){
				$this->modelEmbudos->CambiaLogo($idEmbudo, $logo);
				$_SESSION['logo']=$logo;
				$this->Index();
			}
		}else{
			$this->warningI=true;
			$this->mensajeI="No hay cambios por hacer";
			$this->Index();
		}
	}


	public function CambiaVariables(){
		$_SESSION['idEmbudo'] = $_POST['idEmbudo'];
		$_SESSION['nombre'] = $_POST['nombre'];
		$embudo=$this->modelEmbudos->ObtenerLogo($_SESSION['idEmbudo']);
		$_SESSION['logo']=$embudo->logo;
	}
}
?>