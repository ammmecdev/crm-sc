<?php
require_once 'model/seguimiento.php';
require_once 'model/cotizacion.php';
require_once 'model/negocio.php';


class SeguimientoController{

	public function Index(){
		$seguimiento=true;
		$this->url="?c=seguimiento";
		//$page = "view/page_not/page.php";
		$page="view/procesos/seguimiento.php";
		require_once '../../view/index.php';
	}
	public function __CONSTRUCT()
	{
		try{
			$this->model = new Seguimiento();
			$this->modelNegocios = new Negocio();
			$this->modelCotizacion = new Negocio();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function Exportar(){

		$resultado = $this->modelNegocios->ListarSeg();
		//$resultado = $this->modelCotizacion->Listar();

		$resultado=$resultado->fetchAll(PDO::FETCH_OBJ);
		//$cotizacion=$cotizacion->fetch(PDO::FETCH_OBJ);

		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
                                 //Logotipo
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');
                              //Objeto de PHPExcel
		$objPHPExcel  = new PHPExcel();
                                //Propiedades de Documento
		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Seguimiento");
                                //Establecemos la pestaña activa y nombre a la pestaña
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Seguimiento");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(110);
		$objDrawing->setCoordinates('Y1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>11
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>30,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
				//'style' => PHPExcel_Style_Borders::DIAGONAL_DOWN
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		$estiloTituloColumnasTotales = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => '17202A'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'CCD1D1')
			),
			'borders' => array(
				'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		$estiloTituloColumnasTotal = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => true,
				'size' =>12,
				'color' => array(
					'rgb' => '17202A'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'CCD1D1')
			),
			'borders' => array(
				'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		$estiloTituloColumnasValor = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => '17202A'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '04B404')
			),
			'borders' => array(
				'allborders' => array(
				//'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Borders::DIAGONAL_DOWN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s");

		$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('T3')->applyFromArray($estilo);
		//$objPHPExcel->getActiveSheet()->getStyle('A6:Y6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:Y6')->applyFromArray($estiloTituloColumnas);
		//$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($estiloTituloColumnasTotal);
		//$objPHPExcel->getActiveSheet()->getStyle('F5:R5')->applyFromArray($estiloTituloColumnasTotales);
		$objPHPExcel->getActiveSheet()->getStyle('B6')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

		$objPHPExcel->getActiveSheet()->setCellValue('T3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'SEGUIMIENTO');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'CLIENTE');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'CONSECUTIVO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'SERVICIO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('D6', 'COTIZACIÓN');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
		$objPHPExcel->getActiveSheet()->setCellValue('E6', 'DESCRIPCIÓN');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('F6', 'EQUIPO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('G6', 'IMPORTE SIN IVA');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('H6', 'IMPORTE USD');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('I6', 'PRECIO NETO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('J6', 'OPORTUNIDAD');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('K6', 'RFQ');
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('L6', 'FECHA DE RFQ');
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('M6', 'USUARIO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('N6', 'FECHA COTIZACIÓN');
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('O6', 'PEDIDO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('P6', 'FECHA DE PEDIDO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('Q6', 'FECHA REALIZACIÓN');
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('R6', 'FECHA DE REPORTE');
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('S6', 'FACTURA');
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('T6', 'FECHA DE FACTURA');
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('U6', 'FECHA DE PAGO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('V6', 'REALIZACIÓN');
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('W6', 'FACTURACIÓN');
		$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(25);
		$objPHPExcel->getActiveSheet()->setCellValue('X6', 'RECIBO ELEC DE PAGO');
		$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
		$objPHPExcel->getActiveSheet()->setCellValue('Y6', 'FECHA DE RECIBO');

		$fila = 7;
            //Recorremos los resultados de la consulta y los imprimimos
		foreach ($resultado as $r) :
		//$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r["nombreOrganizacion"]);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->nombreOrganizacion);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->claveConsecutivo);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $r->claveOrganizacion);
			//$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $r->claveCotizacion);
			//$objPHPExcel->getActiveSheet()->setCellValue('N'.$fila, $r->fechaCotizacion);

			$fila++;
		endforeach;
		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Seguimiento.csv"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');
	}
}
?>
