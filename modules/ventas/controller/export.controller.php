<?php
class export{
public function Exportar()
	{
		require_once('../../assets/plugins/TCPDF/tcpdf.php');
		require_once('../../assets/plugins/TCPDF/config/lang/eng.php');


		$idCostoOperativo = $_REQUEST['idCostoOperativo'];
		echo ($idCostoOperativo);
		if ($idCostoOperativo=="") {
			echo "Seleccione un costo operativo";
		}else{
			$resultado = $this->model->ObtenerUsuario();
			$clave= $this->model->ListarDatosA($idCostoOperativo);
			$servicios = $this->model->consultaServicio2($idCostoOperativo);

			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'Legal', true, 'UTF-8', false);

			$pdf->setPageMark();
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Julieta');
			$pdf->SetTitle('Solicitud de costo operativo');


			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 007', PDF_HEADER_STRING);
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			//$pdf->SetHeaderData("../../assets/imagenes/dingo.png", 50, "EJEMPLO IMG GOTA DE AGUA");
			$pdf->setHeaderFont(Array('helveticaB', '', PDF_FONT_SIZE_MAIN)); 
			$pdf->setFooterFont(Array('helveticaB', '', PDF_FONT_SIZE_DATA)); 
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT, PDF_MARGIN_BOTTOM); 
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


			$pdf->setPrintHeader(false); 
			$pdf->setPrintFooter(false);
			$pdf->SetMargins(20, 20, 20, false); 
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			$pdf->SetAutoPageBreak(true, 20); 
			$pdf->SetFont('Helvetica', '', 10);
			$pdf->AddPage();

////////////////////////////prueba de imagen ////////////////////////
			//$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
			//$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
			//$pdf->SetAutoPageBreak(false, 0);
// set bacground image
			//$img_file = K_PATH_IMAGES.'../../assets/imagenes/dingo.png';
			//$pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
			//$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
			//$pdf->setPageMark();

			////////////////////////////////////////////

			$content = '';
			$content = '<style>
body{
        background-image: url("files/julieta.jpg");
} 
 
</style>';
			$content = '<p>
			<h1 style="text-align:center;">SOLICITUD DE COSTO OPERATIVO</h1>	</p>
			<br>';
			$content .= '
			<br>
			<br>
			<table cellpadding="5" cellspacing="3">
			<thead>

			<tr align="center">';
			foreach ($clave as $r) {
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td border="1">FECHA DE EMISION<br><br>'.$r->fechaEmision.'<br></td>
					<td border="1">FECHA DE ENTREGA <br><br>'.$r->fechaEntrega.'<br></td>
					<td border="1">REQUISICION NO.<br><br>'.$r->requisicion.'<br></td>
					<td border="1">CUENTA<br><br>'. $r->claveOrganizacion .'-'. $r->claveConsecutivo . '-' . $r->claveServicio . '<br></td>
					';
				}
			}
			$content .= ' </tr> </thead>';
			$content .= '</table>';
			$content .= '

			<table cellpadding="5" cellspacing="3">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) {
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td border="1" width="25%">CONTACTO<br><br>'.$r->nombreOrganizacion.'<br></td>
					<td border="1" width="75%">RESPONSABLE DE LA CUENTA<br><br>'.$resultado->nombreUsuario.'<br></td>
					';
				}
			}
			$content .= ' </tr> </thead>';
			$content .= '</table>';
			$content .= '
			<table cellpadding="5" cellspacing="3">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) {
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td border="1" width="100%">PROYECTO<br><br>'.$r->tituloNegocio.'<br></td>
					';
				}
			}
			$content .= ' </tr> </thead>';
			$content .= '</table>';

			$content .= '
			<br>
			<br>
			<br>
			<table border="1" align="center">
			<thead>
			<tr align="center">';

			$content .= '	
			<th width="10%" style="background-color:CCD1D1;"><br><br>Clave<br></th>
			<th width="35%" style="background-color:CCD1D1;"><br><br>Descripcion<br></th>
			<th width="10%" style="background-color:CCD1D1;"><br><br>Unidad<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Cantidad<br></th>
			<th width="19%" style="background-color:CCD1D1;"><br><br>Precio Unitario<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Importe<br></th>';
			$content .= ' </tr> </thead>';
			$content .= '</table>';
			$content .= '
			<table border="1" align="center">
			<thead>
			';
			foreach ($servicios as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<tr align="center">
					<td width="10%">'.$r->clave.'</td>
					<td width="35%">'.$r->descripcion.'</td>
					<td width="10%">'.$r->unidad.'</td>
					<td width="13%">'.$r->cantidad.'</td>
					<td width="19%">'.$r->precioUnitario.'</td>
					<td width="13%">'.$r->importe.'</td></tr>';

				}
			endforeach;
			$content .= '  
			<tr align="center">';
			$content .= '	

			<td width="10%"></td>
			<td width="35%"></td>
			<td width="10%"></td>
			<td width="13%"></td>
			<td width="19%"></td>
			<td width="13%"></td>';
			$content .= ' </tr> 

			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	

					<td width="10%"></td>
					<td width="35%"></td>
					<td width="10%"></td>
					<td width="13%"></td>
					<td width="19%" style="background-color:CCD1D1;">Costo Operativo</td>
					<td width="13%">'.$r->costoOperativo.'</td>';
				}
			endforeach;
			$content .= ' </tr> 
			<tr align="center">';
			$content .= '	

			<td width="10%"></td>
			<td width="35%"></td>
			<td width="10%"></td>
			<td width="13%"></td>
			<td width="19%"></td>
			<td width="13%"></td>';
			$content .= ' </tr> 
			<tr align="center">';
			$content .= '	

			<td width="10%"></td>
			<td width="35%"></td>
			<td width="55%" style="background-color:CCD1D1;">Notas Aclaratorias</td>';
			$content .= ' </tr> 
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	

					<td width="10%"></td>
					<td width="35%"></td>
					<td width="23%">Tiempo de Entrega</td>
					<td width="32%">'.$r->tiempoEntrega.'</td>';
				}
			endforeach;
			$content .= ' </tr> 
			</thead>';
			$content .= '</table>';
			$content .= '<br>';
			$content .= '		
			<table border="1" align="center">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<th width="20%"><br>Condiciones de pago:</th>
					<th width="80%"><br>'.$r->condicionesPago.'<br></th>';
				}
			endforeach;
			$content .= ' </tr>
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<th width="20%"><br>Observaciones:</th>
					<th width="80%"><br>'.$r->observaciones.'</th>';
				}
			endforeach;
			$content .= ' </tr></thead>';
			$content .= '</table>';
			$content .= '<br><br><br><br>';
			$content .= '<p>Quedo a sus ordenes para cualquier pregunta o comentario.</p>';
			

													$pdf->writeHTML($content, true, 0, true, 0); //$pdf->lastPage();
													ob_end_clean();
													$pdf->output('Solicitud de Costo Operativo.pdf', 'I');
												}
											}
										}
											?>