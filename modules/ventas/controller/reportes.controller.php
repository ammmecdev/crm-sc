<?php
require_once 'model/costo.php';
require_once 'model/cotizacion.php';
require_once 'model/orden.php';
require_once 'model/evaluacion.php';
//require_once('../../assets/TCPDF/examples/tcpdf_include.php');
require('../../assets/TCPDF/tcpdf.php');

class ReportesController extends TCPDF{
	public $idCostoOperativo;


	public function Index(){
		$this->url="?c=reportes";
	}

	public function Header() {
        // get the current page break margin
		$bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
        // set bacground image
		$img_file = K_PATH_IMAGES.'encabezado.jpg';
		$this->Image($img_file, 0, 0, 212, 70, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
//$this->SetFont('helvetica', 'B', 20);
        // Title
       // $this->Cell(0, 49, '<< REPORTE PRUEBA >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        // set the starting point for the page content
		$this->setPageMark();
	}
	public function Footer() {
        // Position at 15 mm from bottom
		$this->SetY(-5);
		$img_file = K_PATH_IMAGES.'footer.jpg';
		$this->Image($img_file, -4, 250, 213, 46, '', '', '', false, 200, '', false, false, 0);
        // Set font
   // $this->SetFont('helvetica', 'I', 8);
        // Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
	
	public function ExportarCosto (){
		$content = "";
		date_default_timezone_set("America/Mexico_City");
		// create new PDF document
		$pdf = new ReportesController(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Ammmec');
		$pdf->SetTitle('Costo Operativo');
// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// set font
		$pdf->SetFont('Helvetica', '', 11);
// add a page
		$pdf->AddPage();
		$idCostoOperativo = $_GET['idCostoOperativo'];
		if ($idCostoOperativo=="") {
			echo "Seleccione un costo operativo";
		}else{
			$model = new Costo();
			$resultado = $model->ObtenerUsuario();
			$clave= $model->ListarDatosA($idCostoOperativo);
			$servicios = $model->consultaServicio2($idCostoOperativo);
			$content .= '
			<p><h1 style="text-align:center; padding-top: 50px;">SOLICITUD DE COSTO OPERATIVO</h1></p>
			<br>
			<table cellpadding="5" cellspacing="4">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) {
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td border="1">FECHA DE EMISIÓN<br><br>'.$r->fechaEmision.'<br></td>
					<td border="1">FECHA DE ENTREGA <br><br>'.$r->fechaEntrega.'<br></td>
					<td border="1">REQUISICIÓN NO.<br><br>'.$r->requisicion.'<br></td>
					<td border="1">CUENTA<br><br>'. $r->claveOrganizacion .'-'. $r->claveConsecutivo . '-' . $r->claveServicio . '<br></td>
					';
				}
			}
			$content .= '
			</tr>
			</thead>';
			$content .= '</table>';
			$content .= '
			<table cellpadding="5" cellspacing="3">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) {
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td border="1" width="25%">CONTACTO<br><br>'.$r->nombreOrganizacion.'<br></td>
					<td border="1" width="75%">RESPONSABLE DE LA CUENTA<br><br>'.$resultado->nombreUsuario.'<br></td>';
				}
			}
			$content .= '
			</tr> 
			</thead>';
			$content .= '</table>';
			$content .= '
			<table cellpadding="5" cellspacing="2">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) {
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td border="1" width="100%">PROYECTO<br><br>'.$r->tituloNegocio.'<br></td>
					';
				}
			}
			$content .= '
			</tr> 
			</thead>';
			$content .= '</table>';
			$content .= '
			<br>
			<br>
			<br>
			<table border="1" align="center" cellpadding="3">
			<thead>
			<tr align="center">';
			$content .= '	
			<th width="10%" style="background-color:CCD1D1;"><br><br>Clave<br></th>
			<th width="35%" style="background-color:CCD1D1;"><br><br>Descripción<br></th>
			<th width="10%" style="background-color:CCD1D1;"><br><br>Unidad<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Cantidad<br></th>
			<th width="19%" style="background-color:CCD1D1;"><br><br>Precio Unitario<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Importe<br></th>';
			$content .= ' 
			</tr>
			</thead>';
			$content .= '</table>';
			$content .= '
			<table border="1" align="center" cellpadding="3">
			<thead>
			';
			foreach ($servicios as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<tr align="center">
					<td width="10%">'.$r->clave.'</td>
					<td width="35%">'.$r->descripcion.'</td>
					<td width="10%">'.$r->unidad.'</td>
					<td width="13%">'.$r->cantidad.'</td>
					<td width="19%">'.$r->precioUnitario.'</td>
					<td width="13%">'.$r->importe.'</td>
					</tr>';
				}
			endforeach;
			$content .= '  
			<tr align="center">';
			$content .= '	
			<td width="10%"></td>
			<td width="35%"></td>
			<td width="10%"></td>
			<td width="13%"></td>
			<td width="19%"></td>
			<td width="13%"></td>';
			$content .= ' 
			</tr> 
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td width="10%"></td>
					<td width="35%"></td>
					<td width="10%"></td>
					<td width="13%"></td>
					<td width="19%" style="background-color:CCD1D1;">Costo Operativo</td>
					<td width="13%">'.$r->costoOperativo.'</td>';
				}
			endforeach;
			$content .= '
			</tr> 
			<tr align="center">';
			$content .= '	
			<td width="10%"></td>
			<td width="35%"></td>
			<td width="10%"></td>
			<td width="13%"></td>
			<td width="19%"></td>
			<td width="13%"></td>';
			$content .= ' 
			</tr> 
			<tr align="center">';
			$content .= '	
			<td width="10%"></td>
			<td width="35%"></td>
			<td width="55%" style="background-color:CCD1D1;">Notas Aclaratorias</td>';
			$content .= ' 
			</tr> 
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td width="10%"></td>
					<td width="35%"></td>
					<td width="23%">Tiempo de Entrega</td>
					<td width="32%">'.$r->tiempoEntrega.'</td>';
				}
			endforeach;
			$content .= '
			</tr> 
			</thead>';
			$content .= '</table>';
			$content .= '<br><br>';
			$content .= '		
			<table border="1" align="center" cellpadding="3">
			<thead>
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<th width="20%"><br>Condiciones de pago:</th>
					<th width="80%"><br>'.$r->condicionesPago.'<br></th>';
				}
			endforeach;
			$content .= '
			</tr>
			<tr align="center">';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<th width="20%"><br>Observaciones:</th>
					<th width="80%"><br>'.$r->observaciones.'</th>';
				}
			endforeach;
			$content .= '
			</tr>
			</thead>';
			$content .= '</table>';
			$content .= '<br>';
			$content .= '<p>Quedo a sus ordenes para cualquier pregunta o comentario.</p>';
			$content .= '
			<table align="center" cellpadding="3">
			<thead>';
			$content .='	
			<tr align="center">
			<td width="15%"><br></td>
			<td width="85%"></td>
			</tr>
			<tr align="center">
			<td width="15%"><br></td>
			<td width="40%"></td>
			<td width="15%"><br></td>
			</tr>';
			$content .= '</thead>';
			$content .= '</table>';
			//$pdf->writeHTML($content, true, 0, true, 0); 
			//$pdf->lastPage();
			$pdf->writeHTML($content, true, false, true, false, '');
			ob_end_clean();
			$pdf->output('Solicitud de Costo Operativo', 'I');
		}
	}
	public function ExportarCotizacion (){
		echo $_GET['idCotizacion'];
		$idCotizacion = $_GET['idCotizacion'];
		$idCostoOperativo = $_GET['idCostoOperativo'];
		//$idCotizacion=1;
		//$idCostoOperativo=1;
		if ($idCostoOperativo == "" && $idCotizacion == "") {
			echo "Seleccione una cotización";
		}else{
			$this->modelCO = new Costo();
			$this->model = new Cotizacion();
			$costo = $this->modelCO->ListarDatosCosto($idCostoOperativo);
			$servicios = $this->modelCO->consultaServicio2($idCostoOperativo);
			$clave= $this->modelCO->ListarDatosA($idCostoOperativo);
			$cotizacion = $this->model->ObtenerDatosCotizacion($idCotizacion);

		// create new PDF document
			date_default_timezone_set("America/Mexico_City");
			$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 
			$pdf = new ReportesController(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Ammmec');
			$pdf->SetTitle('Cotizacion');
// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// set font
			$pdf->SetFont('Helvetica', '', 11);
// add a page
			$pdf->AddPage();
			$content .= '
			<p><h1 style="text-align:center;">COTIZACIÓN</h1></p>
			<br>';
			$content .= '
			<br>
			<br>
			<table border="1" align="center" cellpadding="5">
			<thead>	
			<tr align="center">';
			foreach ($costo as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '	
					<td width="15%"><br>Proyecto:</td>

					<td width="85%">'.$r->tituloNegocio.'</td>
					</tr>
					<tr align="center">
					<td width="15%"><br>Cliente:</td>
					<td width="40%">'.$r->nombreOrganizacion.'</td>
					<td width="15%"><br>Requisicion:</td>
					<td width="30%">'.$r->requisicion.'</td>
					</tr>
					<tr align="center">
					<td width="15%"><br>Direccion:</td>
					<td width="40%">'.$r->direccion.'</td>
					<td width="15%"><br>Cuenta:</td>
					<td width="30%">'. $r->claveOrganizacion .'-'. $r->claveConsecutivo . '-' . $r->claveServicio . '</td>
					</tr>
					<tr align="center">
					<td width="15%"><br>Localidad:</td>
					<td width="40%">'.$r->localidadEstado.'</td>
					<td width="15%"><br>Fecha:</td>
					<td width="30%">'.$A.'</td>
					</tr>
					<tr align="center">
					<td width="15%"><br>Atension:</td>
					<td width="85%">'.$r->nombreUsuario.'</td>';
				}
			endforeach;
			$content .= ' </tr></thead>';
			$content .= '</table>';
			$content .= '<br><p>Por medio del presente documento, se muestra la siguiente propuesta;</p><br>';
			$content .= '<br><br>';
			$content .= '
			<br>
			<br>
			<br>
			<br>
			<table border="1" align="center" cellpadding="3">
			<thead>';
			$content .= '
			<tr align="center">	
			<th width="100%" style="background-color:CCD1D1;"><br>Productos y/o Servicios</th>
			</tr>
			<tr align="center">
			<th width="10%" style="background-color:CCD1D1;"><br><br>Clave<br></th>
			<th width="32%" style="background-color:CCD1D1;"><br><br>Descripcion<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Unidad<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Cantidad<br></th>
			<th width="19%" style="background-color:CCD1D1;"><br><br>Precio Unitario<br></th>
			<th width="13%" style="background-color:CCD1D1;"><br><br>Importe<br></th>
			</tr>';
			foreach ($servicios as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<tr align="center">	
					<td width="10%">'.$r->clave.'</td>
					<td width="32%">'.$r->descripcion.'</td>
					<td width="13%">'.$r->unidad.'</td>
					<td width="13%">'.$r->cantidad.'</td>
					<td width="19%">$'.$r->precioUnitario.'</td>
					<td width="13%">$'.$r->importe.'</td>
					</tr>';
				}
			endforeach;
			$content .= '
			<tr align="center">';
			$content .= '	
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="19%" style="background-color:CCD1D1;">Subtotal M.N.</td>
			<td width="13%">$'.$cotizacion->subtotal.'</td>
			</tr>';
			$content .= '
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="19%" style="background-color:CCD1D1;">IVA M.N.</td>
			<td width="13%">$'.number_format(($cotizacion->iva/100)*$cotizacion->subtotal,2,".",",").'</td>
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="19%" style="background-color:CCD1D1;">Total M.N.</td>
			<td width="13%">$'.$cotizacion->total.'</td>
			</tr>';
			$content .= '
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="19%"></td>
			<td width="13%"></td>
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="58%" style="background-color:CCD1D1;">Notas Aclaratorias</td>
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<td width="20%">Tiempo de Entrega:</td>
					<td width="38%">'.$r->tiempoEntrega.'</td>';
				}
			endforeach;
			$content .= '
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<td width="20%">Condiciones de Pago:</td>
					<td width="38%">'.$r->condicionesPago.'</td>';
				}
			endforeach;
			$content .= '
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>';
			foreach ($clave as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<td width="20%">Observaciones:</td>
					<td width="38%">'.$r->observaciones.'</td>';
				}
			endforeach;
			$content .= '
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="20%"></td>
			<td width="38%"></td>
			</tr>
			<tr align="center">
			<td width="10%"></td>
			<td width="32%"></td>
			<td width="20%">Vigencia:</td>
			<td width="38%"></td>
			</tr>';
			$content .= '</thead>';
			$content .= '</table>';					
			$content .= '<p>Quedo a sus ordenes para cualquier pregunta o comentario.</p>';
			$content .= '<br><br>';
			$content .= '<p>Atentamente.</p>';
			$content .= '
			<table align="center" cellpadding="5">
			<thead>';
			$content .= '	
			<tr align="center">
			<td width="15%"><br></td>
			<td width="85%"></td>
			</tr>';
			$content .= '</thead>';
			$content .= '</table>';
			//$pdf->writeHTML($content, true, 0, true, 0); 
			//$pdf->lastPage();
			$pdf->writeHTML($content, true, false, true, false, '');
			ob_end_clean();
			$pdf->output('Cotización', 'I');
		}
	}
	public function ExportarOrden (){
		echo $_GET['idCostoOperativo'];
		$idCotizacion=$_GET['idCotizacion'];
		$idCostoOperativo=$_GET['idCostoOperativo'];
		$idOrdenTrabajo=$_GET['idOrdenTrabajo'];
		if ($idCostoOperativo == "" && $idOrdenTrabajo == "") {
			echo "Seleccione una Orden de trabajo";
		}else{
			$this->modelOrden = new Orden();
			$this->modelC = new Costo();
			$CO = $this->modelC->ListarDatosCosto($idCostoOperativo);
			$orden = $this->modelOrden->ListarDatosOrden($idOrdenTrabajo);
			date_default_timezone_set("America/Mexico_City");
			$A= date("Y")."-".date("m")."-".date("d"); 
			$pdf = new ReportesController(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Ammmec');
			$pdf->SetTitle('Orden de Trabajo');
// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// set font
			$pdf->SetFont('Helvetica', '', 11);
// add a page
			$pdf->AddPage();
			$content .= '
			<p><h1 style="text-align:center;">ORDEN DE TRABAJO</h1></p>
			<br>';
			$content .= '
			<br>
			<br>
			<table border="0" align="center" cellpadding="5">
			<thead>	
			<tr align="left">';
			foreach ($CO as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<td width="15%"><br><b>Cuenta:</b></td>
					<td width="55%">'. $r->claveOrganizacion .'-'. $r->claveConsecutivo . '-' . $r->claveServicio . '</td>';
				}
			endforeach;
			$content .= '
			<td width="15%"><br><b>Fecha:</b></td>
			<td width="15%">'.$A.'</td>
			</tr>
			<tr align="left">';
			foreach ($CO as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<td width="15%"><br><b>Proyecto:</b><br></td>
					<td width="85%">'.$r->tituloNegocio.'</td>
					</tr>';
				}
			endforeach;
			$content .= '</thead></table>
			<br>
			<br>';
			$content .= '
			<table cellpadding="5" cellspacing="6">
			<thead>
			<tr align="left">
			<td border="1"><b>No.Pedido / O.C.:</b>&nbsp;&nbsp;'.$orden->noPedido.'</td>
			</tr>';
			foreach ($CO as $r) :
				if (!$idCostoOperativo=="") {
					$content .= '
					<tr>
					<td border="1"><b>Nombre de usuario:</b>&nbsp;&nbsp;'.$r->nombrePersona.'</td>
					</tr>
					<tr>
					<td border="1" width="50%"><b>Tiempo de entrega:</b>&nbsp;&nbsp;'.$r->tiempoEntrega.'</td>';
				}
			endforeach;
			$content .= '
			<td border="1" width="49%"><b>Fecha de realización:</b>&nbsp;&nbsp;'.$orden->fecha.'</td>
			</tr>';
			$content .= '
			</thead>
			</table>
			<br>';
			$content .= '
			<div style="margin-top: 100px;">
			<table cellpadding="5" cellspacing="5">
			<thead>
			<tr>
			<td align="center">Descripción de servicio:</td>
			</tr>
			<tr>
			<td border="1" style="height:250px; max-height:auto;">'.$orden->descripcion.'</td>
			</tr>
			<tr></tr>
			<tr cellpadding="5"></tr>
			<tr>
			<td align="center">Observaciones:</td>
			</tr>
			<tr>
			<td border="1" style="height:100px; max-height:auto;">'.$orden->observaciones.'</td>
			</tr>';
			$content .= '
			</thead>
			</table>
			</div>';
			//$pdf->writeHTML($content, true, 0, true, 0); 
			//$pdf->lastPage();
			$pdf->writeHTML($content, true, false, true, false, '');
			ob_end_clean();
			$pdf->output('Orden de Trabajo', 'I');
		}
	}
	public function ExportarEvaluacion(){
		header('Content-Type: application/pdf');
		$idEvaluacion=$_GET['idEvaluacion'];
		if ($idEvaluacion == "") {
			echo "Seleccione una Evaluación de Servicio";
		}else{
			$this->modelEvaluacion = new Evaluacion();
			$evaluacion = $this->modelEvaluacion->ListarDatosEvaluacion($idEvaluacion);
			$aspectos = $this->modelEvaluacion->ConsultaAspectosEvaluaciones($idEvaluacion);
			$aspectos = $aspectos->fetchAll(PDO::FETCH_OBJ);
			//print_r($aspectos);
			date_default_timezone_set("America/Mexico_City");
			$A= date("Y")."-".date("m")."-".date("d"); 
			$pdf = new ReportesController(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('Ammmec');
			$pdf->SetTitle('Evaluación de Servicio');
// set default header data
			$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// set font
			$pdf->SetFont('Helvetica', '', 11);
// add a page
			$pdf->AddPage();
			$content .= '
			<p><h1 style="text-align:center;">EVALUACIÓN DE SERVICIO</h1></p>
			<br>
			<br>
			<br>
			<table border="0" align="center" cellpadding="5">
			<thead>	
			<tr align="left">
			<td width="15%"><br><b>Contacto:</b></td>
			<td width="55%">'.$evaluacion->nombrePersona.'</td>
			</tr>
			<tr align="left">
			<td width="15%"><br><b>Cliente:</b></td>
			<td width="55%">'.$evaluacion->nombreOrganizacion.'</td>
			<td width="15%"><br><b>Fecha:</b></td>
			<td width="15%" align="center">'.$evaluacion->fechaEvaluacion.'</td>
			</tr>
			</thead></table>
			<p>Con el propósito de evaluar y mejorar nuestro desempeño, se solicita marcar el cuestionario en base a los siguientes criterios de Evaluación.</p>
			<br>
			<br>
			<p align="center"><h4>5 Excelente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 Bueno&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 Regular&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 Malo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 Deficiente</h4></p>
			';
			$content .= '
			<div style="margin-top: 100px;">
			<table cellpadding="5" cellpadding="5">
			<thead>
			<tr style="background-color:CCD1D1;">
			<td align="center" style="border-top:1px solid #000000; border-left:1px solid #000000; border-right:1px solid #000000;">Criterios de Evaluación</td>
			</tr>
			<tr style="background-color:CCD1D1;" align="center">
			<td width="50%" style="border-left:1px solid #000000;"></td>
			<td width="10%">1</td>
			<td width="10%">2</td>
			<td width="10%">3</td>
			<td width="10%">4</td>
			<td width="10%" style="border-right:1px solid #000000;">5</td>
			</tr>';
			foreach ($aspectos as $r) :
				$content .= '
				<tr>
				<td width="50%" border="1">'.$r->aspecto.'</td>';
				if(intval($r->valor)==1){
					$content .= '
					<td width="10%" border="1" align="center" style="background-color: #e0e0d1;">X</td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					</tr>';	
				}elseif(intval($r->valor)==2){
					$content .= '
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center" style="background-color: #e0e0d1;">X</td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					</tr>';	
				}elseif(intval($r->valor)==3){
					$content .= '
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center" style="background-color: #e0e0d1;">X</td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					</tr>';	
				}elseif(intval($r->valor)==4){
					$content .= '
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center" style="background-color: #e0e0d1;">X</td>
					<td width="10%" border="1" align="center"></td>
					</tr>';	
				}elseif(intval($r->valor)==5){
					$content .= '
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center"></td>
					<td width="10%" border="1" align="center" style="background-color: #e0e0d1;">X</td>
					</tr>';	
				}
			endforeach;
			$content .= '
			<tr><br></tr>
			<tr>
			<td width="100%" align="center">Observaciones:</td>
			</tr>
			<tr>
			<td border="1" width="100%" style="height:100px; max-height:auto;">'.$evaluacion->observaciones.'</td>
			</tr>';
			$content .= '
			</thead>
			</table><br><br>
			<p align="center">________________________________________</p>
			<p align="center">Firma del cliente</p>
			</div>';
			$pdf->writeHTML($content, true, false, true, false, '');
			ob_end_clean();
			$pdf->output('Evaluación de Servicio', 'I');
		}
	}
}
?>