<?php
require_once 'model/costo.php';
require_once 'model/negocio.php';
require_once '../../model/notificacion.php';

class CostoController{

	public $idCostoOperativo;

	public function __CONSTRUCT()
	{
		try{
			$this->model = new Costo();
			$this->modelNegocios = new Negocio();
			$this->modelNotificacion = new Notificacion();
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Index(){
		$procesos=true;
		$costo=true;
		$this->url="?c=costo";
		//$page = "view/page_not/page.php";
		$page="view/procesos/costo.php";
		require_once '../../view/index.php';
	}

	public function switchFiltro($bPeriodo, $bTexto)
	{
		switch ($bPeriodo) {
			case 1:
			$resultado=$this->model->ListarPendiente($bTexto);
			break;

			case 2:
			$resultado=$this->model->ListarRechazado($bTexto);
			break;

			case 3:
			$resultado=$this->model->ListarAprobado($bTexto);
			break;

			case 4:
			$resultado=$this->model->ListarSolicitudes($bTexto);
			break;
		}
		return $resultado;
	}


	public function consultas()
	{
		$bTexto = $_REQUEST['btexto'];
		if(isset($_REQUEST['bPeriodo']))
			$bPeriodo=$_REQUEST['bPeriodo'];
		else
			$bPeriodo=null;
		$resultado = $this->switchFiltro($bPeriodo, $bTexto);
		$accion=$_REQUEST['accion'];
		switch ($accion) {
			case 'tabla':
			$this->crearTabla($resultado);
			break;
			case 'exportar':
			$titulo='TODAS LAS SOLICITUDES';
			$pie='solicitudCosto.csv';
			$this->CrearArchivo($resultado,$titulo,$pie);
			break;
		}
	}

	public function rowToArray($r){
		return array(
			$r->idCostoOperativo,
			$r->fechaEmision,
			$r->fechaEntrega,
			$r->requisicion,
			$r->idNegocio,
			$r->idContacto,
			$r->tituloNegocio,
			$r->claveOrganizacion,
			$r->claveConsecutivo,
			$r->claveServicio,
			$r->nombrePersona,
			$r->nombreOrganizacion,
			$r->estado
		);
	}

	public function RegistrarSolicitud()
	{
		try {
			header('Content-Type: application/json');
			$costo = new Costo();
			$costo->idCostoOperativo = $_REQUEST['idCostoOperativo'];
			$costo->fechaEmision = $_REQUEST['fechaEmision'];
			$costo->fechaEntrega = $_REQUEST['fechaEntrega'];
			$costo->requisicion = $_REQUEST['requisicion'];
			$costo->idNegocio = $_REQUEST['idNegocio'];
			$costo->idContacto = $_REQUEST['idContacto'];
			$datos = array();
			if ($costo->idCostoOperativo > 0){
				$this->model->ActualizarSolicitud($costo);
				$row_array['respuesta']  = "ok";
			}else{
				$this->idCostoOperativo = $this->model->RegistrarSolicitud($costo);
				$row_array['respuesta'] = $this->idCostoOperativo;
			}
			array_push($datos, $row_array);
			echo json_encode($datos, JSON_FORCE_OBJECT);
		} catch (Exception $e) {
			echo 'Ha ocurrido un error al intentar guardar la solicitud';
		}
	}

	public function Notificacion()
	{
		try {
			$idCostoOperativo = $_POST['idCostoOperativo'];
			$not = new Notificacion();
			$not->idUsuarioEmisor = $_SESSION['idUsuario'];
			$not->idModuloEmisor= 3 ;
			$not->timestamp = date("Y-m-d H:i:s");
			$not->ruta = "?c=costo&idCostoOperativo=" . $idCostoOperativo;
			$not->idEvento = 2;
			$not->leido=0;
			$not->idRelacion = $idCostoOperativo;
			$idNotificacion = $this->modelNotificacion->Registrar($not);

			/* Emisor */
			$usuarioEmisor = $_SESSION['nombreUsuario'];
			$moduloEmisor = "Departamento de ventas";

			/* Receptores */
			$uNiv3Serv = $this->model->ListarNivelTresOperaciones();
			foreach($uNiv3Serv as $liderServicio):
				$idUsuarioReceptor = $liderServicio->idUsuario;
				$idModuloReceptor = $liderServicio->idModulo;
				$this->modelNotificacion->RegistrarReceptores($idNotificacion, $idUsuarioReceptor, $idModuloReceptor);
			endforeach;

			//Comienza render

			$evento = "Costo operativo";
			$fotoUsuario = '../../assets/imagenes/'.$_SESSION['foto'];
			$arrayjson = array(
				'idNotificacion'  => $idNotificacion,
				'usuarioEmisor'   => $usuarioEmisor,
				'moduloEmisor'    => $moduloEmisor,
				'timestamp'     => $not->timestamp,
				'ruta'        => $not->ruta,
				'evento'      => $evento,
				'idCostoOperativo'  => $not->idRelacion,
				'fotoUsuario'   => $fotoUsuario
			);

			echo json_encode($arrayjson);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


	public function AgregarServicio()
	{
		try {
			$costo = new Costo();
			$costo->idCostoOperativo = $_REQUEST['idCostoOperativo'];
			$costo->idSerCO = $_REQUEST['idSerCO'];
			$costo->clave = $_REQUEST['clave'];
			$costo->descripcion = $_REQUEST['descripcion'];
			$costo->cantidad = $_REQUEST['cantidad'];
			$costo->unidad = $_REQUEST['unidad'];
			if($costo->idSerCO > 0)
				$this->model->ActualizarServicio($costo);
			else
				$this->model->AgregarServicio($costo);
			$this->CarritoServicios($costo->idCostoOperativo);
		} catch (Exception $e) {
			echo 'Ha ocurrido al recuperar la información del servicio agregado';
		}
	}

	public function CarritoServicios($idCostoOperativo)
	{
		$idCostoOperativo = $_REQUEST['idCostoOperativo'];
		$resultado=$this->model->ListarCarrito($idCostoOperativo);
		echo'
		<table class="table table-bordered">
		<thead>
		<tr class="info" align="center">
		<td style="width: 10px;"><strong>Clave</strong></td>
		<td><strong>Descripción</strong></td>
		<td style="width: 100px;"><strong>Unidad</strong></td>
		<td style="width: 100px;"><strong>Cantidad</strong></td>
		<td style="width: 50px;"><strong>Acciones</strong></td>
		</tr>
		</thead>
		<tbody>';
		foreach($resultado as $r):
			echo'<tr>
			<td align="center"><strong>' . $r->clave . '</strong></td>
			<td>' . $r->descripcion . '</td>
			<td align="center">' . $r->unidad . '</td>
			<td align="center">' . $r->cantidad . '</td>
			<td align="center"><button type="button" class="btn btn-primary btn-xs" onclick="editarServicio(' . $r->idSerCO . ')"><i class="fas fa-pen-square"></i></button>
			<button type="button" class="btn btn-danger btn-xs" onclick="quitarServicio(' . $r->idSerCO . ')"><i class="fas fa-minus-circle"></i></button></td>
			</tr>';
		endforeach;
		echo'
		</tbody>
		</table>';
	}

	public function MostrarCarrito()
	{
		$idCostoOperativo = $_REQUEST['idCostoOperativo'];
		if (isset($idCostoOperativo))
			$this->CarritoServicios($idCostoOperativo);
		else
			echo "Ha ocurrido un error al intentar mostrar los servicios";
	}

	public function EditarServicio()
	{
		header('Content-Type: application/json');
		$idSerCO = $_POST['idSerCO'];
		$servicio = $this->model->ObtenerServicio($idSerCO);
		$datos = array();
		$row_array['idSerCO']  = $idSerCO;
		$row_array['descripcion']  = $servicio->descripcion;
		$row_array['clave']  = $servicio->clave;
		$row_array['unidad']  = $servicio->unidad;
		$row_array['cantidad']  = $servicio->cantidad;
		$row_array['precioUnitario']  = $servicio->precioUnitario;
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function QuitarServicio()
	{
		try {
			$idSerCO = $_REQUEST['idSerCO'];
			$idCostoOperativo = $_REQUEST['idCostoOperativo'];
			$this->model->EliminarServicio($idCostoOperativo, $idSerCO);
			$this->CarritoServicios($idCostoOperativo);
		} catch (Exception $e) {
			die($e->getMessage());
			echo 'Ha ocurrido un error al quitar el servicio';
		}
	}

	public function ConsultarProyectoClientePorNegocio()
	{
		try {
			header('Content-Type: application/json');
			$datos = array();
			$idNegocio = $_REQUEST['idNegocio'];
			$consulta = $this->model->ConsultarProyectoClientePorNegocio($idNegocio);
			$row_array['organizacion'] = $consulta->nombreOrganizacion;
			$row_array['tituloNegocio'] = $consulta->tituloNegocio;
			array_push($datos, $row_array);
			echo json_encode($datos, JSON_FORCE_OBJECT);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function ListarPersonasPorNegocio()
	{
		header('Content-Type: application/json');
		$idNegocio=$_POST['idNegocio'];
		$datos = array();
		foreach ($this->model->ListarPersonasPorNegocio($idNegocio) as $persona):
			$row_array['idCliente']  = $persona->idCliente;
			$row_array['nombrePersona']  = $persona->nombrePersona;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}


public function ListarNegocio()
	{
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->modelNegocios->Listar() as $negocio):
			$row_array['idNegocio']  = $negocio->idNegocio;
			$row_array['claveOrganizacion']  = $negocio->claveOrganizacion;
			$row_array['claveConsecutivo']  = $negocio->claveConsecutivo;
			$row_array['claveServicio']  = $negocio->claveServicio;
			array_push($datos, $row_array);
		endforeach;
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}
	public function consultaServicio()
	{
		header('Content-Type: application/json');
		$datos = array();
		$idCostoOperativo=$_POST['idCostoOperativo'];
		$respuesta = $this->model->consultaServicio($idCostoOperativo);
		if(empty($respuesta))
			$row_array['respuesta']  = "false";
		else
			$row_array['respuesta']  = "true";
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function EliminarCostoOperativo()
	{
		try
		{
			$idCostoOperativo = $_REQUEST['idCostoOperativoE'];
			$this->model->Eliminar($idCostoOperativo);
			echo "Se elimino correctamente el costo operativo";
		}
		catch(Exception $e)
		{
			echo "Se ha producido un error al eliminar el costo operativo";
		}
	}

	public function CheckServicio()
	{
		header('Content-Type: application/json');
		$datos = array();
		$idCostoOperativo=$_POST['idCostoOperativo'];
		$query = $this->model->ListarCarrito($idCostoOperativo);
		if(empty($query))
			$row_array['resultado']  = "false";
		else
			$row_array['resultado']  = "true";
		array_push($datos, $row_array);
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}

	public function crearTabla($resultado)
	{
		echo '
		<table class="table table-bordered" id="tb1" style="margin-bottom: 0px;">
		<thead>
		<tr style="background-color: #FCF3CF" align="center">
		<td><strong>Cuenta</strong></td>
		<td><strong>Titulo del negocio</strong></td>
		<td><strong>Fecha de emisión</strong></td>
		<td><strong>Fecha de entrega</strong></td>
		<td><strong>Costo operativo</strong></td>
		<td><strong>Requisición</strong></td>
		<td><strong>Estado</strong></td>
		<td><strong>Editar</strong></td>
		<td><strong>Imprimir</strong></td>
		</tr>
		</thead>
		<tbody>';
		if ($resultado == null) {
			echo '<tr><td class="alert-danger" colspan="9" align="center"> <strong>No se han encontrado solicitudes de costo operativo</strong></td></tr>';
		}else{
			foreach ($resultado as $r):
				$array = $this->rowToArray($r);
				$datos = implode(",", $array);

				echo '<tr>
				<td align="center"> ' . $r->claveOrganizacion .'-'. $r->claveConsecutivo . '-' . $r->claveServicio . ' </td>
				<td align="center"> ' . $r->tituloNegocio . ' </td>
				<td align="center"> ' . $r->fechaEmision . ' </td>
				<td align="center"> ' . $r->fechaEntrega . ' </td>
				<td align="center"> ' . $r->costoOperativo . ' </td>
				<td align="center"> ' . $r->requisicion . ' </td>
				<td';
				if ($r->estado=="Pendiente"){

					$query = $this->model->ListarCarrito($r->idCostoOperativo);
					if (empty($query)) {
						echo ' style="background-color: #F0B27A " align="center"><span class="badge" style="background-color: #F0B27A; color: black">INCOMPLETO</span></td>';
					}else {
						echo ' style="background-color: #F9E79F " align="center"><span class="badge" style="background-color: #F9E79F; color: black">PENDIENTE</span></td>';
					}
					echo ' <td align="center"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mSolicitud" onclick="EditarCostoOperativo('; echo "'". $datos . "'"; echo')"><i class="fas fa-edit"></i></button></td>
					<td align="center"><button class="btn btn-default btn-xs" disabled><i class="fas fa-print"></i></button>';
				}
				elseif($r->estado=="Atendido"){
					echo ' style="background-color: #F9E79F " align="center"><span class="badge" style="background-color: #F9E79F; color: black">ATENDIDO</span></td>
					<td align="center"><button class="btn btn-primary btn-xs" disabled><i class="fas fa-edit"></i></button></td>
					<td align="center"><button class="btn btn-default btn-xs" disabled><i class="fas fa-print"></i></button>';
				}elseif($r->estado=="Rechazado"){
					echo ' style="background-color: #E6B0AA" align="center"><span class="badge" style="background-color: #E6B0AA; color: black">RECHAZADO</span></td>
					<td align="center"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mSolicitud" onclick="myFunctionEditar('; echo "'". $datos . "'"; echo')"><i class="fas fa-edit"></i></button></td>
					<td align="center"><button class="btn btn-default btn-xs" disabled><i class="fas fa-print"></i></button>';
				}elseif($r->estado=="Aprobado"){
					echo ' style="background-color: #A2D9CE" align="center"><span class="badge" style="background-color: #A2D9CE; color: black">APROBADO</span></td>
					<td align="center"><button class="btn btn-primary btn-xs" disabled><i class="fas fa-edit" ></i></button></td>
					<td align="center"><a type="button" href="?c=reportes&a=ExportarCosto&idCostoOperativo='; echo $r->idCostoOperativo . '" target="_blank" class="btn btn-success btn-xs"><i class="fas fa-print"></i></a></td>';
				}else{
					echo '></tr>';
				}
			endforeach;
			echo'
			</tbody>
			</table>';
		}
	}
}
