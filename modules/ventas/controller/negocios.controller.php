<?php 
require_once 'model/negocio.php';
require_once 'model/persona.php';
require_once 'model/organizacion.php';
require_once 'model/embudo.php';
require_once 'model/etapa.php';
require_once 'model/equipo.php';
require_once 'model/deal.php';
require_once 'model/presupuesto.php';


class NegociosController{

	private $contenedor;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT()
	{
			try{
			$this->model = new Negocio();
			$this->modelPersona = new Persona();
			$this->modelOrganizacion = new Organizacion();
			$this->modelEmbudo = new Embudo();
			$this->modelEtapa = new Etapa();
			$this->modelEquipo = new Equipo();
			$this->modelDeal = new Deal();
			$this->modelPresupuesto = new Presupuesto();
		}catch(Exception $e){
			die($e->getMessage());
		}
	
	}

				//Metodo para listar todas las organizaciones para el select
	public function ListarOrganizaciones()
	{
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->modelOrganizacion->ListarO() as $organizacion):
			$row_array['idOrganizacion']  = $organizacion->idOrganizacion;
			$row_array['nombreOrganizacion']  = $organizacion->nombreOrganizacion;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}


	public function Index(){
		$negocios=true;
		$this->contenedor="view/negocios/negocios.php";
		$page="view/negocios/encabezado.php";
		require_once '../../view/index.php';
	}

public function Index2(){
		$page="view/negocios/encabezado.php";
		require_once '../../view/index.php';
	}

	public function lista(){
		$this->url="?c=negocios&a=lista";
		$this->contenedor="view/negocios/lista.php";
		$this->Index2();
	}

	public function timeline(){
		$this->url="?c=negocios&a=timeline";
		$this->contenedor="view/negocios/timeline.php";
		$this->Index2();
	}

	public function ConfigurarEmbudos(){
		$this->url="?c=negocios&a=ConfigurarEmbudos";
		$embudos = true;
		$page="../../view/configuracion/menu.php";
		$contenedor="view/negocios/embudos.php";
		require_once '../../view/index.php';
	}

	public function Guardar(){
		try
		{
			$negocio= new Negocio();
			$negocio->idNegocio=$_REQUEST['idNegocio'];
			$negocio->idUsuario=$_SESSION['idUsuario'];
			$negocio->idOrganizacion=$_REQUEST['idOrganizacion'];
			$negocio->idCliente=$_REQUEST['idCliente'];
			$negocio->idEmbudo=$_REQUEST['idEmbudo'];
			$negocio->idEtapa=$_REQUEST['idEtapa'];
			$negocio->idEquipo=$_REQUEST['idEquipo'];
			$negocio->idUnidadNegocio=$_REQUEST['idUnidadNegocio'];
			$negocio->tituloNegocio=$_REQUEST['tituloNegocio'];
			$negocio->valorNegocio=$_REQUEST['valorNegocio'];
			$negocio->tipoMoneda=$_REQUEST['tipoMoneda'];
			
			if ($_REQUEST['nombreEtapa'] == "Leads") {
				$negocio->claveOrganizacion=null;
				$negocio->claveConsecutivo=null;
				$negocio->claveServicio=null;
			}else{
				$negocio->claveOrganizacion=$_REQUEST['claveOrganizacion'];
				$negocio->claveConsecutivo=$_REQUEST['consecutivo'];
				$negocio->claveServicio=$_REQUEST['servicio'];
				$idNegocioEst=$_REQUEST['idNegocioEst'];
				if (!empty($idNegocioEst)) {
					$this->procesoEstimacion($idNegocioEst, $negocio->valorNegocio);
				}
			}

			$negocio->fechaGanadoPerdido = "";
			$negocio->fechaCierre=$_REQUEST['fechaCierre'];
			date_default_timezone_set("America/Mexico_City");
			$FechaAc= date("Y-m-d H:i:s");
			$negocio->contadorActivo=$FechaAc;
			$negocio->fechaCreado=$FechaAc;
			$negocio->status=0;
			$negocio->ponderacion=$_REQUEST['ponderacion'];
			$negocio->xCambio=$_REQUEST['xCambio'];
			$negocio->estimacion=$_REQUEST['estimacion'];

			/*Datos del presupuesto*/
			if (!empty($_REQUEST['idPresupuesto'])) {
				$presupuesto = new Presupuesto();
				$presupuesto->idPresupuestoGeneral=$_REQUEST['idPresupuesto'];
				$presupuesto->descripcion=$_REQUEST['descripcion'];
				$presupuesto->totalAnual = 0;
			}

			if($negocio->idNegocio>0){
				if ($negocio->estimacion == 0) {
					$this->actualizarDatosEstimacion($negocio);
				}
				$idOldEtapa=$this->model->VerificarIdEtapa($negocio->idNegocio);
				if ($idOldEtapa!=$negocio->idEtapa) {
					$ArrayCont=$this->model->ContenidoEtapas($idOldEtapa);
					$valor2 = explode(",", $ArrayCont);
					$borrar = array($negocio->idNegocio);
					$vector_nuevo = array_diff($valor2, $borrar);
					$cont=implode(',', $vector_nuevo);
					$this->modelEmbudo->OrdenarNegocios($idOldEtapa,$cont);
					$this->modelEmbudo->NegocioContenido($negocio->idEtapa,$negocio->idNegocio);
					$this->model->ActualizarContadorActivo($negocio->idNegocio);
					$this->model->Actualizar($negocio);
					echo "Se han actualizado correctamente los datos";
				}else{
					$this->model->Actualizar($negocio);
					echo "Se han actualizado correctamente los datos";
				}

			}else{
				$A=$this->model->Registrar($negocio);
				$this->model->ActualizaEtapaContenido($A,$negocio);
				if (empty($_REQUEST['idPresupuesto'])) {
					echo "Se ha registrado correctamente el negocio";
				}else{
					$presupuesto->idNegocio = $A;
					$this->modelPresupuesto->RegistrarPN($presupuesto);
					echo "Se ha registrado correctamente el negocio y se ha asignado a un presupuesto";
				}
			}
		}
		catch(Exception $e)
		{ 
			//echo "Se ha producido un error al guardar el negocio";
			die($e->getMessage());
		} 
	}

	public function actualizarDatosEstimacion(Negocio $negocio)
	{
		$clave = $this->model->consultarClave($negocio->idNegocio);
		$idEstimacion = $this->model->obtEstimaciones($clave->claveOrganizacion, $clave->claveConsecutivo, $clave->claveServicio);
		$this->model->actualizarEstimacion($negocio, $idEstimacion);
	}

	public function procesoEstimacion($idNegocio, $valorNegocio)
	{
		$this->model->actualizarValor($idNegocio, $valorNegocio);
	}

	public function CambiarClave()
	{
		try {
			$negocio= new Negocio();
			$negocio->idNegocio=$_REQUEST['idNegocio'];
			$negocio->claveOrganizacion=$_REQUEST['claveOrganizacion'];
			$negocio->claveConsecutivo=$_REQUEST['claveConsecutivo'];
			$negocio->claveServicio=$_REQUEST['claveServicio'];
			if ($negocio->idNegocio>0) {
				$this->model->CambiarClave($negocio);
				echo "Se han actualizado correctamente los datos";
			}
		} catch (Exception $e) {
			echo "Se ha producido un error al tratar de cambiar la clave";
		}
	}

	public function verificarConsecutivo()
	{
		$claveOrganizacion = $_POST['claveOrganizacion'];
		$claveConsecutivo = $_POST['claveConsecutivo'];
		$claveServicio = $_POST['claveServicio'];
		$idNegocio = $_POST['idNegocio'];

		$verificar = $this->model->verificarConsecutivo($claveOrganizacion, $claveConsecutivo, $claveServicio, $idNegocio);
		if ($verificar == "")
			echo "0";
		else
			echo "1";
	}

	/*Metodo para reordenar las etapas de la vista de embudos de configuracion*/
	public function Reordenar(){
		try
		{
			$embudo= new Embudo();
			$data= $_REQUEST['data'];
			$orden = 1;
			$array_elementos = explode(',', $data); /* separamos por comas y guardamos en un array*/
			foreach ($array_elementos as $elemento) {
				/*recordamos que los elementos se guardaban como "nombreEtapa-idEtapa", etc */
				$elemento_id = explode('-', $elemento); /*en $elemento_id[1] tendríamos la id*/
				$id = $elemento_id[1];
				$this->modelEmbudo->Reordenar($id, $orden);
				$orden++;
			}    
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		} 
	}

	//Metodo para pintar la vista de pronostico.
public function ConsultasPronostico(){
	$Año=$_REQUEST['Año'];
	$Mes=$_REQUEST['Mes'];
	$valorIdEmbudo=$_REQUEST['valorIdEmbudo'];
	$valorNomEmbudo=$_REQUEST['valorNomEmbudo'];
	$valorBusqueda=$_REQUEST['valor'];
	$_SESSION['idEmbudo'] = $valorIdEmbudo;
	$_SESSION['nombre'] = $valorNomEmbudo;
	$filtro=$_REQUEST['filtrostatus'];
	date_default_timezone_set("America/Mexico_City"); 
	if ($Año==null) {
		$Año= date("Y");
	}
	if ($Mes==null) {
		$Mes= date("n");
	}
	if ($filtro==null) {
		$filtro=0;
	}
	if (!$valorBusqueda=="") {
		$resultado=$this->model->ConsultaPorValorPronostico($valorBusqueda,$valorIdEmbudo,$filtro);
	}else{
		$resultado=0;
	}
	$this->CrearTablaPro($resultado,$valorIdEmbudo,$valorBusqueda,$filtro,$Mes,$Año);  
}

/*Metodo para actualizar la fecha de cierre*/
				public function CambiaFCierre(){
					$fechaCierre=$_REQUEST['fechaCierre'];
					$idNegocio=$_REQUEST['idNegocio'];
					$this->model->ActualizaCierreN($fechaCierre,$idNegocio);
					echo "Se ha actualizado correctamente la Fecha de cierre";
					/*$this->Index()*/;
				}

				
				/*Metodo para realizar la consulta de negocios de acuerdo al embudo seleccionado.*/
				public function NegociosPorEmbudo(){
					$valorIdEmbudo=$_REQUEST['valorIdEmbudo'];
					$valorNomEmbudo=$_REQUEST['valorNomEmbudo'];
					$valorBusqueda=$_REQUEST['valor'];
					$filtro=$_REQUEST['filtrostatus'];

					$_SESSION['idEmbudo'] = $valorIdEmbudo;
					$_SESSION['nombre'] = $valorNomEmbudo;

					$cantRegistros = $_REQUEST['registros'];
					$pagina=(int)(!isset($_REQUEST['pagina'])) ? 1 : $_REQUEST['pagina']; 
					$inicio = (($pagina - 1) * $cantRegistros);

					$totalRegistros = $this->model->totalNegocios($filtro, $valorIdEmbudo);
					$totalPaginas = ceil($totalRegistros / $cantRegistros);
					$val = 0;

					/* Operacion matematica para boton siguiente y atras */ 
					$IncrimentNum =(($pagina +1)<=$totalPaginas)?($pagina +1):1;
					$DecrementNum =(($pagina -1))<1?1:($pagina -1);

					if ($valorBusqueda == ""){
						$tfoot = "true";
						$resultado=$this->model->NegociosLista($valorIdEmbudo, $filtro, $inicio, $cantRegistros);
					}else{
						$tfoot = "false";
						$resultado=$this->model->ConsultaNegociosValor($valorIdEmbudo, $valorBusqueda, $filtro);
					}

					echo '
					<table class="table table table-hover" style="margin-bottom: 0px;">
					<thead>
					<tr style="background-color: rgba(106, 115, 123, 1);color: white;">
					<td align="center">Acción</td>
					<td>Clave</td>
					<td>Titulo</td>
					<td>Cliente</td>
					<td>Contacto</td>
					<td style="white-space: nowrap" align="right">Valor de negocio</td>
					<td align="center">Ponderación</td>
					<td>Unidad de negocio</td>
					<td>Equipo</td>

					</tr>
					</thead>';
					if($resultado==null){
						echo '<tr><td class="alert-danger" colspan="12" align="center"> <strong>No se encontraron negocios</strong></td></tr>';
					}else{
						foreach ($resultado as $r) :
							if ($r->status == 0) {
								echo '
								<tr class="">';
							}else if($r->status == 1){
								echo '
								<tr class="success">';
							}else if ($r->status == 2){
								echo '
								<tr class="danger">';
							}
							/*si el negocio esta activo se imprime de la siguiente manera*/
							echo '
							<td align="center">
							<div class="btn-group dropdown">
							<button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-h"></i>
							</button>
							
							<ul class="dropdown-menu" style="text-aling: rigth;text-align-last: left;">
							<li><a class="lista" href="#" data-toggle="modal" data-target="#mNegocio" onclick="myFunctionEditar('; ?><?php echo "'".$r->idNegocio."'"; ?> , <?php echo "'".$r->idEtapa."'"; ?> , <?php echo "'".$r->idOrganizacion."'"; ?> , <?php echo "'".$r->idCliente."'"; ?> , <?php echo "'".$r->idEmbudo."'"; ?> , <?php echo "'".$r->idEquipo."'"; ?> , <?php echo "'".$r->claveOrganizacion."'"; ?>, <?php echo "'".$r->claveConsecutivo."'"; ?> , <?php echo "'".$r->claveServicio."'"; ?> , <?php echo "'".$r->tituloNegocio."'"; ?> , <?php echo "'".$r->valorNegocio."'"; ?> , <?php echo "'".$r->tipoMoneda."'"; ?> , <?php echo "'".$r->fechaCierre."'"; ?> , <?php echo "'".$r->ponderacion."'"; ?> , <?php echo "'".$r->xCambio."'"; ?> , <?php echo "'".$r->nombreOrganizacion."'"; ?>, <?php echo "'".$r->nombrePersona."'"; ?>, <?php echo "'".$r->nombreEquipo."'"; ?>, <?php echo "'".$r->nombreEtapa."'"; ?>, <?php echo "'".$r->estimacion."'"; ?>, <?php echo "'".$r->idUnidadNegocio."'"; ?>, <?php echo "'".$r->unidadNegocio."'"; ?> <?php echo ')"><span class="glyphicon glyphicon-pencil"></span> Editar</a></li>';

							if(!empty($r->claveConsecutivo)){
								echo'
								<li role="separator" class="divider"></li>
								<li><a href="#" data-toggle="modal" data-target="#mNegocio" onclick="myFunctionEstimacion('; ?><?php echo "'".$r->idNegocio."'"; ?> , <?php echo "'".$r->idEtapa."'"; ?> , <?php echo "'".$r->idOrganizacion."'"; ?> , <?php echo "'".$r->idCliente."'"; ?> , <?php echo "'".$r->idEmbudo."'"; ?> , <?php echo "'".$r->idEquipo."'"; ?> , <?php echo "'".$r->claveOrganizacion."'"; ?>, <?php echo "'".$r->claveConsecutivo."'"; ?> , <?php echo "'".$r->claveServicio."'"; ?> , <?php echo "'".$r->tituloNegocio."'"; ?> , <?php echo "'".$r->valorNegocio."'"; ?> , <?php echo "'".$r->tipoMoneda."'"; ?> , <?php echo "'".$r->fechaCierre."'"; ?> , <?php echo "'".$r->ponderacion."'"; ?> , <?php echo "'".$r->xCambio."'"; ?> , <?php echo "'".$r->nombreOrganizacion."'"; ?>, <?php echo "'".$r->nombrePersona."'"; ?>, <?php echo "'".$r->nombreEquipo."'"; ?>, <?php echo "'".$r->nombreEtapa."'"; ?>, <?php echo "'".$r->idUnidadNegocio."'"; ?>, <?php echo "'".$r->unidadNegocio."'"; ?> <?php echo ')"><span class="glyphicon glyphicon-usd"></span> Estimación</a></li>';
							}
							if ($r->estimacion == 0 and !empty($r->claveOrganizacion)) {
								echo '
								<li role="separator" class="divider"></li>
								<li><a href="#" data-toggle="modal" data-target="#mCambiarClave" onclick="cambiarClaveNegocio('; ?><?php echo "'".$r->idNegocio."'"; ?>, <?php echo "'".$r->tituloNegocio."'"; ?>, <?php echo "'".$r->idOrganizacion."'"; ?>, <?php echo "'".$r->claveConsecutivo."'"; ?> , <?php echo "'".$r->claveServicio."'"; ?> <?php echo ')"><span class="glyphicon glyphicon-retweet"></span> Cambiar clave</a></li>';
							}
							echo '</ul>
							</div>
							</td>
							<td style="white-space: nowrap">';
							if (!empty($r->claveOrganizacion))
								echo '<a href="?c=deal&idNegocio='.$r->idNegocio.'">'.$r->claveOrganizacion.'-'.$r->claveConsecutivo.'-'.$r->claveServicio.'</a>';
							else
								echo '<a href="?c=deal&idNegocio='.$r->idNegocio.'">Leads</a>';
							echo '</td>
							<td style="white-space: nowrap">'. $r->tituloNegocio .'</td>

							<td style="white-space: nowrap">'. $r->nombreOrganizacion .'</td>
							<td style="white-space: nowrap">'. $r->nombrePersona .'</td>
							<td style="white-space: nowrap" align="right">$'. number_format($r->valorNegocio, 2, '.', ',').' / '.$r->tipoMoneda.'</td>
							<td align="center">'. $r->ponderacion .'</td>
							<td>'. $r->unidadNegocio .'</td>
							<td>'. $r->nombreEquipo .'</td>
							</tr>';

						endforeach;
						if ($tfoot == "true") {
							echo '
							<tfoot>
							<tr>
							<td colspan="8"><p class="help-block" style="padding-top: 80px;">Mostrando '.$inicio.' a '.($cantRegistros + $inicio).' de '.$totalRegistros.' registros</p></td>
							<td colspan="4" align="right">
							<nav id="navPaginacion">';
							echo'
							<ul class="pagination" style="margin: 0px; margin-top: 80px;">
							<li id="previous" onclick="paginador('.$DecrementNum.');"><a><span aria-hidden="true">&laquo;</span></a></li>';
							/* Se resta y suma con el numero de pag actual con la cantidad de numeros a mostrar */
							$tope = 5;
							$Desde=$pagina-(ceil($tope / 2) - 1);
							$Hasta=$pagina+(ceil($tope / 2) - 1);
							/* Se valida */
							$Desde=($Desde<1)?1: $Desde;
							$Hasta=($Hasta<$tope)?$tope:$Hasta;
							/* Se muestra los numeros de paginas */
							for($i=$Desde; $i<=$Hasta;$i++){
								/* Se valida la paginacion total de registros */
								if($i<=$totalPaginas){
									/* Validamos la pag activo */
									if($i==$pagina){
										echo '<li id="li'.$i.'" class="active" onclick="paginador('.$i.');"><a>'.$i.' <span class="sr-only">(current)</span></a></li>';
									}else {
										echo '<li id="li'.$i.'" onclick="paginador('.$i.');"><a>'.$i.' <span class="sr-only">(current)</span></a></li>';
									}     		
								}
							}

							echo '
							<li id="next" onclick="paginador('.$IncrimentNum.');"><a><span aria-hidden="true">&raquo;</span></a></li>
							</ul>
							</nav>
							</td>
							<tr>
							</tfoot>';
						}else{
							echo '
							<tfoot>
							<tr>
							<td colspan="12" style="padding-top: 80px;"></td>
							</tr>
							';
						}
					}
					echo '</table>';
				}

				public function GuardarEmbudo(){ 
					try {
						$embudo = new Embudo();
						$embudo->idEmbudo = $_REQUEST['idEmbudo'];
						$embudo->idUsuario = $_SESSION['idUsuario'];
						$embudo->nombre = $_REQUEST['nombre'];
						$embudo->perdido = 0;
						$embudo->ganado = 0;
						$embudo->logo = "logo.jpg";
						if ($embudo->idEmbudo > 0) {
							if ($embudo->idEmbudo == $_SESSION['idEmbudo'])
								$_SESSION['nombre'] = $embudo->nombre;
							$this->modelEmbudo->Actualizar($embudo);
							echo "Se ha actualizado correctamente el embudo";
						}else{
							$this->modelEmbudo->Registrar($embudo);
							$consulta = $this->modelEmbudo->ListarEmbudo(); 
							echo $consulta;	
						}
					} catch (Exception $e) {
						echo "Se ha producido un error al registrar el embudo";
					}
				}

    //Metodo para registrar etapas por ajax -->
				public function RegistrarEtapa(){
					try {
						$etapa = new Etapa();
						$etapa->idEtapa=$_REQUEST['idEtapa'];
						$etapa->idEmbudo=$_REQUEST['idEmbudo'];
						$etapa->nombreEtapa=$_REQUEST['nombreEtapa'];
						$etapa->probabilidad=$_REQUEST['probabilidad'];
						$etapa->inactividad=$_REQUEST['inactividad'];
						$etapa->contenido="";
						$etapa->consecutivo=1;
						$orden = $this->modelEtapa->VerificarNumOrden($etapa);
						$numOrden = $orden + 1;
						$etapa->orden = $numOrden;
						if ($etapa->idEtapa > 0) {
							$this->modelEtapa->Actualizar($etapa);
							echo "Se ha actualizado correctamente la etapa";
						}else{
							$this->modelEtapa->Registrar($etapa);
							echo "Se ha registrado correctamente la etapa";
						}

					}catch (Exception $e) {
						echo "No se pudo registrar la etapa";
					}
				}

    //Metodo para eliminar etapas por ajax -->
				public function EliminarEtapa(){
					try {
						$idEtapa=$_REQUEST['idEtapa'];
						$this->modelEtapa->Eliminar($idEtapa);
						echo "Se ha eliminado correctamente la etapa";
					}catch (Exception $e) {
						echo "No se pudo eliminar la etapa";
					}
				}

    //Metodo para eliminar embudos por ajax -->
				public function EliminarEmbudo(){
					try {
						if (!isset($_REQUEST['idEmbudo'])){
							header("Location: index.php?c=negocios&a=ConfigurarEmbudos");
						}else{
							$idEmbudo=$_REQUEST['idEmbudo'];
							$this->modelEmbudo->Eliminar($idEmbudo);
							$consulta = $this->modelEmbudo->ListarEmbudo(); 
							echo $consulta;
						}
					}catch (Exception $e) {
						echo "No se pudo eliminar el embudo";
					}
				}

				public function ContadoresPorEmbudo(){
					$temp = 0;
					$sum = 0;

					$idEmbudo=$_REQUEST['idEmbudo'];
					$valorBusqueda=$_REQUEST['valor'];
					$filtro=$_REQUEST['filtrostatus'];

					$contadorNegocios=$this->model->Contador($idEmbudo, $valorBusqueda, $filtro);
					$contadorValor= $this->model->ContadorValor($idEmbudo, $valorBusqueda, $filtro);
					$dolar=$this->model->ConsultaValorDolar();
					$sumaPon = $this->model->ContadorPonderacion($idEmbudo, $valorBusqueda, $filtro,$dolar);

					foreach ($contadorValor as $r):
						if ($r->tipoMoneda == 'USD') {
							$temp = $r->valorNegocio * $dolar;
							$sum = $sum + $temp;
						}else if ($r->tipoMoneda == 'MXN'){
							$sum = $sum + $r->valorNegocio;
						}
					endforeach;

					if ($contadorNegocios == 1) {
						echo '
						<h5><i class="fas fa-balance-scale"></i>&nbsp&nbsp<strong>$'; ?><?php echo number_format($sum, 2, '.', ','); ?> <?php echo " MXN - $";?><?php echo number_format($sumaPon, 2, '.', ','); ?><?php echo " MXN - ";?> <?php echo $contadorNegocios; ?> <?php echo 'Negocio </strong></h5>'; 
					}else { 
						echo '<h5><i class="fas fa-balance-scale"></i>&nbsp&nbsp<strong>$';?><?php echo number_format($sum, 2, '.', ',');  ?> <?php echo " MXN - $";?><?php echo number_format($sumaPon, 2, '.', ','); ?><?php echo " MXN - ";?><?php echo $contadorNegocios; ?> <?php echo 'Negocios </strong></h5>';
					} 
				}

//Metodo para consultar la clave de organizacón segun el id seleccionado
				public function ConsultaClaveOrganizacion(){
					$idOrganizacion=$_REQUEST['valorIdOrganizacion'];
					$consultaClave=$this->model->ConsultaClaveOrganizacion($idOrganizacion);
					echo $consultaClave;
				}

//Metodo para consultar el numero consecutivo
				public function ConsultaConsecutivo(){
					$idOrganizacion=$_REQUEST['valorIdOrganizacion'];
					$consultaClave=$this->model->ConsultaClaveOrganizacion($idOrganizacion);
					$numConsecutivo=$this->model->ConsultaConsecutivo($consultaClave);

					if($numConsecutivo == null){
						$val = 1;
						echo str_pad($val, 4, "0", STR_PAD_LEFT);
					}else{
						$sumConsecutivo = $numConsecutivo + 1;
						echo str_pad($sumConsecutivo, 4, "0", STR_PAD_LEFT);
					}
				}


//Metodo para listar los embudos
			public function ListarEmbudos(){
				header('Content-Type: application/json');
				$datos = array();
				foreach ($this->model->ListarEmbudos() as $e):
					$row_array['idEmbudo']  = $e->idEmbudo;
					$row_array['nombre']  = $e->nombre;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}

  //Metodo para listar organizaciones en base a la persona de contacto
			public function ListarOrganizacionesPorPersona(){
				header('Content-Type: application/json');
				$idCliente=$_REQUEST['idCliente'];
				$datos = array();
				foreach ($this->model->ListarOrganizacionesPorPersona($idCliente) as $organizacion):
					$row_array['idOrganizacion']  = $organizacion->idOrganizacion;
					$row_array['nombreOrganizacion']  = $organizacion->nombreOrganizacion;
					$row_array['clave']  = $organizacion->clave;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}

    //Metodo para listar personas en base a la organizacion
			public function listarPersonasPorOrganizacion(){
				header('Content-Type: application/json');
				$idOrganizacion=$_REQUEST['idOrganizacion'];
				$datos = array();
				foreach ($this->model->listarPersonasPorOrganizacion($idOrganizacion) as $persona):
					$row_array['idCliente']  = $persona->idCliente;
					$row_array['nombrePersona']  = $persona->nombrePersona;
					$row_array['clave']  = $persona->clave;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}
//Metodo para listar personas en base a la organizacion
			public function listarPersonas(){
				header('Content-Type: application/json');
				$datos = array();
				foreach ($this->model->listarPersonas() as $persona):
					$row_array['idCliente']  = $persona->idCliente;
					$row_array['nombrePersona']  = $persona->nombrePersona;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}

    //Metodo para listar presupuestos
			public function listarPresupuestos(){
				$idEmbudo = $_REQUEST['idEmbudo'];
				$idUsuario = $_SESSION['idUsuario'];
				header('Content-Type: application/json');
				$datos = array();
				foreach ($this->modelPresupuesto->listar($idUsuario, $idEmbudo) as $presupuesto):
					$row_array['idPresupuestoGeneral']  = $presupuesto->idPresupuestoGeneral;
					$row_array['nombrePresupuesto']  = $presupuesto->nombrePresupuesto;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}

    //Metodo para listar equipos
			public function ListarEquipo(){
				header('Content-Type: application/json');
				$datos = array();
				$stm = $this->modelEquipo->listar();
				$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
				foreach ($resultado as $equipo):
					$row_array['idEquipo']  = $equipo->idEquipo;
					$row_array['nombreEquipo']  = $equipo->nombreEquipo;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}

			public function ListarUnidadNegocio(){
				header('Content-Type: application/json');
				$datos = array();
				$resultado = $this->model->listarUnidadNegocio();
				foreach ($resultado as $unidad):
					$row_array['idUnidadNegocio']  = $unidad->idUnidadNegocio;
					$row_array['unidadNegocio']  = $unidad->unidadNegocio;
					$row_array['claveUnidadNegocio']  = $unidad->claveUnidadNegocio;
					array_push($datos, $row_array);
				endforeach;   
				echo json_encode($datos, JSON_FORCE_OBJECT);
			}

			public function actualizarDolar(){
				$returnHtml = array();
				$page='https://www.msn.com/es-mx/dinero/conversor-de-divisas';
				$returnRawHtml = file_get_contents( $page );  
				preg_match_all("/(<([\w]+)[^>]*>)(.*?)(<\/\\2>)/",$returnRawHtml,$returnHtml,PREG_PATTERN_ORDER);
				$A= $returnHtml[0][19];
				$arr1 = str_split($A);
				$pila = array();
				$cont=0;
				foreach ($arr1 as $valor) {
					$cont++;
					if ($cont==43||$cont==44||$cont==45||$cont==46||$cont==47) {
						array_push($pila, $valor);  
					}     
				}
				$DOLAR=implode($pila);
				$this->model->ActualizarValorDolar($DOLAR);
				echo $DOLAR;
			}

			//Metodo para realizar la consulta de negocios de acuerdo al embudo seleccionado.
				public function DesplazarPronostico(){
					$mesInicio=$_REQUEST['mesInicio'];
					$Año=$_REQUEST['Año'];
					$filtro=$_REQUEST['filtro'];
					$valorBusqueda=$_REQUEST['resultado'];
					$list1=$_REQUEST['list1'];
					if (!$list1==null) {
						$output=array();
						$list1=parse_str($list1,$output);
						$a=implode(',',$output['item']);
						$a = explode(",", $a);
					}else{
						$a=array();
					}
					$list2=$_REQUEST['list2'];
					if (!$list2==null) {
						$output=array();
						$list2=parse_str($list2,$output);
						$b=implode(',',$output['item']);
						$b = explode(",", $b);
					}else{
						$b=array();
					}
					$list3=$_REQUEST['list3'];
					if (!$list3==null) {
						$output=array();
						$list3=parse_str($list3,$output);
						$c=implode(',',$output['item']);
						$c = explode(",", $c);
					}else{
						$c=array();
					}
					$list4=$_REQUEST['list4'];
					if (!$list4==null) {
						$output=array();
						$list4=parse_str($list4,$output);
						$d=implode(',',$output['item']);
						$d = explode(",", $d);
					}else{
						$d=array();
					}

					if ($valorBusqueda=="") {
						$Valor=$this->model->ConsultaNegociosXMes($_SESSION['idEmbudo'],$filtro);
					}else{

						$Valor=$this->model->ConsultaPorValorPronostico($valorBusqueda,$_SESSION['idEmbudo'],$filtro);
					}
					foreach ($Valor as $key) {
						if ($key->meses==$mesInicio and $key->Año==$Año) {
							if (!in_array($key->idNegocio, $a)) {
								echo '<input type="hidden" id="idNegocio" name="idNegocio" value="'.$key->idNegocio.'">';
							}
						}elseif ($key->meses==$mesInicio+1 and $key->Año==$Año) {
							if (!in_array($key->idNegocio, $b)) {
								echo '<input type="hidden" id="idNegocio" name="idNegocio" value="'.$key->idNegocio.'">';
							}
						}elseif ($key->meses==$mesInicio+2 and $key->Año==$Año) {
							if (!in_array($key->idNegocio, $c)) {
								echo '<input type="hidden" id="idNegocio" name="idNegocio" value="'.$key->idNegocio.'">';
							}
						}elseif($key->meses==$mesInicio+3 and $key->Año==$Año){
							if (!in_array($key->idNegocio, $d)) {
								echo '<input type="hidden" id="idNegocio" name="idNegocio" value="'.$key->idNegocio.'">';
							}
						}
					}
				}

				

				

				

				public function ConsultaEtapaPorEmbudo(){
					$idEmbudo=$_REQUEST['valorIdEmbudo'];
					echo '<select class="form-control"  name="idEtapa" id="selectEtapa" onchange="leads();">
					<option value="">Seleccione la etapa inicial</option>
					'; foreach($this->model->ListarEtapas($idEmbudo) as $r):
					echo '<option class="btn-default" value="'.$r->idEtapa.'">';?><?php echo $r->nombreEtapa; ?><?php echo '</option>'; ?><?php
				endforeach;  
				echo '</select>';
				echo '<input type="hidden" name="idEmbudo" value="'; ?><?php echo  $idEmbudo; ?><?php echo '">';
			}

    
public function CrearTablaPro($resultado,$valorIdEmbudo,$valorBusqueda,$filtro,$Mes,$Año){
	date_default_timezone_set("America/Mexico_City"); 
	$mesA= date("m");
	$A=$mesA;
	$FechaAc= date("Y-m-d");
	$MesV=$Mes;
	$AñoV=$Año;
	$ContadorMeses=0;
	$ciclo=5;
	$AñoMetodos=$Año;
	echo '<div style="overflow-x: auto;">
	<table class="table table-bordered" id="miTablaPersonalizada" style="margin-bottom: 0px;">
	<tr style="background-color: #EAECEE;">';
	for ($i=0; $i <4; $i++) { 
		switch ($Mes) {
			case '01':
			$mes="Enero";
			break;
			case '02':
			$mes="Febrero";
			break;
			case '03':
			$mes="Marzo";
			break;
			case '04':
			$mes="Abril";
			break;
			case '05':
			$mes="Mayo";
			break;
			case '06':
			$mes="Junio";
			break;
			case '07':
			$mes="Julio";
			break;
			case '08':
			$mes="Agosto";
			break;
			case '09':
			$mes="Septiembre";
			break;
			case '10':
			$mes="Octubre";
			break;
			case '11':
			$mes="Noviembre";
			break;
			default:
			$mes="Diciembre";
			break;
		}

		$ContadorMeses++;
		$ciclo--;
		if ($Mes==1 and $ContadorMeses==4 and $ciclo==1) {
			$AñoMetodos=$AñoMetodos+1;
		}
		if ($Mes==1 and $ContadorMeses==3 and $ciclo==2) {
			$AñoMetodos=$AñoMetodos+1;
		}
		if ($Mes==1 and $ContadorMeses==2 and $ciclo==3) {
			$AñoMetodos=$AñoMetodos+1;
		}

		echo '<td>
		<div class="col-lg-12">
		<div class="col-xs-12 col-lg-12" style="padding-left: 0px;">
		<strong>'.$mes.'</strong>
		</div>
		<div class="col-xs-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;">
		<hr style="margin-top: 1px; margin-bottom: 6px;">
		</div>



		<div class="col-xs-12 col-lg-6" style="padding-left: 0px;">
		<h6 style="margin-top: 2px; margin-bottom: 2px;">Presupuesto</h6>
		</div>  
		<div class="col-xs-12 col-lg-6" align="right" style="padding-left: 0px; padding-right: 0px;">
		<h6 style="margin-top: 2px; margin-bottom: 2px;"><span>$ </span>';
		$PresupuestoXMes=$this->model->ConsultaNegociosPresupuestoXMes($Mes,$AñoMetodos,$valorIdEmbudo);
		$suma=0;
		foreach ($PresupuestoXMes as $key) {
			$suma=$suma+$key->monto;   
		}
		echo number_format($suma, 2, '.', ',').' MXN</h6>
		</div>
		<div class="col-xs-12 col-lg-6" style="padding-left: 0px;">
		<h6 style="margin-top: 2px; margin-bottom: 2px;">Estimado</h6>
		</div>
		<div class="col-xs-12 col-lg-6 '.$mes.'" align="right" style="padding-left: 0px; padding-right: 0px;">
		<h6 style="margin-top: 2px; margin-bottom: 2px;"></h6>
		</div>';
		$sumaProbabilidad=0;
		$sumaValorEstimado=0;
		$ResultadoNegociosEstimadoXMes=$this->model->ConsultaNegociosEstimadoXMes($Mes,$AñoMetodos,$valorIdEmbudo);
		foreach ($ResultadoNegociosEstimadoXMes as $resul) {
			$sumaProbabilidad=$sumaProbabilidad+$resul->probabilidad;
			if ($resul->tipoMoneda=='USD') {
				$ValDolar=$this->model->ConsultaValorDolar();
				$sumaValorEstimado=($sumaValorEstimado+($ValDolar*$resul->valorNegocio));
			}else{
				$sumaValorEstimado=($sumaValorEstimado+$resul->valorNegocio);
			}
		}
		if (sizeof($ResultadoNegociosEstimadoXMes)!=0) {
			$sumaProbabilidad=$sumaProbabilidad/sizeof($ResultadoNegociosEstimadoXMes);
		}
		if ($sumaProbabilidad==100 or $sumaProbabilidad==0) {
			echo '<style>
			.'.$mes.':before {
				content:"$'.number_format($sumaValorEstimado, 2, '.', ',').' MXN"; 
				padding:0 auto;
				font-size:12px; 
			}
			</style>';
		}else{
			$valorReal=$sumaValorEstimado*$sumaProbabilidad/100;
			echo ' <style>
			.'.$mes.':before {
				content:"$'.number_format($valorReal, 2, '.', ',').' MXN"; 
				padding:0 auto;
				font-size:12px; 
				}.'.$mes.':hover:before {
					content:"'.intval($sumaProbabilidad).'% de $'.number_format($sumaValorEstimado, 2, '.', ',').' MXN";  
					padding:0 auto;
					font-size:12px;
				}
				</style>';
			}
			echo '<div class="col-xs-12 col-lg-6" style="padding-left: 0px;">
			<h6 style="margin-top: 2px; margin-bottom: 2px;">Ganado</h5>
			</div>
			<div class="col-xs-12 col-lg-6" align="right" style="padding-left: 0px; padding-right: 0px;">
			<h6 style="margin-top: 2px; margin-bottom: 2px;"><span>$ </span>'; 
			$GanadosXMes=$this->model->ConsultaNegociosGanadosXMes($Mes,$AñoMetodos,$valorIdEmbudo);
			$Acumulado=0; 
			foreach ($GanadosXMes as $variable) {
				if ($variable->tipoMoneda=='USD') {
					$ValDolar=$this->model->ConsultaValorDolar();
					$Acumulado=($Acumulado+($ValDolar*$variable->valorNegocio));
				}else{
					$Acumulado=($Acumulado+$variable->valorNegocio);
				}
			}
			echo number_format($Acumulado, 2, '.', ',');
			echo ' MXN</h6>
			</div>

			<div class="col-xs-12 col-lg-6" style="padding-left: 0px;">
			<h6 style="margin-top: 2px; margin-bottom: 2px;">Facturado</h6>
			</div>
			<div class="col-xs-12 col-lg-6" align="right" style="padding-left: 0px; padding-right: 0px;">
			<h6 style="margin-top: 2px; margin-bottom: 2px;"><span>$ </span>59,79</h6>
			</div>

			</div>
			</td>';
			if ($Mes>=12) {
				$Mes=$Mes-11;
			}else{
				$Mes++;
			}
		}
		echo '</tr>
		<tr style="background-color: #FFF;">';
		if ($resultado==0) {

			$B=$this->model->ConsultaNegociosXMes($_SESSION['idEmbudo'],$filtro);
		}else{
			$B=$resultado;
		}

		$conS=1;
		for ($i=0; $i <4 ; $i++) { 
			echo '<td style="padding: 1px;">
			<ul id="sortable'.$conS++.'" class="connectedSortable sortable">';
			$con=0;
			foreach ($B as $r) {
				if ($MesV==13 and $con==0) {
					$Año=$Año+1;
					$MesV=1;
				}
				$con++;
				if ($r->meses==$MesV and $r->Año==$Año) {
					$Res=$this->model->ConsultaNegociosTrueM($r->idNegocio,$filtro);
					if (!empty($Res)) {
						foreach ($Res as $key) {
							$hoy = date("Y-m-d H:i:s");  
							$datetime1 = date_create($hoy);
							$datetime2 = date_create($key->contadorActivo);
							$interval = date_diff($datetime2, $datetime1);
							echo '<li id="item_'.$key->idNegocio.'" class="list-group-item item_embudo ';
							if ($filtro==1) {
								echo 'list-group-item-success '; 
							}elseif ($filtro==2) {
							}else{
								if ($key->inactividad==0) {       
								}elseif ($interval->format('%a')==$key->inactividad) {
									echo 'list-group-item-warning ';         
								}elseif($interval->format('%a')<$key->inactividad){  
								}
								elseif($interval->format('%a')>$key->inactividad) {
									echo 'list-group-item-danger estancado ';        
								}
							}  echo 'ui-sortable-handle">';

							echo '<div class="row">
							<div class="col-xs-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
							<a class="btn" href="?c=deal&idNegocio='; echo $key->idNegocio;  echo '"style="padding: 1px; white-space: pre-wrap; text-align: justify;">';
							if (!empty($key->claveOrganizacion))
								echo $key->claveOrganizacion.'-'.$key->claveConsecutivo.'-'.$key->claveServicio;
							else
								echo 'Leads';
							echo '</a>
							</div>
							<div class="col-xs-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">';
							$Actividad=$this->model->FechaActividadNegocio($key->idNegocio);
							if (!isset($Actividad))  {
								echo '<a class="btn btn-lg" data-toggle="modal" data-target="#añadiractividad" onclick="myFunctionAñadirActividad(';?> <?php echo $key->idOrganizacion;?>,<?php echo "'".$key->nombreOrganizacion."'"; ?>,<?php echo $key->idCliente ?>,<?php echo "'".$key->nombrePersona."'" ?>,<?php echo $key->idNegocio?>,<?php echo "'".$key->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-warning-sign yellow" ></span></a>';
							}elseif ($FechaAc>$Actividad) {
								echo '<a class="btn btn-lg" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?> <?php echo $key->idOrganizacion;?>,<?php echo "'".$key->nombreOrganizacion."'"; ?>,<?php echo $key->idCliente ?>,<?php echo "'".$key->nombrePersona."'" ?>,<?php echo $key->idNegocio?>,<?php echo "'".$key->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-play-circle red" ></span></a>';
							}elseif ($FechaAc==$Actividad) {
								echo '<a class="btn btn-lg" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?> <?php echo $key->idOrganizacion;?>,<?php echo "'".$key->nombreOrganizacion."'"; ?>,<?php echo $key->idCliente ?>,<?php echo "'".$key->nombrePersona."'" ?>,<?php echo $key->idNegocio?>,<?php echo "'".$key->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-play-circle green" ></span></a>';
							}else{   
								echo '<a class="btn btn-lg" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?> <?php echo $key->idOrganizacion;?>,<?php echo "'".$key->nombreOrganizacion."'"; ?>,<?php echo $key->idCliente ?>,<?php echo "'".$key->nombrePersona."'" ?>,<?php echo $key->idNegocio?>,<?php echo "'".$key->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-play-circle silver" ></span></a>';
							}
							echo '</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row">
							<p style="margin-bottom: 0px;"><small>'."\n".$key->tituloNegocio.'<br>'."$".number_format($key->valorNegocio, 2, '.', ',')." ".$key->tipoMoneda.'<br>'.$key->nombreOrganizacion.'</small></p>
							</div>
							</div>
							<div class="col-xs-12 col-lg-12" align="center">';
							if ($filtro==1) {
								echo '<span class="badge">GANADO</span > ';
							}elseif ($filtro==2) {
								echo '<span class="badge">PERDIDO</span > ';
							}else{
								if ($key->inactividad==0) {
								}
								elseif ($interval->format('%a')>$key->inactividad) {
									echo '<span class="badge">'.($interval->format('%a')-$key->inactividad)." Días".'</span > ';
								}
							} 
							echo '</div>
							</div>
							</li>';  
						} 
					} 
				}
			} 
			echo '</ul>
			</td>';
			$MesV++;
		}
		echo '</tr>';
		if (empty($B)) {
			echo '<tr><td class="alert-danger" colspan="4" align="center"> <strong>No se encontraron negocios</strong></td></tr>';
		}
		echo '</table>
		</div>';
		$MesV=$MesV-4; 
		echo '<script>
		$( function() {
			$("#sortable1,#sortable2,#sortable3,#sortable4").sortable({
				connectWith: ".connectedSortable",
				placeholder: "placeholder",
				cursor: "move",
				opacity: 0.7, helper: "clone", 
				cursorAt: { top: 20, left: 56 }, 
				revert: true,
				connectWith: ".connectedSortable",
				update: function(event,ui) {
					var mesInicio='.$MesV.';
					var Año='.$AñoV.';
					var filtro='.$filtro.';
					var resultado="'.$valorBusqueda.'";
					var postDataA=$("#sortable1").sortable("serialize");
					var postDataB=$("#sortable2").sortable("serialize");
					var postDataC=$("#sortable3").sortable("serialize");
					var postDataD=$("#sortable4").sortable("serialize");
					$.post("index.php?c=negocios&a=DesplazarPronostico",{list1:postDataA,list2:postDataB,list3:postDataC,list4:postDataD,mesInicio:mesInicio,filtro:filtro,resultado:resultado,Año:Año},function(mensaje) {
						$("#aaaa").html(mensaje);$("#mTerminar").modal("show"); 
						});
					}
					});
					} );
					</script>';

				}

/*Metodo para reordenar las etapas de la vista de embudos de configuracion*/
								public function Consultas(){
									$valorIdEmbudo=$_REQUEST['valorIdEmbudo'];
									$valorNomEmbudo=$_REQUEST['valorNomEmbudo'];
									$valorBusqueda=$_REQUEST['valor'];
									$_SESSION['idEmbudo'] = $valorIdEmbudo;
									$_SESSION['nombre'] = $valorNomEmbudo;
									$filtro=$_REQUEST['filtrostatus'];
									if ($filtro==null) {
										$filtro=0;
									}
									if (!$valorBusqueda=="") {
										$resultado=$this->model->ConsultaPorValor($valorBusqueda,$valorIdEmbudo,$filtro);
									}else{
										$resultado=0;
									}
									$this->CrearTabla($resultado,$valorIdEmbudo,$valorBusqueda,$filtro);  
								}
								/*Metodo para reordenar las etapas de la vista de embudos de configuracion*/
								public function CrearTabla($resultado,$valorIdEmbudo,$valorBusqueda,$filtro){
									echo '<div style="overflow-x: auto;">
									<table class="table table-bordered" id="miTablaPersonalizada" style="margin-bottom: 0px;">
									<tr style="background-color: #D0D3D4;">
									';date_default_timezone_set("America/Mexico_City");
									$FechaAc= date("Y-m-d");
									$error=0;
									$contadorrr=0;
									foreach($this->model->ListarEtapas($valorIdEmbudo) as $r):
										/*Se imprime el nombre de la etapa y el contenido de la etapa*/
										$A=$this->model->ProbabilidadEtapa($r->idEtapa);
										$valNe=$this->model->ValoresNeg($r->idEtapa,$filtro,$valorBusqueda);
										$resultadoF = count($valNe);
										$con=0;
										$suma=0;
										echo '<td>
										<div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
										<strong>'.$r->nombreEtapa.'</strong>
										</div>
										<div class="col-lg-12 hola'.$contadorrr.'" style="padding-left: 0px; padding-right: 0px;">
										</div></td>';

										if ($A!=100) {
											echo '<style>
											.hola'.$contadorrr.':before {
												content:"'; foreach ($valNe as $key) {
													if (!isset($con)) {
													}else{
														if ($key->tipoMoneda=='USD') {
															$valDol=$this->model->ConsultaValorDolar();
															$newV=$key->valorNegocio*$valDol;
															$suma=$suma+$newV;
														}else{
															$suma=$suma+$key->valorNegocio;
														}
													}
													$con++;
												}
												if ($suma==0) {  
												}else{
													$Res=$A*$suma;
													$Res=$Res/100;
													echo("$".number_format($Res, 2, '.', ',')." MXN ");
												}
												if ($resultadoF==0) {
												}else{
													if ($resultadoF==1) {
														echo($resultadoF." negocio ");
													}else{
														echo($resultadoF." negocios ");
													}
												} 
												echo '"; 
												padding:0 auto;
												font-size:12px; 
											}';
										}else{
											echo '<style>
											.hola'.$contadorrr.':before {
												content:"'; foreach ($valNe as $key) {
													if (!isset($con)) {
													}else{
														if ($key->tipoMoneda=='USD') {
															$valDol=$this->model->ConsultaValorDolar();
															$newV=$key->valorNegocio*$valDol;
															$suma=$suma+$newV;
														}else{
															$suma=$suma+$key->valorNegocio;
														}
													}
													$con++;
												}
												if ($suma==0) {
												}else{
													echo("$".number_format($suma, 2, '.', ',')." MXN ");
												}
												if ($resultadoF==0) {
												}else{
													if ($resultadoF==1) {
														echo($resultadoF." negocio ");
													}else{
														echo($resultadoF." negocios ");
													}
												} 
												echo '"; 
												padding:0 auto;
												font-size:12px; 
											}';
										}

										if ($A!=100 and $resultadoF!=0) {
											echo '.hola'.$contadorrr.':hover:before {
												content:"'; echo $A."% de "; 
												if ($suma==0) {
												}else{
													echo("$".number_format($suma, 2, '.', ',')." MXN ");
													}echo '";  
													padding:0 auto;
													font-size:12px;
												}

												</style>';
											}
											else{
												echo '</style>';
											}
											$contadorrr++;
										endforeach;

										echo '</tr>
										<tr style="background-color: #FFF;">';
										$i=0; foreach($this->model->ListarEtapas($valorIdEmbudo) as $r):
										echo'<td style="padding: 1px;">
										<ul id="sortable'.++$i.'" class="connectedSortable sortable">
										<input type="hidden" name="" id="id';echo $r->idEtapa; $array[] = $r->idEtapa; echo '" value="'.$r->idEtapa.'" >';
										if ($resultado==0) {
											$porciones = explode(",", $r->contenido);
											$A= count($porciones);
										}elseif ($resultado){
											$pila = array();
											foreach ($resultado as $key) {
												foreach ($key as $val ) {
													array_push($pila,$val);
												}
											}
											$porciones = array_unique($pila);
											$A= count($porciones);
										}else{
											$Algo="0,0";
											$porciones = explode(",", $Algo);
											$A= count($porciones);
											if ($error==0) {

											}
											$error++;
										}
										$A= count($porciones);
										if($porciones[0]==0) {
										}else{
											foreach ($porciones as $n):
												$B=$this->model->ConsultaNegociosEmbudo($_SESSION['idEmbudo'],$r->idEtapa,$n,$filtro);
												foreach ($B as $A):
													$Actividad=$this->model->FechaActividadNegocio($A->idNegocio);
													$hoy = date("Y-m-d H:i:s");  
													$datetime1 = date_create($hoy);
													$datetime2 = date_create($A->contadorActivo);
													$interval = date_diff($datetime2, $datetime1);
													if (!isset($Actividad))  {
														echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
														if ($filtro==1) {
															echo 'list-group-item-success'; 
														}elseif ($filtro==2) {
														}else{
															if ($r->inactividad==0) {       
															}elseif ($interval->format('%a')==($r->inactividad)-1) {
																echo 'list-group-item-warning';         
															}elseif($interval->format('%a')<$r->inactividad){  
															}
															elseif($interval->format('%a')>=$r->inactividad) {
																echo 'list-group-item-danger estancado';        
															}
														}
														echo '">';
														echo '
														<div class="row">
														<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
														<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
														if (!empty($A->claveOrganizacion))
															echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
														else
															echo 'Leads';
														echo '</a>
														</div>
														<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
														<a class="btn btn-lg" data-toggle="modal" data-target="#añadiractividad" onclick="myFunctionAñadirActividad(';?> <?php echo $A->idOrganizacion;?>,<?php echo "'".$A->nombreOrganizacion."'"; ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio?>,<?php echo "'".$A->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-warning-sign yellow"></span></a>
														</div>
														<br>
														<div class="col-lg-12">
														<div class="row">
														<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
														</p>
														</div>
														</div>
														<div class="col-xs-12 col-lg-12" align="center">';
														if ($filtro==1) {
															echo '<span class="badge">GANADO</span > ';
														}elseif ($filtro==2) {
															echo '<span class="badge">PERDIDO</span > ';
														}else{
															if ($r->inactividad==0) {
															}
															elseif ($interval->format('%a')>=$r->inactividad) {

																echo '<span class="badge">';
																echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
																echo '</span >';
															}
														}
														echo '</div>
														</div>
														</div>
														</li>';
													}elseif ($FechaAc>$Actividad) { 
														$Estado=$this->model->EstadoActividad($A->idNegocio);
														if ($Estado==0) {
															echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
															if ($filtro==1) {
																echo 'list-group-item-success'; 
															}elseif ($filtro==2) {
															}else{
																if ($r->inactividad==0) {       
																}elseif ($interval->format('%a')==($r->inactividad)-1) {
																	echo 'list-group-item-warning';         
																}elseif($interval->format('%a')<$r->inactividad){  
																}
																elseif($interval->format('%a')>=$r->inactividad) {
																	echo 'list-group-item-danger estancado';        
																}
															}
															echo '">';
															echo '
															<div class="row">
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
															<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
															if (!empty($A->claveOrganizacion))
																echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
															else
																echo 'Leads';
															echo '</a>
															</div>
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
															<a class="btn btn-lg" style="padding: 1px; white-space: pre-wrap; text-align: justify;" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?><?php echo $A->idOrganizacion; ?>,<?php echo "'".$A->nombreOrganizacion."'" ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio ?>,<?php echo "'".$A->tituloNegocio."'" ?><?php echo ')"><span class="glyphicon glyphicon-play-circle red"></span></a>
															</div>
															<br>
															<div class="col-lg-12">
															<div class="row">
															<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
															</p>
															</div>
															</div>
															<div class="col-xs-12 col-lg-12" align="center">';
															if ($filtro==1) {
																echo '<span class="badge">GANADO</span > ';
															}elseif ($filtro==2) {
																echo '<span class="badge">PERDIDO</span > ';
															}else{
																if ($r->inactividad==0) {
																}
																elseif ($interval->format('%a')>=$r->inactividad) {

																	echo '<span class="badge">';
																	echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
																	echo '</span >';
																}
															}
															echo '</div>
															</div>
															</div>
															</li>';
														}else{
															echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
															if ($filtro==1) {
																echo 'list-group-item-success'; 
															}elseif ($filtro==2) {
															}else{
																if ($r->inactividad==0) {       
																}elseif ($interval->format('%a')==($r->inactividad)-1) {
																	echo 'list-group-item-warning';         
																}elseif($interval->format('%a')<$r->inactividad){  
																}
																elseif($interval->format('%a')>=$r->inactividad) {
																	echo 'list-group-item-danger estancado';        
																}
															}
															echo '">';
															echo '
															<div class="row">
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
															<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
															if (!empty($A->claveOrganizacion))
																echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
															else
																echo 'Leads';
															echo '</a>
															</div>
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
															<a class="btn btn-lg" data-toggle="modal" data-target="#añadiractividad" onclick="myFunctionAñadirActividad(';?> <?php echo $A->idOrganizacion;?>,<?php echo "'".$A->nombreOrganizacion."'"; ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio?>,<?php echo "'".$A->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-warning-sign yellow"></span></a>
															</div>
															<br>
															<div class="col-lg-12">
															<div class="row">
															<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
															</p>
															</div>
															</div>
															<div class="col-xs-12 col-lg-12" align="center">';
															if ($filtro==1) {
																echo '<span class="badge">GANADO</span > ';
															}elseif ($filtro==2) {
																echo '<span class="badge">PERDIDO</span > ';
															}else{
																if ($r->inactividad==0) {
																}
																elseif ($interval->format('%a')>=$r->inactividad) {

																	echo '<span class="badge">';
																	echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
																	echo '</span >';
																}
															}
															echo '</div>
															</div>
															</div>
															</li>';
														}
													}elseif ($FechaAc<=$Actividad) { 
														if ($FechaAc==$Actividad) {
															echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
															if ($filtro==1) {
																echo 'list-group-item-success'; 
															}elseif ($filtro==2) {
															}else{
																if ($r->inactividad==0) {       
																}elseif ($interval->format('%a')==($r->inactividad)-1) {
																	echo 'list-group-item-warning';         
																}elseif($interval->format('%a')<$r->inactividad){  
																}
																elseif($interval->format('%a')>=$r->inactividad) {
																	echo 'list-group-item-danger estancado';        
																}
															}
															echo '">';
															echo '
															<div class="row">
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
															<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
															if (!empty($A->claveOrganizacion))
																echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
															else
																echo 'Leads';
															echo '</a>
															</div>
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
															<a class="btn btn-lg" style="padding: 1px; white-space: pre-wrap; text-align: justify;" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?><?php echo $A->idOrganizacion; ?>,<?php echo "'".$A->nombreOrganizacion."'" ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio ?>,<?php echo "'".$A->tituloNegocio."'" ?><?php echo ')"><span class="glyphicon glyphicon-play-circle green"></span></a>
															</div>
															<br>
															<div class="col-lg-12">
															<div class="row">
															<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
															</p>
															</div>
															</div>
															<div class="col-xs-12 col-lg-12" align="center">';
															if ($filtro==1) {
																echo '<span class="badge">GANADO</span > ';
															}elseif ($filtro==2) {
																echo '<span class="badge">PERDIDO</span > ';
															}else{
																if ($r->inactividad==0) {
																}
																elseif ($interval->format('%a')>=$r->inactividad) {

																	echo '<span class="badge">';
																	echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
																	echo '</span >';
																}
															}
															echo '</div>
															</div>
															</div>
															</li>';
														}else{
															echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
															if ($filtro==1) {
																echo 'list-group-item-success'; 
															}elseif ($filtro==2) {
															}else{
																if ($r->inactividad==0) {       
																}elseif ($interval->format('%a')==($r->inactividad)-1) {
																	echo 'list-group-item-warning';         
																}elseif($interval->format('%a')<$r->inactividad){  
																}
																elseif($interval->format('%a')>=$r->inactividad) {
																	echo 'list-group-item-danger estancado';        
																}
															}
															echo '">';
															echo '
															<div class="row">
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
															<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
															if (!empty($A->claveOrganizacion))
																echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
															else
																echo 'Leads';
															echo '</a>
															</div>
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
															<a class="btn btn-lg" style="padding: 1px; white-space: pre-wrap; text-align: justify;" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?><?php echo $A->idOrganizacion; ?>,<?php echo "'".$A->nombreOrganizacion."'" ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio ?>,<?php echo "'".$A->tituloNegocio."'" ?><?php echo ')"><span class="glyphicon glyphicon-play-circle silver"></span></a>
															</div>
															<br>
															<div class="col-lg-12">
															<div class="row">
															<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
															</p>
															</div>
															</div>
															<div class="col-xs-12 col-lg-12" align="center">';
															if ($filtro==1) {
																echo '<span class="badge">GANADO</span > ';
															}elseif ($filtro==2) {
																echo '<span class="badge">PERDIDO</span > ';
															}else{
																if ($r->inactividad==0) {
																}
																elseif ($interval->format('%a')>=$r->inactividad) {

																	echo '<span class="badge">';
																	echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
																	echo '</span >';
																}
															}
															echo '</div>
															</div>
															</div>
															</li>';
														}
													} 
												endforeach;
											endforeach;
										} 
										echo '</ul>
										</td>';
									endforeach;
									echo '</tr>';

									if ($resultado==0) {
										$valor=$this->model->VerificarContenido($valorIdEmbudo);
										if (empty($valor)) {
											$true=true;
											$contador=$this->modelEmbudo->ContadorEtapas($valorIdEmbudo);
											echo '<tr><td class="alert-danger" colspan="'.$contador.'" align="center"> <strong>No se encontraron negocios </strong></td></tr>';
										}
									}elseif ($resultado){
									}else{
										$true=true;
										$contador=$this->modelEmbudo->ContadorEtapas($valorIdEmbudo);
										echo '<tr><td class="alert-danger" colspan="'.$contador.'" align="center"> <strong>No se encontraron negocios</strong></td></tr>';
									}
									if (!isset($true)) {
										$contadorNegocios=$this->model->Contador($valorIdEmbudo, $valorBusqueda, $filtro);
										if ($contadorNegocios==0) {
											$contador=$this->modelEmbudo->ContadorEtapas($valorIdEmbudo);

										}
									}


									echo '</table>
									</div>
									<script>
									$( function() {
										$( "'; for ($a=1; $a<=$i; $a++) {
											if ($a == $i) {
												echo("#sortable".$a);
											}else{
												echo("#sortable".$a.",");
											}
											} echo '"  ).sortable({
												placeholder: "placeholder",
												cursor: "move",
												opacity: 0.7, helper: "clone", 
												cursorAt: { top: 20, left: 56 }, 
												revert: true,
												connectWith: ".connectedSortable",
												update: function(event,ui) {
													'; for ($a=1; $a<=$i; $a++) {           
														echo 'var postDataA';echo($a);echo'=$("#sortable';echo($a); echo '").sortable("serialize");';
													} 
													foreach($array as $r):
														echo 'var id'.$r.'=$("#id'.$r.'").val();';
													endforeach; 
  echo 'var array = [';foreach($array as $r): echo($r.","); endforeach; echo '];';//--> me llevo en conjunto todos los ids de las etapas de de venta en forma de json

  echo 'var conjunto=['; for ($a=1; $a<=$i; $a++) { if ($a==$i) {echo("postDataA".$a); }else echo("postDataA".$a.","); } echo '];';//--> me llevo en conjunto todos los ids de los negocios en forma de json
  if ($resultado) {
  	echo 'var busqueda=5;';
  }else{
  	echo 'var busqueda=0;';
  }
  echo 'var valorBusqueda="'.$valorBusqueda.'";';
  echo 'var filtro="'.$filtro.'";';
  echo '$.post("index.php?c=negocios&a=DesplazarNegocios",{conjunto:JSON.stringify(conjunto),numlistas:JSON.stringify(array),busqueda:busqueda,valorBusqueda:valorBusqueda,filtro:filtro},function(mensaje) {
  	$("#resultBusquedaEmbudo").html(mensaje);
  	Contadores();
  	});
  }
  });
  } );
  </script>';
}


				public function Eliminar(){
					try
					{
						$idNegocio=$_REQUEST['idNegocio'];
						$Res=$this->model->NegocioDetalles($idNegocio);
						$ArregloDeNegociosAntiguo=$this->modelEmbudo->ArregloDeNegociosAntiguo($Res['idEtapa']);
						$ArregloDeNegociosAntiguo = explode(",", $ArregloDeNegociosAntiguo);
						$ArregloDeNegociosAntiguo = array_diff($ArregloDeNegociosAntiguo, array($idNegocio));
						$ArregloDeNegociosAntiguo=implode(',',$ArregloDeNegociosAntiguo);
						$this->modelEmbudo->ReordenarContenido($_SESSION['idEmbudo'],$Res['idEtapa'],$ArregloDeNegociosAntiguo);
						$this->model->Eliminar($idNegocio);
						echo "Se elimino correctamente el negocio";
					}
					catch(Exception $e)
					{
						$this->error=true;
						echo "Se ha producido un error al eliminar el negocio";
					} 
				}

				public function tabsEmbudo(){
					$valorIdEmbudo=$_REQUEST['valorIdEmbudo'];
					if ($valorIdEmbudo == "") {
						$valorIdEmbudo = 0;
					}
					$resultado = $this->modelEmbudo->Listar();
					echo '
					<br>
					<div class="row"> 
					<div class="panel panel-default"> 
					<div class="panel-heading"> 
					<ul class="nav nav-pills nav-justified">';
					foreach ($resultado as $r) :
						$ContadorEtapas = $this->modelEmbudo->ContadorEtapas( "'".$r->idEmbudo."'" );
						if ($r->idEmbudo == $valorIdEmbudo){
							echo '
							<li role="presentation" class="active"><a data-toggle="tab" href="#'. $r->idEmbudo .'"> '. $r->nombre .' <span class="badge">'.$ContadorEtapas.'</span></a></li>';
						}else {
							echo '
							<li role="presentation"><a data-toggle="tab" href="#'.$r->idEmbudo.'"> '.$r->nombre.' <span class="badge">'.$ContadorEtapas.'</span></a></li>';
						}
					endforeach;
					echo '</ul>
					</div> 
					<div class="panel-body"> 
					<div class="tab-content">';  
					foreach ($resultado as $e) :
						if ($e->idEmbudo == $valorIdEmbudo){
							echo '
							<div id="'.$e->idEmbudo.'" class="tab-pane fade in active">';
						}else { 
							echo '
							<div id="'.$e->idEmbudo.'" class="tab-pane fade">'; 
						}
						echo '
						<div class="container-fluid"> 
						<div class="col-xs-7 col-lg-10"> 
						<div class="col-xs-12 col-lg-6"> 
						<h3>'.$e->nombre.'</h3>
						</div> 
						<div class="col-lg-4">
						<br>
						<div class="btn-group"> 
						<input type="hidden" name="hideIdEmbudo" id="hideIdEmbudo" value="'.$e->idEmbudo.'">
						<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionEditar('; ?><?php echo "'".$e->idEmbudo."'"; ?>,<?php echo "'".$e->nombre."'"; ?><?php echo ')">Editar</a>
						</div> 
						<div class="btn-group"> 
						<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mEliminar" onclick="myFunctionEliminar('.$e->idEmbudo.')" >Eliminar</a>
						</div> 
						</div>	
						</div> 
						<div class="col-xs-5 col-lg-2" align="right"> 
						<br>
						<a href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#mEtapa" onclick="myFunctionNuevoEtapa('.$e->idEmbudo.');">Añadir etapa</a>
						</div> 
						</div> 

						<div class="jumbotron horizontal"> 
						<ul class="sortable menu">';
						foreach ($this->modelEmbudo->ListarEtapas("'".$e->idEmbudo."'") as $r) :
							echo '
							<li class="list-group-item item_embudo caja" id="'.$r->nombreEtapa.'-'.$r->idEtapa.'"><h5><a href="" data-toggle="modal" data-target="#mEtapa" onclick="myFunctionEditarEtapa('; ?><?php echo "'".$e->idEmbudo."'"; ?>,<?php echo "'".$r->idEtapa."'"; ?>,<?php echo "'".$r->nombreEtapa."'"; ?>,<?php echo "'".$r->probabilidad."'"; ?>,<?php echo "'".$r->inactividad."'"; ?><?php echo ');">'.$r->nombreEtapa.'</a></h5>';
							if ($r->inactividad == 1){
								echo'<font size="1" class="help-block">'.$r->probabilidad.'% de probabilidad <br> Estancado en '.$r->inactividad.' día </font>';
							} else if($r->inactividad > 1){
								echo '<font size="1" class="help-block">'.$r->probabilidad.'% de probabilidad <br> Estancado en '.$r->inactividad.' días </font>';
							}else{
								echo '<font size="1" class="help-block">'.$r->probabilidad.' % de probabilidad </font>';
							}
							echo '
							</li>';
						endforeach;
						echo '
						</ul>
						</div> 
						</div>'; 
					endforeach;
					echo '
					</div> 
					</div> 
					</div> 
					</div> 
					<script>
					$(function() {
						$( ".sortable" ).sortable({
							update: function(event, ui) {
								$(".ajax-loader").show();
								var orden = $(this).sortable("toArray").toString();
								$.ajax({
									url: "index.php?c=negocios&a=Reordenar",
									type: "POST",
									data: {"data": orden}
									}).done(function(data) {

										});
										$(".ajax-loader").hide();
									}
									});
									$( ".sortable" ).disableSelection();
									});
									</script>';
								}
								
//Metodo para validar lo que llega por ajax para depues mandarlo al modelo negocios para asi guradar el nuevo acomodo de los negocios
	public function DesplazarNegocios(){
    $ids=json_decode($_REQUEST['numlistas']); //-->aqui llega el conjunto de ids del todas las etapas en forma de array
    $conjunto=json_decode($_REQUEST['conjunto']);//-->aqui llega el conjunto de ids del todos los negocios super importante poner la siguente estructura item_135 en el id del objeto que se despalasara en nuestro caso es un <li id="item_135" donde 135 es el id del negocio, de tal manera que llegara con la siguente estructura item[]=135 llega en forma de array en conjunto llegan todos los ids de los negocios separados en forma de array separados por posiscions ejemplo 0->1,2,3 1->5,6,8 
    $busqueda=$_REQUEST['busqueda'];
    $valorIdEmbudo=$_SESSION['idEmbudo'];
    $filtro=$_REQUEST['filtro'];

    if ($busqueda==5) {
    	$valorBusqueda=$_REQUEST['valorBusqueda'];
    	$resultado=$this->model->ConsultaPorValor($valorBusqueda,$valorIdEmbudo,$filtro);
    }else{
    	$valorBusqueda="";
    	$resultado=0;
    }

    $cont=0;//-->contador para que el arreglo $conjunto pueda moverse de etapa o de posicion 
    foreach ($ids as $id) {//-->se crea un ciclo para obtener cada id de etapa por separado cada vuelta es por cada etapa de venta 
      $output=array();//--> se crea un arreglo vacio
      $list1=parse_str($conjunto[$cont],$output);//-->parse_str — Convierte el string en variables en $output se almacenan los ids de los negocios 
      if (empty($output)) {
        //-->si el arreglo esta vacio o mas bien si no hay nada en la etapa de venta no se hace nada
      }else{
        $a=implode(',',$output['item']);//--> implode Une elementos de un array en un string eje 1,2,3,4
        $porciones = explode(",", $a);//-->Divide un string en varios string ahora $porciones tiene los ids de la etapa
        foreach ($porciones as $n): //--> $n es el id del negocio aqui se logra obtener por separado
          $B=$this->model->ConsultaNegociosEmbudo($_SESSION['idEmbudo'],$id,$n,$filtro);//-->ojo tu al momento de desplazar un negocio a otra etapa habra una respuesta negativa en una consulta donde $n=idNegocio no corresponde a $id que es la nueva etapa donde se coloco este negocio por ende etrara al siguente if ya localizado la nueva etapa y el id del negocio
          if (empty($B)) {
           $idOldEtapa=$this->model->VerificarIdEtapa($n);//--> se obtiene el atiguo id de la etapa del negocio 
           //echo "El antiguo id de la etapa es ".$idOldEtapa."<br>";
           //echo "El nuevo id de la etapa es ".$id."<br>";
           //echo "El id del negocio es ".$n."<br>";

           //Aqui me triago el contenido donde se encontraba el negocio para eliminar el id del negocio con array_diff una ves eliminado actualizo esa etapa del negocio
           $ArregloDeNegociosAntiguo=$this->modelEmbudo->ArregloDeNegociosAntiguo($idOldEtapa);
           $ArregloDeNegociosAntiguo = explode(",", $ArregloDeNegociosAntiguo);
           $ArregloDeNegociosAntiguo = array_diff($ArregloDeNegociosAntiguo, array($n));
           $ArregloDeNegociosAntiguo=implode(',',$ArregloDeNegociosAntiguo);
           $this->modelEmbudo->ReordenarContenido($_SESSION['idEmbudo'],$idOldEtapa,$ArregloDeNegociosAntiguo);

          //Aqui me triago el contenido donde se concatenara el negocio o sea su nueva posicion o ubicacion
           $CadenaDeNegociosNuevo=$this->modelEmbudo->CadenaDeNegociosNuevo($id);
           if ($CadenaDeNegociosNuevo==null) {
           	$CadenaDeNegociosNuevo=($n);
           }else{
           	$CadenaDeNegociosNuevo=($CadenaDeNegociosNuevo.",".$n);
           }
           $this->modelEmbudo->ReordenarContenido($_SESSION['idEmbudo'],$id,$CadenaDeNegociosNuevo);
           $this->model->ActualizarEtapaNegocio($id,$n);//-->Actualiza la etapa del negocio
           $this->model->ActualizarContadorActivo($n);//--> se actualiza el contador para el cambio de color del negocio
           //Metodos sin importancia para la opcion sortable
           $accion="Etapa";
           $campo1=$this->modelDeal->NombreEtapa($idOldEtapa);
           $campo2=$this->modelDeal->NombreEtapa($id);
           date_default_timezone_set("America/Mexico_City");
           $FechaAc= date("Y-m-d H:i:s");
           $this->modelDeal->RegistroCambios($accion,$campo1,$campo2,$FechaAc,$n,$_SESSION['idUsuario']);
       }       
   endforeach;
}
++$cont;
}


echo '<div style="overflow-x: auto;">
<table class="table table-bordered" id="miTablaPersonalizada" style="margin-bottom: 0px;">
<tr style="background-color: #D0D3D4;">
';date_default_timezone_set("America/Mexico_City");
$FechaAc= date("Y-m-d");
$error=0;
$contadorrr=0;
foreach($this->model->ListarEtapas($valorIdEmbudo) as $r):
    //---Se imprime el nombre de la etapa y el contenido de la etapa---//
	$A=$this->model->ProbabilidadEtapa($r->idEtapa);
	$valNe=$this->model->ValoresNeg($r->idEtapa,$filtro,$valorBusqueda);
	$resultadoF = count($valNe);
	$con=0;
	$suma=0;
	echo '<td>
	<div class="col-lg-12" style="padding-left: 0px; padding-right: 0px;">
	<strong>'.$r->nombreEtapa.'</strong>
	</div>
	<div class="col-lg-12 hola'.$contadorrr.'" style="padding-left: 0px; padding-right: 0px;">
	</div></td>';

	if ($A!=100) {
		echo '<style>
		.hola'.$contadorrr.':before {
			content:"'; foreach ($valNe as $key) {
				if (!isset($con)) {
				}else{
					if ($key->tipoMoneda=='USD') {
						$valDol=$this->model->ConsultaValorDolar();
						$newV=$key->valorNegocio*$valDol;
						$suma=$suma+$newV;
					}else{
						$suma=$suma+$key->valorNegocio;
					}
				}
				$con++;
			}
			if ($suma==0) {  
			}else{
				$Res=$A*$suma;
				$Res=$Res/100;
				echo("$".number_format($Res, 2, '.', ',')." MXN ");
			}
			if ($resultadoF==0) {
			}else{
				if ($resultadoF==1) {
					echo($resultadoF." negocio ");
				}else{
					echo($resultadoF." negocios ");
				}
			} 
			echo '"; 
			padding:0 auto;
			font-size:12px; 
		}';
	}else{
		echo '<style>
		.hola'.$contadorrr.':before {
			content:"'; foreach ($valNe as $key) {
				if (!isset($con)) {
				}else{
					if ($key->tipoMoneda=='USD') {
						$valDol=$this->model->ConsultaValorDolar();
						$newV=$key->valorNegocio*$valDol;
						$suma=$suma+$newV;
					}else{
						$suma=$suma+$key->valorNegocio;
					}
				}
				$con++;
			}
			if ($suma==0) {
			}else{
				echo("$".number_format($suma, 2, '.', ',')." MXN ");
			}
			if ($resultadoF==0) {
			}else{
				if ($resultadoF==1) {
					echo($resultadoF." negocio ");
				}else{
					echo($resultadoF." negocios ");
				}
			} 
			echo '"; 
			padding:0 auto;
			font-size:12px; 
		}';
	}

	if ($A!=100 and $resultadoF!=0) {
		echo '.hola'.$contadorrr.':hover:before {
			content:"'; echo $A."% de "; 
			if ($suma==0) {
			}else{
				echo("$".number_format($suma, 2, '.', ',')." MXN ");
				}echo '";  
				padding:0 auto;
				font-size:12px;
			}

			</style>';
		}
		else{
			echo '</style>';
		}
		$contadorrr++;
	endforeach;

	echo '</tr>
	<tr style="background-color: #FFF;">';
	$i=0; foreach($this->model->ListarEtapas($valorIdEmbudo) as $r):
	echo'<td style="padding: 1px;">
	<ul id="sortable'.++$i.'" class="connectedSortable sortable">
	<input type="hidden" name="" id="id';echo $r->idEtapa; $array[] = $r->idEtapa; echo '" value="'.$r->idEtapa.'" >';
	if ($resultado==0) {
		$porciones = explode(",", $r->contenido);
		$A= count($porciones);
	}elseif ($resultado){
		$pila = array();
		foreach ($resultado as $key) {
			foreach ($key as $val ) {
				array_push($pila,$val);
			}
		}
		$porciones = array_unique($pila);
		$A= count($porciones);
	}else{
		$Algo="0,0";
		$porciones = explode(",", $Algo);
		$A= count($porciones);
		if ($error==0) {

		}
		$error++;
	}
	$A= count($porciones);
	if($porciones[0]==0) {
	}else{
		foreach ($porciones as $n):
			$B=$this->model->ConsultaNegociosEmbudo($_SESSION['idEmbudo'],$r->idEtapa,$n,$filtro);
			foreach ($B as $A):
				$Actividad=$this->model->FechaActividadNegocio($A->idNegocio);
				$hoy = date("Y-m-d H:i:s");  
				$datetime1 = date_create($hoy);
				$datetime2 = date_create($A->contadorActivo);
      $interval = date_diff($datetime2, $datetime1);//--> sACAR DIFEREBCIAS DE HORAS MESES ETC........
      if (!isset($Actividad))  {
      	echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
      	if ($filtro==1) {
      		echo 'list-group-item-success'; 
      	}elseif ($filtro==2) {
      	}else{
      		if ($r->inactividad==0) {       
      		}elseif ($interval->format('%a')==($r->inactividad)-1) {
      			echo 'list-group-item-warning';         
      		}elseif($interval->format('%a')<$r->inactividad){  
      		}
      		elseif($interval->format('%a')>=$r->inactividad) {
      			echo 'list-group-item-danger estancado';        
      		}
      	}
      	echo '">';
      	echo '
      	<div class="row">
      	<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
      	<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
      	if (!empty($A->claveOrganizacion))
      		echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
      	else
      		echo 'Leads';
      	echo '</a>
      	</div>
      	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
      	<a class="btn btn-lg" data-toggle="modal" data-target="#añadiractividad" onclick="myFunctionAñadirActividad(';?> <?php echo $A->idOrganizacion;?>,<?php echo "'".$A->nombreOrganizacion."'"; ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio?>,<?php echo "'".$A->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-warning-sign yellow"></span></a>
      	</div>
      	<br>
      	<div class="col-lg-12">
      	<div class="row">
      	<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
      	</p>
      	</div>
      	</div>
      	<div class="col-xs-12 col-lg-12" align="center">';
      	if ($filtro==1) {
      		echo '<span class="badge">GANADO</span > ';
      	}elseif ($filtro==2) {
      		echo '<span class="badge">PERDIDO</span > ';
      	}else{
      		if ($r->inactividad==0) {
      		}
      		elseif ($interval->format('%a')>=$r->inactividad) {

      			echo '<span class="badge">';
      			echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
      			echo '</span >';
      		}
      	}
      	echo '</div>
      	</div>
      	</div>
      	</li>';
      }elseif ($FechaAc>$Actividad) { 
      	$Estado=$this->model->EstadoActividad($A->idNegocio);
      	if ($Estado==0) {
      		echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
      		if ($filtro==1) {
      			echo 'list-group-item-success'; 
      		}elseif ($filtro==2) {
      		}else{
      			if ($r->inactividad==0) {       
      			}elseif ($interval->format('%a')==($r->inactividad)-1) {
      				echo 'list-group-item-warning';         
      			}elseif($interval->format('%a')<$r->inactividad){  
      			}
      			elseif($interval->format('%a')>=$r->inactividad) {
      				echo 'list-group-item-danger estancado';        
      			}
      		}
      		echo '">';
      		echo '
      		<div class="row">
      		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
      		<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
      		if (!empty($A->claveOrganizacion))
      			echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
      		else
      			echo 'Leads';
      		echo '</a>
      		</div>
      		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
      		<a class="btn btn-lg" style="padding: 1px; white-space: pre-wrap; text-align: justify;" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?><?php echo $A->idOrganizacion; ?>,<?php echo "'".$A->nombreOrganizacion."'" ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio ?>,<?php echo "'".$A->tituloNegocio."'" ?><?php echo ')"><span class="glyphicon glyphicon-play-circle red"></span></a>
      		</div>
      		<br>
      		<div class="col-lg-12">
      		<div class="row">
      		<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
      		</p>
      		</div>
      		</div>
      		<div class="col-xs-12 col-lg-12" align="center">';
      		if ($filtro==1) {
      			echo '<span class="badge">GANADO</span > ';
      		}elseif ($filtro==2) {
      			echo '<span class="badge">PERDIDO</span > ';
      		}else{
      			if ($r->inactividad==0) {
      			}
      			elseif ($interval->format('%a')>=$r->inactividad) {

      				echo '<span class="badge">';
      				echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
      				echo '</span >';
      			}
      		}
      		echo '</div>
      		</div>
      		</div>
      		</li>';
      	}else{
      		echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
      		if ($filtro==1) {
      			echo 'list-group-item-success'; 
      		}elseif ($filtro==2) {
      		}else{
      			if ($r->inactividad==0) {       
      			}elseif ($interval->format('%a')==($r->inactividad)-1) {
      				echo 'list-group-item-warning';         
      			}elseif($interval->format('%a')<$r->inactividad){  
      			}
      			elseif($interval->format('%a')>=$r->inactividad) {
      				echo 'list-group-item-danger estancado';        
      			}
      		}
      		echo '">';
      		echo '
      		<div class="row">
      		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
      		<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
      		if (!empty($A->claveOrganizacion))
      			echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
      		else
      			echo 'Leads';
      		echo '</a>
      		</div>
      		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
      		<a class="btn btn-lg" data-toggle="modal" data-target="#añadiractividad" onclick="myFunctionAñadirActividad(';?> <?php echo $A->idOrganizacion;?>,<?php echo "'".$A->nombreOrganizacion."'"; ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio?>,<?php echo "'".$A->tituloNegocio."'"?><?php echo')" style="padding: 1px; white-space: pre-wrap; text-align: justify;"><span class="glyphicon glyphicon-warning-sign yellow"></span></a>
      		</div>
      		<br>
      		<div class="col-lg-12">
      		<div class="row">
      		<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
      		</p>
      		</div>
      		</div>
      		<div class="col-xs-12 col-lg-12" align="center">';
      		if ($filtro==1) {
      			echo '<span class="badge">GANADO</span > ';
      		}elseif ($filtro==2) {
      			echo '<span class="badge">PERDIDO</span > ';
      		}else{
      			if ($r->inactividad==0) {
      			}
      			elseif ($interval->format('%a')>=$r->inactividad) {

      				echo '<span class="badge">';
      				echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
      				echo '</span >';
      			}
      		}
      		echo '</div>
      		</div>
      		</div>
      		</li>';
      	}
      }elseif ($FechaAc<=$Actividad) { 
      	if ($FechaAc==$Actividad) {
      		echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
      		if ($filtro==1) {
      			echo 'list-group-item-success'; 
      		}elseif ($filtro==2) {
      		}else{
      			if ($r->inactividad==0) {       
      			}elseif ($interval->format('%a')==($r->inactividad)-1) {
      				echo 'list-group-item-warning';         
      			}elseif($interval->format('%a')<$r->inactividad){  
      			}
      			elseif($interval->format('%a')>=$r->inactividad) {
      				echo 'list-group-item-danger estancado';        
      			}
      		}
      		echo '">';
      		echo '
      		<div class="row">
      		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
      		<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
      		if (!empty($A->claveOrganizacion))
      			echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
      		else
      			echo 'Leads';
      		echo '</a>
      		</div>
      		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
      		<a class="btn btn-lg" style="padding: 1px; white-space: pre-wrap; text-align: justify;" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?><?php echo $A->idOrganizacion; ?>,<?php echo "'".$A->nombreOrganizacion."'" ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio ?>,<?php echo "'".$A->tituloNegocio."'" ?><?php echo ')"><span class="glyphicon glyphicon-play-circle green"></span></a>
      		</div>
      		<br>
      		<div class="col-lg-12">
      		<div class="row">
      		<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
      		</p>
      		</div>
      		</div>
      		<div class="col-xs-12 col-lg-12" align="center">';
      		if ($filtro==1) {
      			echo '<span class="badge">GANADO</span > ';
      		}elseif ($filtro==2) {
      			echo '<span class="badge">PERDIDO</span > ';
      		}else{
      			if ($r->inactividad==0) {
      			}
      			elseif ($interval->format('%a')>=$r->inactividad) {

      				echo '<span class="badge">';
      				echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
      				echo '</span >';
      			}
      		}
      		echo '</div>
      		</div>
      		</div>
      		</li>';
      	}else{
      		echo '<li id="item_'.$A->idNegocio.'" class="list-group-item item_embudo '; 
      		if ($filtro==1) {
      			echo 'list-group-item-success'; 
      		}elseif ($filtro==2) {
      		}else{
      			if ($r->inactividad==0) {       
      			}elseif ($interval->format('%a')==($r->inactividad)-1) {
      				echo 'list-group-item-warning';         
      			}elseif($interval->format('%a')<$r->inactividad){  
      			}
      			elseif($interval->format('%a')>=$r->inactividad) {
      				echo 'list-group-item-danger estancado';        
      			}
      		}
      		echo '">';
      		echo '
      		<div class="row">
      		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="padding-left: 0px; padding-right: 1px;">
      		<a class="btn" href="?c=deal&idNegocio='; echo $A->idNegocio;  echo '" style="padding: 1px;">';
      		if (!empty($A->claveOrganizacion))
      			echo $A->claveOrganizacion.'-'.$A->claveConsecutivo.'-'.$A->claveServicio;
      		else
      			echo 'Leads';
      		echo '</a>
      		</div>
      		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right" style="padding-left: 1px; padding-right: 10px;">
      		<a class="btn btn-lg" style="padding: 1px; white-space: pre-wrap; text-align: justify;" data-toggle="modal" data-target="#completarActividad" onclick="myFunctionVerActividad(';?><?php echo $A->idOrganizacion; ?>,<?php echo "'".$A->nombreOrganizacion."'" ?>,<?php echo $A->idCliente ?>,<?php echo "'".$A->nombrePersona."'" ?>,<?php echo $A->idNegocio ?>,<?php echo "'".$A->tituloNegocio."'" ?><?php echo ')"><span class="glyphicon glyphicon-play-circle silver"></span></a>
      		</div>
      		<br>
      		<div class="col-lg-12">
      		<div class="row">
      		<p style="margin-bottom: 0px;">'."\n".$A->tituloNegocio.'<br>'."$".number_format($A->valorNegocio, 2, '.', ',')." ".$A->tipoMoneda.'<br>'.$A->nombreOrganizacion.'
      		</p>
      		</div>
      		</div>
      		<div class="col-xs-12 col-lg-12" align="center">';
      		if ($filtro==1) {
      			echo '<span class="badge">GANADO</span > ';
      		}elseif ($filtro==2) {
      			echo '<span class="badge">PERDIDO</span > ';
      		}else{
      			if ($r->inactividad==0) {
      			}
      			elseif ($interval->format('%a')>=$r->inactividad) {

      				echo '<span class="badge">';
      				echo (($interval->format('%a')-$r->inactividad)== 1) ? ($interval->format('%a')-$r->inactividad)." Día" : ($interval->format('%a')-$r->inactividad)." Días";
      				echo '</span >';
      			}
      		}
      		echo '</div>
      		</div>
      		</div>
      		</li>';
      	}
      } 
  endforeach;
endforeach;
} 
echo '</ul>
</td>';
endforeach;
echo '</tr>';

if ($resultado==0) {
	$valor=$this->model->VerificarContenido($valorIdEmbudo);
	if (empty($valor)) {
		$true=true;
		$contador=$this->modelEmbudo->ContadorEtapas($valorIdEmbudo);
		echo '<tr><td class="alert-danger" colspan="'.$contador.'" align="center"> <strong>No hay coincidencias </strong></td></tr>';
	}
}elseif ($resultado){
}else{
	$true=true;
	$contador=$this->modelEmbudo->ContadorEtapas($valorIdEmbudo);
	echo '<tr><td class="alert-danger" colspan="'.$contador.'" align="center"> <strong>No hay coincidencias </strong></td></tr>';
}

if (!isset($true)) {
	$contadorNegocios=$this->model->Contador($valorIdEmbudo, $valorBusqueda, $filtro);
	if ($contadorNegocios==0) {
		$contador=$this->modelEmbudo->ContadorEtapas($valorIdEmbudo);
		echo '<tr><td class="alert-danger" colspan="'.$contador.'" align="center"> <strong>No hay coincidencias </strong></td></tr>';
	}
}


echo '</table>
</div>
<script>
$( function() {
	$( "'; for ($a=1; $a<=$i; $a++) {
		if ($a == $i) {
			echo("#sortable".$a);
		}else{
			echo("#sortable".$a.",");
		}
		} echo '"  ).sortable({
			placeholder: "placeholder",
			cursor: "move",
			opacity: 0.7, helper: "clone", 
			cursorAt: { top: 20, left: 56 }, 
			revert: true,
			connectWith: ".connectedSortable",
			update: function(event,ui) {
				'; for ($a=1; $a<=$i; $a++) {           
					echo 'var postDataA';echo($a);echo'=$("#sortable';echo($a); echo '").sortable("serialize");';
				} 
				foreach($array as $r):
					echo 'var id'.$r.'=$("#id'.$r.'").val();';
				endforeach; 
				echo 'var array = [';foreach($array as $r): echo($r.","); endforeach; echo '];';

				echo 'var conjunto=['; for ($a=1; $a<=$i; $a++) { if ($a==$i) {echo("postDataA".$a); }else echo("postDataA".$a.","); } echo '];';
				if ($resultado) {
					echo 'var busqueda=5;';
				}else{
					echo 'var busqueda=0;';
				}
				echo 'var valorBusqueda="'.$valorBusqueda.'";';
				echo 'var filtro="'.$filtro.'";';
				echo '$.post("index.php?c=negocios&a=DesplazarNegocios",{conjunto:JSON.stringify(conjunto),numlistas:JSON.stringify(array),busqueda:busqueda,valorBusqueda:valorBusqueda,filtro:filtro},function(mensaje) {
					$("#resultBusquedaEmbudo").html(mensaje);
					Contadores();
					});
				}
				});
				} );
				</script>';
			}


 //Metodo Para imprimir las actividades de cada negocio Chava

			public function ActividadesPorNegocio(){
				$idNegocio=$_REQUEST['valorIdNegocio'];
				$actividadesVencidas=$this->model->ActividadesNegocioVencidas($idNegocio);
				$actividadesPlaneadas=$this->model->ActividadesNegocioPlaneada($idNegocio);
				?>
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>    
					<h4 class="modal-title" id="myModalLabel"><strong>Actividades Planeadas</strong></h4>
				</div>
				<div class="modal-body" style="padding-bottom: 0px;">
					<div class="list-group">
						<?php 
						if ($actividadesVencidas) {
							$resultado = count($actividadesVencidas);
							?>
							<h4 class="list-group-item-heading" style="padding-top: 15px;"><strong>Vencida</strong><span>(<?php echo($resultado); ?>)</span></h4>
							<?php 
							foreach ($actividadesVencidas as $n):
								?>
								<div class="list-group-item">           
									<hr style="padding-bottom: 0px; padding-top: 0px">
									<p class="list-group-item-text">
										<div class="row">
											<div class="col-lg-12">
												<div class="col-xs-10 col-lg-10" style="padding-left: 1px;">
													<?php
													switch ($n->tipo) {
														case 'Llamada':
														?>
														<span class="glyphicon glyphicon-earphone"></span> Llamada
														<?php 
														break;
														case 'Reunion':
														?>
														<span class="glyphicon glyphicon-user"></span> Reunión
														<?php 
														break;
														case 'Tarea':
														?>
														<span class="glyphicon glyphicon-time"></span> Tarea
														<?php 
														break;
														case 'Plazo':
														?>
														<span class="glyphicon glyphicon-flag"></span> Plazo
														<?php 
														break;
														case 'Email':
														?>
														<span class="glyphicon glyphicon-send"></span> Email
														<?php 
														break;
														case 'Comida':
														?>
														<span class="glyphicon glyphicon-cutlery"></span> Comida
														<?php 
														break;

														default:
														?>
														<span class="glyphicon glyphicon-earphone"></span> WhatsApp
														<?php 
														break;
													} 
													?>

												</div>
												<div class="col-xs-2 col-lg-2" align="right" style="padding-right: 0px">
													<button type="button" data-toggle="modal" data-target="#mTerminarr" 
													<?php if($n->completado==0)
													{ 
														echo 'onclick="cambiaEstado('.$n->idActividad.',0,null)" class="btn btn-xs btn-default"'; 
													}else
													{ 
														echo 'onclick="cambiaEstado('.$n->idActividad.',1,'; echo "'".$n->fechaCompletado."'"; echo ')" class="btn btn-xs btn-success"';  
													} 
													echo '
													id="checkEstado"><span class="glyphicon glyphicon-check"></span></button>'; ?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-5 col-sm-4 col-md-4 col-lg-4" style="padding-right: 0px">
												<p class="help-block">Vencida hace</p>

											</div>
											<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding-left: 0px;">
												<p class="help-block"><?php date_default_timezone_set("America/Mexico_City"); 
												$A= date("Y")."-".date("m")."-".date("d");

												$datetime1 = date_create($A);
												$datetime2 = date_create($n->fechaActividad);
												$interval = date_diff($datetime1, $datetime2);

												if ($interval->format('%a')==1) {
													echo $interval->format('%a día');
												}else{
													echo $interval->format('%a días');
												}


												?>  <span>&nbsp;</span> <strong><?php echo $n->nombreUsuario; ?></strong></p>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<textarea id="txtNotass" type="text" class="form-control autoExpand" placeholder="Notas" name="notas" readonly><?php echo $n->notas; ?></textarea>
											</div>
										</div>
									</p>
								</div>
								<?php 
							endforeach;
						}if($actividadesPlaneadas){
							$resultado = count($actividadesPlaneadas);
							?>
							<h4 class="list-group-item-heading" style="padding-top: 15px;"><strong>Planeado</strong><span>(<?php   echo($resultado); ?>)</span></h4>
							<?php
							foreach ($actividadesPlaneadas as $n):
								?>
								<div class="list-group-item" >
									<hr style="padding-bottom: 0px; padding-top: 0px">
									<p class="list-group-item-text">
										<div class="row">
											<div class="col-lg-12">
												<div class="col-xs-10 col-lg-10" style="padding-left: 1px;">
													<?php
													switch ($n->tipo) {
														case 'Llamada':
														?>
														<span class="glyphicon glyphicon-earphone"></span> Llamada
														<?php 
														break;
														case 'Reunion':
														?>
														<span class="glyphicon glyphicon-user"></span> Reunión
														<?php 
														break;
														case 'Tarea':
														?>
														<span class="glyphicon glyphicon-time"></span> Tarea
														<?php 
														break;
														case 'Plazo':
														?>
														<span class="glyphicon glyphicon-flag"></span> Plazo
														<?php 
														break;
														case 'Email':
														?>
														<span class="glyphicon glyphicon-send"></span> Email
														<?php 
														break;
														case 'Comida':
														?>
														<span class="glyphicon glyphicon-cutlery"></span> Comida
														<?php 
														break;

														default:
														?>
														<span class="glyphicon glyphicon-earphone"></span> WhatsApp
														<?php 
														break;
													} 

													?>
												</div>
												<div class="col-xs-2 col-lg-2" align="right" style="padding-right: 0px">
													<button type="button" data-toggle="modal" data-target="#mTerminarr" onclick="cambiaEstado(<?php echo $n->idActividad.',0' ?>)" class="btn btn-xs btn-success" id="checkEstado"><span class="glyphicon glyphicon-check"></span></button>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-5 col-sm-4 col-md-4 col-lg-4" style="padding-right: 0px">
												<p class="help-block">Se vence </p>

											</div>
											<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding-left: 0px;">
												<p class="help-block"><?php date_default_timezone_set("America/Mexico_City"); 
												$A= date("Y")."-".date("m")."-".date("d");

												$datetime1 = date_create($A);
												$datetime2 = date_create($n->fechaActividad);
												$interval = date_diff($datetime1, $datetime2);

												if ($interval->format('%a')==1) {
													echo $interval->format('en %a día');
												}elseif($interval->format('%a')==0){
													echo $interval->format('Hoy');
												}else{
													echo $interval->format('en %a días');
												}


												?> <span>&nbsp;</span> <strong><?php echo $n->nombreUsuario; ?></strong></p>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<textarea id="txtNotass" type="text" class="form-control autoExpand" placeholder="Notas" name="notas" readonly><?php echo $n->notas; ?></textarea>
											</div>
										</div>
									</p>
								</div>
								<?php 
							endforeach;
						}
						?>

					</div>
					<div class="modal-footer">
						<div class="col-xs-12 col-lg-12" align="center">
							<button type="button" class="btn btn-link" data-toggle="modal" data-target="#añadiractividad" onclick="myFunctionlimpiar()"><span>+ </span>Programar una actividad</button>
						</div>
					</div>

					<?php 
				}


			public function Exportar(){
				$varIdEmbudo=$_POST['txtIdEmbudo'];
				$varValorBusqueda=$_POST['valorBusqueda'];
				$filtroStatus=$_POST['filtroStatus'];

				if (!$varValorBusqueda=="")
					$resultadoConsulta=$this->model->ConsultaNegociosValor($varIdEmbudo, $varValorBusqueda, $filtroStatus);
				else
					$resultadoConsulta=$this->model->ConsultaNegocios($varIdEmbudo,$filtroStatus);  

  //ob_start();
				require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';

      //Logotipo
				$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

      //Objeto de PHPExcel
				$objPHPExcel  = new PHPExcel();

      //Propiedades de Documento
				$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Negocios");

      //Establecemos la pestaña activa y nombre a la pestaña
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setTitle("Negocios");
				$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('Logotipo');
				$objDrawing->setDescription('Logotipo');
				$objDrawing->setImageResource($gdImage);
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setHeight(115);
				$objDrawing->setCoordinates('J1');
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

				$estilo = array(
					'font' => array(
						'name'      => 'Calibri',
						'bold'      => true,
						'italic'    => false,
						'strike'    => false,
						'size' =>11
					),
					'fill' => array(
						'type'  => PHPExcel_Style_Fill::FILL_SOLID
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_NONE
						)
					),
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				);

				$estiloTituloReporte = array(
					'font' => array(
						'name'      => 'Calibri',
						'bold'      => true,
						'italic'    => false,
						'strike'    => false,
						'size' =>23,
						'color' => array(
							'rgb' => 'E31B23'
						)
					),
					'fill' => array(
						'type'  => PHPExcel_Style_Fill::FILL_SOLID
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_NONE
						)
					),
					'alignment' => array(
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				);


				$estiloTituloColumnas = array(
					'font' => array(
						'name'  => 'Calibri',
						'bold'  => false,
						'size' =>12,
						'color' => array(
							'rgb' => 'FFFFFF'
						)
					),
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'E31B23')
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					),
					'alignment' =>  array(
						'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				);

				$estiloGanados = array(
					'font' => array(
						'name'  => 'Calibri',
						'bold'  => false,
						'size' =>11,
					),
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '7DCEA0')
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_NONE
						)
					)
				);

				$estiloPerdidos = array(
					'font' => array(
						'name'  => 'Calibri',
						'bold'  => false,
						'size' =>11,
					),
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'F1948A')
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_NONE
						)
					)
				);

				$estiloAlinearContenido = array(
					'alignment' =>  array(
						'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				);

				$estiloInformacion = new PHPExcel_Style();
				$estiloInformacion->applyFromArray( array(
					'font' => array(
						'name'  => 'Calibri',
						'size' =>12,
						'color' => array(
							'rgb' => '000000'
						)
					),
					'fill' => array(
						'type'  => PHPExcel_Style_Fill::FILL_SOLID
					),
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					),
					'alignment' =>  array(
						'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
					)
				));
				date_default_timezone_set("America/Mexico_City");

            $A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); //->No obitiene la fecha real


            $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('G3')->applyFromArray($estilo);
            $objPHPExcel->getActiveSheet()->getStyle('A6:j6')->applyFromArray($estiloTituloReporte);
            $objPHPExcel->getActiveSheet()->getStyle('A6:j6')->applyFromArray($estiloTituloColumnas);

            $objPHPExcel->getActiveSheet()->setCellValue('G3', 'FECHA: '.$A);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'REPORTE DE NEGOCIOS');
            $objPHPExcel->getActiveSheet()->mergeCells('A3:D3');
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Titulo');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('B6', 'Clave');
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
            $objPHPExcel->getActiveSheet()->setCellValue('C6', 'Consecutivo');
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
            $objPHPExcel->getActiveSheet()->setCellValue('D6', 'Servicio');
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue('E6', 'Etapa');
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue('F6', 'Valor de negocio');
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue('G6', 'Fecha de cierre');
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->setCellValue('H6', 'Ponderación');
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $objPHPExcel->getActiveSheet()->setCellValue('I6', 'Cliente'); 
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
            $objPHPExcel->getActiveSheet()->setCellValue('J6', 'Contacto');
            //Establecemos en que fila inciara a imprimir los datos
            $fila = 7; 

            //Recorremos los resultados de la consulta y los imprimimos
            foreach ($resultadoConsulta as $row) :
            	if ($row->status == 1) {
            		$objPHPExcel->getActiveSheet()->getStyle('A'.$fila.':'.'J'.$fila)->applyFromArray($estiloGanados);
            	}else if ($row->status == 2) {
            		$objPHPExcel->getActiveSheet()->getStyle('A'.$fila.':'.'J'.$fila)->applyFromArray($estiloPerdidos);
            	}

            	$objPHPExcel->getActiveSheet()->getStyle('B'.$fila.':D'.$fila)->applyFromArray($estiloAlinearContenido);
            	$objPHPExcel->getActiveSheet()->getStyle('F'.$fila.':H'.$fila)->applyFromArray($estiloAlinearContenido);
            	$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            	$objPHPExcel->getActiveSheet()->getStyle('C'.$fila)->getNumberFormat()->setFormatCode('0000');

            	$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $row->tituloNegocio);
            	$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $row->claveOrganizacion);
            	$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $row->claveConsecutivo);
            	$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $row->claveServicio);
            	$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, $row->nombreEtapa);
            	$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $row->valorNegocio);
            	$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $row->fechaCierre);
            	$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $row->ponderacion);
            	$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $row->nombreOrganizacion);
            	$objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $row->nombrePersona);

            	$fila++; 
            endforeach;

            header("Content-Type: application/vnd.ms-excel");
            header('Content-Disposition: attachment;filename="Negocios.csv"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            ob_end_clean();
            $objWriter->save('php://output');

        } 
	
	}
    ?>