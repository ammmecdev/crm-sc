<?php 
require_once 'model/actividad.php';



class NegociosController{

	private $contenedor;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT()
	{
			$this->modelOrganizacion = new Actividad();
	
	}

				//Metodo para listar todas las organizaciones para el select
	public function ListarOrganizaciones()
	{
		header('Content-Type: application/json');
		$datos = array();
		foreach ($this->modelOrganizacion->ListarOrganizaciones() as $organizacion):
			$row_array['idOrganizacion']  = $organizacion->idOrganizacion;
			$row_array['nombreOrganizacion']  = $organizacion->nombreOrganizacion;
			array_push($datos, $row_array);
		endforeach;		
		echo json_encode($datos, JSON_FORCE_OBJECT);
	}


	public function Index(){
		$negocios=true;
		$this->contenedor="";
		$page="view/negocios/encabezado.php";
		require_once '../../view/index.php';
	}

	
	}
    ?>

