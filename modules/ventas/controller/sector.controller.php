<?php
require_once 'model/sector.php';

class SectorController{
	private $model;
	private $url;
	private $pdo;
	private $mensaje;
	private $error;

	public function __CONSTRUCT(){
		$this->model = new Sector();
	}
	public function Index(){
		$clientes=true;
		$sector=true;
		$this->url="?c=sector";
		$page="view/sector/sector.php";
		require_once '../../view/index.php';
	}
	public function Guardar(){
		try {
			$sector= new Sector();
			$sector->idSector=$_REQUEST['idSector'];
			$sector->nombreSector=$_REQUEST['nombreSector'];
			$sector->claveSector=$_REQUEST['claveSector'];
			if($sector->idSector>0){
				$this->model->Actualizar($sector);
				echo "Se han actualizado correctamente los datos del sector industrial";
			}else{
				$this->model->Registrar($sector);
				echo "Se han registrado correctamente los datos del sector industrial";
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
	public function Contador(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == ''){
			$stm = $this->model->Listar();
		}else{
			$stm = $this->model->ConsultaSectores($bTexto, $bAlfabetica);
		}
		echo $stm->rowCount();
	}

	/*Metodo para realizar las consultas de acuerdo al valor de busqueda*/
	public function Consultas(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		if ($bTexto ==  '' && $bAlfabetica == '')
			$stm = $this->model->Listar();
		else
			$stm = $this->model->ConsultaSectores($bTexto, $bAlfabetica);
		$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
		$this->crearTabla($resultado);
	}

	public function CrearTabla($resultado){
		echo '<table class="table table table-hover" id="tbl">
		<thead>
		<tr style="background-color: rgba(106, 115, 123, 1);color: white;">
		<td align="center" width="5%">Editar</td>
		<td align="center" width="25%">Clave Sector</th>
		<td width="70%">Nombre del Sector</th>
		</tr>
		</thead>
		<tbody>';

		if($resultado==null){
			echo '<tr><td class="alert-danger" colspan="3" align="center"> <strong>No se encontraron sectores</strong></td></tr>';
		} else {
			foreach ($resultado as $r) :
				echo '
				<tr>
				<td align="center" style="width: 200px;"><a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mSector" onclick="myFunctionEditar('.$r->idSector; ?>,<?php echo  "'".$r->claveSector."'"; ?>,<?php echo  "'".$r->nombreSector."'"; ?>,<?php echo ')"> <span class="glyphicon glyphicon-pencil"></span></a>
				</td>
				<td align="center" style="width: 200px;"><strong>'. $r->claveSector.'</strong></td>
				<td style="white-space: nowrap;">'. $r->nombreSector.'</td>
				</tr>';
			endforeach;
		}
		echo '</tbody>
		</table>';
	}
	public function VerificaEx(){
		$bTexto = $_REQUEST['bTexto'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];
		try {
			if ($bTexto ==  '' && $bAlfabetica == ''){
				$stm = $this->model->Listar();
			}else{
				$stm = $this->model->ConsultaSectores($bTexto, $bAlfabetica);
			}
			$resultado = $stm->fetchAll(PDO::FETCH_OBJ);
			if($resultado!=null){
				echo "false";
			}else{
				echo "true";   
			} 
		} catch (Exception $e) {
			echo 'Algo salio mal. Intentelo de nuevo';
		} 
	}
	public function VerificarClave(){
		$Clave=$_REQUEST['claveSector'];
		$idSector=$_REQUEST['idSector'];
		$verificarCla=$this->model->VerificarClave($Clave,$idSector);
		if ($verificarCla==null) {
			echo "0";
		}else{
			echo "1";
		}
	}
	public function Eliminar(){
		try
		{
			$idSector=$_REQUEST['idSectorE'];
			$this->model->Eliminar($idSector);
			echo "Se elimino correctamente el sector";
		}
		catch(Exception $e)
		{
			echo "error";
    //echo $e->getMessage();

		}   
	}
	public function Exportar(){
		$valorBusqueda = $_REQUEST['buscar'];
		$bAlfabetica = $_REQUEST['bAlfabetica'];

		if ($valorBusqueda== '' && $bAlfabetica == ''){
			$resultado=$this->model->Listar(); 
		}else{
			$resultado=$this->model->ConsultaSectores($valorBusqueda, $bAlfabetica);
		}
		$resultado=$resultado->fetchAll(PDO::FETCH_OBJ);


		require '../../assets/plugins/PHPExcel/Classes/PHPExcel/IOFactory.php';
		$gdImage = imagecreatefrompng('../../assets/imagenes/logoammmec.png');

		$objPHPExcel  = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("Ammmec")->setDescription("Sectores");

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle("Sectores");
		$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
		$objDrawing->setName('Logotipo');
		$objDrawing->setDescription('Logotipo');
		$objDrawing->setImageResource($gdImage);
		$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
		$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
		$objDrawing->setHeight(130);
		$objDrawing->setCoordinates('C1');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$estilo = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>10
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloReporte = array(
			'font' => array(
				'name'      => 'Calibri',
				'bold'      => true,
				'italic'    => false,
				'strike'    => false,
				'size' =>16,
				'color' => array(
					'rgb' => 'E31B23'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_NONE
				)
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloTituloColumnas = array(
			'font' => array(
				'name'  => 'Calibri',
				'bold'  => false,
				'size' =>12,
				'color' => array(
					'rgb' => 'FFFFFF'
				)
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E31B23')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);

		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray( array(
			'font' => array(
				'name'  => 'Calibri',
				'size' =>12,
				'color' => array(
					'rgb' => '000000'
				)
			),
			'fill' => array(
				'type'  => PHPExcel_Style_Fill::FILL_SOLID
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			'alignment' =>  array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		));
		date_default_timezone_set("America/Mexico_City");
		$A= date("Y")."-".date("m")."-".date("d")."  "." ".date("H:i:s"); 

		$objPHPExcel->getActiveSheet()->getStyle('B3:C3')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($estilo);
		$objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A6:B6')->applyFromArray($estiloTituloColumnas);

		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'FECHA: '.$A);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'SECTORES');
		$objPHPExcel->getActiveSheet()->mergeCells('B3:C3');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(28);
		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Clave de sector');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Nombre de sector');
		$fila = 7; 

		foreach ($resultado as $r) :
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $r->claveSector);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $r->nombreSector);
			$fila++; 
		endforeach;

		header("Content-Type: application/vnd.ms-excel");
		header('Content-Disposition: attachment;filename="Sectores.csv"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		ob_end_clean();
		$objWriter->save('php://output');    
	}  
}
?>