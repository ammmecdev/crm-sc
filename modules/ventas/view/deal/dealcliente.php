<link rel="stylesheet" type="text/css" href="assets/css/dealcliente.css">
<!-- <script src="assets/js/dealcliente.js"></script> -->
<?php 
$idOrganizacion = $_REQUEST['idOrganizacion']; 
$Resultado=$this->model->Detalles($idOrganizacion); 
date_default_timezone_set("America/Mexico_City");
$FechaAc= date("Y-m-d"); 
$valorTipoTarea = isset($_REQUEST['valor']) ? $_REQUEST['valor'] : '';
?>

<!-- <?php  $idEmbudo = $_REQUEST['idEmbudo']; ?> -->
<input type="hidden" id="txtidUsuario" value=" <?php echo $Resultado['idUsuario']; ?>">

<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" align="center">
						<i class="mdi mdi-business mdi-3x"></i>
					</div>
					<div class="col-xs-6 col-sm-8 col-md-9 col-lg-9">
						<div class="btn-group" role="group">
							<h3 style="margin-top: 10px;"><span><?php echo $Resultado['nombreOrganizacion']; ?></span></h3>
						</div>				
					</div>
					<div class="col-xs-4 col-sm-2 col-md-2 col-lg-2" align="right">
						<div class="btn-group" role="group">
							<button data-target="#mNegocio" data-toggle="modal" onclick="myFunctionNuevo('<?php echo $Resultado['nombreOrganizacion']; ?>')" class="btn btn-sm btn-success" style="margin-top: 10px;">Añadir negocio</button>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>
	<!--Columna Izquierda-->
	<!-- Panel Detalles del cliente -->
	<aside class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<h4><span><a>Detalles</a></span></h4>
						<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h5>Clave: <span style="color:#757575"><?php echo $Resultado['clave']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>Teléfono: <span style="color:#757575"><?php echo $Resultado['telefono']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>Extención telefonica: <span style="color:#757575"><?php echo $Resultado['extensionTelefonica']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>Tipo de teléfono: <span style="color:#757575"><?php echo $Resultado['tipoTelefono']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>Dirección: <span style="color:#757575"><?php echo $Resultado['direccion']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>Localidad / Estado: <span style="color:#757575"><?php echo $Resultado['localidadEstado']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>RFC: <span style="color:#757575"><?php echo $Resultado['rfc']; ?></span></h5>
					</div>
					<div class="col-lg-12">
						<h5>Página web: <span style="color:#757575"><?php echo $Resultado['paginaWeb']; ?></span></h5>
					</div>
				</div>
			</div>
		</div>

		<!-- Panel Contactos -->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-lg-10">
						<h4><span><a>Contactos</a></span></h4>
					</div>
					<div class="col-xs-6 col-lg-2" align="right">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mPersona"><span class="glyphicon glyphicon-plus" onclick="AgregarContactos('<?php  echo $Resultado['idOrganizacion']; ?>' , '<?php echo $Resultado['nombreOrganizacion']; ?>');"></span>
							</button>
						</div>
					</div>
					<div class="col-xs-12 col-lg-12">
						<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					</div>
				</div>
				<div id="divContactos">
					<!-- Aqui se imprimen los contactos del cliente -->
				</div>
				<div class="row">
					<div class="col-lg-12" style="margin-top: 10px;">
						<div class="btn-group">
							<a href="#" class="btn btn-default btn-xs" onclick="verPersonas('<?php echo $Resultado['nombreOrganizacion']; ?>')" data-toggle="modal" data-target="#mVerPersonas">Ver todo
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Panel Negocios -->
		<div class="panel panel-default"> 
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<h4><span><a>Negocios</a></span></h4>
						<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12" id="divNegocios">
						<!-- Aqui se imprimen los negocios abiertos -->
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<hr style="margin-top: 10px; margin-bottom: 5px;">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div id="divContadorNC"><!-- Contador de Negocios Cerrador--></div>
						<div class="progress" id="divProgressN">
							<!-- Aqui se imprime la barra progress para los negocios ganados y perdidos -->
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="col-xs-3 col-lg-2">
								<div class="row">
									<div class="col-lg-12">
										<p class="help-block" style="margin-bottom: 0px;"><small>Ganados</small></p>
									</div>
									<div class="col-lg-12">
										<p class="help-block" style="margin-top: 0px;"><small>Perdidos</small></p>
									</div>
								</div>
							</div>
							<div class="col-xs-2 col-lg-2" id="divContadoresN">
								<!-- Aqui se imprime el total de negocios ganados / perdidos -->
							</div>
							<div class="col-xs-2 col-lg-3">
								<div class="row" id="divPorcentajesN">
									<!-- Aqui se imprime los porcentajes de negocios ganados y perdidos -->
								</div>
							</div>
							<div class="col-xs-5 col-lg-5" align="right">
								<div class="row" id="divValorNegocio">
									<!-- Aqui se imprime el valor acumulado de los negocios ganados y perdidos -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12" align="left">
						<button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#mNegocios" onclick="verNegocios();">Ver todos los negocios</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Panel Actividades -->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-lg-12">
						<h4><span><a>Resumen</a></span></h4>
						<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					</div>
				</div>
				<?php $countActividades=$this->model->ContadorActividades($Resultado['idOrganizacion']); ?>
				<div class="row" <?php if($countActividades==0){ ?> style="display: none;" <?php } ?> >
					<div class="col-xs-12 col-lg-12">
						<p class="help-block">Actividades principales</p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="progress" id="divProgressAct">	
							<!-- Aqui se imprime la barra de progress de actividades -->
						</div> 
					</div>
					<div class="col-xs-8 col-sm-7 col-md-7 col-lg-8" id="divTipoAct">
						<!-- Aqui se imprime el tipo de actividad -->
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" id="divCantidadAct">
						<!-- Aqui se imprime la cantidad total de actividades por cada tipo de actividad -->
					</div>
					<div class="col-xs-2 col-sm-2 col-md-3 col-lg-2" id="divPorcentajeAct">
						<!-- Aqui se imprime el porcentaje de las actividades -->
					</div>


					<div class="col-xs-12 col-lg-12">
						<p class="help-block">Usuarios más activos</p>
					</div>
					<div class="col-xs-12 col-lg-12">
						<div class="progress" id="divProgressUA">
							<!-- Aqui se imprime la barra de progress de los usuarios mas activos  -->
						</div>
					</div>
					<div class="col-xs-8 col-sm-7 col-md-7 col-lg-8" id="divUsuarioAct">
						<!-- Aqui se imprime el nombre del usuario -->
					</div>
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" id="divUserCantAct">
						<!-- Aqui se imprime la cantidad de actividades registradas segun el usuario -->
					</div>
					<div class="col-xs-2 col-sm-3 col-md-3 col-lg-2" id="divUserPorcenAct">
						<!-- Aqui se imprime el porcentaje de la suma de actividades por usuario   -->
					</div>
				</div>
			</div>
		</div>

		<!-- Panel Seguidores -->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-sm-8 col-md-8 col-lg-10" id="divContHead">
						<!-- Aqui se imprime el contador y el arreglo en un input -->
					</div>
					<div class="col-xs-6 col-sm-4 col-md-4 col-lg-2" align="right">
						<div class="btn-group">
							<button type="button" onclick="myFunctionAgregarSeguidores(<?php echo $idOrganizacion; ?>);" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mAgregarSeguidores"><span class="glyphicon glyphicon-plus"></span>
							</button>
						</div>
					</div>
				</div>
				<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
				<br>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="col-lg-2 col-xs-12 col-sm-12 col-md-12">
						<img src="../../assets/imagenes/<?php echo $Resultado['foto']; ?>" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;">
					</div>
					<div class="col-lg-10 col-xs-12 col-sm-12 col-md-12">
						<p class="help-block"><?php echo $Resultado['nombreUsuario']; ?></p>
					</div>
				</div>
				<div id="divUserSeguidores">
					<!-- Aqui se imprime el nombre de los seguidores -->
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;">
						<div class="btn-group">
							<button href="#" data-toggle="modal" data-target="#mVerSeguidores" onclick="myFunctionVerSeguidores();" class="btn btn-default btn-xs">Ver todo
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</aside>
	<!-- Fin de columna izquierda-->

	<!--Columna Derecha-->
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<ul class="nav nav-pills nav-justified">
					<li class="active" id="liNot" ><a data-toggle="tab"onclick="irNotas();" href="#nota">
						<i class="far fa-sticky-note"></i> Tomar notas</a>
					</li>
					<li id="liAct"><a data-toggle="tab"  href="#actividad" onclick="irActividades();">
						<i class="far fa-calendar-alt"></i> Añadir actividad</a>
					</li>
				</ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<!-- Sección de notas -->
					<div id="Notas" style="display:none;">
						<div class="bs-example-padded-bottom"> 
							<button type="button"  onclick="irNotas();" class="btn btn-default btn-lg" data-toggle="modal" data-target="#gridSystemModal" style="width: 100%; padding: 20px; border-style: dashed; font-size: 15px; background-color: white"> Haz click aquí para crear una nota </button> 
						</div>
					</div>
					<div id="nota" class="tab-pane fade in active">
						<form method="post" action="index.php?c=dealcliente&a=RegistrarNota" id="form-notas">
							<br>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
								<h2 style="margin-top: -10px; margin-bottom: 20px;">Añadir nota</h2>
							</div>
							<div class="form-group">
								<textarea id="txtNotas" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas"></textarea>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<label for="archivo" class="col-sm-2 control-label" style="padding-top: 7px;"><i class="fas fa-paperclip"></i> Adjuntar archivo: </label>
								<div class="col-sm-10">
									<input type="file" class="form-control" name="archivo" id="archivo">
								</div>
							</div>
							<div class="messages">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<hr>
							</div>
							<div class="form-group" align="right">
								<button class="btn btn-success" type="submit">Guardar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" onclick="ocultaNotas()">Cancelar</button> 
							</div>	
						</form>
					</div>
					<div id="Actividades" style="display:none;">
						<div class="bs-example-padded-bottom"> 
							<button type="button"  onclick="irActividades();" class="btn btn-default btn-lg" data-toggle="modal" data-target="#gridSystemModal" style="width: 100%; padding: 20px; border-style: dashed; font-size: 15px; background-color: white"> Haz click aquí para agregar una actividad </button> 
						</div>
					</div>
					<div id="actividad" class="tab-pane fade">
						<form method="post" action="index.php?c=actividades&a=Guardar" id="form-actividades">
							<div class="form-group">
								<input type="hidden" class="form-control" value="0" name="idActividad" id="txtIdActividad" readonly>
								<input type="hidden" class="form-control" id="txtConfirmado" name="confirmado" readonly>
							</div>
							<br>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h2 style="margin-top: -25px; margin-bottom: 20px;">Añadir actividad</h2>
							</div>
							<div class="row" style="margin-top: -20px">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<font size="1">TIPO DE ACTIVIDAD</font>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="btn-group form-group">
										<label class="btn btn-sm btn-default" id="labLlamada" onclick="tipoActividad('Llamada');" data-toggle="tooltip" title="Llamada">
											<i class="mdi mdi-phone-in-talk mdi-lg"></i>
											<input type="radio" class="oculto" name="options" id="optionLlamada">
										</label>
										<label class="btn btn-sm btn-default" id="labReunion" onclick="tipoActividad('Reunion');" data-toggle="tooltip" title="Reunión">
											<i class="mdi mdi-group mdi-lg"></i>
											<input type="radio" class="oculto" name="options" id="optionReunion">
										</label>
										<label class="btn btn-sm btn-default" id="labTarea" onclick="tipoActividad('Tarea');" data-toggle="tooltip" title="Tarea">
											<span>
												<i class="far fa-clock" style="font-size: 14px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionTarea">
										</label>
										<label class="btn btn-sm btn-default" id="labPlazo" onclick="tipoActividad('Plazo');" data-toggle="tooltip" title="Plazo">
											<span>
												<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionPlazo">
										</label>
										<label class="btn btn-sm btn-default" id="labEmail" onclick="tipoActividad('Email');"  data-toggle="tooltip" title="Correo electrónico">
											<i class="mdi mdi-email mdi-lg"></i>
											<input type="radio" class="oculto" name="options" id="optionEmail">
										</label>
										<label class="btn btn-sm btn-default" id="labComida" onclick="tipoActividad('Comida');" data-toggle="tooltip" title="Comida">
											<span>
												<i class="fas fa-utensils" style="font-size: 13px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionComida">
										</label>
										<label class="btn btn-sm btn-default" id="labWhatsApp" onclick="tipoActividad('WhatsApp');" data-toggle="tooltip" title="WhatsApp">
											<span>
												<i class="fab fa-whatsapp" style="font-size: 15px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionWhatsapp">
										</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipo" required>
							</div>

							<div class="form-group">
								<textarea id="txtNotasA" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas" required></textarea>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<font size="1">FECHA</font>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<input type="date" class="form-control" name="fecha" id="txtfechaActividad" required />
									</div>
									<div class="col-lg-12">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name='checkTiempo' id="checkTiempo"> Asignar hora y duración
									</label>
								</div>
							</div>

							<div id="divTiempo"> 
								<div class="row">
									<div class="form-group">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<font size="1">HORA</font>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="time" class="form-control" name="hora" id="txtHora" onblur="deshabilitaConfirmado()" />
										</div>
										<div class="col-lg-12">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-lg-12">
											<font size="1">DURACIÓN</font>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<input type="number" class="form-control" name="duracion" id="txtDuracion" min="1" max="100" onblur="deshabilitaConfirmado()"/>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<select class="form-control" name="tipoDuracion" id="selectTipoDuracion" onchange="deshabilitaConfirmado()">
												<option value="minutos">Minutos</option>
												<option value="horas">Horas</option>
											</select>
										</div>
										<div class="col-lg-12">
										</div>
									</div>
								</div>
							</div>

							<input name="radioFiltros" value="negocios" type="hidden">

							<div class="form-group" id="selectPersonas" style="margin-top: 15px;">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business"></i></span>
									<input class="form-control" id="txtNomOrganizacion" name="nombreOrganizacion" readonly="readonly" value="<?php echo  $Resultado['nombreOrganizacion']; ?>">
									<input type="hidden" id="txtIdOrganizacion" name="idOrganizacion" value="<?php echo $Resultado['idOrganizacion']; ?>">
								</div>
							</div>

							<div class="form-group" id="selectPersonas">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
									<select class="form-control selectpicker" data-live-search="true" name="nombrePersona" id="selectIdPersonaAct" required>
									</select>
								</div>
							</div>

							<div class="form-group" id="selectPersonas">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="fas fa-briefcase"></i></span>
									<select class="form-control selectpicker" data-live-search="true" name="idNegocio" id="selectIdNegocioF">
									</select>
								</div>
							</div>

							<div class="row" id="divCruzadas" style="margin-bottom: -50px;">
								<div class="form-group">
									<div class="col-lg-12">
										<div class="help-block with-errors" id="cruzadas"></div>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="submit" class="btn btn-success" id="btnGuardarActividad">Guardar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" onclick="ocultaActividades();">Cancelar</button> 
							</div>
							<!--Fin Modal content-->
						</form>
						<!--FORMULARIO DE ACTIVIDADES-->
					</div>

					<div id="adjuntar" class="tab-pane fade">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label for="archivo" class="col-sm-3 control-label" style="padding-top: 7px;">Seleccionar Archivo:</label>
							<div class="col-sm-9">
								<input type="file" class="form-control" name="archivo" id="archivo">

							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<hr>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="right">
								<button type="submit" class="btn btn-sm btn-success" onclick="registrarArchivo();">Guardar</button>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="panel panel-default">
			<ul class="nav nav-pills nav-justified"  style="padding: 10px 10px 10px 10px">
				<div class="col-xs-3">
					<li role="presentation" align="center"><a href="#" onclick="consulta(0); return false">Todos <span class="label label-default" id="countTotal"></span></a></li>
				</div>
				<div class="col-xs-3">
					<li role="presentation" align="center"><a href="#" onclick="consulta(1); return false">Actividades <span class="label label-primary" id="countActividades"></span></a></li>
				</div>
				<div class="col-xs-3">
					<li role="presentation" align="center"><a href="#" onclick="consulta(2); return false">Notas <span class="label label-success" id="countNotas"></span></a></li>
				</div>
				<div class="col-xs-3">
					<li role="presentation" align="center"><a href="#" onclick="consulta(5); return false">Negocios <span class="label label-danger" id="countNegocios"></span></a></li>
				</div>
			</ul>
		</div>
	</div>

	<!-- Linea del tiempo -->

	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="timeline-centered" id="div-linea">

		</div>
	</div>

	<!-- Inicio de MODALES ............................................................................ -->

	<!--Inicio modal de editar actividad -->
	<div class="modal fade" id="mActividades" role="dialog">   
		<div class="modal-dialog">
			<form  method="post" action="index.php?c=actividades&a=Guardar" enctype="multipart/form-data" role="form" id="form-actividadesE">
				<input type="hidden" class="form-control" name="idActividad" id="txtIdActividadE"/>
				<input type="hidden" class="form-control" name="actividad" value="completa"/>
				<input type="hidden" class="form-control" id="txtConfirmadoE" name="confirmado" readonly>

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header bg-success">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title">Editar actividad</h3>
					</div>
					<div class="modal-body"> 

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">TIPO DE ACTIVIDAD</font>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="btn-group form-group">
									<label class="btn btn-sm btn-default" id="labLlamadaE" onclick="tipoActividadE('Llamada');" data-toggle="tooltip" title="Llamada">
										<i class="mdi mdi-phone-in-talk mdi-lg"></i>
										<input type="radio" class="oculto" name="options" id="optionLlamadaE">
									</label>
									<label class="btn btn-sm btn-default" id="labReunionE" onclick="tipoActividadE('Reunion');" data-toggle="tooltip" title="Reunión">
										<i class="mdi mdi-group mdi-lg"></i>
										<input type="radio" class="oculto" name="options" id="optionReunionE">
									</label>
									<label class="btn btn-sm btn-default" id="labTareaE" onclick="tipoActividadE('Tarea');" data-toggle="tooltip" title="Tarea">
										<span>
											<i class="far fa-clock" style="font-size: 14px;"></i>
										</span>
										<input type="radio" class="oculto" name="options" id="optionTareaE">
									</label>
									<label class="btn btn-sm btn-default" id="labPlazoE" onclick="tipoActividadE('Plazo');" data-toggle="tooltip" title="Plazo">
										<span>
											<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>
										</span>
										<input type="radio" class="oculto" name="options" id="optionPlazoE">
									</label>
									<label class="btn btn-sm btn-default" id="labEmailE" onclick="tipoActividadE('Email');"  data-toggle="tooltip" title="Correo electrónico">
										<i class="mdi mdi-email mdi-lg"></i>
										<input type="radio" class="oculto" name="options" id="optionEmailE">
									</label>
									<label class="btn btn-sm btn-default" id="labComidaE" onclick="tipoActividadE('Comida');" data-toggle="tooltip" title="Comida">
										<span>
											<i class="fas fa-utensils" style="font-size: 13px;"></i>
										</span>
										<input type="radio" class="oculto" name="options" id="optionComidaE">
									</label>
									<label class="btn btn-sm btn-default" id="labWhatsAppE" onclick="tipoActividadE('WhatsApp');" data-toggle="tooltip" title="WhatsApp">
										<span>
											<i class="fab fa-whatsapp" style="font-size: 15px;"></i>
										</span>
										<input type="radio" class="oculto" name="options" id="optionWhatsappE">
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipoE" required>
						</div>

						<div class="form-group">
							<textarea id="txtNotasAE" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas" required></textarea>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">FECHA</font>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="date" class="form-control" name="fecha" id="txtfechaActividadE" required />
								</div>
								<div class="col-lg-12">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name='checkTiempo' id="checkTiempoE"> Asignar hora y duración
								</label>
							</div>
						</div>

						<div id="divTiempoE"> 
							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<font size="1">HORA</font>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<input type="time" class="form-control" name="hora" id="txtHoraE" onblur="deshabilitaConfirmadoE()" />
									</div>
									<div class="col-lg-12">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-lg-12">
										<font size="1">DURACIÓN</font>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<input type="number" class="form-control" name="duracion" id="txtDuracionE" min="1" max="100" onblur="deshabilitaConfirmadoE()"/>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<select class="form-control" name="tipoDuracion" id="selectTipoDuracionE" onchange="deshabilitaConfirmadoE()">
											<option value="minutos">Minutos</option>
											<option value="horas">Horas</option>
										</select>
									</div>
									<div class="col-lg-12">
									</div>
								</div>
							</div>
						</div>

						<input name="radioFiltros" value="negocios" type="hidden">

						<div class="form-group" id="selectPersonas" style="margin-top: 15px;">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business"></i></span>
								<input class="form-control" id="txtNomOrganizacion" name="nombreOrganizacion" readonly="readonly" value="<?php echo  $Resultado['nombreOrganizacion']; ?>">
								<input type="hidden" id="txtIdOrganizacion" name="idOrganizacion" value="<?php echo $Resultado['idOrganizacion']; ?>">
							</div>
						</div>

						<div class="form-group" id="selectPersonas">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
								<select class="form-control selectpicker" data-live-search="true" name="nombrePersona" id="selectIdPersonaActE" required>
								</select>
							</div>
						</div>

						<div class="form-group" id="selectPersonas">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="fas fa-briefcase"></i></span>
								<select class="form-control selectpicker" data-live-search="true" name="idNegocio" id="selectIdNegocioFE">
								</select>
							</div>
						</div>

						<div class="row" id="divCruzadasE" style="margin-bottom: -50px;">
							<div class="form-group">
								<div class="col-lg-12">
									<div class="help-block with-errors" id="cruzadasE"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
							<div class="col-xs-2 col-lg-2" align="left" style="padding-left: 0px">

							</div>
							<div class="col-xs-10 col-lg-10" align="right" style="padding-right: 0px">
								<input type="submit" class="btn btn-success" id="btnGuardarE">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div> 
						</div>
					</div>

				</div>
			</form>
		</div>
	</div>     
	<!--fin modal de editar actividad -->



	<!--Inicio modal de añadir negocio -->
	<div class="modal fade" id="mNegocio" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<form data-toggle="validator" method="post" action="index.php?c=negocios&a=Guardar" role="form" id="form-negocios">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header bg-gray">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong><label>Añadir Negocio</label></strong></h4>
					</div>
					<div class="modal-body">  
						<!-- Cuerpo -->  
						<!-- Estos son los ids que se van a enviar por post para registrar las llaves foraneas de organizaciones y personas -->
						<input type="hidden" class="form-control" name="idOrganizacion" id="inputOrganizacion">
						<input type="hidden" class="form-control" name="idCliente" id="inputCliente">
						<input type="hidden" class="form-control" name="idNegocio" id="txtIdNegocio">
						<input type="hidden" name="idNegocioEst" id="txtIdNegEst">
						<input type="hidden" name="estimacion" id="txtEstimacion" value="0">
						<!-- Nombre de la organización -->
						<div class="form-group" id="divOrganizacion">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2">
									<span class="glyphicon glyphicon-briefcase"></span></span>
									<input class="form-control" readonly="readonly" data-live-search="true" name="idOrganizacionN" id="selectIdOrganizacion" >

								</div>
							</div>
							<!-- Nombre de la persona de contacto para el filtro de organizacion-->
							<div class="form-group" id="divPersonasF">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">
										<span class="glyphicon glyphicon-user"></span></span>
										<select class="form-control selectpicker" data-live-search="true" name="idClienteF" id="selectIdPersonaF" onchange="obtenerIdPersona();" required>
										</select>
									</div>
								</div>

								<div class="form-group">
									<h5>Título del negocio</h5>
									<input type="text" class="form-control" name="tituloNegocio" id="txtTituloNegocio" maxlength="50" placeholder="Título del negocio" required>
								</div>

								<h5>Etapa del embudo</h5>
								<div class="form-group" id="ConsultaEtapa">
									<!-- Aquí se imprime el select segun sea el embudo seleccionado y el input tipo text que contiene el idEmbudo -->
									<div class="help-block with-errors"></div>
								</div>

								<input type="hidden" class="form-control" name="nombreEtapa" id="txtNombreEtapa">

								<div id="divClaveNegocio">
									<div class="row">
										<div class="form-group">
											<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0px;">
												<h5>Clave del Negocio</h5>
											</div>
										</div>
									</div>

									<div class="row" style="padding-bottom: 5px;">
										<div class="form-group">
											<div class="col-xs-12 col-md-12 col-lg-12">
												<div class="row">
													<div class="col-xs-4 col-md-4 col-lg-4">
														<p class="help-block">Clave</p>
													</div>
													<div class="col-xs-4 col-md-4 col-lg-4">
														<p class="help-block">Consecutivo</p>
													</div>
													<div class="col-xs-4 col-md-4 col-lg-4">
														<p class="help-block">Servicio</p>
													</div>
												</div>
											</div>
											<div class="col-xs-4 col-md-4 col-lg-4">
												<input type="text" class="form-control" maxlength="3" name="claveOrganizacion" id="txtClaveOrganizacion" placeholder="-- -- --" readonly required>
											</div>

											<div class="col-xs-4 col-md-4 col-lg-4">
												<input type="text" class="form-control" maxlength="4" name="consecutivo" id="txtConsecutivo" value="0000" readonly required>
											</div>
											<div class="col-xs-4 col-md-4 col-lg-4">
												<select name="servicio" id="txtServicio" class="form-control">
													<option value="">---</option>
													<option value="AST" title="Asesoría Técnica">AST</option>
													<option value="FTA" title="Fabricación">FTA</option>
													<option value="MCC" title="Mantenimiento Correctivo en Campo">MCC</option>
													<option value="MCT" title="Mantenimiento Correctivo en Taller">MCT</option>
													<option value="CBM" title="Mantenimiento basado en la condición">CBM</option>
													<option value="MEM" title="Montaje">MEM</option>
													<option value="SMR" title="Suministros">SMR</option>
													<option value="VVA" title="Varios">VVA</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<h5>Unidad de negocio</h5>
								<div class="form-group">
									<select class="form-control" name="idUnidadNegocio" id="selectIdUnidadNegocio">
									</select>
								</div>


								<h5>Equipo</h5>
								<div class="form-group">
									<select class="form-control selectpicker" data-live-search="true" name="idEquipo" id="selectIdEquipo" required>
									</select>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0px;">
											<h5>Valor del Negocio</h5>
										</div>

										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding-left: 10px;">
											<h5>Tipo de Moneda</h5>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-xs-4 col-sm-4" style="width: 40%; padding-right: 0px">
											<input type="text" class="form-control" maxlength="12" name="valorNegocio" id="txtValorNegocio" placeholder="Valor" required>

										</div>
										<div class="col-xs-8 col-sm-8" style="width: 60%">
											<select class="form-control" name="tipoMoneda" id="selectTipoMoneda" required>
												<option value="MXN">Pesos Méxicanos (MXN)</option>
												<option value="USD">US Dollar (USD)</option>
											</select>
										</div>        
									</div>
								</div>

								<div class="form-group">
									<h5>Fecha de cierre prevista</h5>
									<input type="date" class="form-control" name="fechaCierre" id="txtFechaCierre" required>
								</div>

								<h5>% Ponderación</h5>
								<div class="form-group">
									<select class="form-control" name="ponderacion" id="selectPonderacion" required>
										<option value="">Seleccione la ponderación</option>
										<option value="25%">25 %</option>
										<option value="50%">50 %</option>
										<option value="75%">75 %</option>
										<option value="100%">100 %</option>
									</select>
								</div>

								<h5 id="labelPresupuesto">Crear Presupuesto</h5>
								<div class="form-group formulario" id="divPresupuesto">
									<div class="radio" style="padding-left: 0px;">
										<input type="radio" name="filtro" id="No" value="0" checked>
										<label for="No">No</label>

										<input type="radio" name="filtro" id="Si" value="1">
										<label for="Si">Si</label>
									</div>
								</div>

								<div id="ActivarPresupuesto">
									<div class="form-group">
										<select name="idPresupuesto" id="selectPresupuesto" class="form-control selectpicker" data-live-search="true">

										</select>                        
									</div>
									<div class="form-group">
										<textarea name="descripcion" id="txtNotas" class="form-control" placeholder="Descripción"></textarea>
									</div>
								</div>

								<input type="hidden" class="form-control" name="xCambio" id="valorUSD" value="1">
								<!-- fin cuerpo -->
							</div>
							<div class="modal-footer">
								<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
									<div class="col-lg-3" align="left" style="padding-left: 0px">

									</div>
									<div class="col-lg-9" align="right" style="padding-right: 0px">
										<input type="submit" class="btn btn-success" id="btnGuardar" >
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>  
									</div>  
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>     
			<!--fin modal de añadir negocio -->

			<!--Inicio modal de añadir persona -->
			<div class="modal fade" id="mPersona" role="dialog">
				<div class="modal-dialog">
					<form  method="POST" id="form_persona" action="index.php?c=personas&a=Guardar" role="form" enctype="multipart/form-data">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header bg-success">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><strong> <label>Añadir contacto
								</label></strong></h4>
							</div>
							<!-- Cuerpo -->
							<div class="modal-body">
								<div class="form-group">
									<input type="hidden" class="form-control" name="idCliente" id="txtIdCliente" value="">
								</div>
								<div class="form-group">
									<h5>Nombre Completo</h5>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon2"><i class="fas fa-user" style="font-size: 18px;"></i></span>
										<input type="text" class="form-control" aria-describedby="basic-addon2" name="nombrePersona" id="txtNomPersona" required placeholder="Nombre Completo">
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
											<h5>Teléfono</h5>
										</div>
										<div class="col-xs-5 col-sm-5 col-md-5 col-lg-6" style="padding-left: 50px;">
											<h5>Extensión</h5>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 0px">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-contact-phone" style="font-size: 18px;"></i></span>
												<input type="text" class="form-control" aria-describedby="basic-addon2" name="telefono" id="txtTelefono" required placeholder="Teléfono">
											</div>

										</div>
										<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 15px">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-dialpad" style="font-size: 18px;"></i></span>
												<input type="text" class="form-control" aria-describedby="basic-addon2" name="extension" id="txtExtension" required placeholder="Extensión">
											</div>

										</div>             
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
											<h5>Tipo de Teléfono</h5>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-xs-8 col-sm-6" style="width: 100%">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon2"><i class="fas fa-bars" style="font-size: 19px;"></i></span>
												<select class="form-control" name="tipoTelefono" id="selectTipoTelefono">
													<option value="">Tipo de teléfono</option>
													<option value="Trabajo">Trabajo</option>
													<option value="Casa">Casa</option>
													<option value="Celular">Celular</option>
													<option value="Otro">Otro</option>
												</select>
											</div>
										</div>                
									</div>
								</div>
								<div class="form-group">
									<h5>Correo Electrónico</h5>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-email" style="font-size: 17px;"></i></span>
										<input type="email" class="form-control" aria-describedby="basic-addon2" name="email" id="txtEmail" required placeholder="Correo Electrónico">
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business" style="font-size: 18px;"></i></span>
										<input class="form-control selectpicker" data-live-search="true" name="idOrganizacion" id="selectIdOrganizaciones" required readonly="readonly">
										<input type="hidden" class="form-control" name="idOrganizacion" id="txtidOrganizacion">
									</div>

								</div>
								<h5>Honorífico</h5>
								<div class="row">
									<div class="form-group">
										<div class="col-xs-8 col-sm-6" style="width: 100%">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon2"><i class="fas fa-graduation-cap" style="font-size: 14px;"></i></span>
												<select class="form-control" name="honorifico" id="selectHonorifico" required>
													<option value="">Honorifico</option>
													<option value="Ing.">Ing.</option>
													<option value="Lic.">Lic.</option>
													<option value="Sr.">Sr.</option>
													<option value="Srta.">Srta.</option>
												</select>
											</div>
										</div>                
									</div>
								</div>

								<div class="form-group">
									<h5>Puesto</h5>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
										<input type="text" class="form-control" name="puesto" id="txtPuesto" placeholder="Puesto" required>
									</div>
								</div>
								<div class="form-group">
									<input type="hidden" name="propietario" id="txtPropietario" value="1">
								</div>
							</div>
							<!-- fin cuerpo --> 
							<div class="modal-footer">
								<div class="col-xs-3 col-lg-3" align="left">

								</div>
								<div class="col-xs-9 col-lg-9" align="right">
									<input type="submit" class="btn btn-success" value="Guardar">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								</div>
							</div>
						</div>
					</form>
				</div>  
			</div>    
			<!--fin modal de añadir persona-->



			<!--inicio modal de agregar Seguidores  -->
			<div class="modal fade" id="mAgregarSeguidores" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form  method="post" data-toggle="validator" action="index.php?c=dealcliente&a=AgregarSeguidores" enctype="multipart/form-data" role="form" id="form-AgregarSeguidores">
							<div class="modal-header bg-success">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title"><strong><font></font>Agregar Seguidores</strong></h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<div id="ResuS">
										<!--Resultado Nombre de negocio -->
									</div>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><i class="fas fa-search" style="font-size: 16px;"></i></span>
										<select class="form-control selectpicker" data-live-search="true" name="idUsuario" id="selectIdUsuario" required>
										</select>
										<input type="hidden" class="form-control" name="idOrganizacion" id="txtidClienteS" value="">
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn btn-success" value="Guardar">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

							</div>
						</form>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<!--fin modal de agregar Seguidores  -->
			<!--Modal NEGOCIOS-->
			<div id="mNegocios" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header bg-success">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title"><strong>Negocios</strong></h4>
						</div>
						<div class="modal-body"> 
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<div class="col-xs-2 col-lg-1">
										<i class="mdi mdi-business mdi-3x"></i>
									</div>
									<div class="col-xs-8 col-lg-9">
										<h4><strong><label><?php echo $Resultado['nombreOrganizacion']; ?></label></strong></h4>
									</div>
									<div class="col-xs-2 col-lg-2" align="right">
										<div class="btn-group">
										</div>
									</div>
								</div>					
							</div>
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<hr style="margin-top: 5px; margin-bottom: 10px;">
									<div style="overflow-x: auto;">
										<div class="table-responsive">
											<div id="ResumenVerNegocios"> </div>
										</div>
									</div>
								</div>					
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-xs-12 col-lg-12" align="right">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--Modal NEGOCIOS-->
			<!--Modal Ver Personas-->
			<div id="mVerPersonas" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-header bg-success">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong>Contactos</strong></h4>
					</div>
					<div class="modal-content">
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-2 col-sm-4 col-md-4 col-lg-1" align="center">
									<i class="mdi mdi-business mdi-3x"></i>
								</div>
								<div class="col-xs-6 col-sm-4 col-md-4 col-lg-9">
									<h4 style="margin-top: 15px;"><strong><label><?php echo $Resultado['nombreOrganizacion']; ?></label></strong></h4>
								</div>
								<div class="col-xs-2 col-lg-2" align="right">
									<div class="btn-group">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-lg-12">
										<hr style="margin-top: 5px; margin-bottom: 15px;">
									</div>
								</div>
							</div>
							<div style="overflow-x: auto;">
								<div class="table-respinsive">
									<div id="ResumenVerPersonas"></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-xs-12 col-lg-12" align="right">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--Modal Ver Personas-->
			<div class="modal fade" id="mEliminarA" role="dialog">   
				<div class="modal-dialog">
					<form  method="post" action="index.php?c=actividades&a=Eliminar" enctype="multipart/form-data" id="form-eliminar">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header bg-danger">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><strong>Eliminar actividad</strong></h4>
							</div>
							<div class="modal-body"  style="margin-bottom: -30px;">  
								<!-- Cuerpo --> 
								<p style="font-size: 20px;">¿Está seguro que desea eliminar la actividad?</p>
								<input id="txtIdActividadE" name="idActividad" hidden>
								<!-- fin cuerpo --> 
							</div>
							<div class="modal-footer">
								<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
									<input type="submit" class="btn btn-danger" value="Eliminar">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
								</div>
								<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
							</div>
						</div>
					</form>
				</div>
			</div> 

			<!--Modal Ver Seguidores-->
			<div class="modal fade" id="mVerSeguidores" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header bg-success">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"><strong>Ver Seguidores</strong></h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<div class="col-xs-2 col-lg-1">
										<i class="mdi mdi-business mdi-3x"></i>
									</div>
									<div class="col-xs-8 col-lg-9">
										<h4><strong><label><?php echo $Resultado['nombreOrganizacion']; ?></label></strong></h4>
									</div>
								</div>					
							</div>
							<div class="row">
								<div class="col-xs-12 col-lg-12">
									<hr style="margin-top: 5px; margin-bottom: 10px;">
									<div style="overflow-x: auto;">
										<div class="table-responsive">
											<div id="ResuVerS"> </div>
										</div>
									</div>
								</div>					
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-lg-12" align="right">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="mTerminar" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form  method="post" data-toggle="validator" action="index.php?c=actividades&a=CambiaEstado" enctype="multipart/form-data" role="form" id="form-cambiaEstado">
							<div class="modal-header bg-success">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title"><strong><font id="labActComp"></font></strong></h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="">Fecha:</label>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
										<input type="date" class="form-control" name="fechaCompletado" required id="txtFechaCompletado" >
									</div>
								</div>
							</div>

							<input type="hidden" id="txtEstado" name="completado">
							<input type="hidden" id="txtIdActividad2" name="idActividad">

							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<input type="submit" class="btn btn-success" id="Guardar2">
							</div>
						</form>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
			<!--Modal Ver Seguidores-->
			<div class="modal fade" id="mTerminar" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form  method="post" data-toggle="validator" action="index.php?c=actividades&a=CambiaEstado" enctype="multipart/form-data" role="form" id="form-cambiaEstado">
							<div class="modal-header bg-success">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title"><strong><font id="labActComp"></font></strong></h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="">Fecha:</label>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
										<input type="date" class="form-control" name="fechaCompletado" required id="txtFechaCompletado" >
									</div>
								</div>
							</div>

							<input type="hidden" id="txtEstado" name="completado">
							<input type="hidden" id="txtIdActividad2" name="idActividad">

							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								<input type="submit" class="btn btn-success" id="Guardar2">
							</div>
						</form>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<!--Inicio modal de confirmar para eliminar usuarios -->
			<div class="modal fade" id="mEliminarS" role="dialog">   
				<div class="modal-dialog">
					<form  method="post" id="form_seguidoresdel" action="index.php?c=dealcliente&a=EliminarSeguidores" enctype="multipart/form-data">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header bg-danger">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><strong>Desvincular Usuario</strong></h4>
							</div>
							<div class="modal-body">  
								<p style="font-size: 18px;">¿Está seguro que desea desvincular el usuario?</p>
								<input type="hidden" class="form-control" name="idUsuario" id="txtidUsuarioE" value="">
								<input type="hidden" class="form-control" name="idOrganizacion" id="txtIdOrganizacionEl" value="">
							</div>
							<div class="modal-footer">
								<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
									<input type="submit" class="btn btn-danger" value="Aceptar">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>     
			<!--fin modal de confirmar para eliminar usuarios -->

			<!--Inicio modal de confirmar para eliminar Contactos -->
			<div class="modal fade" id="mEliminarContacto" role="dialog">   
				<div class="modal-dialog">
					<form  method="post" id="form_personasdel" action="index.php?c=personas&a=Eliminar" enctype="multipart/form-data">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header bg-danger">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><strong>Eliminar Contacto</strong></h4>
							</div>
							<div class="modal-body">  
								<p style="font-size: 18px;">¿Está seguro que desea eliminar el contacto?</p>
								<input type="hidden" class="form-control" name="idCliente" id="txtidContactos" value="">
							</div>
							<div class="modal-footer">
								<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
									<input type="submit" class="btn btn-danger" value="Eliminar">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>     
			<!--fin modal de confirmar para eliminar usuarios -->

			<!--Inicio modal para eliminar negocio  SI-->
			<div class="modal fade" id="mEliminar" role="dialog">   
				<div class="modal-dialog">
					<form  method="post" action="index.php?c=negocios&a=Eliminar" role="form" id="form-negocios-eliminarN">
						<div class="modal-content">
							<div class="modal-header bg-danger">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><strong>Eliminar negocio</strong></h4>
							</div>
							<div class="modal-body"  style="margin-bottom: -30px;">   
								<p style="font-size: 20px;">¿Está seguro que desea eliminar el negocio?</p>
								<input type="hidden" id="txtIdNegocioEl" name="idNegocio">
								<input type="hidden" id="txtNomOrg" name="NomOrganizacion"> 
							</div>
							<div class="modal-footer">
								<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
									<input type="submit" class="btn btn-danger" value="Eliminar">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>     
			<!--fin modal para eliminar negocio -->

			<script >
				var idOrganizacion= <?php echo $_REQUEST['idOrganizacion']; ?>;
				var nombreOrganizacion= "<?php echo $Resultado['nombreOrganizacion']; ?>";
				var idEmbudo= <?php echo $_SESSION['idEmbudo']; ?>;
				var val = null;
				$(document).ready(function() {
					ocultaNotas();
					consulta();
					actualizarContactos();
					actualizarNegocios();
					actualizarDetallesNegocio();
					actualizarDetallesActividad();
					actualizarDetallesUserActivo();
					actualizarSeguidores();

					$('#form_seguidoresdel').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
							success: function(respuesta){
								$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Seguidor desvinculado con exito</center></strong></div>');
								$('#mVerSeguidores').modal('toggle');
								$('#mEliminarS').modal('toggle');
								$('#mensajejs').show();
								$('#mensajejs').delay(3500).hide(600);
								actualizarSeguidores();
								consulta();
							}
						})
						return false;
					});

					$('#form-negocios-eliminarN').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se elimino correctamente el negocio</center></strong></div>');
							$('#mEliminar').modal('toggle');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							consulta();
							verNegocios();
							actualizarNegocios();
							actualizarDetallesNegocio();
						});
						return false;
					});

					$('#form-cambiaEstado').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
							$('#mTerminar').modal('toggle');
							$('#completarActividad').modal('toggle');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							consulta();
						});
						return false;
					});

					$('#form-AgregarSeguidores').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Seguidor vinculado con exito</center></strong></div>');
							$('#mAgregarSeguidores').modal('toggle');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							consulta();
							actualizarSeguidores();
						});
						return false;
					});

					$('#form-negocios').submit(function() {
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
							success: function(respuesta) {
								$('#mNegocio').modal('toggle');
								$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
								$('#mensajejs').show();
								$('#mensajejs').delay(1000).hide(600);
								consulta();
								actualizarNegocios(); 
							}    
						})        
						return false;
					});

					$("#No").change(function(){
						myFunctionCheckedNo();
					});

					$("#Si").change(function(){
						myFunctionCheckedSi();
					});

					$('#form-eliminar').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							$('#mEliminarA').modal('toggle');
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							consulta();
						});
						return false;
					});

					$('#form-actividades').bootstrapValidator({
						submitHandler: function(validator, form, submitButton) {
							$.ajax({
								type: 'POST',
								url: form.attr('action'),
								data: form.serialize(),
								success: function(respuesta){
									if(respuesta[0].mensaje == "cruzada"){
										$('#divCruzadas').show();
										$("#cruzadas").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
										$('#txtConfirmado').val('true');
										$('#btnGuardarActividad').val('Confirmar');
										$('#btnGuardarActividad').prop('disabled',false);
									}else{
										$('#añadiractividad').modal('toggle');
										$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
										$('#mensajejs').show();
										$('#mensajejs').delay(3500).hide(600);
										ocultaActividades();
										actualizarDetallesActividad();
										consulta();
									}
								}
							})      
							return false;
						},
						fields: {
							options: {
								validators: {
									notEmpty: {
										message: 'Selección Obligatoria'
									}
								}
							},
							notas: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							fecha: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							hora: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							duracion: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							idNegocio: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							}
						}
					});

					$('#form-actividadesE').bootstrapValidator({
						submitHandler: function(validator, form, submitButton) {
							$.ajax({
								type: 'POST',
								url: form.attr('action'),
								data: form.serialize(),
								success: function(respuesta){
									console.log()
									if(respuesta[0].mensaje == "cruzada"){
										$('#divCruzadasE').show();
										$("#cruzadasE").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
										$('#txtConfirmadoE').val('true');
										$('#btnGuardarE').val('Confirmar');
										$('#btnGuardarE').prop('disabled',false);
									}else{
										$('#mActividades').modal('toggle');
										$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
										$('#mensajejs').show();
										$('#mensajejs').delay(3500).hide(600);
										actualizarDetallesActividad();
										consulta();
									}
								}
							})      
							return false;
						},
						fields: {
							options: {
								validators: {
									notEmpty: {
										message: 'Selección Obligatoria'
									}
								}
							},
							notas: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							fecha: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							hora: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
							duracion: {
								validators: {
									notEmpty: {
										message: 'Campo Obligatorio'
									}
								}
							},
						}
					});
					$('#mActividades')
					.on('hidden.bs.modal', function () {
						$('#form-actividadesE').bootstrapValidator('resetForm', true);
					});

					$('#form-notas').bootstrapValidator({
						submitHandler: function(validator, form, submitButton) {
							var notas = $('#txtNotas').val();
							var data = new FormData();
							var file = $("#archivo")[0].files[0];
							data.append('idOrganizacion',idOrganizacion);
							data.append('archivo',file);
							data.append('notas',notas);
							$.ajax({
								type: 'POST',
								url: form.attr('action'),
								data: data,              
								cache: false,
								contentType: false,
								processData: false,       
								success: function(dat){
									if (dat=="Peso") {
										$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Archivo demasiado grande</center></strong></div>');
										$('#mensajejs').show();
										$('#mensajejs').delay(3500).hide(600);
									}else if(dat=="Otro"){
										$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Solo se permiten PDF</center></strong></div>');
										$('#mensajejs').show();
										$('#mensajejs').delay(3500).hide(600);
									}else{
										$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+dat+'</center></strong></div>');
										$('#mensajejs').show();
										$('#mensajejs').delay(3500).hide(600);
									}        	
									ocultaNotas();
									consulta();
								}
							})      
							return false;
						},
						fields: {
							notas: {
								validators: {
									notEmpty: {
										message: 'Debe añadir un contenido a la nota'
									}
								}
							},
						}
					});

					$('#form-negocios-eliminar').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							$('#mEliminar').modal('hide');
							$('#mNegocios').modal('hide');
							consulta();
						});
						return false;
					}); 

					$('#form_persona').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							if (respuesta=="error") {
								$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al eliminar la Persona de contacto</center></strong></div>');
							}else{
								$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
							}if (respuesta =='') {
								$('#mPersona').modal('');
							}else{
								$('#mPersona').modal('toggle');
								$('#form_persona').modal('hide');
								$('#mensajejs').show();
								$('#mensajejs').delay(3500).hide(600);
							}
							consulta();
							actualizarContactos();
							$('#txtIdCliente').val("");
							$('#txtNomPersona').val("");
							$('#txtTelefono').val("");
							$('#txtExtension').val("");
							$('#selectTipoTelefono').val("");
							$('#txtEmail').val("");
							$('#selectHonorifico').val("");
							$('#txtPuesto').val("");
						});
						return false;
					});

					$('#form_personasdel').submit(function(){
						$.ajax({
							type: 'POST',
							url: $(this).attr('action'),
							data: $(this).serialize(),
						}).done(function(respuesta){
							if (respuesta=="error") {
								$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al eliminar la Persona de contacto</center></strong></div>');
							}else{
								$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
							}
							$('#mEliminarContacto').modal('toggle');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							consulta();
							verPersonas();
							actualizarContactos();
						}
						);
						return false;
					}); 

					$('#checkTiempo').on( 'click', function() {
						if( $(this).is(':checked') ){
							$('#divTiempo').show();
							$('#txtHora').focus();
							$('#divCruzadas').show();
							$('#txtConfirmado').val("false");
						} else {
							$('#divTiempo').hide();
							$('#divCruzadas').html("");
							$('#divCruzadas').hide();
							$('#btnGuardarActividad').val('Guardar');
						}
					}); 

					$('#checkTiempoE').on( 'click', function() {
						if( $(this).is(':checked') ){
							$('#divTiempoE').show();
							$('#txtHoraE').focus();
							$('#divCruzadasE').show();
							$('#txtConfirmadoE').val("false");
						} else {
							$('#divTiempoE').hide();
							$('#divCruzadasE').html("");
							$('#divCruzadasE').hide();
							$('#btnGuardarE').val('Guardar');
						}
					}); 
				});

function actualizarContactos(){ 
	var idOrganizacion = <?php echo $_GET['idOrganizacion']; ?>;
	$.get('index.php?c=dealcliente&a=mostrarContactos',  
		{idOrganizacion: idOrganizacion},
		function(contacto) {
			contactos = "";
			for (var i in contacto) {
				contactos = contactos +`<div class="col-lg-12">
				<a href="?c=dealcontacto&idCliente=${contacto[i].idCliente}" class="btn btn-link">${contacto[i].nombrePersona}</a></div>`;
			} 
			$('#divContactos').html(contactos);
		});
}

function actualizarNegocios(){
	var idOrganizacion = <?php echo $_GET['idOrganizacion']; ?>;
	$.get('index.php?c=dealcliente&a=mostrarNegocios',  
		{idOrganizacion: idOrganizacion},
		function(negocio) {
			var contador = Object.keys(negocio).length;
			negocios = "";
			negocios = negocios +`<h5>Negocios Abiertos <span class="badge">`+contador+`</span></h5>`;
			for (var i in negocio) {
				negocios = negocios +`<a href="?c=deal&idNegocio=${negocio[i].idNegocio}">
				<div class="panel panel-default" style="margin-bottom: 5px;">
				<div class="panel-body" style="padding: 6px;">
				<div class="row">
				<div class="col-xs-8 col-lg-8">`;
				if(negocio[i].claveOrganizacion !== null){
					negocios = negocios + `<p class="help-block" style="margin: 0px;"><small>${negocio[i].claveOrganizacion+"-"+negocio[i].claveConsecutivo+"-"+negocio[i].claveServicio}</small></p>`;

				}
				negocios = negocios + `
				<p class="help-block" style="margin: 0px;"><small>${negocio[i].tituloNegocio}</small></p>
				<p class="help-block" style="margin: 0px;"><small>${negocio[i].valorNegocio+ " " + negocio[i].tipoMoneda+ " - " + negocio[i].nombreOrganizacion}</small></p>
				</div>
				<div class="col-xs-4 col-lg-4" align="right">
				<div class="btn-group">
				</div>
				</div>
				</div>
				</div>
				</div>
				</a>`;
			} 
			$('#divNegocios').html(negocios);

		});
}

function actualizarDetallesNegocio() {
	var idOrganizacion = <?php echo $_GET['idOrganizacion']; ?>;
	$.get('index.php?c=dealcliente&a=mostrarDetallesNegocio',  
		{idOrganizacion: idOrganizacion},
		function(negocio) {
			contadorNC=contadores=progress=porcentajes=valorNegocios= "";

			contadorNC = contadorNC +`<h5> Negocios Cerrados <span class="badge" id="cntNC">${negocio[0].NegociosCerrados}</span></h5>`;

			if (negocio[0].PorcietoP != 100) { 
				progress = progress +`<div class="progress-bar progress-bar-success" aria-valuemin="0" aria-valuemax="100" role="progressbar" style="width: ${negocio[0].PorcietoG}%" data-toggle="tooltip" data-placement="top" title="Negocios ganados"></div>`;
			} 
			progress=progress +`<div class="progress-bar progress-bar-danger" aria-valuemin="0" arial-valuenow="1" aria-valuemax="100" role="progressbar" style="width: ${negocio[0].PorcietoP}%" data-toggle="tooltip" data-placement="top" title="Negocios perdidos"></div>`;

			contadores=contadores +`<div class="col-lg-12">
			<p class="help-block" style="margin-bottom: 0px;"><small>${negocio[0].NegociosGanados}</small></p>
			</div>
			<div class="col-lg-12">
			<p class="help-block" style="margin-top: 0px;"><small>${negocio[0].NegociosPerdidos}</small></p>
			</div>`;

			porcentajes=porcentajes +`<div class="col-lg-12">
			<p class="help-block" style="margin-bottom: 0px;"><small><strong>${(negocio[0].PorcietoG).toFixed(1)}%</strong></small></p>
			</div>
			<div class="col-lg-12">
			<p class="help-block" style="margin-top: 0px;"><small><strong>${(negocio[0].PorcietoP).toFixed(1)}%</strong></small></p>
			</div>`;

			valorNegocios=valorNegocios +`<div class="col-lg-12">
			<p class="help-block" style="margin-bottom: 0px;"><small>${negocio[0].SumaGanados} <strong>$MXN</strong></small></p>
			</div>
			<div class="col-lg-12">
			<p class="help-block" style="margin-top: 0px;"><small>${negocio[0].SumaPerdidos} <strong>$MXN</strong></small></p></div>`;

			$('#divContadorNC').html(contadorNC);
			$('#divProgressN').html(progress);
			$('#divContadoresN').html(contadores);
			$('#divPorcentajesN').html(porcentajes);
			$('#divValorNegocio').html(valorNegocios);

		});
}

function actualizarDetallesActividad() {
	var idOrganizacion = <?php echo $_GET['idOrganizacion']; ?>;
	$.get('index.php?c=dealcliente&a=mostrarDetallesActividad',  
		{idOrganizacion: idOrganizacion},
		function(arrayAct) {
			progressAct = tipoAct = cantidadAct = porcentajeAct = "";
			for (var [key, value] of Object.entries(arrayAct[0])) { /*(key, value);*/
				if (value != 0) {
					resultadoPorciento=((value * 100) / arrayAct[1].sumaArray);
					progressAct=progressAct +`<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="${resultadoPorciento}" aria-valuemin="0" aria-valuemax="100" style="width: ${resultadoPorciento}%" data-toggle="tooltip" data-placement="top" title="${key}">
					</div>`;
					tipoAct=tipoAct +`<strong><p class="help-block"> ${key} </p></strong>`;
					cantidadAct=cantidadAct +`<p class="help-block"> ${value} </p>`;
					porcentajeAct=porcentajeAct +`<strong><p class="help-block"> ${resultadoPorciento.toFixed(1)}% </p></strong>`;
				} 
			}
			$('#divProgressAct').html(progressAct);
			$('#divTipoAct').html(tipoAct);
			$('#divCantidadAct').html(cantidadAct);
			$('#divPorcentajeAct').html(porcentajeAct);
		});
}

function actualizarDetallesUserActivo() {
	var idOrganizacion = <?php echo $_GET['idOrganizacion']; ?>;
	var idUsuario = <?php echo $Resultado['idUsuario']; ?>; 
	$.get('index.php?c=dealcliente&a=mostrarDetallesUserActivo',  
		{idOrganizacion: idOrganizacion, idUsuario: idUsuario},
		function(arrayUActivo) {
			progressUA = usuarioAct = usercantidadAct = userporcenAct = "";
			for (var [key, value] of Object.entries(arrayUActivo[0].array)) {
				if (value != 0 ) {
					porciento=((value * 100) / arrayUActivo[1].sumaArray);
					progressUA = progressUA +`<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="${porciento}" aria-valuemin="0" aria-valuemax="100" style="width: ${porciento}%" data-toggle="tooltip" data-placement="top" title="${key}">
					</div>`;
					usuarioAct = usuarioAct + `<p class="help-block">${key}</p>`;
					usercantidadAct = usercantidadAct +`<p class="help-block">${value}</p>`;
					userporcenAct = userporcenAct +`<strong><p class="help-block">${porciento.toFixed(1)}%</p></strong>`;
				}		
			}
			$('#divProgressUA').html(progressUA);
			$('#divUsuarioAct').html(usuarioAct);
			$('#divUserCantAct').html(usercantidadAct);
			$('#divUserPorcenAct').html(userporcenAct);
		});
}

function actualizarSeguidores() {
	var idOrganizacion = <?php echo $_GET['idOrganizacion']; ?>;
	var idUsuario = <?php echo $Resultado['idUsuario']; ?>;

	$.get('index.php?c=dealcliente&a=mostrarSeguidores',  
		{idOrganizacion: idOrganizacion, idUsuario: idUsuario},
		function(arraySeguidores) {
			conthead = seguidores = "";
			conthead = conthead +`<h4><span><a>Seguidores</a></span>
			<span class="badge" id="cntNC">${arraySeguidores[1].countS}</span>
			<input type="hidden" id="txtArrayS" value="${arraySeguidores[0].array}"></h4>`;

			for (var [key, value] of Object.entries(arraySeguidores[2])) {
				seguidores = seguidores +`<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 8px;">
				<div class="col-lg-2 col-xs-12 col-sm-12 col-md-12">
				<img src="../../assets/imagenes/${value.foto}" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;">
				</div>
				<div class="col-lg-10 col-xs-12 col-sm-12 col-md-12">
				<p class="help-block">${value.nombreUsuario}</p> 
				</div>
				</div>`;
			}
			$('#divContHead').html(conthead);
			$('#divUserSeguidores').html(seguidores);
		});
}

function myFunctionEliminaUsuario(idOrganizacion,idUsuario) {
	$('#txtIdOrganizacionEl').val(idOrganizacion);  
	$('#txtidUsuarioEl').val(idUsuario);
}

function myFunctionVerSeguidores() {
	var idUsuario = <?php echo $Resultado['idUsuario']; ?>;
	var array = "";
	var array = $('#txtArrayS').val();
	var array = array.split(",");
	$.post("index.php?c=dealcliente&a=ListarSeguidores", {idUsuario:idUsuario,idOrganizacion:idOrganizacion,array:JSON.stringify(array)}, function(tablaSeguidores) {
		$("#ResuVerS").html(tablaSeguidores);
	});
}

function consulta(valor){
	if (valor==null) {
		if(val==null){
			val=0;
		}
	}else{
		val = valor;
	}
	$.get("index.php?c=dealcliente&a=Consultas", {idOrganizacion:idOrganizacion,valor:val}, function(res) {
		$('#div-linea').html('');
		for (var i in res){
			if(res[i].tipov == 1)
				renderActividad(res[i]);
			else if(res[i].tipov == 2)
				renderNotas(res[i]);
			else if(res[i].tipov == 5)
				renderNegocios(res[i]);
			contadores2();
		}
	});
}

function contadores2()
{
	idOrganizacion = idOrganizacion;
	$.get('?c=dealcliente&a=Contadores', {idOrganizacion:idOrganizacion}, function(res){
		$('#countTotal').html(res[0].countTotal);
		$('#countActividades').html(res[0].countActividades);
		$('#countNotas').html(res[0].countNotas);
		$('#countNegocios').html(res[0].countNegocios);
	});
}

function renderNegocios(res)
{
	var contLinea = $('#div-linea').html();
	date = (res.fecha).substr(5, 2);
	nomMes=obtenerMes(date);
	fecha = (res.fecha).substr(8, 2)+" de "+nomMes+" del "+(res.fecha).substr(0, 4);
	var cont = 
	`<!--Negocios-->
	<article class="timeline-entry">
	<div class="timeline-entry-inner">
	<div class="timeline-icon bg-danger">
	<i class="fas fa-dollar-sign"></i>
	</div>
	<div class="timeline-label">
	<h2>`;
	if (res.status==2) {
		cont = cont + `<a style="color:red">Negocio perdido: </a>`;
	}else if (res.status==1) {
		cont = cont + `<a style="color:green">Negocio ganado: </a>`;
	}else{
		cont = cont + `Negocio creado: `;
	}
	cont = cont + `<a href="?c=deal&idNegocio=${res.idNegocio}">`;

	if(res.claveOrganizacion !== null ){
		cont = cont + `<span>${res.claveOrganizacion} - ${res.claveConsecutivo} - ${res.claveServicio}</span>`;
	}else{
		cont = cont + `<span>En leads</span>`;
	}

	cont = cont + `</a></h2>
	<hr style="margin-top: 0px; ">
	<p>${res.tituloNegocio} $ ${res.valorNegocio} ${res.tipoMoneda}<br>
	${fecha}
	</p>
	</div>
	</div>
	</article>
	<!--Negocios-->`;

	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);
}

function renderActividad(res)
{
	var contLinea = $('#div-linea').html();
	date = (res.fecha).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaActividad = (res.fecha).substr(8, 2)+" de "+nomMes+" del "+(res.fecha).substr(0, 4);
	date = (res.timestamp).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaRegistro = (res.timestamp).substr(8, 2)+" de "+nomMes+" del "+(res.timestamp).substr(0, 4);
	if (res.horaInicio!=null) {
		var duracion=res.horaInicio+' - '+res.horaFin;
	}
	var cont = 
	` <!--Actividades-->
	<article class="timeline-entry">
	<div class="timeline-entry-inner">
	<div class="timeline-icon bg-primary">
	<i class="fas fa-calendar-alt"></i>
	</div>
	<div class="timeline-label">
	<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-right:0px;">
	<div class="btn-group">
	<h2 style="margin-bottom:3px;">
	<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#mTerminar" onclick="cambiaEstado(${res.idActividad},${res.completado},'${res.fechaCompletado}')"> `;
	if (res.completado==0) {
		cont = cont + `<i class="far fa-circle" style="font-size: 16px;"></i>`;
	}else{
		cont = cont + `<i class="fas fa-check-circle" style="font-size: 16px; color: green;"></i>`;
	}
	cont = cont + ` </button>
	<span><a>`;
	if (res.tipo=="Llamada") {
		cont = cont + `<i class="mdi mdi-phone-in-talk"></i>`;
	} else if (res.tipo=="Reunión") {
		cont = cont + `<i class="mdi mdi-group mdi-lg"></i> `;
	}else if (res.tipo=="Tarea") {
		cont = cont + `<i class="far fa-clock" style="font-size: 15px;"></i> `;
	}else if (res.tipo=="Plazo") {
		cont = cont + `<i class="fas fa-hourglass-half" style="font-size: 14px;"></i> `;
	}else if (res.tipo=="Email") {
		cont = cont + `<i class="fas fa-envelope" style="font-size: 16px;"></i> `;
	}else if (res.tipo=="Comida") {
		cont = cont + `<i class="fas fa-utensils" style="font-size: 14px;"></i> `;
	}else{
		cont = cont + `<i class="fab fa-whatsapp" style="font-size: 16px;"></i> `;
	}
	cont = cont + `${res.tipo}</a></span></h2>
	<p><span><i class="fas fa-dollar-sign" style="font-size: 16px;"></i> ${res.tituloNegocio}</span></p>
	</div>
	</div>
	<div class="col-xs-10 col-sm-4 col-md-4 col-lg-4" align="center" style="padding-top: 4px; padding-left:0px;">
	<h2 style="margin-bottom: 7px;"><i class="mdi mdi-business mdi-lg"></i> ${res.nombreOrganizacion}</h2>
	<p><i class="fas fa-user"></i>
	${res.nombrePersona}</p>
	</div> 
	<div class="col-xs-2 col-sm-4 col-md-4 col-lg-4" align="right">
	<button type="button" class="btn-link dropdown-toggle" id="dropdownAct" data-toggle="dropdown" >
	<i class="far fa-edit" style="font-size: 16px;"></i>
	</button>
	<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownAct">
	<li role="presentation" id="ya"><a data-toggle="modal" data-target="#mActividades" href="#actividad" href="#actividad" onclick="editarActividad(${res.idActividad},'${res.tipo}','${res.notas}','${res.fecha}','${res.duracion}','${res.horaInicio}',${res.idOrganizacion},'${res.nombreOrganizacion}',${res.idNegocio},'${res.tituloNegocio}','${res.nombrePersona}');">Editar</a></li>
	<li><a href="#" data-toggle="modal" data-target="#mEliminarA" onclick="eliminarActividad(${res.idActividad}); return false">Eliminar</a></li>
	</ul>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12">
	<hr style="margin-top: 3px; margin-bottom: 10px;">
	</div>
	<div class="col-lg-12">
	<div class="form-group">
	<textarea name="" class="form-control" id="txtNotas0" readonly="">${res.notas}</textarea>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-6 col-sm-6 col-md-6">
	<p style="font-size: 11px;">Fecha: ${fechaActividad}<br>Hora: ${res.horaInicioF} - ${res.horaFinF}</p>
	</div>
	<div class="col-lg-6" col-sm-6 col-md-6 align="right">
	<p class="help-block" style="font-size: 11px;">${res.nombreUsuario}<br>${fechaRegistro}</p>
	</div>
	</div>
	</div>
	</div>
	</article>
	<!--Actividades--> `;

	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);
}

function renderNotas(res)
{
	var contLinea = $('#div-linea').html();
	date = (res.fecha).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaNota = (res.fecha).substr(8, 2)+" de "+nomMes+" del "+(res.fecha).substr(0, 4);
	var cont = `
	<!--Notas-->
	<article class="timeline-entry">
	<div class="timeline-entry-inner">
	<div class="timeline-icon bg-success">
	<i class="fas fa-sticky-note"></i>
	</div>
	<div class="timeline-label">
	<div class="row">
	<div class="col-xs-10 col-lg-11" align="left">
	<h2><i class="mdi mdi-business"></i></i>
	<span><a>${nombreOrganizacion}</a></span></h2>
	</div>
	<div class="col-xs-2 col-lg-1" align="right">
	<button type="button" class="btn btn-link dropdown-toggle" id="dropdownNota" data-toggle="dropdown" >
	<i class="far fa-edit" style="font-size: 16px;"></i>
	</button>
	<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownNota">
	<li><a href="#" onclick="quitarReadOnly('txtNotas${res.idContenido}'); return false">Editar</a></li>
	<li><a href="#" onclick="eliminarNota('${res.idContenido}'); return false">Eliminar</a></li>
	</ul>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-3 col-md-4 col-xs-5" align="left">
	<p class="help-block">${fechaNota}</p>
	</div>
	<div class="col-lg-9 col-md-8 col-xs-7" align="right">
	<p class="help-block">${res.nombreUsuario}</p>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12">
	<hr style="margin-top: 3px; margin-bottom: 10px;">
	</div>
	<div class="col-lg-12">
	<div class="form-group">
	<textarea name="" class="form-control" id="txtNotas${res.idContenido}" readonly>${res.notas}</textarea>
	</div>
	</div>						
	</div>`;
	if(res.archivos!=null){
		cont = cont + `<div class="row">
		<div class="col-lg-12" align="right">
		<h2><i class="far fa-file-pdf" style="font-size: 20px; color: red;"></i> <span><a href="assets/files/archivosNegocios/${res.archivos}" download="${res.archivos}">${res.archivos}</a></span></h2>
		</div> `;
	}
	cont = cont + `</div>
	</div>
	</div>
	</article>
	<!--Notas-->
	`;
	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);
}

function obtenerMes(numero)
{
	switch(numero) {
		case '1':
		return 'Enero';
		break;
		case '2':
		return 'Febrero';
		break;
		case '3':
		return 'Marzo';
		break;
		case '4':
		return 'Abril';
		break;
		case '5':
		return 'Mayo';
		break;
		case '6':
		return 'Junio';
		break;
		case '7':
		return 'Julio';
		break;
		case '8':
		return 'Agosto';
		break;
		case '9':
		return 'Septiembre';
		break;
		case '10':
		return 'Octubre';
		break;
		case '11':
		return 'Noviembre';
		break;
		case '12':
		return 'Diciembre';
		break;
	}
}

function myFunctionAgregarSeguidores(idOrganizacion) {
	$('#txtidClienteS').val(idOrganizacion); 
	$.post("index.php?c=dealcliente&a=NombreContacto", {idOrganizacion: idOrganizacion}, function(mensaje) {
		$("#ResuS").html(mensaje);
	});
	listarUsuarios();
}

function listarUsuarios(){
	var idUsuario = $('#txtidUsuario').val();
	var array = $('#txtArrayS').val();
	var array = array.split(",");
	datos = {array:JSON.stringify(array),idUsuario:idUsuario};
	$.ajax({
		url: "index.php?c=dealcliente&a=ListarUsuarios",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdUsuario").empty();
		var selector = document.getElementById("selectIdUsuario");
		selector.options[0] = new Option("Seleccionar usuario","");
		for (var i in respuesta) {
						// console.log(Object.values(respuesta));
						var j = parseInt(i) + 1;
						selector.options[j] = new Option(respuesta[i].nombreUsuario,respuesta[i].idUsuario);
					}
					$('#selectIdUsuario').selectpicker('refresh');
				}); 
	$("#selectIdUsuario").empty();
	var selectPersona = document.getElementById("selectIdUsuario");
}

function myFunctionNuevo(selectIdOrganizacion) {
	$('#inputOrganizacion').val(idOrganizacion);
	$('#selectEtapa').val("");
	$('#selectIdOrganizacion').val(selectIdOrganizacion);
	$('#selectIdPersona').val("");
	$('#selectIdPersonaA').val("");
	$('#txtClaveNegocio').val("");
	$('#txtTituloNegocio').val("");
	$('#txtValorNegocio').val("");
	$('#selectTipoMoneda').val("MXN");
	$('#txtFechaCierre').val("");
	$('#txtContadorActivo').val("");
	$('#txtStatus').val("");
	$('#selectPonderacion').val("");
	$('#txtXcambio').val("");
	$('#txtServicio').val("");
	$('#divClaveNegocio').hide();
	$('#divPresupuesto').show();
	$('#labelPresupuesto').show();
	$('#btnGuardar').val("Guardar");  
	filtrarPorOrganizaciones();
	listarEquipos();
	listarUnidadNegocio();
	listarPresupuestos();
	myFunctionCheckedNo();
	$.post("index.php?c=negocios&a=ConsultaEtapaPorEmbudo", {valorIdEmbudo: idEmbudo }, function(mensaje) {
		$("#ConsultaEtapa").html(mensaje);
	});
}

function filtrarPorOrganizaciones(){
	datos = {idOrganizacion:idOrganizacion};
	$.ajax({
		url: "index.php?c=negocios&a=listarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#divPersonasF").show();
		$("#selectIdPersonaF").empty();
		var selector = document.getElementById("selectIdPersonaF");
		selector.options[0] = new Option("Seleccione la persona de contacto","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectIdPersonaF').selectpicker('refresh');
	});
	ConsultaClave(idOrganizacion);
	sumaConsecutivo(idOrganizacion);
}

/*Metodo para consultar la clave de organizacion segun el id seleccionado y imprimirlo en la caja de texto*/
function ConsultaClave(idOrganizacion){
	$.post("index.php?c=negocios&a=ConsultaClaveOrganizacion", { valorIdOrganizacion: idOrganizacion }, function(cOrganizacion) {
		$("#txtClaveOrganizacion").val(cOrganizacion);
	});

}
/*Metodo para traer el consecutivo segun la clave de la organizacion y imprimirlo en la caja de texto*/
function sumaConsecutivo(idOrganizacion){
	$.post("index.php?c=negocios&a=ConsultaConsecutivo", { valorIdOrganizacion: idOrganizacion }, function(numConsecutivo) {
		$("#txtConsecutivo").val(numConsecutivo);
	});
}

function listarUnidadNegocio(){
	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarUnidadNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdUnidadNegocio").empty();
		var selectUnidadNegocio = document.getElementById("selectIdUnidadNegocio");
		selectUnidadNegocio.options[0] = new Option("Seleccione la unidad de negocio","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selectUnidadNegocio.options[j] = new Option(respuesta[i].unidadNegocio,respuesta[i].idUnidadNegocio);
		}
		$('#selectIdUnidadNegocio').selectpicker('refresh');
	}); 
}

function listarEquipos(){
	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarEquipo",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdEquipo").empty();
		var selectEquipo = document.getElementById("selectIdEquipo");
		selectEquipo.options[0] = new Option("Seleccione el equipo","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selectEquipo.options[j] = new Option(respuesta[i].nombreEquipo,respuesta[i].idEquipo);
		}
		$('#selectIdEquipo').selectpicker('refresh');
	}); 
}

obtenerIdPersona = function (idPersona){
	var idPersona = $('#selectIdPersonaF').val();
	$('#inputCliente').val(idPersona);
}
function myFunctionCheckedSi(){
	$("#ActivarPresupuesto").show();
	$('#Si').prop('checked', true);
}

function myFunctionCheckedNo(){
	$("#ActivarPresupuesto").hide();
	$('#No').prop('checked', true);
	$('#txtNotas').val("");
}

function listarPresupuestos(){
	datos = {idEmbudo:idEmbudo};
	$.ajax({
		url: "index.php?c=negocios&a=listarPresupuestos",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectPresupuesto").empty();
		var selector = document.getElementById("selectPresupuesto");
		selector.options[0] = new Option("Seleccione presupuesto","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePresupuesto,respuesta[i].idPresupuestoGeneral);
		}
		$('#selectPresupuesto').selectpicker('refresh');
	});
}

function ocultaNotas() {
	$('#Notas').toggle();
	$('#nota').toggle(); 
	$('#Ocultadiv').hide(); 
	$('#txtNotas').val("");
	$('#archivo').val("");
}

function verNotas() {
	$('#Notas').toggle();
	$('#nota').toggle(); 
}

function verActividades() {
	$('#Actividades').toggle();
	$('#actividad').toggle(); 
}

function irActividades() {
	$('#checkTiempo').prop('checked',false);
	$('#divTiempo').hide();
	$('#divCruzadas').hide();
	$('#divFiltro').show();
	$('#txtIdActividad').val(0);
	$('#txtTipo').val(""); 
	$('#txtTituloNegocio').val("");
	$('#txtNombreOrganizacion').val("");
	$('#txtfechaActividad').val("");
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#txtNotasA').val("");
	$('#txtConfirmado').val("false");
	listarPersona();
	listarNegociosPorOrganizacion();
	$('#Notas').hide();
	$('#nota').hide();
	checkTiempo.checked = false;
	$('#Actividades').hide();
	$('#divTiempo').hide();
	$('#actividad').show(); 
}
function ocultaActividades() {
	desmarcar();
	$('#form-actividades').bootstrapValidator('resetForm', true);
	$('#checkTiempo').prop('checked',false);
	$('#divTiempo').hide();
	$('#divCruzadas').hide();
	$('#divFiltro').show();
	$('#txtIdActividad').val(0);
	$('#txtTipo').val(""); 
	$('#txtTituloNegocio').val("");
	$('#txtNombreOrganizacion').val("");
	$('#txtfechaActividad').val("");
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#txtNotasA').val("");
	$('#txtConfirmado').val("false");
	listarPersona();
	listarNegociosPorOrganizacion();
	$('#Actividades').toggle();
	$('#actividad').toggle(); 
	$('#Ocultadiv').hide(); 
}
function irNotas() {
	$('#Actividades').hide();
	$('#checkTiempo').prop('checked',false);
	$('#actividad').hide();
	$('#Notas').hide();
	$('#nota').show(); 
}
function listarPersona(){
	datos = {idOrganizacion:idOrganizacion};
	$.ajax({
		url: "index.php?c=dealcliente&a=ListarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdPersonaAct").empty();
		var selector = document.getElementById("selectIdPersonaAct");
		selector.options[0] = new Option("Seleccione la persona de Contacto","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectIdPersonaAct').selectpicker('refresh');
	}); 
	$("#selectIdPersonaAct").empty();
	var selectPersona = document.getElementById("selectIdPersonaAct");
}

function listarNegociosPorOrganizacion(){
	datos = {idOrganizacion:idOrganizacion};
	$.ajax({
		url: "index.php?c=actividades&a=ListarNegociosPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdNegocioF").empty();
		var selector = document.getElementById("selectIdNegocioF");
		selector.options[0] = new Option("Seleccione el negocio de actividad","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
		}
		$('#selectIdNegocioF').selectpicker('refresh');
	}); 
	$("#selectIdNegocioF").empty();
	var selectPersona = document.getElementById("selectIdNegocioF");
}

/*Metodo para cambiar el estado en la base de datos de acuerdo al idActividad*/
cambiaEstado = function(idActividad,estado,fechaCompletado)
{
	$('#txtIdActividad2').val(idActividad);
	$('#txtEstado').val(estado);
	if(estado==1){
		$('#labActComp').html("Actividad completada");
		$('#Guardar2').val('Desacompletar');
		$('#txtFechaCompletado').val(fechaCompletado);
	}
	else{
		$('#labActComp').html("Completar actividad");
		$('#Guardar2').val('Completar');
		$('#txtFechaCompletado').val('');
	}
}

function AgregarContactos(idOrganizacion, nombreOrganizacion) {
	$('#txtidOrganizacion').val(idOrganizacion); 
	$('#selectIdOrganizaciones').val(nombreOrganizacion); 
}

function verPersonas(nombreOrganizacion) {
	$.post("index.php?c=dealcliente&a=ListarPersonas", {idOrganizacion:idOrganizacion}, function(mensaje) {
		$("#ResumenVerPersonas").html(mensaje);
	});
}

function verNegocios() {
	$.post("index.php?c=dealcliente&a=ListarNegocios", {idOrganizacion:idOrganizacion}, function(mensaje) {
		$("#ResumenVerNegocios").html(mensaje);
	});
}

eliminarContacto = function (idCliente) {
	$('#txtidContactos').val(idCliente);
}

myFunctionEliminaregocio = function (idNegocio, nombreOrg) {
	$('#txtIdNegocioE').val(idNegocio);
	$('#txtNomOrg').val(nombreOrg);
	$('#labTituloE').html("Eliminar Negocio");
}

function quitarReadOnly(id)
{
	$("#"+id).removeAttr("readonly");
	$( "#"+id ).blur(function() {
		$("#"+id).attr("readonly","readonly");
		var contenido = $('#'+id).val();
		patron = "txtNotas",
		nuevoValor    = "",
		id = id.replace(patron, nuevoValor);
		$.post("index.php?c=deal&a=ActualizarNota", {id:id,contenido,contenido}, function(resultado) {
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota actualizada</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
		});
	});
}

function eliminarNota(id)
{
	$.post("index.php?c=deal&a=EliminarNota", {id:id}, function(resultado) {
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota eliminada</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
		consulta();
	});
}

function eliminarActividad(id)
{
	$('#txtIdActividadE').val(id);  
}

tipoActividad = function (tipo)
{
	$('#txtTipo').val(tipo);

	switch(tipo){
		case 'Llamada':
		desmarcar("Llamada");
		break;
		case 'Reunion':
		desmarcar("Reunion");
		break;
		case 'Tarea':
		desmarcar("Tarea");
		break;
		case 'Plazo':
		desmarcar("Plazo");
		break;
		case 'Email':
		desmarcar("Email");
		break;
		case 'Comida':
		desmarcar("Comida");
		break;
		case 'WhatsApp':
		desmarcar("WhatsApp");
		break;
		default: 
		break;
	}
}  

desmarcar = function(tipo) {
	var miarray = ["Llamada", "Reunion", "Tarea", "Plazo", "Email", "Comida", "WhatsApp"];

	for (var i = 0; i < miarray.length; i++) {
		if (tipo == miarray[i]) {
			$("#lab"+miarray[i]).addClass("btn-pressed");
			$("#lab"+miarray[i]).removeClass("btn-default");
		}else{
			$("#lab"+miarray[i]).removeClass("btn-pressed");
			$("#lab"+miarray[i]).addClass("btn-default");
		}
	}
}

function deshabilitaConfirmado(){
	$('#txtConfirmado').val("false");
	$('#divCruzadas').hide();
	$('#btnGuardarActividad').val("Guardar");
	$('#btnGuardarActividad').prop('disabled',false);
}

editarActividad = function(idActividad, tipoActividad, notas, fechaActividad, duracion, horaInicio, idOrganizacion, nombreOrganizacion, idNegocio, tituloNegocio, nombrePersona){
	camposEditar();  

	$('#txtIdActividadE').val(idActividad);  
	$('#txtTipoE').val(tipoActividad);
	$('#txtNotasAE').val(notas);  
	$('#txtfechaActividadE').val(fechaActividad);
	var arregloDuracion = duracion.split(" ");
	$('#txtDuracionE').val(arregloDuracion[0]);
	$('#txtHoraE').val(horaInicio)
	$('#txtIdOrganizacionE').val(idOrganizacion);
	$('#txtNombreOrganizacion').val(nombreOrganizacion);
	$('#txtIdNegocioE').val(idNegocio);
	$('#txtConfirmadoE').val('false');

	switch(tipoActividad) {
		case 'Llamada':
		$('#optionLlamadaE').prop('checked',true);
		break;
		case 'Reunion':
		$('#optionReunionE').prop('checked',true);
		break;
		case 'Tarea':
		$('#optionTareaE').prop('checked',true);
		break;
		case 'Plazo':
		$('#optionPlazoE').prop('checked',true);
		break;
		case 'Email':
		$('#optionEmailE').prop('checked',true);
		break;
		case 'Comida':
		$('#optionComidaE').prop('checked',true);
		break;
		case 'WhatsApp':
		$('#optionWhatsappE').prop('checked',true);
		break;
	}

	if( horaInicio != ''){
		$('#checkTiempoE').prop('checked',true);
		$('#divTiempoE').show();
		$('#btnGuardarE').val('Guardar');
	} else {
		$('#checkTiempo').prop('checked',false);
		$('#divTiempoE').hide();
		$('#btnGuardarE').val('Guardar');
	}

	datos = {idOrganizacion:idOrganizacion};
	$.ajax({
		url: "index.php?c=dealcliente&a=ListarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdPersonaActE").empty();
		var selectIdPersonaAct = document.getElementById("selectIdPersonaActE");
		if (nombrePersona != "")
			selectIdPersonaAct.options[0] = new Option(nombrePersona,nombrePersona);
		else
			selectIdPersonaAct.options[0] = new Option("Seleccione la persona de contacto","");
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].nombrePersona != nombrePersona){
				selectIdPersonaAct.options[++j] = new Option(respuesta[i].nombrePersona,respuesta[i].nombrePersona);
			}
		}
		$('#selectIdPersonaActE').selectpicker('refresh');
	});
	datos = {idOrganizacion:idOrganizacion};
	$.ajax({
		url: "index.php?c=actividades&a=ListarNegociosPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		console.log(respuesta);
		$("#selectIdNegocioFE").empty();
		var selectIdNegocioF = document.getElementById("selectIdNegocioFE");
		if (tituloNegocio != "")
			selectIdNegocioF.options[0] = new Option(tituloNegocio,idNegocio);
		else
			selectIdNegocioF.options[0] = new Option("Seleccione la persona de contacto","");
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].tituloNegocio != tituloNegocio){
				selectIdNegocioF.options[++j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
			}
		}
		$('#selectIdNegocioFE').selectpicker('refresh');
	});
	/*----- Select tipo de duración --------*/
	$('#selectTipoDuracion').empty(); 
	var selectTipoDuracion = document.getElementById("selectTipoDuracion");
	if(arregloDuracion[1] == "minuto" || arregloDuracion[1] == "minutos"){
		selectTipoDuracion.options[0] = new Option("minutos","minutos");
		selectTipoDuracion.options[1] = new Option("horas","horas");
	}
	else{
		selectTipoDuracion.options[0] = new Option("horas","horas");
		selectTipoDuracion.options[1] = new Option("minutos","minutos");
	}
}


camposEditar = function()
{
	$('#cruzadasE').html('');
	$('#divCruzadasE').hide();
	$('#divFiltro').hide();
}


tipoActividadE = function (tipo)
{
	$('#txtTipoE').val(tipo);

	switch(tipo){
		case 'Llamada':
		desmarcarE("Llamada");
		break;
		case 'Reunion':
		desmarcarE("Reunion");
		break;
		case 'Tarea':
		desmarcarE("Tarea");
		break;
		case 'Plazo':
		desmarcarE("Plazo");
		break;
		case 'Email':
		desmarcarE("Email");
		break;
		case 'Comida':
		desmarcarE("Comida");
		break;
		case 'WhatsApp':
		desmarcarE("WhatsApp");
		break;
		default: 
		break;
	}
}  

desmarcarE = function(tipo) {
	var miarray = ["LlamadaE", "ReunionE", "TareaE", "PlazoE", "EmailE", "ComidaE", "WhatsAppE"];

	for (var i = 0; i < miarray.length; i++) {
		if (tipo == miarray[i]) {
			$("#lab"+miarray[i]).addClass("btn-pressed");
			$("#lab"+miarray[i]).removeClass("btn-default");
		}else{
			$("#lab"+miarray[i]).removeClass("btn-pressed");
			$("#lab"+miarray[i]).addClass("btn-default");
		}
	}
}

function deshabilitaConfirmadoE(){
	$('#txtConfirmado').val("false");
	$('#divCruzadasE').hide();
	$('#btnGuardarE').val("Guardar");
	$('#btnGuardarE').prop('disabled',false);
}

function leads(){
	var etapa = $('#selectEtapa option:selected').html();
	if(etapa == "Leads"){
		$('#divClaveNegocio').hide();
	}else{
		$('#divClaveNegocio').show();
	}
	$('#txtNombreEtapa').val(etapa);
}

</script>
