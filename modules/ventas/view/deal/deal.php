<style>
.boton{
	border-radius: 88px 88px 88px 88px;
	-moz-border-radius: 88px 88px 88px 88px;
	-webkit-border-radius: 88px 88px 88px 88px;
	border: 0px solid #000000;
}
.progress-bar {
	border-right: solid 5px #FFF;
}
.progress-bar:last-child {
	border: none; 
}
input.oculto[type="radio"]{
	visibility: hidden;
}
.dropdown-menu{
	min-width: 90px; !important;
}
</style>

<?php 
$idNegocio = $_GET['idNegocio'];
$negocio = $this->model->NegocioDetalles($idNegocio);
?>

<div class="padding-nav">
	<!-- Inputs para cargar ids y utilizarlos en el momdnto indicado -->
	<input type="hidden" id="txtidCliente" value="<?php echo $resultado['idCliente']; ?>">
	<input type="hidden" id="txtidOrganizacion" value="<?php echo $resultado['idOrganizacion']; ?>">
	<input type="hidden" id="idOrganizaciontex" value="<?php echo $resultado['idOrganizacion']; ?>">
	<input type="hidden" id="txtidUsuario" value="<?php echo $resultado['idUsuario']; ?>">

	<!-- Panel donde contiene la información del negocio -->

	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<div class="btn-group" role="group">
							<h3 style="margin-top: 10px;">
								<span>
									<?php
									if (!empty($resultado['claveOrganizacion']))
										echo $resultado['claveOrganizacion'].'-'.$resultado['claveConsecutivo'].'-'.$resultado['claveServicio'].'</b> - '.$resultado['tituloNegocio'];
									else
										echo $resultado['tituloNegocio'];
									?>
								</span></h3>
							</div>				
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" align="right" id="div-gan-perd">
							<div class="btn-group form-group" role="group" >
								<?php 
								if ($resultado['status']==0) {
									echo '<button class="btn btn-success" onclick="status(1,'.$idNegocio.')" >Ganado</button>';
								}elseif ($resultado['status']==1) {
									echo '<button class="btn btn-success boton">Ganado</button>';
								}else{
									echo '<button class="btn btn-danger boton">Perdido</button>';
								}
								echo '</div>
								<div class="btn-group form-group" role="group">';
								if ($resultado['status']==0) {
									echo '<button class="btn btn-danger" onclick="status(2,'.$idNegocio.');listarCausas();">Perdido</button>';
								}elseif ($resultado['status']==1) {
									echo '<button class="btn btn-default" onclick="status(0,'.$idNegocio.')">Reabrir</button>';
								}else{
									echo '<button class="btn btn-default" onclick="status(9,'.$idNegocio.')">Reabrir</button>';
								}
								?>
							</div>
							<div class="btn-group form-group" style="padding-left: 0px; padding-right: 0px;">

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="btn-group" role="group">
								<h4><span class="glyphicon glyphicon-usd"></span><?php echo " " . number_format($resultado['valorNegocio'], 2, '.', ',').' '.$resultado['tipoMoneda'];?></h4>
							</div>	
							<div class="btn-group" role="group">
								<!--<a href="#" class="btn btn-link">Añadir productos</a>-->
							</div>
							<div class="btn-group" role="group">
								<a href="?c=dealcontacto&idCliente=<?php echo $resultado['idCliente']; ?>" class="btn btn-link"><span class="glyphicon glyphicon-user"></span> <?php echo " ".$resultado['nombrePersona']; ?></a>
							</div>
							<div class="btn-group" role="group">
								<a href="?c=dealcliente&idOrganizacion=<?php echo $resultado['idOrganizacion']; ?>" class="btn btn-link"><span class="glyphicon glyphicon-briefcase"></span><?php echo " ".$resultado['nombreOrganizacion']; ?></a>
							</div>	
						</div>
					</div>
					<div class="row">
						<div class="form-row">
							<div class="form-group col-md-6 col-xs-12">
								<label for="inputEmail4">Embudo</label>
								<select id="idEmbudo" class="form-control" onchange="cambiaEmbudo(this.value, <?php echo $idNegocio ?>, <?php echo $resultado['idEtapa']; ?>, <?php echo $embudo->idEmbudo ?>)">
									<option value="<?php echo $embudo->idEmbudo; ?>" selected><?php echo $embudo->nombre; ?></option>
									<?php 
									foreach($this->modelEmbudo->Listar() as $r):
										if ($embudo->nombre!=$r->nombre) 
											echo '<option value="'.$r->idEmbudo.'">'.$r->nombre.'</option>';
									endforeach;		
									?>
								</select>
							</div>
							<div class="form-group col-md-6 col-xs-12">
								<label for="idEtapa">Etapa</label>
								<select id="idEtapa" class="form-control" onchange="cambiaEtapa(this.value, <?php echo $idNegocio ?>, <?php echo $resultado['idEtapa']; ?>)">
									<?php 
									foreach($this->modelN->ListarEtapas($idEmbudo) as $r):
										echo '<option value="'.$r->idEtapa.'"'; 
										echo ($resultado['nombreEtapa']==$r->nombreEtapa) ? 'selected' : ''; 
										echo '>'.$r->nombreEtapa.'</option>';
									endforeach;		
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="right">
							<div class="btn-group" role="group">
								<a href="" class="btn btn-link dropdown-toggle" id="dropdownfecha" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Fecha de cierre prevista">
									<span class="glyphicon glyphicon-calendar"></span>
									<font id="labFechaCierre">
										<?php if (!isset($resultado['fechaCierre'])) {
											echo "Establecer fecha de cierre";
										}elseif($resultado['fechaCierre']=='0000-00-00'){
											echo "Establecer fecha de cierre";
										}else{ 
											$date = substr($resultado['fechaCierre'], 5, 2);
											$nombreMes=$this->model->NombreMes($date);
											echo substr($resultado['fechaCierre'], 8, 2)." de ".$nombreMes." del ".substr($resultado['fechaCierre'], 0, 4);
										} ?>
									</font>
								</a>
								<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownfecha">
									<div class="container" style="width: 300px">
										<div class="col-lg-12">
											<br>
											<li align="center"><input id="txtfechaCierre" type="date" class="form-control" name="" value="<?php echo $resultado['fechaCierre']; ?>"></li>
											<hr size="1">
											<li>
												<div class="row" style="width: 290px">
													<div class="col-xs-3 col-lg-3" align="left">
														<button onclick="eliminaFechaCierre();" class="btn btn-danger btn-sm">
															<span class="glyphicon glyphicon-trash"></span>
														</button>											
													</div>
													<div class="col-xs-4 col-lg-4" align="right">
														<button href="" class="btn btn-default btn-sm">
															Cancelar
														</button>
													</div>
													<div class="col-xs-4 col-lg-4" align="right">
														<button  onclick="registrarFechaCierre();" class="btn btn-success btn-sm">Guardar</button>
													</div>
												</div>
											</li>
										</div>			
									</div>
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>

		<!-- Inicio de contenedor -->

		<!-- Panel lateral donde contiene la información que es parte del negocio -->

		<aside class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6 col-lg-7">
							<h4><span><a>Detalles del negocio</a></span></h4>
						</div>
					</div>
					<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					<div class="row">
						<div class="col-lg-12">
							<div class="col-lg-12">
								<h5>Ponderación: <span style="color:#757575"><?php echo $resultado['ponderacion']; ?></span></h5>
							</div>
							<div class="col-lg-12">
								<h5>Equipo: <span style="color:#757575"><?php echo $resultado['nombreEquipo']; ?></span></h5>
							</div>
							<div class="col-lg-12">
								<h5>Sector industrial: <span style="color:#757575">Minero</span></h5>
							</div>
							<div class="col-lg-12">
								<h5>Unidad de negocio: <span style="color:#757575"><?php echo $resultado['unidadNegocio']; ?></span></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6 col-lg-7">
							<h4><span><a>Cliente</a></span></h4>
						</div>
					</div>
					<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					<br>
					<div class="row">
						<div class="col-lg-12">
							<div class="col-xs-2 col-lg-2">
								<span class="img-rounded"><i class="mdi mdi-business mdi-3x"></i></span>
							</div>
							<div class="col-xs-10 col-lg-10">
								<h5 style="margin-top: 19px;"><a href="?c=dealcliente&idOrganizacion=<?php echo $resultado['idOrganizacion']?>"><?php echo $resultado['nombreOrganizacion']; ?></a></h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="col-lg-12">
								<h5>Dirección: <span style="color:#757575"><?php echo $resultado['direccion']; ?></span></h5>
							</div>
							<div class="col-lg-12">
								<h5>Página web: <span style="color:#757575"><?php echo $resultado['paginaWeb']; ?></span></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-5 col-lg-7">
							<h4><span><a>Contacto</a></span></h4>
						</div>
					</div>
					<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					<br>

					<div class="row">
						<div class="col-lg-12">
							<div class="col-xs-2 col-lg-2">
								<span><i class="mdi mdi-account-circle mdi-3x"></i></span>
							</div>
							<div class="col-xs-10 col-lg-10">
								<h5 style="margin-top: 19px;"><a href="?c=dealcontacto&idCliente=<?php echo $resultado['idCliente']; ?>"><?php echo $contacto->honorifico . " " . $contacto->nombrePersona; ?></a></h5>
							</div>	
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12">
							<div class="col-xs-12 col-lg-12">
								<h5>Teléfono: <span style="color:#757575"><?php echo $contacto->telefono; ?></span></h5>
							</div>
							<div class="col-xs-12 col-lg-12">
								<h5>Correo: <span style="color:#757575"><?php echo $contacto->email; ?></span></h5>
							</div>
							<div class="col-xs-12 col-lg-12">
								<h5>Puesto: <span style="color:#757575"><?php echo $contacto->puesto; ?></span></h5>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6 col-sm-8 col-md-8 col-lg-10">
							<h4><span><a>Participantes</a></span></h4>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-2" align="right">
							<div class="btn-group">
								<button type="button" onclick="myFunctionAgregarPersona(<?php echo $idNegocio; ?>);" data-toggle="modal" data-target="#mAgregarParticiante" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
							</div>
						</div>
					</div>
					<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					<br>
					<input type="hidden" id="txtArray" value="">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<a href="?c=dealcontacto&idCliente=<?php echo $resultado['idCliente']; ?>" class="btn btn-link"> <?php echo $resultado['nombrePersona']; ?></a>
					</div>
					<div id="div-participantes">
						<!-- Aqui se imprimen los participantes (Contactos) del negocio -->
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 5px;">
							<div class="btn-group" align="left">
								<a href="#" data-toggle="modal" onclick="myFunctionVerPersona(<?php echo $resultado['idCliente']; ?>);" data-target="#mVerParticiante"  class="btn btn-default btn-xs">Ver todo</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6 col-lg-12">
							<h4><span><a>Resumen</a></span></h4>
						</div>
					</div>
					<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					<br>
					<div class="row">
						<div class="col-xs-6 col-sm-8 col-md-8 col-lg-7">
							<h5>Antigüedad del negocio</h5>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-5" align="right">
							<h5><strong>
								<?php 
								$FechaAc= date("Y-m-d");
								$contadorActivo = substr($resultado['fechaCreado'], 0, 10);
								$datetime1 = new DateTime($contadorActivo);
								$datetime2 = new DateTime($FechaAc);
								$interval = $datetime1->diff($datetime2);
								echo $interval->format('%a días');
								?>
							</strong></h5>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
							<p class="help-block">Inactivo (días)</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2" align="right">
							<p class="help-block">
								<?php
								$contadorActivo = substr($resultado['contadorActivo'], 0, 10);
								$datetime1 = new DateTime($contadorActivo);
								$datetime2 = new DateTime($FechaAc);
								$interval = $datetime1->diff($datetime2);
								echo $interval->format('%a');
								?>
							</p>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-6">
							<p class="help-block">Creado</p>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-8 col-lg-6" align="right">
							<p class="help-block">
								<?php 
								$date = substr($resultado['fechaCreado'], 5, 2);
								$nombreMes=$this->model->NombreMes($date);
								echo substr($resultado['fechaCreado'], 8, 2)." de ".$nombreMes." de ".substr($resultado['fechaCreado'], 0, 4);
								?>
							</p>
						</div>

						<?php $countActividades=$this->model->ContadorActividades($idNegocio); ?>
						<div <?php if($countActividades==0){ ?> style="display: none;" <?php } ?> >

							<div class="col-xs-12 col-lg-12">
								<p class="help-block">Actividades principales</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="progress" id="divProgressAct">
									<!-- Aqui se imprime la barra de progress de las actividades relacionadas con el negocio -->
								</div>
							</div>
							<div class="col-xs-8 col-sm-7 col-md-7 col-lg-8" id="divTipoAct">
								<!-- Aqui se imprime el tipo de actividad -->
							</div>
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" id="divCantidadAct">
								<!-- Aqui se imprime la cantidad de actividades por el tipo -->
							</div>
							<div class="col-xs-2 col-sm-2 col-md-3 col-lg-2" id="divPorcentajeAct">
								<!-- Aqui se imprime el porcentaje que corresponde al número de actividades -->
							</div>

							<div class="col-xs-12 col-lg-12">
								<p class="help-block">Usuarios más activos</p>
							</div>
							<div class="col-xs-12 col-lg-12">
								<div class="progress" id="divProgressUA">
									<!-- Aqui se imprime la barra de progress de los usuarios mas activos -->
								</div>
							</div>
							<div class="col-xs-8 col-sm-7 col-md-7 col-lg-8" id="divUsuarioAct">
								<!-- Aqui se imprime el nombre del usuario activo -->
							</div>
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" id="divUserCantAct">
								<!-- Aqui se imprime la cantidad de actividades por usuario -->
							</div>
							<div class="col-xs-2 col-sm-3 col-md-3 col-lg-2" id="divUserPorcenAct">
								<!-- Aqui se imprime el porcentaje que corresponde a la cantidad de actividades por usuario -->
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-6 col-sm-8 col-md-8 col-lg-10" id="divContHead">
							<!-- Aqui se imprime el número de seguidores -->
						</div>
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-2" align="right">
							<div class="btn-group">
								<button type="button" onclick="myFunctionAgregarSeguidores(<?php echo $idNegocio; ?>);" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mAgregarSeguidores"><span class="glyphicon glyphicon-plus"></span>
								</button>
							</div>
						</div>
					</div>
					<hr size="2" style="margin-top: 5px; margin-bottom: 5px;">
					<br>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-lg-2 col-xs-12 col-sm-12 col-md-12">
							<img src="../../assets/imagenes/<?php echo $resultado['foto']; ?>" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;">
						</div>
						<div class="col-lg-10 col-xs-12 col-sm-12 col-md-12">
							<p class="help-block"><?php echo $resultado['nombreUsuario']; ?></p> 
						</div>
					</div>
					<div id="divUserSeguidores">
						<!-- Aqui se imprimen los seguidores -->
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 10px;">
							<div class="btn-group">
								<button href="#" data-toggle="modal" data-target="#mVerSeguidores" onclick="myFunctionVerSeguidores(<?php echo $resultado['idUsuario'] ?>);" class="btn btn-default btn-xs">Ver todo</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</aside>

	<!-- Comienza el panel de formularios de notas, actividades, archivos -->

	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<ul class="nav nav-pills nav-justified">
					<li role="presentation" id="liNot" class="active"><a data-toggle="tab" href="#nota" onclick="irNotas();"><i class="far fa-sticky-note"></i> Tomar notas</a></li>
					<li role="presentation" id="liAct"><a data-toggle="tab" href="#actividad" onclick="irActividades();"><i class="far fa-calendar-alt"></i> Añadir actividad</a></li>
				</ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<!-- Sección de notas -->

					<div id="Notas" style="display:none;">
						<div class="bs-example-padded-bottom"> 
							<button type="button"  onclick="irNotas();" class="btn btn-default btn-lg" data-toggle="modal" data-target="#gridSystemModal" style="width: 100%; padding: 20px; border-style: dashed; font-size: 15px; background-color: white"> Haz click aquí para crear una nota </button> 
						</div>
					</div>
					<div id="nota" class="tab-pane fade in active">
						<form method="post" action="?c=deal&a=RegistrarNota" id="form-notas">
							<br>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
								<h2 style="margin-top: -10px; margin-bottom: 20px;">Añadir nota</h2>
							</div>
							<div class="form-group">
								<textarea id="txtNotas" class="form-control autoExpand" placeholder="Ingresa el contenido de tu nota aquí..." name="notas"></textarea>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<label for="archivo" class="col-sm-2 control-label" style="padding-top: 7px;"><i class="fas fa-paperclip"></i> Adjuntar archivo: </label>
								<div class="col-sm-10">
									<input type="file" class="form-control" name="archivo" id="archivo">
								</div>
							</div>
							<div class="messages">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<hr>
							</div>
							<div class="form-group" align="right">
								<button class="btn btn-success" type="submit" >Guardar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" onclick="ocultaNotas()">Cancelar</button> 
							</div>	
						</form>
					</div>
					<div id="Actividades" style="display:none;">
						<div class="bs-example-padded-bottom"> 
							<button type="button"  onclick="irActividades();" class="btn btn-default btn-lg" data-toggle="modal" data-target="#gridSystemModal" style="width: 100%; padding: 20px; border-style: dashed; font-size: 15px; background-color: white"> Haz click aquí para agregar una actividad </button> 
						</div>
					</div>

					<!-- Formulario de actividades -->

					<div id="actividad" class="tab-pane fade">
						<form method="post" action="index.php?c=actividades&a=Guardar" id="form-actividades">
							<div class="form-group">
								<input type="hidden" class="form-control" value="0" name="idActividad" id="txtIdActividad" readonly>
								<input type="hidden" class="form-control" id="txtConfirmado" name="confirmado" readonly>
							</div>
							<br>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h2 style="margin-top: -25px; margin-bottom: 20px;">Añadir actividad</h2>
							</div>
							<div class="row" style="margin-top: -20px">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<font size="1">TIPO DE ACTIVIDAD</font>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="btn-group form-group">
										<label class="btn btn-sm btn-default" id="labLlamada" onclick="tipoActividad('Llamada');" data-toggle="tooltip" title="Llamada">
											<i class="mdi mdi-phone-in-talk mdi-lg"></i>
											<input type="radio" class="oculto" name="options" id="optionLlamada">
										</label>
										<label class="btn btn-sm btn-default" id="labReunion" onclick="tipoActividad('Reunion');" data-toggle="tooltip" title="Reunión">
											<i class="mdi mdi-group mdi-lg"></i>
											<input type="radio" class="oculto" name="options" id="optionReunion">
										</label>
										<label class="btn btn-sm btn-default" id="labTarea" onclick="tipoActividad('Tarea');" data-toggle="tooltip" title="Tarea">
											<span>
												<i class="far fa-clock" style="font-size: 14px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionTarea">
										</label>
										<label class="btn btn-sm btn-default" id="labPlazo" onclick="tipoActividad('Plazo');" data-toggle="tooltip" title="Plazo">
											<span>
												<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionPlazo">
										</label>
										<label class="btn btn-sm btn-default" id="labEmail" onclick="tipoActividad('Email');"  data-toggle="tooltip" title="Correo electrónico">
											<i class="mdi mdi-email mdi-lg"></i>
											<input type="radio" class="oculto" name="options" id="optionEmail">
										</label>
										<label class="btn btn-sm btn-default" id="labComida" onclick="tipoActividad('Comida');" data-toggle="tooltip" title="Comida">
											<span>
												<i class="fas fa-utensils" style="font-size: 13px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionComida">
										</label>
										<label class="btn btn-sm btn-default" id="labWhatsApp" onclick="tipoActividad('WhatsApp');" data-toggle="tooltip" title="WhatsApp">
											<span>
												<i class="fab fa-whatsapp" style="font-size: 15px;"></i>
											</span>
											<input type="radio" class="oculto" name="options" id="optionWhatsapp">
										</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipo" required>
							</div>

							<div class="form-group">
								<textarea id="txtNotasA" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas" required></textarea>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<font size="1">FECHA</font>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<input type="date" class="form-control" name="fecha" id="txtfechaActividad" required />
									</div>
									<div class="col-lg-12">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name='checkTiempo' id="checkTiempo"> Asignar hora y duración
									</label>
								</div>
							</div>

							<div id="divTiempo"> 
								<div class="row">
									<div class="form-group">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<font size="1">HORA</font>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<input type="time" class="form-control" name="hora" id="txtHora" onblur="deshabilitaConfirmado()" />
										</div>
										<div class="col-lg-12">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-lg-12">
											<font size="1">DURACIÓN</font>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<input type="number" class="form-control" name="duracion" id="txtDuracion" min="1" max="100" onblur="deshabilitaConfirmado()"/>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<select class="form-control" name="tipoDuracion" id="selectTipoDuracion" onchange="deshabilitaConfirmado()">
												<option value="minutos">Minutos</option>
												<option value="horas">Horas</option>
											</select>
										</div>
										<div class="col-lg-12">
										</div>
									</div>
								</div>
							</div>

							<input name="radioFiltros" value="negocios" type="hidden">

							<div class="form-group" id="div-personas" style="margin-top: 15px;">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-tower"></span></span>
									<input class="form-control" id="txtNomOrganizacion" name="nombreOrganizacion" readonly="readonly" value="<?php echo $resultado['nombreOrganizacion'] ?>">
									<input type="hidden" id="txtIdOrganizacion" name="idOrganizacion" value="<?php echo $resultado['idOrganizacion'] ?>">
								</div>
							</div>

							<div class="form-group" id="div-personas">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user" style="font-size: 16px;"></i></span>
									<select class="form-control selectpicker" data-live-search="true" name="nombrePersona" id="selectIdPersonaAct">
									</select>
								</div>
							</div>

							<div class="form-group" id="div-negocios">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-briefcase"></span></span>
									<input class="form-control" id="txttituloNegocio" required readonly value="<?php echo $resultado['claveOrganizacion'].'-'.$resultado['claveConsecutivo'].'-'.$resultado['claveServicio'].' - '.$resultado['tituloNegocio']; ?>">
									<input type="hidden" id="txtidNegocio" name="idNegocio" value="<?php echo $resultado['idNegocio']; ?>">
								</div>
							</div>

							<div class="row" id="divCruzadas" style="margin-bottom: -50px;">
								<div class="form-group">
									<div class="col-lg-12">
										<div class="help-block with-errors" id="cruzadas"></div>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<input type="submit" class="btn btn-success" id="btnGuardarActividad" value="Guardar">
								<button type="button" class="btn btn-default" data-dismiss="modal" onclick="ocultaActividades()">Cancelar</button> 
							</div>
						</form>
					</div>			
				</div>
			</div>
		</div>
	</div>

	<!-- Panel de contador de actividades, notas, archivos, cambios -->

	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="panel panel-default">
			<ul class="nav nav-pills nav-justified">
				<li role="presentation">
					<a href="#" onclick="consulta(0); return false">Todos 
						<span class="label label-default" id="countTotal">

						</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#" onclick="consulta(1); return false">Actividades 
						<span class="label label-primary" id="countActividades">

						</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#" onclick="consulta(2); return false">Notas 
						<span class="label label-success" id="countNotas">

						</span>
					</a>
				</li>
				<li role="presentation">
					<a href="#" onclick="consulta(4); return false">Cambios 
						<span class="label label-danger" id="countRegCambios">

						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>


	<!-- Linea del tiempo -->

	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="timeline-centered" id="div-linea">
		<!-- <div style="text-align: center;">
			<span class="label label-default">Planeadas</span>
			<div id="div-planeadas" style="margin-top: 20px;">
			</div>
		</div>
		<div style="text-align: center;" id="div-pasadas">
			<span class="label label-default">Pasadas</span>
		</div> -->
	</div>
</div>

<!--Modal razon perdido-->
<div class="modal fade" id="mPedido" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form-perdido" action="index.php?c=deal&a=CambiaStatus" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header  bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo">Marcar como perdido</label> </strong></h4>
				</div>
				<div class="modal-body">  
					<!-- Cuerpo --> 
					<div class="form-group">
						<h5>Razón perdido</h5>

						<select class="form-control selectpicker" data-live-search="true" name="razonPerdido" id="selectIdCausa" required>
						</select>
					</div>
					<input type="hidden"  value="2" name="valor">
					<input type="hidden" id="idNegocio" name="idNegocio">
					<div class="form-group">
						<h5>Comentarios</h5>
						<textarea id="txtcomentarios" type="text" class="form-control autoExpand" name="comentarios" required style="margin-top: 0px; margin-bottom: 0px; height: 80px;"></textarea>
					</div>
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Marcar como perdido">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar negocio -->

<!--Inicio modal de editar actividad -->
<div class="modal fade" id="mActividades" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=actividades&a=Guardar" enctype="multipart/form-data" role="form" id="form-actividadesE">
			<input type="hidden" class="form-control" name="idActividad" id="txtIdActividadE"/>
			<input type="hidden" class="form-control" name="actividad" value="completa"/>
			<input type="hidden" class="form-control" id="txtConfirmadoE" name="confirmado" readonly>

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Editar actividad</h3>
				</div>
				<div class="modal-body"> 

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<font size="1">TIPO DE ACTIVIDAD</font>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="btn-group form-group">
								<label class="btn btn-sm btn-default" id="labLlamadaE" onclick="tipoActividadE('Llamada');" data-toggle="tooltip" title="Llamada">
									<i class="mdi mdi-phone-in-talk mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionLlamadaE">
								</label>
								<label class="btn btn-sm btn-default" id="labReunionE" onclick="tipoActividadE('Reunion');" data-toggle="tooltip" title="Reunión">
									<i class="mdi mdi-group mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionReunionE">
								</label>
								<label class="btn btn-sm btn-default" id="labTareaE" onclick="tipoActividadE('Tarea');" data-toggle="tooltip" title="Tarea">
									<span>
										<i class="far fa-clock" style="font-size: 14px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionTareaE">
								</label>
								<label class="btn btn-sm btn-default" id="labPlazoE" onclick="tipoActividadE('Plazo');" data-toggle="tooltip" title="Plazo">
									<span>
										<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionPlazoE">
								</label>
								<label class="btn btn-sm btn-default" id="labEmailE" onclick="tipoActividadE('Email');"  data-toggle="tooltip" title="Correo electrónico">
									<i class="mdi mdi-email mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionEmailE">
								</label>
								<label class="btn btn-sm btn-default" id="labComidaE" onclick="tipoActividadE('Comida');" data-toggle="tooltip" title="Comida">
									<span>
										<i class="fas fa-utensils" style="font-size: 13px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionComidaE">
								</label>
								<label class="btn btn-sm btn-default" id="labWhatsAppE" onclick="tipoActividadE('WhatsApp');" data-toggle="tooltip" title="WhatsApp">
									<span>
										<i class="fab fa-whatsapp" style="font-size: 15px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionWhatsappE">
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipoE" required>
					</div>

					<div class="form-group">
						<textarea id="txtNotasAE" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas" required></textarea>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<font size="1">FECHA</font>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="date" class="form-control" name="fecha" id="txtfechaActividadE" required />
							</div>
							<div class="col-lg-12">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name='checkTiempo' id="checkTiempoE"> Asignar hora y duración
							</label>
						</div>
					</div>

					<div id="divTiempoE"> 
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">HORA</font>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="time" class="form-control" name="hora" id="txtHoraE" onblur="deshabilitaConfirmadoE()" />
								</div>
								<div class="col-lg-12">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-lg-12">
									<font size="1">DURACIÓN</font>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<input type="number" class="form-control" name="duracion" id="txtDuracionE" min="1" max="100" onblur="deshabilitaConfirmadoE()"/>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<select class="form-control" name="tipoDuracion" id="selectTipoDuracionE" onchange="deshabilitaConfirmadoE()">
										<option value="minutos">Minutos</option>
										<option value="horas">Horas</option>
									</select>
								</div>
								<div class="col-lg-12">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group" id="div-personas" style="margin-top: 15px;">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-tower"></span></span>
							<input class="form-control" name="nombreOrganizacion" readonly="readonly" value="<?php echo $resultado['nombreOrganizacion'] ?>">
							<input type="hidden" id="txtIdOrganizacionE" name="idOrganizacion" value="<?php echo $resultado['idOrganizacion'] ?>">
						</div>
					</div>

					<div class="form-group" id="div-personas">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user" style="font-size: 16px;"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="nombrePersona" id="selectIdPersonaActE">
							</select>
						</div>
					</div>

					<div class="form-group" id="div-negociosE">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-briefcase"></span></span>
							<input class="form-control" id="txttituloNegocioE" required readonly value="<?php echo $resultado['claveOrganizacion'].'-'.$resultado['claveConsecutivo'].'-'.$resultado['claveServicio'].' - '.$resultado['tituloNegocio']; ?>">
							<input type="hidden" id="txtidNegocio" name="idNegocio" value="<?php echo $resultado['idNegocio']; ?>">
						</div>
					</div>

					<div class="row" id="divCruzadasE" style="margin-bottom: -50px;">
						<div class="form-group">
							<div class="col-lg-12">
								<div class="help-block with-errors" id="cruzadasE"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-xs-2 col-lg-2" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar organización" id="btnEliminar" onclick="eliminarActividad()">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</div>
						<div class="col-xs-10 col-lg-10" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" id="btnGuardarE">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						</div> 
					</div>
				</div>

			</div>
		</form>
	</div>
</div>     
<!--fin modal de editar actividad -->

<!--inicio modal de agregar participante -->
<div class="modal fade" id="mAgregarParticiante" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form  method="post" data-toggle="validator" action="index.php?c=deal&a=AgregarParticipantes" role="form" id="form-AgregarParticiante">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font></font>Agregar Participantes</strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div id="Resu">
							<!--Resultado Nombre de negocio -->
						</div>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user" style="font-size: 16px;"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="idPersona" id="selectIdPersona" required>
								<!-- Aqui se imprime la lista de los contactos -->
							</select>
							<input type="hidden" class="form-control" name="idNegocio" id="txtidNegocioA" value="">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-success" value="Guardar">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--fin modal de agregar participante -->

<!--inicio modal de agregar Seguidores  -->
<div class="modal fade" id="mAgregarSeguidores" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form  method="post" data-toggle="validator" action="index.php?c=deal&a=AgregarSeguidores" enctype="multipart/form-data" role="form" id="form-AgregarSeguidores">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font></font>Agregar Seguidores</strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div id="ResuS">
							<!--Resultado Nombre de negocio -->
						</div>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-search" style="font-size: 16px;"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="idUsuario" id="selectIdUsuario" required>
							</select>
							<input type="hidden" class="form-control" name="idNegocio" id="txtidNegocioS" value="">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-success" value="Guardar">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--fin modal de agregar Seguidores  -->

<!--inicio modal de ver participantes -->
<div class="modal fade" id="mVerParticiante" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form>
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font></font>Ver Participantes</strong></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-lg-12">
							<div class="col-xs-2 col-lg-1">
								<span class="glyphicon glyphicon-usd mdi-3x"></span>
							</div>
							<div class="col-xs-8 col-lg-9">
								<h4><strong><label><?php echo $negocio['tituloNegocio']; ?></label></strong></h4>
							</div>
							<div class="col-xs-2 col-lg-2" align="right">
								<div class="btn-group">
								</div>
							</div>
						</div>					
					</div>
					<div class="row">
						<div class="col-xs-12 col-lg-12">
							<hr style="margin-top: 5px; margin-bottom: 10px;">
							<div style="overflow-x: auto;">
								<div class="table-responsive">
									<div id="ResuVerP"> </div>
								</div>
							</div>
						</div>	
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--fin modal de ver participantes -->

<!--inicio modal de ver seguidores  -->
<div class="modal fade" id="mVerSeguidores" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form>
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font></font>Seguidores</strong></h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-lg-12">
							<div class="col-xs-2 col-lg-1">
								<span class="glyphicon glyphicon-usd mdi-3x"></span>
							</div>
							<div class="col-xs-8 col-lg-9">
								<h4><strong><label><?php echo $negocio['tituloNegocio']; ?></label></strong></h4>
							</div>
							<div class="col-xs-2 col-lg-2" align="right">
								<div class="btn-group">
								</div>
							</div>
						</div>					
					</div>
					<div class="row">
						<div class="col-xs-12 col-lg-12">
							<hr style="margin-top: 5px; margin-bottom: 10px;">
							<div style="overflow-x: auto;">
								<div class="table-responsive">
									<div id="ResuVerS"></div>
								</div>
							</div>
						</div>	
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--fin modal de ver seguidores -->

<!--Inicio modal de confirmar para eliminar persona -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form_contactodel" action="index.php?c=deal&a=EliminarParticipantes" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Desvincular participante</strong></h4>
				</div>
				<div class="modal-body">  
					<p style="font-size: 18px;">¿Está seguro que quieres desvincular al contacto?</p>
					<input type="hidden" class="form-control" name="idCliente" id="txtidClienteE" value="">
					<input type="hidden" class="form-control" name="idNegocio" id="txtidNegocioE" value="">
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Aceptar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar persona -->

<!--Inicio modal de confirmar para eliminar usuarios -->
<div class="modal fade" id="mEliminarS" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form_seguidoresdel" action="index.php?c=deal&a=EliminarSeguidores" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Desvincular usuario</strong></h4>
				</div>
				<div class="modal-body" style="padding-bottom: 0px;">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea desvincular el usuario?</p>
					<input type="hidden" class="form-control" name="idUsuario" id="txtidUsuarioE" value="">
					<input type="hidden" class="form-control" name="idNegocio" id="txtidNegocioES" value="">
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Aceptar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar usuarios -->

<!--Inicio modal de etapas de enbudo -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="mEtapas" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form_etapas" action="index.php?c=deal&a=CambiaEmbudo" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-gray">
					<button type="button" class="close" data-dismiss="modal" onclick="restartSelectEtapas()">&times;</button>
					<h4 class="modal-title"><strong>Embudo <label id="labEmbudoM"></label></strong></h4>
				</div>
				<input name="idNegocio" id="idNegocioM" hidden>
				<input name="idEmbudo" id="idEmbudoM" hidden>
				<input name="idOldEtapa" id="idOldEtapaM" hidden>
				<input name="idOldEmbudo" id="idOldEmbudoM" hidden>

				<div class="modal-body" style="padding-bottom: 0px;">  
					
					<!-- Cuerpo --> 
					
					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;">
								<h4>Seleccione la etapa del embudo</h4>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<select id="idEtapaM" class="form-control custom-select" multiple size="10" name="idEtapa">
								</select>
							</div>
						</div>
					</div>

					<div id="divClave">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px; margin-top:3px;">
									<h4>Clave del Negocio</h4>
								</div>
							</div>
						</div>

						<div class="row" style="padding-bottom: 5px;">
							<div class="form-group">
								<div class="col-xs-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-4 col-md-4 col-lg-4">
											<p class="help-block">Clave</p>
										</div>
										<div class="col-xs-4 col-md-4 col-lg-4">
											<p class="help-block">Consecutivo</p>
										</div>
										<div class="col-xs-4 col-md-4 col-lg-4">
											<p class="help-block">Servicio</p>
										</div>
									</div>
								</div>
								<div class="col-xs-4 col-md-4 col-lg-4">
									<input type="text" class="form-control" maxlength="3" name="claveOrganizacion" id="txtClaveOrganizacion" readonly>
								</div>

								<div class="col-xs-4 col-md-4 col-lg-4">
									<input type="text" class="form-control" maxlength="4" name="claveConsecutivo" id="txtConsecutivo" value="0000" readonly>
								</div>
								<div class="col-xs-4 col-md-4 col-lg-4">
									<select name="claveServicio" id="txtServicio" class="form-control">
										<option value="">---</option>
										<option value="MPV" title="Mantenimiento preventivo">MPV</option>
										<option value="AST" title="Asesoría Técnica">AST</option>
										<option value="FTA" title="Fabricación">FTA</option>
										<option value="MCC" title="Mantenimiento Correctivo en Campo">MCC</option>
										<option value="MCT" title="Mantenimiento Correctivo en Taller">MCT</option>
										<option value="CBM" title="Mantenimiento basado en la condición">CBM</option>
										<option value="MEM" title="Montaje">MEM</option>
										<option value="SMR" title="Suministros">SMR</option>
										<option value="VVA" title="Varios">VVA</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<!-- fin cuerpo --> 

				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-success" value="Guardar">
						<button type="button" class="btn btn-default" data-dismiss="modal" onclick="restartSelectEtapas()">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de cambiar embudo -->

<!--Inicio modal de cambio de embudo existoso-->
<div class="modal fade" id="mCambioExitoso" role="dialog">   
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header bg-success">
				<p style="font-size: 18px; margin-bottom: -4px"><i class="fas fa-info-circle"></i> <strong> Aviso </strong></p>
			</div>
			<div class="modal-body" style="margin-bottom: -30px;">  
				<p style="font-size: 20px;" id="mensaje">Se ha cambiado correctamente de embudo este negocio</p>
				<!-- fin cuerpo --> 
			</div>
			<div class="modal-footer">
				<div class="col-xs-12 col-sm-12 col-lg-12" align="right">  
					<a href="?c=deal&idNegocio=<?php echo $idNegocio ?>" type="submit" class="btn btn-success">Terminar</a>    
				</div>
				<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
			</div>
		</div>
	</div>
</div>     
<!--fin modal de cambio exitoso-->

<div class="modal fade" id="mAsignarClave" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=negocios&a=CambiarClave" role="form" id="form_clave">
			<!-- Modal content-->
			<input id="idNegocioMC" name="idNegocio" hidden>
			<div class="modal-content">
				<div class="modal-header bg-gray">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Clave de negocio</strong></h4>
				</div>
				<div class="modal-body">  
					<div class="row" style="padding-bottom: 5px;">
						<div class="form-group">
							<div class="col-xs-12 col-md-12 col-lg-12">
								<div class="row">
									<div class="col-xs-4 col-md-4 col-lg-4">
										<p class="help-block">Clave</p>
									</div>
									<div class="col-xs-4 col-md-4 col-lg-4">
										<p class="help-block">Consecutivo</p>
									</div>
									<div class="col-xs-4 col-md-4 col-lg-4">
										<p class="help-block">Servicio</p>
									</div>
								</div>
							</div>
							<div class="col-xs-4 col-md-4 col-lg-4">
								<input type="text" class="form-control" maxlength="3" name="claveOrganizacion" id="txtClaveOrganizacionM" placeholder="-- -- --" readonly>
							</div>

							<div class="col-xs-4 col-md-4 col-lg-4">
								<input type="text" class="form-control" maxlength="4" name="claveConsecutivo" id="txtConsecutivoM" value="0000" readonly="">
							</div>
							<div class="col-xs-4 col-md-4 col-lg-4">
								<select name="claveServicio" id="txtServicio" class="form-control">
									<option value="">---</option>
									<option value="MPV" title="Mantenimiento preventivo">MPV</option>
									<option value="AST" title="Asesoría Técnica">AST</option>
									<option value="FTA" title="Fabricación">FTA</option>
									<option value="MCC" title="Mantenimiento Correctivo en Campo">MCC</option>
									<option value="MCT" title="Mantenimiento Correctivo en Taller">MCT</option>
									<option value="CBM" title="Mantenimiento basado en la condición">CBM</option>
									<option value="MEM" title="Montaje">MEM</option>
									<option value="SMR" title="Suministros">SMR</option>
									<option value="VVA" title="Varios">VVA</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-success" value="Guardar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="mTerminar" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form  method="post" data-toggle="validator" action="index.php?c=actividades&a=CambiaEstado" enctype="multipart/form-data" role="form" id="form-cambiaEstado">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font id="labActComp"></font></strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="">Fecha:</label>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="date" class="form-control" name="fechaCompletado" required id="txtFechaCompletado" >
						</div>
					</div>
				</div>

				<input type="hidden" id="txtEstado" name="completado">
				<input type="hidden" id="txtIdActividad2" name="idActividad">

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<input type="submit" class="btn btn-success" id="Guardar2">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="mEliminarA" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=actividades&a=Eliminar" enctype="multipart/form-data" id="form-eliminar">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar actividad</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar la actividad?</p>
					<input id="txtIdActividadEl" name="idActividad" hidden>
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de eliminar negocio -->
<script>
	var val = null;
	var idOrganizacion = <?php echo $resultado['idOrganizacion']; ?>;
	var claveOrganizacion = <?php echo $claveOrganizacion; ?>;

	var idNegocio= <?php echo $_REQUEST['idNegocio']; ?>;
	var idEmbudo= <?php echo $_SESSION['idEmbudo']; ?>;
	var selectOld="";
	var idEmbudoModule = "";
	var idEtapaModule = "";
	var idOldEtapaModule = "";

	$(document).ready(function(){
		ocultaNotas();
		consulta();
		actualizarParticipantes();
		actualizarDetallesActividad();
		actualizarDetallesUserActivo();
		actualizarSeguidores();

		$('#form_contactodel').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Participante desvinculado con éxito</center></strong></div>');
				$('#mVerParticiante').modal('toggle');
				$('#mEliminar').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
				consulta();
				actualizarParticipantes();
			});
			return false;
		});

		$('#idEtapaM').change(function(){
			var etapa = $('#idEtapaM option:selected').html();
			if(etapa!='Leads'){
				if(claveOrganizacion==0){
					$.get('index.php?c=negocios&a=ConsultaClaveOrganizacion', {valorIdOrganizacion: idOrganizacion}, function(NewClaveOrganizacion){
						$('#txtClaveOrganizacion').val(NewClaveOrganizacion);
					});	
					$.get('index.php?c=negocios&a=ConsultaConsecutivo', {valorIdOrganizacion: idOrganizacion}, function(consecutivo){
						$('#txtConsecutivo').val(consecutivo);
					});	
					$('#divClave').show();
				}
			}else{
				$('#divClave').hide();
			}
		});

		$('#form_seguidoresdel').submit(function(){
			$.ajax({   
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Seguidor desvinculado con éxito</center></strong></div>');
				$('#mVerSeguidores').modal('toggle');
				$('#mEliminarS').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
				consulta();
				actualizarSeguidores();
			});
			return false;
		});

		$('#form-AgregarParticiante').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Participante vinculado con éxito</center></strong></div>');
				$('#mAgregarParticiante').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
				consulta();
				actualizarParticipantes();
			});
			return false;
		}); 

		$('#form_etapas').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
				}).done(function(){
					$('#mCambioExitoso').modal({backdrop: 'static', keyboard: false})
				});
				return false;
			},
			fields: {
				idEtapa: {
					validators: {
						notEmpty: {
							message: 'Selecciona una etapa'
						}
					}
				},
				claveServicio: {
					validators: {
						notEmpty: {
							message: 'HOLA'
						}
					}
				},
			}
		});

		$('#mEtapas')
		.on('hidden.bs.modal', function () {
			$('#form_etapas').bootstrapValidator('resetForm', true);
		});

		$('#form_clave').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
				}).done(function(){
					$('#mAsignarClave').modal('toggle');
					actualizaEtapa();
				});
				return false;
			},
			fields: {
				claveServicio: {
					validators: {
						notEmpty: {
							message: ' '
						}
					}
				},
			}
		});

		$('#mAsignarClave')
		.on('hidden.bs.modal', function () {
			$('#form_clave').bootstrapValidator('resetForm', true);
		});

		$('#form-AgregarSeguidores').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){	
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Seguidor vinculado con éxito</center></strong></div>');
				$('#mAgregarSeguidores').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
				consulta();
				actualizarSeguidores();
			});
			return false;
		}); 

		$('#form-perdido').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				$('#mPedido').modal('toggle');
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Este negocio se ha perdido</center></strong></div>');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);		
				consulta(); 
				botonesGanadoPerdido();
			});
			return false;
		});

		$('#form-cambiaEstado').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
				$('#mTerminar').modal('toggle');
				$('#completarActividad').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
				consulta();
			});
			return false;
		});

		$('#form-eliminar').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				$('#mEliminarA').modal('toggle');
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
				consulta();
			});
			return false;
		});

		$('#form-actividades').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					success: function(respuesta){
						if(respuesta[0].mensaje == "cruzada"){
							$('#divCruzadas').show();
							$("#cruzadas").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
							$('#txtConfirmado').val('true');
							$('#btnGuardarActividad').val('Confirmar');
							$('#btnGuardarActividad').prop('disabled',false);
						}else{
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							ocultaActividades();
							consulta();
							actualizarDetallesActividad();
							actualizarDetallesUserActivo();
						}
					}
				})      
				return false;
			},
			fields: {
				options: {
					validators: {
						notEmpty: {
							message: 'Selección Obligatoria'
						}
					}
				},
				notas: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				fecha: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				hora: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				duracion: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
			}
		});

		$('#form-actividadesE').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					success: function(respuesta){
						if(respuesta[0].mensaje == "cruzada"){
							$('#divCruzadasE').show();
							$("#cruzadasE").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
							$('#txtConfirmadoE').val('true');
							$('#btnGuardarE').val('Confirmar');
							$('#btnGuardarE').prop('disabled',false);
						}else{
							$('#mActividades').modal('toggle');
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							consulta();
						}
					}
				})      
				return false;
			},
			fields: {
				options: {
					validators: {
						notEmpty: {
							message: 'Selección Obligatoria'
						}
					}
				},
				notas: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				fecha: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				hora: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				duracion: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
			}
		});
		
		$('#mActividades')
		.on('hidden.bs.modal', function () {
			$('#form-actividadesE').bootstrapValidator('resetForm', true);
		});

		$('#checkTiempo').on( 'click', function() {
			if( $(this).is(':checked') ){
				$('#divTiempo').show();
				$('#txtHora').focus();
				$('#divCruzadas').show();
				$('#txtConfirmado').val("false");
			} else {
				$('#divTiempo').hide();
				$('#divCruzadas').html("");
				$('#divCruzadas').hide();
				$('#btnGuardarActividad').val('Guardar');
			}
		}); 

		$('#checkTiempoE').on( 'click', function() {
			if( $(this).is(':checked') ){
				$('#divTiempoE').show();
				$('#txtHoraE').focus();
				$('#divCruzadasE').show();
				$('#txtConfirmadoE').val("false");
			} else {
				$('#divTiempoE').hide();
				$('#divCruzadasE').html("");
				$('#divCruzadasE').hide();
				$('#btnGuardarE').val('Guardar');
			}
		}); 

		$('#form-notas').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				var idCliente = $('#txtidCliente').val();
				var idOrganizacion = $('#txtidOrganizacion').val();
				var notas = $('#txtNotas').val();
				var data = new FormData();
				var file = $("#archivo")[0].files[0];
				data.append('idNegocio',idNegocio);
				data.append('idCliente',idCliente);
				data.append('idOrganizacion',idOrganizacion);
				data.append('archivo',file);
				data.append('notas',notas);
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: data,           
					cache: false,
					contentType: false,
					processData: false,       
					success: function(dat){
						if (dat=="Peso") {
							$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Archivo demasiado grande</center></strong></div>');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
						}else if(dat=="Otro"){
							$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Solo se permiten PDF</center></strong></div>');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
						}else{
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+dat+'</center></strong></div>');
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
						}        	
						ocultaNotas();
						consulta();
					}
				})      
				return false;
			},
			fields: {
				notas: {
					validators: {
						notEmpty: {
							message: 'Debe añadir un contenido a la nota'
						}
					}
				},
			}
		});
	});

function actualizarDetallesActividad() {
	var idNegocio = <?php echo $_GET['idNegocio']; ?>;
	$.get('index.php?c=deal&a=mostrarDetallesActividad',  
		{idNegocio: idNegocio},
		function(arrayAct) {
			progressAct = tipoAct = cantidadAct = porcentajeAct = "";
			for (var [key, value] of Object.entries(arrayAct[0])) { /*(key, value);*/
				if (value != 0) {
					resultadoPorciento=((value * 100) / arrayAct[1].sumaArray);
					progressAct=progressAct +`<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="${resultadoPorciento}" aria-valuemin="0" aria-valuemax="100" style="width: ${resultadoPorciento}%" data-toggle="tooltip" data-placement="top" title="${key}">
					</div>`;
					tipoAct=tipoAct +`<strong><p class="help-block"> ${key} </p></strong>`;
					cantidadAct=cantidadAct +`<p class="help-block"> ${value} </p>`;
					porcentajeAct=porcentajeAct +`<strong><p class="help-block"> ${resultadoPorciento.toFixed(1)}% </p></strong>`;
				} 
			}
			$('#divProgressAct').html(progressAct);
			$('#divTipoAct').html(tipoAct);
			$('#divCantidadAct').html(cantidadAct);
			$('#divPorcentajeAct').html(porcentajeAct);
		});
}

function actualizarDetallesUserActivo() {
	var idNegocio = <?php echo $_GET['idNegocio']; ?>;
	var idUsuario = <?php echo $negocio['idUsuario']; ?>; 
	$.get('index.php?c=deal&a=mostrarDetallesUserActivo',  
		{idNegocio: idNegocio, idUsuario: idUsuario},
		function(arrayUActivo) {
			progressUA = usuarioAct = usercantidadAct = userporcenAct = "";
			for (var [key, value] of Object.entries(arrayUActivo[0].array)) {
				if (value != 0 ) {
					porciento=((value * 100) / arrayUActivo[1].sumaArray);
					progressUA = progressUA +`<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="${porciento}" aria-valuemin="0" aria-valuemax="100" style="width: ${porciento}%" data-toggle="tooltip" data-placement="top" title="${key}">
					</div>`;
					usuarioAct = usuarioAct + `<p class="help-block">${key}</p>`;
					usercantidadAct = usercantidadAct +`<p class="help-block">${value}</p>`;
					userporcenAct = userporcenAct +`<strong><p class="help-block">${porciento.toFixed(1)}%</p></strong>`;
				}		
			}
			$('#divProgressUA').html(progressUA);
			$('#divUsuarioAct').html(usuarioAct);
			$('#divUserCantAct').html(usercantidadAct);
			$('#divUserPorcenAct').html(userporcenAct);
		});
}

function actualizarSeguidores() {
	var idNegocio = <?php echo $_GET['idNegocio']; ?>;
	var idUsuario = <?php echo $negocio['idUsuario']; ?>;
	$.get('index.php?c=deal&a=mostrarSeguidores',  
		{idNegocio: idNegocio, idUsuario: idUsuario},
		function(arraySeguidores) {
			conthead = seguidores = "";
			conthead = conthead +`<h4><span><a>Seguidores</a></span>
			<span class="badge" id="cntNC">${arraySeguidores[1].countS}</span>
			<input type="hidden" id="txtArrayS" value="${arraySeguidores[0].array}"></h4>`;

			for (var [key, value] of Object.entries(arraySeguidores[2])) {
				seguidores = seguidores +`<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 8px;">
				<div class="col-lg-2 col-xs-12 col-sm-12 col-md-12">
				<img src="../../assets/imagenes/${value.foto}" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;">
				</div>
				<div class="col-lg-10 col-xs-12 col-sm-12 col-md-12">
				<p class="help-block">${value.nombreUsuario}</p> 
				</div>
				</div>`;
			}
			$('#divContHead').html(conthead);
			$('#divUserSeguidores').html(seguidores);
		});
}

function listarContactos()
{
	$.get('?c=deal&a=ListarParticipantes', 
		{idNegocio:idNegocio}, 
		function(respuesta){
			$("#selectNombrePersona").empty();
			var selector = document.getElementById("selectNombrePersona");
			selector.options[0] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
			var j=0;
			for (var i in respuesta) {
				selector.options[++j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
			}
			$('#selectIdPersonaAct').selectpicker('refresh');
			$("#selectIdPersonaAct").empty();
			var selectPersona = document.getElementById("selectIdPersonaAct");
		});
}

function consulta(valor){
	if (valor==null) {
		if(val==null){
			val=0;
		}
	}else{
		val = valor;
	}
	$.get("index.php?c=deal&a=Consulta", {idNegocio:idNegocio,valor:val}, function(res) {
		$('#div-linea').html('');
		for (var i in res){
			if(res[i].tipov == 1)
				renderActividad(res[i]);
			else if(res[i].tipov == 2)
				renderNotas(res[i]);
			else if(res[i].tipov == 3)
				renderArchivos(res[i]);
			else if(res[i].tipov == 4)
				renderCambios(res[i]);
		}
	});
	contadores();
}

function renderActividad(res)
{
	var contLinea = $('#div-linea').html();
	date = (res.fecha).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaActividad = (res.fecha).substr(8, 2)+" de "+nomMes+" del "+(res.fecha).substr(0, 4);
	date = (res.timestamp).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaRegistro = (res.timestamp).substr(8, 2)+" de "+nomMes+" del "+(res.timestamp).substr(0, 4);
	if (res.horaInicio!=null) {
		var duracion=res.horaInicio+' - '+res.horaFin;
	}
	var cont = 
	` <!--Actividades-->
	<article class="timeline-entry">
	<div class="timeline-entry-inner">
	<div class="timeline-icon bg-primary">
	<i class="fas fa-calendar-alt"></i>
	</div>
	<div class="timeline-label">
	<div class="row">
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-right:0px;">
	<div class="btn-group">
	<h2 style="margin-bottom:3px;">
	<button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#mTerminar" onclick="cambiaEstado(${res.idActividad},${res.completado},'${res.fechaCompletado}')"> `;
	if (res.completado==0) {
		cont = cont + `<i class="far fa-circle" style="font-size: 16px;"></i>`;
	}else{
		cont = cont + `<i class="fas fa-check-circle" style="font-size: 16px; color: green;"></i>`;
	}
	cont = cont + ` </button>
	<span><a>`;
	if (res.tipo=="Llamada") {
		cont = cont + `<i class="mdi mdi-phone-in-talk"></i>`;
	} else if (res.tipo=="Reunión") {
		cont = cont + `<i class="mdi mdi-group mdi-lg"></i> `;
	}else if (res.tipo=="Tarea") {
		cont = cont + `<i class="far fa-clock" style="font-size: 15px;"></i> `;
	}else if (res.tipo=="Plazo") {
		cont = cont + `<i class="fas fa-hourglass-half" style="font-size: 14px;"></i> `;
	}else if (res.tipo=="Email") {
		cont = cont + `<i class="fas fa-envelope" style="font-size: 16px;"></i> `;
	}else if (res.tipo=="Comida") {
		cont = cont + `<i class="fas fa-utensils" style="font-size: 14px;"></i> `;
	}else{
		cont = cont + `<i class="fab fa-whatsapp" style="font-size: 16px;"></i> `;
	}
	cont = cont + `${res.tipo}</a></span></h2>
	<p><span><i class="fas fa-dollar-sign" style="font-size: 16px;"></i> ${res.tituloNegocio}</span></p>
	</div>
	</div>
	<div class="col-xs-10 col-sm-4 col-md-4 col-lg-4" align="center" style="padding-top: 4px; padding-left:0px;">
	<h2 style="margin-bottom: 7px;"><i class="mdi mdi-business mdi-lg"></i> ${res.nombreOrganizacion}</h2>
	<p><i class="fas fa-user"></i>
	${res.nombrePersona}</p>
	</div> 
	<div class="col-xs-2 col-sm-4 col-md-4 col-lg-4" align="right">
	<button type="button" class="btn-link dropdown-toggle" id="dropdownAct" data-toggle="dropdown" >
	<i class="far fa-edit" style="font-size: 16px;"></i>
	</button>
	<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownAct">
	<li role="presentation" id="ya"><a data-toggle="modal" data-target="#mActividades" href="#actividad" href="#actividad" onclick="editarActividad(${res.idActividad},'${res.tipo}','${res.notas}','${res.fecha}','${res.duracion}','${res.horaInicio}',${res.idOrganizacion},'${res.nombreOrganizacion}',${res.idNegocio},'${res.tituloNegocio}','${res.nombrePersona}');">Editar</a></li>
	<li><a href="#" data-toggle="modal" data-target="#mEliminarA" onclick="eliminarActividad(${res.idActividad}); return false">Eliminar</a></li>
	</ul>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12">
	<hr style="margin-top: 3px; margin-bottom: 10px;">
	</div>
	<div class="col-lg-12">
	<div class="form-group">
	<textarea name="" class="form-control" id="txtNotas0" readonly="">${res.notas}</textarea>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-6 col-sm-6 col-md-6">
	<p style="font-size: 11px;">Fecha: ${fechaActividad}<br>Hora: ${res.horaInicioF} - ${res.horaFinF}</p>
	</div>
	<div class="col-lg-6" col-sm-6 col-md-6 align="right">
	<p class="help-block" style="font-size: 11px;">${res.nombreUsuario}<br>${fechaRegistro}</p>
	</div>
	</div>
	</div>
	</div>
	</article>
	<!--Actividades--> `;

	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);

	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);
}

function renderNotas(res)
{
	var contLinea = $('#div-linea').html();
	date = (res.fecha).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaNota = (res.fecha).substr(8, 2)+" de "+nomMes+" del "+(res.fecha).substr(0, 4);
	var cont = `
	<!--Notas-->
	<article class="timeline-entry">
	<div class="timeline-entry-inner">
	<div class="timeline-icon bg-success">
	<i class="fas fa-sticky-note"></i>
	</div>
	<div class="timeline-label">
	<div class="row">
	<div class="col-xs-10 col-lg-11" align="left">
	<h2><i class="fas fa-dollar-sign" style="font-size: 16px;"></i>`;
	if(res.claveOrganizacion !== null )
		cont = cont + `<span>${res.claveOrganizacion}-${res.claveConsecutivo}-${res.claveServicio} </span>`;
	else
		cont = cont + `<span> Leads </span>`;
	
	cont = cont + `${res.tituloNegocio}</h2></div>
	<div class="col-xs-2 col-lg-1" align="right">
	<button type="button" class="btn btn-link dropdown-toggle" id="dropdownNota" data-toggle="dropdown" >
	<i class="far fa-edit" style="font-size: 16px;"></i>
	</button>
	<ul class="dropdown-menu dropdown-menu-right btn-sm" aria-labelledby="dropdownNota">
	<li><a href="#" onclick="quitarReadOnly('txtNotas${res.idContenido}'); return false">Editar</a></li>
	<li><a href="#" onclick="eliminarNota('${res.idContenido}'); return false">Eliminar</a></li>
	</ul>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-3 col-md-4 col-xs-5" align="left">
	<p class="help-block">${fechaNota}</p>
	</div>
	<div class="col-lg-9 col-md-8 col-xs-7" align="right">
	<p class="help-block">${res.nombreUsuario}</p>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12">
	<hr style="margin-top: 3px; margin-bottom: 10px;">
	</div>
	<div class="col-lg-12">
	<div class="form-group">
	<textarea name="" class="form-control" id="txtNotas${res.idContenido}" readonly>${res.notas}</textarea>
	</div>
	</div>						
	</div>`;
	if(res.archivos!=null){
		cont = cont + `<div class="row">
		<div class="col-lg-12" align="right">
		<h2><i class="far fa-file-pdf" style="font-size: 20px; color: red;"></i> <span><a href="assets/files/archivosNegocios/${res.archivos}" download="${res.archivos}">${res.archivos}</a></span></h2>
		</div> `;
	}
	cont = cont + `</div>
	</div>
	</div>
	</article>
	<!--Notas-->
	`;
	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);
}

function renderCambios(res)
{
	var contLinea = $('#div-linea').html();
	date = (res.fecha).substr(5, 2);
	nomMes=obtenerMes(date);
	fechaCambio = (res.fecha).substr(8, 2)+" de "+nomMes+" del "+(res.fecha).substr(0, 4);
	var cont = `
	<!--Cambios-->
	<article class="timeline-entry">
	<div class="timeline-entry-inner">
	<div class="timeline-icon bg-default">
	<i class="fas fa-exchange-alt"></i>
	</div>
	<div class="timeline-label">
	<h2><a>${res.accion}: </a><a `;
	if (res.campo1=="Ganado" || res.campo1=="+1") {
		cont = cont + `style="color:green"`;
	}else if(res.campo1=="Perdido" || res.campo1=="-1"){
		cont = cont + `style="color:red"`;
	}
	cont = cont + `>${res.campo1}</a> <i class="fas fa-arrow-right"></i> <a `;
	if (res.campo2=="Ganado") {
		cont = cont + ` style="color:green" `;
	}else if(res.campo2=="Perdido"){
		cont = cont + ` style="color:red" `;
	}
	cont = cont + ` '> ${res.campo2}</a></h2>
	<hr style="margin-top: 0px; ">
	<p>${fechaCambio}</p>
	</div>
	</div>
	</article>
	<!--Cambios-->`;
	var contNew = cont + contLinea;
	$('#div-linea').html(contNew);
}

function obtenerMes(numero)
{
	switch(numero) {
		case '01':
		return 'enero';
		break;
		case '02':
		return 'febrero';
		break;
		case '03':
		return 'marzo';
		break;
		case '04':
		return 'abril';
		break;
		case '05':
		return 'mayo';
		break;
		case '06':
		return 'junio';
		break;
		case '07':
		return 'julio';
		break;
		case '08':
		return 'agosto';
		break;
		case '09':
		return 'septiembre';
		break;
		case '10':
		return 'octubre';
		break;
		case '11':
		return 'noviembre';
		break;
		case '12':
		return 'diciembre';
		break;
	}
}

function status(valor,idNegocio){
	if (valor==2) {
		$('#mPedido').modal('show');
	}else if(valor==9){
		$.post("index.php?c=deal&a=ReabrirPerdido", {idNegocio:idNegocio}, function(resultado) {
		});
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se abierto el negocio</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
	}else{
		$.post("index.php?c=deal&a=CambiaStatus", {valor:valor,idNegocio:idNegocio}, function(resultado) {
			if (valor==0) {
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se abierto el negocio</center></strong></div>');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600); 
			}else if(valor==1){
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha ganado el negocio</center></strong></div>');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600); 
			}
		});
	}
	botonesGanadoPerdido();
	consulta();
}

function botonesGanadoPerdido()
{
	$('#div-gan-perd').html('');
	$.get('?c=deal&a=ObtenerStatusNegocio',{idNegocio: idNegocio}, function(res){
		var cont="";
		cont = cont + `<div class="btn-group form-group" role="group">`;
		if (res==0) {
			cont = cont + `<button class="btn btn-success" onclick="status(1,${idNegocio})" >Ganado</button>`;
		}else if (res==1) {
			cont = cont + `<button class="btn btn-success boton">Ganado</button>`;
		}else{
			cont = cont + `<button class="btn btn-danger boton">Perdido</button>`;
		}
		cont = cont + `</div>
		<div class="btn-group form-group" role="group">`;
		if (res==0) {
			cont = cont + `<button class="btn btn-danger" onclick="status(2,${idNegocio});listarCausas();">Perdido</button>`;
		}else if (res==1) {
			cont = cont + `<button class="btn btn-default" onclick="status(0,${idNegocio})">Reabrir</button>`;
		}else{
			cont = cont + `<button class="btn btn-default" onclick="status(9,${idNegocio})">Reabrir</button>`;
		}
		cont = cont + `</div>`;
		$('#div-gan-perd').html(cont);

	});
}

function cambiaEtapa(idEtapa,idNegocio,idOldEtapa) {
	idEtapaModule = idEtapa;
	idOldEtapaModule = idOldEtapa;
	var etapa = $('#idEtapa option:selected').html();
	if(etapa!='Leads'){
		if(claveOrganizacion==0){
			$.get('index.php?c=negocios&a=ConsultaClaveOrganizacion', {valorIdOrganizacion: idOrganizacion}, function(NewClaveOrganizacion){
				$('#txtClaveOrganizacionM').val(NewClaveOrganizacion);
			});	
			$.get('index.php?c=negocios&a=ConsultaConsecutivo', {valorIdOrganizacion: idOrganizacion}, function(consecutivo){
				$('#txtConsecutivoM').val(consecutivo);
			});	
			$('#idNegocioMC').val(idNegocio);
			$('#mAsignarClave').modal('toggle');
		}else{
			actualizaEtapa();
		}
	}else{
		actualizaEtapa();
	}
}

function actualizaEtapa()
{
	$.post("index.php?c=deal&a=CambiaEatapa", {idNegocio:idNegocio,idEtapa:idEtapaModule,idOldEtapa:idOldEtapaModule}, function(resultado) {
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha actualizado la etapa del negocio</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600); 
	});
	consulta();
}



function cambiaEmbudo(idEmbudo, idNegocio, idOldEtapa, idOldEmbudo)
{
	nombreEmbudo = $('#idEmbudo option:selected').html();

	selectOld = $('#idEmbudo').html();

	idEmbudoModule=idEmbudo;

	$('#labEmbudoM').html(nombreEmbudo);
	$('#idEmbudoM').val(idEmbudo);
	$('#idOldEmbudoM').val(idOldEmbudo);
	$('#idNegocioM').val(idNegocio);
	$('#idOldEtapaM').val(idOldEtapa);

	$('#divClave').hide();

	$('#mEtapas').modal('toggle');
	$.get("?c=deal&a=ListarEtapas", { idEmbudo: idEmbudo }, function( etapas ) {
		$("#idEtapaM").empty();
		var selector = document.getElementById("idEtapaM");
		for (var i in etapas) 
			selector.options[i] = new Option(etapas[i].nombreEtapa,etapas[i].idEtapa);
	});
}

function restartSelectEtapas(){
	$('#idEmbudo').html(selectOld);
}

function ocultaNotas() {
	$('#Notas').toggle();
	$('#nota').toggle(); 
	$('#archivo').val("");
	$('#txtNotas').val("");
}

function verNotas() {
	$('#Notas').toggle();
	$('#nota').toggle(); 
}

function ocultaActividades() {
	desmarcar()
	var idOrganizacion = $('#idOrganizaciontex').val();
	$('#form-actividades').bootstrapValidator('resetForm', true);
	$('#checkTiempo').prop('checked',false);
	$('#divTiempo').hide();
	$('#divCruzadas').hide();
	$('#divFiltro').show();
	$('#txtIdActividad').val(0);
	$('#txtTipo').val(""); 
	$('#txtTituloNegocio').val("");
	$('#txtNombreOrganizacion').val("");
	$('#txtIdOrganizacion').val(idOrganizacion);
	$('#txtfechaActividad').val("");
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#txtNotasA').val("");
	$('#txtConfirmado').val("false");
	$('#Actividades').toggle();
	$('#actividad').toggle();
}

function verActividades() {
	$('#Actividades').toggle();
	$('#actividad').toggle(); 
}

function irActividades() {
	var idOrganizacion = $('#idOrganizaciontex').val();
	$('#checkTiempo').prop('checked',false);
	$('#divTiempo').hide();
	$('#divCruzadas').hide();
	$('#divFiltro').show();
	$('#txtIdActividad').val(0);
	$('#txtTipo').val(""); 
	$('#txtTituloNegocio').val("");
	$('#txtNombreOrganizacion').val("");
	$('#txtIdOrganizacion').val(idOrganizacion);
	$('#txtfechaActividad').val("");
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#txtNotasA').val("");
	$('#txtConfirmado').val("false");
	listarPersonaParaAct();
	$('#Notas').hide();
	$('#nota').hide();
	$('#Actividades').hide();
	$('#divTiempo').hide();
	$('#actividad').show(); 
}

function irNotas() {
	$('#Actividades').hide();
	$('#checkTiempo').prop('checked',false);
	$('#actividad').hide();
	$('#Notas').hide();
	$('#nota').show(); 
}

function eliminaFechaCierre() {
	$.post("index.php?c=deal&a=EliminaFechaCierre", {idNegocio:idNegocio}, function(resultado) {
		$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Fecha de cierre eliminada</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);  
		consulta();
	});
}

function registrarFechaCierre() {
	var fechaCierre = $('#txtfechaCierre').val();
	$.post("index.php?c=deal&a=RegistrarFechaCierre", {idNegocio:idNegocio,fechaCierre:fechaCierre}, function(resultado) {
		if (resultado=="Ok"){
			var res = fechaCierre.split("-");
			$.get("?c=deal&a=ObtenerMes", { numero: res[1] }, function( mes ) {
				fechaCierre = res[2] + " de " + mes + " del " + res[0];
				$('#labFechaCierre').html(fechaCierre);
			});
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Fecha de cierre establecida</center></strong></div>');
		}else{
			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Error seleccione una fecha de cierre</center></strong></div>');
		}
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600); 
		consulta();
	});
}

function eliminarActividad(id)
{
	$('#txtIdActividadEl').val(id);  
}

function myFunctionAgregarPersona(idNegocio) {
	$('#txtidNegocioA').val(idNegocio); 
	$.post("index.php?c=deal&a=NombreNegocio", {idNegocio: idNegocio}, function(mensaje) {
		$("#Resu").html(mensaje);
	});
	listarPersona();
}

function myFunctionAgregarSeguidores(idNegocio) {
	$('#txtidNegocioS').val(idNegocio); 
	$.post("index.php?c=deal&a=NombreNegocio", {idNegocio: idNegocio}, function(mensaje) {
		$("#ResuS").html(mensaje);
	});
	listarUsuarios();
}

function myFunctionVerPersona(idCliente) {
	var array = $('#txtArray').val();
	var array = array.split(",");
	$.post("index.php?c=deal&a=VerPersonas", {idCliente:idCliente,idNegocio:idNegocio,array:JSON.stringify(array)}, function(mensaje) {
		$("#ResuVerP").html(mensaje);
	});
}

function myFunctionVerSeguidores(idUsuario) {
	var array = $('#txtArrayS').val();
	var array = array.split(",");
	$.get("index.php?c=deal&a=VerSeguidores", {idUsuario:idUsuario,idNegocio:idNegocio,array:JSON.stringify(array)}, function(mensaje) {
		$("#ResuVerS").html(mensaje);
	});
}

function myFunctionEliminaPersona(idNegocio,idCliente) {
	$('#txtidNegocioE').val(idNegocio);  
	$('#txtidClienteE').val(idCliente);
}

function myFunctionEliminaUsuario(idNegocio, idUsuario) {
	$('#txtidNegocioES').val(idNegocio);  
	$('#txtidUsuarioE').val(idUsuario);  
}

function listarPersona(){
	var idOrganizacion = $('#idOrganizaciontex').val();
	var idCliente = $('#txtidCliente').val();
	var array = $('#txtArray').val();
	var array = array.split(",");
	datos = {idOrganizacion:idOrganizacion,array:JSON.stringify(array),idCliente:idCliente};
	$.ajax({
		url: "index.php?c=deal&a=ListarPersona",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdPersona").empty();
		var selector = document.getElementById("selectIdPersona");
		selector.options[0] = new Option("Seleccione la persona de Contacto","");
		for (var i in respuesta) {
			// console.log(Object.values(respuesta));
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectIdPersona').selectpicker('refresh');
	}); 
	$("#selectIdPersona").empty();
	var selectPersona = document.getElementById("selectIdPersona");
}

function actualizarParticipantes()
{
	idNegocio = <?php echo $_GET['idNegocio']; ?>;
	idCliente = $('#txtidCliente').val();
	$.get('?c=deal&a=ObtenerArregloP', 
		{idNegocio:idNegocio, idCliente:idCliente}, 
		function(res){
			$('#txtArray').val(res[0].arregloP);
			$('#labCountP').html(res[0].countP);
		});
	$.get('?c=deal&a=ListarParticipantes', 
		{idNegocio:idNegocio}, 
		function(personas){
			participantes="";
			for(var i in personas){
				participantes=participantes+`<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<a href="?c=dealcontacto&idCliente=${personas[i].idCliente}" class="btn btn-link">${personas[i].nombrePersona}</a>
				</div>`;
			}
			$('#div-participantes').html(participantes);
		});
}

function listarUsuarios(){
	var idOrganizacion = $('#txtIdOrganizacion').val();
	var idUsuario = $('#txtidUsuario').val();
	var array = $('#txtArrayS').val();
	var array = array.split(",");
	datos = {idOrganizacion:idOrganizacion,array:JSON.stringify(array),idUsuario:idUsuario};
	$.ajax({
		url: "index.php?c=deal&a=ListarUsuarios",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdUsuario").empty();
		var selector = document.getElementById("selectIdUsuario");
		selector.options[0] = new Option("Seleccionar usuario","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].nombreUsuario,respuesta[i].idUsuario);
		}
		$('#selectIdUsuario').selectpicker('refresh');
	}); 
	$("#selectIdUsuario").empty();
	var selectPersona = document.getElementById("selectIdUsuario");
}

function listarPersonaParaAct(){
	var idOrganizacion = $('#idOrganizaciontex').val();
	var array = $('#txtArray').val();
	var idCliente = $('#txtidCliente').val();
	var array = array.split(",");
	array.push(idCliente);
	datos = {idOrganizacion:idOrganizacion,array:JSON.stringify(array)};
	$.ajax({
		url: "index.php?c=deal&a=ListarPersonaVinculadas",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdPersonaAct").empty();
		var selector = document.getElementById("selectIdPersonaAct");
		selector.options[0] = new Option("Seleccione la persona de Contacto","");
		for (var i in respuesta) {
			// console.log(Object.values(respuesta));
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectIdPersonaAct').selectpicker('refresh');
	}); 
	$("#selectIdPersonaAct").empty();
	var selectPersona = document.getElementById("selectIdPersonaAct");
}

function listarCausas(){
	$('#txtcomentarios').val(""); 
	datos=0;
	$.ajax({
		url: "index.php?c=deal&a=ListarCausas",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdCausa").empty();
		var selector = document.getElementById("selectIdCausa");
		selector.options[0] = new Option("Seleccione la causa","");
		for (var i in respuesta) {
			// console.log(Object.values(respuesta));
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].nombreCausa,respuesta[i].idcausasPerdida);
		}
		$('#selectIdCausa').selectpicker('refresh');
	}); 
	$("#selectIdCausa").empty();
	var selectPersona = document.getElementById("selectIdCausa");
	$('#idNegocio').val(idNegocio); 
}



//Metodo para cambiar el estado en la base de datos de acuerdo al idActividad
cambiaEstado = function(idActividad,estado,fechaCompletado)
{
	$('#txtIdActividad2').val(idActividad);
	$('#txtEstado').val(estado);
	if(estado==1){
		$('#labActComp').html("Actividad completada");
		$('#Guardar2').val('Desacompletar');
		$('#txtFechaCompletado').val(fechaCompletado);
	}
	else{
		$('#labActComp').html("Completar actividad");
		$('#Guardar2').val('Completar');
		$('#txtFechaCompletado').val('');
	}
}

function quitarReadOnly(id)
{
	$("#"+id).removeAttr("readonly");
	$("#"+id).focus();
	$( "#"+id ).blur(function() {
		$("#"+id).attr("readonly","readonly");
		var contenido = $('#'+id).val();
		patron = "txtNotas",
		nuevoValor    = "",
		id = id.replace(patron, nuevoValor);
		$.post("index.php?c=deal&a=ActualizarNota", {id:id,contenido,contenido}, function(resultado) {
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota actualizada</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
		});
	});
}
function eliminarNota(id)
{
	$.post("index.php?c=deal&a=EliminarNota", {id:id}, function(resultado) {
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota eliminada</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
	});
	consulta();
}
function eliminarArchivo(id,nombre)
{
	$.post("index.php?c=deal&a=EliminarArchivo", {id:id,nombre:nombre}, function(resultado) {
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Archivo Eliminado</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
	});
	consulta();
}

tipoActividad = function (tipo)
{
	$('#txtTipo').val(tipo);

	switch(tipo){
		case 'Llamada':
		desmarcar("Llamada");
		break;
		case 'Reunion':
		desmarcar("Reunion");
		break;
		case 'Tarea':
		desmarcar("Tarea");
		break;
		case 'Plazo':
		desmarcar("Plazo");
		break;
		case 'Email':
		desmarcar("Email");
		break;
		case 'Comida':
		desmarcar("Comida");
		break;
		case 'WhatsApp':
		desmarcar("WhatsApp");
		break;
		default: 
		break;
	}
}  

desmarcar = function(tipo) {
	var miarray = ["Llamada", "Reunion", "Tarea", "Plazo", "Email", "Comida", "WhatsApp"];

	for (var i = 0; i < miarray.length; i++) {
		if (tipo == miarray[i]) {
			$("#lab"+miarray[i]).addClass("btn-pressed");
			$("#lab"+miarray[i]).removeClass("btn-default");
		}else{
			$("#lab"+miarray[i]).removeClass("btn-pressed");
			$("#lab"+miarray[i]).addClass("btn-default");
		}
	}
}

function deshabilitaConfirmado(){
	$('#txtConfirmado').val("false");
	$('#divCruzadas').hide();
	$('#btnGuardarActividad').val("Guardar");
	$('#btnGuardarActividad').prop('disabled',false);
}

function contadores()
{
	idNegocio = idNegocio;
	$.get('?c=deal&a=Contadores', {idNegocio:idNegocio}, function(res){
		$('#countTotal').html(res[0].countTotal);
		$('#countActividades').html(res[0].countActividades);
		$('#countNotas').html(res[0].countNotas);
		$('#countRegCambios').html(res[0].countRegCambios);
	})
}

$(function(){
	$('[rel="popover"]').popover({
		container: 'body',
		html: true,
		content: function () {
			var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
			return clone;
		}
	}).click(function(e) {
		e.preventDefault();
	});
});

editarActividad = function(idActividad, tipoActividad, notas, fechaActividad, duracion, horaInicio, idOrganizacion, nombreOrganizacion, idNegocio, tituloNegocio, nombrePersona){
	camposEditar();  

	$('#txtIdActividadE').val(idActividad);  
	$('#txtTipoE').val(tipoActividad);
	$('#txtNotasAE').val(notas);  
	$('#txtfechaActividadE').val(fechaActividad);
	var arregloDuracion = duracion.split(" ");
	$('#txtDuracionE').val(arregloDuracion[0]);
	$('#txtHoraE').val(horaInicio)
	$('#txtIdOrganizacionE').val(idOrganizacion);
	$('#txtNombreOrganizacion').val(nombreOrganizacion);
	$('#txtConfirmadoE').val('false');

	switch(tipoActividad) {
		case 'Llamada':
		$('#optionLlamadaEe').prop('checked',true);
		break;
		case 'Reunion':
		$('#optionReunionE').prop('checked',true);
		break;
		case 'Tarea':
		$('#optionTareaE').prop('checked',true);
		break;
		case 'Plazo':
		$('#optionPlazoE').prop('checked',true);
		break;
		case 'Email':
		$('#optionEmailE').prop('checked',true);
		break;
		case 'Comida':
		$('#optionComidaE').prop('checked',true);
		break;
		case 'WhatsApp':
		$('#optionWhatsappE').prop('checked',true);
		break;
	}

	if( horaInicio != ''){
		$('#checkTiempoE').prop('checked',true);
		$('#divTiempoE').show();
		$('#btnGuardarE').val('Guardar');
	} else {
		$('#checkTiempo').prop('checked',false);
		$('#divTiempoE').hide();
		$('#btnGuardarE').val('Guardar');
	}

	var idOrganizacion = $('#txtIdOrganizacion').val();
	var array = $('#txtArray').val();
	var idCliente = $('#txtidCliente').val();
	var array = array.split(",");
	array.push(idCliente);
	datos = {idOrganizacion:idOrganizacion,array:JSON.stringify(array)};
	$.ajax({
		url: "index.php?c=deal&a=ListarPersonaVinculadas",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdPersonaActE").empty();
		var selectIdPersonaActE = document.getElementById("selectIdPersonaActE");
		if (nombrePersona != "")
			selectIdPersonaActE.options[0] = new Option(nombrePersona,nombrePersona);
		else
			selectIdPersonaActE.options[0] = new Option("Seleccione la persona de contacto","");
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].nombrePersona != nombrePersona){
				selectIdPersonaActE.options[++j] = new Option(respuesta[i].nombrePersona,respuesta[i].nombrePersona);
			}
		}
		$('#selectIdPersonaActE').selectpicker('refresh');
	});
	//----- Select tipo de duración --------
	$('#selectTipoDuracionE').empty(); 
	var selectTipoDuracionE = document.getElementById("selectTipoDuracionE");
	if(arregloDuracion[1] == "minuto" || arregloDuracion[1] == "minutos"){
		selectTipoDuracionE.options[0] = new Option("minutos","minutos");
		selectTipoDuracionE.options[1] = new Option("horas","horas");
	}
	else{
		selectTipoDuracionE.options[0] = new Option("horas","horas");
		selectTipoDuracionE.options[1] = new Option("minutos","minutos");
	}
}


camposEditar = function()
{
	$('#cruzadasE').html('');
	$('#divCruzadasE').hide();
	$('#divFiltro').hide();
}


tipoActividadE = function (tipo)
{
	$('#txtTipoE').val(tipo);

	switch(tipo){
		case 'Llamada':
		desmarcarE("Llamada");
		break;
		case 'Reunion':
		desmarcarE("Reunion");
		break;
		case 'Tarea':
		desmarcarE("Tarea");
		break;
		case 'Plazo':
		desmarcarE("Plazo");
		break;
		case 'Email':
		desmarcarE("Email");
		break;
		case 'Comida':
		desmarcarE("Comida");
		break;
		case 'WhatsApp':
		desmarcarE("WhatsApp");
		break;
		default: 
		break;
	}
}  

desmarcarE = function(tipo) {
	var miarray = ["LlamadaE", "ReunionE", "TareaE", "PlazoE", "EmailE", "ComidaE", "WhatsAppE"];

	for (var i = 0; i < miarray.length; i++) {
		if (tipo == miarray[i]) {
			$("#lab"+miarray[i]).addClass("btn-pressed");
			$("#lab"+miarray[i]).removeClass("btn-default");
		}else{
			$("#lab"+miarray[i]).removeClass("btn-pressed");
			$("#lab"+miarray[i]).addClass("btn-default");
		}
	}
}

function deshabilitaConfirmadoE(){
	$('#txtConfirmado').val("false");
	$('#divCruzadasE').hide();
	$('#btnGuardarE').val("Guardar");
	$('#btnGuardarE').prop('disabled',false);
}

</script>
