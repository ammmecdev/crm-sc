<!--Encabezado-->
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row" style="margin-bottom: 5px;">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-5">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mOrden" onclick="nuevaOrden()">Nueva Orden</a>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
						<center><h3 style="margin-top: 0px;">Orden de Trabajo</h3></center>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-5" align="right">
						<form  method="POST" action="?c=usuarios&a=Exportar&g=true" enctype="multipart/form-data" id="form-exportar">
							<div class="btn-group">
									<input type="text" id = "vBusqueda" name="vBusqueda" class="form-control" placeholder="Buscar"  onkeyup="consultas();">
								
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-3">
						<div class="btn-group btn-group-justified" role="group" aria-label="...">
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-default" id="btnOrdenes" onclick="verOrdenes();">Ordenes de Trabajo</button>
							</div>
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-default" id="btnReportes" onclick="verReportes();">Reportes Técnicos</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Encabezado-->

<!--Contenedor-->
<div class="table-responsive" id="tbl">
	<div id="resultadoBusqueda">
		<!--Aqui se vera a tabla de resultados-->
	</div>
	
</div>

<!--Contenedor-->

<!-- Modal Nueva Orden -->
<?php include './view/procesos/modal/orden/mOrden.php'; ?>

<!-- Modal Eliminar Orden -->
<?php include './view/procesos/modal/orden/mOrdenEliminar.php'; ?>

<script src="./assets/js/orden.js"></script>