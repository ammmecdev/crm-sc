<!--Modal Nueva Orden-->
<div id="mOrden" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		
		<div class="modal-content">
			<form id="form_orden" action="index.php?c=orden&a=Guardar" class="form_horizontal" method="post">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Orden de Trabajo</strong></h4>
			</div>
			<div class="modal-body">
				
					<input type="hidden" id="idCotizacion" name="idCotizacion" value="0" readonly>
					<input type="hidden" id="idOrdenTrabajo" name="idOrdenTrabajo" value="0" readonly>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<h4 align="center"><strong>ORDEN DE TRABAJO</strong></h4>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-lg-1">
							<div class="form-group">
								<h5><strong>Cuenta:</strong></h5>	
							</div>
							
						</div>
						<div class="col-xs-12 col-lg-3">
							<div class="form-group">
								<select name="selectCuenta" id="selectCuenta" class="form-control selectpicker" data-live-search="true">
								</select>
							</div>
						</div>
						<div class="col-xs-2 col-lg-6">
							<div class="form-group">
								<h5 align="right"><strong>Fecha:</strong></h5>
							</div>
						</div>
						<div class="col-xs-2 col-lg-2">
							<div class="form-group">
								<h5 align="right" id="fechaValor"><strong></strong></h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-1">
							<div class="form-group">
								<h5><strong>Proyecto:</strong></h5>
							</div>
						</div>
						<div class="col-lg-11">
							<div class="form-group">
								<input id="tituloNegocio" name="tituloNegocio" type="text" class="form-control" readonly="true" align="center" placeholder="Nueva Orden" style="text-align: center;">
							</div>
						</div>
						<!--<div class="col-lg-12">
							<h5><strong>Descripción del Negocio:</strong></h5>
							<textarea name="" id="txtTexto" class="form-control"></textarea>
						</div> -->
						<div class="col-lg-12">
							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="col-lg-6">
								<div class="form-group">
									<h5><strong>No. pedido / O.C.:</strong></h5>
								</div>
								<div class="form-group">
									<input id="numeroPedido" name="numeroPedido" type="text" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<h5><strong>Nombre de Usuario:</strong></h5>
								</div>
								<div class="form-group">
									<select name="selectContacto" id="selectContacto" class="form-control selectpicker" data-live-search="true">
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="col-lg-6">
								<div class="form-group">
									<h5><strong>Fecha de Realización:</strong></h5>
								</div>
								<div class="form-group">
									<input id="fechaRealizacion" name="fechaRealizacion" type="date" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<h5><strong>Fecha de Entrega:</strong></h5>
								</div>
								<div class="form-group">
									<input id="fechaEntrega" name="fechaEntrega" type="date" class="form-control" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<hr>
							<div class="form-group">
								<h5 align="center"><strong>Descripción de Servicio</strong></h5>	
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<textarea name="txtNotas" id="txtNotas" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<h5 align="center"><strong>Observaciones</strong></h5>	
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<textarea name="txtTexto" id="txtTexto" class="form-control"></textarea>
							</div>
						</div>
					</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
				<input type="submit" class="btn btn-sm btn-success" value="Guardar">
			</div>
			</form>
		</div>
		
	</div>
</div>
<!--Modal Nueva Orden-->