
<!--Modal Nueva Cotización-->
<div id="mCotizacion" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="form_cotizacion" action="?c=cotizacion&a=Guardar" method="post" class="form_horizontal">
			<div class="modal-header bg-success">
				<input type="text" id="txtIdCotizacion" name="idCotizacion" value="0" hidden = true>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Cotización</strong></h4>
			</div>
			<div class="modal-body">
					<div class="row">
						<div class="col-lg-12">
							<h4 align="center"><strong>Datos Generales</strong></h4>
						</div>
						<div class="col-lg-3">
							<h5><strong>Clave de Cotización</strong></h5>
							<input type="text" class="form-control" id="txtClaveCotizacion" name="claveCotizacion" placeholder="MIN-0001">
						</div>
						<div class="col-lg-9">
							<h5 align="center"><strong>Proyecto</strong></h5>
							<input type="text" class="form-control" id="txtProyecto" name="proyecto" readonly="true">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							<h5><strong>Cuenta</strong></h5>
							<select class="form-control selectpicker" data-live-search="true" id="selectIdCostoOperativo" name="idCostoOperativo" onchange="obtenerDatosCostoOperativo();">
								<option value="">Seleccione la cuenta</option>
								
							</select>
						</div>
						<div class="col-lg-6">
							<h5><strong>Cliente</strong></h5>
							<input type="text" class="form-control" id="txtIdOrganizacion" name="idOrganizacion" readonly="true">
						</div>
						<div class="col-lg-3">
							<h5><strong>Requisición</strong></h5>
							<input name="requisicion" type="text" class="form-control" id="txtRequisicion"  readonly="true">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							<h5><strong>Fecha</strong></h5>
							<input type="date" class="form-control" id="txtFecha" name="fecha">
						</div>
						<div class="col-lg-9">
							<h5><strong>Dirección</strong></h5>
							<div class="input-group">
								<span class="input-group-addon"><i class="mdi mdi-location-on" style="font-size: 17px;"></i></span>
								<input name="direccion" id="txtDireccion"  type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3">
							<h5><strong>Localidad</strong></h5>
							<input type="text" class="form-control" id="txtLocalidad" name="localidad">
						</div>
					<!--	<div class="col-lg-3">
							<h5><strong>Estado</strong></h5>
							<input type="text" class="form-control" id="txtEstado" name="estado">
						</div> -->
						<div class="col-lg-6">
							<h5><strong>Atención</strong></h5>
							<input type="text" class="form-control" id="txtAtencion" name="atencion" readonly="true" style="text-align: center;">
							<hr style="margin-bottom: 15px;">
						</div>
					</div>
					<div align="right" id="togglecomplementos">
						<button class="btn-link" type="button" data-toggle="collapse" data-target="#complementos" aria-expanded="false" aria-controls="collapseExample">
							<i class="fas fa-plus"></i> Agregar complementos
						</button>
					</div>
					<div class="collapse" id="complementos"> 
						<div class="card card-body" style="margin-bottom: 5px;">
							<div class="row">
								<div class="col-lg-6">
									<h5><strong>IVA (%):</strong></h5><input type="text" class="form-control" name="iva" id="txtIva">
								</div>
								<div class="col-lg-6">
									<h5><strong>Factor:</strong></h5><input type="text" class="form-control" name="factor" id="txtFactor">
								</div>
							</div>
						</div>
					</div>
				
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12">
						<h4 align="center"><strong>Productos y/o Servicios Cotizados</strong></h4>
					</div>
					<div class="table-responsive" id="tablaServicios">
						<!-- Aqui se muestra la tabla de servicios -->
					</div>
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
				<input type="submit" id="guardarmCotizacion" class="btn btn-sm btn-success" value="Guardar">
			</div>
			</form>	
		</div>
	</div>
</div>
<!--Fin del modal Nueva Cotización-->