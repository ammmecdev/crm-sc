<!--Modal Confimación para eliminar Cotizacion-->
<div id="mCotizacionEliminar" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
        <form  method="post" action="index.php?c=cotizacion&a=Eliminar"  role="form" id="form-eliminar">
            <input hidden="true" id="idCotizacionEliminar" name="idCotizacionEliminar">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>Confirmación</strong></h4>
                </div>
                <div class="modal-body">
                    <h5 class="body-title" id="mybodylabel">Esta seguro que desea eliminar esta Cotización?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Salir</button>
                    <input type="submit" class="btn btn-danger" id="btnGuardar" value="Eliminar">
                </div>
            </div>
        </form>
	</div>
</div>
<!--Modal Confimación para eliminar Cotizacion-->