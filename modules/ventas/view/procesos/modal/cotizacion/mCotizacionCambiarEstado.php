<!--Modal Confimación para cambiar estado-->
<div id="mCotizacionCambiarEstado" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
        <form  method="post" action="index.php?c=cotizacion&a=CambiarEstado"  role="form" id="form-cambiar-estado">
            <input hidden="true" id="idCotizacionCambiarEstado" name="idCotizacionCambiarEstado">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>Cambiar Estado</strong></h4>
                </div>
                <div class="modal-body">
                    <select class="form-control selectpicker" data-live-search="true" id="selectEstadoCotizacion" name="selectEstadoCotizacion">
                        <option value="Pendiente">Pendiente</option>
                        <option value="Rechazado">Rechazado</option>
                        <option value="Aprobado">Aprobado</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Salir</button>
                    <input type="submit" class="btn btn-success" id="btnGuardar" value="Cambiar Estado">
                </div>
            </div>
        </form>
	</div>
</div>
<!--Modal Confimación para cambiar estado-->