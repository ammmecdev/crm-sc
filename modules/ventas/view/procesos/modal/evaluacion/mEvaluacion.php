
<!--Modal Nueva Evaluación-->
<div id="mEvaluacion" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
    
		<div class="modal-content">
        <form id="form_evaluacion" action="index.php?c=evaluacion&a=Guardar" class="form_horizontal" method="post">
			
			<input type="hidden" class="form-control" value="0" name="idEvaluacion" id="idEvaluacion" readonly>
			
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Evaluación de Servicio</strong></h4>
			</div>
            
			    <div class="modal-body">
				
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<h5><strong>Cliente:</strong></h5>
							</div>
							<div class="form-group">
								<select name="selectOrganizaciones" id="selectOrganizaciones" class="form-control selectpicker" data-live-search="true" >
									<!-- Aqui van las Organizaciones -->
								</select>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<h5><strong>Contacto:</strong></h5>
							</div>
							<div class="form-group">
								<select name="selectPersonas" id="selectPersonas" class="form-control selectpicker" data-live-search="true">
                                    <!-- Aqui van los Contactos -->
								</select>
							</div>
						</div>
						
						<div class="col-lg-4">
							<div class="form-group">
								<h5><strong>Fecha:</strong></h5>
							</div>
							<div class="form-group">
								<input name="dateFechaEvaluacion" id="dateFechaEvaluacion"type="date" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<hr style="margin-top: 0px; margin-bottom: 5px;">
						</div>
						<div class="col-lg-1"></div>
						<div class="col-lg-10" align="justify">
							<h5 class="help-block">
								Con el propósito de evaluar y mejorar nuestro desempeño, se solicita marcar el cuestionario en base a los siguientes
								criterios de evaluación:
							</h5>
						</div>
						<div class="col-lg-1"></div>
					</div>
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-2" align="center">
							<div class="form-group">
								<h5><strong>5 Excelente</strong></h5>
							</div>
						</div>
						<div class="col-lg-2" align="center">
							<div class="form-group">
								<h5><strong>4 Bueno</strong></h5>
							</div>
						</div>
						<div class="col-lg-2" align="center">
							<div class="form-group">
								<h5><strong>3 Regular</strong></h5>
							</div>
						</div>
						<div class="col-lg-2" align="center">
							<div class="form-group">
								<h5><strong>2 Malo</strong></h5>
							</div>
						</div>
						<div class="col-lg-2" align="center">
							<div class="form-group">
								<h5><strong>1 Deficiente</strong></h5>
							</div>
						</div>
						<div class="col-lg-1"></div>
					</div>
					<div class="row">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr class="active">
										<td align="center" colspan="7"><strong>Criterios de Evaluación</strong></td>
									</tr>
									<tr class="active">
										<td colspan="2"></td>
										<td align="center"><strong>1</strong></td>
										<td align="center"><strong>2</strong></td>
										<td align="center"><strong>3</strong></td>
										<td align="center"><strong>4</strong></td>
										<td align="center"><strong>5</strong></td>
									</tr>
								</thead>
								<tbody id="aspecto1" class="form-group">
									<tr>
										<td align="center"><strong>1</strong></td>
										<td>La calidad del trabajo fue:</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
												<!-- p1r1 == pregunta1respuesta1-->
													<input type="radio" name="optUno" id="p1r1" value="1" style="margin-left:auto; margin-right:auto;" data-toggle="tooltip">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optUno" id="p1r2" value="2" style="margin-left:auto; margin-right:auto;" data-toggle="tooltip">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optUno" id="p1r3" value="3" style="margin-left:auto; margin-right:auto;" data-toggle="tooltip">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optUno" id="p1r4" value="4" style="margin-left:auto; margin-right:auto;" data-toggle="tooltip">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optUno" id="p1r5" value="5" style="margin-left:auto; margin-right:auto;" data-toggle="tooltip">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								<tbody id="aspecto2" class="form-group">
									<tr>
										<td align="center"><strong>2</strong></td>
										<td>Entrega del Trabajo de acuerdo a programa y tiempo de entrega:</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optDos"  id="p2r1" value="1" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optDos" id="p2r2" value="2" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optDos" id="p2r3" value="3" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optDos" id="p2r4" value="4" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optDos" id="p2r5" value="5" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								<tbody id="aspecto3" class="form-group">
									<tr>
										<td align="center"><strong>3</strong></td>
										<td>El orden y limpieza al realizar y entregar el trabajo fue:</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optTres" id="p3r1" value="1" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optTres" id="p3r2" value="2" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optTres" id="p3r3" value="3" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optTres" id="p3r4" value="4" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optTres"id="p3r5" value="5" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								<tbody id="aspecto4" class="form-group">
									<tr>
										<td align="center"><strong>4</strong></td>
										<td>El tiempo de respuesta del proveedor fue:</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCuatro" id="p4r1" value="1" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCuatro" id="p4r2" value="2" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCuatro" id="p4r3" value="3" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCuatro" id="p4r4" value="4" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCuatro" id="p4r5" value="5" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								<tbody id="aspecto5" class="form-group">
									<tr>
										<td align="center"><strong>5</strong></td>
										<td>Se cumplió con procedimientos y normatividad:</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCinco" id="p5r1" value="1" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCinco" id="p5r2" value="2" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCinco" id="p5r3" value="3" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCinco" id="p5r4" value="4" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optCinco" id="p5r5" value="5" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								<tbody id="aspecto6" class="form-group">
									<tr>
										<td align="center"><strong>6</strong></td>
										<td>Se proporcionó Soporte Técnico y Servicio Post-Venta:</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optSeis" id="p6r1" value="1" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optSeis" id="p6r2" value="2" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optSeis" id="p6r3" value="3" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optSeis" id="p6r4" value="4" style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
										<td>
											<div class="radio" style="margin-top: 0px; margin-bottom: 0px;">
												<label>
													<input type="radio" name="optSeis" id="p6r5" value="5 "style="margin-left:auto; margin-right:auto;">
												</label>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tfoot id="footpromedio" class="form-group">
									<tr>
										<td colspan="2" align="right"><strong>Promedio:</strong></td>
										<td colspan="5" align="center" id="promedio" class="lectura"><input id="promediotxt" name ="promediotxt"  type="text" class="lectura" style="text-align: center;" readonly  value="0"></td>
										
									</tr>
								</tfoot> 
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<hr style="margin-top: 0px;">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<h5 align="center"><strong>Observaciones</strong></h5>
							</div>
							<div class="form-group">
								<textarea name="txtNotas" id="txtNotas" class="form-control"></textarea>
							</div>
						</div>
					</div>
                    </div>
                    <div class="modal-footer">
						<input type="submit" class="btn btn-success" id="btnGuardar" value="Guardar">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
                        
                    </div>
           
                    </form>
		</div>
        
	</div>
</div>
<!--Modal Nueva Evaluación-->