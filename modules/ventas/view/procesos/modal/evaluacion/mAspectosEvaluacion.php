<!--Modal Aspectos de la Evaluacion-->
<div id="mAspectosEvaluacion" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Aspectos de la Evaluación de Servicio</strong></h4>
			</div>
			<div class="modal-body">
				<div id="resultadoAspectosModal">
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Salir</button>
			</div>
		</div>
	</div>
</div>
<!--Modal Aspectos de la Evaluacion-->