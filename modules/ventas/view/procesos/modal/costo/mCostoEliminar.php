<!--Inicio modal para eliminar el costo operativo -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=costo&a=EliminarCostoOperativo" role="form" id="form-eliminarPN">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Costo Operativo</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;">   
					<!-- Cuerpo --> 
					<p style="font-size: 18px;">¿Está seguro que desea eliminar el costo operativo?</p>
					<input type="text" id="idCostoOperativoE" name="idCostoOperativoE">
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal para eliminar el costo operativo -->