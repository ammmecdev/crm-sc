
<!--Modal Crear Solicitud-->
<div id="mSolicitud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form action="?c=costo&a=RegistrarSolicitud" id="form_crear" method="post">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><strong>Solicitud de Costo Operativo</strong></h4>
				</div>
				<div class="modal-body">
					<fieldset>
						<legend><font size="4"> Datos generales </font></legend>
						<div class="row">
							<input name="idCostoOperativo" id="txtIdCostoOperativo" type="hidden" class="form-control">

							<div class="col-lg-4">
								<div class="form-group">
									<h5><strong>Fecha de Emisión:</strong></h5>
									<input name="fechaEmision" id="txtFechaEmision" type="date" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<h5><strong>Entrega Requerida:</strong></h5>
									<input name="fechaEntrega" id="txtFechaEntrega" type="date" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<h5><strong>Requisición No.</strong></h5>
									<input name="requisicion" id="txtRequisicion" type="text" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group">
									<h5><strong>Cuenta:</strong></h5>
									<select name="idNegocio" id="selectCuenta" class="form-control selectpicker" data-live-search="true" onchange="consultarPorCuenta()" required>
										<option value="">Seleccione la cuenta</option>
									</select>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<h5><strong>Contacto:</strong></h5>
									<select name="idContacto" id="selectContacto" class="form-control selectpicker" data-live-search="true" required>
										<option value="">Seleccione la persona de contacto</option>
									</select>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									<h5><strong>Cliente:</strong></h5>
									<input id="txtCliente" type="text" class="form-control" readonly="true">
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<h5 align="center"><strong>Proyecto</strong></h5>
									<input id="txtProyecto" type="text" class="form-control" readonly="true">
								</div>
							</div>
						</div>
						<div class="row" id="divCrear">
							<div class="col-lg-12" align="right">
								<div class="form-group">
									<br>
									<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
									<button type="submit" class="btn btn-sm btn-success">Crear Solicitud</button>
								</div>
							</div>
							<div class="col-lg-12">
								<hr style="margin-bottom: 10px;">
							</div>
						</div>
					</fieldset>

					<div id="divCarrito">
						<fieldset>
							<legend><font size="4"> Datos del servicio </font></legend>
							<input name="idSerCO" id="txtIdSerCO" type="hidden" class="form-control">
							<div class="row">
                                <div class="col-lg-12">
									<div class="form-group">
										<p id="verificador" class="text-danger"></p>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<h5><strong>Clave:</strong></h5>
										<input name="clave" id="txtClave" type="text" class="form-control">
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<h5><strong>Unidad:</strong></h5>
										<select name="unidad" id="txtUnidad" class="form-control">
											<option value="">--Seleccione--</option>
											<option value="Pieza">Pieza</option>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<h5><strong>Cantidad:</strong></h5>
										<input name="cantidad" id="txtCantidad" type="number" class="form-control">
									</div>
								</div>
								<div class="col-lg-10">
									<div class="form-group">
										<h5><strong>Descripción:</strong></h5>
										<textarea name="descripcion" id="txtNotas" class="form-control"></textarea>
									</div>
								</div>
								<div class="col-lg-2">
									<div class="col-lg-12 hidden-xs" style="margin-top: 30px;">

									</div>
									<div class="col-xs-12 col-lg-12 center-block">
										<div class="form-group">
											<button onclick="limpiarInputs()" type="button" class="btn btn-sm btn-default btn-block">Limpiar <i class="fas fa-eraser"></i></button>
											<button onclick="agregarServicio()" type="button" class="btn btn-sm btn-success btn-block">Añadir <i class="fas fa-cart-arrow-down"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<legend><font size="4"> Carrito de servicios </font></legend>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="table-responsive" id="tabla_carrito">
										<table class="table table-bordered">
											<thead>
												<tr class="info" align="center">
													
													<td><strong>Descripción</strong></td>
													<td style="width: 100px;"><strong>Unidad</strong></td>
													<td style="width: 100px;"><strong>Cantidad</strong></td>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>

				<div class="modal-footer" id="divEnviar">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<div class="col-xs-3 col-lg-3" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar costo operativo" id="btnEliminar" onclick="EliminarCostoOperativo()"><i class="fas fa-trash-alt" style="font-size: 15px;"></i></a>
						</div>
						<div class="col-xs-9 col-lg-9" align="right" style="padding-right: 0px">
							<button type="button" class="btn btn-sm btn-default" data-dismiss="modal" onclick="consultasCO_Ven();">Cancelar</button>
							<button type="submit" class="btn btn-sm btn-success" id="btnEnviarSolicitud">Enviar Solicitud</button> 
						</div>  
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!--/Modal Crear Solicitud-->