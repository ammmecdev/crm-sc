<style>
.table>thead>tr>th {
	vertical-align: middle;
	text-align: center;
	cursor: pointer;
}
input.lineal {
	border: none;
	border-bottom: 1px #ddd solid;
	cursor: pointer;

}
</style>
<!--Encabezado-->
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-lg-12">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<form action="">
									<div class="btn-group form-group">
										<input type="text" id="txtBuscar" class="form-control" placeholder="Bucar...">
									</div>
								</form>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="center">
								<h3 style="margin-top: 3px;">Seguimiento</h3>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
								<form metdod="POST" action="?c=seguimiento&a=Exportar">
									<div class="btn-group"  style="padding-left: 0px; padding-right: 0px;">
										<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro" id="btnExportar"><i class="fas fa-download"></i></button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!--<div class="col-xs-4 col-sm-6 col-md-6 col-lg-4" align="right">
						<form method="POST">
							<div class="btn-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro" id="btnExportar"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=seguimiento&a=Exportar" enctype="multipart/form-data">
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" id="btnExportar" name="btnExportar" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
							</div>
						</form>-->

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="row">
									<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
										<div class="col-xs-8 col-sm-9 col-md-8 col-lg-8" style="margin-top: 6px; padding-right: 0px; white-space: nowrap;">
											<strong>Filtrar del día:&nbsp;</strong>
											<input type="date" class="form-control lineal" style="display: inline-block;">
										</div>
									</div>
									<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
										<div class="col-xs-9 col-sm-9 col-md-8 col-lg-8" style="margin-top: 6px; padding-right: 0px; white-space: nowrap;">
											<strong>al día:&nbsp;</strong>
											<input type="date" class="form-control lineal" style="display: inline-block;">
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Encabezado-->

	<!--Contenedor-->
	<div style="overflow-x: auto;">
		<table class="table table-bordered table-hover" style="margin-bottom: 0px;">
			<thead>
				<tr>
					<th colspan="5" style="cursor: default; text-align: right; background-color: #fff;">
						<small>Totales</small>
					</th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="white-space: nowrap; cursor: default; text-align: right; background-color: #EAEDED;">
						<small>$ 921,420.00</small>
					</th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="white-space: nowrap; cursor: default; text-align: right; background-color: #EAEDED;">
						<small>$ 1,068,847.00</small>
					</th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="white-space: nowrap; cursor: default; background-color: #EAEDED;">
						<small>0 CANCELADO</small>
					</th>
					<th style="white-space: nowrap; cursor: default; background-color: #EAEDED;">
						<small>1 COTIZADO</small>
					</th>
					<th style="white-space: nowrap; cursor: default; background-color: #EAEDED;">
						<small>2 PEDIDO</small>
					</th>
					<th style="white-space: nowrap; cursor: default; background-color: #EAEDED;">
						<small>3 FACTURADO</small>
					</th>
					<th style="white-space: nowrap; cursor: default; background-color: #EAEDED;">
						<small>4 PAGADO</small>
					</th>
					<th style="white-space: nowrap; cursor: default; background-color: #EAEDED;">
						<small>5 COMPROBANTE</small>
					</th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
					<th style="cursor: default; background-color: #fff;"></th>
				</tr>
				<tr style="background-color: rgb(106, 115, 123); color: #FFF;">
					<th id="cliente" style="white-space: nowrap;">Cliente 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="consecutivo" style="white-space: nowrap;">Consecutivo 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="servicio" style="white-space: nowrap;">Servicio 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="cotizacion" style="white-space: nowrap;">Cotización 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="descripcion" style="white-space: nowrap;">Descripción 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="equipo" style="white-space: nowrap;">Equipo 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="importe_iva" style="white-space: nowrap; cursor: default;">Importe sin<br><small>iva</small></th>
					<th id="importe_usd" style="white-space: nowrap; cursor: default;">Importe USD</th>
					<th id="precio_neto" style="white-space: nowrap; cursor: default;">Precio Neto</th>
					<th id="oportunidad" style="white-space: nowrap;">Oportunidad 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="rfq" style="white-space: nowrap;">RFQ 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_rfq" style="white-space: nowrap;">Fecha de RFQ 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="usuario" style="white-space: nowrap;">Usuario 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_cotizacion" style="white-space: nowrap;">Fecha de<br><small>cotización</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="pedido" style="white-space: nowrap;">Pedido 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_pedido" style="white-space: nowrap;">Fecha de <br><small>pedido</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_realizacion" style="white-space: nowrap;">Fecha de <br><small>realización</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_reporte" style="white-space: nowrap;">Fecha de <br><small>reporte</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="factura" style="white-space: nowrap;">Factura 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_facturacion" style="white-space: nowrap;">Fecha de <br><small>facturación</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="fecha_pago" style="white-space: nowrap;">Fecha de <br><small>pago</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="realizacion" style="white-space: nowrap;">Realización 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="facturacion" style="white-space: nowrap;">Facturación 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
					<th id="recibo_pago" style="white-space: nowrap; cursor: default;">Recibo electrónico<br><small>de pago</small></th>
					<th id="fecha_recibo" style="white-space: nowrap;">Fecha de<br><small>recibo</small> 
						<i class="glyphicon glyphicon-sort" style="font-size: 12px;"></i>
					</th>
				</tr>
			</thead>
			<tbody style="background-color: #fff;">
				<tr>
					<td align="center">MEB</td>
					<td align="center">008</td>
					<td align="center">AST</td>
					<td align="center">MEB 0006</td>
					<td>Finiquito Asesoria tecnica Mantenimiento Molino 1</td>
					<td align="center" style="white-space: nowrap;">Molinos</td>
					<td style="white-space: nowrap;">$ 100,000.00</td>
					<td style="white-space: nowrap;">$ 108,472.65</td>
					<td style="white-space: nowrap;">$ 116,000.00</td>
					<td align="center" style="white-space: nowrap;">4</td>
					<td align="center" style="white-space: nowrap;">227506</td>
					<td align="center" style="white-space: nowrap;">2018-09-24</td>
					<td align="center" style="white-space: nowrap;">Marcos Flores Saucedo</td>
					<td align="center" style="white-space: nowrap;">2018-01-03</td>
					<td align="center" style="white-space: nowrap;">OC-00002853</td>
					<td align="center" style="white-space: nowrap;">2017-11-30</td>
					<td align="center" style="white-space: nowrap;">2017-12-06</td>
					<td align="center" style="white-space: nowrap;">2017-12-15</td>
					<td align="center" style="white-space: nowrap;">A749</td>
					<td align="center" style="white-space: nowrap;">2017-12-13</td>
					<td align="center" style="white-space: nowrap;">2018-01-08</td>
					<td align="center" style="white-space: nowrap;">DICIEMBRE</td>
					<td align="center" style="white-space: nowrap;">DICIEMBRE</td>
					<td align="center" style="white-space: nowrap;">B 1</td>
					<td align="center" style="white-space: nowrap;">2018-01-19</td>
				</tr>
				<tr>
					<td align="center">TOR</td>
					<td align="center">001</td>
					<td align="center">MPV</td>
					<td align="center">TOR 0001</td>
					<td>Reembabitado de chumaceras</td>
					<td></td>
					<td>$ 175,000.00</td>
					<td></td>
					<td>$ 203,000.00</td>
					<td>1</td>
					<td>144678</td>
					<td></td>
					<td></td>
					<td></td>
					<td>PO711.0000285361</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td align="center">COM</td>
					<td align="center">001</td>
					<td align="center">MPV</td>
					<td align="center"></td>
					<td>Estimacion 4 Rehabilitacion Molino 1</td>
					<td>Molinos</td>
					<td>$ 5,500.00</td>
					<td>$ 108,472.65</td>
					<td>$ 6,380.00</td>
					<td>5</td>
					<td>036734</td>
					<td>2017-08-13</td>
					<td>Bruno Padilla Guerrero</td>
					<td></td>
					<td>4500022599</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>B 4</td>
					<td></td>
				</tr>
				<tr>
					<td>FRE</td>
					<td></td>
					<td></td>
					<td></td>
					<td>zxy</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>8</td>
					<td>744536</td>
					<td>2015-03-10</td>
					<td style="white-space: nowrap;">Alejandro Castro Saucedo</td>
					<td></td>
					<td>PO724.0000256867</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>B 15</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!--Contenedor-->

	<!--Modal Crear Solicitud-->
	<script src="./assets/js/seguimiento.js"></script>