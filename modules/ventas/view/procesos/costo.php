<style>
input.linea[type=text] {
	border: none;
	border-bottom: 1px solid #009688;
}
</style>
<!--Encabezado-->
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mSolicitud" onclick="solicitudNueva()"> Crear Solicitud </a>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<h3 align="center" style="margin-top: 3px;">Costo Operativo</h3>
					</div>
	
					<!--<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=costo&a=Exportarpdf" enctype="multipart/form-data" name="form-exportar" id="form-exportar">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="idCostoOperativo2" class="form-control" id="txtIdCostoOperativo2">
									<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultas();">
								</div>
							</div>
							<div class="btn-group form-group"> 
								<button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download" target="_blank"></i></button>
							</div>
						</form>
					</div>-->

					<form method="POST" id="form-expo">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
							<div class="btn-group">
								<input type="hidden" name="idCostoOpe" class="form-control" id="txtIdCostoOpe">
								<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultasCO_Ven();">
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-12 botones" align="right">
						<div class="horizontal" role="group">
							<button id="btnPendiente" type="button" class="btn btn-sm btn-primary" onclick="filtrarCosto('1')">Pendientes</button>
							<button id="btnRechazado" type="button" class="btn btn-sm btn-default" onclick="filtrarCosto('2')">Rechazados</button>
							<button id="btnAprobado" type="button" class="btn btn-sm btn-default" onclick="filtrarCosto('3')">Aprobados</button>
							<button id="btnTodos" type="button" class="btn btn-sm btn-default" onclick="filtrarCosto('4')">Todas</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<!--Encabezado-->



<!--Contenedor-->
<div class="table-responsive" id="tbl">
	<div id="resultadoBusqueda"></div>
</div>
<!--Contenedor-->

<!-- Modal Nuevo Costo -->
<?php include './view/procesos/modal/costo/mCosto.php'; ?>

<!-- Modal Para confirmación eliminación del Costo-->
<?php include './view/procesos/modal/costo/mCostoEliminar.php'; ?>

<script type="text/javascript" src="./assets/js/costo.js"></script>