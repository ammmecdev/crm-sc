<!--Encabezado-->
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-xs">
								<div class="btn-group btn-group-justified" role="group" aria-label="...">
									<div class="btn-group" role="group">
										<button type="button" class="btn btn-default" id="btnFacturas" onclick="verFacturas();" data-toggle="tooltip" title="Facturación"><span class="hidden-xs">Facturación</span> <i class="fas fa-file-invoice-dollar" style="font-size: 17px;"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" class="btn btn-default" id="btnPagos" onclick="verPagos();" data-toggle="tooltip" title="Pagos"><span class="hidden-xs">Pagos</span> <i class="fas fa-hand-holding-usd" style="font-size: 17px;"></i></button>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-lg-4">
								<h3 align="center" style="margin-top: 3px;">Facturación y Pagos</h3>
							</div>
							<div class="col-xs-12 col-sm-4 col-lg-4" align="right">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-sm hidden-md hidden-lg">
						<div class="btn-group btn-group-justified" role="group" aria-label="...">
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-default" id="btnFacturas2" onclick="verFacturas();" data-toggle="tooltip" title="Facturación"><span class="hidden-xs">Facturación</span> <i class="fas fa-file-invoice-dollar" style="font-size: 17px;"></i></button>
							</div>
							<div class="btn-group" role="group">
								<button type="button" class="btn btn-default" id="btnPagos2" onclick="verPagos();" data-toggle="tooltip" title="Pagos"><span class="hidden-xs">Pagos</span> <i class="fas fa-hand-holding-usd" style="font-size: 17px;"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Encabezado-->

<!--Contenedor-->
<div class="panel panel-default" id="pnlFacturas">
	<div class="panel-heading">
		<button type="button" class="btn btn-success btn-sm" onclick="myFunctionNuevaFactura()" data-toggle="modal" data-target="#mFactura">Nueva Factura</button>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr style="background-color: #FCF3CF;">
					<td align="center"><strong>Acción</strong></td>
					<td align="center"><strong>Negocio</strong></td>
					<td align="center"><strong>Número de Factura</strong></td>
					<td align="center"><strong>Fecha de Factura</strong></td>
					<td align="center"><strong>Documento</strong></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach($this->model->ListarFactura() as $r): ?>
					<tr>
						<td align="center">
							<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#mFactura" onclick="myFunctionEditarFactura(<?php echo $r->idFactura; ?>,<?php echo $r->idCotizacion; ?>,<?php echo  "'".$r->noFactura."'"; ?>,<?php echo  $r->fechaFactura; ?>)">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
						</td>
						<td align="center"><?php echo $r->idCotizacion ?></td>
						<td align="center"><?php echo $r->noFactura ?></td>
						<td align="center"><?php echo $r->fechaFactura ?></td>
						<td align="center"><?php echo $r->ruta ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>		
</div>

<div class="panel panel-default" id="pnlPagos">
	<div class="panel-heading">
		<button type="button" class="btn btn-success btn-sm" onclick="myFunctionNuevoPago()" data-toggle="modal" data-target="#mPago">Nuevo Pago</button>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr style="background-color: #FCF3CF;">
					<td align="center"><strong>Acción</strong></td>
					<td align="center"><strong>Negocio</strong></td>
					<td align="center"><strong>Fecha de Pago</strong></td>
					<td align="center"><strong>Fecha de Recibo</strong></td>
					<td align="center"><strong>Número de Recibo</strong></td>
					<td align="center"><strong>Documento</strong></td>
				</tr>	
			</thead>
			<tbody>
				<?php foreach($this->model->ListarPago() as $r): ?>
				<tr>
					<td align="center">
						<a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#mPago" onclick="myFunctionEditarPago(<?php echo $r->idPago; ?>,<?php echo $r->idCotizacion; ?>,<?php echo  "'".$r->fechaPago."'"; ?>,<?php echo  $r->fechaRecibo; ?>,<?php echo  $r->noPago; ?>)">
							<i class="glyphicon glyphicon-pencil"></i>
						</a>
					</td>
					<td align="center"><?php echo $r->idCotizacion ?></td>
					<td align="center"><?php echo $r->fechaPago ?></td>
					<td align="center"><?php echo $r->fechaRecibo ?></td>
					<td align="center"><?php echo $r->noPago ?></td>
					<td align="center"><?php echo $r->ruta ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<!--Contenedor-->
<!--Modal Nueva Factura-->
<div class="modal fade" id="mFactura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><strong> <label id="labTitulo"></label></strong></h4>
			</div>
			<div class="modal-body">
				<form  method="POST" id="form_factura" action="index.php?c=facpago&a=GuardarFactura" role="form" enctype="multipart/form-data">
					<input type="hidden" class="form-control" id="idFactura" name="idFactura" value = "0">
					<div class="form-group">
						<h5>Seleccionar Negocio</h5>
						<select name="selectCotizacion" id="selectCotizacion" class="form-control selectpicker" data-live-search="true" required>
							<option value="">Seleccione el negocio</option>
						</select>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6 col-lg-6">
								<h5>Número de Factura</h5>
								<input type="text" name="noFactura" id="noFactura" class="form-control">
							</div>
							<div class="col-xs-6 col-lg-6">
								<h5>Fecha de la Factura</h5>
								<input type="date" name="fechaFactura" id="fechaFactura" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<h5>Anexar documento</h5>
						<input type="file" name="factura" id="factura" class="form-control">
					</div>
					<div class="modal-footer" style="margin-top: 0px;">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<input type="submit" class="btn btn-success" value="Guardar">
					</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
<!--Modal Nueva Factura-->

<!--Modal Nuevo Pago-->
<div class="modal fade" id="mPago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><strong> <label id="labTitulo"></label></strong></h4>
			</div>
			<div class="modal-body">
				<form id="form_pago" method="POST" action="index.php?c=facpago&a=GuardarPago" role="form" enctype="multipart/form-data">
					<input type="hidden" class="form-control" id="idPago" name="idPago">
					<div class="form-group">
						<h5>Seleccionar Negocio</h5>
						<select class="form-control selectpicker" name="selectCotizacionPago" id="selectCotizacionPago"></select>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6 col-lg-6">
								<h5>Fecha de Pago</h5>
								<input type="date" class="form-control" name="fechaPago" id="fechaPago">
							</div>
							<div class="col-xs-6 col-lg-6">
								<h5>Fecha de Recibo</h5>
								<input type="date" class="form-control" name="fechaRecibo" id="fechaRecibo">
							</div>
						</div>
					</div>
					<div class="form-group">
						<h5>Número de Recibo de Pago</h5>
						<input type="text" class="form-control" name="noPago" id="noPago">
					</div>
					<div class="form-group">
						<h5>Anexar documento</h5>
						<input type="file" class="form-control" name="pago" id="pago">
					</div>
					<div class="modal-footer" style="margin-top: 0px;">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-success">Guardar</button>
					</div>
				</form>
			</div>

		</div>
	</div>
</div>
<!--Modal Nuevo Pago-->
<script src="./assets/js/facpago.js"></script>