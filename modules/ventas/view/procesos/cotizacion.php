<style>
	input#Carrito[type=text] {
		border: none;
		border-bottom: 1px solid #009688;
	}
	.boton {
		border-radius: 88px 88px 88px 88px;
		-moz-border-radius: 88px 88px 88px 88px;
		-webkit-border-radius: 88px 88px 88px 88px;
		border: 0px solid #000000;
	}
	.lectura {
		border: none; 
		background-color: transparent; 
		width: 50%; 
		text-align: center;
	}
	.has-error .help-block {
		color: red;
		}
</style>
<!--Encabezado-->
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mCotizacion" >Nueva Cotización</a>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<h3 align="center" style="margin-top: 3px;">Cotización</h3>
					</div>
					<form method="POST" id="form-expo">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
							<div class="btn-group">
								<input type="hidden" name="idCotiza" class="form-control" id="txtIdCotiza">
								<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultas();">
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-lg-12 botones" align="right">
						<div class="horizontal" role="group">
							<button id="btnPendiente" type="button" class="btn btn-sm btn-primary" onclick="filtrarCosto('1')">Pendientes</button>
							<button id="btnRechazado" type="button" class="btn btn-sm btn-default" onclick="filtrarCosto('2')">Rechazados</button>
							<button id="btnAprobado" type="button" class="btn btn-sm btn-default" onclick="filtrarCosto('3')">Aprobados</button>
							<button id="btnTodos" type="button" class="btn btn-sm btn-default" onclick="filtrarCosto('4')">Todas</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Encabezado-->

<!--Modal para cambiar estado de la cotización -->
<div id="mEstadoCotizacion" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><strong>Cambiar el estado</strong></h4>
			</div>
			<div class="modal-body">
				<h5 style="margin-top: -9px"><strong>Estado actual: </strong><span class="badge" style="background-color: #F9E79F; color: black">Pendiente</span></h5>
				<select class="form-control">
					<option>Aprobado</option>
					<option>Rechazado</option>
				</select>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
				<a href="" class="btn btn-sm btn-success boton">Guardar</a>
			</div>
		</div>
	</div>
</div>
<!-- Fin del modal -->

<!-- Modal Nueva Cotizacion -->
<?php require_once './view/procesos/modal/cotizacion/mCotizacion.php'; ?>

<!-- Modal Eliminar Cotizacion -->
<?php require_once './view/procesos/modal/cotizacion/mCotizacionEliminar.php'; ?>

<!-- Modal Cambiar Estado Cotizacion -->
<?php require_once './view/procesos/modal/cotizacion/mCotizacionCambiarEstado.php'; ?>

<!-- Inicio de contenedor -->
<div class="table-responsive" id="tbl">
	<div id="resultadoBusqueda">
		<!--Aqui se vera a tabla de resultados-->
	</div>
</div>
<!-- Fin de contenedor -->

<!--Metodos js para la cotización -->
<script type="text/javascript" src="./assets/js/cotizacion.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfZw3DDsyLMfVWyhKt9JojWcay2bVd1GA&libraries=places&callback=initAutocomplete"
async defer></script> -->