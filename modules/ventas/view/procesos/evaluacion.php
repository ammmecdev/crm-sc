<style>
.lectura {
	border: none; 
	background-color: transparent; 
	width: 50%; 
	text-align: center;
}
</style>
<!--Encabezado-->
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default" s>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mEvaluacion">Nueva Evaluación</a>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" align="center">
						<div class="btn-group">
							<h5><strong><font id="h5Contador"></font></strong></h5>
						</div>
					</div>

					<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" align="center">
						<form method="POST" action="?c=evaluacion&a=Exportar" enctype="multipart/form-data" id="form-exportar">
							
							<div class="col-lg-3" style="padding-left: 0px;">
								<select class="form-control selectpicker" id="selectAnio" name="selectAnio" onchange="consultas()"> 
									<option value="">Año</option>
									<!-- Aqui se imprime el año de los presupuestos -->
								</select>
							</div>
							<div class=" col-lg-1 hidden-xs" style="padding-left: 0px; ">
								<i class="mdi mdi-remove mdi-2x mdi-rotate-90"></i>
							</div>
							<div class="col-lg-3" style="padding-left: 0px;">
								<select class="form-control selectpicker" id="selectMes" name="selectMes" onchange="consultas();">
									<option value="">Mes</option>
									<option value="1">Enero</option>
									<option value="2">Febrero</option>
									<option value="3">Marzo</option>
									<option value="4">Abril</option>
									<option value="5">Mayo</option>
									<option value="6">Junio</option>
									<option value="7">Julio</option>
									<option value="8">Agosto</option>
									<option value="9">Septiembre</option>
									<option value="10">Octubre</option>
									<option value="11">Noviembre</option>
									<option value="12">Diciembre</option>
								</select>
							</div>
							
						
							<div  align="right">
								<div class="btn-group">
									<div class="form-group">
										<input type="text" id = "vBusqueda" name="vBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultas();">
									</div>
								</div>
								<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
									<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro" id="btnExportar"><i class="fas fa-download"></i></button>
								</div>
							</div>
						</form>
					
					</div>
						
						
				</div>
					
					
				</div>
			</div>
		</div>
	</div>
</div>
<!--Encabezado-->

<!-- Inicio del contenedor -->
<div class="table-responsive" id="tbl">  

	<div id="resultadoBusqueda">
		<!--Aqui se vera a tabla de resultados-->
	</div>

</div>
<!-- Fin del contenedor -->

<!-- Modal Nueva Evaluacion -->
<?php include './view/procesos/modal/evaluacion/mEvaluacion.php'; ?>

<!-- Modal Para ver Aspectos de la Evaluacion -->
<?php include './view/procesos/modal/evaluacion/mAspectosEvaluacion.php'; ?>


<!-- Modal Para Confirmacion de borrado -->
<?php include './view/procesos/modal/evaluacion/mEvaluacionEliminar.php'; ?>


<script src="./assets/js/evaluacion.js">