<style>
.sortable {
	border: 0px solid;
	list-style-type: none;
	min-height: 24px;
}

.sortable li {
	width: auto;
	list-style-type: none;
}

.placeholder {
	border: 1px;
	margin: 0 1em 1em 0;
	height: 40px;
	background-color: rgba(0, 0, 0, 0);
	/*gba(242, 243, 244);*/
}
.green {
	color: green;
}
.yellow {
	color: #DAA520;
}
.silver {
	color: #808080;
}
.red {
	color: red;
}

table{
	table-layout: fixed;
	border: 2px solid #000;
}

ul,ol{
	margin-top: 0px;
	margin-bottom: 0px;
}

#miTablaPersonalizada td{
	width: 180px;
	overflow: auto;
	min-width: 180px;
}

input.oculto[type="radio"]{
	visibility: hidden;
}

.btn-pressed {
	background-color: #00a65a;
	border-color: #00a65a; 
}

label.btn-pressed>i {
	color: #fff;
}
label.btn-pressed>span {
	color: #fff;
}

.estancado {
	color: #000;
	border-color: #fff;
}

.item_embudo{
	border: 1px solid #c4c4c4; 
	box-shadow: 0 0px 1px 0 rgba(0,0,0,0.2), 0 6px 14px 0 rgba(0,0,0,0.19);
}

p{
	margin-left: 5px;
}
</style>
<!-- Inicio del contenedor -->
<div id="resultBusquedaEmbudo">
	<!--Aqui se vera a tabla de resultados-->
</div>
<!-- Fin del contenedor -->

<!-- Modal -->
<div class="modal fade" id="completarActividad" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div id="ModalR">

			</div>
		</div>
	</div>
</div>

<!--Modal Completado-->

<div class="modal fade" id="mTerminarr" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  method="post" data-toggle="validator" action="index.php?c=actividades&a=CambiaEstado" enctype="multipart/form-data" role="form" id="form-cambiaEstado">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font id="labActComp"></font></strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="">Fecha:</label>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="date" class="form-control" name="fechaCompletado" required id="txtFechaCompletado">
						</div>
					</div>
				</div>

				<input type="hidden" id="txtEstado" name="completado">
				<input type="hidden" id="txtIdActividad2" name="idActividad">

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<input type="submit" class="btn btn-success" value="Guardar">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal Añadir Actividad-->
<div class="modal fade" id="añadiractividad" role="dialog">
	<div class="modal-dialog">
		<form method="post" action="index.php?c=actividades&a=Guardar" id="form-actividades">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label>Programar actividad</label> </strong></h4>
				</div>
				<div class="modal-body">  
					<!-- Cuerpo -->
					<div class="form-group">
						<input type="hidden" class="form-control" value="0" name="idActividad" id="txtIdActividad" readonly>
						<input type="hidden" class="form-control" id="txtConfirmado" name="confirmado" readonly>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<font size="1">TIPO DE ACTIVIDAD</font>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="btn-group form-group">
								<label class="btn btn-sm btn-default" id="labLlamada" onclick="tipoActividad('Llamada');" data-toggle="tooltip" title="Llamada">
									<i class="mdi mdi-phone-in-talk mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionLlamada">
								</label>
								<label class="btn btn-sm btn-default" id="labReunion" onclick="tipoActividad('Reunion');" data-toggle="tooltip" title="Reunión">
									<i class="mdi mdi-group mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionReunion">
								</label>
								<label class="btn btn-sm btn-default" id="labTarea" onclick="tipoActividad('Tarea');" data-toggle="tooltip" title="Tarea">
									<span>
										<i class="far fa-clock" style="font-size: 14px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionTarea">
								</label>
								<label class="btn btn-sm btn-default" id="labPlazo" onclick="tipoActividad('Plazo');" data-toggle="tooltip" title="Plazo">
									<span>
										<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionPlazo">
								</label>
								<label class="btn btn-sm btn-default" id="labEmail" onclick="tipoActividad('Email');"  data-toggle="tooltip" title="Correo electrónico">
									<i class="mdi mdi-email mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionEmail">
								</label>
								<label class="btn btn-sm btn-default" id="labComida" onclick="tipoActividad('Comida');" data-toggle="tooltip" title="Comida">
									<span>
										<i class="fas fa-utensils" style="font-size: 13px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionComida">
								</label>
								<label class="btn btn-sm btn-default" id="labWhatsApp" onclick="tipoActividad('WhatsApp');" data-toggle="tooltip" title="WhatsApp">
									<span>
										<i class="fab fa-whatsapp" style="font-size: 15px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionWhatsapp">
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipo" required>
					</div>

					<div class="form-group">
						<textarea id="txtNotasA" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas" required></textarea>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<font size="1">FECHA</font>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="date" class="form-control" name="fecha" id="txtfechaActividad" required />
							</div>
							<div class="col-lg-12">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name='checkTiempo' id="checkTiempo"> Asignar hora y duración
							</label>
						</div>
					</div>

					<div id="divTiempo"> 
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">HORA</font>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="time" class="form-control" name="hora" id="txtHora" onblur="deshabilitaConfirmado()" />
								</div>
								<div class="col-lg-12">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-lg-12">
									<font size="1">DURACIÓN</font>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<input type="number" class="form-control" name="duracion" id="txtDuracion" min="1" max="100" onblur="deshabilitaConfirmado()"/>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<select class="form-control" name="tipoDuracion" id="selectTipoDuracion" onchange="deshabilitaConfirmado()">
										<option value="minutos">Minutos</option>
										<option value="horas">Horas</option>
									</select>
								</div>
								<div class="col-lg-12">
								</div>
							</div>
						</div>
					</div>

					<input name="radioFiltros" value="negocios" type="hidden">

					<div class="form-group" id="selectPersonas" style="margin-top: 15px;">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-tower"></span></span>
							<input class="form-control" id="txtNomOrganizacion" name="nombreOrganizacion" readonly="readonly">
							<input type="hidden" id="txtIdOrganizacion" name="idOrganizacion">
						</div>
					</div>

					<div class="form-group" id="selectPersonas">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
							<input class="form-control"  id="txtnombrePersona" name="nombrePersona" readonly="readonly">
							<input type="hidden" id="txtidCliente" name="idCliente">
						</div>
					</div>

					<div class="form-group" id="selectPersonas">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-briefcase"></span></span>
							<input class="form-control" id="txttituloNegocio" required disabled>
							<input type="hidden" id="txtidNegocio" name="idNegocio">
						</div>
					</div>

					<div class="row" id="divCruzadas" style="margin-bottom: -50px;">
						<div class="form-group">
							<div class="col-lg-12">
								<div class="help-block with-errors" id="cruzadas"></div>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-success" id="btnGuardarActividad">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> 
				</div>
			</div>
			<!--Fin Modal content-->
		</form>
	</div>
</div>


<script type="text/javascript">
	$( '#checkTiempo' ).on( 'click', function() {
		if( $(this).is(':checked') ){
			$('#divTiempo').show();
			$('#txtHora').focus();
			$('#divCruzadas').show();
			$('#txtConfirmado').val("false");
			$('#btnGuardarActividad').prop('disabled', false);
			$('#txtHora').prop('disabled', false);
			$('#txtDuracion').prop('disabled', false);
		} else {
			$('#divTiempo').hide();
			$('#divCruzadas').html("");
			$('#divCruzadas').hide();
			$('#btnGuardarActividad').val('Guardar');
			$('#btnGuardarActividad').prop('disabled', false);
			$('#txtHora').prop('disabled', true);
			$('#txtDuracion').prop('disabled', true);
		}
	});
	
//Metodo para cambiar el estado en la base de datos de acuerdo al idActividad
cambiaEstado = function(idActividad,estado,fechaCompletado)
{
	$('#txtIdActividad2').val(idActividad);
	$('#txtEstado').val(estado);
	if(estado==1){
		$('#labActComp').html("Actividad completada");
		$('#Guardar2').val('Desacompletar');
		$('#txtFechaCompletado').val(fechaCompletado);
	}
	else{
		$('#labActComp').html("Completar actividad");
		$('#Guardar2').val('Completar');
		$('#txtFechaCompletado').val('');
	}
}

//Metodo de busqueda por ajax
consultas = function (){ 
	var idEmbudo = $('#selectIdEmbudo').val();
	var busqueda=$("#buscar").val();
	var filtro = $('#filtros').val();
	var nombre = $('#selectIdEmbudo option:selected').html();
	$.post("index.php?c=negocios&a=Consultas", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro }, function(mensaje) {
		$("#resultBusquedaEmbudo").html(mensaje);
	});
}

$('#form-cambiaEstado').submit(function(){
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
	}).done(function(respuesta){
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
		$('#mTerminarr').modal('toggle');
		$('#completarActividad').modal('toggle');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
		consultas();
	});
	return false;
});

function myFunctionVerActividad(idOrganizacion, nombreOrganizacion, idCliente, nombrePersona, idNegocio, tituloNegocio) {
	$('#txtIdActividad').val("");
	$('#txtIdOrganizacion').val(idOrganizacion); 
	$('#txtNomOrganizacion').val(nombreOrganizacion);
	$('#txtidCliente').val(idCliente);
	$('#txtnombrePersona').val(nombrePersona);
	$('#txtidNegocio').val(idNegocio);
	$('#txttituloNegocio').val(tituloNegocio);
	$.post("index.php?c=negocios&a=ActividadesPorNegocio", {valorIdNegocio: idNegocio}, function(mensaje) {
		$("#ModalR").html(mensaje);
	});
}

function myFunctionNuevaActividad(idActividad,tipo,fechaHora,notas) {
	$('#txtIdActividad').val(idActividad); 
	$('#txtTipo').val(tipo); 
	$('#txtfechaActividad').val(fechaHora);
	$('#txtNotasA').val(notas);
}

function myFunctionlimpiar() {
	$('#divTiempo').hide();
	$('#txtIdActividad').val("");
	$('#txtTipo').val(""); 
	$('#txtfechaActividad').val("");
	$('#txtNotasA').val("");
	$('#divCruzadas').hide();
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#checkTiempo').prop('checked',false);

}
function myFunctionAñadirActividad(idOrganizacion, nombreOrganizacion,idCliente,nombrePersona,idNegocio, tituloNegocio) 
{
	$('#btnGuardarActividad').val('Guardar');
	$('#txtIdActividad').val("");
	$('#txtTipo').val(""); 
	$('#txtfechaActividad').val("");
	$('#txtNotasA').val("");
	$('#divCruzadas').hide();
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#checkTiempo').prop('checked',false);
	$('#txtIdOrganizacion').val(idOrganizacion); 
	$('#txtNomOrganizacion').val(nombreOrganizacion);  
	$('#txtidCliente').val(idCliente);
	$('#txtnombrePersona').val(nombrePersona);
	$('#txtidNegocio').val(idNegocio);
	$('#txttituloNegocio').val(tituloNegocio);
	$('#divTiempo').hide();
	$('#txtHora').prop('disabled', true);
	$('#txtDuracion').prop('disabled', true);
}

$('#form-actividades').bootstrapValidator({
	submitHandler: function(validator, form, submitButton) {
		$.ajax({
			type: 'POST',
			url: form.attr('action'),
			data: form.serialize(),
			success: function(respuesta){
				if(respuesta[0].mensaje == "cruzada"){
					$('#divCruzadas').show();
					$("#cruzadas").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
					$('#txtConfirmado').val('true');
					$('#btnGuardarActividad').val('Confirmar');
					$('#btnGuardarActividad').prop('disabled',false);
				}else{
					$('#añadiractividad').modal('toggle');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
					$('#completarActividad').modal('toggle');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					consultas();
				}
			}
		})      
		return false;
	},
	fields: {
		options: {
			validators: {
				notEmpty: {
					message: 'Selección Obligatoria'
				}
			}
		},
		notas: {
			validators: {
				notEmpty: {
					message: 'Campo Obligatorio'
				}
			}
		},
		fecha: {
			validators: {
				notEmpty: {
					message: 'Campo Obligatorio'
				}
			}
		},
		hora: {
			validators: {
				notEmpty: {
					message: 'Campo Obligatorio'
				}
			}
		},
		duracion: {
			validators: {
				notEmpty: {
					message: 'Campo Obligatorio'
				}
			}
		},
	}
});
$('#añadiractividad')
.on('shown.bs.modal', function() {
	/*$('#form-actividades').find('[name="fechaEmision"]').focus();*/
})
.on('hidden.bs.modal', function () {
	$('#form-actividades').bootstrapValidator('resetForm', true);
});

tipoActividad = function (tipo)
{
	$('#txtTipo').val(tipo);

	switch(tipo){
		case 'Llamada':
		desmarcar("Llamada");
		break;
		case 'Reunion':
		desmarcar("Reunion");
		break;
		case 'Tarea':
		desmarcar("Tarea");
		break;
		case 'Plazo':
		desmarcar("Plazo");
		break;
		case 'Email':
		desmarcar("Email");
		break;
		case 'Comida':
		desmarcar("Comida");
		break;
		case 'WhatsApp':
		desmarcar("WhatsApp");
		break;
		default: 
		break;
	}
}  

desmarcar = function(tipo) {
	var miarray = ["Llamada", "Reunion", "Tarea", "Plazo", "Email", "Comida", "WhatsApp"];

	for (var i = 0; i < miarray.length; i++) {
		if (tipo == miarray[i]) {
			$("#lab"+miarray[i]).addClass("btn-pressed");
			$("#lab"+miarray[i]).removeClass("btn-default");
		}else{
			$("#lab"+miarray[i]).removeClass("btn-pressed");
			$("#lab"+miarray[i]).addClass("btn-default");
		}
	}
}

function deshabilitaConfirmado(){
	$('#txtConfirmado').val("false");
	$('#divCruzadas').hide();
	$('#btnGuardarActividad').val("Guardar");
	$('#btnGuardarActividad').prop('disabled',false);
}


$(function(){
	$('[rel="popover"]').popover({
		container: 'body',
		html: true,
		content: function () {
			var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
			return clone;
		}
	}).click(function(e) {
		e.preventDefault();
	});
});

</script>