<style>
.formulario label {
	display: inline-block;
	cursor: pointer;
	background-color: rgba(0,0,0,.1);
	color: #0074D9;
	position: relative;
	padding: 5px 15px 5px 51px;
	font-size: 1em;
	border-radius: 5px;
	-webkit-transition: all 0.3s ease;
	-o-transition: all 0.3s ease;
	transition: all 0.3s ease;
}
.formulario label:hover {
	background: rgba(0, 116, 217, 0.1);
}
.formulario input[type="radio"] {
	display: none;
}
.formulario input[type="radio"]:checked + label {
	padding: 5px 15px;
	background: #0074D9;
	border-radius: 2px;
	color: #fff;
}
ul.pagination>li>a{
	cursor: pointer;
	background-color: #fff;
	border-color: #ddd;	
}
ul.pagination>li#previous>a{
	color: #000;
	background-color: #fff;
	border-color: #ddd;
}

</style>
<?php 
if (!empty($_REQUEST['a'])) {
	$A=$_REQUEST['a'];
	if ($A=='timeline') {
		$btntrue=true;
	}
}else{
	$btntrue=true;
}

?>
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-xs-5 col-sm-4 col-md-4 col-lg-4" id="pan2">
					<div class="btn-group form-group" role="group">
						<a href="?c=negocios" class="btn btn-default btn-sm" data-toggle="tooltip" title="Embudo"><i class="mdi mdi-equalizer mdi-rotate-180 mdi-lg"></i></a>
						<a href="?c=negocios&a=lista" class="btn btn-default btn-sm" data-toggle="tooltip" title="Lista"><i class="fas fa-list"></i></a>
						<a href="?c=negocios&a=timeline" class="btn btn-default btn-sm" data-toggle="tooltip" title="Pronóstico"><i class="mdi mdi-restore mdi-lg"></i></a>
					</div>
					<div class="btn-group">
						<div class="form-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mNegocio" onclick="myFunctionNuevo()"> Añadir Negocio</a> 
						</div>
					</div>
				</div>
				<div class="col-xs-2 col-sm-4 col-md-3 col-lg-4" style="padding-left: 0px; padding-right: 0px;" align="center">
					<?php 
					if (!empty($_REQUEST['a'])) {
						$A=$_REQUEST['a'];
						if ($A=='timeline') {
							?>

							<div class="col-xs-12 col-lg-4" style="margin-bottom: 3px;">
								<select class="form-control selectpicker show-menu-arrow" id="selectAño" onchange="mesAño();">
									<?php 
									date_default_timezone_set("America/Mexico_City"); 
									$Año= date("Y");
									$AñoD=$Año-3;
									$AñoP=$Año;
									?>
									<option value=""><?php echo $Año; ?></option>
									<?php 
									for ($i=0; $i < 2; $i++) { 
										$AñoD=$AñoD+1;
										?>
										<option value="<?php echo $AñoD; ?>"><?php echo $AñoD; ?></option>
										<?php 
									}
									?>
									<?php 
									for ($i=0; $i < 2; $i++) { 
										$AñoP=$AñoP+1;
										?>
										<option value="<?php echo $AñoP; ?>"><?php echo $AñoP; ?></option>
										<?php 
									}
									?>
								</select>
							</div>
							<div class="col-xs-12 col-lg-1 hidden-xs" style="padding-left: 0px; padding-right: 0px;">
								<i class="mdi mdi-remove mdi-2x mdi-rotate-90"></i>
							</div>
							<div class="col-lg-4" style="padding-left: 15px;">
								<select class="form-control selectpicker show-menu-arrow" id="selectMes" onchange="mesAño();">
									<?php 
									$meses = array("Cero","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
									$mes= date("n");

									for ($i=0; $i < 12; $i++) { 
										if ($mes==13) {
											$mes=1;
										}
										echo '<option value="'.$mes.'">'.$meses[$mes].'</option>';
										$mes++;
									}

									?>
								</select>
							</div>
							<?php 
						}elseif ($A=='lista') { ?>
							<div class="btn-group" id="resultadoContadorNegocios">
								<!-- Aqui se imprimira el contantador de negocios-->
							</div>
						<?php }
					}else { ?>
						<div class="btn-group" id="resultadoContadorNegocios">
							<!-- Aqui se imprimira el contantador de negocios-->
						</div>
					<?php } ?>
				</div>

				<div class="col-xs-5 col-sm-4 col-md-5 col-lg-4" style="padding-left: 25px; padding-right: 0px;" align="right">
					<form  method="POST" action="?c=negocios&a=Exportar" enctype="multipart/form-data">
						<div class="btn-group">
							<div class="form-group">
								<input type="hidden" name="txtIdEmbudo" id="txtBusIdEmbudo">
								<input type="hidden" name="filtroStatus" id="txtFiltro">
								<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="buscar" onkeyup="paginador(paginaActual);">
							</div> 
						</div>
						<div class="btn-group form-group" style="padding-left: 0px; padding-right: 0px;">
							<select class="form-control selectpicker show-menu-arrow" name="idEmbudo" type="button" id="selectIdEmbudo" onchange="listarEmbudos()">

								<option class="btn-default" value="<?php echo $_SESSION['idEmbudo']?>"><?php echo $_SESSION['nombre']; ?> </option>

							</select>
						</div>
						<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
							<?php 
							if (!empty($_REQUEST['a'])) {
								$A=$_REQUEST['a'];
								if ($A=='timeline') {
								}elseif ($A=='lista') { 
									echo '<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>';
								}
							}else { 
								echo '<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>';
							} 
							?>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-7 col-sm-9 col-md-9 col-lg-9">
						<?php 
						if (!empty($_REQUEST['a'])) {
							$A=$_REQUEST['a'];
							if ($A=='lista') {
								?>
								<div class="row">
									<div class="col-xs-3 col-lg-2">
										<div class="row">
											<div class="col-xs-5 col-lg-5" style="margin-top: 6px;">
												<p>Registros:</p>
											</div>
											<div class="col-xs-5 col-lg-7">
												<select class="form-control selectpicker" id="selectPorPagina" onchange="paginador(1);" width=50%>
													<option value="5">5</option>
													<option value="10">10</option>
													<option value="25">25</option>
													<option value="50">50</option>
													<option value="100">100</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							<?php } } ?>
						</div>
						<div class="col-xs-5 col-sm-3 col-md-3 col-lg-3">
							<div class="col-lg-5"></div>
							<div class="col-xs-12 col-lg-7" style="padding-left: 0px; padding-right: 0px;">
								<select class="form-control selectpicker show-menu-arrow" onchange="tipoFiltro();" id="selectFiltro">
									<optgroup label="Mostrar negocios">
										<option value="0" data-icon="mdi mdi-equalizer mdi-lg mdi-rotate-180">Abiertos</option>
										<option value="1" data-icon="mdi mdi-check-circle mdi-lg">Ganados</option>
										<option value="2" data-icon="mdi mdi-cancel mdi-lg">Perdidos</option>
										<?php if (empty($btntrue)) { ?>
											<option value="" data-icon="mdi mdi-view-week mdi-lg">Todos</option>
										<?php } ?>
									</optgroup>
								</select>
							</div>
							<input type="hidden" value="0" id="filtros">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Inicio modal de añadir negocio -->
	<div class="modal fade" id="mNegocio" role="dialog" data-backdrop="static" data-keyboard="false" tabindex="-1">
		<div class="modal-dialog">
			<form data-toggle="validator" method="post" action="index.php?c=negocios&a=Guardar" role="form" id="form-negocios">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header bg-gray">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong><label id="labTitulo"></label></strong></h4>
					</div>
					<div class="modal-body">  
						<!-- Cuerpo -->
						<!-- Este idNegocio es para saber si es nuevo o simplemente se va editar-->
						<input type="hidden" name="idNegocio" id="txtIdNegocio">
						<input type="hidden" name="idNegocioEst" id="txtIdNegEst">
						<input type="hidden" name="estimacion" id="txtEstimacion">
						
						<div class="form-group" id="divFiltros">
							<div class="col-sm-12">
								<div class="radio">
									<input type="radio" id="porOrganizacion" name="fOrganizacion"  value="1" checked>Filtrar por cliente
								</div>
								<div class="radio">
									<input type="radio" id="porPersona" name="fPersona" value="0">Filtrar por persona de contacto
								</div>
							</div>
						</div>

						<!-- Estos son los ids que se van a enviar por post para registrar las llaves foraneas de organizaciones y personas -->
						<input type="hidden" class="form-control" name="idOrganizacion" id="inputOrganizacion">
						<input type="hidden" class="form-control" name="idCliente" id="inputCliente">

						<!-- Nombre de la organización -->
						<div class="form-group" id="divOrganizacion">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2">
									<span class="glyphicon glyphicon-briefcase"></span>
								</span>
								<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idOrganizacionN" id="selectIdOrganizacion" onchange="listarPersonasPorOrganizacion();">
								</select>
							</div>
						</div>

						<!-- Nombre de la persona de contacto para el filtro de organizacion-->
						<div class="form-group" id="divPersonasF">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<span class="glyphicon glyphicon-user"></span>
								</span>
								<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idClienteF" id="selectIdPersonaF" onchange="obtenerIdPersona();">
								</select>
							</div>
						</div>

						<!-- Nombre de la persona de contacto-->
						<div class="form-group" id="divPersonas">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<span class="glyphicon glyphicon-user"></span>
								</span>
								<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idClienteN" id="selectIdPersona" onchange="listarOrganizacionesPorPersona();">
								</select>
							</div>
						</div>

						<!-- Nombre de la organización para el filtro de personas -->
						<div class="form-group" id="divOrganizacionF">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2">
									<span class="glyphicon glyphicon-briefcase"></span>
								</span>
								<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idOrganizacionF" id="selectIdOrganizacionF" onchange="obtenerIdOrganizacion();">
								</select>
							</div>
						</div>

						<div class="form-group">
							<h5>Título del negocio</h5>
							<input type="text" class="form-control" name="tituloNegocio" id="txtTituloNegocio" maxlength="50" placeholder="Título del negocio">
						</div>

						<h5>Etapa del embudo</h5>
						<div class="form-group" id="ConsultaEtapa">
							<!-- Aquí se imprime el select segun sea el embudo seleccionado y el input tipo hidden que contiene el idEmbudo -->
						</div>

						<input type="hidden" class="form-control" name="nombreEtapa" id="txtNombreEtapa">

						<div id="divClaveNegocio">
							<div class="row">
								<div class="form-group">
									<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0px;">
										<h5>Clave del Negocio</h5>
									</div>
								</div>
							</div>

							<div class="row" style="padding-bottom: 5px;">
								<div class="form-group">
									<div class="col-xs-12 col-md-12 col-lg-12">
										<div class="row">
											<div class="col-xs-4 col-md-4 col-lg-4">
												<p class="help-block">Clave</p>
											</div>
											<div class="col-xs-4 col-md-4 col-lg-4">
												<p class="help-block">Consecutivo</p>
											</div>
											<div class="col-xs-4 col-md-4 col-lg-4">
												<p class="help-block">Servicio</p>
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-md-4 col-lg-4">
										<input type="text" class="form-control" maxlength="3" name="claveOrganizacion" id="txtClaveOrganizacion" placeholder="-- -- --" readonly>
									</div>

									<div class="col-xs-4 col-md-4 col-lg-4">
										<input type="text" class="form-control" maxlength="4" name="consecutivo" id="txtConsecutivo" value="0000" readonly>
									</div>
									<div class="col-xs-4 col-md-4 col-lg-4" id="selectServicios">
										<select name="servicio" id="txtServicio" class="form-control">
											<option value="">---</option>
											<option value="CAP" title="Capacitación">CAP</option>
											<option value="CST" title="Consultoría técnica">CST</option>
											<option value="ASC" title="Servicio AssetCare">ASC</option>
											<option value="SAV" title="Servicio de Análisis de Vibraciones">SAV</option>
											<option value="SBD" title="Servicio de Balanceo Dinámico">SBD</option>
											<option value="STI" title="Servicio de Termografía Infrarroja">STI</option>
											<option value="SMR" title="Suministros">SMR</option>
											<option value="PDM" title="Mantenimiento Predictivo">PDM</option>
										</select>
									</div>
									<div class="col-xs-4 col-md-4 col-lg-4" id="inputServicio">
										<input type="text" class="form-control" id="servicioEst" readonly>
									</div>
								</div>
							</div>
						</div>

						<h5>Unidad de negocio</h5>
						<div class="form-group">
							<select class="form-control" name="idUnidadNegocio" id="selectIdUnidadNegocio">
							</select>
						</div>

						<h5>Equipo</h5>
						<div class="form-group">
							<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idEquipo" id="selectIdEquipo">
							</select>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0px;">
									<h5>Valor del Negocio</h5>
								</div>

								<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="padding-left: 10px;">
									<h5>Tipo de Moneda</h5>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-4 col-sm-4" style="width: 40%; padding-right: 0px">
									<input type="text" class="form-control" maxlength="12" name="valorNegocio" id="txtValorNegocio" placeholder="Valor" data-toggle="tooltip" title="Formato 000.00 / 000">

								</div>
								<div class="col-xs-8 col-sm-8" style="width: 60%" id="divSelectTipoMoneda">
									<select class="form-control" name="tipoMoneda" id="selectTipoMoneda" required>
										<option value="MXN">Pesos Méxicanos (MXN)</option>
										<option value="USD">US Dollar (USD)</option>
									</select>
								</div>
								<div class="col-xs-8 col-sm-8" style="width: 60%" id="divInputTipoMoneda">
									<input type="text" class="form-control" id="inputTipoMoneda" readonly>
								</div>        
							</div>
						</div>

						<div class="form-group" id="divFechaCierre">
							<h5>Fecha de cierre prevista</h5>
							<input type="date" class="form-control" name="fechaCierre" id="txtFechaCierre">
						</div>
						
						<div id="divPonderacion">
							<h5>% Ponderación</h5>
							<div class="form-group">
								<select class="form-control" name="ponderacion" id="selectPonderacion">
									<option value="">Seleccione la ponderación</option>
									<option value="25%">25 %</option>
									<option value="50%">50 %</option>
									<option value="75%">75 %</option>
									<option value="100%">100 %</option>
								</select>
							</div>
						</div>

						<h5 id="labelPresupuesto">Agregar a presupuesto</h5>
						<div class="form-group formulario" id="divPresupuesto">
							<div class="radio" style="padding-left: 0px;">
								<input type="radio" name="filtro" id="No" value="0" checked>
								<label for="No">No</label>

								<input type="radio" name="filtro" id="Si" value="1">
								<label for="Si">Si</label>
							</div>
						</div>

						<div id="ActivarPresupuesto">
							<div class="form-group">
								<select name="idPresupuesto" id="selectPresupuesto" class="form-control selectpicker show-menu-arrow" data-live-search="true">

								</select>                        
							</div>
							<div class="form-group">
								<textarea name="descripcion" id="txtNotas" class="form-control" placeholder="Descripción"></textarea>
							</div>
						</div>

						<input type="hidden" class="form-control" name="xCambio" id="valorUSD" value="1">
						<!-- fin cuerpo -->
					</div>
					<div class="modal-footer">
						<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
							<div class="col-lg-3" align="left" style="padding-left: 0px">
								<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar negocio" id="btnEliminar" onclick="myFunctionEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
							</div>
							<div class="col-lg-9" align="right" style="padding-right: 0px">
								<input type="submit" class="btn btn-success" id="btnGuardar" >
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>  
							</div>  
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>     
	<!--fin modal de añadir negocio -->

	<!--Inicio modal para eliminar negocio -->
	<div class="modal fade" id="mEliminar" role="dialog">   
		<div class="modal-dialog">
			<form  method="post" action="index.php?c=negocios&a=Eliminar" role="form" id="form-negocios-eliminar">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header bg-danger">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong>Eliminar negocio</strong></h4>
					</div>
					<div class="modal-body"  style="margin-bottom: -30px;">   
						<!-- Cuerpo --> 
						<p style="font-size: 20px;">¿Esta seguro que desea eliminar el negocio?</p>
						<input type="hidden" id="txtIdNegocioE" name="idNegocio">
						<input type="hidden" class="form-control" name="cambio" value="<?php echo $_REQUEST['a']; ?>">
						<!-- fin cuerpo --> 
					</div>
					<div class="modal-footer">
						<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
							<input type="submit" class="btn btn-danger" value="Eliminar">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
						</div>
						<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
					</div>
				</div>
			</form>
		</div>
	</div>     
	<!--fin modal para eliminar negocio -->

	<!--Inicio modal para cambiar la clave -->
	<div class="modal fade" id="mCambiarClave" role="dialog">   
		<div class="modal-dialog">
			<form  data-toggle="validator" method="post" action="index.php?c=negocios&a=CambiarClave" role="form" id="form-cambiar">
				<div class="modal-content">
					<div class="modal-header bg-gray">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong>Clave de negocio</strong></h4>
					</div>
					<div class="modal-body">  
						<h5 id="txtNombreNegocio"></h5>
						<input type="hidden" name="idNegocio" id="id_idNegocio">
						<hr style="margin-top: 14px; margin-bottom: 8px;">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-5 col-sm-5 col-md-5 col-lg-12" style="padding-right: 0px;">
									<h5><span class="glyphicon glyphicon-retweet"></span><strong> Cambiar clave de forma manual. </strong></h5>
								</div>
							</div>
						</div>

						<div class="row" style="padding-bottom: 5px;">
							<div class="form-group">
								<div class="col-xs-12 col-md-12 col-lg-12">
									<div class="row">
										<div class="col-xs-4 col-md-4 col-lg-4">
											<p class="help-block">Clave</p>
										</div>
										<div class="col-xs-4 col-md-4 col-lg-4">
											<p class="help-block">Consecutivo</p>
										</div>
										<div class="col-xs-4 col-md-4 col-lg-4">
											<p class="help-block">Servicio</p>
										</div>
									</div>
								</div>

								<div class="col-xs-4 col-md-4 col-lg-4">
									<input type="text" class="form-control" maxlength="3" name="claveOrganizacion" id="txtClaveOrg" placeholder="-- -- --" readonly>
								</div>

								<div class="col-xs-4 col-md-4 col-lg-4">
									<input type="text" class="form-control" maxlength="4" name="claveConsecutivo" id="txtCConsecutivo" placeholder="0000" maxlength="4">
								</div>
								<div class="col-xs-4 col-md-4 col-lg-4">
									<select name="claveServicio" id="txtCServicio" class="form-control">
										<option value="">---</option>
											<option value="CAP" title="Capacitación">CAP</option>
											<option value="CST" title="Consultoría técnica">CST</option>
											<option value="ASC" title="Servicio AssetCare">ASC</option>
											<option value="SAV" title="Servicio de Análisis de Vibraciones">SAV</option>
											<option value="SBD" title="Servicio de Balanceo Dinámico">SBD</option>
											<option value="STI" title="Servicio de Termografía Infrarroja">STI</option>
											<option value="SMR" title="Suministros">SMR</option>
											<option value="PDM" title="Mantenimiento Predictivo">PDM</option>
									</select>
								</div>
							</div>
							<div id="alerta" class="col-lg-12" style="margin-top: 5px;"></div>
						</div>
					</div>

					<div class="modal-footer">
						<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
							<input type="submit" class="btn btn-success" id="btnGuardarCClave" value="Guardar">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>     
	<!--fin modal para para cambiar la clave -->

	<!-- El include nos imprimira el contenedor segun sea el caso -->
	<?php include($this->contenedor); ?>
	
	<script>	
		var paginaActual = 1;
		$(document).ready(function() {
			paginador(paginaActual);
			listarEmbudos();
			Contadores();

			$('#form-negocios').bootstrapValidator({ /*Función para registrar y actualizar negocios por ajax*/
				submitHandler: function(validator, form, submitButton) {
					$.ajax({
						type: 'POST',
						url: form.attr('action'),
						data: form.serialize(),
						success: function(respuesta) {
							$('#mNegocio').modal('toggle');
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							paginador(paginaActual); /*Imprimir la lista al realizar el registro */
							Contadores();
						}    
					});        
					return false;
				},
				fields: {
					idOrganizacionN: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idClienteF: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idClienteN: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idOrganizacionF: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					tituloNegocio: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					servicio: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idEquipo: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idUnidadNegocio: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					valorNegocio: {
						validators: {
							regexp: {
								regexp: /^(([0-9]){1})?([0-9]+([.]?){1})?(([0-9]{2,2})+){1}?$/,
								message: 'Formato incorrecto'
								/*^((([0-9])+[,]){1})?([0-9]+([.]?){1})?(([0-9]{2,2})+){1}?$*/
							},
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idEtapa: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					fechaCierre: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					ponderacion: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					idPresupuesto: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					descripcion: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					}
				}
			});
			$('#mNegocio')
			.on('shown.bs.modal', function() {
			})
			.on('hidden.bs.modal', function () {
				$('#form-negocios').bootstrapValidator('resetForm', true);
			});
			$("#porOrganizacion").change(function(){
				filtrarPorOrganizaciones();
			});

			$("#porPersona").change(function(){
				filtrarPorPersonas();
			});

			$("#No").change(function(){
				myFunctionCheckedNo();
			});

			$("#Si").change(function(){
				myFunctionCheckedSi();
			});

			$('#form-negocios-eliminar').submit(function() {
				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: $(this).serialize(),
					success: function(respuesta) {
						$('#mNegocio').modal('toggle');
						$('#mEliminar').modal('toggle');
						$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
						$('#mensajejs').show();
						$('#mensajejs').delay(3500).hide(600); 
						paginador(paginaActual); /*Imprimir la lista al realizar el registro */
						Contadores();
					}    
				})        
				return false;
			}); 

			$('#form-cambiar').bootstrapValidator({
				submitHandler: function(validator, form, submitButton) {
					$.ajax({
						type: 'POST',
						url: form.attr('action'),
						data: form.serialize(),
						success: function(respuesta) {
							$('#mCambiarClave').modal('toggle');
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
							$('#mensajejs').show();
							$('#mensajejs').delay(3500).hide(600);
							paginador(paginaActual);
							Contadores();
						}    
					});        
					return false;
				},
				fields: {
					clave_Servicio: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					},
					clave_Consecutivo: {
						validators: {
							notEmpty: {
								message: 'Campo Obligatorio'
							}
						}
					}
				}
			});
			$('#mCambiarClave')
			.on('shown.bs.modal', function() {	
			})
			.on('hidden.bs.modal', function () {
				$('#form-cambiar').bootstrapValidator('resetForm', true);
			});
		});

$("#txtCConsecutivo").keyup(function() {
	$('#alerta').hide();
	var claveOrganizacion = $('#txtClaveOrg').val();
	var claveConsecutivo = $('#txtCConsecutivo').val();
	var claveServicio = $('#txtCServicio').val();
	var idNegocio = $('#id_idNegocio').val();

	$.post("index.php?c=negocios&a=verificarConsecutivo", {claveOrganizacion: claveOrganizacion, claveConsecutivo: claveConsecutivo, claveServicio: claveServicio, idNegocio: idNegocio}, function(respuesta) {
		console.log(respuesta);
		if (respuesta == 0){
			$( "#btnGuardarCClave" ).prop("disabled", false);
		}else if (respuesta == 1){
			$('#alerta').show();
			$("#alerta").html('<p style="color: #dd4b39; font-size: 13px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este consecutivo ya existe </p>');
			$("#btnGuardarCClave").prop("disabled", true);
		}
	})
});

function myFunctionEditar(idNegocio, idEtapa, idOrganizacion, idCliente, idEmbudo, idEquipo, claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda, fechaCierre, ponderacion,xCambio,nombreOrganizacion,nombrePersona, nombreEquipo, nombreEtapa, estimacion, idUnidadNegocio, unidadNegocio) {

	$('#divFiltros').hide();
	$('#divPersonas').hide();
	$('#divPersonasF').hide();
	$('#divOrganizacionF').hide();
	$('#divOrganizacion').show();
	$('#txtEstimacion').val(estimacion);

	if(estimacion > 0){
		$('#divPonderacion').hide();
		$('#selectServicios').hide();
		$('#inputServicio').show();
		$('#divSelectTipoMoneda').hide();
		$('#divInputTipoMoneda').show();
	}else{
		$('#divPonderacion').show();
		$('#inputServicio').hide();
		$('#selectServicios').show();
		$('#divSelectTipoMoneda').show();
		$('#divInputTipoMoneda').hide();
	}

	$('#txtIdNegocio').val(idNegocio);
	$('#selectEtapa').val(idEtapa);
	$('#txtNombreEtapa').val(nombreEtapa);
	$('#txtIdNegEst').val("");
	/*Se desactiva el required del validador para elmetodo de editar*/
	$("#selectIdOrganizacion").removeAttr("required");
	$("#selectIdPersona").removeAttr("required");

	if(nombreEtapa == "Leads")
		$('#divClaveNegocio').hide();
	else
		$('#divClaveNegocio').show();

	$("#selectIdOrganizacion").empty(); /*Limpiar el select para despues pintar la consulta*/
	var selectIdOrganizacion = document.getElementById("selectIdOrganizacion");
	selectIdOrganizacion.options[0] = new Option(nombreOrganizacion,idOrganizacion);
	$('#selectIdOrganizacion').selectpicker('refresh');

	/*Estos son los id que se envian en el formulario */
	$('#inputOrganizacion').val(idOrganizacion);
	$('#inputCliente').val(idCliente);

	datos = {"idOrganizacion":idOrganizacion};
	$.ajax({
		url: "index.php?c=negocios&a=listarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#divPersonasF").show();
		$("#selectIdPersonaF").empty();
		var selector = document.getElementById("selectIdPersonaF");
		selector.options[0] = new Option(nombrePersona,idCliente);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idCliente != idCliente){
				j=j+1;
				selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
				$("#txtClaveOrganizacion").val(respuesta[i].clave);
			}
			$('#selectIdPersonaF').selectpicker('refresh');
		}
	});

	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarEquipo",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdEquipo").empty();
		var selectEquipo = document.getElementById("selectIdEquipo"); 
		selectEquipo.options[0] = new Option(nombreEquipo,idEquipo);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idEquipo != idEquipo){
				j=j+1;
				selectEquipo.options[j] = new Option(respuesta[i].nombreEquipo,respuesta[i].idEquipo);
			}
			$('#selectIdEquipo').selectpicker('refresh');
		}
	});

	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarUnidadNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdUnidadNegocio").empty();
		var selectUnidadNegocio = document.getElementById("selectIdUnidadNegocio"); 
		selectUnidadNegocio.options[0] = new Option(unidadNegocio,idUnidadNegocio);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idUnidadNegocio != idUnidadNegocio){
				j=j+1;
				selectUnidadNegocio.options[j] = new Option(respuesta[i].unidadNegocio,respuesta[i].idUnidadNegocio);
			}
			$('#selectIdUnidadNegocio').selectpicker('refresh');
		}
	});

	$('#selectIdPersona').val(idCliente);
	$('#txtIdEmbudo').val(idEmbudo);
	$('#txtClaveOrganizacion').val(claveOrganizacion);
	$('#txtServicio').val(claveServicio);
	$('#servicioEst').val(claveServicio);

	if(claveConsecutivo == "")
		sumaConsecutivo(idOrganizacion);
	else
		$('#txtConsecutivo').val(claveConsecutivo);

	$('#txtTituloNegocio').val(tituloNegocio);
	$('#txtValorNegocio').val(valorNegocio);
	$('#selectTipoMoneda').val(tipoMoneda);

	if(tipoMoneda == "USD")
		$('#inputTipoMoneda').val("US Dollar (USD)");
	else
		$('#inputTipoMoneda').val("Pesos Méxicanos (MXN)");
	
	$('#txtFechaCierre').val(fechaCierre);
	$('#selectPonderacion').val(ponderacion);
	$('#txtXcambio').val(xCambio);
	$('#divPresupuesto').hide();
	$('#labelPresupuesto').hide();
	$('#btnGuardar').val("Actualizar");  
	$('#btnEliminar').show();
	$('#labTitulo').html("Editar Negocio");
	myFunctionCheckedNo();
}

function myFunctionEstimacion(idNegocio, idEtapa, idOrganizacion, idCliente, idEmbudo, idEquipo, claveOrganizacion, claveConsecutivo, claveServicio, tituloNegocio, valorNegocio, tipoMoneda, fechaCierre, ponderacion,xCambio,nombreOrganizacion,nombrePersona, nombreEquipo, nombreEtapa, idUnidadNegocio, unidadNegocio) {

	$('#divFiltros').hide();
	$('#divPersonas').hide();
	$('#divPersonasF').hide();
	$('#divOrganizacionF').hide();
	$('#divOrganizacion').show();
	$('#divPonderacion').hide();

	$('#txtIdNegocio').val("");
	$('#txtIdNegEst').val(idNegocio);
	$('#txtEstimacion').val(1);
	$('#selectEtapa').val(idEtapa);
	$('#txtNombreEtapa').val(nombreEtapa);
	/*Se desactiva el required del validador para elmetodo de editar*/
	$("#selectIdOrganizacion").removeAttr("required");
	$("#selectIdPersona").removeAttr("required");

	if(nombreEtapa == "Leads")
		$('#divClaveNegocio').hide();
	else
		$('#divClaveNegocio').show();

	$("#selectIdOrganizacion").empty(); /*Limpiar el select para despues pintar la consulta*/
	var selectIdOrganizacion = document.getElementById("selectIdOrganizacion");
	selectIdOrganizacion.options[0] = new Option(nombreOrganizacion,idOrganizacion);
	$('#selectIdOrganizacion').selectpicker('refresh');

	/*Estos son los id que se envian en el formulario */
	$('#inputOrganizacion').val(idOrganizacion);
	$('#inputCliente').val(idCliente);

	datos = {"idOrganizacion":idOrganizacion};
	$.ajax({
		url: "index.php?c=negocios&a=listarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#divPersonasF").show(); /*Activamos el select de personas*/
		$("#selectIdPersonaF").empty();
		var selector = document.getElementById("selectIdPersonaF");
		selector.options[0] = new Option(nombrePersona,idCliente);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idCliente != idCliente){
				j=j+1;
				selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
				$("#txtClaveOrganizacion").val(respuesta[i].clave);
			}
			$('#selectIdPersonaF').selectpicker('refresh');
		}
	});

	$.ajax({
		url: "index.php?c=negocios&a=ListarEquipo",
		type: "POST"
	}).done(function(respuesta){
		$("#selectIdEquipo").empty();
		var selectEquipo = document.getElementById("selectIdEquipo"); 
		selectEquipo.options[0] = new Option(nombreEquipo,idEquipo);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idEquipo != idEquipo){
				j=j+1;
				selectEquipo.options[j] = new Option(respuesta[i].nombreEquipo,respuesta[i].idEquipo);
			}
			$('#selectIdEquipo').selectpicker('refresh');
		}
	});

	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarUnidadNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdUnidadNegocio").empty();
		var selectUnidadNegocio = document.getElementById("selectIdUnidadNegocio"); 
		selectUnidadNegocio.options[0] = new Option(unidadNegocio,idUnidadNegocio);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idUnidadNegocio != idUnidadNegocio){
				j=j+1;
				selectUnidadNegocio.options[j] = new Option(respuesta[i].unidadNegocio,respuesta[i].idUnidadNegocio);
			}
			$('#selectIdUnidadNegocio').selectpicker('refresh');
		}
	});

	$('#selectIdPersona').val(idCliente);
	$('#txtIdEmbudo').val(idEmbudo);
	$('#txtClaveOrganizacion').val(claveOrganizacion);
	$('#selectServicios').hide();
	$('#inputServicio').show();
	$('#txtServicio').val(claveServicio);
	$('#servicioEst').val(claveServicio);
	$('#txtConsecutivo').val(claveConsecutivo);
	$('#txtTituloNegocio').val(tituloNegocio);
	$('#txtValorNegocio').val("");
	$('#divSelectTipoMoneda').hide();
	$('#divInputTipoMoneda').show();
	$('#selectTipoMoneda').val(tipoMoneda);
	if(tipoMoneda == "USD")
		$('#inputTipoMoneda').val("US Dollar (USD)");
	else
		$('#inputTipoMoneda').val("Pesos Méxicanos (MXN)");
	$('#txtFechaCierre').val("");
	$('#selectPonderacion').val(ponderacion);
	$('#txtXcambio').val(xCambio);
	$('#nomOrganizacion').html(nombreOrganizacion)
	$('#divPresupuesto').hide();
	$('#labelPresupuesto').hide();
	$('#btnGuardar').val("Guardar");  
	$('#btnEliminar').hide();
	$('#labTitulo').html("Estimación de Negocio");
	myFunctionCheckedNo();

}

function myFunctionNuevo() {
	$('#divFiltros').show();
	$('#divClaveNegocio').hide();
	$('#porOrganizacion').prop('checked', true);
	$('#porPersona').prop('checked', false);
	$('#txtIdNegocio').val("");
	$('#txtEstimacion').val(0);
	$('#selectEtapa').val("");
	$('#selectIdOrganizacionA').val("");
	$('#selectIdPersona').val("");
	$('#selectIdPersonaA').val("");
	$('#txtClaveNegocio').val("");
	$('#txtTituloNegocio').val("");
	$('#txtValorNegocio').val("");
	$('#selectTipoMoneda').val("MXN");
	$('#inputTipoMoneda').val("");
	$('#divSelectTipoMoneda').show();
	$('#divInputTipoMoneda').hide();
	$('#txtFechaCierre').val("");
	$('#txtContadorActivo').val("");
	$('#txtStatus').val("");
	$('#selectPonderacion').val("");
	$('#txtXcambio').val("");
	$('#txtServicio').val("");
	$('#txtIdNegEst').val("");

	$('#selectServicios').show();
	$('#inputServicio').hide();
	$('#servicioEst').val("");

	$('#divPresupuesto').show();
	$('#labelPresupuesto').show();
	$('#divPonderacion').show();
	$('#btnGuardar').val("Guardar");  
	$('#btnEliminar').hide();
	$('#labTitulo').html("Añadir Negocio");
	//listarPersonas();
	//listarOrganizaciones();
	filtrarPorOrganizaciones();
	listarEquipos();
	listarPresupuestos();
	myFunctionCheckedNo();
	listarUnidadNegocio();
}

/*Este metodo se ejecuta al cambiar el fitro del modal de negocios (radio)*/
filtrarPorOrganizaciones = function()
{
	listarOrganizaciones(); /*Listar las organizaciones al marcar el radio button*/
	$("#txtClaveOrganizacion").val("");
	$("#inputOrganizacion").val("");
	$("#inputCliente").val("");
	$("#txtConsecutivo").val("0000");
	$('#porOrganizacion').prop('checked', true);
	$('#porPersona').prop('checked', false);
	$("#divPersonas").hide();
	$("#divPersonasF").hide();
	$("#divOrganizacionF").hide();
	$("#divOrganizacion").show();
	$("#selectIdOrganizacion").prop('required', true);
	$("#selectIdPersonaF").prop('required', true);
	$("#selectIdPersona").removeAttr("required");
	$("#selectIdOrganizacionF").removeAttr("required");
}

/*Este metodo se ejecuta al cambiar el fitro del modal de negocios (radio)*/
filtrarPorPersonas = function()
{
	listarPersonas(); /*Listar las personas al marcar el radio button*/
	$("#txtClaveOrganizacion").val("");
	$("#inputOrganizacion").val("");
	$("#inputCliente").val("");
	$("#txtConsecutivo").val("0000");
	$('#porOrganizacion').prop('checked', false);
	$('#porPersona').prop('checked', true);
	$("#divOrganizacion").hide();
	$("#divOrganizacionF").hide();
	$("#divPersonasF").hide();
	$("#divPersonas").show();
	$("#selectIdPersona").prop('required', true);
	$("#selectIdOrganizacionF").prop('required', true);
	$("#selectIdOrganizacion").removeAttr("required");
	$("#selectIdPersonaF").removeAttr("required");
}

function myFunctionEliminar(){
	var idNegocio = $('#txtIdNegocio').val();
	$('#txtIdNegocioE').val(idNegocio);
	$('#labTituloE').html("Eliminar Negocio");  
}

function tipoFiltro(){
	var num = $('#selectFiltro').val();
	$('#filtros').val(num);
	paginador(1);
	Contadores();
}

function mesAño(){
	paginador(paginaActual);
}

NegociosPorEmbudo = function (numRegistros, busqueda){
	var idEmbudo = $('#selectIdEmbudo').val();
	var busqueda=$("#buscar").val();
	var filtro = $('#filtros').val();
	var Año = $('#selectAño').val();
	var Mes = $('#selectMes').val();

	$('#txtBusIdEmbudo').val(idEmbudo);
	$('#txtFiltro').val(filtro);

	var nombre = $('#selectIdEmbudo option:selected').html();
	/*Vista lista*/
	$.post("index.php?c=negocios&a=NegociosPorEmbudo", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro, registros: numRegistros, pagina: paginaActual}, function(mensaje) {
		$("#resultBusqueda").html(mensaje);
	});
	/*Vista embudo*/
	$.post("index.php?c=negocios&a=Consultas", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro }, function(mensaje) {
		$("#resultBusquedaEmbudo").html(mensaje);
	});
	/*Vista pronostico*/
	$.post("index.php?c=negocios&a=ConsultasPronostico", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro, Año: Año, Mes: Mes }, function(mensaje) {
		$("#ReslutadoPro").html(mensaje);
	});

	$.post("index.php?c=negocios&a=ConsultaEtapaPorEmbudo", {valorIdEmbudo: idEmbudo }, function(mensaje) {
		$("#ConsultaEtapa").html(mensaje);
	});
}

function paginador(pagina) {
	paginaActual = pagina;
	var numRegistros = $('#selectPorPagina').val();
	var busqueda = $("#buscar").val();
	NegociosPorEmbudo(numRegistros, busqueda);
	switchColor(numRegistros); 
}

function switchColor(numRegistros){
	$.post("index.php?c=negocios&a=obtenerRegistros", {}, function(totalRegistros) {
		var totalPaginas = Math.ceil(totalRegistros / numRegistros);
		for (var i = 1; i <= totalPaginas; i++) {
			if (i == paginaActual) {
				$("#li"+i).addClass("active");
			}else{
				$("#li"+i).removeClass("active");
			}
		}
	}); 
}

function Contadores(){
	var idEmbudo = $('#selectIdEmbudo').val();
	var busqueda=$("#buscar").val();
	var filtro = $('#filtros').val();
	$.post("index.php?c=negocios&a=ContadoresPorEmbudo", {idEmbudo: idEmbudo, valor: busqueda, filtrostatus: filtro }, function(mensaje) {
		$("#resultadoContadorNegocios").html(mensaje);
	});
}

function cambiarClaveNegocio(idNegocio, tituloNegocio, idOrganizacion, claveConsecutivo, claveServicio) {	
	$('#txtNombreNegocio').html(tituloNegocio);
	$('#id_idNegocio').val(idNegocio);
	$('#alerta').hide();
	if(claveConsecutivo == "")
		sumaConsecutivo(idOrganizacion);
	else
		$('#txtCConsecutivo').val(claveConsecutivo);
	if(claveServicio == "")
		$('#txtCServicio').val('');	
	else
		$('#txtCServicio').val(claveServicio);
	ConsultaClave(idOrganizacion);
}


function listarEmbudos(){
	var idEmbudo = $('#selectIdEmbudo').val();
	var nombre = $('#selectIdEmbudo option:selected').html();

	var selector = document.getElementById("selectIdEmbudo");
	selector.options[0] = new Option(nombre, idEmbudo);
	$.ajax({
		url: "index.php?c=negocios&a=ListarEmbudos",
	}).done(function(respuesta){
		var j=0;
		for (var i in respuesta) {
			if(idEmbudo != respuesta[i].idEmbudo){
				var j=j+1;
				selector.options[j] = new Option(respuesta[i].nombre,respuesta[i].idEmbudo);
			}
		}
		$('#selectIdEmbudo').selectpicker('refresh');
	});
	paginador(paginaActual);
	Contadores();
}

function listarOrganizaciones(){
	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarOrganizaciones",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdOrganizacion").empty();
		var selector = document.getElementById("selectIdOrganizacion");
		selector.options[0] = new Option("Seleccione el cliente","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
		}
		$('#selectIdOrganizacion').selectpicker('refresh');
	}); 
}

function listarPersonas(){
	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=listarPersonas",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdPersona").empty();
		var selector = document.getElementById("selectIdPersona");
		selector.options[0] = new Option("Seleccione la persona de contacto","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectIdPersona').selectpicker('refresh');
	});
}

function listarPresupuestos(){
	var idEmbudo = $('#selectIdEmbudo').val();
	datos = {"idEmbudo":idEmbudo};
	$.ajax({
		url: "index.php?c=negocios&a=listarPresupuestos",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectPresupuesto").empty();
		var selector = document.getElementById("selectPresupuesto");
		selector.options[0] = new Option("Seleccione presupuesto","");								
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePresupuesto,respuesta[i].idPresupuestoGeneral);
		}
		$('#selectPresupuesto').selectpicker('refresh');
	});
}

listarOrganizacionesPorPersona = function (idCliente){
	var idCliente = $('#selectIdPersona').val();
	$('#inputCliente').val(idCliente);
	datos = {"idCliente":idCliente};
	$.ajax({
		url: "index.php?c=negocios&a=listarOrganizacionesPorPersona",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		/*console.log(JSON.stringify(respuesta));*/
		$("#divOrganizacionF").show();
		$("#selectIdOrganizacionF").empty();
		var selector = document.getElementById("selectIdOrganizacionF");
		for (var i in respuesta) {
			selector.options[0] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
			$("#txtClaveOrganizacion").val(respuesta[i].clave);
			$("#inputOrganizacion").val(respuesta[i].idOrganizacion);
			ConsultaClave(respuesta[i].idOrganizacion);
			sumaConsecutivo(respuesta[i].idOrganizacion);
		}
		$('#selectIdOrganizacionF').selectpicker('refresh');
	}); 
}

listarPersonasPorOrganizacion = function (idOrganizacion){
	var idOrganizacion = $('#selectIdOrganizacion').val();
	$('#inputOrganizacion').val(idOrganizacion);
	datos = {"idOrganizacion":idOrganizacion};
	$.ajax({
		url: "index.php?c=negocios&a=listarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#divPersonasF").show(); /*Activamos el select de personas*/
		$("#selectIdPersonaF").empty();
		var selector = document.getElementById("selectIdPersonaF");
		selector.options[0] = new Option("Seleccione la persona de contacto","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectIdPersonaF').selectpicker('refresh');
	});
	ConsultaClave(idOrganizacion);
	sumaConsecutivo(idOrganizacion);
}

/*Metodo para consultar la clave de organizacion segun el id seleccionado y imprimirlo en la caja de texto */
function ConsultaClave(idOrganizacion){
	$.post("index.php?c=negocios&a=ConsultaClaveOrganizacion", { valorIdOrganizacion: idOrganizacion }, function(cOrganizacion) {
		$("#txtClaveOrganizacion").val(cOrganizacion);
		$("#txtClaveOrg").val(cOrganizacion);
	});

}
/*Metodo para traer el consecutivo segun la clave de la organizacion y imprimirlo en la caja de texto*/
function sumaConsecutivo(idOrganizacion){
	$.post("index.php?c=negocios&a=ConsultaConsecutivo", { valorIdOrganizacion: idOrganizacion }, function(numConsecutivo) {
		$("#txtConsecutivo").val(numConsecutivo);
		$("#txtCConsecutivo").val(numConsecutivo);
	});
}

obtenerIdPersona = function (idPersona){
	var idPersona = $('#selectIdPersonaF').val();
	$('#inputCliente').val(idPersona);
}

function listarUnidadNegocio(){
	$.ajax({
		url: "index.php?c=negocios&a=ListarUnidadNegocio",
		type: "POST",
	}).done(function(respuesta){
		console.log(respuesta);
		$("#selectIdUnidadNegocio").empty();
		var selectUnidadNegocio = document.getElementById("selectIdUnidadNegocio");
		selectUnidadNegocio.options[0] = new Option("Seleccione la unidad de negocio","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selectUnidadNegocio.options[j] = new Option(respuesta[i].unidadNegocio,respuesta[i].idUnidadNegocio);
		}
		$('#selectIdUnidadNegocio').selectpicker('refresh');
	}); 
}

function listarEquipos(){
	datos = {};
	$.ajax({
		url: "index.php?c=negocios&a=ListarEquipo",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdEquipo").empty();
		var selectEquipo = document.getElementById("selectIdEquipo");
		selectEquipo.options[0] = new Option("Seleccione el equipo","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selectEquipo.options[j] = new Option(respuesta[i].nombreEquipo,respuesta[i].idEquipo);
		}
		$('#selectIdEquipo').selectpicker('refresh');
	}); 
}

function myFunctionCheckedSi(){
	$("#ActivarPresupuesto").show();
	$('#Si').prop('checked', true);
}

function myFunctionCheckedNo(){
	$("#ActivarPresupuesto").hide();
	$('#No').prop('checked', true);
	$('#txtNotas').val("");
}

function leads(){
	var etapa = $('#selectEtapa option:selected').html();
	if(etapa == "Leads"){
		$('#divClaveNegocio').hide();
	}else{
		$('#divClaveNegocio').show();
	}
	$('#txtNombreEtapa').val(etapa);
}
</script>