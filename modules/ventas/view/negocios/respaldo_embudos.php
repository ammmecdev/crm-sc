<style>
.embudos{
	display: inline-flex;
	flex-direction: row;
	justify-content: space-around;
	align-items: center;
	align-content: center;
	white-space: nowrap;
}
.horizontal{
	overflow: auto;
	overflow-x: scroll;
	overflow-y: hidden;
	background: #FAFAFA;
}

.caja{
	border:inherit 0px #000000;
	border-top-left-radius: 0px;
	border-top-right-radius: 75px;
	border-bottom-left-radius:0px;
	border-bottom-right-radius:75px;
	-moz-border-radius-top-left: 0px;
	-moz-border-radius-top-right:75px;
	-moz-border-radius-bottom-left:0px;
	-moz-border-radius-bottom-right:75px;
	-webkit-border-top-left-radius: 0px;
	-webkit-border-top-right-radius:75px;
	-webkit-border-bottom-left-radius:0px;
	-webkit-border-bottom-right-radius:75px;
	background:-webkit-gradient(linear, 25% 0%, 10% 91%, from(#EEEEEE), to(#E0E0E0));
	-moz-box-shadow: 4px 3px 5px #9E9E9E;
	-webkit-box-shadow: 4px 3px 5px #9E9E9E;
	box-shadow: 4px 3px 5px #9E9E9E;
	margin: 5px;
	padding-right: 60px;
	height: 85px;*/
}
.list-group-item:first-child {
	border-top-right-radius: 75px;
	border-top-left-radius: 0px;
}

.list-group-item:last-child{
	border-bottom-right-radius: 75px;
	border-bottom-left-radius: 0px;*/
}
</style>
<!--Encabezado del contenido-->
<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
				<h3 style="margin-top: 16px;">Personalizar etapas de venta</h3>
			</div>
			<div class="col-xs-2 col-sm-3 col-md-3 col-lg-3" align="right" style="margin-top: 16px;">
				<a href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionNuevo()">Añadir embudo nuevo</a>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->

<!--Contenido-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="row" id="tabsEmbudo">
		<div id="mActualizar"></div>
		<br>
		<ul class="nav nav-tabs nav-justified">
			<?php $var = 0; ?><?php foreach ($this->modelEmbudo->Listar() as $embudo): ?><?php $ContadorEtapas = $this->modelEmbudo->ContadorEtapas( "'".$embudo->idEmbudo."'" ); ?><?php if ($var == 0){ ?>
			<li role="presentation" class="active"><a data-toggle="tab" href="#<?php echo $embudo->idEmbudo; ?>"><?php echo $embudo->nombre; ?> <span class="badge"><?php echo ($ContadorEtapas); ?></span></a></li>
			<?php $var = $var + 1; }else { ?>
			<li role="presentation"><a data-toggle="tab" href="#<?php echo $embudo->idEmbudo; ?>"><?php echo $embudo->nombre; ?> <span class="badge"><?php echo ($ContadorEtapas); ?></span></a></li><?php } ?><?php endforeach; ?>
		</ul>
		<div class="tab-content">
			<!-- Inicio de tab 1 -->
			<?php $var = 1; ?>
			<?php foreach ($this->modelEmbudo->Listar() as $e): ?>
				<?php if ($var == 1){ ?>
				<div id="<?php echo $e->idEmbudo; ?>" class="tab-pane fade in active">
					<?php $var = $var + 1; }else { ?>
					<div id="<?php echo $e->idEmbudo; ?>" class="tab-pane fade">
						<?php } ?>
						<div class="container-fluid" style="background-color: #FFF;">
							<div class="col-xs-7 col-lg-10">
								<div class="col-xs-12 col-lg-6">
									<h3><?php echo $e->nombre; ?></h3>	
								</div>
								<div class="col-lg-4">
									<br>
									<div class="btn-group">
										<input type="hidden" name="hideIdEmbudo" id="hideIdEmbudo" value="<?php echo $e->idEmbudo; ?>">
										<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionEditar(<?php echo "'".$e->idEmbudo."'"; ?>,<?php echo "'".$e->nombre."'"; ?>)">Editar</a>
									</div>
									<div class="btn-group">
										<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mEliminar" onclick="myFunctionEliminar(<?php echo "'".$e->idEmbudo."'"; ?>)" >Eliminar</a>
									</div>
								</div>
							</div>
							<div class="col-xs-5 col-lg-2" align="right">
								<br>
								<a href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionNuevoEtapa(<?php echo "'".$e->idEmbudo."'"; ?>);">Añadir etapa</a>
							</div>
						</div>
						<div class="jumbotron horizontal">
							<!--Mostrar etapas de ventas creadas-->
							<ul class="sortable">
								<?php foreach ($this->modelEmbudo->ListarEtapas("'".$e->idEmbudo."'") as $r): ?>
									<li class="list-group-item caja" id="<?php echo $r->nombreEtapa; ?>-<?php echo $r->idEtapa; ?>">
										<h5><a href="" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionEditarEtapa(<?php echo "'".$e->idEmbudo."'"; ?>,<?php echo "'".$r->idEtapa."'"; ?>,<?php echo "'".$r->nombreEtapa."'"; ?>,<?php echo "'".$r->probabilidad."'"; ?>,<?php echo "'".$r->inactividad."'"; ?>);"><?php echo $r->nombreEtapa; ?></a></h5>
										<?php 
										if ($r->inactividad == 1){ ?>
										<font size="1" class="help-block"> <?php echo "".$r->probabilidad."" ?> % de probabilidad <br> Estancado en <?php echo $r->inactividad; ?> día </font>
										<?php } else if($r->inactividad > 1){ ?>
										<font size="1" class="help-block"> <?php echo "".$r->probabilidad."" ?> % de probabilidad <br> Estancado en <?php echo $r->inactividad; ?> días </font>
										<?php }else{ ?>
										<font size="1" class="help-block"> <?php echo "".$r->probabilidad."" ?> % de probabilidad </font>
										<?php } ?>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				<?php endforeach; ?>
				<!-- Fin del tab 1 -->
			</div>
		</div>
	</div>
</div>
<!--Contenido-->

<!--Inicio de Modal añadir embudo-->
<div class="modal fade" id="mEmbudo" role="dialog">
	<div class="modal-dialog">
		<form  method="post" id="form-embudo">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo"></label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">  
					<div class="form-group">
						<input type="hidden" name="idEmbudo" id="txtIdEmbudo">
						<input type="hidden" name="idEtapa" id="txtIdEtapa">

						<div class="form-group" id="nombreEmbudo">
							<h5>Titulo del Embudo</h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-filter"></span></span>
								<input type="text" class="form-control" name="nombre" id="txtNombre" placeholder="Título del embudo" required>
							</div>
						</div>

						<div class="form-group" id="divEtapa">
							<h5>Nombre de la etapa</h5>
							<div class="input-group">
								<span class="input-group-addon"><span class="glyphicon glyphicon-flag"></span></span>
								<input type="text" class="form-control" id="txtNombreEtapa">
							</div>
						</div>

						<div class="row" id="labProNegocio">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h5>Probabilidad del negocio</h5>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group" id="proNegocio">
								<div class="col-xs-5 col-md-4" style="padding-right: 0px">
									<div class="input-group">
										<div class="input-group-addon">
											<strong>%</strong>
										</div>
										<input type="text" class="form-control" maxlength="3" id="txtProbabilidad">
									</div>
								</div>
								<div class="col-xs-6 col-sm-6" style="padding-left: 5px ">
									<p class="help-block">% (0...100)</p>
								</div>                
							</div>
						</div>

						<div class="form-group" id="radioEstancandose">
							<h5>Negocio "estancándose"</h5>
							<div class="col-md-3">
								<div class="radio">
									<label for="si">
										<input type="radio" name="optradio" onclick="myFunctionChecked('si', 'si')" id="txtCheckedSi">
										Sí
									</label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="radio">
									<label for="no">
										<input type="radio" name="optradio" onclick="myFunctionChecked('no', 'no')" id="txtCheckedNo">
										No
									</label>
								</div>
							</div>
						</div>

						<div class="row" id="labDiasInactividad">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<h5>Mostrar "estancado" después de</h5>
								</div>
							</div>
						</div>

						<div class="row" id="diasInactividad">
							<div class="form-group" id="">
								<div class="col-xs-5 col-md-3" style="padding-right: 0px">
									<input type="text" class="form-control" maxlength="3" name="inactividad" id="txtInactividad">
								</div>
								<div class="col-xs-6 col-sm-6" style="padding-left: 5px ">
									<p class="help-block">días de inactividad</p>
								</div>                
							</div>
						</div>
						<!--Fin modal añadir etapa-->
						<input type="hidden" name="idUsuario" id="txtIdUsuario" value="1">
					</div>
				</div>

				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-lg-3" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar etapa" id="btnEliminar" onclick="myFunctionEliminarEtapa()"><span class="glyphicon glyphicon-trash"></span></a>
						</div>
						<div class="col-lg-9" align="right" style="padding-right: 0px">
							<input type="button" class="btn btn-success" id="btnGuardarEmbudo" onclick="myFunctionAccionNuevo()" value="">
							<input type="button" class="btn btn-success" id="btnEditarEmbudo" onclick="myFunctionAccionEditar()" value="">
							<input type="button" class="btn btn-success" id="btnGuardarEtapa" onclick="myFunctionAccionNuevoEtapa()" value="">
							<input type="button" class="btn btn-success" id="btnEditarEtapa" onclick="myFunctionAccionEditarEtapa()" value="">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
						</div> 
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de añadir embudo-->

<!--Inicio modal de eliminar embudo -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Embudo</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar el embudo?</p>
					<input type="hidden" id="txtIdEmbudoE" name="idEmbudo">
					<input type="hidden" id="txtIdEtapaE" name="idEtapa">
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="button" class="btn btn-danger" value="Eliminar" id="btnEliminarEtapa" onclick="myFunctionAccionEliminarEtapa()">
						<input type="button" class="btn btn-danger" value="Eliminar" id="btnEliminarEmbudo" onclick="myFunctionAccionEliminarEmbudo()">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de eliminar embudo -->
</div>
<script>
	function myFunctionNuevo() {
		$('#txtIdEmbudo').val("");
		$('#txtNombre').val("");
		$('#nombreEmbudo').show();
		$('#radioEstancandose').hide();
		$('#proNegocio').hide();
		$('#labProNegocio').hide();
		$('#divEtapa').hide();
		$('#btnEliminar').hide();
		$('#diasInactividad').hide();
		$('#labDiasInactividad').hide();
		$('#btnGuardarEmbudo').show();
		$('#btnEditarEmbudo').hide();
		$('#btnGuardarEtapa').hide();
		$('#btnEditarEtapa').hide();
		$('#btnGuardarEmbudo').val("Guardar");
		$('#labTitulo').html("Añadir Embudo");
	}
	function myFunctionAccionNuevo(){
		var idEmbudo = $('#txtIdEmbudo').val();
		var hideIdEmbudo = $('#hideIdEmbudo').val();
		var nombreEmbudo = $('#txtNombre').val();
		if (nombreEmbudo != "") {

			datos = {"nombreEmbudo":nombreEmbudo};
			$.ajax({
				url: "index.php?c=negocios&a=RegistrarEmbudo",
				type: "POST",
				data: datos
			}).done(function(respuestaEmbudo){
       //console.log(JSON.stringify(respuestaEmbudo));
       if (!isNaN(respuestaEmbudo)) {
       	$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>El registro de embudo se realizó correctamente</center></strong></div>');

       	$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: respuestaEmbudo}, function(mensaje){
       		$("#tabsEmbudo").html(mensaje);
       	});

       	$('#mensajejs').show();
       	$('#mensajejs').delay(3500).hide(600); 

       }else{
       	$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuestaEmbudo+'</center></strong></div>');

       	$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: hideIdEmbudo}, function(mensaje) {
       		$("#tabsEmbudo").html(mensaje);
       	});

       	$('#mensajejs').show();
       	$('#mensajejs').delay(3500).hide(600);

       }
   });
			$('#mEmbudo').modal('toggle');
		}else{
			alert("Ingrese el nombre del embudo"); 
		}
	}

	function myFunctionNuevoEtapa($idEmbudo) {
		$('#txtIdEmbudo').val($idEmbudo);
		$('#txtIdEtapa').val("");
		$('#txtNombreEtapa').val("");
		$('#txtProbabilidad').val("");
		$('#nombreEmbudo').hide();
		$('#radioEstancandose').show();
		$('#proNegocio').show();
		$('#labProNegocio').show();
		$('#divEtapa').show();
		$('#btnEliminar').hide();
		$('#btnGuardarEmbudo').hide();
		$('#btnEditarEmbudo').hide();
		$('#btnGuardarEtapa').show();
		$('#btnEditarEtapa').hide();
		$('#btnGuardarEtapa').val("Guardar");
		$('#labTitulo').html("Añadir Etapa");
		myFunctionChecked('no'); 
	}

	function myFunctionAccionNuevoEtapa() {
		var idEmbudo = $('#txtIdEmbudo').val();
		var nombreEtapa = $('#txtNombreEtapa').val();
		var probabilidad = $('#txtProbabilidad').val();
		var inactividad = $('#txtInactividad').val();

		datos = {"idEmbudo":idEmbudo, "nombreEtapa":nombreEtapa, "probabilidad":probabilidad, "inactividad":inactividad};
		$.ajax({
			url: "index.php?c=negocios&a=RegistrarEtapa",
			type: "POST",
			data: datos
		}).done(function(respuestaEtapa){
       //console.log(JSON.stringify(respuestaEmbudo));

       $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuestaEtapa+'</center></strong></div>');

       $.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje){
       	$("#tabsEmbudo").html(mensaje);
       }); 

       $('#mensajejs').show();
       $('#mensajejs').delay(3500).hide(600);

   });
		$('#mEmbudo').modal('toggle');
	}

	function myFunctionEditar($idEmbudo, $nombre) {
		$('#txtIdEmbudo').val($idEmbudo);
		$('#txtNombre').val($nombre);
		$('#nombreEmbudo').show();  
		$('#radioEstancandose').hide();
		$('#proNegocio').hide();
		$('#labProNegocio').hide();
		$('#divEtapa').hide();
		$('#btnEliminar').hide();
		$('#diasInactividad').hide();
		$('#labDiasInactividad').hide(); 
		$('#btnGuardarEmbudo').hide();
		$('#btnEditarEmbudo').show();
		$('#btnGuardarEtapa').hide();
		$('#btnEditarEtapa').hide();
		$('#btnEditarEmbudo').val("Actualizar");
		$('#labTitulo').html("Editar Embudo");
	}

	function myFunctionAccionEditar(){
		var idEmbudo = $('#txtIdEmbudo').val();
		var nombreEmbudo = $('#txtNombre').val();

		datos = {"idEmbudo":idEmbudo, "nombreEmbudo":nombreEmbudo};
		$.ajax({
			url: "index.php?c=negocios&a=ActualizarEmbudo",
			type: "POST",
			data: datos
		}).done(function(respuestaEditarEmbudo){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuestaEditarEmbudo+'</center></strong></div>');

			$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje) {
				$("#tabsEmbudo").html(mensaje);
			}); 

			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);

		});
		$('#mEmbudo').modal('toggle');
	}

	function myFunctionEditarEtapa($idEmbudo, $idEtapa, $nombreEtapa, $probabilidad, $inactividad) {
		$('#txtIdEmbudo').val($idEmbudo);
		$('#txtIdEtapa').val($idEtapa);
		$('#txtNombreEtapa').val($nombreEtapa);
		$('#txtProbabilidad').val($probabilidad);
		$('#txtInactividad').prop('value', '0');
		$('#nombreEmbudo').hide();
		$('#radioEstancandose').show();
		$('#proNegocio').show();
		$('#labProNegocio').show();
		$('#divEtapa').show();
		$('#btnEliminar').show();
		$('#btnGuardarEmbudo').hide();
		$('#btnEditarEmbudo').hide();
		$('#btnGuardarEtapa').hide();
		$('#btnEditarEtapa').show();
		$('#btnEditarEtapa').val("Actualizar");
		$('#labTitulo').html("Editar Etapa");
		myFunctionChecked('si', $inactividad);
	}

	function myFunctionAccionEditarEtapa() {
		var idEtapa = $('#txtIdEtapa').val();
		var idEmbudo = $('#txtIdEmbudo').val();
		var nombreEtapa = $('#txtNombreEtapa').val();
		var probabilidad = $('#txtProbabilidad').val();
		var inactividad = $('#txtInactividad').val();

		datos = {"idEtapa":idEtapa, "nombreEtapa":nombreEtapa, "probabilidad":probabilidad, "inactividad":inactividad};
		$.ajax({
			url: "index.php?c=negocios&a=ActualizarEtapa",
			type: "POST",
			data: datos
		}).done(function(respuestaEtapa){
       //console.log(JSON.stringify(respuestaEmbudo));
       $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuestaEtapa+'</center></strong></div>');

       $.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje){
       	$("#tabsEmbudo").html(mensaje);
       }); 
       $('#mensajejs').show();
       $('#mensajejs').delay(3500).hide(600);
   });
		$('#mEmbudo').modal('toggle');
	}

	function myFunctionEliminar($idEmbudo){
		$('#txtIdEmbudoE').val($idEmbudo);  
		$('#btnEliminarEmbudo').show();
		$('#btnEliminarEtapa').hide();
	}
	function myFunctionAccionEliminarEmbudo(){
		var idEmbudo = $('#txtIdEmbudoE').val();

		datos = {"idEmbudo":idEmbudo};
		$.ajax({
			url: "index.php?c=negocios&a=EliminarEmbudo",
			type: "POST",
			data: datos
		}).done(function(respuestaEmbudo){
			if (!isNaN(respuestaEmbudo)) {
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha eliminado correctamente el embudo </center></strong></div>');

				$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: respuestaEmbudo}, function(mensaje){
					$("#tabsEmbudo").html(mensaje);
				}); 

				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);

			}else{
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuestaEmbudo+'</center></strong></div>');

				$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje){
					$("#tabsEmbudo").html(mensaje);
				});

				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
			}
		});
		$('#mEliminar').modal('toggle');
	}


	function myFunctionEliminarEtapa(){
		var idEtapa = $('#txtIdEtapa').val();
		var idEmbudo = $('#txtIdEmbudo').val();
		$('#txtIdEmbudoE').val(idEmbudo);
		$('#txtIdEtapaE').val(idEtapa); 
		$('#mEmbudo').modal('toggle');
		$('#btnEliminarEmbudo').hide();
		$('#btnEliminarEtapa').show();
	}

	function myFunctionAccionEliminarEtapa(){
		var idEtapa = $('#txtIdEtapaE').val();
		var idEmbudo = $('#txtIdEmbudoE').val();

		datos = {"idEtapa":idEtapa};
		$.ajax({
			url: "index.php?c=negocios&a=EliminarEtapa",
			type: "POST",
			data: datos
		}).done(function(respuestaEtapa){
       //console.log(JSON.stringify(respuestaEmbudo));
       $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuestaEtapa+'</center></strong></div>');

       $.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje){
       	$("#tabsEmbudo").html(mensaje);
       }); 

       $('#mensajejs').show();
       $('#mensajejs').delay(3500).hide(600);

   });
		$('#mEliminar').modal('toggle');
	}


	function myFunctionChecked($checked, $inactividad){
		if($checked == "si" && $inactividad > 0){
			$('#txtCheckedSi').prop('checked', true);
			$('#txtCheckedNo').prop('checked', false);
			$('#txtInactividad').prop('value', $inactividad);
			$('#labDiasInactividad').show();
			$('#diasInactividad').show();
		}else if ($checked == "si" && $inactividad == 0){
			$('#txtCheckedNo').prop('checked', true);
			$('#txtCheckedSi').prop('checked', false);
			$('#txtInactividad').prop('value', '0');
			$('#labDiasInactividad').hide();
			$('#diasInactividad').hide(); 
		} else if ($checked == "si" && $inactividad == 'si'){
			$('#txtCheckedSi').prop('checked', true);
			$('#txtCheckedNo').prop('checked', false);
			$('#txtInactividad').prop('value', '');
			$('#labDiasInactividad').show();
			$('#diasInactividad').show();
		}else{
			$('#txtCheckedNo').prop('checked', true);
			$('#txtCheckedSi').prop('checked', false);
			$('#txtInactividad').prop('value', '0');
			$('#labDiasInactividad').hide();
			$('#diasInactividad').hide(); 
		} 
	} 
	$(function() {
		$( ".sortable" ).sortable({
			update: function(event, ui) {
				$(".ajax-loader").show();
				var orden = $(this).sortable('toArray').toString();
				$.ajax({
					url: "index.php?c=negocios&a=Reordenar",
					type: "POST",
					data: {"data": orden}
				}).done(function(data) {
              //console.log(JSON.stringify(data));
          });
				$(".ajax-loader").hide();
			}
		});
		$( ".sortable" ).disableSelection();
	});
</script>