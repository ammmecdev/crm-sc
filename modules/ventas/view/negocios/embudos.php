<style>
	.horizontal{
		overflow: auto;
		overflow-x: scroll;
		overflow-y: hidden;
		background: #FAFAFA;
	}
	.menu {

	}
	.menu li {
		display: inline-block;
	}
	.caja{
		border:inherit 0px #000000;
		border-top-left-radius: 0px;
		border-top-right-radius: 75px;
		border-bottom-left-radius:0px;
		border-bottom-right-radius:75px;
		-moz-border-radius-top-left: 0px;
		-moz-border-radius-top-right:75px;
		-moz-border-radius-bottom-left:0px;
		-moz-border-radius-bottom-right:75px;
		-webkit-border-top-left-radius: 0px;
		-webkit-border-top-right-radius:75px;
		-webkit-border-bottom-left-radius:0px;
		-webkit-border-bottom-right-radius:75px;
		background:-webkit-gradient(linear, 25% 0%, 10% 91%, from(#EEEEEE), to(#E0E0E0));
		-moz-box-shadow: 4px 3px 5px #9E9E9E;
		-webkit-box-shadow: 4px 3px 5px #9E9E9E;
		box-shadow: 4px 3px 5px #9E9E9E;
		margin: 5px;
		padding-right: 60px;
	/*	height: 85px;*/
	}
	.list-group-item:first-child {
		border-top-right-radius: 75px;
		border-top-left-radius: 0px;
	}

	.list-group-item:last-child{
		border-bottom-right-radius: 75px;
		border-bottom-left-radius: 0px;
	}
</style>
<!--Encabezado del contenido-->
<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
				<h3 style="margin-top: 16px;">Personalizar etapas de venta</h3>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->

<!--Contenido-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="right" style="margin-top: 16px;">
	<a href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionNuevo()">Añadir embudo nuevo</a>
</div>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="tabsEmbudo">
	<br>
	<div class="row"> 
		<div class="panel panel-default"> 
			<div class="panel-heading"> 
				<ul class="nav nav-pills nav-justified">
					<?php $var = 0; ?><?php foreach ($this->modelEmbudo->Listar() as $embudo): ?><?php $ContadorEtapas = $this->modelEmbudo->ContadorEtapas( "'".$embudo->idEmbudo."'" ); ?><?php if ($var == 0){ ?>
						<li role="presentation" class="active"><a data-toggle="tab" href="#<?php echo $embudo->idEmbudo; ?>"><?php echo $embudo->nombre; ?> <span class="badge"><?php echo ($ContadorEtapas); ?></span></a></li>
						<?php $var = $var + 1; }else { ?>
							<li role="presentation"><a data-toggle="tab" href="#<?php echo $embudo->idEmbudo; ?>"><?php echo $embudo->nombre; ?> <span class="badge"><?php echo ($ContadorEtapas); ?></span></a></li><?php } ?><?php endforeach; ?>
						</ul>
					</div> 
					<div class="panel-body"> 
						<div class="tab-content"> 
							<!-- Inicio de tab 1 -->
							<?php $var = 1; ?>
							<?php foreach ($this->modelEmbudo->Listar() as $e): ?>
								<?php if ($var == 1){ ?>
									<div id="<?php echo $e->idEmbudo; ?>" class="tab-pane fade in active">
										<?php $var = $var + 1; } else { ?>
											<div id="<?php echo $e->idEmbudo; ?>" class="tab-pane fade"> 
												<?php } ?>
												<div class="container-fluid" style="background-color: #FFF;">
													<div class="col-xs-7 col-lg-10"> 
														<div class="col-xs-12 col-lg-6"> 
															<h3><?php echo $e->nombre; ?></h3>	
														</div> 
														<div class="col-lg-4">
															<br>
															<div class="btn-group"> 
																<input type="hidden" name="hideIdEmbudo" id="hideIdEmbudo" value="<?php echo $e->idEmbudo; ?>">
																<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mEmbudo" onclick="myFunctionEditar(<?php echo "'".$e->idEmbudo."'"; ?>,<?php echo "'".$e->nombre."'"; ?>)">Editar</a>
															</div> 
															<div class="btn-group"> 
																<a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#mEliminar" onclick="myFunctionEliminar(<?php echo "'".$e->idEmbudo."'"; ?>)" >Eliminar</a>
															</div> 
														</div> 
													</div> 
													<div class="col-xs-5 col-lg-2" align="right"> 
														<br>
														<a href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#mEtapa" onclick="myFunctionNuevoEtapa(<?php echo "'".$e->idEmbudo."'"; ?>);">Añadir etapa</a>
													</div> 
												</div> 


												<div class="jumbotron horizontal"> 
													<!--Mostrar etapas de ventas creadas-->
													<ul class="sortable menu">
														<?php foreach ($this->modelEmbudo->ListarEtapas("'".$e->idEmbudo."'") as $r): ?>
															<li class="list-group-item caja" id="<?php echo $r->nombreEtapa; ?>-<?php echo $r->idEtapa; ?>">
																<h5><a href="" data-toggle="modal" data-target="#mEtapa" onclick="myFunctionEditarEtapa(<?php echo "'".$e->idEmbudo."'"; ?>,<?php echo "'".$r->idEtapa."'"; ?>,<?php echo "'".$r->nombreEtapa."'"; ?>,<?php echo "'".$r->probabilidad."'"; ?>,<?php echo "'".$r->inactividad."'"; ?>);"><?php echo $r->nombreEtapa; ?></a></h5>
																<?php 
																if ($r->inactividad == 1){ ?>
																	<font size="1" class="help-block"> <?php echo "".$r->probabilidad."" ?> % de probabilidad <br> Estancado en <?php echo $r->inactividad; ?> día </font>
																	<?php } else if($r->inactividad > 1){ ?>
																		<font size="1" class="help-block"> <?php echo "".$r->probabilidad."" ?> % de probabilidad <br> Estancado en <?php echo $r->inactividad; ?> días </font>
																		<?php }else{ ?>
																			<font size="1" class="help-block"> <?php echo "".$r->probabilidad."" ?> % de probabilidad </font>
																			<?php } ?>
																		</li>
																	<?php endforeach; ?>
																</ul>
															</div> 
														</div> 
													<?php endforeach; ?>
													<!-- Fin del tab 1 -->
												</div> 
											</div> 
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--Contenido-->

						<?php include 'view/negocios/modales/modalesEmbudo.php'; ?>
						<script src="assets/js/embudos.js"></script>
