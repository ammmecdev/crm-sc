<style>

.sortable {
	border: 0px solid;
	list-style-type: none;
	min-height: 24px;
}

.sortable li {
	width: auto;
	list-style-type: none;
}

.placeholder {
	border: 1px;
	margin: 0 1em 1em 0;
	height: 40px;
	background-color: rgb(242, 243, 244);
}
.green {
	color: green;
}
.yellow {
	color: #DAA520;
}
.silver {
	color: #808080;
}
.red {
	color: red;
}

ul,ol{
	margin-top: 0px;
	margin-bottom: 0px;
}

table{
	table-layout: fixed;
}

#miTablaPersonalizada th,td{
	width: 180px;
	overflow: auto;
	min-width: 180px;
}
</style>
<div id="ReslutadoPro"></div>

<!-- Modal -->
<div class="modal fade" id="completarActividad" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div id="ModalR">

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mTerminar" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="myFunctionAccionReset()"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><strong>Fecha de cierre prevista</strong></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">Fecha:</label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
						<input type="date" class="form-control" id="fechaCierre" required>

					</div>
				</div>
			</div>
			<div id="aaaa"></div>
			<div class="modal-footer">
				<input type="button" class="btn btn-default" onclick="myFunctionAccionReset()" value="Cerrar">
				<input type="button" class="btn btn-success" onclick="myFunctionAccionCFC()" value="Guardar">
			</div>
			
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="mTerminarr" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  method="post" data-toggle="validator" action="index.php?c=actividades&a=CambiaEstado" enctype="multipart/form-data" role="form" id="form-cambiaEstado">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="myFunctionAccionReset()"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font id="labActComp">Completar actividad</font></strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="">Fecha:</label>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="date" class="form-control" name="fechaCompletado" required id="txtFechaCompletado">
						</div>
					</div>
				</div>

				<input type="hidden" id="txtEstado" name="completado">
				<input type="hidden" id="txtIdActividad2" name="idActividad">

				<div class="modal-footer">
					<input type="button" class="btn btn-default" value="Cerrar" onclick="myFunctionAccionReset()">
					<input type="submit" class="btn btn-success" value="Guardar">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="añadiractividad" role="dialog">
	<div class="modal-dialog">
		<form method="post" action="index.php?c=actividades&a=Guardar" id="form-actividades">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label>Programar actividad</label> </strong></h4>
				</div>
				<div class="modal-body">  
					<!-- Cuerpo -->
					<div class="form-group">
						<div class="form-group">
							<input type="hidden" class="form-control" value="0" name="idActividad" id="txtIdActividad" readonly>
							<input type="hidden" class="form-control" id="txtConfirmado" name="confirmado" readonly>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">TIPO DE ACTIVIDAD</font>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<div class="btn-group btn-group-sm" role="group" aria-label="...">
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="Llamada" onclick="tipoActividad('Llamada');"><i class="mdi mdi-phone-in-talk mdi-lg"></i></button>
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="Reunión" onclick="tipoActividad('Reunión');"><i class="mdi mdi-group mdi-lg"></i></button>
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="Tarea" onclick="tipoActividad('Tarea');"><i class="far fa-clock" style="font-size: 14px;"></i></button>
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="Plazo" onclick="tipoActividad('Plazo');"><i class="fas fa-hourglass-half" style="font-size: 13px;"></i></button>
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="Correo electrónico" onclick="tipoActividad('Email');"><i class="mdi mdi-email mdi-lg"></i></</button>
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="Comida" onclick="tipoActividad('Comida');"><i class="fas fa-utensils" style="font-size: 13px;"></i></button>
										<button type="button" class="btn btn-default" data-toggle="tooltip" title="WhatsApp" onclick="tipoActividad('WhatsApp');"><i class="fab fa-whatsapp" style="font-size: 15px;"></i></button>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipo" required>
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							<textarea id="txtNotasAct" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas" required></textarea>
							<div class="help-block with-errors"></div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">FECHA</font>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="date" class="form-control" name="fecha" id="txtfechaActividad" required />
								</div>
								<div class="col-lg-12">
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name='checkTiempo' id="checkTiempo"> Asignar hora y duración
								</label>
							</div>
						</div>

						<div id="divTiempo"> 
							<div class="row">
								<div class="form-group">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<font size="1">HORA</font>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<input type="time" class="form-control" name="hora" id="txtHora" onblur="deshabilitaConfirmado()" />
									</div>
									<div class="col-lg-12">
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="form-group">
									<div class="col-lg-12">
										<font size="1">DURACIÓN</font>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<input type="number" class="form-control" name="duracion" id="txtDuracion" onblur="deshabilitaConfirmado()"/>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<select class="form-control" name="tipoDuracion" id="selectTipoDuracion" onchange="deshabilitaConfirmado()">
											<option value="minutos">Minutos</option>
											<option value="horas">Horas</option>
										</select>
									</div>
									<div class="col-lg-12">
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
						</div>


						<div class="form-group" id="selectPersonas">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-tower"></span></span>
								<input class="form-control" id="txtNomOrganizacion" name="nombreOrganizacion" readonly="readonly">
								<input type="hidden" id="txtIdOrganizacion" name="idOrganizacion">
							</div>
						</div>

						<div class="form-group" id="selectPersonas">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
								<input class="form-control"  id="txtnombrePersona" name="nombrePersona" readonly="readonly">
								<input type="hidden" id="txtidCliente" name="idCliente">
							</div>
						</div>

						<div class="form-group" id="selectPersonas">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-briefcase"></span></span>
								<input class="form-control" id="txttituloNegocio" required disabled>
								<input type="hidden" id="txtidNegocio" name="idNegocio">
							</div>
						</div>
						<div class="row" id="divCruzadas" style="margin-bottom: -50px;">
							<div class="form-group">
								<div class="col-lg-12">
									<div class="help-block with-errors" id="cruzadas"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-success" value="Guardar" >
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> 
				</div>
			</div>
			<!--Fin Modal content-->
		</form>
	</div>
</div>

<script>
	$( '#checkTiempo' ).on( 'click', function() {
		if( $(this).is(':checked') ){
			$('#divTiempo').show();
			$('#txtHora').focus();
			$('#divCruzadas').show();
			$('#txtConfirmado').val("false");
		} else {
			$('#divTiempo').hide();
			$('#divCruzadas').html("");
			$('#divCruzadas').hide();
			$('#btnGuardar').val('Guardar');
		}
	});
	tipoActividad = function ($tipo)
	{
		$('#txtTipo').val($tipo);
	} 
	function myFunctionAñadirActividad(idOrganizacion, nombreOrganizacion,idCliente,nombrePersona,idNegocio, tituloNegocio) {
		$('#txtIdActividad').val("");
		$('#txtTipo').val(""); 
		$('#txtfechaActividad').val("");
		$('#txtNotasAct').val("");
		$('#divCruzadas').hide();
		$('#txtHora').val("");
		$('#txtDuracion').val("");
		$('#checkTiempo').prop('checked',false);
		$('#txtIdOrganizacion').val(idOrganizacion); 
		$('#txtNomOrganizacion').val(nombreOrganizacion);  
		$('#txtidCliente').val(idCliente);
		$('#txtnombrePersona').val(nombrePersona);
		$('#txtidNegocio').val(idNegocio);
		$('#txttituloNegocio').val(tituloNegocio);
		$('#divTiempo').hide();
	}
	//Metodo para cambiar el estado en la base de datos de acuerdo al idActividad
	cambiaEstado = function(idActividad,estado,fechaCompletado)
	{
		$('#txtIdActividad2').val(idActividad);
		$('#txtEstado').val(estado);
		if(estado==1){
			$('#labActComp').html("Actividad completada");
			$('#Guardar2').val('Desacompletar');
			$('#txtFechaCompletado').val(fechaCompletado);
		}
		else{
			$('#labActComp').html("Completar actividad");
			$('#Guardar2').val('Completar');
			$('#txtFechaCompletado').val('');
		}
	}

	$('#form-cambiaEstado').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mTerminarr').modal('toggle');
			$('#completarActividad').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consultas();
		});
		return false;
	});
	function myFunctionVerActividad(idOrganizacion, nombreOrganizacion, idCliente, nombrePersona, idNegocio, tituloNegocio) {
		$('#txtIdActividad').val("");
		$('#txtIdOrganizacion').val(idOrganizacion); 
		$('#txtNomOrganizacion').val(nombreOrganizacion);
		$('#txtidCliente').val(idCliente);
		$('#txtnombrePersona').val(nombrePersona);
		$('#txtidNegocio').val(idNegocio);
		$('#txttituloNegocio').val(tituloNegocio);
		$.post("index.php?c=negocios&a=ActividadesPorNegocio", {valorIdNegocio: idNegocio}, function(mensaje) {
			$("#ModalR").html(mensaje);
		});
	}

	consultas = function (){ 
		var idEmbudo = $('#selectIdEmbudo').val();
		var busqueda=$("#buscar").val();
		var filtro = $('#filtros').val();
		var Año = $('#selectAño').val();
		var Mes = $('#selectMes').val();
		var nombre = $('#selectIdEmbudo option:selected').html();
		$.post("index.php?c=negocios&a=ConsultasPronostico", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro, Año: Año, Mes: Mes }, function(mensaje) {
			$("#ReslutadoPro").html(mensaje);
		});
	}

	function myFunctionAccionCFC() {
		var Año = $('#selectAño').val();
		var Mes = $('#selectMes').val();
		var fechaCierre = $('#fechaCierre').val();
		var idNegocio = $('#idNegocio').val();
		var idEmbudo = $('#selectIdEmbudo').val();
		var busqueda=$("#buscar").val();
		var filtro = $('#filtros').val();
		var nombre = $('#selectIdEmbudo option:selected').html();	
		datos = {"idNegocio":idNegocio, "fechaCierre":fechaCierre};
		$.ajax({
			url: "index.php?c=negocios&a=CambiaFCierre",
			type: "POST",
			data: datos
		}).done(function(respuestaEtapa){
       //console.log(JSON.stringify(respuestaEmbudo));

       $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button><strong><center>'+respuestaEtapa+'</center></strong></div>');

       $.post("index.php?c=negocios&a=ConsultasPronostico", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro, Año: Año, Mes: Mes  }, function(mensaje){
       	$("#ReslutadoPro").html(mensaje);
       }); 
   });
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
		$('#mTerminar').modal('toggle'); 
		$('#fechaCierre').val("");
	}

	function myFunctionAccionReset() {
		var idEmbudo = $('#selectIdEmbudo').val();
		var busqueda=$("#buscar").val();
		var filtro = $('#filtros').val();
		var nombre = $('#selectIdEmbudo option:selected').html();	
		var Año = $('#selectAño').val();
		var Mes = $('#selectMes').val();

		$.post("index.php?c=negocios&a=ConsultasPronostico", {valorIdEmbudo: idEmbudo, valor: busqueda, valorNomEmbudo: nombre, filtrostatus: filtro, Año: Año, Mes: Mes  }, function(mensaje){
			$("#ReslutadoPro").html(mensaje);
		}); 

		$('#mTerminar').modal('toggle'); 
	}
	$('#form-actividades').submit(function() {
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(respuesta){
				console.log(respuesta);
				if(respuesta[0].mensaje == "cruzada"){
					$('#divCruzadas').show();
					$("#cruzadas").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
					$('#txtConfirmado').val('true');
					$('#btnGuardar').val('Confirmar');
				}else{
					$('#añadiractividad').modal('toggle');
					$('#completarActividad').modal('hide');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(1000).hide(600);
					consultas();
				}
			}
		})      
		return false;
	}); 
	function myFunctionlimpiar() {
		alert("hola");
		$('#divTiempo').hide();
		$('#txtIdActividad').val("");
		$('#txtTipo').val(""); 
		$('#txtfechaActividad').val("");
		$('#txtNotasAct').val("");
		$('#divCruzadas').hide();
		$('#txtHora').val("");
		$('#txtDuracion').val("");
		$('#checkTiempo').prop('checked',false);

	}
</script>