<!--Inicio de Modal añadir embudo-->
<div class="modal fade" id="mEmbudo" role="dialog">
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=negocios&a=GuardarEmbudo" id="form-embudo" role="form">
			<div class="modal-content"><!-- Modal content-->
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo"></label></strong></h4>
				</div>
				<div class="modal-body"><!-- Cuerpo -->
					<div class="form-group">
						<input type="hidden" name="idEmbudo" id="txtIdEmbudo">

						<div class="form-group" id="nombreEmbudo">
							<h5>Titulo del Embudo</h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-filter"></span></span>
								<input type="text" class="form-control" name="nombre" id="txtNombre" placeholder="Título del embudo" required>
							</div>
						</div>			
						<input type="hidden" name="idUsuario" id="txtIdUsuario" value="1">
					</div>
				</div><!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<input type="submit" class="btn btn-success" id="btnGuardar" value="">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de añadir embudo-->

<!--Inicio de Modal añadir embudo-->
<div class="modal fade" id="mEtapa" role="dialog">
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=negocios&a=RegistrarEtapa" id="form-etapa"  role="form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTituloEtapa"></label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">  
					<input type="hidden" name="idEtapa" id="txtIdEtapa">
					<input type="hidden" name="idEmbudo" id="txtIdEmbudoEtapa">

					<div class="form-group" id="divEtapa">
						<h5>Nombre de la etapa</h5>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-flag"></span></span>
							<input type="text" class="form-control" id="txtNombreEtapa" name="nombreEtapa">
						</div>
					</div>

					<div class="row" id="labProNegocio">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h5>Probabilidad del negocio</h5>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group" id="proNegocio">
							<div class="col-xs-5 col-md-5" style="padding-right: 0px">
								<div class="input-group">
									<div class="input-group-addon">
										<strong>%</strong>
									</div>
									<input type="text" class="form-control" maxlength="3" id="txtProbabilidad" name="probabilidad">
								</div>
							</div>
							<div class="col-xs-6 col-sm-6" style="padding-left: 5px ">
								<p class="help-block">% (0...100)</p>
							</div>
						</div>
					</div>

					<div class="form-group" id="radioEstancandose">
						<h5>Negocio "estancándose"</h5>
						<div class="col-md-3">
							<div class="radio">
								<label for="si">
									<input type="radio" name="optradio" onclick="myFunctionChecked('si', 'si')" id="txtCheckedSi">
									Sí
								</label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="radio">
								<label for="no">
									<input type="radio" name="optradio" onclick="myFunctionChecked('no', 'no')" id="txtCheckedNo">
									No
								</label>
							</div>
						</div>
					</div>

					<div class="row" id="labDiasInactividad">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h5>Mostrar "estancado" después de</h5>
							</div>
						</div>
					</div>

					<div class="row" id="diasInactividad">
						<div class="form-group" id="">
							<div class="col-xs-5 col-md-3" style="padding-right: 0px">
								<input type="text" class="form-control" maxlength="3" name="inactividad" id="txtInactividad">
							</div>
							<div class="col-xs-6 col-sm-6" style="padding-left: 5px ">
								<p class="help-block">días de inactividad</p>
							</div>                
						</div>
					</div>
					<!--Fin modal añadir etapa-->
					<input type="hidden" name="idUsuario" id="txtIdUsuario" value="1">
				</div>

				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-lg-3" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminarEtapa" data-toggle="tooltip" title="Eliminar etapa" id="btnEliminarEtapa" onclick="myFunctionEliminarEtapa()"><span class="glyphicon glyphicon-trash"></span></a>
						</div>
						<div class="col-lg-9" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" id="btnGuardarEtapa">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
						</div> 
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de añadir embudo-->


<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=negocios&a=EliminarEmbudo" id="form-emEliminar" role="form">
			<div class="modal-content"><!-- Modal content-->
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Embudo</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;"><!-- Cuerpo --> 
					<p style="font-size: 18px;">¿Está seguro que desea eliminar el embudo?</p>
					<input type="hidden" id="txtIdEmbudoE" name="idEmbudo">
				</div>	<!-- fin cuerpo -->
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar" id="btnEliminarEmbudo">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
					</div>
				</div>
			</div>
		</form>
	</div>
</div>  

<div class="modal fade" id="mEliminarEtapa" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=negocios&a=EliminarEtapa" id="form-etEliminar" role="form">
			<div class="modal-content"><!-- Modal content-->
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Etapa</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;"><!-- Cuerpo --> 
					<p style="font-size: 18px;">¿Está seguro que desea eliminar la etapa?</p>
					<input type="hidden" id="txtIdEtapaME" name="idEtapa">
				</div>	<!-- fin cuerpo -->
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar" id="btnEliminarEtapa">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
					</div>
				</div>
			</div>
		</form>
	</div>
</div>    
