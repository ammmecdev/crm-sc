<style>
/* unvisited link */
a.estadisticas:link {
	color: #000000;
}
/* visited link */
a.estadisticas:visited {
	color: #9E9E9E;
}

/* mouse over link */
a.estadisticas:hover {
	color: #2196F3;
}

/* selected link */
a.estadisticas:active {
	color: #0D47A1;
}

/*ABIERTOS*/
.abiertos{
	color: #5499C7;
}

.abiertos:hover{
	color: #1F618D;
}
/*ABIERTOS*/

/*GANADOS*/
.ganados{
	color: #52BE80;
}
.ganados:hover{
	color: #1E8449;
}
/*GANADOS*/

/*PERDIDOS*/
.perdidos{
	color: #CD6155;
}
.perdidos:hover{
	color: #922B21;
}
/*PERDIDOS*/

/* #85144b */
.circle {
	background-color: #3c8dbc;
	color: white;
	border-radius: 50%;
	border: 1x solid grey;
	padding:10px;
}
.circle2 {
	background-color: #85144b;
	color: white;
	border-radius: 50%;
	border: 1x solid grey;
	padding:10px;
}
</style>
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default" style="margin-bottom: 0px;">
			<div class="panel-body">
				<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" id="pan2">
					<div class="col-xs-12  col-lg-12">
						<font size="5"><strong>Tablero</strong></font>
						<hr class="hidden-md hidden-lg" style="margin-top: 1px; margin-bottom: 10px">
					</div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="padding-left: 0px; padding-right: 0px; margin-bottom: 5px;">

					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0px; padding-left: 0px">
						<div class="col-lg-12">
							<input type="date" class="form-control" name="fechaInicio" id="txtFechaInicio" onchange="Consultas();">
						</div>
					</div>

					<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="center">
						<i class="mdi mdi-keyboard-arrow-left circle2" style="font-size: 16px;"></i>
					</div>

					<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" style="padding-right: 0px; padding-left: 0px">
						<div class="col-lg-12">
							<input type="date" class="form-control" name="fechaFinal" id="txtFechaFinal" onchange="Consultas();">
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7" style="padding-left: 0px; padding-right: 0px">

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-right: 0px; padding-left: 0px; margin-bottom: 5px;">
						<div class="col-lg-12" style="padding-left: 0px; padding-right: 0px">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="center">
								<i class="mdi mdi-equalizer mdi-rotate-180 circle" style="font-size: 16px;"></i>
							</div>
							<div class="col-xs-10 col-md-10 col-lg-10">
								<!--Select para el filtro por embudo -->
								<select class="form-control selectpicker show-menu-arrow" name="idEmbudo" type="button" aria-haspopup="true" aria-expanded="true" id="selectIdEmbudo" onchange="Consultas()">
									<option class="btn-default" value="<?php echo $_SESSION['idEmbudo']?>"><?php echo $_SESSION['nombre']; ?></option>
								</select>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-left: 0px">
						<div class="col-lg-12" style="padding-right: 0px; padding-left: 0px">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="center">
								<i class="mdi mdi-person circle" style="font-size: 16px;"></i>
							</div>
							<div class="col-xs-10 col-lg-10" style="padding-right: 0px">
								<select class="form-control selectpicker show-menu-arrow" name="idUsuario" id="selectIdUsuario" onchange="Consultas()">
									<option value="">Todos</option>

								</select>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-left: 0px">
						<div class="col-lg-12" style="padding-right: 0px; padding-left: 0px">
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" align="center">
								<i class="glyphicon glyphicon-usd circle" style="font-size: 13px;"></i>
							</div>
							<div class="col-xs-10 col-lg-10" style="padding-right: 0px">
								<select class="form-control selectpicker show-menu-arrow" id="selectTipoMoneda" onchange="Consultas()">
									<option value="VR">Valor real</option>
									<option value="MXN">Pesos mexicanos</option>
									<option value="USD">Dolares</option>	
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="margen">
	<!--Primer columna-->
	<div class="col-xs-12 col-lg-4">
		<br>
		<!--Cuerpo Columna-->

		<!--Primer panel-->
		<div class="panel panel-default">
			<div class="panel-body" id="NuevosNegocios">
				<!-- Aqui se imprime la estructura del panel de nuevos negocios-->
			</div>
		</div>

		<!--Segundo panel-->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-xs-12 col-lg-12">
							<h4 class="estadisticas">Actividades añadidas</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="col-xs-12 col-lg-12" align="right">
							<p class="help-block" style="margin-top: 0px;"><i class="far fa-calendar-alt" style="color:#337ab7; font-size: 22px;"></i></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="col-xs-12 col-lg-12" id="barraActividadesA">
							<!-- Aqui se muestra las actividades añadidas -->
						</div>
					</div>
				</div>
			</div>
		</div>

		
	</div>

	<!--Inicio de la segunda columna ...................   -->
	<div class="col-xs-12 col-lg-4">
		<br>
		<!--Cuerpo columna-->

		<!--Primer panel-->
		<div class="panel panel-default">
			<div class="panel-body" id="NegociosPerdidos">
				<!-- Aqui nos imprime la estructura del panel de negocios perdidos -->
			</div>
		</div>

		<!--Segundo panel-->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-xs-12 col-lg-12">
							<h4 class="estadisticas">Actividades completadas</h4>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="col-xs-12 col-lg-12" align="right">
							<p class="help-block" style="margin-top: 0px;"><i class="far fa-calendar-alt" style="color: #3c763d; font-size: 22px;"></i></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="col-xs-12 col-lg-12" id="barraActividadesC">
							<!-- Aqui se muestra las actividades completadas -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--Segundo panel ..................................................................................
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-xs-12 col-lg-12">
							<a href=""><h4>Correos electrónicos enviados</h4></a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="col-xs-6 col-lg-6">
							<h4><strong>0</strong></h4>
						</div>
						<div class="col-xs-6 col-lg-6" align="right">
							<span class="glyphicon glyphicon-send"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<hr>
					</div>
				</div>
			</div>
		</div>

		Tercer panel
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="col-xs-12 col-lg-12">
							<a href=""><h4>Correos electrónicos recibidos</h4></a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="col-xs-6 col-lg-6">
							<h4><strong>0</strong></h4>
						</div>
						<div class="col-xs-6 col-lg-6" align="right">
							<span class="glyphicon glyphicon-send"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<hr>
					</div>
				</div>
			</div>
		</div>......................................................................................-->
	</div> 
	<!-- Cerrar la Segunda columna ....................... -->


	<!-- Inicio Tercer Columna ............................-->
	<div class="col-xs-12 col-lg-4">
		<br>
		<!--Cuerpo Columna-->
		<!--Primer panel-->
		<div class="panel panel-default">
			<div class="panel-body" id="NegociosGanados">
				<!-- Aqui se imprime la estructura del panel de negocios ganados-->
			</div>
		</div>

		<!--Segundo panel-->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-lg-12">
						<div class="col-xs-12 col-lg-12">
							<h4 class="estadisticas">Mayoría de negocios ganados por</h4>
						</div>
					</div>
				</div>
				<div class="row" id="NegociosGanadosPor">
					<!-- Aqui se imprimen los usuarios (N / Ganados) -->
				</div>
			</div>
		</div>

		<!--Tercer panel-->
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row" style="padding-bottom: 10px;">
					<div class="col-lg-12">
						<div class="col-xs-12 col-lg-12">
							<h4 class="estadisticas">Mayoría de negocios iniciados por</h4>
						</div>
					</div>
				</div>
				<div class="row" id="NegociosIniciadosPor">
					<!-- Aqui se imprimen los usuarios (N / Iniciados) -->
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header bg-success">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><strong id="tituloModal"></strong></h4>
					</div>
					<div class="modal-body">
						<div style="overflow-x: auto;">
							<div class="table-responsive" id="imprimirContenido">
								<!-- Aqui se imprime la tabla de negocios-->
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<script>
        //Funcion para ejecuatar funciones al recargar el sistema
        window.onload=function(){
        	Consultas();
        	listarUsuarios();
        }

        Consultas = function (idEmbudo){
        	var idEmbudo = $('#selectIdEmbudo').val();
        	var nombre = $('#selectIdEmbudo option:selected').html();

        	var idUsuario = $('#selectIdUsuario').val();
        	var FechaInicio = $('#txtFechaInicio').val();
        	var FechaFin = $('#txtFechaFinal').val();
        	var tipoMoneda = $('#selectTipoMoneda').val();
        	
        	$("#selectIdEmbudo").empty();
        	var selector = document.getElementById("selectIdEmbudo");
        	selector.options[0] = new Option(nombre, idEmbudo);
        	$.ajax({
        		url: "index.php?c=panel&a=ListarEmbudos",
        	}).done(function(respuesta){
        		var j=0;
        		for (var i in respuesta) {
        			if(idEmbudo != respuesta[i].idEmbudo){
        				var j=j+1;
        				selector.options[j] = new Option(respuesta[i].nombre,respuesta[i].idEmbudo);
        			}
        		}
        		$('#selectIdEmbudo').selectpicker('refresh');
        	});

        	$.post("index.php?c=panel&a=barraActividadesA", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin }, function(estructura) {
        		$("#barraActividadesA").html(estructura);
        	});
        	$.post("index.php?c=panel&a=barraActividadesC", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin }, function(estructura) {
        		$("#barraActividadesC").html(estructura);
        	});
        	$.post("index.php?c=panel&a=NegociosIniciadosPor", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin }, function(consulta) {
        		$("#NegociosIniciadosPor").html(consulta);
        	});
        	$.post("index.php?c=panel&a=NegociosGanadosPor", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin }, function(consulta) {
        		$("#NegociosGanadosPor").html(consulta);
        	});
        	$.post("index.php?c=panel&a=NegociosGanados", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin, tipoMoneda: tipoMoneda }, function(consulta) {
        		$("#NegociosGanados").html(consulta);
        	});
        	$.post("index.php?c=panel&a=NegociosPerdidos", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin, tipoMoneda: tipoMoneda }, function(consulta) {

        		$("#NegociosPerdidos").html(consulta);
        	});
        	$.post("index.php?c=panel&a=NuevosNegocios", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin, tipoMoneda: tipoMoneda }, function(consulta) {
        		$("#NuevosNegocios").html(consulta);
        	});
        }
        function listarUsuarios(){
        	$.ajax({
        		url: "index.php?c=panel&a=listarUsuarios",
        		type: "POST"
        	}).done(function(respuesta){
        		$("#selectIdUsuario").empty();
        		var selector = document.getElementById("selectIdUsuario");
        		selector.options[0] = new Option("Todos","");
        		for (var i in respuesta) {
        			var j=parseInt(i)+1;
        			selector.options[j] = new Option(respuesta[i].nombreUsuario,respuesta[i].idUsuario);
        		}
        		$('#selectIdUsuario').selectpicker('refresh');
        	});
        }

        contenidoModal = function (opcion){
        	var idEmbudo = $('#selectIdEmbudo').val();
        	var idUsuario = $('#selectIdUsuario').val();
        	var FechaInicio = $('#txtFechaInicio').val();
        	var FechaFin = $('#txtFechaFinal').val();
        	var tipoMoneda = $('#selectTipoMoneda').val();
  			var dollar = <?php echo $dolar; ?>;

        	if (opcion == 1) {
        		$.post("index.php?c=panel&a=TablaGanados", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin, tipoMoneda: tipoMoneda, dollar: dollar }, function(consulta) {
        			$("#imprimirContenido").html(consulta);
        			$("#tituloModal").html('Negocios ganados');
        		});

        	}else if (opcion == 2) {
        		$.post("index.php?c=panel&a=TablaPerdidos", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin, tipoMoneda: tipoMoneda, dollar: dollar }, function(consulta) {
        			$("#imprimirContenido").html(consulta);
        			$("#tituloModal").html('Negocios perdidos');

        		});

        	}else{
        		$.post("index.php?c=panel&a=TablaIniciados", { valorIdEmbudo: idEmbudo, idUsuario: idUsuario, FechaInicio: FechaInicio, FechaFin: FechaFin, tipoMoneda: tipoMoneda, dollar: dollar }, function(consulta) {
        			$("#imprimirContenido").html(consulta);
        			$("#tituloModal").html('Negocios iniciados');
        		});

        	}        
        }
    </script>