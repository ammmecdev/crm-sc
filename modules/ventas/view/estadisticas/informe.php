<link rel="stylesheet" href="assets/css/informe.css">

<!--Contenedor-->
<div class="padding-nav2">
	<div class="wrapper">
		<!-- Sidebar Holder -->
		<nav id="sidebar" class="navbar-default">
			<div class="sidebar-header">
				<h3 align="center">Estadísticas</h3>
			</div>


			<ul class="list-unstyled components">
				<li>
					<div class="container-fluid">
						<div class="form-group">
							<h5>Seleccione un embudo:</h5>
							<select name="" id="" class="form-control selectpicker">
								<option value="">AMMMEC 2018</option>
								<option value="">DINGO</option>
								<option value="">PAULS FANS</option>
							</select>
						</div>
					</div>	
				</li>
				<li>
					<div class="container-fluid">
						<div class="form-group">
							<h5>Seleccione un rango de fecha:</h5>
							<input type="date" class="form-control">
						</div>	
					</div>
				</li>
				<li class="line"></li>
			</ul>			

			<ul class="list-unstyled components">
				<li class="active">
					<a href="#homeSubmenu" class="lista" data-toggle="collapse" aria-expanded="false">Plan de Trabajo</a>
					<ul class="collapse list-unstyled" id="homeSubmenu">
						<li><a href="#" class="lista">Home 1</a></li>
						<li><a href="#" class="lista">Home 2</a></li>
						<li><a href="#" class="lista">Home 3</a></li>
					</ul>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Cuenta Planeada</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#pageSubmenu" class="lista" data-toggle="collapse" aria-expanded="false">Instrucciones de Trabajo</a>
					<ul class="collapse list-unstyled" id="pageSubmenu">
						<li><a href="#" class="lista">Page 1</a></li>
						<li><a href="#" class="lista">Page 2</a></li>
						<li><a href="#" class="lista">Page 3</a></li>
					</ul>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Cuenta Real</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Indicador de Productividad</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Indicador de Seguridad</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Indicador Tiempo de Entrega</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Indicador TPEF</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Balance General</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Autoevaluación</a>
				</li>
				<li class="line"></li>
				<li>
					<a href="#" class="lista">Costos Planeados</a>
				</li>
				<li class="line"></li>
			</ul>
		</nav>

		<!-- Page Content Holder -->
		<div id="content">

			<nav class="navbar navbar-default" id="navbar">
				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" id="sidebarCollapse" class="navbar-btn">
							<span></span>
							<span></span>
							<span></span>
						</button>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li style="margin-top: 8px;">

							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div>
<!--Contenedor-->

<script src="assets/js/informe.js"></script>