<style>
.negro{
	color: black;
}
.negro:hover{
	color:black;
}

div.horizontal{
	overflow: auto;
	white-space: nowrap;
}
div.horizontal button{
	display: inline-block;
	text-align: center;
	text-decoration: none;
	margin-left: 0px;
	margin-right: 0px;
}
.table{
	margin-bottom: 0px;
}
ul.pagination>li>a{
	cursor: pointer;
	background-color: #fff;
	border-color: #ddd;	
}
ul.pagination>li#previous>a{
	color: #000;
	background-color: #fff;
	border-color: #ddd;
}
</style>
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-5 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mOrganizacion" onclick="myFunctionNuevo();"> Añadir Cliente</a>
						</div>
					</div>
					
					<div class="col-xs-3 col-sm-4 col-md-4 col-lg-4" align="center">
						<div class="btn-group">
							<h5><strong><font id="h5Contador"></font></strong></h5>
						</div>
					</div>
					
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=organizaciones&a=Exportar" enctype="multipart/form-data" >
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="bAlfabetica" class="form-control" id="txtbAlfabetica">
									<input type="text" name="buscar" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="paginador(1);">
								</div> 
							</div>

							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" class="btn btn-default" name="btnExportar" id="btnExportar" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="horizontal form-group" role="group">
							<button type="button" class="btn btn-sm btn-primary" onclick="filtrarOrganizaciones('')" id="btn-todas">Todas</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('A')" id="btn-a">A</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('B')" id="btn-b">B</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('C')" id="btn-c">C</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('D')" id="btn-d">D</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('E')" id="btn-e">E</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('F')" id="btn-f">F</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('G')" id="btn-g">G</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('H')" id="btn-h">H</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('I')" id="btn-i">I</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('J')" id="btn-j">J</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('K')" id="btn-k">K</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('L')" id="btn-l">L</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('M')" id="btn-m">M</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('N')" id="btn-n">N</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Ñ')" id="btn-ñ">Ñ</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('O')" id="btn-o">O</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('P')" id="btn-p">P</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Q')" id="btn-q">Q</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('R')" id="btn-r">R</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('S')" id="btn-s">S</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('T')" id="btn-t">T</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('U')" id="btn-u">U</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('V')" id="btn-v">V</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('W')" id="btn-w">W</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('X')" id="btn-x">X</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Y')" id="btn-y">Y</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Z')" id="btn-z">Z</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="white-space: nowrap; font-weight: normal;">
						Registros:
						<select class="form-control selectpicker" id="selectPorPagina" onchange="paginador(1);" data-width="auto" style="display: inline-block;">
							<option value="5">5</option>
							<option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<!--Fin de Encabezado-->

<!-- Inicio contenedor -->
<div style="overflow-x: auto;" id="resultadoBusqueda">
	<!--Aqui se vera a tabla de resultados-->
</div>

<!--Inicio modal de añadir cliente -->
<div class="modal fade" id="mOrganizacion" role="dialog">   
	<div class="modal-dialog">
		<form id="form-organizacion" method="post" action="?c=organizaciones&a=Guardar" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong> <label id="labTitulo"></label></strong></h4>
				</div>
				<div class="modal-body">  
					<!-- Cuerpo --> 
					<div class="form-group">
						<input type="hidden" class="form-control" name="idOrganizacion" id="txtIdOrganizacion" value="">
						
						<div class="form-group">
							<h5>Nombre completo</h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business" style="font-size: 18px;"></i></span>
								<input type="text" class="form-control" aria-describedby="basic-addon1" name="nombreOrganizacion" id="txtNomOrganizacion" 
								value="txtNomOrganizacion" placeholder="Nombre del Cliente">
							</div>
							<div id="alertanombre">							
							</div>
						</div>
					</div>

					<div class="form-group">
						<input type="hidden" name="propietario" id="txtPropietario" value="1">
					</div>

					<div class="form-group">
						<h5>Clave</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="far fa-keyboard" style="font-size: 16px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon1" name="clave" id="txtClave" maxlength="3" placeholder="Clave del Cliente">
						</div>
						<div id="alertaclave">							
						</div>
					</div>

					<div class="form-group">
						<h5>Dirección</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-location-on" style="font-size: 17px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon1" name="direccion" id="txtDireccion" placeholder="Dirección del Cliente">
						</div>
					</div>

					<div class="form-group">
						<h5>Localidad / estado</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-explore" style="font-size: 17px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon1" name="localidadEstado" id="txtlocalidadEstado" placeholder="Localidad / Estado del Cliente">
						</div>
					</div>

					<div class="form-group">
						<h5>Página web</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-language" style="font-size: 17px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon1" name="paginaWeb" placeholder="Página web del cliente" id="txtPaginaWeb">
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-5 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
								<h5>Teléfono</h5>
							</div>
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-6" style="padding-left: 50px;">
								<h5>Extensión </h5>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 0px">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-phone" style="font-size: 16px;"></i></span>
									<input type="text" class="form-control" aria-describedby="basic-addon2" name="telefono" id="txtTelefono" required placeholder="Telefono">
								</div>
							</div>

							<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 15px">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-dialpad" style="font-size: 16px;"></i></span>
									<input type="text" class="form-control" aria-describedby="basic-addon2" name="extensionTelefonica" id="txtExtension" required placeholder="Extensión">
								</div>
							</div>             
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
								<h5>Tipo de teléfono</h5>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-8 col-sm-6" style="width: 100%">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="fas fa-bars" style="font-size: 17px;"></i></span>
									<select class="form-control" name="tipoTelefono" id="selectTipoTelefono">
										<option value="">Tipo de teléfono</option>
										<option value="Trabajo">Trabajo</option>
										<option value="Casa">Casa</option>
										<option value="Celular">Celular</option>
										<option value="Otro">Otro</option>
									</select>
								</div>
							</div>                
						</div>
					</div>

					<div class="form-group">
						<h5>Sector industrial</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-industry" style="font-size: 16px;"></i></span>
							<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idSector" id="selectIdSectorIndustrial">
							</select>
						</div>
					</div>

					<div class="form-group">
						<h5>RFC</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="far fa-id-card" style="font-size: 14px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon1" name="rfc" placeholder="Ej. VECJ880326 XXX" id="txtRFC">
						</div>
					</div>
				</div>
				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-3 col-lg-3" align="left">
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar organización" 
						id="btnEliminar" onclick="myFunctionEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
					</div>
					<div class="col-xs-9 col-lg-9" align="right"> 
						<input type="submit" class="btn btn-success" value="Guardar" id="btnGuardar" >
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de añadir organizacion -->

<!--Inicio modal de confirmar para eliminar negocio -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form-organizaciondel" action="index.php?c=organizaciones&a=Eliminar" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header  bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo">Eliminar Cliente</label> </strong></h4>
				</div>
				<div class="modal-body">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar el cliente?</p>
					<input type="hidden" class="form-control" name="idOrganizacion" id="txtIdOrganizacionE" value="">
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-success" value="Eliminar" >
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar negocio -->
<script src="./assets/js/clientes.js"></script>