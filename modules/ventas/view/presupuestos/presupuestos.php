<link rel="stylesheet" type="text/css" href="assets/css/presupuesto.css">

<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">

				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group form-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mPresupuestoGeneral" onclick="myFunctionNuevo()"> Añadir Presupuesto</a>
						</div>
						<div class="btn-group form-group" role="group">
							<a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#mPresupuestoNegocio" data-toggle="tooltip" data-placement="bottom" title="Agregar negocio" onclick="myFunctionNuevoPN()" id="btnPushNegocio"><i class="mdi mdi-note-add mdi-lg" style="font-size: 18px;"></i></a>
							<a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#mPresupuestoCreados" onclick="listarPresupuestosGenerales()" data-toggle="tooltip" data-placement="bottom" title="Presupuestos creados"><i class="fas fa-clone" style="font-size: 15px;"></i></a>
						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="col-lg-4" style="padding-left: 0px;">
							<select class="form-control selectpicker" id="selectAnio" onchange="consultaOnchange()"> 
								<option value="">Año</option>
								<!-- Aqui se imprime el año de los presupuestos -->
							</select>
						</div>
						<div class="col-xs-12 col-lg-1 hidden-xs" style="padding-left: 0px; padding-right: 43px;">
							<i class="mdi mdi-remove mdi-2x mdi-rotate-90"></i>
						</div>
						<div class="col-lg-4" style="padding-left: 0px;">
							<select class="form-control selectpicker" id="selectMes" onchange="consultaOnchange();">
								<option value="">Mes</option>
								<option value="Enero">Enero</option>
								<option value="Febrero">Febrero</option>
								<option value="Marzo">Marzo</option>
								<option value="Abril">Abril</option>
								<option value="Mayo">Mayo</option>
								<option value="Junio">Junio</option>
								<option value="Julio">Julio</option>
								<option value="Agosto">Agosto</option>
								<option value="Septiembre">Septiembre</option>
								<option value="Octubre">Octubre</option>
								<option value="Noviembre">Noviembre</option>
								<option value="Diciembre">Diciembre</option>
							</select>
						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=presupuesto&a=Exportar">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="idPG" id="txtIdPG">
									<input type="hidden" name="idMes" id="txtIdMes">
									<input type="text"  name="buscar" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultaOnchange();">
								</div> 
							</div>
							<div class="btn-group form-group" style="padding-left: 0px; padding-right: 0px;">
								<select class="form-control selectpicker show-menu-arrow" name="idEmbudo" type="button" id="selectIdEmbudo" onchange="listarEmbudos()">
									<option class="btn-default" value="<?php echo $_SESSION['idEmbudo']?>"><?php echo $_SESSION['nombre']; ?> </option>
								</select>
							</div>
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro" id="btnExportar"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->

<!-- Modal  -->
<div class="modal fade" id="mPresupuestoCreados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gray">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Presupuestos creados</strong></h4>
			</div>
			<div class="modal-body" >
				<div class="form-group">
					<strong id="avisoEditar"></strong>
				</div>
				<div class="form-group" id="presupuestoGeneral">
					<!--Aqui se imprime la table de presupuestos generales -->
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<!--Inicio modal de añadir presupuesto general -->
<div class="modal fade" id="mPresupuestoGeneral" role="dialog">
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=presupuesto&a=Guardar" role="form" id="form-presupuesto">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTituloPresupuesto"></label></strong></h4>
				</div>
				<div class="modal-body">  
					<h4>Presupuesto <strong  id="labelPeriodo"></strong></h4>
					<hr>
					<input type="hidden" name="idPresupuestoGeneral" id="txtIdPresupuestoGeneral">

					<div class="form-group">
						<h5>Nombre: </h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-file-alt" style="font-size: 18px;"></i></span>
							<input type="text" class="form-control" name="nombrePresupuesto" id="txtNombrePresupuesto" placeholder="Nombre del presupuesto" value="">
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 0;">
						<h5>Periodo: </h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2">
								<i class="far fa-calendar-alt" style="font-size: 18px"></i></span>
								<select class="form-control selectpicker show-menu-arrow" name="periodo" id="selectPeriodo" onchange="functionPeriodo();">
									<option value="">Seleccione el año del presupuesto</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div id="aviso" class="col-lg-12" style="margin-bottom: 10px;"></div>
						</div>
						<div class="form-group">
							<h5>Responsable: </h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><i class="fas fa-user" style="font-size: 18px;"></i></span>
								<input type="text" class="form-control" name="responsable" id="txtResponsable" value="<?php echo $_SESSION['nombreUsuario']; ?>" data-bv-excluded="false" readonly>
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
							<div class="col-lg-12" align="right" style="padding-right: 0px">
								<input type="submit" class="btn btn-success"  id="btnPresupuesto">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>  
							</div>  
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>     
	<!--fin modal de añadir presupuesto general -->
	<!--Inicio modal para eliminar presupuestos generales -->
	<div class="modal fade" id="mEliminarPG" role="dialog">   
		<div class="modal-dialog">
			<form  method="post" action="index.php?c=presupuesto&a=Eliminar" role="form" id="form-eliminar">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header bg-danger">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong>Eliminar presupuesto</strong></h4>
					</div>
					<div class="modal-body"  style="margin-bottom: -30px;">   
						<!-- Cuerpo --> 
						<p style="font-size: 20px;">¿Está seguro que desea eliminar el presupuesto?</p>
						<input type="hidden" id="txtIdPGE" name="idPresupuestoGeneral">
						<!-- fin cuerpo --> 
					</div>
					<div class="modal-footer">
						<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
							<input type="submit" class="btn btn-danger" value="Eliminar">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>     
	<!--fin modal para eliminar presupuestos generales -->
	<!--Inicio modal de añadir presupuesto de negocio -->
	<div class="modal fade" id="mPresupuestoNegocio" role="dialog">
		<div class="modal-dialog">
			<form data-toggle="validator" method="post" action="index.php?c=presupuesto&a=GuardarNPresupuesto" role="form" id="form-presupuestoNegocio">
				<div class="modal-content">
					<div class="modal-header bg-success">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><strong><label id="labTituloPresupuestoNegocio"></label></strong></h4>
					</div>
					<div class="modal-body">  
						<input type="hidden" name="idPresupuesto" id="txtIdPresupuesto">

						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<h5>Filtrar por negocio</h5>
									<select name="idNegocio" id="selectIdNegocio" class="form-control selectpicker show-menu-arrow" data-live-search="true" onchange="activarDatos()">
										<option value="">Seleccione un negocio</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group" id="divCliente">
							<h5>Cliente</h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2">
									<i class="fas fa-briefcase" style="font-size: 16px"></i></span>
									<input type="text" class="form-control" name="idOrganizacion" id="txtIdOrganizacion" readonly>
								</div>
							</div>

							<div class="form-group" id="divServicio">
								<h5>Servicio</h5>
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2">
										<i class="fas fa-wrench" style="font-size: 16px"></i></span>
										<input type="text" class="form-control" name="claveServicio" id="txtClaveServicio" readonly>
									</div>
								</div>

								<div class="form-group" id="divEquipo">
									<h5>Equipo</h5>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon2">
											<i class="fas fa-shopping-cart" style="font-size: 16px"></i></span>
											<input type="text" class="form-control" name="equipo" id="txtEquipo" readonly>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<h5>Descripción</h5>
												<textarea name="descripcion" id="txtNotas" class="form-control"></textarea>
											</div>
										</div>
									</div>
									<input type="hidden" id="txtIdPresupuestoG" name="idPresupuestoGeneral">
								</div>
								<div class="modal-footer">
									<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
										<div class="col-lg-3" align="left" style="padding-left: 0px">
											<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar negocio del presupuesto" id="btnEliminar" onclick="myFunctionEliminar()"><i class="fas fa-trash-alt" style="font-size: 15px;"></i></a>
										</div>
										<div class="col-lg-9" align="right" style="padding-right: 0px">
											<input type="submit" class="btn btn-success" id="btnGuardar" value="Guardar" >
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>  
										</div>  
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>     
				<!--fin modal de añadir presupuesto de negocio -->

				<!--Inicio modal para eliminar negocios del presupuesto -->
				<div class="modal fade" id="mEliminar" role="dialog">   
					<div class="modal-dialog">
						<form  method="post" action="index.php?c=presupuesto&a=EliminarPN" role="form" id="form-eliminarPN">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header bg-danger">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title"><strong>Eliminar Negocio</strong></h4>
								</div>
								<div class="modal-body"  style="margin-bottom: -30px;">   
									<!-- Cuerpo --> 
									<p style="font-size: 18px;">¿Está seguro que desea eliminar el negocio del presupuesto ?</p>
									<input type="hidden" id="txtIdPresupuestoN" name="idPresupuestoN">
									<!-- fin cuerpo --> 
								</div>
								<div class="modal-footer">
									<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
										<input type="submit" class="btn btn-danger" value="Eliminar">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>     
				<!--fin modal para eliminar negocios del presupuesto -->
				
				<div class="modal fade" id="mDetalles" role="dialog">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header bg-success">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" id="titleModal"></h4>
							</div>
							<div class="modal-body">  
								<h4 id="servicio" style="margin-top: 0px;"></h4>

								<div class="row" style="padding-top: 10px">
									<div class="col-lg-6">
										<p style="color: #2980B9; margin: 0px; padding-bottom: 13px;"><font size="3">Presupuesto: </font></p>
										<table class="table">
											<tr>
												<td><strong>Mes</strong></td>
												<td align="right"><strong>Monto (MXN)</strong></td>
											</tr>
											<tbody id="bodyPresupuesto"></tbody>
											<tr id="total"></tr>
										</table>
									</div>
									<div class="col-lg-6">
										<p style="color: #2980B9; margin: 0px; padding-bottom: 13px;"><font size="3">Estimaciones: </font></p>
										<table class="table">
											<tr>
												<td><strong>#</strong></td>
												<td><strong>Fecha prevista</strong></td>
												<td align="right"><strong>Monto (MXN)</strong></td>
											</tr>
											<tbody id="bodyEstimacion">
												
											</tbody>
											<tr id="tfootEstimacion">

											</tr>
										</table>
									</div>
								</div>

								<div class="bs-callout bs-callout-danger">
									<h5 id="calloutTotal"></h5>
								</div>	

							</div>
							<div class="modal-footer">
								<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
									<button type="button" class="btn btn-success" data-dismiss="modal">Aceptar</button>   
								</div>
							</div>
						</div>
					</div>
				</div>     

				<!--Inicio del Contenido-->
				<div style="overflow-x: auto;" id="tablaPresupuestos">
					<!-- Aqui se imprime la tabla de presupuestos -->
				</div>
				<!--Fin del contenido-->
				<script src="assets/js/presupuesto.js"></script>