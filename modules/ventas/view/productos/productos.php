<style>
div.horizontal{
	overflow: auto;
	white-space: nowrap;
}
div.horizontal button{
	display: inline-block;
	text-align: center;
	text-decoration: none;
	margin-left: 0px;
	margin-right: 0px;
}

</style>
<div class="panel panel-default" style="margin-bottom: 0px;">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<div class="btn-group">
					<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#añadirProd" onClick="myFunction()"> Añadir Servicio</a>
				</div>
				<br><br>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-3 col-lg-4" align="center">
				<div class="btn-group">
					<h6><strong>10 Servicios</strong></h6>
				</div>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-2 col-lg-4" align="right">
				<form  method="POST" action="?c=Actividades&a=Exportar" enctype="multipart/form-data">
					<div class="btn-group">
						<div class="form-group">
							<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="buscar" onkeyup="consultas();">
						</div> 
					</div>
					<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
						<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
					</div>
				</form>
			</div>
		</div>


		<div class="row">
			<div class="col-xs-12 col-lg-12">
				<div class="horizontal" role="group" aria-label="...">
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('')">Todas</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('A')" >A</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('B')" >B</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('C')" >C</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('D')" >D</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('E')" >E</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('F')" >F</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('G')" >G</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('H')" >H</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('I')" >I</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('J')" >J</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('K')" >K</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('L')" >L</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('M')" >M</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('N')" >N</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Ñ')" >Ñ</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('O')" >O</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('P')" >P</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Q')" >Q</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('R')" >R</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('s')" >S</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('T')" >T</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('U')" >U</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('V')" >V</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('W')" >W</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('X')" >X</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Y')" >Y</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarOrganizaciones('Z')" >Z</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->



<!--Inicio modal de añadir persona -->
<div class="modal fade" id="añadirProd" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="actualizarMateria.php" enctype="multipart/form-data" onsubmit="return actual();">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Añadir servicio nuevo</strong></h4>
				</div>
				<div class="modal-body">	
					<!-- Cuerpo --> 
					<form action="" method="post">
						<div class="form-group">
							<div class="form-group">
								<h5>Nombre del producto</h5>
								<input type="text" class="form-control">
							</div>
							<div class="form-group">
								<h5>Código del producto</h5>
								<input type="text" class="form-control">
							</div>
							<h5>Precio unitario</h5>
							<div class="row">
								<div class="form-group">
									<div class="col-xs-4 col-sm-4" style="width: 40%">
										<input type="text" class="form-control">
									</div>
									<div class="col-xs-8 col-sm-8" style="width: 60%">
										<select class="form-control">
											<option>US Dollar (USD)</option>
											<option>Pesos Méxicanos (MXN)</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<h5>Productos y Servicios</h5>
								<select class="form-control">
									<option>Ninguno</option>
									<option></option>
								</select>
							</div>
							<div class="form-group">
								<h5>Equipo</h5>
								<select class="form-control">
									<option>Ninguno</option>
									<option>Molinos</option>
									<option>Celdas</option>
								</select>
							</div>
							<div class="form-group">
								<h5>Costo por unidad <span class="glyphicon glyphicon-question-sign"></span></h5>
								<input type="text" class="form-control">
							</div>
							
							<div class="form-group">
								<h5>Costo directo <span class="glyphicon glyphicon-question-sign"></span></h5>
								<input type="text" class="form-control">
							</div>
							
							<div class="form-group">
								<h5>Teléfono</h5>
								<input type="text" class="form-control">
							</div>
							
						</div>
						
					</form>
					
					
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-lg-12">
						<input type="submit" class="btn btn-success" value="Guardar">
						<button type="button" class="btn btn-default">Cancelar</button>
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de añadir negocio -->

<!-- Inicio de contenedor -->
<div class="table-responsive" id="tbl">
	<table class="table table-hover">
		<tr>
			<th></th>
			<th>Nombre completo</th>
			<th>Código de producto</th>
			<th>Precio Unitario</th>
			<th>Tipo Moneda Unitario</th>
			<th>Precio P/Unidad</th>
			<th>Precio Directo</th>
			<th>Producto y Servicio</th>
			<th>Equipo</th>
			<th>Precio Referencia</th>
			<th>Tipo Moneda Referencia</th>
			<th>Tiempo Entrega</th>
		</tr>

		<?php foreach($this->model->Listar() as $r): ?>

			<tr>   
				<td><input type="radio" name="check" value="check" onclick="myFunction2(); myFunction3();"></td>

				<td><?php echo $r->NOMBREPRODUCTO; ?></td>
				<td><?php echo $r->CODIGOPRODUCTO; ?></td>
				<td>$ <?php echo $r->PUNITARIO; ?></td>
				<td><?php echo $r->TIPOMONEDAUNI; ?></td>
				<td>$ <?php echo $r->COSTOXUNIDAD; ?></td>
				<td>$ <?php echo $r->COSTODIRECTO; ?></td>
				<td><?php echo $r->PROYSER; ?></td>
				<td><?php echo $r->EQUIPO; ?></td>
				<td>$ <?php echo $r->PRECIOREFERENCIA; ?></td>
				<td><?php echo $r->TIPOMONEDAREF; ?></td>
				<td><?php echo $r->TIEMPOENTREGA; ?></td>
			</tr>
			
		<?php endforeach; ?>

	</table>
</div>
<!-- Fin de contenedor -->