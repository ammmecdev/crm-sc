
<ul class="nav navbar-nav">
	<li class="admin1"><a <?php if(isset($negocios)){ ?> id="seleccion" <?php } ?> href="?c=negocios"><i class="fas fa-dollar-sign" style="font-size: 18px;"></i> Negocios</label><span class="sr-only">(current)</span></a></li>
	<li class="admin2"><a <?php if(isset($actividades)){ ?> id="seleccion" <?php } ?> href="?c=actividades"><i class="far fa-calendar-alt" style="font-size: 18px;"></i> Actividades</a></li>

	<li class="dropdown" id="admin3">
		<a <?php if(isset($clientes)){ ?> id="seleccion" <?php } ?> href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-users" style="font-size: 18px;"></i> Clientes <span class="caret"></span></a>
		<ul class="dropdown-menu">
			<li><a <?php if(isset($contacto)){ ?> id="submenu" <?php } ?> href="?c=personas"><i class="fas fa-user" style="font-size: 15px;"></i>&nbsp; Contacto</a></li>
			<li role="separator" class="divider"></li>
			<li><a <?php if(isset($cliente)){ ?> id="submenu" <?php } ?> href="?c=organizaciones"><i class="mdi mdi-business mdi-lg"></i> Cliente</a></li>
			<li role="separator" class="divider"></li>
			<li><a <?php if(isset($sector)){ ?> id="submenu" <?php } ?> href="?c=sector"><i class="fas fa-industry" style="font-size: 15px;"></i>&nbsp; Sector Industrial</a></li>
		</ul>
	</li>
	<li class="dropdown" id="admin3">
		<a <?php if(isset($estadisticas)){ ?> id="seleccion" <?php } ?> href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-chart-line mdi-lg"></i> Estadisticas <span class="caret"></span>
		</a>
		<ul class="dropdown-menu">
			<li><a <?php if(isset($panel)){ ?> id="submenu" <?php } ?> href="?c=panel"><i class="mdi mdi-dashboard mdi-lg"></i> Panel</a></li>
			<li role="separator" class="divider"></li>
			<li><a <?php if(isset($informe)){ ?> id="submenu" <?php } ?> href="?c=informe"><i class="mdi mdi-trending-up mdi-lg" style="font-size: 18px;"></i> Indicadores</a></li>
			<!-- <li role="separator" class="divider"></li>
			<li><a href=""><i class="mdi mdi-timeline mdi-lg"></i> </a></li> -->
		</ul>
	</li>
	<li class="dropdown" id="admin3">
		<a <?php if(isset($productos)){ ?> id="seleccion" <?php } ?> href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-inbox" style="font-size: 18px;"></i> Productos <span class="caret"></span></a>
		<ul class="dropdown-menu">
			<!--<li><a href="?c=productos"><i class="fas fa-wrench" style="font-size: 17px;"></i> Servicios</a></li>-->
			<!-- <li role="separator" class="divider"></li> -->
			<li><a <?php if(isset($equipo)){ ?> id="submenu" <?php } ?> href="?c=equipo"><i class="mdi mdi-shopping-cart mdi-lg"></i> Equipo</a></li>
		</ul>
	</li>
	<li class="dropdown" id="admin3">
		<a <?php if(isset($procesos)){ ?> id="seleccion" <?php } ?> href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cogs" style="font-size: 16px;"></i> Procesos <span class="caret"></span></a>
		<ul class="dropdown-menu">
			<li><a <?php if(isset($presupuesto)){ ?> id="submenu" <?php } ?> href="?c=presupuesto"><i class="fas fa-calculator" style="font-size: 16px;"></i> Presupuesto</a></li>
		</ul>
	</li>
	<!--<li class="admin4"><a href="?c=deal">Detalles</a></li>-->
</ul>

