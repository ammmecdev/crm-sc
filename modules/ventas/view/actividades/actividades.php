<link rel="stylesheet" href="./assets/css/actividades.css">

<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mActividades" onclick="nuevaActividad()"> Añadir actividad </a>
						</div>
						<br><br>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="center">
						<div class="btn-group">
							<h5><strong><font id="h5Contador"></font></strong></h5>
						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=actividades&a=Exportar" enctype="multipart/form-data" id="form-exportar">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="periodo" class="form-control" id="periodo">
									<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="consultas();">
								</div> 
							</div>
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-sm-5 col-lg-5" style="padding-right: 0px;">
						<div class="btn-group form-group btn-group-sm" role="group">
							<select class="form-control" name="tipoAct" id="tipoAct" onchange="consultas()">
								<option value="completas">Completas</option>
								<option value="leads">Leads</option>
								<option value="internas">Internas</option>
							</select>
						</div>
						<div class="btn-group form-group btn-group-sm" role="group" aria-label="...">
							<button id="btnTodas" type="button" class="btn btn-default" onclick="filtrarActividades('')">Todas</button>
							<button id="btnLlamada" type="button" class="btn btn-default" data-toggle="tooltip" title="Llamada" onclick="filtrarActividades('Llamada')"><i class="mdi mdi-phone-in-talk mdi-lg"></i></button>
							<button id="btnReunion" type="button" class="btn btn-default" data-toggle="tooltip" title="Reunión" onclick="filtrarActividades('Reunion')"><i class="mdi mdi-group mdi-lg"></i></button>
							<button id="btnTarea" type="button" class="btn btn-default" data-toggle="tooltip" title="Tarea" onclick="filtrarActividades('Tarea')"><i class="far fa-clock" style="font-size: 14px;"></i></button>
							<button id="btnPlazo" type="button" class="btn btn-default" data-toggle="tooltip" title="Plazo" onclick="filtrarActividades('Plazo')"><i class="fas fa-hourglass-half" style="font-size: 13px;"></i></button>
							<button id="btnEmail" type="button" class="btn btn-default" data-toggle="tooltip" title="Email" onclick="filtrarActividades('Email')"><i class="mdi mdi-email mdi-lg"></i></button>
							<button id="btnComida" type="button" class="btn btn-default" data-toggle="tooltip" title="Comida" onclick="filtrarActividades('Comida')"><i class="fas fa-utensils" style="font-size: 13px;"></i></button>
							<button id="btnWhatsApp" type="button" class="btn btn-default" data-toggle="tooltip" title="WhatsApp" onclick="filtrarActividades('WhatsApp')"><i class="fab fa-whatsapp" style="font-size: 15px;"></i></button>
						</div>
					</div>

					<div class="col-xs-12 col-sm-7 col-lg-7 botones" align="right">
						<div class="horizontal" role="group">
							<button id="btnPlaneado" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('1')">Planeado</button>
							<button id="btnVencida" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('2')">Vencida</button>
							<button id="btnHoy" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('3')">Hoy</button>
							<button id="btnManana" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('4')">Mañana</button>
							<button id="btnEstaSemana" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('5')">Esta semana</button>
							<button id="btnProximaSemana" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('6')">Próxima semana</button>
							<button id="btnCompletado" type="button" class="btn btn-sm btn-default" onclick="filtrarActividades('7')">Completado</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->

<!-- Inicio del contenedor -->
<div class="table-responsive" id="tbl">  

	<div id="resultadoBusqueda">
		<!--Aqui se vera a tabla de resultados-->
	</div>

</div>
<!-- Fin del contenedor -->

<?php include './view/actividades/modal/mActividad.php'; ?>

<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=actividades&a=Eliminar" enctype="multipart/form-data" id="form-eliminar">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar actividad</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar la actividad?</p>
					<input id="txtIdActividadE" name="idActividad" hidden>
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de eliminar negocio -->

<!--Modal Completado-->
<div class="modal fade" id="mTerminar" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form  method="post" data-toggle="validator" action="index.php?c=actividades&a=CambiaEstado" enctype="multipart/form-data" role="form" id="form-cambiaEstado">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><strong><font id="labActComp"></font></strong></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="">Fecha:</label>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="date" class="form-control" name="fechaCompletado" required id="txtFechaCompletado">
						</div>
					</div>
				</div>

				<input type="hidden" id="txtEstado" name="completado">
				<input type="hidden" id="txtIdActividad2" name="idActividad">

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<input type="submit" class="btn btn-success" id="Guardar2">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Fin Modal Completado-->

<script src="./assets/js/actividades.js"></script>
