<!--Inicio modal de añadir actividad -->
<div class="modal fade" id="mActividades" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=actividades&a=Guardar" enctype="multipart/form-data" role="form" id="form-actividades">
			<input type="hidden" class="form-control" name="actividad" id="txtActividad"/>

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo"></label> </strong></h4>
				</div>
				<div class="modal-body"> 

					<!-- Cuerpo -->
					<div class="form-group">
						<input type="hidden" class="form-control" value="0" name="idActividad" id="txtIdActividad" readonly>
						<input type="hidden" class="form-control" id="txtConfirmado" name="confirmado" readonly>
					</div>

					<div class="row">

						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;">
								<font size="1">TIPO DE ACTIVIDAD</font>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="btn-group form-group">
								<label class="btn btn-sm btn-default" id="labLlamada" onclick="tipoActividad('Llamada');" data-toggle="tooltip" title="Llamada">
									<i class="mdi mdi-phone-in-talk mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionLlamada">
								</label>
								<label class="btn btn-sm btn-default" id="labReunion" onclick="tipoActividad('Reunion');" data-toggle="tooltip" title="Reunión">
									<i class="mdi mdi-group mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionReunion">
								</label>
								<label class="btn btn-sm btn-default" id="labTarea" onclick="tipoActividad('Tarea');" data-toggle="tooltip" title="Tarea">
									<span>
										<i class="far fa-clock" style="font-size: 14px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionTarea">
								</label>
								<label class="btn btn-sm btn-default" id="labPlazo" onclick="tipoActividad('Plazo');" data-toggle="tooltip" title="Plazo">
									<span>
										<i class="fas fa-hourglass-half" style="font-size: 13px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionPlazo">
								</label>
								<label class="btn btn-sm btn-default" id="labEmail" onclick="tipoActividad('Email');"  data-toggle="tooltip" title="Correo electrónico">
									<i class="mdi mdi-email mdi-lg"></i>
									<input type="radio" class="oculto" name="options" id="optionEmail">
								</label>
								<label class="btn btn-sm btn-default" id="labComida" onclick="tipoActividad('Comida');" data-toggle="tooltip" title="Comida">
									<span>
										<i class="fas fa-utensils" style="font-size: 13px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionComida">
								</label>
								<label class="btn btn-sm btn-default" id="labWhatsApp" onclick="tipoActividad('WhatsApp');" data-toggle="tooltip" title="WhatsApp">
									<span>
										<i class="fab fa-whatsapp" style="font-size: 15px;"></i>
									</span>
									<input type="radio" class="oculto" name="options" id="optionWhatsapp">
								</label>
							</div>
						</div>

					</div>

					<div class="form-group">
						<input style="text-align:center; font-weight: bold;" readonly class="form-control" placeholder="Actividad" name="tipo" id="txtTipo">
					</div>

					<div class="form-group">
						<textarea id="txtNotasA" type="text" class="form-control autoExpand" placeholder="Descripción de actividad" name="notas"></textarea>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;">
								<font size="1">FECHA</font>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<input type="date" class="form-control" name="fecha" id="txtfechaActividad"/>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 5px;">
								<div class="checkbox">
									<label>
										<input type="checkbox" name='checkTiempo' id="checkTiempo"> Asignar hora y duración
									</label>
								</div>
							</div>
						</div>
					</div>

					<div id="divTiempo"> 
						<div class="row">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<font size="1">HORA</font>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<input type="time" class="form-control" name="hora" id="txtHora" onblur="deshabilitaConfirmado()">
								</div>
							</div>
						</div>

						<div class="row" style="margin-top: 15px;">
							<div class="form-group">
								<div class="col-lg-12">
									<font size="1">DURACIÓN</font>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<input type="number" class="form-control" name="duracion" id="txtDuracion" min="1" max="100" value="1" onblur="deshabilitaConfirmado()">
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<select class="form-control" name="tipoDuracion" id="selectTipoDuracion" onchange="deshabilitaConfirmado()">
										<option value="minutos">Minutos</option>
										<option value="horas">Horas</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group" style="margin-top: 15px;">
						<font size="1" id="fontVinculacion">VINCULADO A</font>
						<div id="divFiltro">
							<div class="radio">
								<label for="">
									<input type="radio" id="porOrganizacion" name="radioFiltros"  value="cliente"><font size="1">Filtrar por cliente</font>
								</label>
							</div>
							<div class="radio">
								<label for="">
									<input type="radio" id="porNegocio" name="radioFiltros" value="negocios"><font size="1">Filtrar por negocios</font>
								</label>
							</div>
							<div class="radio">
								<label for="">
									<input type="radio" id="actividadInterna" name="radioFiltros" value="interna"><font size="1">Actividad interna</font>
								</label>
							</div>
						</div>
					</div>

					<!--Div select Principal de filtro por organizaciones -->
					<div class="form-group" id="selectOrganizaciones">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business mdi-lg"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="idOrganizacion" id="selectIdOrganizaciones" onchange="listarPorOrganizacion()">
							</select>
						</div>
					</div>
					<!--/Div organizaciones-->

					<div class="form-group" id="selectNegociosF">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-briefcase"></i></span>
							<select class="form-control" data-live-search="true" name="idNegocio" id="selectIdNegocioF"  onchange="obtenerIdNegocioF()">
							</select>
						</div>
					</div>   

					<!--Div select Principal de filtro por negocios -->
					<div class="form-group" id="selectNegocios">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-briefcase"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="idNegocio" id="selectIdNegocio"  onchange="listarOrganizacionesPorNegocio()">
							</select>
						</div>
					</div>       
					<!--/Div negocios--> 

					<div class="row" id="divOrganizaciones" style="margin-bottom: 15px;">
						<div class="form-group">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business mdi-lg"></i></span>
									<input type="text" class="form-control" id="txtNombreOrganizacion" name="nombreOrganizacion" readonly/>
									<input type="hidden" class="form-control" name="idOrganizacion" id="txtIdOrganizacion">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group" id="selectPersonas">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="nombrePersona" id="selectNombrePersona">
								<option value="">Seleccione un contacto</option>
							</select>
						</div>
					</div>

					<div class="row" id="divCruzadas" style="margin-bottom: -50px;">
						<div class="form-group">
							<div class="col-lg-12">
								<div id="cruzadas"></div>
							</div>
						</div>
					</div>
					<!-- fin cuerpo --> 
				</div>

				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-left: 0px; padding-right: 0px">
						<div class="col-xs-2 col-lg-2" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar organización" id="btnEliminar" onclick="eliminarActividad()">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</div>
						<div class="col-xs-10 col-lg-10" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" id="btnGuardar">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						</div> 
					</div>
				</div>

			</div>
		</form>
	</div>
</div>     
<!--fin modal de añadir negocio -->