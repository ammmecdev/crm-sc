<style>
	h2 {
		width: 100%;
		opacity: 0.4;
		font-size: 38pt;
	}
	.imag {
		padding-top: 80px;
		width: 400px;
		opacity: 0.6;
	}

	@media (min-width: 1367px) and (max-width: 2500px) { 

		.imag{
			padding-top: 170px;
			width: 400px;
			opacity: 0.6;
		}

	}
	@media (max-width: 767px) {

		.imag{
			padding-top: 53px;
			width: 400px;
			opacity: 0.6;
		}

	}
</style>
<div style="padding-top: 230px;">
	<div class="row">
			<div class="col-lg-12" align="center">
				<h2 align="center"><i class="fas fa-exclamation-triangle mdi-2x"></i> PÁGINA EN CONSTRUCCIÓN</h2>
			</div>
	</div>
	
</div>