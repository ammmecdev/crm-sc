<link rel="stylesheet" type="text/css" href="assets/css/equipo.css">

<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mEquipo" onclick="nuevoEquipo()"> Añadir Equipo</a>
						</div>
						<br><br>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-3 col-lg-4" align="center">
						<div class="btn-group">
							<h5><strong><font id="h5Contador"></font></strong></h5>
						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-2 col-lg-4" align="right">
						<form  method="POST" action="?c=equipo&a=Exportar" id="form-exportar">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="bAlfabetica" class="form-control" id="txtBAlfabetica">
									<input type="text" name="buscar" class="form-control" placeholder="Buscar" id="txtBuscar" onkeyup="pintarTabla();">
								</div> 
							</div>
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro" id="btnExportar"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
				</div>


				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="horizontal" role="group" aria-label="...">
							<button type="button" class="btn btn-sm btn-primary" onclick="filtrarEquipo('')" id="btn-todas">Todas</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('A')" id="btn-a">A</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('B')" id="btn-b">B</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('C')" id="btn-c">C</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('D')" id="btn-d">D</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('E')" id="btn-e">E</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('F')" id="btn-f">F</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('G')" id="btn-g">G</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('H')" id="btn-h">H</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('I')" id="btn-i">I</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('J')" id="btn-j">J</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('K')" id="btn-k">K</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('L')" id="btn-l">L</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('M')" id="btn-m">M</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('N')" id="btn-n">N</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('Ñ')" id="btn-ñ">Ñ</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('O')" id="btn-o">O</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('P')" id="btn-p">P</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('Q')" id="btn-q">Q</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('R')" id="btn-r">R</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('S')" id="btn-s">S</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('T')" id="btn-t">T</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('U')" id="btn-u">U</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('V')" id="btn-v">V</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('W')" id="btn-w">W</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('X')" id="btn-x">X</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('Y')" id="btn-y">Y</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarEquipo('Z')" id="btn-z">Z</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->


<!--Inicio modal de añadir persona -->
<div class="modal fade" id="mEquipo" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=equipo&a=Guardar" id="form-equipo" role="form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo"></label></strong></h4>
				</div>
				<div class="modal-body">	
					<!-- Cuerpo --> 
					<form action="" method="post">

						<input type="hidden" name="equipo" id="txtEquipo">

						<div class="row" style="margin-top: -8px">
							<div class="form-group">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right: 0px;">
									<h5>Clave del equipo</h5>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-xs-6 col-md-6 col-lg-6">
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><i class="far fa-keyboard" style="font-size: 16px;"></i>
										</span>
										<input type="text" class="form-control" maxlength="3" name="clave" data-bv-feedbackicons="false" id="txtClave" maxlength="3">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-6 col-md-6 col-lg-6">
		
								</div>
							</div>
							<div id="alerta" class="col-lg-12"></div>
						</div>


						<div class="form-group">
							<h5>Nombre del equipo</h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="fas fa-box" style="font-size: 18px;"></i></span>
								<input type="text" class="form-control" name="nombreEquipo" id="txtNombreEquipo" maxlength="70">
							</div>
						</div>

					</form>

					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<div class="col-xs-3 col-lg-3" align="left" style="padding-left: 0px">
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar negocio" id="btnEliminar" onclick="modalEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
						</div>
						<div class="col-xs-9 col-lg-9" align="right" style="padding-right: 0px">
							<input type="submit" class="btn btn-success" id="btnGuardar" >
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>  
						</div>  
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de añadir equipo -->

<!--Inicio modal para eliminar equipo -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="index.php?c=equipo&a=Eliminar" role="form" id="form-eliminar">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Equipo</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;">   
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Está seguro que desea eliminar el equipo?</p>
					<input type="hidden" id="txtIdEquipo" name="idEquipo">
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>   
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal para eliminar equipo -->

<!-- Inicio de contenedor -->
<div class="table-responsive" id="tbl" style="background-color: ">
	<div id="crearTabla">
		<!--Aqui se vera a tabla de resultados-->
	</div>
</div>
<!-- Fin de contenedor -->

<script src="assets/js/equipo.js"></script>

