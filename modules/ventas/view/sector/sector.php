<link rel="stylesheet" href="./assets/css/sector.css">
<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mSector" onclick="myFunctionNuevo()"> Añadir Sector </a>
						</div>
						<br><br>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="center">
						<div class="btn-group">
							<h5><strong><font id="h5Contador"></font></strong></h5>
						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=sector&a=Exportar" enctype="multipart/form-data">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="bAlfabetica" class="form-control" id="txtbAlfabetica">
									<input type="text" name="buscar" class="form-control" placeholder="Buscar" id="buscar" onkeyup="pintarTabla();">
								</div> 
							</div>
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="sumbit" id="btnExportar" name="btnExportar" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
				</div>

			<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="horizontal" role="group">
							<button type="button" class="btn btn-sm btn-primary" onclick="filtrarSector('')" id="btn-todas">Todas</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('A')" id="btn-a">A</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('B')" id="btn-b">B</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('C')" id="btn-c">C</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('D')" id="btn-d">D</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('E')" id="btn-e">E</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('F')" id="btn-f">F</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('G')" id="btn-g">G</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('H')" id="btn-h">H</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('I')" id="btn-i">I</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('J')" id="btn-j">J</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('K')" id="btn-k">K</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('L')" id="btn-l">L</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('M')" id="btn-m">M</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('N')" id="btn-n">N</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('Ñ')" id="btn-ñ">Ñ</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('O')" id="btn-o">O</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('P')" id="btn-p">P</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('Q')" id="btn-q">Q</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('R')" id="btn-r">R</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('S')" id="btn-s">S</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('T')" id="btn-t">T</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('U')" id="btn-u">U</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('V')" id="btn-v">V</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('W')" id="btn-w">W</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('X')" id="btn-x">X</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('Y')" id="btn-y">Y</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarSector('Z')" id="btn-z">Z</button>
						</div>
					</div>
				</div>    
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->

<!--Inicio del Contenedor-->
<div style="overflow-x: auto;" id="resultadoBusqueda">
	<!--Aqui se vera a tabla de resultados-->
</div>
<!--Fin de Contenedor-->

<!--Inicio modal de añadir persona -->
<div class="modal fade" id="mSector" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="?c=sector&a=Guardar" enctype="multipart/form-data" id="form_sector">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo"></label></strong></h4>
				</div>
				<div class="modal-body">	
					<!-- Cuerpo --> 
					<div class="form-group">
						<input type="hidden" class="form-control" name="idSector" id="idSector" value="">
						<div class="form-group">
							<h5>Clave de Sector</h5>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="far fa-keyboard" style="font-size: 16px;"></i></span>
								<input type="text" maxlength="3" name="claveSector" id="claveSector" class="form-control" placeholder="Clave del Sector">
							</div>
							<div id="alertaclave"></div>
						</div>
					</div>
					<div class="form-group">
						<h5>Nombre del Sector</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-industry" style="font-size: 17px;"></i></span>
							<input type="text" name="nombreSector" id="nombreSector" class="form-control" placeholder="Nombre del Sector">
						</div>
					</div>
				</div>
				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-3 col-lg-3" align="left">
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar Sector" 
						id="btnEliminar" onclick="myFunctionEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
					</div>
					<div class="col-xs-9 col-lg-9" align="right"> 
						<input type="submit" class="btn btn-success" value="Guardar" id="btnGuardar" >
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button> 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>   
<!--Inicio modal de confirmar para eliminar sector -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="formsectordel" action="index.php?c=sector&a=Eliminar" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header  bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong><label id="labTitulo">Eliminar Sector</label> </strong></h4>
				</div>
				<div class="modal-body">  
					<p style="font-size: 18px;">¿Está seguro que desea eliminar el sector?</p>
					<input type="hidden" class="form-control" name="idSectorE" id="idSectorE" value="">
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar" >
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar sector -->  
<script src="./assets/js/sector.js"></script>