<!--Inicio modal agenda telefonica -->
<div class="modal fade" id="mAgenda" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Agenda telefonica</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-1">
							<i class="mdi mdi-account-circle mdi-3x" style="font-size: 60px;"></i>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
							<div class="col-lg-12" id="nomPersona">
								<!-- Honorifico nombre persona -->
							</div>
							<div class="col-lg-12" id="telPersona"> 
								<!-- (extensión) telefono, tipo de teléfono -->
							</div>

						</div>
					</div>
				</div>
				<hr style="margin-top: 15px; margin-bottom: 5px;">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<h5 id="verificador" class="text-danger"></h5>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-left: 17px;">
							<h5>Teléfono</h5>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" style="padding-left: 17px;">
							<h5>Extensión</h5>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4" style="padding-left: 17px;">
							<h5>Tipo de Teléfono</h5>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2" style="padding-left: 0px;">

						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
						<input type="hidden" name="idTelefono_tel" id="idTelefono">
						<input type="hidden" name="idCliente_tel" id="idCliente">

						<div class="col-xs-12 col-sm-3 col-lg-3" style="padding-right: 0px">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-contact-phone" style="font-size: 18px;"></i></span>
								<input type="text" class="form-control" aria-describedby="basic-addon2" name="telefono_tel" id="telefono" placeholder="Teléfono">
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-lg-3" style="padding-right: 0px">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-dialpad" style="font-size: 18px;"></i></span>
								<input type="text" class="form-control" aria-describedby="basic-addon2" name="extension_tel" id="extension" placeholder="Extensión">
							</div>
						</div> 
						<div class="col-xs-12 col-sm-3 col-lg-3">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon2"><i class="fas fa-bars" style="font-size: 18px;"></i></span>
								<select class="form-control" name="tipoTelefono_tel" id="tipoTelefono">
									<option value="">Tipo de teléfono</option>
									<option value="Oficina">Oficina</option>
									<option value="Casa">Casa</option>
									<option value="Celular">Celular</option>
									<option value="Otro">Otro</option>
								</select>
							</div>              
						</div>
						<div class="col-xs-12 col-sm-3 col-lg-3" align="right">
							<button type="button" onclick="agregarTelefono()" class="btn btn-primary btn-sm" style="width: 48%;"><i class="fas fa-plus-circle" style="margin-right: 8px; font-size: 13px;"></i> Añadir</button>
							<button type="button" onclick="limpiarInputs()" class="btn btn-warning btn-sm" style="width: 48%;"><i class="fas fa-eraser" style="margin-right: 8px; font-size: 13px;"></i> Limpiar</button>
						</div>   
					</div>
				</div>
				<hr>
				<div class="form-group">
					<div class="table-responsive" id="tabla_telefonos">
						<!-- Aqui se muestra la tabla de telefonos -->
					</div>
				</div>
			</div>
			<div class="modal-footer" style="margin-top: 0px;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- fin modal agenda telefonica -->