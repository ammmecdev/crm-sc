<style>
	div.horizontal{
		overflow: auto;
		white-space: nowrap;
	}
	div.horizontal button{
		display: inline-block;
		text-align: center;
		text-decoration: none;
		margin-left: 0px;
		margin-right: 0px;
	}
</style>
<div class="panel panel-default" style="margin-bottom: 0px;">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<div class="btn-group">
					<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mPersona" onclick="myFunctionNuevo()"> Añadir Contacto </a>
				</div>
				<br><br>
			</div>

			<div class="col-xs-3 col-sm-4 col-md-4 col-lg-4" align="center">
				<div class="btn-group">
					<h5><strong><font id="h5Contador"></font></strong></h5>
				</div>
			</div>

			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
				<form  method="POST" action="?c=personas&a=Exportar" id="form-exportar" enctype="multipart/form-data">
					<div class="btn-group">
						<div class="form-group">
							<input type="hidden" name="periodo" class="form-control" id="periodo">
							<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="buscar" onkeyup="dibujaTabla();">
						</div> 
					</div>
					<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
						<button type="sumbit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
					</div>
				</form>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-lg-12">
				<div class="horizontal" role="group">
					<button type="button" class="btn btn-sm btn-primary" onclick="filtrarPersonas('')" id="btn-todas">Todas</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('A')" id="btn-a">A</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('B')" id="btn-b">B</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('C')" id="btn-c">C</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('D')" id="btn-d">D</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('E')" id="btn-e">E</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('F')" id="btn-f">F</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('G')" id="btn-g">G</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('H')" id="btn-h">H</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('I')" id="btn-i">I</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('J')" id="btn-j">J</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('K')" id="btn-k">K</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('L')" id="btn-l">L</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('M')" id="btn-m">M</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('N')" id="btn-n">N</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Ñ')" id="btn-ñ">Ñ</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('O')" id="btn-o">O</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('P')" id="btn-p">P</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Q')" id="btn-q">Q</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('R')" id="btn-r">R</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('S')" id="btn-s">S</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('T')" id="btn-t">T</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('U')" id="btn-u">U</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('V')" id="btn-v">V</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('W')" id="btn-w">W</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('X')" id="btn-x">X</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Y')" id="btn-y">Y</button>
					<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Z')" id="btn-z">Z</button>
				</div>
			</div>
		</div>    
	</div>
</div>
<!--Fin de Encabezado-->


<!--Inicio del Contenedor-->
<div style="overflow-x:auto;" id="resultadoBusqueda">
	<!--<div class="table-responsive" id="tbl"> 

		<div >
			Aqui se vera a tabla de resultados
		</div> 
	</div>-->
</div>
<!--Fin del Contenedor-->

<!--Inicio modal de añadir persona -->
<div class="modal fade" id="mPersona" role="dialog">
	<div class="modal-dialog">
		<form  method="post" id="form_persona" action="?c=personas&a=Guardar" role="form" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong> <label id="labTitulo"></label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" class="form-control" name="idCliente" id="txtIdCliente" value="">
					</div>
					<div class="form-group">
						<h5>Nombre Completo</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-user" style="font-size: 18px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon2" name="nombrePersona" id="txtNomPersona" required placeholder="Nombre Completo">
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
								<h5>Teléfono</h5>
							</div>
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-6" style="padding-left: 50px;">
								<h5>Extensión</h5>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 0px">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-contact-phone" style="font-size: 18px;"></i></span>
									<input type="text" class="form-control" aria-describedby="basic-addon2" name="telefono" id="txtTelefono" required placeholder="Teléfono">
								</div>

							</div>
							<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 15px">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-dialpad" style="font-size: 18px;"></i></span>
									<input type="text" class="form-control" aria-describedby="basic-addon2" name="extension" id="txtExtension" required placeholder="Extensión">
								</div>

							</div>             
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
								<h5>Tipo de Teléfono</h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-8 col-sm-6" style="width: 100%">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="fas fa-bars" style="font-size: 19px;"></i></span>
									<select class="form-control" name="tipoTelefono" id="selectTipoTelefono">
										<option value="">Tipo de teléfono</option>
										<option value="Trabajo">Trabajo</option>
										<option value="Casa">Casa</option>
										<option value="Celular">Celular</option>
										<option value="Otro">Otro</option>
									</select>
								</div>
							</div>                
						</div>
					</div>
					<div class="form-group">
						<h5>Correo Electrónico</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-email" style="font-size: 17px;"></i></span>
							<input type="email" class="form-control" aria-describedby="basic-addon2" name="email" id="txtEmail" required placeholder="Correo Electrónico">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business" style="font-size: 18px;"></i></span>
							<select class="form-control selectpicker" data-live-search="true" name="idOrganizacion" id="selectIdOrganizaciones" required>
							</select>
						</div>

					</div>
					<h5>Honorífico</h5>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-8 col-sm-6" style="width: 100%">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="fas fa-graduation-cap" style="font-size: 14px;"></i></span>
									<select class="form-control" name="honorifico" id="selectHonorifico" required>
										<option value="">Honorifico</option>
										<option value="Ing.">Ing.</option>
										<option value="Lic.">Lic.</option>
										<option value="Sr.">Sr.</option>
										<option value="Srta.">Srta.</option>
									</select>
								</div>
							</div>                
						</div>
					</div>

					<div class="form-group">
						<h5>Puesto</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
							<input type="text" class="form-control" name="puesto" id="txtPuesto" placeholder="Puesto" required>
						</div>
					</div>
					<div class="form-group">
						<input type="hidden" name="propietario" id="txtPropietario" value="1">
					</div>
				</div>
				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-3 col-lg-3" align="left">
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar Persona" id="btnEliminar" onclick="myFunctionEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
					</div>
					<div class="col-xs-9 col-lg-9" align="right">
						<input type="submit" class="btn btn-success" value="Guardar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de añadir persona-->

<!--Inicio modal de confirmar para eliminar persona -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form_contactodel" action="index.php?c=personas&a=Eliminar" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Persona</strong></h4>
				</div>
				<div class="modal-body">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar la Persona?</p>
					<input type="hidden" class="form-control" name="idCliente" id="txtIdClienteE" value="">

					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-success" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
					<!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar persona -->

<script>
	window.onload=function(){
		dibujaTabla();
	}
	var bAlfabetica='';
	   //Metodo de busqueda por ajax
	   dibujaTabla = function (){ 
	   	var bTexto = $("#buscar").val();
	   	$.post("index.php?c=personas&a=Consultas", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(resultado) {
	   		$("#resultadoBusqueda").html(resultado); 
	   	});
	   	$.post("index.php?c=personas&a=Contador", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(noEquipo) {
	   		if (noEquipo==1)
	   			$("#h5Contador").html(noEquipo+' Contacto');
	   		else
	   			$("#h5Contador").html(noEquipo+' Contactos'); 
	   	})	
	   }

	   filtrarPersonas = function (busqueda){
	   	bAlfabetica = busqueda;
	   	switch(busqueda) {
	   		case 'A':
	   		desmarcar("a");
	   		break;
	   		case 'B':
	   		desmarcar("b");
	   		break;
	   		case 'C':
	   		desmarcar("c");
	   		break;
	   		case 'D':
	   		desmarcar("d");
	   		break;
	   		case 'E':
	   		desmarcar("e");
	   		break;
	   		case 'F':
	   		desmarcar("f");
	   		break;
	   		case 'G':
	   		desmarcar("g");
	   		break;
	   		case 'H':
	   		desmarcar("h");
	   		break;
	   		case 'I':
	   		desmarcar("i");
	   		break;
	   		case 'J':
	   		desmarcar("j");
	   		break;
	   		case 'K':
	   		desmarcar("k");
	   		break;
	   		case 'L':
	   		desmarcar("l");
	   		break;
	   		case 'M':
	   		desmarcar("m");
	   		break;
	   		case 'N':
	   		desmarcar("n");
	   		break;
	   		case 'Ñ':
	   		desmarcar("ñ");
	   		break;
	   		case 'O':
	   		desmarcar("o");
	   		break;
	   		case 'P':
	   		desmarcar("p");
	   		break;
	   		case 'Q':
	   		desmarcar("q");
	   		break;
	   		case 'R':
	   		desmarcar("r");
	   		break;
	   		case 'S':
	   		desmarcar("s");
	   		break;
	   		case 'T':
	   		desmarcar("t");
	   		break;
	   		case 'U':
	   		desmarcar("u");
	   		break;
	   		case 'V':
	   		desmarcar("v");
	   		break;
	   		case 'W':
	   		desmarcar("w");
	   		break;
	   		case 'X':
	   		desmarcar("x");
	   		break;
	   		case 'Y':
	   		desmarcar("y");
	   		break;
	   		case 'Z':
	   		desmarcar("z");
	   		break;
	   		default:
          //bAlfabetica='';
          desmarcar("todas");
          break;
      }
      dibujaTabla();
  }

  function listarOrganizaciones(){
  	datos = {};
  	$.ajax({
  		url: "index.php?c=personas&a=listarOrganizaciones",
  		type: "POST",
  		data: datos
  	}).done(function(respuesta){
  		$("#selectIdOrganizaciones").empty();
  		var selector = document.getElementById("selectIdOrganizaciones");
  		selector.options[0] = new Option("Seleccione el cliente","");
  		for (var i in respuesta) {
  			var j = parseInt(i) + 1;
  			selector.options[j] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
  		}
  		$('#selectIdOrganizaciones').selectpicker('refresh');
  	}); 
  }

  /*$('#form_persona').submit(function() {
  	$.ajax({
  		type: 'POST',
  		url: $(this).attr('action'),
  		data: $(this).serialize(),
  		success: function(respuesta){
  			//alert(respuesta);
  			if (respuesta == 'error') {
  				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center> error </center></strong></div>');
  			}else{
  				$("#mensajejs").html('<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
  			}
  			$('#mPersona').modal('toggle');

  			$('#mensajejs').show();
  			$('#mensajejs').delay(1000).hide(600);
  			dibujaTabla();
  		}
  	});
  	return false;
  }); */
  $('#form_persona').submit(function(){
  	$.ajax({
  		type: 'POST',
  		url: $(this).attr('action'),
  		data: $(this).serialize(),
  	}).done(function(respuesta){
  		if (respuesta=="error") {
  			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al guardar la Persona de contacto</center></strong></div>');
  		}else{
  			$("#mensajejs").html('<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
  		}
  		$('#mPersona').modal('toggle');
  		$('#mensajejs').show();
  		$('#mensajejs').delay(2500).hide(600);
    //$('#form-organizacion').modal('hide');
    dibujaTabla();
});
  	return false;
  }); 

  $('#form-contactodel').submit(function(){
  	$.ajax({
  		type: 'POST',
  		url: $(this).attr('action'),
  		data: $(this).serialize(),
  	}).done(function(respuesta){
  		$("#mensajejs").html('<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
  		$('#mPersona').modal('toggle');
  		$('#mEliminar').modal('toggle');
  		$('#mensajejs').show();
  		$('#mensajejs').delay(2500).hide(600);
  		dibujaTabla();
  	});
  	return false;
  }); 

  myFunctionEditarPersona = function (idCliente,honorifico, nombrePersona, idOrganizacion, nombreOrganizacion,telefono,extension, tipoTelefono, email, puesto) {
  	$('#txtIdCliente').val(idCliente);  
  	$('#txtNomPersona').val(nombrePersona);
  	$('#txtTelefono').val(telefono);
  	$('#txtExtension').val(extension);
  	$('#selectTipoTelefono').val(tipoTelefono);
  	$('#txtEmail').val(email);
  	$('#selectHonorifico').val(honorifico); 
  	$('#txtPuesto').val(puesto);
  	$('#btnEliminar').show();
  	$('#labTitulo').html("Editar Contacto");

		//--- Select de organizaciones ---
		$("#selectIdOrganizaciones").empty();
		var selectOrganizaciones = document.getElementById("selectIdOrganizaciones");
		selectOrganizaciones.options[0] = new Option(nombreOrganizacion , idOrganizacion);

		$.ajax({
			url: "index.php?c=personas&a=ListarOrganizaciones",
		}).done(function(respuesta){
			console.log(respuesta);
			var j=0;
			for (var i in respuesta) {
				if(idOrganizacion!=respuesta[i].idOrganizacion){
					var j=j+1;
					selectOrganizaciones.options[j] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
				}
			}
			$('#selectIdOrganizaciones').selectpicker('refresh');
		});
	}

	function myFunctionEliminar(){
		var idCliente = $('#txtIdCliente').val();
		$('#txtIdClienteE').val(idCliente);  
	}

	function desmarcar(letra){
		var miarray = ["todas", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

		for (var i = 0; i < miarray.length; i++) {
			if (letra == miarray[i]) {
				$("#btn-"+miarray[i]).addClass("btn-primary");
				$("#btn-"+miarray[i]).removeClass("btn-default");
			}else{
				$("#btn-"+miarray[i]).removeClass("btn-primary");
				$("#btn-"+miarray[i]).addClass("btn-default");
			}
		}
	}
	function myFunctionNuevo() {
		$('#txtIdCliente').val("");
		$('#selectIdOrganizaciones').val("");
		$('#txtNomPersona').val("");
		$('#txtTelefono').val("");
		$('#txtExtension').val("");
		$('#selectTipoTelefono').val("");
		$('#txtEmail').val("");
		$('#selectHonorifico').val("");
		$('#txtPuesto').val("");
		$('#btnEliminar').hide();
		$('#labTitulo').html("Añadir contacto");
		listarOrganizaciones();
	}
</script>