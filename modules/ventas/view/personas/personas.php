<style>
div.horizontal{
	overflow: auto;
	white-space: nowrap;
}
div.horizontal button{
	display: inline-block;
	text-align: center;
	text-decoration: none;
	margin-left: 0px;
	margin-right: 0px;
}
ul.pagination>li>a{
	cursor: pointer;
	background-color: #fff;
	border-color: #ddd;	
}
ul.pagination>li#previous>a{
	color: #000;
	background-color: #fff;
	border-color: #ddd;
}
</style>

<div class="padding-nav">
	<div style="padding-top: 21px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="btn-group">
							<a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#mPersona" onclick="myFunctionNuevo()"> Añadir Contacto </a>
						</div>
						<br><br>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="center">
						<div class="btn-group">
							<h5><strong><font id="h5Contador"></font></strong></h5>
						</div>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" align="right">
						<form  method="POST" action="?c=personas&a=Exportar" id="form-exportar">
							<div class="btn-group">
								<div class="form-group">
									<input type="hidden" name="bAlfabetica" class="form-control" id="txtbAlfabetica">
									<input type="text" name="valorBusqueda" class="form-control" placeholder="Buscar" id="buscar" onkeyup="paginador(1);">
								</div> 
							</div>
							<div class="btn-group form-group"  style="padding-left: 0px; padding-right: 0px;">
								<button type="submit"  id="btnExportar" name="btnExportar" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Exportar resultados del filtro"><i class="fas fa-download"></i></button>
							</div>
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 col-lg-10">
						<div class="horizontal form-group" role="group">
							<button type="button" class="btn btn-sm btn-primary" onclick="filtrarPersonas('')" id="btn-todas">Todas</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('A')" id="btn-a">A</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('B')" id="btn-b">B</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('C')" id="btn-c">C</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('D')" id="btn-d">D</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('E')" id="btn-e">E</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('F')" id="btn-f">F</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('G')" id="btn-g">G</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('H')" id="btn-h">H</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('I')" id="btn-i">I</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('J')" id="btn-j">J</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('K')" id="btn-k">K</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('L')" id="btn-l">L</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('M')" id="btn-m">M</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('N')" id="btn-n">N</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Ñ')" id="btn-ñ">Ñ</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('O')" id="btn-o">O</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('P')" id="btn-p">P</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Q')" id="btn-q">Q</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('R')" id="btn-r">R</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('S')" id="btn-s">S</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('T')" id="btn-t">T</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('U')" id="btn-u">U</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('V')" id="btn-v">V</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('W')" id="btn-w">W</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('X')" id="btn-x">X</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Y')" id="btn-y">Y</button>
							<button type="button" class="btn btn-sm btn-default" onclick="filtrarPersonas('Z')" id="btn-z">Z</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="white-space: nowrap; font-weight: normal;">
						Registros:&nbsp;
						<select class="form-control selectpicker" data-width="auto" id="selectPorPagina" onchange="paginador(1);" style="display: inline-block;">
							<option value="5">5</option>
							<option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
					</div>
				</div>    
			</div>
		</div>
	</div>
</div>
<!--Fin de Encabezado-->

<!--Inicio del Contenedor-->
<div style="overflow-x:auto;" id="resultadoBusqueda">
	<!-- Aqui se vera a tabla de resultados -->
</div>
<!--Fin del Contenedor-->

<!--Inicio modal de añadir persona -->
<div class="modal fade" id="mPersona" role="dialog">
	<div class="modal-dialog">
		<form  method="POST" id="form_persona" action="index.php?c=personas&a=Guardar" role="form" enctype="multipart/form-data">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong> <label id="labTitulo"></label></strong></h4>
				</div>
				<!-- Cuerpo -->
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" class="form-control" name="idCliente" id="txtIdCliente" value="">
						<input type="hidden" class="form-control" name="idUsuario" id="txtIdUsuario" value="">

					</div>
					<div class="form-group">
						<h5>Nombre Completo</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="fas fa-user" style="font-size: 18px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon2" name="nombrePersona" id="txtNomPersona" placeholder="Nombre Completo">
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
								<h5>Tipo de Teléfono</h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-8 col-sm-6" style="width: 100%">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="fas fa-bars" style="font-size: 19px;"></i></span>
									<select class="form-control" name="tipoTelefono" id="selectTipoTelefono">
										<option value="">Tipo de teléfono</option>
										<option value="Trabajo">Oficina</option>
										<option value="Casa">Casa</option>
										<option value="Celular">Celular</option>
										<option value="Otro">Otro</option>
									</select>
								</div>
							</div>                
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-5" style="padding-left: 17px;">
								<h5>Teléfono</h5>
							</div>
							<div class="col-xs-5 col-sm-5 col-md-5 col-lg-6" style="padding-left: 50px;">
								<h5>Extensión</h5>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 0px">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-contact-phone" style="font-size: 18px;"></i></span>
									<input type="text" class="form-control" aria-describedby="basic-addon2" name="telefono" id="txtTelefono" placeholder="Teléfono">
								</div>

							</div>
							<div class="col-xs-4 col-sm-6" style="width: 50%; padding-right: 15px">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-dialpad" style="font-size: 18px;"></i></span>
									<input type="text" class="form-control" aria-describedby="basic-addon2" name="extension" id="txtExtension" placeholder="Extensión">
								</div>

							</div>             
						</div>
					</div>

					<div class="form-group">
						<h5>Correo Electrónico</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon2"><i class="mdi mdi-email" style="font-size: 17px;"></i></span>
							<input type="text" class="form-control" aria-describedby="basic-addon2" name="email" id="txtEmail" placeholder="Correo Electrónico">
						</div>
					</div>
					<div class="form-group">
						<h5>Cliente</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-business" style="font-size: 18px;"></i></span>
							<select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="idOrganizacion" id="selectIdOrganizaciones">
							</select>
						</div>

					</div>
					<h5>Honorífico</h5>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-8 col-sm-6" style="width: 100%">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon2"><i class="fas fa-graduation-cap" style="font-size: 14px;"></i></span>
									<select class="form-control" name="honorifico" id="selectHonorifico">
										<option value="">Honorifico</option>
										<option value="Ing.">Ing.</option>
										<option value="Lic.">Lic.</option>
										<option value="Sr.">Sr.</option>
										<option value="Srta.">Srta.</option>
									</select>
								</div>
							</div>                
						</div>
					</div>

					<div class="form-group">
						<h5>Puesto</h5>
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fas fa-certificate" style="font-size: 18px;"></i></span>
							<input type="text" class="form-control" name="puesto" id="txtPuesto" placeholder="Puesto">
						</div>
					</div>
					<div class="form-group">
						<input type="hidden" name="propietario" id="txtPropietario" value="1">
					</div>
				</div>
				<!-- fin cuerpo --> 
				<div class="modal-footer">
					<div class="col-xs-3 col-lg-3" align="left">
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar Persona" id="btnEliminar" onclick="myFunctionEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
					</div>
					<div class="col-xs-9 col-lg-9" align="right">
						<input type="submit" class="btn btn-success" value="Guardar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
			</div>
		</form>
	</div>  
</div>    
<!--fin modal de añadir persona-->

<!--Inicio modal de confirmar para eliminar persona -->
<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" id="form_contactodel" action="index.php?c=personas&a=Eliminar" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar Persona</strong></h4>
				</div>
				<div class="modal-body">  
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar la Persona?</p>
					<input type="hidden" class="form-control" name="idCliente" id="txtIdClienteE" value="">
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-success" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal de confirmar para eliminar persona -->

<!-- Modal agenda telefonicas-->
<?php include './view/personas/modal/mAgenda.php'; ?>

<div>
	<?php if(isset($this->error)){ ?>
		<div class="alert alert-danger alert-bottom2" align="center">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<strong><?php echo $this->mensaje; ?></strong>
		</div>
	<?php }?>
</div>

<script src="./assets/js/contactos.js"></script>