$(document).ready(function() {

});

var table = $('table');

$('#cliente, #consecutivo, #servicio, #cotizacion, #descripcion, #equipo, #oportunidad, #rfq, #fecha_rfq, #usuario, #fecha_cotizacion, #pedido, #fecha_pedido, #fecha_realizacion, #fecha_reporte, #factura, #fecha_facturacion, #fecha_pago, #realizacion, #facturacion, #fecha_recibo')
.wrapInner('<span title="Ordenar esta columna"/>')
.each(function(){

	var th = $(this),
	thIndex = th.index(),
	inverse = false;

	th.click(function(){

		table.find('td').filter(function(){

			return $(this).index() === thIndex;

		}).sortElements(function(a, b){

			return $.text([a]) > $.text([b]) ?
			inverse ? -1 : 1
			: inverse ? 1 : -1;

		}, function(){

                    // parentNode is the element we want to move
                    return this.parentNode; 
                    
                });

		inverse = !inverse;

	});

});