$(document).ready(function() {
    pintarTabla();

    $('#form_sector').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
            }).done(function(respuesta){
                if (respuesta=="error") {
                    $("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al guardar el sector industrial</center></strong></div>');

                }else{
                    $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
                }if (respuesta =='') {
                    $('#mSector').modal('');
                }else{
                    $('#mSector').modal('toggle');
                    $('#form_sector').modal('hide');
                    $('#mensajejs').show();
                    $('#mensajejs').delay(3500).hide(600);
                    pintarTabla();
                }
            });
            return false;
        },
        fields: {
            claveSector: {
                validators: {
                    stringLength: {
                        min: 3,
                        max: 3,
                        message: 'Mínimo 3 - Máximo 3 letras'
                    },
                    regexp: {
                        regexp: /^([A-Z])*$/,
                        message: 'Sólo letras en mayusculas'
                    },
                    notEmpty: {
                        message: 'Campo Obligatorio'
                    }
                }
            },
            nombreSector: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z ÑñÁÉÍÓÚáéíóú])*$/,
                        message: 'Sólo letras'
                    },
                    notEmpty: {
                        message: 'Campo Obligatorio'
                    }
                }
            }
        }
    });
    $('#mSector')
    .on('shown.bs.modal', function() {
        $('#form_sector').find('[name="claveSector"]').focus();
    })
    .on('hidden.bs.modal', function () {
        $('#form_sector').bootstrapValidator('resetForm', true);
    });

    $('#formsectordel').submit(function(){
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
        }).done(function(respuesta){
            if (respuesta=="error") {
                $("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al eliminar el sector industrial</center></strong></div>');
            }else{
                $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
            }
            $('#mSector').modal('toggle');
            $('#mEliminar').modal('toggle');
            $('#mensajejs').show();
            $('#mensajejs').delay(3500).hide(600);
            pintarTabla();
        });
        return false;
    }); 
});

var bAlfabetica='';

/* Metodo de busqueda por ajax */
pintarTabla = function (){ 
    var bTexto = $("#buscar").val();
    $.post("index.php?c=sector&a=Consultas", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(resultado) {
        $("#resultadoBusqueda").html(resultado); 
    });
    $.post("index.php?c=sector&a=VerificaEx", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(resultado) {
        if(resultado == "false")
            $('#btnExportar').attr("disabled",false);               
        else
            $('#btnExportar').attr("disabled",true);   
    });
    
    $.post("index.php?c=sector&a=Contador", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(noEquipo) {
        if (noEquipo==1)
            $("#h5Contador").html(noEquipo+' Sector');
        else
            $("#h5Contador").html(noEquipo+' Sectores'); 
    })  
}


filtrarSector = function (busqueda){
    $('#txtbAlfabetica').val(busqueda);
    bAlfabetica = busqueda;
    switch(busqueda) {
        case 'A':
        desmarcar("a");
        break;
        case 'B':
        desmarcar("b");
        break;
        case 'C':
        desmarcar("c");
        break;
        case 'D':
        desmarcar("d");
        break;
        case 'E':
        desmarcar("e");
        break;
        case 'F':
        desmarcar("f");
        break;
        case 'G':
        desmarcar("g");
        break;
        case 'H':
        desmarcar("h");
        break;
        case 'I':
        desmarcar("i");
        break;
        case 'J':
        desmarcar("j");
        break;
        case 'K':
        desmarcar("k");
        break;
        case 'L':
        desmarcar("l");
        break;
        case 'M':
        desmarcar("m");
        break;
        case 'N':
        desmarcar("n");
        break;
        case 'Ñ':
        desmarcar("ñ");
        break;
        case 'O':
        desmarcar("o");
        break;
        case 'P':
        desmarcar("p");
        break;
        case 'Q':
        desmarcar("q");
        break;
        case 'R':
        desmarcar("r");
        break;
        case 'S':
        desmarcar("s");
        break;
        case 'T':
        desmarcar("t");
        break;
        case 'U':
        desmarcar("u");
        break;
        case 'V':
        desmarcar("v");
        break;
        case 'W':
        desmarcar("w");
        break;
        case 'X':
        desmarcar("x");
        break;
        case 'Y':
        desmarcar("y");
        break;
        case 'Z':
        desmarcar("z");
        break;
        default:
          //bAlfabetica='';
          desmarcar("todas");
          break;
      }
      pintarTabla();
  }

  myFunctionEditar = function (idSector, claveSector, nombreSector) {
    $('#idSector').val(idSector);  
    $('#claveSector').val(claveSector);
    $('#nombreSector').val(nombreSector);
    $('#btnEliminar').show();
    $('#labTitulo').html("Editar Sector");
}

function desmarcar(letra){
    var miarray = ["todas", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

    for (var i = 0; i < miarray.length; i++) {
        if (letra == miarray[i]) {
            $("#btn-"+miarray[i]).addClass("btn-primary");
            $("#btn-"+miarray[i]).removeClass("btn-default");
        }else{
            $("#btn-"+miarray[i]).removeClass("btn-primary");
            $("#btn-"+miarray[i]).addClass("btn-default");
        }
    }
}
$("#claveSector").keyup(function(){
    $('#alertaclave').hide();
    var claveSector = $('#claveSector').val();
    var idSector = $('#idSector').val();
    $.post("index.php?c=sector&a=VerificarClave", {claveSector: claveSector,idSector: idSector}, function(respuesta) {
        if (respuesta==0){
            $( "#btnGuardar" ).prop( "disabled", false );
        }else{
            $('#alertaclave').show();
            $("#alertaclave").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Esta clave ya existe </p>');
            $( "#btnGuardar" ).prop( "disabled", true );
        }
    })
});

function myFunctionEliminar(){
    var idSector = $('#idSector').val();
    $('#idSectorE').val(idSector);  
}
function myFunctionNuevo() {
    $('#idSector').val("");
    $('#claveSector').val("");
    $('#nombreSector').val("");
    $('#btnEliminar').hide();
    $('#labTitulo').html("Añadir Sector");
}