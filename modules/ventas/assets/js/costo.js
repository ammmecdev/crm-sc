$(document).ready(function() {
	var registro="";
	filtrarCosto();

	$('#form_crear').bootstrapValidator({
		/*feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},*/
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					if(respuesta[0].respuesta == "ok"){
						$('#mSolicitud').modal('toggle');
						var mensajeHTML = `<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button>
						<strong><center>Se ha enviado correctamente la solicitud al departamento de operaciones</center></strong>
						</div>`;
						$("#mensajejs").html(mensajeHTML);
						$('#mensajejs').show();
						$('#mensajejs').delay(3500).hide(600);
						mandarNotificacion(registro);
						consultasCO_Ven();
					}else{
						$('#txtIdCostoOperativo').val(respuesta[0].respuesta);
						$('#divCrear').hide();	
						$('#divCarrito').show();
						$('#divEnviar').show();
						limpiarInputs();
						$.post("index.php?c=costo&a=MostrarCarrito", {idCostoOperativo: respuesta[0].respuesta}, function(tabla_carrito) {
							$('#tabla_carrito').html(tabla_carrito);
						});
						ValidarContenidoCarrito(respuesta[0].respuesta);
						consultasCO_Ven(); 
					}
				}
			})
			return false;
		},
		fields: {
			fechaEmision: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			fechaEntrega: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			requisicion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			idNegocio: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			idContacto: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			clave: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			unidad: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			cantidad: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			descripcion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
		}
	});
	$('#mSolicitud')
	.on('shown.bs.modal', function() {
		$('#form_crear').find('[name="fechaEmision"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#verificador').text("");
		$('#form_crear').bootstrapValidator('resetForm', true);
	});
});

consultasCO_Ven = function ()
{
	var btexto = $('#txtBuscar').val();
	if (bPeriodo==''){
		$.post("index.php?c=costo&a=Consultas", {btexto : btexto, accion: 'tabla'}, function(mensaje) {
			$("#resultadoBusqueda").html(mensaje);
		}); 
	}else{
		$.post("index.php?c=costo&a=Consultas", {btexto : btexto, bPeriodo: bPeriodo, accion: 'tabla'}, function(mensaje) {
			$("#resultadoBusqueda").html(mensaje);
		}); 
	}
}

solicitudNueva = function()
{
	registro = "nuevo";
	$('#txtIdCostoOperativo').val('');
	$('#divCrear').show();
	$('#divCarrito').hide();
	$('#divEnviar').hide();
	$('#txtFechaEmision').val('');
	$('#txtFechaEntrega').val('');
	$('#txtIdCostoOperativo').val('');
	$('#txtRequisicion').val('');
	$('#txtCliente').val('');	
	$("#selectContacto").empty();
	var selector = document.getElementById("selectContacto");
	selector.options[0] = new Option("Seleccione la persona de contacto","");
	$('#selectContacto').selectpicker('refresh');
	$('#txtProyecto').val('');
	listarNegocio();
}

agregarServicio = function()
{
	var idSerCO = $('#txtIdSerCO').val();
	var idCostoOperativo = $('#txtIdCostoOperativo').val();
	var clave = $('#txtClave').val();
	var descripcion = $('#txtNotas').val();
	var unidad = $('#txtUnidad').val();
	var cantidad = $('#txtCantidad').val();
	var precioUnitario = $('#txtPrecioUnitario').val();
	if(clave!="" && descripcion!="" && unidad!="" && cantidad != ""){
		$('#verificador').text("");
		datos = {
			"idSerCO" : idSerCO,
			"idCostoOperativo" : idCostoOperativo,
			"clave" : clave,
			"descripcion" : descripcion,
			"unidad" : unidad,
			"cantidad" : cantidad,
			"precioUnitario" : precioUnitario
		};
		$.ajax({
			type: 'POST',
			url: '?c=costo&a=AgregarServicio',
			data: datos,
		}).done(function(tabla_carrito){
			$('#tabla_carrito').html(tabla_carrito);
			limpiarInputs();
			ValidarContenidoCarrito(idCostoOperativo);
		});
	}else{
		$('#verificador').text("Verifique que la clave, unidad, cantidad y descripción esten llenos");
	}

} 

editarServicio = function(idSerCO)
{
	dato = {
		"idSerCO" : idSerCO,
	};
	$.ajax({
		type: 'POST',
		url: '?c=costo&a=EditarServicio',
		data: dato,
	}).done(function(servicio){
		limpiarInputs();
		$('#txtIdSerCO').val(servicio[0].idSerCO);
		$('#txtClave').val(servicio[0].clave);
		$('#txtNotas').val(servicio[0].descripcion);
		$('#txtUnidad').val(servicio[0].unidad);
		$('#txtCantidad').val(servicio[0].cantidad);
		$('#txtPrecioUnitario').val(servicio[0].precioUnitario);
	});
} 

quitarServicio = function(idSerCO)
{
	var idCostoOperativo = $('#txtIdCostoOperativo').val();
	datos = {
		"idSerCO" : idSerCO,
		"idCostoOperativo" : idCostoOperativo
	};
	$.ajax({
		type: 'POST',
		url: '?c=costo&a=QuitarServicio',
		data: datos,
	}).done(function(tabla_carrito){
		$('#tabla_carrito').html(tabla_carrito);
	});
	ValidarContenidoCarrito(idCostoOperativo);
} 

limpiarInputs = function (){
	$('#txtIdSerCO').val(0);
	$('#txtClave').val("");
	$('#txtNotas').val("");
	$('#txtCantidad').val("");
	$('#txtClave').focus();
}

consultarPorCuenta = function()
{
	idNegocio = $('#selectCuenta').val();
	$.post("index.php?c=costo&a=ConsultarProyectoClientePorNegocio", {idNegocio: idNegocio}, function(mensaje) {
		$("#txtCliente").val(mensaje[0].organizacion);
		$("#txtProyecto").val(mensaje[0].tituloNegocio);
	});
	listarPersonasPorNegocio(idNegocio);
}

listarPersonasPorNegocio = function (idNegocio)
{
	datos = {"idNegocio":idNegocio};
	$.ajax({
		url: "index.php?c=costo&a=ListarPersonasPorNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectContacto").empty();
		var selector = document.getElementById("selectContacto");
		selector.options[0] = new Option("Seleccione la persona de contacto","");
		j=0;
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
		}
		$('#selectContacto').selectpicker('refresh');
	});
}

listarNegocio = function ()
{
	$.ajax({
		url: "index.php?c=costo&a=listarNegocio",
		type: "POST"
	}).done(function(respuesta){
		$("#selectCuenta").empty();
		var selector = document.getElementById("selectCuenta");
		selector.options[0] = new Option("Seleccione la cuenta","");
		j=0;
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].claveOrganizacion+"-"+respuesta[i].claveConsecutivo+"-"+respuesta[i].claveServicio,respuesta[i].idNegocio);
		}
		$('#selectCuenta').selectpicker('refresh');
	});
}

var bPeriodo = '';

mandarNotificacion = function ()
{
	idCostoOperativo = $('#txtIdCostoOperativo').val();
	data = {"idCostoOperativo" : idCostoOperativo}
	$.ajax({
		type: "POST",
		url: "?c=costo&a=Notificacion",
		dataType:"html",
		data: data,
		success: function(data) 
		{
			console.log(data);
			var json = eval("(" + data + ")");
      // insco : informacion para notificacion de solicitud de costo operativo
      socket.emit( 'insco', { 
      	idNotificacion : json.idNotificacion,
      	usuarioEmisor : json.usuarioEmisor,
      	moduloEmisor : json.moduloEmisor,
      	timestamp : json.timestamp,
      	ruta : json.ruta,
      	evento : json.evento,
      	idCostoOperativo : json.idCostoOperativo,
      	fotoUsuario : json.fotoUsuario,
      	registro : registro
      });
  }
});
}

filtrarCosto = function (busqueda)
{
	switch(busqueda) {
		case '1':
		if(bPeriodo != 1){
			bPeriodo = 1;
			defaultClassPeriodos();
			$("#btnPendiente").removeClass("btn-default");
			$("#btnPendiente").addClass("btn-primary");
		}
		break;
		case '2':
		if(bPeriodo != 2){
			bPeriodo = 2;
			defaultClassPeriodos();
			$("#btnRechazado").removeClass("btn-default"); 
			$("#btnRechazado").addClass("btn-primary"); 
		}
		break;
		case '3':
		if(bPeriodo != 3){
			bPeriodo = 3; 
			defaultClassPeriodos();
			$("#btnAprobado").removeClass("btn-default");  
			$("#btnAprobado").addClass("btn-primary");  
		}
		break;
		case '4':
		if(bPeriodo != 4){
			bPeriodo = 4; 
			defaultClassPeriodos();
			$("#btnTodos").removeClass("btn-default");  
			$("#btnTodos").addClass("btn-primary");  
		}
		break;
		default:
		bPeriodo=1;
		defaultClassPeriodos();
		$("#btnPendiente").removeClass("btn-default");  
		$("#btnPendiente").addClass("btn-primary");  
		break;
	}
	consultasCO_Ven();
}

defaultClassPeriodos=function()
{
	$("#btnPendiente").removeClass("btn-primary");
	$("#btnRechazado").removeClass("btn-primary");  
	$("#btnTodos").removeClass("btn-primary"); 
	$("#btnAprobado").removeClass("btn-primary"); 

	$("#btnPendiente").addClass("btn-default");
	$("#btnRechazado").addClass("btn-default"); 
	$("#btnTodos").addClass("btn-default"); 
	$("#btnAprobado").addClass("btn-default"); 
}

EditarCostoOperativo = function(array)
{
	var array = array.split(",");
	var idCostoOperativo = array[0];
	var fechaEmision = array[1];
	var fechaEntrega = array[2];
	var requisicion = array[3];
	var idNegocio = array[4];
	var idContacto = array[5];
	var tituloNegocio = array[6];
	var claveOrganizacion = array[7];
	var claveConsecutivo = array[8];
	var claveServicio = array[9];
	var nombrePersona = array[10];
	var nombreOrganizacion = array[11];
	var estado = array[12];
	registro="actualizar";

	$('#txtIdCostoOperativo').val(idCostoOperativo);
	$('#txtFechaEmision').val(fechaEmision);  
	$('#txtFechaEntrega').val(fechaEntrega);
	$('#txtRequisicion').val(requisicion);
	$('#txtProyecto').val(tituloNegocio);
	$('#txtCliente').val(nombreOrganizacion);
	$('#divCrear').hide();
	$('#divCarrito').show();
	$('#divEnviar').show();

	if (estado == 'Rechazado') 
		$('#btnEliminar').hide();
	else
		$('#btnEliminar').show();

	$.post("index.php?c=costo&a=MostrarCarrito", {idCostoOperativo: idCostoOperativo}, function(tabla_carrito) {
		$('#tabla_carrito').html(tabla_carrito);
	});

	$.ajax({
		url: "index.php?c=costo&a=ListarNegocio",
		type: "POST"
	}).done(function(respuesta){
		$("#selectCuenta").empty();
		var selectorCuenta = document.getElementById("selectCuenta");
		selectorCuenta.options[0] = new Option(claveOrganizacion+"-"+claveConsecutivo+"-"+claveServicio,idNegocio);
		j=0;
		for (var i in respuesta) {
			if(respuesta[i].idNegocio != idNegocio){
				j=j+1;
				selectorCuenta.options[j] = new Option(respuesta[i].claveOrganizacion+"-"+respuesta[i].claveConsecutivo+"-"+respuesta[i].claveServicio,respuesta[i].idNegocio);
			}
		}
		$('#selectCuenta').selectpicker('refresh');
	});

	datos = {"idNegocio":idNegocio};
	$.ajax({
		url: "index.php?c=costo&a=ListarPersonasPorNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		console.log(JSON.stringify(respuesta));
		$("#selectContacto").empty();
		$('#selectContacto').selectpicker('refresh');
		var selectorContacto = document.getElementById("selectContacto");
		selectorContacto.options[0] = new Option(nombrePersona,idContacto);
		j=0;
		for (var i in respuesta) {
			if (respuesta[i].idCliente != idContacto) {
				var j=j+1;
				selectorContacto.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].idCliente);
			}
		}
		$('#selectContacto').selectpicker('refresh');
	});

	ValidarContenidoCarrito(idCostoOperativo);
}

function EliminarCostoOperativo()
{
	var idCostoOperativo = $('#txtIdCostoOperativo').val();
	$('#idCostoOperativoE').val(idCostoOperativo);  
}

function ValidarContenidoCarrito(idCostoOperativo) {
	$.post("index.php?c=costo&a=CheckServicio", {idCostoOperativo: idCostoOperativo}, function(respuesta) {
		if (respuesta[0].resultado == "false") {
			$('#btnEnviarSolicitud').attr("disabled", true);
		}else{
			$('#btnEnviarSolicitud').attr("disabled", false);
		}
	});
}

exportarPdf = function(idCostoOpe)
{
	location.href='?c=costo&a=Exportar&idCostoOperativo='+idCostoOpe;
}
