const formatter = new Intl.NumberFormat('en-US', {
	minimumFractionDigits: 2
  })

$(document).ready(function() {

	$('#form_cotizacion').bootstrapValidator({
		/*feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},*/
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					respuesta = JSON.parse(respuesta);
					
					$('#mCotizacion').modal('toggle');
					$('#form_cotizacion').trigger("reset");
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					consultas();
					
				}
			});      
			return false;
		},
		fields: {
			claveCotizacion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			idCostoOperativo: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			fecha: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
			

		}
	});

	$('#form-eliminar').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$('#mCotizacionEliminar').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consultas();
		});
		return false;
	}); 

	$('#form-cambiar-estado').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$('#mCotizacionCambiarEstado').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consultas();
		});
		return false;
	}); 

	filtrarCosto();
	consultaCostoOperativo();

	
	$('#mCotizacion')
	.on('shown.bs.modal', function() {
		//$('#form_cotizacion').find('[name="claveCotizacion"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$("#complementos").removeClass("in");
		
		$("#selectIdCostoOperativo").val("");
		$("#selectIdCostoOperativo").selectpicker('refresh');

		$('#tablaServicios').html("");

		$('#form_cotizacion').trigger("reset");
		$('#form_cotizacion').bootstrapValidator('resetForm', true);
	});
});

consultaCostoOperativo = function()
{ //Metodo para listar la cuenta (CO) en el modal nueva cotizacion
	$.ajax({
		url: "index.php?c=cotizacion&a=listarCostoOperativo",
		type: "POST"
	}).done(function(respuesta){
		$("#selectIdCostoOperativo").empty();
		var selector = document.getElementById("selectIdCostoOperativo");
		selector.options[0] = new Option("Seleccione la cuenta","");
		j=0;
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].claveNegocio, respuesta[i].idCostoOperativo);
		}
		$('#selectIdCostoOperativo').selectpicker('refresh');
	});
}

obtenerDatosCostoOperativo = function()
{ // Metodo para obtener los datos del CO seleccionado
	var idCostoOperativo = $('#selectIdCostoOperativo').val();
	if (idCostoOperativo != "") {
		datos = {"idCostoOperativo": idCostoOperativo};
		$.ajax({
			url: "index.php?c=cotizacion&a=datosCostoOperativo",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			console.log(respuesta);
			$('#txtProyecto').val(respuesta[0].tituloNegocio);
			$('#txtIdOrganizacion').val(respuesta[0].nombreOrganizacion);
			$('#txtRequisicion').val(respuesta[0].requisicion);
			$('#txtAtencion').val(respuesta[0].nombreUsuario);
		});
		obtenerServicios(idCostoOperativo);
	}else{
		limpiarImput("select");
	}
}

obtenerServicios = function(idCostoOperativo, subT = null, iva = null, total = null)
{ //Metodo para crear la tabla de servicios según el CO seleccionado
	$.post("index.php?c=cotizacion&a=crearTablaServicios", {idCostoOperativo : idCostoOperativo}, function(resultado) {
		
		resultado = JSON.parse(resultado);
		var subTotal = 0;
		var tabla = '<table class="table table-bordered">'+
		'<thead>'+
		'<tr style="background-color: #EEEEEE;">'+
		'<td align="center"><strong>Clave</strong></td>'+
		'<td align="center"><strong>Descripción</strong></td>'+
		'<td align="center"><strong>Unidad</strong></td>'+
		'<td align="center"><strong>Cantidad</strong></td>'+
		'<td align="center"><strong>Precio Unitario</strong></td>'+
		'<td align="center"><strong>Importe</strong></td>'+
		'</tr>'+
		'</thead>'+
		'<tbody>';
		for(let i in resultado){
			tabla += '<tr>';
			tabla += '<td>' + resultado[i].clave + '</td>';
			tabla += '<td>' + resultado[i].descripcion + '</td>';
			tabla += '<td align="center">' + resultado[i].unidad + '</td>';
			tabla += '<td align="center">' + resultado[i].cantidad + '</td>';
			tabla += '<td align="center">' + resultado[i].precioUnitario + '</td>';
			tabla += '<td align="center">' + resultado[i].importe + '</td>';
			tabla += '</tr>';
			subTotal +=  + resultado[i].importe;
		}
		tabla += '<tr>'+
		'<td colspan="5" align="right" style="background-color: #EEEEEE;">'+
		'<small><strong>Subtotal M.N.</strong></small>'+
		'</td>';
		if(subT!=null){
			tabla += '<td align="center" style="background-color: #E0E0E0;"><strong>$ <input type="text" class="lectura" id="txtSubTotal"  name="subTotal"  readonly value="'+formatter.format(subT)+'"></strong></td>';
		}else{
			tabla += '<td align="center" style="background-color: #E0E0E0;"><strong>$ <input type="text" class="lectura" id="txtSubTotal"  name="subTotal"  readonly value="'+formatter.format(subTotal)+'"></strong></td>';
		}
		tabla +='</tr>'+
		'<tr>'+
		'<td colspan="5" align="right" style="background-color: #EEEEEE;">'+
		'<small><strong>IVA M.N.</strong></small>'+
		'</td>';
		if(iva!=null){
			tabla +='<td align="center" style="background-color: #E0E0E0;"><strong>$ <input type="text" class="lectura" name="totalIva" id="txtTotalIva" readonly value="'+formatter.format(iva)+'"></strong></td>';
		}else{
			tabla +='<td align="center" style="background-color: #E0E0E0;"><strong>$ <input type="text" class="lectura" name="totalIva" id="txtTotalIva" readonly value="'+formatter.format(0)+'"></strong></td>';
		}
		tabla+='</tr>'+
		'<tr>'+
		'<td colspan="5" align="right" style="background-color: #EEEEEE;">'+
		'<small><strong>Total M.N.</strong></small>'+
		'</td>';
		if(total!=null){
		   tabla+='<td align="center" style="background-color: #E0E0E0;"><strong>$ <input type="text" class="lectura" id="total" name="total" 	readonly value="'+formatter.format(total)+'"></strong></td>';
		}else{
			tabla+='<td align="center" style="background-color: #E0E0E0;"><strong>$ <input type="text" class="lectura" id="total" name="total" 	readonly value="'+formatter.format(subTotal)+'"></strong></td>';
		}
		tabla += '</tr>'+
		'</tbody>'+
		'</table>';
		
		$("#tablaServicios").html(tabla);
	});
}

limpiarImput = function(opcion)
{
	if(opcion == "select"){
		$('#txtProyecto').val("");
		$('#txtIdOrganizacion').val("");
		$('#txtRequisicion').val("");
		$('#txtAtencion').val("");
	}else if (opcion == "Nueva") {
		$('#txtProyecto').val("");
		$('#txtIdOrganizacion').val("");
		$('#txtRequisicion').val("");
		$('#txtClaveCotizacion').val("");
		$('#txtFecha').val("");
		$('#txtDireccion').val("");
		$('#txtLocalidad').val("");
		$('#txtEstado').val("");
		$('#txtAtencion').val("");
	}
}

consultas = function ()
{
	var btexto = $('#txtBuscar').val();
		$.post("index.php?c=cotizacion&a=Consultas", {btexto : btexto, bPeriodo: bPeriodo, accion: 'tabla'}, function(resultado) {
			
			resultado = JSON.parse(resultado);
			var tabla = '<table class="table table-bordered" style="margin-bottom: 0px;">'+
			'<thead>'+
			'<tr align="center" style="background-color: #FCF3CF;">'+
					'<td><strong>Acciones</strong></td>'+
			'<td><strong>Clave cotización</strong></td>'+
			'<td><strong>Cuenta</strong></td>'+
			'<td><strong>Fecha</strong></td>'+
			'<td><strong>Requisición</strong></td>'+
			'<td><strong>Atención</strong></td>'+
			'<td><strong>Total</strong></td>'+
			'<td><strong>Estado</strong></td>'+
			'</tr>'+
			'</thead>'+
			'<tbody>';
			if(resultado.length==0){
				tabla += '<tr><td class="alert-danger" colspan="10" align="center"> <strong>No se encontraron cotizaciones </strong></td></tr>';
			}else{
				for(let i in resultado){
					var cotizacion = JSON.stringify(resultado[i]);
					cotizacion = cotizacion.replace(/\"/g,"'");
					tabla+='<tr>'+
					'<td align="center" style="white-space: nowrap;">'+
					'<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mCotizacion" onclick="editarCotizacion('+cotizacion+')" data-toggle="tooltip" title="Editar cotización"><span class="glyphicon glyphicon-pencil"></span></button> '+
					'<button   class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mCotizacionEliminar" onclick="rellenarCampoID('+resultado[i].idCotizacion+')"><i class="glyphicon glyphicon-trash"></i> </button> '+
					'<button   class="btn btn-info btn-xs" data-toggle="modal" data-target="#mCotizacionCambiarEstado" onclick="rellenarCampoIDEstado('+resultado[i].idCotizacion+')"><i class="glyphicon glyphicon-refresh"></i> </button> ';
					if(resultado[i].estado == 'Aprobado'){
						tabla +='<a  class="btn btn-success btn-xs" href="?c=reportes&a=ExportarCotizacion&idCotizacion='+resultado[i].idCotizacion+'&idCostoOperativo='+resultado[i].idCostoOperativo+'" target="_blank"><i class="fas fa-print"></i> </a> ';
					}
					tabla +='</td>';
					tabla+='<td>'+resultado[i].claveCotizacion+'</td>';
					tabla+='<td>'+resultado[i].cuenta+'</td>';
					tabla+='<td>'+resultado[i].fechaCotizacion+'</td>';
					tabla+='<td>'+resultado[i].requisicion+'</td>';
					tabla+='<td>'+resultado[i].nombreUsuario+'</td>';
					tabla+='<td>'+resultado[i].total+'</td>';
					switch(resultado[i].estado){
						case 'Aprobado':
							tabla+='<td align="center" style="background-color: #A2D9CE"> <strong> Aprobado </strong></td>';
							break;
						case 'Rechazado':
							tabla+='<td align="center" style="background-color: #E6B0AA"> <strong> Rechazado </strong> </td>';
							break;
						case 'Pendiente':
							tabla+='<td align="center" style="background-color: #F9E79F"> <strong> Pendiente </strong> </td>';
							break;
					}
					tabla+='</tr>';
				}
			}
			tabla+='</tbody></table>';

			$("#resultadoBusqueda").html(tabla);
			
		}); 
	
}

function rellenarCampoID(idCotizacion){
	$('#idCotizacionEliminar').val(idCotizacion);
}

function rellenarCampoIDEstado(idCotizacion){
	$('#idCotizacionCambiarEstado').val(idCotizacion);
}

function editarCotizacion(cotizacion){
	$("#txtIdCotizacion").val(cotizacion.idCotizacion);
	$("#selectIdCostoOperativo").val(cotizacion.idCostoOperativo);
	$("#selectIdCostoOperativo").selectpicker('refresh');
	obtenerDatosCostoOperativo();

	$("#txtClaveCotizacion").val(cotizacion.claveCotizacion);

	

	$("#txtFecha").val(cotizacion.fechaCotizacion);

	$("#txtDireccion").val(cotizacion.direccion);

	$("#txtLocalidad").val(cotizacion.localidad);

	if(cotizacion.iva!=0){
		$("#txtIva").val(cotizacion.iva);
	}

	if(cotizacion.factor!=0){
		$("#txtFactor").val(cotizacion.factor);
	}

	totalIva =  (parseFloat(cotizacion.iva)/100) * parseFloat(cotizacion.subtotal);

	obtenerServicios(cotizacion.idCostoOperativo,cotizacion.subtotal,totalIva,cotizacion.total);

	
}


var bPeriodo = '';

filtrarCosto = function (busqueda)
{
	switch(busqueda) {
		case '1':
		if(bPeriodo != 1){
			bPeriodo = 1;
			defaultClassPeriodos();
			$("#btnPendiente").removeClass("btn-default");
			$("#btnPendiente").addClass("btn-primary");
		}
		break;
		case '2':
		if(bPeriodo != 2){
			bPeriodo = 2;
			defaultClassPeriodos();
			$("#btnRechazado").removeClass("btn-default"); 
			$("#btnRechazado").addClass("btn-primary"); 
		}
		break;
		case '3':
		if(bPeriodo != 3){
			bPeriodo = 3; 
			defaultClassPeriodos();
			$("#btnAprobado").removeClass("btn-default");  
			$("#btnAprobado").addClass("btn-primary");  
		}
		break;
		case '4':
		if(bPeriodo != 4){
			bPeriodo = 4; 
			defaultClassPeriodos();
			$("#btnTodos").removeClass("btn-default");  
			$("#btnTodos").addClass("btn-primary");  
		}
		break;
		default:
		bPeriodo=1;
		defaultClassPeriodos();
		$("#btnPendiente").removeClass("btn-default");  
		$("#btnPendiente").addClass("btn-primary");  
		break;
	}
	consultas();
}

defaultClassPeriodos=function()
{
	$("#btnPendiente").removeClass("btn-primary");
	$("#btnRechazado").removeClass("btn-primary");  
	$("#btnTodos").removeClass("btn-primary"); 
	$("#btnAprobado").removeClass("btn-primary"); 

	$("#btnPendiente").addClass("btn-default");
	$("#btnRechazado").addClass("btn-default"); 
	$("#btnTodos").addClass("btn-default"); 
	$("#btnAprobado").addClass("btn-default"); 
}
exportarPdf = function(idCotiza)
{
	location.href='?c=cotizacion&a=Exportar&idCotizacion='+idCotiza;
}

$("#txtIva").keyup(function() {
	var subTotal = $('#txtSubTotal').val(); 
	var iva = $('#txtIva').val();
	var factor = $('#txtFactor').val();
	// // var val2 = fNumber.go(iva, "$");
	var totalIva = parseFloat(subTotal) * (iva/100);
		
	var val = parseFloat(totalIva);
	var total = parseFloat(totalIva) + parseFloat(subTotal);
	$('#txtTotalIva').val(formatter.format(val));
	if(factor!=""){
		$('#total').val(formatter.format(total * factor));
	}else{
		$('#total').val(formatter.format(total));
	}
	
});

$("#txtFactor").keyup(function() {
	var subTotal = $('#txtSubTotal').val(); 
	var iva = $('#txtIva').val();
	var factor = $('#txtFactor').val();
	// // var val2 = fNumber.go(iva, "$");
	var totalIva = parseFloat(subTotal) * (iva/100);
	var val = parseFloat(totalIva);
	var total = parseFloat(totalIva) + parseFloat(subTotal);
	$('#txtTotalIva').val(formatter.format(val));
	if(factor!=""){
		$('#total').val(formatter.format(total * factor));
	}else{
		$('#total').val(formatter.format(total));
	}
	
	
});

var fNumber = {
sepMil: ",", // separador para los miles
sepDec: '.', // separador para los decimales
formatear:function (num){
	num +='';
	var splitStr = num.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? this.sepDec + splitStr[1] : '';
	var regx = /(\d+)(\d{3})/;
	while (regx.test(splitLeft)) {
		splitLeft = splitLeft.replace(regx, '$1' + this.sepMil + '$2');
	}
	return this.simbol + splitLeft + splitRight;
},
go:function(num, simbol){
	this.simbol = simbol ||'';
	// alert(this.formatear(num));
	return this.formatear(num);
}
}

// var placeSearch, autocomplete;
// var componentForm = {
// 	street_number: 'short_name',
// 	route: 'long_name',
// 	locality: 'long_name',
// 	administrative_area_level_1: 'short_name',
// 	country: 'long_name',
// 	postal_code: 'short_name'
// };

// function initAutocomplete() {
// 	/*Create the autocomplete object, restricting the search to geographical
// 	location types.*/
// 	autocomplete = new google.maps.places.Autocomplete(
// 		/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
// 		{types: ['geocode']});

// 	/* When the user selects an address from the dropdown, populate the address
// 	fields in the form. */
// 	autocomplete.addListener('place_changed', fillInAddress);
// }

// function fillInAddress() {
// 	/* Get the place details from the autocomplete object. */
// 	var place = autocomplete.getPlace();

// 	for (var component in componentForm) {
// 		document.getElementById(component).value = '';
// 		document.getElementById(component).disabled = false;
// 	}

//         /* Get each component of the address from the place details
//         and fill the corresponding field on the form. */
//         for (var i = 0; i < place.address_components.length; i++) {
//         	var addressType = place.address_components[i].types[0];
//         	if (componentForm[addressType]) {
//         		var val = place.address_components[i][componentForm[addressType]];
//         		document.getElementById(addressType).value = val;
//         	}
//         }
//     }

//     function geolocate() {
//     	if (navigator.geolocation) {
//     		navigator.geolocation.getCurrentPosition(function(position) {
//     			var geolocation = {
//     				lat: position.coords.latitude,
//     				lng: position.coords.longitude
//     			};
//     			var circle = new google.maps.Circle({
//     				center: geolocation,
//     				radius: position.coords.accuracy
//     			});
//     			autocomplete.setBounds(circle.getBounds());
//     		});
//     	}
//     }