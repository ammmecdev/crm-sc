$(document).ready(function() {
	


	$('#form_orden').bootstrapValidator({
		/*feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},*/
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					respuesta = JSON.parse(respuesta);
					
					$('#mOrden').modal('toggle');
					$('#form_orden').trigger("reset");
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					consultas();
					
				}
			});      
			return false;
		},
		fields: {
			selectCuenta: {
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			numeroPedido: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			fechaRealizacion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},

		}
	});
	rellenarFormulario();
	verOrdenes();
	consultas();

	$('#form-eliminar').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$('#mOrdenEliminar').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consultas();
		});
		return false;
	}); 
	

	$('#mOrden')
	.on('shown.bs.modal', function() {
		/*$('#form_orden').find('[name="claveCotizacion"]').focus();*/
	})
	.on('hidden.bs.modal', function () {
		$("#selectContacto").val("");
		$("#selectContacto").selectpicker('refresh');
		$("#selectCuenta").val("");
		$("#selectCuenta").selectpicker('refresh');
		$('#idOrdenTrabajo').val(0);
		$('#form_orden').trigger("reset");
		
		$('#form_orden').bootstrapValidator('resetForm', true);
	});
});

function nuevaOrden(){
	rellenarFormulario();
}

function verOrdenes(){
	$("#btnOrdenes").addClass("active");
	$("#btnReportes").removeClass("active");
	$("#tabla-reportes").hide();
	$("#tabla-ordenes").show();
	switchColor('A');
}

function verReportes(){
	$("#btnReportes").addClass("active");
	$("#btnOrdenes").removeClass("active");
	$("#tabla-ordenes").hide();
	$("#tabla-reportes").show();
	switchColor('B');
}

function switchColor(option){
	switch(option) {
		case 'A':
		$("#btnOrdenes").addClass("btn-primary");
		$("#btnOrdenes").removeClass("btn-default");

		$("#btnReportes").removeClass("btn-primary");
		$("#btnReportes").addClass("btn-default");
		break;
		case 'B':
		$("#btnReportes").addClass("btn-primary");
		$("#btnReportes").removeClass("btn-default");

		$("#btnOrdenes").removeClass("btn-primary");
		$("#btnOrdenes").addClass("btn-default");
		default:
		break;
	}
}

function rellenarFormulario(){
	var f = new Date();
	$('#fechaValor').text(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
	$.post("index.php?c=personas&a=ListarPersonas",{}, function(resultado){
		$('#selectContacto').empty();

		var selector = document.getElementById("selectContacto");
		selector.options[0] = new Option("Seleccione el Usuario","");

		for(let i in resultado){

			var option = new Option(resultado[i].nombrePersona,resultado[i].idCliente);
			
			$(option).html(resultado[i].nombrePersona);
			$("#selectContacto").append(option);
			$('#selectContacto option:not(:selected)').attr('disabled',true);
		}

		$("#selectContacto").selectpicker('refresh');

	});

	$.post("index.php?c=cotizacion&a=listarNegocioConCotizacion",{},function(resultado){
		$('#selectCuenta').empty();
		var selector = document.getElementById("selectCuenta");
		selector.options[0] = new Option("Seleccione la cuenta","");
		for(let i in resultado){

			var option = new Option(resultado[i].claveOrganizacion+" "+resultado[i].claveConsecutivo+" "+resultado[i].claveServicio,resultado[i].idNegocio);
			
			$(option).html(resultado[i].claveOrganizacion+" "+resultado[i].claveConsecutivo+" "+resultado[i].claveServicio);
			$(option).prop( "disabled", false );
			$("#selectCuenta").append(option);

		}
		
		$("#selectCuenta").selectpicker('refresh');
		$("#selectCuenta").on('change',function() {

			$.post("index.php?c=cotizacion&a=listarNegocioConCotizacion",{idNegocio:$( "#selectCuenta option:selected" ).val()}, function(resultado){
				
				$("#idCotizacion").val(resultado[0].idCotizacion);
				
				$("#tituloNegocio").val(resultado[0].tituloNegocio);

				$("#selectContacto").val(resultado[0].idCliente);
				$('#selectContacto option:not(:selected)').attr('disabled',true);
				$('#selectContacto option:selected').attr('disabled',false);
				$("#selectContacto").selectpicker('refresh');

				$("#fechaEntrega").val(resultado[0].fechaEntrega);
				
			});
		});
	});
		
}

function consultas(){
	var bTexto = $("#vBusqueda").val();
	$.post("index.php?c=orden&a=Consultas",{accion:'tabla', bTexto: bTexto}, function (resultado)  {
		resultado = JSON.parse(resultado);
		var tabla = '<div class="porlets-content">'+
		'<div class="table-responsive">'+
		'<table class="table table-bordered" id="tabla-ordenes" style="margin-bottom: 0px;">'+
		'<thead>'+
			'<tr style="background-color: #FCF3CF;">'+
				'<td align="center"><strong>Acciones</strong></td>'+
				'<td align="center"><strong>Cuenta</strong></td>'+
				'<td align="center"><strong>Fecha de Entrega</strong></td>'+
				'<td align="center"><strong>Proyecto</strong></td>'+
				'<td align="center"><strong>No. Pedido / O.C.</strong></td>'+
				'<td align="center"><strong>Nombre de Usuario</strong></td>'+
				'<td align="center"><strong>Tiempo de Entrega</strong></td>'+
				'<td align="center"><strong>Fecha de Realización</strong></td>'+
				'<td align="center"><strong>Estado</strong></td>'+
			//	'<td align="center"><strong>Descripción</strong></td>'+
			//	'<td align="center"><strong>Observaciones</strong></td>'+
			'</tr>'+
		'</thead>'+
		'<tbody>';

		if(resultado.length==0){
			tabla += '<tr><td class="alert-danger" colspan="10" align="center"> <strong>No se encontraron Ordenes de trabajo </strong></td></tr>';
		}else{
			for(let i in resultado){
				var ordenTrabajo = JSON.stringify(resultado[i]);
				ordenTrabajo = ordenTrabajo.replace(/\"/g,"'");
				
				tabla+='<tr>';
				tabla+='<td align="center" style="white-space: nowrap;"><button id="btnEditar" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mOrden" onclick="editarOrden('+ordenTrabajo+')"> <span class="glyphicon glyphicon-pencil"></span></button> ';
				if(resultado[i].estado!='Aprobado'){
					tabla +='<button   class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mOrdenEliminar" onclick="rellenarCampoID('+resultado[i].idOrdenTrabajo+')"><i class="glyphicon glyphicon-trash"></i> </button> ';
				}
				tabla+='<a class="btn btn-success btn-xs" href="?c=reportes&a=ExportarOrden&idCotizacion='+resultado[i].idCotizacion+'&idCostoOperativo='+resultado[i].idCostoOperativo+'&idOrdenTrabajo='+resultado[i].idOrdenTrabajo+'" target="_blank"><i class="fas fa-print"></i></a></td>';
				//'<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mOrdenEliminar" onclick="solicitarPDF('+resultado[i].idEvaluacion+')" target="_blank"><i class="glyphicon glyphicon-file"></i></button></td>';
				tabla+='<td>'+resultado[i].claveOrganizacion +' '+resultado[i].claveConsecutivo+' '+resultado[i].claveServicio+'</td>'+
						'<td>'+resultado[i].fechaEntrega+'</td>'+
						'<td>'+resultado[i].tituloNegocio+'</td>'+
						'<td>'+resultado[i].noPedido+'</td>'+
						'<td>'+resultado[i].nombrePersona+'</td>'+
						'<td>'+resultado[i].tiempoEntrega+'</td>'+
						'<td>'+resultado[i].fecha+'</td>';

				switch(resultado[i].estado){
					case 'Pendiente':
						tabla+='<td align="center"  style="background-color: #F9E79F; color:black;" ><strong> Pendiente </strong></td>';
						break;

					case 'Aprobado':
						tabla+='<td align="center" style="background-color: #A2D9CE"> Aprobado </td>';
						break;

					case 'Rechazado':
						tabla+='<td align="center" style="background-color: #E6B0AA"> Rechazado </td>';
						break;
				}
				tabla+="</tr>";
			}

		}

		tabla += "</table></div></div>";
		$("#resultadoBusqueda").html(tabla);
	});
}

function editarOrden(ordenTrabajo){
	
	$('#idOrdenTrabajo').val(ordenTrabajo.idOrdenTrabajo);

	$('#selectCuenta').val(ordenTrabajo.idNegocio);
	$('#selectCuenta option:not(:selected)').attr('disabled',true);
	$("#selectCuenta").selectpicker('refresh');

	$('#tituloNegocio').val(ordenTrabajo.tituloNegocio);

	$('#numeroPedido').val(ordenTrabajo.noPedido);

	$('#selectContacto').val(ordenTrabajo.idCliente);
	$('#selectContacto option:selected').attr('disabled',false);
	$("#selectContacto").selectpicker('refresh');

	$('#fechaRealizacion').val(ordenTrabajo.fecha.substring(0,10));

	$('#fechaEntrega').val(ordenTrabajo.fechaEntrega);

	$('#txtNotas').val(ordenTrabajo.descripcion);

	$('#txtTexto').val(ordenTrabajo.observaciones);

}

function convertDate(inputFormat) {
	function pad(s) { return (s < 10) ? '0' + s : s; }
	var d = new Date(inputFormat);
	return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
  }

  function rellenarCampoID(idOrden){
	  
	  $('#idOrdenEliminar').val(idOrden);
  }