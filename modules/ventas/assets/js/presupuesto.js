$(document).ready(function() {
	listarEmbudos();
	consultaAnidada('false');
	/* obtenerPeriodo();
	listarPresupuestos();*/

	/* Validadores Añadir Presupuesto */
	$('#form-presupuesto').bootstrapValidator({
		excluded: [':disabled', ':hidden', ':not(:visible)'],
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					if (respuesta == "actualizar") {
						$('#mPresupuestoGeneral').modal('toggle');
						$("#avisoEditar").html('<div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 0px; margin-bottom: 0px;"><h6 style="margin-top: 0px; margin-bottom: 0px;"><strong><center>Se han actualizado correctamente los datos</center></strong></h6></div>');
						$('#avisoEditar').show();
						$('#avisoEditar').delay(3500).hide(600);
						listarPresupuestosGenerales();
						consultaAnidada("false");
					}else{
						$('#mPresupuestoGeneral').modal('toggle');
						$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
						$('#mensajejs').show();
						$('#mensajejs').delay(3500).hide(600);
						consultaOnchange();
					}
				}
			});   
			return false;
		},
		fields: {
			nombrePresupuesto: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			periodo: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
		}
	});
	$('#mPresupuestoGeneral')
	.on('shown.bs.modal', function() {
		$('#form-presupuesto').find('[name="nombrePresupuesto"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form-presupuesto').bootstrapValidator('resetForm', true);
	});
	/*Fin Validadores Añadir Presupuesto*/

	/*Validadores Añadir Negocios a Presupuesto*/
	$('#form-presupuestoNegocio').bootstrapValidator({
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					$('#mPresupuestoNegocio').modal('toggle');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					consultaOnchange();
				}
			});   
			return false;
		},
		fields: {
			idNegocio: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			descripcion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
		}
	});
	$('#mPresupuestoNegocio')
	.on('shown.bs.modal', function() {
		/*$('#form-presupuestoNegocio').find('[name="idNegocio"]').focus();*/
	})
	.on('hidden.bs.modal', function () {
		$('#form-presupuestoNegocio').bootstrapValidator('resetForm', true);
	});
	/*Validadores Añadir Negocios a Presupuesto*/
});

function functionPeriodo(){
	var periodo = $('#selectPeriodo').val();
	$('#labelPeriodo').html(periodo)	
}

function myFunctionEliminar()
{
	var idPresupuesto = $('#txtIdPresupuesto').val();
	$('#txtIdPresupuestoN').val(idPresupuesto);  
}

function myFunctionNuevo() {
	obtenerPeriodo();
	$('#selectPeriodo').val("");
	$('#aviso').hide();
	$('#selectPeriodo').selectpicker('refresh');
	$('#labelPeriodo').html("");
	$('#txtNombrePresupuesto').val("");
	$('#txtIdPresupuestoGeneral').val("");
	$('#labTituloPresupuesto').html("Añadir Presupuesto");
	$('#btnPresupuesto').val("Guardar");
}

function myFunctionNuevoPN() {
	$('#selectIdNegocio').val("");
	$('#selectIdNegocio').selectpicker('refresh');
	var idPresupuestoGeneral = $('#selectAnio').val();
	$('#labTituloPresupuestoNegocio').html("Añadir negocio al presupuesto");
	$('#divCliente').hide();
	$('#divServicio').hide();
	$('#divEquipo').hide();
	$('#btnEliminar').hide();
	$('#txtIdPresupuesto').val('');	
	$('#txtNotas').val('');
	$('#txtIdOrganizacion').val('');
	$('#txtEquipo').val('');
	$('#txtClaveServicio').val('');
	$('#txtIdPresupuestoG').val(idPresupuestoGeneral);
	$('#btnGuardar').val("Guardar");
	listarNegocios();
}

function FunctionEditarPN(idPresupuesto, idNegocio, tituloNegocio, descripcion){
	var idEmbudo = $('#selectIdEmbudo').val();
	$('#labTituloPresupuestoNegocio').html("Editar negocio del presupuesto");
	$('#divCliente').show();
	$('#divServicio').show();
	$('#divEquipo').show();
	$('#btnEliminar').show();
	$('#btnGuardar').val("Actualizar");
	$('#txtIdPresupuesto').val(idPresupuesto);

	datos = {"idEmbudo": idEmbudo};
	$.ajax({
		url: "index.php?c=presupuesto&a=ListarNegocios",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectIdNegocio").empty();
		var selectIdNegocio = document.getElementById("selectIdNegocio"); 
		selectIdNegocio.options[0] = new Option(tituloNegocio, idNegocio);
		var j=0;
		for (var i in respuesta) {
			if(respuesta[i].idNegocio != idNegocio){
				j=j+1;
				selectIdNegocio.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
			}
		}
		$('#selectIdNegocio').selectpicker('refresh');
		activarDatos();
	});
	$('#txtNotas').val(descripcion);
}
$('#form-presupuestoNegocio').submit(function() {
});

$('#form-eliminar').submit(function() {
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function(mensaje){
			$('#mEliminarPG').modal('toggle');
			$("#avisoEditar").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-top: 0px; margin-bottom: 0px;"><h6 style="margin-top: 0px; margin-bottom: 0px;"><strong><center>'+mensaje+'</center></strong></h6></div>');
			$('#avisoEditar').show();
			$('#avisoEditar').delay(1800).hide(600);
			listarPresupuestosGenerales();
			consultaAnidada("false");
		}
	});   
	return false;
});

$('#form-eliminarPN').submit(function() {
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function(mensaje){
			$('#mEliminar').modal('toggle');
			$('#mPresupuestoNegocio').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-top: 0px; margin-bottom: 0px;"><h6 style="margin-top: 0px; margin-bottom: 0px;"><strong><center>'+mensaje+'</center></strong></h6></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			listarPresupuestosGenerales();
			consultaOnchange();
		}
	});   
	return false;
});

function listarPresupuestosGenerales() {
	$.post("index.php?c=presupuesto&a=ConsultaGeneral", {}, function(tabla) {
		$("#presupuestoGeneral").html(tabla);
	});
}

function FunctionEditarPG(datos){
	obtenerPeriodo();
	var datos = datos.split(",");
	var idPresupuestoGeneral = datos[0];
	var nombrePresupuesto = datos[1];
	var periodo = datos[2];
	var totalAnual = datos[3];
	var nombreUsuario = datos[6];
	$('#labelPeriodo').html(periodo);
	$('#labTituloPresupuesto').html("Editar Presupuesto");
	$('#txtIdPresupuestoGeneral').val(idPresupuestoGeneral);  
	$('#txtNombrePresupuesto').val(nombrePresupuesto);
	$('#selectPeriodo').val(periodo);
	$('#selectPeriodo').selectpicker('refresh');
	$('#txtResponsable').val(nombreUsuario);
	$('#btnPresupuesto').val("Actualizar");
}

function FunctionEliminarPG(datos){
	var datos = datos.split(",");
	var idPresupuestoGeneral = datos[0];
	$('#txtIdPGE').val(idPresupuestoGeneral);
}

function consultaOnchange() {
	var idEmbudo = $('#selectIdEmbudo').val();
	var idPresupuestoGeneral = $('#selectAnio').val();
	var periodo = $('#selectAnio option:selected').html();
	$('#btnPushNegocio').attr("disabled", false);
	var bTexto = $("#txtBuscar").val();
	var Mes = $('#selectMes').val();
	$('#txtIdPG').val(idPresupuestoGeneral);
	$('#txtIdMes').val(Mes);
	datos = {"idEmbudo": idEmbudo};
	$.ajax({
		url: "index.php?c=presupuesto&a=ListarPeriodo",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectAnio").empty();
		selectAnio.options[0] = new Option(periodo, idPresupuestoGeneral);
		j=0;
		for (var i in respuesta) {
			if (respuesta[i].idPresupuestoGeneral != idPresupuestoGeneral) {
				selectAnio.options[++j] = new Option(respuesta[i].periodo,respuesta[i].idPresupuestoGeneral);
			}
		}
		$('#selectAnio').selectpicker('refresh');
		crearTabla(idEmbudo, idPresupuestoGeneral, "true", bTexto, Mes);
	});
}

function consultaAnidada(status){
	var bTexto = $("#txtBuscar").val();
	var idEmbudo = $('#selectIdEmbudo').val();
	var idPresupuestoGeneral = $('#selectAnio').val();
	var Mes = $('#selectMes').val();
	$('#txtIdMes').val(Mes);
	var anio = (new Date).getFullYear();
	var accion = "false";
	datos = {"idEmbudo": idEmbudo};
	$.ajax({
		url: "index.php?c=presupuesto&a=ListarPeriodo",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		if ($.isEmptyObject(respuesta)) {
			$("#selectAnio").empty();
			var selectAnio = document.getElementById("selectAnio");
			selectAnio.options[0] = new Option("Año","");
			$('#selectAnio').selectpicker('refresh');
			$('#btnPushNegocio').attr("disabled", true);
			$('#btnExportar').attr("disabled", true);
			$('#selectMes').val("");
			$('#selectMes').selectpicker('refresh');
			crearTabla(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes);
		}else{
			for (var i in respuesta) {
				if (respuesta[i].periodo == anio) {
					$('#btnPushNegocio').attr("disabled", false);
					$('#btnExportar').attr("disabled", false);
					$('#selectMes').val("");
					$('#selectMes').selectpicker('refresh');
					$('#txtIdPG').val(respuesta[i].idPresupuestoGeneral);
					accion = "true";
					periodoIgu(idEmbudo, respuesta[i].idPresupuestoGeneral, "true", bTexto, Mes, respuesta, respuesta[i].periodo);	
				}
			}
			if (accion == "false") {
				$('#btnPushNegocio').attr("disabled", true);
				$('#btnExportar').attr("disabled", true);
				$('#selectMes').val("");
				$('#selectMes').selectpicker('refresh');
				periodoDif(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes, respuesta);
			}
		}
	});
}

function periodoDif(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes, myArray) {
	$("#selectAnio").empty();
	var selectAnio = document.getElementById("selectAnio");
	selectAnio.options[0] = new Option("Año","");
	var n = 1; 
	for (var i in myArray){
		selectAnio.options[n] = new Option(myArray[i].periodo,myArray[i].idPresupuestoGeneral);
		++n;
	}
	$('#selectAnio').selectpicker('refresh');
	crearTabla(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes);
}

function periodoIgu(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes, myArray, periodo) {
	$("#selectAnio").empty();
	var selectAnio = document.getElementById("selectAnio");
	selectAnio.options[0] = new Option(periodo,idPresupuestoGeneral);
	var n = 1; 
	for (var i in myArray){
		if(idPresupuestoGeneral != myArray[i].idPresupuestoGeneral){
			selectAnio.options[n] = new Option(myArray[i].periodo,myArray[i].idPresupuestoGeneral);
			++n;
		}
	}
	$('#selectAnio').selectpicker('refresh');
	crearTabla(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes);
}

function obtenerPeriodo() {
	$("#selectPeriodo").empty();
	var selector = document.getElementById("selectPeriodo");
	selector.options[0] = new Option("Seleccione el año del presupuesto","");
	var fecha = new Date();
	var anio = fecha.getFullYear();
	for (var i = 1; i < 5; i++) {
		selector.options[i] = new Option(anio,anio);
		anio = anio + 1;
	}
	$('#selectPeriodo').selectpicker('refresh');
}


function crearTabla(idEmbudo, idPresupuestoGeneral, status, bTexto, Mes) {
	$.post("index.php?c=presupuesto&a=pintarTabla", { idEmbudo: idEmbudo, idPresupuestoGeneral: idPresupuestoGeneral, status: status, bTexto: bTexto, Mes: Mes }, function(tabla) {
		$('#tablaPresupuestos').html(tabla);
	});
	$.post("index.php?c=presupuesto&a=verificaEx", { idEmbudo: idEmbudo, idPresupuestoGeneral: idPresupuestoGeneral, status: status, bTexto: bTexto, Mes: Mes }, function(accion) {
		if(accion == "false"){
			$('#btnExportar').attr("disabled",false);               

		}else{
			$('#btnExportar').attr("disabled",true);   
		}
	});
}

function listarEmbudos(){
	var nombre = $('#selectIdEmbudo option:selected').html();
	var idEmbudo = $('#selectIdEmbudo').val();
	$.post("index.php?c=presupuesto&a=CambiarEmbudo", { idEmbudo: idEmbudo, nombre: nombre }, function() {
	});
	var selector = document.getElementById("selectIdEmbudo");
	selector.options[0] = new Option(nombre, idEmbudo);
	$.ajax({
		url: "index.php?c=presupuesto&a=ListarEmbudos",
	}).done(function(respuesta){
		var j=0;
		for (var i in respuesta) {
			if(idEmbudo != respuesta[i].idEmbudo){
				var j=j+1;
				selector.options[j] = new Option(respuesta[i].nombre,respuesta[i].idEmbudo);
			}
		}
		$('#selectIdEmbudo').selectpicker('refresh');
	});
	consultaAnidada('false');
}

function listarNegocios(){
	var idEmbudo = $('#selectIdEmbudo').val();
	$("#selectIdNegocio").empty();
	var selector = document.getElementById("selectIdNegocio");
	selector.options[0] = new Option("Seleccione un negocio", "");
	datos = {"idEmbudo":idEmbudo};
	$.ajax({
		url: "index.php?c=presupuesto&a=ListarNegocios",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		var j=0;
		for (var i in respuesta) {	
			var j=j+1;
			selector.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
		}
		$('#selectIdNegocio').selectpicker('refresh');
	});
}

function activarDatos(){
	$('#divCliente').show();
	$('#divEquipo').show();
	$('#divServicio').show();
	var idNegocio = $('#selectIdNegocio').val();
	datos = {"idNegocio":idNegocio};
	$.ajax({
		url: "index.php?c=presupuesto&a=listarDatosNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		console.log(JSON.stringify(respuesta));
		for (var i in respuesta) {
			$('#txtClaveServicio').val(respuesta[i].claveServicio);
			$('#txtEquipo').val(respuesta[i].nombreEquipo);
			$('#txtIdOrganizacion').val(respuesta[i].nombreOrganizacion);
		}
	});
}

function listarPresupuestos(){
	$.post("index.php?c=presupuesto&a=ListarPresupuestos", {}, function(respuesta) {
		$('#selectPresupuestos').html(respuesta);			
	});
}

$("#selectPeriodo").change(function() {
	$('#aviso').hide();
	var periodo = $('#selectPeriodo').val();
	var idPresupuestoGeneral = $('#txtIdPresupuestoGeneral').val();
	$.post("index.php?c=presupuesto&a=VerificarPresupuesto", {periodo: periodo, idPresupuestoGeneral: idPresupuestoGeneral}, function(respuesta) {
		console.log(respuesta);
		if (respuesta == 0){
			$( "#btnPresupuesto" ).prop("disabled", false);
		}else if (respuesta == 1){
			$('#aviso').show();
			$("#aviso").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este presupuesto ya existe </p>');
			$("#btnPresupuesto").prop("disabled", true);
		}
	})
});

function FunctionDetalles(claveServicio, periodo, totalAnual, idNegocio, tituloNegocio, Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre) {
	$('#titleModal').html('Negocio: <span style="color:#757575">'+tituloNegocio+'</span>');
	var servicio = consultaServicio(claveServicio);
	$('#servicio').html(servicio);
	var dataMes = new Object();
	var data = new Object();
	data = {claveServicio: claveServicio, periodo: periodo, totalAnual: totalAnual, idNegocio: idNegocio};	
	dataMes = {Enero: Enero, Febrero: Febrero, Marzo: Marzo, Abril: Abril, Mayo: Mayo, Junio: Junio, Julio: Julio, Agosto: Agosto, Septiembre: Septiembre, Octubre: Octubre, Noviembre: Noviembre, Diciembre: Diciembre};
	var totalPresupuesto = detallesPresupuesto(data, dataMes);
	detallesEstimacion(data.idNegocio, totalPresupuesto);
}

function detallesPresupuesto(data, dataMes) {
	var array = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
	for (var i in array) {
		if (dataMes[array[i]] > 0) {
			var contenido = contenido + `
			<tr>
			<td>${array[i]}-${data.periodo}</td>
			<td align="right">${FormatNumber(dataMes[array[i]])}</td>
			</tr>`;
		}else{
			var contenido = contenido + `
			<tr></tr>`;
		}
	}
	var total = `<td colspan="2" align="right">Total: ${FormatNumber(data.totalAnual)}</td>`;
	$('#bodyPresupuesto').html(contenido);
	$('#total').html(total);
	return data.totalAnual;
}

function detallesEstimacion(idNegocio, totalPresupuesto) {
	$.get("index.php?c=presupuesto&a=obtenerClave", {idNegocio: idNegocio}, function(claveJSON) {
		var totalEstimacion = 0;
		var item = 1;
		for (var i in claveJSON) {
			fecha = formatDate(claveJSON[i].fechaCierre);
			var contenido = contenido + `<tr>
			<td>${item}</td>
			<td>${fecha}</td>
			<td align="right">${FormatNumber(claveJSON[i].valorNegocio)}</td>
			</tr>`;
			totalEstimacion = parseFloat(totalEstimacion) + parseFloat(claveJSON[i].valorNegocio);
			++item;
		}
		var tfoot = `<td colspan="3" align="right">Total: ${FormatNumber(totalEstimacion)}</td>`;
		var total = (totalPresupuesto - totalEstimacion);
		var callout =`<h5>Fórmula: <span style="color:#000">(${FormatNumber(totalPresupuesto)}  -  ${FormatNumber(totalEstimacion)})</span> diferencia: <span style="color:#000"> ${FormatNumber(total)} </span></h5>`; 
		$('#bodyEstimacion').html(contenido);
		$('#tfootEstimacion').html(tfoot);
		$('#calloutTotal').html(callout);
	});
}

function FormatNumber(number) {
	var nf = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
		minimumFractionDigits: 2,
		maximumFractionDigits: 2
	});
	return nf.format(number); /* ‘$123,456.79’*/
}

function formatDate(fecha) {
	var date = fecha.split("-");
	var year = date[0];
	var month = date[1];
	var day = date[2];
	var meses = new Object();
	month = parseInt(month);
	meses = {m1: "Enero", m2: "Febrero", m3: "Marzo", m4: "Abril", m5: "Mayo", m6: "Junio", m7: "Julio", m8: "Agosto", m9: "Septiembre", m10: "Octubre", m11: "Noviembre", m12: "Diciembre"};
	var fecha_formateada = day + ' de ' + meses['m'+month] + ' del ' + year;
	return fecha_formateada;
}

function consultaServicio(claveServicio) {
	switch(claveServicio){
		case 'MPV':
		return "Mantenimiento Preventivo";
		break;
		case 'AST':
		return "Asesoría Técnica";
		break;
		case 'FTA':
		return "Fabricación";
		break;
		case 'MCC':
		return "Mantenimiento correctivo en campo";
		break;
		case 'MCT':
		return "Mantenimiento correctivo en taller";
		break;
		case 'CBM':
		return "Mantenimiento basado en la condición";
		break;
		case 'MEM':
		return "Montaje";
		break;
		case 'SMR':
		return "Suministros";
		break;
		case 'VVA':
		return "Varios";
		break;
		default:
		break;
	}
}



