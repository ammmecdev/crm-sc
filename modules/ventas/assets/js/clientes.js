var paginaActual = 1;
$(document).ready(function() {
    paginador(paginaActual);
    $("#txtNomOrganizacion").blur(function(){
        $('#alertanombre').hide();
        var txtNomOrganizacion = $('#txtNomOrganizacion').val();
        var txtIdOrganizacion = $('#txtIdOrganizacion').val();
        $.post("index.php?c=organizaciones&a=VerificaNombre", {txtNomOrganizacion: txtNomOrganizacion,txtIdOrganizacion:txtIdOrganizacion}, function(respuesta) {
            if (respuesta==0){
                $( "#btnGuardar" ).prop( "disabled", false );
            }else{
                $('#alertanombre').show();
                $("#alertanombre").html('<h5 style="color: red">Este nombre ya se encuentra registrado</h5>');
                $( "#btnGuardar" ).prop( "disabled", true );
            }
        })
    });

    $("#txtClave").blur(function(){
        $('#alertaclave').hide();
        var txtClave = $('#txtClave').val();
        var txtIdOrganizacion = $('#txtIdOrganizacion').val();
        $.post("index.php?c=organizaciones&a=VerificarClave", {txtClave: txtClave,txtIdOrganizacion:txtIdOrganizacion}, function(respuesta) {
            if (respuesta==0){
                $( "#btnGuardar" ).prop( "disabled", false );
            }else{
                $('#alertaclave').show();
                $("#alertaclave").html('<h5 style="color: red">Esta clave ya se encuentra registrada</h5>');
                $( "#btnGuardar" ).prop( "disabled", true );
            }
        })
    });

    $('#form-organizacion').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
            }).done(function(respuesta){
                if (respuesta=="errorN") {
                    $("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Este nombre de Organización ya se encuentra registrado</center></strong></div>');
                }else if(respuesta=="errorC"){
                    $("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Esta clave Organización ya se encuentra registrada</center></strong></div>');
                }
                else{
                    $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
                }
                $('#mOrganizacion').modal('toggle');
                $('#mensajejs').show();
                $('#mensajejs').delay(3500).hide(600);
                paginador(paginaActual);
            });
            return false;
        },
        fields: {
            nombreOrganizacion: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z ÑñÁÉÍÓÚáéíóú])*$/,
                        message: 'Sólo letras'
                    },
                    notEmpty: {
                        message: 'Campo Obligatorio'
                    }
                }
            },
            clave: {
                validators: {
                    stringLength: {
                        min: 3,
                        max: 3,
                        message: 'Mínimo 3 - Máximo 3 letras'
                    },
                    regexp: {
                        regexp: /^([A-Z])*$/,
                        message: 'Sólo letras en mayusculas'
                    },
                    notEmpty: {
                        message: 'Campo Obligatorio'
                    }
                }
            },
            direccion: {
                validators: {
                    stringLength: {
                        max: 50,
                    },
                }
            },
            localidadEstado: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z ÑñÁÉÍÓÚáéíóú ])*$/,
                        message: 'Sólo letras y espacios'
                    }
                }
            },
            paginaWeb: {
                validators: {
                    regexp: {
                        regexp: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
                        message: 'Dirección URL No valida'
                    },
                }
            },
            telefono: {
                validators: {
                    stringLength: {
                        min: 7,
                        max: 10,
                        message: 'Mínimo 7 - Máximo 10 caracteres'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Sólo números'
                    },
                }
            },
            extensionTelefonica: {
                validators: {
                    stringLength: {
                        min: 1,
                        max: 4,
                        message: 'Mínimo 1 - Máximo 4 caracteres'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Sólo números'
                    }, 
                }
            },
            idSector: {
                validators: {
                    notEmpty: {
                        message: 'Campo Obligatorio'
                    }
                }
            },
            rfc: {
                validators: {
                    regexp: {
                        regexp: /([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})/,
                        message: 'El RFC no es válido'
                    }
                }
            }
        }
    });
$('#mOrganizacion')
.on('shown.bs.modal', function() {
    $('#form-organizacion').find('[name="nombreOrganizacion"]').focus();
})
.on('hidden.bs.modal', function () {
    $('#form-organizacion').bootstrapValidator('resetForm', true);
});
/*Fin de Validadores de Organizaciones*/

$('#form-organizaciondel').submit(function(){
    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serialize(),
    }).done(function(respuesta){
        if (respuesta=="false") {
            $("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Error el Cliente tiene Negocios asociados</center></strong></div>');
        }else{
            $("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se a eliminado el cliente con exito</center></strong></div>');
        }
        $('#mOrganizacion').modal('toggle');
        $('#mEliminar').modal('toggle');
        $('#mensajejs').show();
        $('#mensajejs').delay(3500).hide(600);
        myFunctionLimpia();
        paginador(paginaActual);
    });
    return false;
});

});

var bAlfabetica='';
pintarTabla = function (numRegistros, bTexto)
{
    $.post("index.php?c=organizaciones&a=Consultas", {bTexto: bTexto, bAlfabetica: bAlfabetica, registros: numRegistros, pagina: paginaActual}, function(resultado) {
        $("#resultadoBusqueda").html(resultado); 
    });
    $.post("index.php?c=organizaciones&a=VerificaEx", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(resultado) {
        if(resultado == "false")
            $('#btnExportar').attr("disabled", false);               
        else
            $('#btnExportar').attr("disabled", true);   
    });
    $.post("index.php?c=organizaciones&a=Contador", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(noEquipo) {
        if (noEquipo==1)
            $("#h5Contador").html(noEquipo+' Cliente');
        else
            $("#h5Contador").html(noEquipo+' Clientes'); 
    })
}

function paginador(pagina) {
    paginaActual = pagina;
    var numRegistros = $('#selectPorPagina').val();
    var bTexto = $("#txtBuscar").val();
    pintarTabla(numRegistros, bTexto);
    switchColor(numRegistros); 
}

function switchColor(numRegistros){
    $.post("index.php?c=organizaciones&a=obtenerRegistros", {}, function(totalRegistros) {
        var totalPaginas = Math.ceil(totalRegistros / numRegistros);
        for (var i = 1; i <= totalPaginas; i++) {
            if (i == paginaActual) {
                $("#li"+i).addClass("active");
            }else{
                $("#li"+i).removeClass("active");
            }
        }
    }); 
}

filtrarOrganizaciones = function (busqueda){
    $('#txtbAlfabetica').val(busqueda);
    bAlfabetica = busqueda;
    switch(busqueda) {
        case 'A':
        desmarcar("a");
        break;
        case 'B':
        desmarcar("b");
        break;
        case 'C':
        desmarcar("c");
        break;
        case 'D':
        desmarcar("d");
        break;
        case 'E':
        desmarcar("e");
        break;
        case 'F':
        desmarcar("f");
        break;
        case 'G':
        desmarcar("g");
        break;
        case 'H':
        desmarcar("h");
        break;
        case 'I':
        desmarcar("i");
        break;
        case 'J':
        desmarcar("j");
        break;
        case 'K':
        desmarcar("k");
        break;
        case 'L':
        desmarcar("l");
        break;
        case 'M':
        desmarcar("m");
        break;
        case 'N':
        desmarcar("n");
        break;
        case 'Ñ':
        desmarcar("ñ");
        break;
        case 'O':
        desmarcar("o");
        break;
        case 'P':
        desmarcar("p");
        break;
        case 'Q':
        desmarcar("q");
        break;
        case 'R':
        desmarcar("r");
        break;
        case 'S':
        desmarcar("s");
        break;
        case 'T':
        desmarcar("t");
        break;
        case 'U':
        desmarcar("u");
        break;
        case 'V':
        desmarcar("v");
        break;
        case 'W':
        desmarcar("w");
        break;
        case 'X':
        desmarcar("x");
        break;
        case 'Y':
        desmarcar("y");
        break;
        case 'Z':
        desmarcar("z");
        break;
        default:
        desmarcar("todas");
        break;
    }
    paginador(paginaActual);
}


function listarSectorIndustrial(){
    datos = {};
    $.ajax({
        url: "index.php?c=organizaciones&a=ListarSectorIndustrial",
        type: "POST",
        data: datos
    }).done(function(respuesta){
        $("#selectIdSectorIndustrial").empty();
        var selector = document.getElementById("selectIdSectorIndustrial");
        selector.options[0] = new Option("Seleccione el sector industrial","");
        for (var i in respuesta) {
            var j = parseInt(i) + 1;
            selector.options[j] = new Option(respuesta[i].nombreSector,respuesta[i].idSector);
        }
        $('#selectIdSectorIndustrial').selectpicker('refresh');
    }); 
}

function myFunctionEditar(IdO, NomOrga,Clave,Direccion,localidadEstado,paginaWeb,telefono,extensionTelefonica,tipoTelefono,nombreSector,idSector,rfc) {
    $('#alertanombre').hide();
    $('#alertaclave').hide();
    $('#txtIdOrganizacion').val(IdO); 
    $('#txtNomOrganizacion').val(NomOrga);
    $('#txtClave').val(Clave);  
    $('#txtDireccion').val(Direccion);
    $('#txtlocalidadEstado').val(localidadEstado);
    $('#txtPaginaWeb').val(paginaWeb);
    $('#txtTelefono').val(telefono);
    $('#txtExtension').val(extensionTelefonica);
    $('#selectTipoTelefono').val(tipoTelefono);
    $('#txtRFC').val(rfc);
    $('#btnEliminar').show();
    $('#labTitulo').html("Editar cliente");

//--- Select ---
$("#selectIdSectorIndustrial").empty();
var selectSectorIndustrial = document.getElementById("selectIdSectorIndustrial");
selectSectorIndustrial.options[0] = new Option(nombreSector , idSector);

$.ajax({
    url: "index.php?c=organizaciones&a=ListarSectorIndustrial",
}).done(function(respuesta){
    console.log(respuesta);
    var j=0;
    for (var i in respuesta) {
        if(idSector!=respuesta[i].idSector){
            var j=j+1;
            selectSectorIndustrial.options[j] = new Option(respuesta[i].nombreSector,respuesta[i].idSector);
        }
    }
    $('#selectIdSectorIndustrial').selectpicker('refresh');
});
}

function myFunctionNuevo() {
    $('#alertanombre').hide();
    $('#alertaclave').hide();
    $('#txtIdOrganizacion').val(""); 
    $('#txtNomOrganizacion').val("");  
    $('#txtDireccion').val("");
    $('#txtPaginaWeb').val("");
    $('#txtTelefono').val("");
    $('#txtPropietario').val("");
    $('#txtClave').val("");
    $('#txtlocalidadEstado').val("");
    $('#txtExtension').val("");
    $('#selectTipoTelefono').val("");
    $('#txtRFC').val("");
    $('#btnEliminar').hide();
    $('#labTitulo').html("Añadir cliente");
    listarSectorIndustrial();
}

function myFunctionLimpia() {
    $('#usuario').val(""); 
    $('#pass').val("");
}

function myFunctionEliminar(){
    var idOrganizacion = $('#txtIdOrganizacion').val();
    $('#txtIdOrganizacionE').val(idOrganizacion);  
}

function desmarcar(letra){
    var miarray = ["todas", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

    for (var i = 0; i < miarray.length; i++) {
        if (letra == miarray[i]) {
            $("#btn-"+miarray[i]).addClass("btn-primary");
            $("#btn-"+miarray[i]).removeClass("btn-default");
        }else{
            $("#btn-"+miarray[i]).removeClass("btn-primary");
            $("#btn-"+miarray[i]).addClass("btn-default");
        }
    }
}