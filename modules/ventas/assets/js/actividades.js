$(document).ready(function() {

	$( '#checkTiempo' ).on( 'click', function() {
		if( $(this).is(':checked') ){
			$('#divTiempo').show();
			$('#txtHora').focus();
			$('#divCruzadas').show();
			$('#txtConfirmado').val("false");
		} else {
			$('#divTiempo').hide();
			$('#divCruzadas').html("");
			$('#divCruzadas').hide();
			$('#btnGuardar').val('Guardar');
		}
	});

	filtrarActividades('');
	consultas();

	$('#form-exportar').submit(function() {
		var bTexto = $("#txtBuscar").val();
		if (bPeriodo==''){
			location.href = '?c=actividades&a=Consultas&bTexto='+bTexto+'&bActividad='+bActividad+'&accion=exportar';
		}else{
			location.href = '?c=actividades&a=Consultas&bTexto='+bTexto+'&bActividad='+bActividad+'&bPeriodo='+bPeriodo+'&accion=exportar&periodos=true';
		}
		return false;
	});

	$('#form-actividades').bootstrapValidator({
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					if(respuesta[0].mensaje == "cruzada"){
						$('#divCruzadas').show();
						$("#cruzadas").html("<label style='color:red;'>Hay actividades cruzadas con la actividad actual:</label><br><br><pre><label style='color:blue; margin-bottom-80px;'>&nbsp;Actividades cruzadas</label>"+respuesta[0].tabla+"</pre><p align='right' style='color:blue'>¿Desea guardar de todas formas?</p>");
						$('#txtConfirmado').val('true');
						$('#btnGuardar').val('Confirmar');
						$('#btnGuardar').attr('hidden',false);
						$('#btnGuardar').attr('disabled',false);
					}else{
						$('#mActividades').modal('toggle');
						$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
						$('#mensajejs').show();
						$('#mensajejs').delay(3500).hide(600);
						consultas();
					}
				}
			})      
			return false;
		},
		fields: {
			options: {
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			notas: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			fecha: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			radioFiltros: {
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			idOrganizacion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
		}
	});
	$('#mActividades')
	.on('shown.bs.modal', function() {
		/*$('#form-actividades').find('[name="fechaEmision"]').focus();*/
	})
	.on('hidden.bs.modal', function () {
		$('#form-actividades').bootstrapValidator('resetForm', true);
	});

	$('#form-cambiaEstado').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mTerminar').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consultas();
		});
		return false;
	}); 

	$("#porOrganizacion").change(function(){
		filtrarPorOrganizaciones();
		
	});

	$("#porNegocio").change(function(){
		filtrarPorNegocios();
		// $('#form-actividades').data('bootstrapValidator').resetField('idOrganizacion', true);
	});

	$("#actividadInterna").change(function(){
		actividadInterna();
	});

});

$('#form-eliminar').submit(function(){
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
	}).done(function(respuesta){
		$('#mActividades').modal('toggle');
		$('#mEliminar').modal('toggle');
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
		consultas();
	});
	return false;
}); 


editarActividad = function(datos){
	var datos = datos.split(",");
	console.log(datos);
	var idActividad = datos[0];
	var tipoActividad = datos[1];
	var notas = datos[2];
	var fechaActividad = datos[3];
	var duracion = datos[4];
	var horaInicio = datos [5];
	var idOrganizacion = datos[6];
	var nombreOrganizacion = datos[7];
	var idNegocio = datos[8];
	var tituloNegocio = datos[9];
	var nombrePersona = datos[10]; 

	var tipoActividadEdit='';

	if(idNegocio!='' && idOrganizacion!='')
		tipoActividadEdit='completa';
	else if(idNegocio=='' && idOrganizacion!='')
		tipoActividadEdit='leads';
	else if(idNegocio=='' && idOrganizacion=='')
		tipoActividadEdit='interna';

	camposEditar(tipoActividadEdit); 

	$('#txtActividad').val(tipoActividadEdit);

	switch(tipoActividad) {
		case 'Llamada':
		$('#optionLlamada').prop('checked',true);
		break;
		case 'Reunion':
		$('#optionReunion').prop('checked',true);
		break;
		case 'Tarea':
		$('#optionTarea').prop('checked',true);
		break;
		case 'Plazo':
		$('#optionPlazo').prop('checked',true);
		break;
		case 'Email':
		$('#optionEmail').prop('checked',true);
		break;
		case 'Comida':
		$('#optionComida').prop('checked',true);
		break;
		case 'WhatsApp':
		$('#optionWhatsapp').prop('checked',true);
		break;
	}

	$('#txtIdActividad').val(idActividad);  
	$('#txtTipo').val(tipoActividad);
	$('#txtNotasA').val(notas);  
	$('#txtfechaActividad').val(fechaActividad);
	var arregloDuracion = duracion.split(" ");
	$('#txtDuracion').val(arregloDuracion[0]);
	$('#txtHora').val(horaInicio)
	$('#txtIdOrganizacion').val(idOrganizacion);
	$('#txtNombreOrganizacion').val(nombreOrganizacion);
	$('#txtConfirmado').val('false');

	if( horaInicio != ""){
		$('#checkTiempo').prop('checked',true);
		$('#divTiempo').show();
		$('#btnGuardar').val('Guardar');
	} else {
		$('#checkTiempo').prop('checked',false);
		$('#divTiempo').hide();
		$('#btnGuardar').val('Guardar');
	}

//----- Select tipo de duración --------
$('#selectTipoDuracion').empty(); 
var selectTipoDuracion = document.getElementById("selectTipoDuracion");
if(arregloDuracion[1] == "minuto" || arregloDuracion[1] == "minutos"){
	selectTipoDuracion.options[0] = new Option("minutos","minutos");
	selectTipoDuracion.options[1] = new Option("horas","horas");
}
else{
	selectTipoDuracion.options[0] = new Option("horas","horas");
	selectTipoDuracion.options[1] = new Option("minutos","minutos");
}

var selectIdNegocioF = document.getElementById("selectIdNegocioF");
selectIdNegocioF.options[0] = new Option(tituloNegocio,idNegocio);

datos = {"idOrganizacion":idOrganizacion};
$.ajax({
	url: "index.php?c=actividades&a=ListarPersonasPorOrganizacion",
	type: "POST",
	data: datos
}).done(function(respuesta){
	$("#selectNombrePersona").empty();
	var selectNombrePersona = document.getElementById("selectNombrePersona");
	if (nombrePersona != "")
		selectNombrePersona.options[0] = new Option(nombrePersona,nombrePersona);
	else
		selectNombrePersona.options[0] = new Option("Seleccione la persona de contacto","");
	var j=0;
	for (var i in respuesta) {
		if(respuesta[i].nombrePersona != nombrePersona){
			selectNombrePersona.options[++j] = new Option(respuesta[i].nombrePersona,respuesta[i].nombrePersona);
		}
	}
	$('#selectNombrePersona').selectpicker('refresh');
});
}

filtrarPorOrganizaciones = function()
{
	$("#selectIdOrganizaciones option[value='']").attr("selected",true);
	$("#divOrganizaciones").hide();
	$("#txtIdOrganizacion").prop('disabled', true);
	$("#selectPersonas").hide();
	$("#selectNegocios").hide();
	$("#selectNegociosF").hide(); 
	$("#selectOrganizaciones").show();
	$("#selectIdOrganizaciones").removeAttr("disabled");
	$("#selectIdNegocio").prop('disabled', true);
	$.ajax({
		url: "index.php?c=actividades&a=ListarOrganizaciones",
		type: "POST",
	}).done(function(respuesta){
		//console.log(respuesta);
		$("#selectIdOrganizaciones").empty();
		var selectIdOrganizaciones = document.getElementById("selectIdOrganizaciones");
		selectIdOrganizaciones.options[0] = new Option("Selecciona un cliente","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selectIdOrganizaciones.options[j] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
		}
		$('#selectIdOrganizaciones').selectpicker('refresh');
	});
}

ocultarFiltros = function()
{
	$("#divOrganizaciones").hide();
	$("#selectPersonas").hide();
	$("#selectNegocios").hide();
	$("#selectNegociosF").hide(); 
	$("#selectOrganizaciones").hide();
}

actividadInterna = function()
{
	$("#selectNegociosF").hide();
	$("#selectPersonas").hide();
	$("#selectOrganizaciones").hide();
	$("#divOrganizaciones").hide(); 
	$("#selectIdOrganizaciones").prop('disabled', true);
	$("#divOrganizaciones").hide();
	$("#txtIdOrganizacion").prop('disabled', true);
	$("#selectPersonas").hide();
	$("#selectNegocios").hide();
	$("#selectNegociosF").hide(); 
	$("#selectIdNegocio").prop('disabled', true);
	$('#divCruzadas').hide();
}

filtrarPorNegocios = function()
{
	$("#selectNegociosF").hide();
	$("#selectPersonas").hide();
	$("#selectOrganizaciones").hide();
	$("#divOrganizaciones").hide(); 
	$("#selectNegocios").show();
	$("#selectIdNegocio").removeAttr("disabled");
	$("#selectIdOrganizaciones").prop('disabled', true);
	$("#txtIdOrganizacion").removeAttr("disabled");
	$.ajax({
		url: "index.php?c=actividades&a=ListarNegocios",
		type: "POST",
	}).done(function(respuesta){
		$("#selectIdNegocio").empty();
		var selectIdNegocio = document.getElementById("selectIdNegocio");
		selectIdNegocio.options[0] = new Option("Selecciona un negocio","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selectIdNegocio.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
		}
		$('#selectIdNegocio').selectpicker('refresh');
	});
}

tipoActividad = function (tipo)
{
	$('#txtTipo').val(tipo);

	switch(tipo){
		case 'Llamada':
		desmarcar("Llamada");
		break;
		case 'Reunion':
		desmarcar("Reunion");
		break;
		case 'Tarea':
		desmarcar("Tarea");
		break;
		case 'Plazo':
		desmarcar("Plazo");
		break;
		case 'Email':
		desmarcar("Email");
		break;
		case 'Comida':
		desmarcar("Comida");
		break;
		case 'WhatsApp':
		desmarcar("WhatsApp");
		break;
		default: 
		break;
	}
}  

desmarcar = function(tipo) {
	var miarray = ["Llamada", "Reunion", "Tarea", "Plazo", "Email", "Comida", "WhatsApp"];

	for (var i = 0; i < miarray.length; i++) {
		if (tipo == miarray[i]) {
			$("#lab"+miarray[i]).addClass("btn-pressed");
			$("#lab"+miarray[i]).removeClass("btn-default");
		}else{
			$("#lab"+miarray[i]).removeClass("btn-pressed");
			$("#lab"+miarray[i]).addClass("btn-default");
		}
	}
}

camposEditar = function(tipoActividadEdit)
{
	$('#labTitulo').html("Editar actividad");
	$('#btnEliminar').show();
	$('#divCruzadas').hide();
	$('#divFiltro').hide();
	$('#cruzadas').html('');
	$('#selectNegocios').hide();
	$('#selectOrganizaciones').hide();

	if(tipoActividadEdit!='interna'){
		$('#fontVinculacion').show();
		$("#txtIdOrganizacion").removeAttr("disabled");
		$('#divOrganizaciones').show();
		$('#selectPersonas').show();
		$("#selectIdOrganizaciones").prop('disabled', true);
	}

	if(tipoActividadEdit=='completa'){
		$('#selectNegociosF').show();
		$("#selectIdNegocio").prop('disabled', true);
	}

	if(tipoActividadEdit=='leads'){
		$('#selectNegociosF').hide();
		$('#selectIdNegocio').prop('disabled', true);
	}

	if(tipoActividadEdit=='interna')
	{
		$('#fontVinculacion').hide();
		ocultarFiltros();
		actividadInterna();
	}

	desmarcar();
}

function nuevaActividad() 
{
	$('#fontVinculacion').show();
	$('#btnGuardar').val('Guardar');
	$('#porOrganizacion').prop('checked',false);
	$('#actividadInterna').prop('checked',false);
	$('#porNegocio').prop('checked',false);
	$('#optionLlamada').prop('checked',false);
	$('#optionReunion').prop('checked',false);
	$('#optionTarea').prop('checked',false);
	$('#optionPlazo').prop('checked',false);
	$('#optionEmail').prop('checked',false);
	$('#optionComida').prop('checked',false);
	$('#optionWhatsapp').prop('checked',false);
	$('#checkTiempo').prop('checked',false);
	$('#divTiempo').hide();
	$('#divCruzadas').hide();
	$('#selectOrganizaciones').hide();
	$('#divFiltro').show();
	$('#txtIdActividad').val(0);
	$('#txtTipo').val(""); 
	$('#txtTituloNegocio').val("");
	$('#txtNombreOrganizacion').val("");
	$('#txtIdOrganizacion').val("");
	$('#txtfechaActividad').val("");
	$('#txtHora').val("");
	$('#txtDuracion').val("");
	$('#txtNotasA').val("");
	$('#txtConfirmado').val("false");
	$('#btnEliminar').hide();
	$('#labTitulo').html("Programar actividad");
	$('#txtActividad').val("");
	ocultarFiltros();
	desmarcar();
}

function deshabilitaConfirmado(){
	$('#txtConfirmado').val("false");
	$('#divCruzadas').hide();
	$('#btnGuardar').val("Guardar");
	$('#btnGuardar').prop('disabled',false);
}

function eliminarActividad()
{
	var idActividad = $('#txtIdActividad').val();
	$('#txtIdActividadE').val(idActividad);  
}

listarPorOrganizacion = function(){
	var idOrganizacion = $('#selectIdOrganizaciones').val();
	$.post("index.php?c=actividades&a=ConsultaNombreOrganizacion", {idOrganizacion: idOrganizacion}, function(nombreOrganizacion) {
		$("#txtNombreOrganizacion").val(nombreOrganizacion);
	});
	listarNegociosPorOrganizacion(idOrganizacion);
	listarPersonasPorOrganizacion(idOrganizacion);
}

listarNegociosPorOrganizacion = function (idOrganizacion)
{
	datos = {"idOrganizacion":idOrganizacion};
	$.ajax({
		url: "?c=actividades&a=ListarNegociosPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$('#selectNegociosF').show();
		$("#selectIdNegocioF").empty();
		var selector = document.getElementById("selectIdNegocioF");
		selector.options[0] = new Option("Seleccione el negocio de actividad","");
		var j=0;
		for (var i in respuesta) 
		{
			j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
		}
	});
}

listarPersonasPorOrganizacion = function(idOrganizacion){
	datos = {"idOrganizacion":idOrganizacion};
	$.ajax({
		url: "?c=actividades&a=ListarPersonasPorOrganizacion",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$("#selectNombrePersona").empty();
		$('#selectPersonas').show();
		var selector = document.getElementById("selectNombrePersona");
		selector.options[0] = new Option("Seleccione la persona de contacto","");
		var j=0;
		for (var i in respuesta) {
			j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].nombrePersona);
		}
		$('#selectNombrePersona').selectpicker('refresh');
	});
}

obtenerIdNegocioF = function ()
{
	var idNegocio = $('#selectIdNegocioF').val();
	listarPersonasPorNegocio(idNegocio);
}

obtenerIdNegocio = function ()
{
	var idNegocio = $('#selectIdNegocio').val();
	listarPersonasPorNegocio(idNegocio);
}

listarPersonasPorNegocio = function (idNegocio)
{
	datos = {"idNegocio":idNegocio};
	$.ajax({
		url: "index.php?c=actividades&a=ListarPersonasPorNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		console.log(respuesta);
		$("#selectNombrePersona").empty();
		$("#selectPersonas").show();
		var selector = document.getElementById("selectNombrePersona");
		selector.options[0] = new Option("Seleccione la persona de contacto","");
		for (var i in respuesta) {
			var j=parseInt(i)+1;
			selector.options[j] = new Option(respuesta[i].nombrePersona,respuesta[i].nombrePersona);
		}
		$('#selectNombrePersona').selectpicker('refresh');
	});
}

listarOrganizacionesPorNegocio = function ()
{
	var idNegocio = $('#selectIdNegocio').val();
	datos = {"idNegocio":idNegocio};
	$.ajax({
		url: "index.php?c=actividades&a=ObtenerOrganizacionPorNegocio",
		type: "POST",
		data: datos
	}).done(function(respuesta){
		$('#txtNombreOrganizacion').val(respuesta[0].nombreOrganizacion);
		$('#txtIdOrganizacion').val(respuesta[0].idOrganizacion);
		$("#divOrganizaciones").show();
		obtenerIdNegocio();
	})
}

//Metodo para cambiar el estado en la base de datos de acuerdo al idActividad
cambiaEstado = function(idActividad,estado,fechaCompletado)
{
	$('#txtIdActividad2').val(idActividad);
	$('#txtEstado').val(estado);
	if(estado==1){
		$('#labActComp').html("Actividad completada");
		$('#Guardar2').val('Desacompletar');
		$('#txtFechaCompletado').val(fechaCompletado);
	}
	else{
		$('#labActComp').html("Completar actividad");
		$('#Guardar2').val('Completar');
		$('#txtFechaCompletado').val('');
	}
}

var bPeriodo='';
var bActividad='';

consultas = function ()
{ 
	var tAct =  $('#tipoAct').val();
	var bTexto = $("#txtBuscar").val();
	if (bPeriodo==''){
		$.post("index.php?c=actividades&a=Consultas", {bTexto: bTexto, bActividad: bActividad, accion:'tabla', tAct: tAct}, function(mensaje) {
			$("#resultadoBusqueda").html(mensaje);
		});
		$.post("index.php?c=actividades&a=Consultas", {bTexto: bTexto, bActividad: bActividad, accion:'contador', tAct: tAct}, function(noActividades) {
			if (noActividades==1)
				$("#h5Contador").html(noActividades+' Actividad');
			else
				$("#h5Contador").html(noActividades+' Actividades');
		});
	}
	else{
		$.post("index.php?c=actividades&a=Consultas", {
			bTexto: bTexto, 
			periodos: "true", 
			bPeriodo: bPeriodo, 
			bActividad:bActividad,
			accion: 'tabla',
			tAct: tAct 
		}, function(mensaje) {
			$("#resultadoBusqueda").html(mensaje);
		});  
		$.post("index.php?c=actividades&a=Consultas", {
			bTexto: bTexto, 
			periodos: "true", 
			bPeriodo: bPeriodo, 
			bActividad:bActividad,
			accion: 'contador',
			tAct: tAct 
		}, function(noActividades) {
			if (noActividades==1)
				$("#h5Contador").html(noActividades+' Actividad');
			else
				$("#h5Contador").html(noActividades+' Actividades');
		});  
	}
}

filtrarActividades = function (busqueda)
{
	switch(busqueda) {
		case '1':
		if(bPeriodo==1){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=1;
			defaultClassPeriodos();
			$("#btnPlaneado").removeClass("btn-default");
			$("#btnPlaneado").addClass("btn-primary");
		}
		break;
		case '2':
		if(bPeriodo==2){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=2;
			defaultClassPeriodos();
			$("#btnVencida").removeClass("btn-default"); 
			$("#btnVencida").addClass("btn-primary"); 
		}
		break;
		case '3':
		if(bPeriodo==3){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=3;
			defaultClassPeriodos();
			$("#btnHoy").removeClass("btn-default"); 
			$("#btnHoy").addClass("btn-primary"); 
		}
		break;
		case '4':
		if(bPeriodo==4){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=4;
			defaultClassPeriodos();
			$("#btnManana").removeClass("btn-default"); 
			$("#btnManana").addClass("btn-primary"); 
		}
		break;
		case '5':
		if(bPeriodo==5){
			bPeriodo='';
			defaultClassPeriodos(); 
		}else{
			bPeriodo=5;
			defaultClassPeriodos();
			$("#btnEstaSemana").removeClass("btn-default"); 
			$("#btnEstaSemana").addClass("btn-primary"); 
		}
		break;
		case '6':
		if(bPeriodo==6){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=6;
			defaultClassPeriodos();
			$("#btnProximaSemana").removeClass("btn-default"); 
			$("#btnProximaSemana").addClass("btn-primary"); 

		}
		break;
		case '7':
		if(bPeriodo==7){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=7; 
			defaultClassPeriodos();
			$("#btnCompletado").removeClass("btn-default");  
			$("#btnCompletado").addClass("btn-primary");  
		}
		break;
		case '8':
		if(bPeriodo==8){
			bPeriodo='';
			defaultClassPeriodos();
		}else{
			bPeriodo=8;
			defaultClassPeriodos();
			$("#btnLeads").removeClass("btn-default");  
			$("#btnLeads").addClass("btn-primary"); 
		}
		break;
		case 'Llamada':
		if(bActividad=='Llamada'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='Llamada';
			defaultClassActividades();
			$("#btnLlamada").removeClass("btn-default");
			$("#btnLlamada").addClass("btn-primary"); 
		}
		break;
		case 'Reunion':
		if(bActividad=='Reunion'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='Reunion';
			defaultClassActividades();
			$("#btnReunion").removeClass("btn-default"); 
			$("#btnReunion").addClass("btn-primary"); 
		}
		break;
		case 'Tarea':
		if(bActividad=='Tarea'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='Tarea';
			defaultClassActividades();
			$("#btnTarea").removeClass("btn-default"); 
			$("#btnTarea").addClass("btn-primary"); 
		}
		break;
		case 'Plazo':
		if(bActividad=='Plazo'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='Plazo';
			defaultClassActividades();
			$("#btnPlazo").removeClass("btn-default");  
			$("#btnPlazo").addClass("btn-primary"); 
		}
		break;
		case 'Email':
		if(bActividad=='Email'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='Email';
			defaultClassActividades();
			$("#btnEmail").removeClass("btn-default"); 
			$("#btnEmail").addClass("btn btn-primary");
		}
		break;
		case 'Comida':
		if(bActividad=='Comida'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='Comida';
			defaultClassActividades();
			$("#btnComida").removeClass("btn-default"); 
			$("#btnComida").addClass("btn-primary"); 
		}
		break;
		case 'WhatsApp':
		if(bActividad=='WhatsApp'){
			bActividad='';
			defaultClassActividades();
			filtrarActividades(bActividad);
		}else{
			bActividad='WhatsApp';
			defaultClassActividades();
			$("#btnWhatsApp").removeClass("btn-default");  
			$("#btnWhatsApp").addClass("btn-primary"); 
		}
		break;
		default:
		bActividad='';
		defaultClassActividades();
		$("#btnTodas").removeClass("btn-default");
		$("#btnTodas").addClass("btn-primary");
		break;
	}
	consultas();
}

defaultClassActividades=function()
{
	$("#btnTodas").removeClass("btn-primary");
	$("#btnLlamada").removeClass("btn-primary"); 
	$("#btnReunion").removeClass("btn-primary"); 
	$("#btnTarea").removeClass("btn-primary"); 
	$("#btnPlazo").removeClass("btn-primary"); 
	$("#btnEmail").removeClass("btn-primary"); 
	$("#btnComida").removeClass("btn-primary"); 
	$("#btnWhatsApp").removeClass("btn-primary"); 

	$("#btnTodas").addClass("btn-default");
	$("#btnLlamada").addClass("btn-default"); 
	$("#btnReunion").addClass("btn-default"); 
	$("#btnTarea").addClass("btn-default"); 
	$("#btnPlazo").addClass("btn-default"); 
	$("#btnEmail").addClass("btn-default"); 
	$("#btnComida").addClass("btn-default"); 
	$("#btnWhatsApp").addClass("btn-default"); 
}

defaultClassPeriodos=function()
{
	$("#btnPlaneado").removeClass("btn-primary");
	$("#btnVencida").removeClass("btn-primary"); 
	$("#btnHoy").removeClass("btn-primary"); 
	$("#btnManana").removeClass("btn-primary"); 
	$("#btnEstaSemana").removeClass("btn-primary"); 
	$("#btnProximaSemana").removeClass("btn-primary"); 
	$("#btnCompletado").removeClass("btn-primary"); 
	$("#btnLeads").removeClass("btn-primary"); 

	$("#btnPlaneado").addClass("btn-default");
	$("#btnVencida").addClass("btn-default"); 
	$("#btnHoy").addClass("btn-default"); 
	$("#btnManana").addClass("btn-default"); 
	$("#btnEstaSemana").addClass("btn-default"); 
	$("#btnProximaSemana").addClass("btn-default"); 
	$("#btnCompletado").addClass("btn-default"); 
	$("#btnLeads").addClass("btn-default");
}
