var paginaActual = 1;
$(document).ready(function() {
	paginador(paginaActual);

	$('#form_persona').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
        	valid: 'glyphicon glyphicon-ok',
        	invalid: 'glyphicon glyphicon-remove',
        	validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
        	$.ajax({
        		type: 'POST',
        		url: form.attr('action'),
        		data: form.serialize(),
        	}).done(function(respuesta){
        		if (respuesta=="error") {
        			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al guardar la Persona de contacto</center></strong></div>');
        		}else{
        			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
        		}if (respuesta =='') {
        			$('#mPersona').modal('');
        		}else{
        			$('#mPersona').modal('toggle');
        			$('#form_persona').modal('hide');
        			$('#mensajejs').show();
        			$('#mensajejs').delay(3500).hide(600);
        			paginador(paginaActual);
        		}
        	});
        	return false;
        },
        fields: {
        	nombrePersona: {
        		validators: {
        			regexp: {
        				regexp: /^([a-zA-Z ÑñÁÉÍÓÚáéíóú])*$/,
        				message: 'Sólo letras'
        			},
        			notEmpty: {
        				message: 'Campo Obligatorio'
        			}
        		}
        	},
        	telefono: {
        		validators: {
        			regexp: {
        				regexp: /^[0-9]+$/,
        				message: 'Sólo números'
        			} 
        		}
        	},
        	extension: {
        		validators: {
        			stringLength: {
        				min: 1,
        				max: 4,
        				message: 'Mínimo 1 - Máximo 4 caracteres'
        			},
        			regexp: {
        				regexp: /^[0-9]+$/,
        				message: 'Sólo números'
        			} 
        		}
        	},
        	email: {
        		validators: {
              notEmpty: {
                message: 'Campo Obligatorio'
              }
            }
          },
          idOrganizacion: {
            validators: {
             notEmpty: {
              message: 'Campo Obligatorio'
            }
          }
        },
        puesto: {
          validators: {
           regexp: {
            regexp: /^[a-zA-Z0-9 ÑñÁÉÍÓÚáéíóú]*$/,
            message: 'Sólo caracteres alfanuméricos'
          }
        }
      }
    }
  });
	$('#mPersona')
	.on('shown.bs.modal', function() {
		$('#form_persona').find('[name="nombrePersona"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form_persona').bootstrapValidator('resetForm', true);
	});

	$('#form_contactodel').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			if (respuesta=="error") {
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>ERROR! El contacto tiene negocios asociados.</center></strong></div>');
			}else{
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			}
			$('#mPersona').modal('toggle');
			$('#mEliminar').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			paginador(paginaActual);
		});
		return false;
	}); 
});

var bAlfabetica='';
function dibujaTabla(numRegistros, bTexto){ 
	$.post("index.php?c=personas&a=Consultas", {bTexto: bTexto, bAlfabetica: bAlfabetica, registros: numRegistros, pagina: paginaActual}, function(resultado) {
		$("#resultadoBusqueda").html(resultado); 
	});
	$.post("index.php?c=personas&a=VerificaEx", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(resultado) {
		if(resultado == "false")
			$('#btnExportar').attr("disabled",false);               
		else
			$('#btnExportar').attr("disabled",true);
	});
	$.post("index.php?c=personas&a=Contador", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(noContacto) {
		if (noContacto==1)
			$("#h5Contador").html(noContacto+' Contacto');
		else
			$("#h5Contador").html(noContacto+' Contactos'); 
	});
}

function paginador(pagina) {
	paginaActual = pagina;
	var numRegistros = $('#selectPorPagina').val();
	var bTexto = $("#buscar").val();
	dibujaTabla(numRegistros, bTexto);
	switchColor(numRegistros); 
}

function switchColor(numRegistros){
	$.post("index.php?c=personas&a=obtenerRegistros", {}, function(totalRegistros) {
		var totalPaginas = Math.ceil(totalRegistros / numRegistros);
		for (var i = 1; i <= totalPaginas; i++) {
			if (i == paginaActual) {
				$("#li"+i).addClass("active");
			}else{
				$("#li"+i).removeClass("active");
			}
		}
	}); 
}

filtrarPersonas = function (busqueda){
	$('#txtbAlfabetica').val(busqueda);
	bAlfabetica = busqueda;
	switch(busqueda) {
		case 'A':
		desmarcar("a");
		break;
		case 'B':
		desmarcar("b");
		break;
		case 'C':
		desmarcar("c");
		break;
		case 'D':
		desmarcar("d");
		break;
		case 'E':
		desmarcar("e");
		break;
		case 'F':
		desmarcar("f");
		break;
		case 'G':
		desmarcar("g");
		break;
		case 'H':
		desmarcar("h");
		break;
		case 'I':
		desmarcar("i");
		break;
		case 'J':
		desmarcar("j");
		break;
		case 'K':
		desmarcar("k");
		break;
		case 'L':
		desmarcar("l");
		break;
		case 'M':
		desmarcar("m");
		break;
		case 'N':
		desmarcar("n");
		break;
		case 'Ñ':
		desmarcar("ñ");
		break;
		case 'O':
		desmarcar("o");
		break;
		case 'P':
		desmarcar("p");
		break;
		case 'Q':
		desmarcar("q");
		break;
		case 'R':
		desmarcar("r");
		break;
		case 'S':
		desmarcar("s");
		break;
		case 'T':
		desmarcar("t");
		break;
		case 'U':
		desmarcar("u");
		break;
		case 'V':
		desmarcar("v");
		break;
		case 'W':
		desmarcar("w");
		break;
		case 'X':
		desmarcar("x");
		break;
		case 'Y':
		desmarcar("y");
		break;
		case 'Z':
		desmarcar("z");
		break;
		default:
          //bAlfabetica='';
          desmarcar("todas");
          break;
        }
        paginador(paginaActual);
      }

      function listarOrganizaciones(){
       $.ajax({
        url: "index.php?c=personas&a=listarOrganizaciones",
        type: "POST",
      }).done(function(respuesta){
        $("#selectIdOrganizaciones").empty();
        var selector = document.getElementById("selectIdOrganizaciones");
        selector.options[0] = new Option("Seleccione el cliente","");
        for (var i in respuesta) {
         var j = parseInt(i) + 1;
         selector.options[j] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
       }
       $('#selectIdOrganizaciones').selectpicker('refresh');
     }); 
    }

    myFunctionEditarPersona = function (idCliente,honorifico, nombrePersona, idOrganizacion, nombreOrganizacion,telefono,extension, tipoTelefono, email, puesto) {
     $('#txtIdCliente').val(idCliente);  
     $('#txtNomPersona').val(nombrePersona);
     $('#txtTelefono').val(telefono);
     $('#txtExtension').val(extension);
     $('#selectTipoTelefono').val(tipoTelefono);
     $('#txtEmail').val(email);
     $('#selectHonorifico').val(honorifico); 
     $('#txtPuesto').val(puesto);
     $('#btnEliminar').show();
     $('#labTitulo').html("Editar Contacto");

     /*--- Select de organizaciones --- */
     $("#selectIdOrganizaciones").empty();
     var selectOrganizaciones = document.getElementById("selectIdOrganizaciones");
     selectOrganizaciones.options[0] = new Option(nombreOrganizacion , idOrganizacion);

     $.ajax({
      url: "index.php?c=personas&a=ListarOrganizaciones",
    }).done(function(respuesta){
      console.log(respuesta);
      var j=0;
      for (var i in respuesta) {
       if(idOrganizacion!=respuesta[i].idOrganizacion){
        var j=j+1;
        selectOrganizaciones.options[j] = new Option(respuesta[i].nombreOrganizacion,respuesta[i].idOrganizacion);
      }
    }
    $('#selectIdOrganizaciones').selectpicker('refresh');
  });
  }

  function myFunctionEliminar(){
  	var idCliente = $('#txtIdCliente').val();
  	$('#txtIdClienteE').val(idCliente);  
  }

  function desmarcar(letra){
  	var miarray = ["todas", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

  	for (var i = 0; i < miarray.length; i++) {
  		if (letra == miarray[i]) {
  			$("#btn-"+miarray[i]).addClass("btn-primary");
  			$("#btn-"+miarray[i]).removeClass("btn-default");
  		}else{
  			$("#btn-"+miarray[i]).removeClass("btn-primary");
  			$("#btn-"+miarray[i]).addClass("btn-default");
  		}
  	}
  }
  function myFunctionNuevo() {
  	$('#txtIdCliente').val("");
  	$('#selectIdOrganizaciones').val("");
  	$('#txtNomPersona').val("");
  	$('#txtTelefono').val("");
  	$('#txtExtension').val("");
  	$('#selectTipoTelefono').val("");
  	$('#txtEmail').val("");
  	$('#selectHonorifico').val("");
  	$('#txtPuesto').val("");
  	$('#btnEliminar').hide();
  	$('#labTitulo').html("Añadir contacto");
  	listarOrganizaciones();
  }

  function MostrarTelefonos(idCliente, honorifico, nombrePersona, extension, telefono, tipoTelefono) {
    $("#nomPersona").html("<h4><strong>"+honorifico+" "+nombrePersona+"</strong></h4>");
    $("#telPersona").html("<span style='color:#757575'>Tel: ("+extension+") "+telefono+", "+tipoTelefono+"</span>")

    limpiarInputs();
    $('#idCliente').val(idCliente);
    $.post("index.php?c=personas&a=MostrarTelefonos", {idCliente: idCliente}, function(tabla_telefonos) {
      $('#tabla_telefonos').html(tabla_telefonos);
    });
  }

  agregarTelefono = function()
  {
    var idTelefono = $('#idTelefono').val();
    var extension = $('#extension').val();
    var telefono = $('#telefono').val();
    var tipoTelefono = $('#tipoTelefono').val();
    var idCliente = $('#idCliente').val();
    
    if(telefono!="" && tipoTelefono!=""){
      $('#verificador').text("");
      datos = {
        "idTelefono" : idTelefono,
        "extension" : extension,
        "telefono" : telefono,
        "tipoTelefono" : tipoTelefono,
        "idCliente" : idCliente
      };
      $.ajax({
        type: 'POST',
        url: '?c=personas&a=AgregarTelefono',
        data: datos,
      }).done(function(tabla_telefonos){
        $('#tabla_telefonos').html(tabla_telefonos);
        limpiarInputs();
      });
    }else{
      $('#verificador').text("Verifique que el teléfono y el tipo de teléfono esten llenos");
    }
  } 

  function limpiarInputs() {
    $('#idTelefono').val(0);
    $('#extension').val("");
    $('#telefono').val("");
    $('#tipoTelefono').val("");
    $('#verificador').text("");
  }

  editarTelefono = function(idTelefono, extension, telefono, tipoTelefono)
  {
    $('#idTelefono').val(idTelefono);
    $('#extension').val(extension);
    $('#telefono').val(telefono);
    $('#tipoTelefono').val(tipoTelefono);
  } 

  quitarTelefono = function(idTelefono)
  {
   var idCliente = $('#idCliente').val();
   datos = {
    "idTelefono" : idTelefono,
    "idCliente" : idCliente
  };
  $.ajax({
    type: 'POST',
    url: '?c=personas&a=QuitarTelefono',
    data: datos,
  }).done(function(tabla_telefonos){
    $('#tabla_telefonos').html(tabla_telefonos);
    limpiarInputs();
  });
} 

