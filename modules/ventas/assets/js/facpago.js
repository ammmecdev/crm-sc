$(document).ready(function() {
	verFacturas();

	/*Validdores Nueva Factura*/
	$('#form_factura').bootstrapValidator({
		submitHandler: function(validator, form, submitButton) {
			/*Aquí va el Ajax para el método submit del formulario (Sustituir $(this). por form.)*/
			var data = new FormData();
			var idFactura = $('#idFactura').val();
			var selectCotizacion = $('#selectCotizacion').val();
			var noFactura = $('#noFactura').val();
			var fechaFactura = $('#fechaFactura').val();
			var factura = $('#factura')[0].files[0];

			data.append('idFactura',idFactura);
			data.append('selectCotizacion',selectCotizacion);
			data.append('noFactura',noFactura);
			data.append('fechaFactura',fechaFactura);
			data.append('factura',factura);

			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: data,
				cache: false,
				contentType: false,
				processData: false,
			}).done(function(respuesta){
				
					$('#mFactura').modal('toggle');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					
					verFacturas();
			});
			return false;
		},
		fields: {
			/*Name de los campos a validar*/
		}
	});
	$('#mFactura')
	.on('shown.bs.modal', function() {
		$('#form_factura').find('[name="selectCotizacion"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form_factura').bootstrapValidator('resetForm', true);
	});
	/*Validdores Nueva Factura*/


	/*Validdores Nuevo Pago*/
	$('#form_pago').bootstrapValidator({
		/*feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},*/
		submitHandler: function(validator, form, submitButton) {
			/*Aquí va el Ajax para el método submit del formulario (Sustituir $(this). por form.)*/
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
			}).done(function(respuesta){
				if (respuesta=="error") {
					$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al guardar el pago</center></strong></div>');
				}else{
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
				}if (respuesta =='') {
					$('#mPago').modal('');
				}else{
					$('#mPago').modal('toggle');
					$('#form_pago').modal('hide');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
				}
			});
			return false;
		},
		fields: {
			/*Name de los campos a validar*/
		}
	});
	$('#mPago')
	.on('shown.bs.modal', function() {
		$('#form_pago').find('[name="claveCotizacion"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form_pago').bootstrapValidator('resetForm', true);
	});
	/*Validdores Nuevo Pago*/
});

function verFacturas(){
	$("#btnFacturas").addClass("active");
	$("#btnFacturas2").addClass("active");
	$("#btnPagos").removeClass("active");
	$("#btnPagos").removeClass("active");
	$("#pnlPagos").hide();
	$("#pnlFacturas").show();
	switchColor('A');
}

function verPagos(){
	$("#btnPagos").addClass("active");
	$("#btnPagos2").addClass("active");
	$("#btnFacturas").removeClass("active");
	$("#btnFacturas2").removeClass("active");
	$("#pnlFacturas").hide();
	$("#pnlPagos").show();
	switchColor('B');
}
function myFunctionNuevaFactura() {
	$('#idFactura').val("");
	$('#selectCotizacion').val("");
	$('#noFactura').val("");
	$('#fechaFactura').val("");
	$('#factura').val("");
	$('#labTitulo').html("Nueva Factura");
	listarNego();
}
function myFunctionNuevoPago() {
	$('#idPago').val("");
	$('#selectCotizacionPago').val("");
	$('#fechaPago').val("");
	$('#fechaRecibo').val("");
	$('#noPago').val("");
	$('#pago').val("");
	$('#labTitulo').html("Nuevo Pago");
	listarNegoP();
}
function switchColor(option){
	switch(option) {
		case 'A':
		$("#btnFacturas").addClass("btn-primary");
		$("#btnFacturas").removeClass("btn-default");

		$("#btnFacturas2").addClass("btn-primary");
		$("#btnFacturas2").removeClass("btn-default");

		$("#btnPagos").removeClass("btn-primary");
		$("#btnPagos").addClass("btn-default");

		$("#btnPagos2").removeClass("btn-primary");
		$("#btnPagos2").addClass("btn-default");

		break;
		case 'B':
		$("#btnPagos").addClass("btn-primary");
		$("#btnPagos").removeClass("btn-default");

		$("#btnPagos2").addClass("btn-primary");
		$("#btnPagos2").removeClass("btn-default");

		$("#btnFacturas").removeClass("btn-primary");
		$("#btnFacturas").addClass("btn-default");

		$("#btnFacturas2").removeClass("btn-primary");
		$("#btnFacturas2").addClass("btn-default");
		default:
		break;
	}
}

$(window).resize(function(){
	if ($(window).width()<767) {
		$("#Busqueda").removeClass("btn-group");
	}else {
		$("#Busqueda").addClass("btn-group");
	}
});

function listarNego(){
	$.ajax({
		url: "index.php?c=facpago&a=listarNegocios1",
		type: "POST",
	}).done(function(respuesta){
		$("#selectCotizacion").empty();
		var selector = document.getElementById("selectCotizacion");
		selector.options[0] = new Option("Seleccione el negocio","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
		}
		$('#selectCotizacion').selectpicker('refresh');
	}); 
}
function listarNegoP(){
	$.ajax({
		url: "index.php?c=facpago&a=listarNegocios1",
		type: "POST",
	}).done(function(respuesta){
		$("#selectCotizacionPago").empty();
		var selector = document.getElementById("selectCotizacionPago");
		selector.options[0] = new Option("Seleccione el negocio","");
		for (var i in respuesta) {
			var j = parseInt(i) + 1;
			selector.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
		}
		$('#selectCotizacionPago').selectpicker('refresh');
	}); 
}

myFunctionEditarFactura = function (idFactura, selectCotizacion, noFactura, fechaFactura) {
	alert (idFactura);
	alert (noFactura);
	alert (fechaFactura);
	$('#idFactura').val(idFactura);
	$('#noFactura').val(noFactura);
	$('#fechaFactura').val(fechaFactura);
	$('#labTitulo').html("Editar Factura");

	$("#selectCotizacion").empty();
  	var selectCotizacion = document.getElementById("selectCotizacion");
  	selectCotizacion.options[0] = new Option(tituloNegocio , idCotizacion);

  	$.ajax({
  		url: "index.php?c=facpago&a=listarNegocios1",
  	}).done(function(respuesta){
  		//console.log(respuesta);
  		var j=0;
  		for (var i in respuesta) {
  			if(idCotizacion!=respuesta[i].idCotizacion){
  				var j=j+1;
  				selectCotizacion.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idCotizacion);
  			}
  		}
  		$('#selectCotizacion').selectpicker('refresh');
  	});
}