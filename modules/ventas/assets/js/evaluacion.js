$(document).ready(function() {

	$('#form_evaluacion').bootstrapValidator({
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					respuesta = JSON.parse(respuesta);
					$('#mEvaluacion').modal('toggle');
					$('#form_evaluacion').trigger("reset");
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta[0].mensaje+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					llenarSelectAnio();
					consultas();
					
				}
			});      
			return false;
		},
		fields:{
			dateFechaEvaluacion: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			optUno:{
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			optDos:{
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			optTres:{
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			optCuatro:{
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			optCinco:{
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			},
			optSeis:{
				validators: {
					notEmpty: {
						message: 'Selección Obligatoria'
					}
				}
			}
		}
		
	});

	$('#form-eliminar').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$('#mEvaluacionEliminar').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			llenarSelectAnio();
			consultas();
		});
		return false;
	}); 
	consultas();
	llenarSelects();
	llenarSelectAnio();
	listenersOfOptions();
	$('#mEvaluacion')
	.on('shown.bs.modal', function() {
		
	});
	$('#mEvaluacion')
	.on('hidden.bs.modal', function () {
		
		$('input[name="optUno"]').attr('checked', false);
		$('input[name="optDos"]').attr('checked', false);
		$('input[name="optTres"]').attr('checked', false);
		$('input[name="optCuatro"]').attr('checked', false);
		$('input[name="optCinco"]').attr('checked', false);
		$('input[name="optSeis"]').attr('checked', false);
		$('#idEvaluacion').val(0);

		$('#form_evaluacion').trigger("reset");
		$('#form_evaluacion').bootstrapValidator('resetForm', true);
		
	});

});

consultas = function ()
{ 
	var bTexto = $("#vBusqueda").val();
	var mes = $( "#selectMes option:selected" ).val();
	var anio = $( "#selectAnio option:selected" ).val();
	
	$.post("index.php?c=evaluacion&a=Consultas", { accion:'tabla',bTexto:bTexto,mes:mes,anio:anio}, function(resultado) {

		resultado = JSON.parse(resultado);

		var tabla = '<div class="porlets-content">'+
		'<div class="table-responsive">'+
		'<table class="display table table-bordered table-hover" id="dynamic-table">'+
		'<thead>'+
		'<tr style="background-color: #FCF3CF;">'+
		'<td align="center"><strong>Acción</strong></td>'+
		'<td><strong>Contacto</strong></td>'+
		'<td><strong>Cliente</strong></td>'+
		'<td><strong>Fecha</strong></td>'+
		//'<td><strong>Evaluación</strong></td>'+
		'</tr>'+
		'</thead>';

		if(resultado.length==0){
			tabla += '<tr><td class="alert-danger" colspan="10" align="center"> <strong>No se encontraron evaluaciones de servicio </strong></td></tr>';
		}else{
			for(let i in resultado){
				
				var evaluacion = JSON.stringify(resultado[i]);
				evaluacion = evaluacion.replace(/\"/g,"'");
				tabla+='<tr>';
				/*tabla+='<td align="center">'+
				'<div class="btn-group" role="group">'+
				'<button style="margin-top: -10px;" id="btnGroupDrop1" type="button" class="btn btn-light btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fas fa-ellipsis-h"></i></button>'+
				'<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="min-width:120px;">'+
				'<a class="dropdown-item" href="#"><i class="fa fa-trash" style="color:#dc3545!important"></i> Borrar</a>'+
				'<a class="dropdown-item" href="#"><i class="fa fa-edit" style="color:#0062cc!important"></i> Editar</a>'+
				'</div>'+
				'</div>'+
				'</td>';*/
				tabla+='<td align="center" style="white-space: nowrap;">'+
				/*'<div class="dropdown show ">'+
					'<a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
						'<i class="glyphicon glyphicon-list"></i>'+
					'</a>'+
				'<div class="dropdown-menu " style="min-width:150px" aria-labelledby="dropdownMenuLink">'+*/
					//'<ul class="dropdown-item" style="width:100%"><button   id="btnEditar" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mEvaluacion" onclick="editarEvaluacion('+evaluacion+')"> <span class="glyphicon glyphicon-pencil"></span> </button> Actualizar</ul> '+
					'<button   id="btnEditar" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mEvaluacion" onclick="editarEvaluacion('+evaluacion+')"> <span class="glyphicon glyphicon-pencil"></span> </button> '+
						
					//'<ul class="dropdown-item" style="width:100%"><button   class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mEvaluacionEliminar" onclick="rellenarCampoID('+resultado[i].idEvaluacion+')"><i class="glyphicon glyphicon-trash"></i> </button> Eliminar</ul> '+
					'<button   class="btn btn-danger btn-xs" data-toggle="modal" data-target="#mEvaluacionEliminar" onclick="rellenarCampoID('+resultado[i].idEvaluacion+')"><i class="glyphicon glyphicon-trash"></i> </button> '+
					
					//'<ul class="dropdown-item" style="width:100%"><a  class="btn btn-success btn-xs" href="?c=reportes&a=ExportarEvaluacion&idEvaluacion='+resultado[i].idEvaluacion+'" target="_blank"><i class="fas fa-print"></i> </a> </ul> Ver PDF</div></td>'; 
					'<a  class="btn btn-success btn-xs" href="?c=reportes&a=ExportarEvaluacion&idEvaluacion='+resultado[i].idEvaluacion+'" target="_blank"><i class="fas fa-print"></i> </a> '+
					'<a href=""class="btn btn-xs btn-primary" data-toggle="modal" data-target="#mAspectosEvaluacion" onclick="aspectosEvaluacionData('+resultado[i].idEvaluacion+')"><i class="fas fa-list"></i></a>';
				//'<button class="btn btn-primary btn-xs" onclick="solicitarPDF('+resultado[i].idEvaluacion+')" target="_blank"><i class="glyphicon glyphicon-file"></i></button></td>';
				tabla+='<td>'+resultado[i].honorifico + ' '+resultado[i].nombrePersona+'</td>'+
						'<td>'+resultado[i].nombreOrganizacion+'</td>'+
						'<td>'+resultado[i].fechaEvaluacion+'</td>';
				//tabla+='<td><a href=""class="btn btn-xs btn-primary" data-toggle="modal" data-target="#mAspectosEvaluacion" onclick="aspectosEvaluacionData('+resultado[i].idEvaluacion+')"><i class="fas fa-list"></i></a></td>';
				tabla+="</tr>";
				
			}
		}
		tabla+= '</table></div></div>';
		$("#resultadoBusqueda").html(tabla);
	});
	$.post("index.php?c=evaluacion&a=Consultas", { accion:'contador',bTexto:bTexto,mes:mes,anio:anio}, function(noEvaluaciones) {
		if (noEvaluaciones==1)
			$("#h5Contador").html(noEvaluaciones+' Evaluación');
		else
			$("#h5Contador").html(noEvaluaciones+' Evaluaciones');
	});
	
}



function rellenarCampoID(idEvaluacion){
	$("input[name=idEvaluacionEliminar]").val(idEvaluacion);
}

function aspectosEvaluacionData(idEvaluacion){
	$.post("index.php?c=evaluacion&a=AspectosEvaluacion",{idEvaluacion:idEvaluacion}, function(resultado){
		var tabla ='<table class="display table table-bordered table-hover" id="dynamic-table">';

		tabla+='<tr>';
		tabla+='<td><strong>Aspecto</strong></td>';
		tabla+='<td><strong>Valor</strong></td>';
		tabla+="</tr>"; 

		resultado = JSON.parse(resultado);
		for(let i in resultado){
			tabla+='<tr>';
			tabla+='<td>'+resultado[i].aspecto+'</td>';
			tabla+='<td>'+resultado[i].valor+'</td>';
			tabla+="</tr>";
		}
		
		tabla+='</table>';
		
		if(resultado[1].observaciones != null){
			tabla+='<div><strong>Observaciones</strong>: '+resultado[1].observaciones+'</div>';
		}
		$("#resultadoAspectosModal").html(tabla);
	});
}

function llenarSelectAnio(){
	var anios = [];
	$.post("index.php?c=evaluacion&a=Consultas", { accion:'tabla',bTexto:"",mes:"",anio:""}, function(resultado) {
		resultado = JSON.parse(resultado);
		for(let i in resultado){
			var anio = parseInt(resultado[i].fechaEvaluacion);
			if(!anios.includes(anio)){
				anios.push(anio);
				
			}
		}
		$('#selectAnio').empty();
		var selector = document.getElementById("selectAnio");
		selector.options[0] = new Option("Año","");
		for (i = 0; i < anios.length; i++) { 
			var option = new Option(anios[i],anios[i]);
			
			$(option).html(anios[i]);
			$("#selectAnio").append(option);
			
		}
		$("#selectAnio").selectpicker('refresh');
	});
}


function llenarSelects(){
	

	$.post("index.php?c=personas&a=ListarOrganizaciones",{}, function(resultado){
		var selector = document.getElementById("selectOrganizaciones");
		selector.options[0] = new Option("Seleccione el Cliente","");
		for(let i in resultado){

			var option = new Option(resultado[i].nombreOrganizacion,resultado[i].idOrganizacion);
			
			$(option).html(resultado[i].nombreOrganizacion);
			$("#selectOrganizaciones").append(option);
			
		}
		$("#selectOrganizaciones").selectpicker('refresh');
		$("#selectOrganizaciones").on('change',function() {
			$.post("index.php?c=personas&a=ListarPersonasDeOrganizacion",{idOrganizacion:$( "#selectOrganizaciones option:selected" ).val()}, function(resultado){
				$('#selectPersonas').empty()
				for(let i in resultado){

					var option = new Option(resultado[i].nombrePersona,resultado[i].idCliente);
					
					$(option).html(resultado[i].nombrePersona);
					$("#selectPersonas").append(option);
				}

				$("#selectPersonas").selectpicker('refresh');
				
			});
		});

	});

	
}

function editarEvaluacion(datos){
	$("#idEvaluacion").val(datos.idEvaluacion);
	$("#selectOrganizaciones").val(datos.idOrganizacion);
	$("#selectOrganizaciones").selectpicker('refresh');

	$.post("index.php?c=personas&a=ListarPersonasDeOrganizacion",{idOrganizacion:datos.idOrganizacion}, function(resultado){
		$('#selectPersonas').empty();
		for(let i in resultado){

			var option = new Option(resultado[i].nombrePersona,resultado[i].idCliente);
			
			$(option).html(resultado[i].nombrePersona);
			$("#selectPersonas").append(option);
		}
		$("#selectPersonas").val(datos.idContacto);
		$("#selectPersonas").selectpicker('refresh');
		
	});
	
	

	$("#dateFechaEvaluacion").val(datos.fechaEvaluacion);
	$("#txtNotas").val(datos.observaciones);
	$("#promediotxt").val(datos.promedio);
	
	$.post("index.php?c=evaluacion&a=AspectosEvaluacion",{idEvaluacion:datos.idEvaluacion}, function(resultado){
		resultado = JSON.parse(resultado);
		switch(parseInt(resultado[0].valor)){
			case 1:
				$("#p1r1").attr('checked',true);
				break;
			case 2:
				$("#p1r2").attr('checked',true);
				break;
			case 3:
				$("#p1r3").attr('checked',true);
				break;
			case 4:
				$("#p1r4").attr('checked',true);
				break;
			case 5:
				$("#p1r5").attr('checked',true);
				break;
		}

		switch(parseInt(resultado[1].valor)){
			case 1:
				$("#p2r1").attr('checked',true);
				break;
			case 2:
				$("#p2r2").attr('checked',true);
				break;
			case 3:
				$("#p2r3").attr('checked',true);
				break;
			case 4:
				$("#p2r4").attr('checked',true);
				break;
			case 5:
				$("#p2r5").attr('checked',true);
				break;
		}

		switch(parseInt(resultado[2].valor)){
			case 1:
				$("#p3r1").attr('checked',true);
				break;
			case 2:
				$("#p3r2").attr('checked',true);
				break;
			case 3:
				$("#p3r3").attr('checked',true);
				break;
			case 4:
				$("#p3r4").attr('checked',true);
				break;
			case 5:
				$("#p3r5").attr('checked',true);
				break;
		}

		switch(parseInt(resultado[3].valor)){
			case 1:
				$("#p4r1").attr('checked',true);
				break;
			case 2:
				$("#p4r2").attr('checked',true);
				break;
			case 3:
				$("#p4r3").attr('checked',true);
				break;
			case 4:
				$("#p4r4").attr('checked',true);
				break;
			case 5:
				$("#p4r5").attr('checked',true);
				break;
		}

		switch(parseInt(resultado[4].valor)){
			case 1:
				$("#p5r1").attr('checked',true);
				break;
			case 2:
				$("#p5r2").attr('checked',true);
				break;
			case 3:
				$("#p5r3").attr('checked',true);
				break;
			case 4:
				$("#p5r4").attr('checked',true);
				break;
			case 5:
				$("#p5r5").attr('checked',true);
				break;
		}

		switch(parseInt(resultado[5].valor)){
			case 1:
				$("#p6r1").attr('checked',true);
				break;
			case 2:
				$("#p6r2").attr('checked',true);
				break;
			case 3:
				$("#p6r3").attr('checked',true);
				break;
			case 4:
				$("#p6r4").attr('checked',true);
				break;
			case 5:
				$("#p6r5").attr('checked',true);
				break;
		}
	});
	
}

function solicitarPDF(idEvaluacion){
	
	$.ajax({
        type: "POST", 
        url: "index.php?c=reportes&a=ExportarEvaluacion",
        data: "idEvaluacion=" + idEvaluacion,
		
        success: function(data){
            var win = window.open();
            win.document.write(data);
        }
    }) 
	$.post("index.php?c=reportes&a=ExportarEvaluacion",{idEvaluacion:idEvaluacion}, function(resultado){
		var w = window.open('about:blank','pdfEvaluacion');
		w.document.write(resultado);
	});
}

function listenersOfOptions(){

	//listener para el grupo de option 1
	$('input[type=radio][name=optUno]').change(function(){
		var promedio = 0;
		promedio += Number(this.value);

		if($('input[type=radio][name=optDos]').is(':checked')){
			promedio += Number($('input[type=radio][name=optDos]:checked').val());
		}

		if($('input[type=radio][name=optTres]').is(':checked')){
			promedio += Number($('input[type=radio][name=optTres]:checked').val());
		}

		if($('input[type=radio][name=optCuatro]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCuatro]:checked').val());
		}

		if($('input[type=radio][name=optCinco]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCinco]:checked').val());
		}
		
		if($('input[type=radio][name=optSeis]').is(':checked')){
			promedio += Number($('input[type=radio][name=optSeis]:checked').val());
		}

		promedio = promedio / 3.0;

		actualizarPromedio(promedio);
	});

	//listener para el grupo de option 2
	$('input[type=radio][name=optDos]').change(function(){
		var promedio = 0;
		promedio += Number(this.value);
		if($('input[type=radio][name=optUno]').is(':checked')){
			promedio += Number($('input[type=radio][name=optUno]:checked').val());
		}

		if($('input[type=radio][name=optTres]').is(':checked')){
			promedio += Number($('input[type=radio][name=optTres]:checked').val());
		}

		if($('input[type=radio][name=optCuatro]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCuatro]:checked').val());
		}

		if($('input[type=radio][name=optCinco]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCinco]:checked').val());
		}
		
		if($('input[type=radio][name=optSeis]').is(':checked')){
			promedio += Number($('input[type=radio][name=optSeis]:checked').val());
		}

		promedio = promedio / 3.0;

		
		actualizarPromedio(promedio);
	});

	//listener para el grupo de option 3
	$('input[type=radio][name=optTres]').change(function(){
		var promedio = 0;
		promedio += Number(this.value);
		if($('input[type=radio][name=optUno]').is(':checked')){
			promedio += Number($('input[type=radio][name=optUno]:checked').val());
		}

		if($('input[type=radio][name=optDos]').is(':checked')){
			promedio += Number($('input[type=radio][name=optDos]:checked').val());
		}

		if($('input[type=radio][name=optCuatro]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCuatro]:checked').val());
		}

		if($('input[type=radio][name=optCinco]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCinco]:checked').val());
		}
		
		if($('input[type=radio][name=optSeis]').is(':checked')){
			promedio += Number($('input[type=radio][name=optSeis]:checked').val());
		}

		promedio = promedio / 3.0;

		actualizarPromedio(promedio);
	});

	//listener para el grupo de option 4
	$('input[type=radio][name=optCuatro]').change(function(){
		var promedio = 0;
		promedio += Number(this.value);
		if($('input[type=radio][name=optUno]').is(':checked')){
			promedio += Number($('input[type=radio][name=optUno]:checked').val());
		}

		if($('input[type=radio][name=optTres]').is(':checked')){
			promedio += Number($('input[type=radio][name=optTres]:checked').val());
		}

		if($('input[type=radio][name=optDos]').is(':checked')){
			promedio += Number($('input[type=radio][name=optDos]:checked').val());
		}

		if($('input[type=radio][name=optCinco]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCinco]:checked').val());
		}
		
		if($('input[type=radio][name=optSeis]').is(':checked')){
			promedio += Number($('input[type=radio][name=optSeis]:checked').val());
		}

		promedio = promedio / 3.0;

		actualizarPromedio(promedio);
	});

	//listener para el grupo de option 5
	$('input[type=radio][name=optCinco]').change(function(){
		var promedio = 0;
		promedio += Number(this.value);
		if($('input[type=radio][name=optUno]').is(':checked')){
			promedio += Number($('input[type=radio][name=optUno]:checked').val());
		}

		if($('input[type=radio][name=optTres]').is(':checked')){
			promedio += Number($('input[type=radio][name=optTres]:checked').val());
		}

		if($('input[type=radio][name=optCuatro]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCuatro]:checked').val());
		}

		if($('input[type=radio][name=optDos]').is(':checked')){
			promedio += Number($('input[type=radio][name=optDos]:checked').val());
		}
		
		if($('input[type=radio][name=optSeis]').is(':checked')){
			promedio += Number($('input[type=radio][name=optSeis]:checked').val());
		}

		promedio = promedio / 3.0;

		actualizarPromedio(promedio);
	});

	//listener para el grupo de option 6
	$('input[type=radio][name=optSeis]').change(function(){
		var promedio = 0;
		promedio += Number(this.value);
		if($('input[type=radio][name=optUno]').is(':checked')){
			promedio += Number($('input[type=radio][name=optUno]:checked').val());
		}

		if($('input[type=radio][name=optTres]').is(':checked')){
			promedio += Number($('input[type=radio][name=optTres]:checked').val());
		}

		if($('input[type=radio][name=optCuatro]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCuatro]:checked').val());
		}

		if($('input[type=radio][name=optCinco]').is(':checked')){
			promedio += Number($('input[type=radio][name=optCinco]:checked').val());
		}
		
		if($('input[type=radio][name=optDos]').is(':checked')){
			promedio += Number($('input[type=radio][name=optDos]:checked').val());
		}

		promedio = promedio / 3.0;

		actualizarPromedio(promedio);
	});

	
}

function actualizarPromedio(promedio){
	var with2Decimals = promedio.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
	$('#promediotxt').val(with2Decimals);
}
