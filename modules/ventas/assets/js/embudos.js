$(document).ready(function() {
	/*Validadores Añadir Embudo*/
	$('#form-embudo').bootstrapValidator({
		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
		excluded: [':disabled', ':hidden', ':not(:visible)'],
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			var idEmbudo = $("#txtIdEmbudo").val();
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					$('#mEmbudo').modal('toggle');
					var valor = "";
					if (!isNaN(respuesta)) {
						valor = respuesta;
						$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>El registro de embudo se realizó correctamente</center></strong></div>');
					}else{
						valor = idEmbudo;
						$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
					}
					$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: valor}, function(mensaje){
						$("#tabsEmbudo").html(mensaje);
					});
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
				}
			})      
			return false;
		},
		fields: {
			nombre: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
		}
	});
	$('#mEmbudo')
	.on('shown.bs.modal', function() {
		$('#form-embudo').find('[name="nombre"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form-embudo').bootstrapValidator('resetForm', true);
	});
	/*Fin Validadores Añadir Embudo*/


	/*Validadores Añadir Etapa*/
	$('#form-etapa').bootstrapValidator({
		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
		excluded: [':disabled', ':hidden', ':not(:visible)'],
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			var idEmbudo = $("#txtIdEmbudoEtapa").val();
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta){
					$('#mEtapa').modal('toggle');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
					$('#mensajejs').show();
					$('#mensajejs').delay(3500).hide(600);
					$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje){
						$("#tabsEmbudo").html(mensaje);
					}); 
				}
			})      
			return false;
		},
		fields: {
			nombreEtapa: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			probabilidad: {
				validators: {
					regexp: {
						regexp: /^[0-9]+$/,
						message: 'Sólo números'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
		}
	});
	$('#mEtapa')
	.on('shown.bs.modal', function() {
		$('#form-etapa').find('[name="nombreEtapa"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form-etapa').bootstrapValidator('resetForm', true);
	});
	/*Fin Validadores Añadir Etapa*/
});
function myFunctionNuevo() {
	$('#txtIdEmbudo').val(0);
	$('#txtNombre').val("");
	$('#btnGuardar').val("Guardar");
	$('#labTitulo').html("Añadir Embudo");
}

function myFunctionEditar($idEmbudo, $nombre) {
	$('#txtIdEmbudo').val($idEmbudo);
	$('#txtNombre').val($nombre);
	$('#btnGuardar').val("Actualizar");
	$('#labTitulo').html("Editar Embudo");
}

function myFunctionEliminar($idEmbudo){
	$('#txtIdEmbudoE').val($idEmbudo);  
	$('#btnEliminarEmbudo').show();
}

$('#form-emEliminar').submit(function() {
	var idEmbudo = $('#txtIdEmbudoE').val();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function(respuesta){
			console.log(respuesta);
			$('#mEliminar').modal('toggle');
			var valor = "";
			if (!isNaN(respuesta)){
				valor = respuesta;
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha eliminado correctamente el embudo </center></strong></div>');
			}else{
				valor = idEmbudo;
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			}
			$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: valor}, function(mensaje){
				$("#tabsEmbudo").html(mensaje);
			});
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
		}
	})
	return false;
});

function myFunctionNuevoEtapa($idEmbudo) {
	$('#txtIdEmbudoEtapa').val($idEmbudo);
	$('#txtIdEtapa').val("");
	$('#txtNombreEtapa').val("");
	$('#txtProbabilidad').val("");
	$('#radioEstancandose').show();
	$('#proNegocio').show();
	$('#labProNegocio').show();
	$('#divEtapa').show();
	$('#btnEliminarEtapa').hide();
	$('#btnGuardarEtapa').val("Guardar");
	$('#labTituloEtapa').html("Añadir Etapa");
	myFunctionChecked('no'); 
}

function myFunctionEditarEtapa($idEmbudo, $idEtapa, $nombreEtapa, $probabilidad, $inactividad) {
	$('#txtIdEmbudoEtapa').val($idEmbudo);
	$('#txtIdEtapa').val($idEtapa);
	$('#txtNombreEtapa').val($nombreEtapa);
	$('#txtProbabilidad').val($probabilidad);
	$('#txtInactividad').prop('value', '0');
	$('#radioEstancandose').show();
	$('#proNegocio').show();
	$('#labProNegocio').show();
	$('#divEtapa').show();
	$('#btnEliminarEtapa').show();
	$('#btnGuardarEtapa').val("Actualizar");
	$('#labTituloEtapa').html("Editar Etapa");
	myFunctionChecked('si', $inactividad);
}

function myFunctionEliminarEtapa(){
	var idEtapa = $('#txtIdEtapa').val();
	var idEmbudo = $('#txtIdEmbudoEtapa').val();
	$('#txtIdEtapaME').val(idEtapa); 
}

$('#form-etEliminar').submit(function() {
	var idEmbudo = $("#txtIdEmbudoEtapa").val();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function(respuesta){
			$('#mEliminarEtapa').modal('toggle');
			$('#mEtapa').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			$.post("index.php?c=negocios&a=tabsEmbudo", {valorIdEmbudo: idEmbudo}, function(mensaje){
				$("#tabsEmbudo").html(mensaje);
			}); 
		}
	})      
	return false;
});

function myFunctionChecked($checked, $inactividad){
	if($checked == "si" && $inactividad > 0){
		$('#txtCheckedSi').prop('checked', true);
		$('#txtCheckedNo').prop('checked', false);
		$('#txtInactividad').prop('value', $inactividad);
		$('#labDiasInactividad').show();
		$('#diasInactividad').show();
	}else if ($checked == "si" && $inactividad == 0){
		$('#txtCheckedNo').prop('checked', true);
		$('#txtCheckedSi').prop('checked', false);
		$('#txtInactividad').prop('value', '0');
		$('#labDiasInactividad').hide();
		$('#diasInactividad').hide(); 
	} else if ($checked == "si" && $inactividad == 'si'){
		$('#txtCheckedSi').prop('checked', true);
		$('#txtCheckedNo').prop('checked', false);
		$('#txtInactividad').prop('value', '');
		$('#labDiasInactividad').show();
		$('#diasInactividad').show();
	}else{
		$('#txtCheckedNo').prop('checked', true);
		$('#txtCheckedSi').prop('checked', false);
		$('#txtInactividad').prop('value', '0');
		$('#labDiasInactividad').hide();
		$('#diasInactividad').hide(); 
	} 
} 
$(function() {
	$( ".sortable" ).sortable({
		update: function(event, ui) {
			$(".ajax-loader").show();
			var orden = $(this).sortable('toArray').toString();
			$.ajax({
				url: "index.php?c=negocios&a=Reordenar",
				type: "POST",
				data: {"data": orden}
			}).done(function(data) {
              //console.log(JSON.stringify(data));
          });
			$(".ajax-loader").hide();
		}
	});
	$( ".sortable" ).disableSelection();
});
