$(document).ready(function() {
	pintarTabla();

	$('#form-equipo').bootstrapValidator({
		// To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
		excluded: [':disabled', ':hidden', ':not(:visible)'],
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
				success: function(respuesta) {
					//console.log(respuesta);
					$('#mEquipo').modal('toggle');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
					$('#mensajejs').show();
				    $('#mensajejs').delay(3500).hide(600);
					pintarTabla();
				}    
			});        
			return false;
		},
		fields: {
			nombreEquipo: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			},
			clave: {
				validators: {
					// stringLength: {
					// 	min: 3,
					// 	max: 3,
					// 	message: 'Mínimo 3 - Máximo 3 letras'
					// },
					regexp: {
						regexp: /^([A-Z])*$/,
						message: 'Sólo letras en mayúsculas'
					},
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
		}
	});
	$('#mEquipo')
	.on('shown.bs.modal', function() {
		$('#form-equipo').find('[name="clave"]').focus();
	})
	.on('hidden.bs.modal', function () {
		$('#form-equipo').bootstrapValidator('resetForm', true);
	});

	//Función para eliminar negocios por ajax
	$('#form-eliminar').submit(function() {
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(respuesta) {
				$('#mEliminar').modal('toggle');
				$('#mEquipo').modal('toggle');
				$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);  
				pintarTabla();
			}    
		})        
		return false;
	}); 
});

var bAlfabetica='';
pintarTabla = function ()
{
	var bTexto = $("#txtBuscar").val();
	$.post("index.php?c=equipo&a=Consultas", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(tabla) {
		$("#crearTabla").html(tabla);	
	});

	$.post("index.php?c=equipo&a=verExportar", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(resultado) {
		if (resultado == "true" )
			$("#btnExportar").attr("disabled", false);
		else
			$("#btnExportar").attr("disabled", true);	
	});

	$.post("index.php?c=equipo&a=Contador", {bTexto: bTexto, bAlfabetica: bAlfabetica}, function(noEquipo) {
		if (noEquipo==1)
			$("#h5Contador").html(noEquipo+' equipo');
		else
			$("#h5Contador").html(noEquipo+' equipos');	
	})
}

myFunctionEditar = function(idEquipo, clave, nombreEquipo){
	$('#txtEquipo').val(idEquipo);  
	$('#txtClave').val(clave);
	$('#txtNombreEquipo').val(nombreEquipo); 
	$('#btnEliminar').show();
	$('#labTitulo').html("Editar Equipo");
	$('#btnGuardar').val("Actualizar");   
}

nuevoEquipo = function (){
	consecutivo();
	$('#txtEquipo').val('');  
	$('#txtClave').val('');
	$('#txtNombreEquipo').val(''); 
	$('#btnEliminar').hide();
	$('#labTitulo').html("Añadir Equipo Nuevo"); 
	$('#btnGuardar').val("Guardar");
}

function modalEliminar()
{
	var idEquipo = $('#txtEquipo').val();
	$('#txtIdEquipo').val(idEquipo);  
}

function filtrarEquipo(busqueda){
	$('#txtBAlfabetica').val(busqueda);
	bAlfabetica = busqueda;
	switch(busqueda) {
		case 'A':
		desmarcar("a");
		break;
		case 'B':
		desmarcar("b");
		break;
		case 'C':
		desmarcar("c");
		break;
		case 'D':
		desmarcar("d");
		break;
		case 'E':
		desmarcar("e");
		break;
		case 'F':
		desmarcar("f");
		break;
		case 'G':
		desmarcar("g");
		break;
		case 'H':
		desmarcar("h");
		break;
		case 'I':
		desmarcar("i");
		break;
		case 'J':
		desmarcar("j");
		break;
		case 'K':
		desmarcar("k");
		break;
		case 'L':
		desmarcar("l");
		break;
		case 'M':
		desmarcar("m");
		break;
		case 'N':
		desmarcar("n");
		break;
		case 'Ñ':
		desmarcar("ñ");
		break;
		case 'O':
		desmarcar("o");
		break;
		case 'P':
		desmarcar("p");
		break;
		case 'Q':
		desmarcar("q");
		break;
		case 'R':
		desmarcar("r");
		break;
		case 'S':
		desmarcar("s");
		break;
		case 'T':
		desmarcar("t");
		break;
		case 'U':
		desmarcar("u");
		break;
		case 'V':
		desmarcar("v");
		break;
		case 'W':
		desmarcar("w");
		break;
		case 'X':
		desmarcar("x");
		break;
		case 'Y':
		desmarcar("y");
		break;
		case 'Z':
		desmarcar("z");
		break;
		default:
	    		//bAlfabetica='';
	    		desmarcar("todas");
	    		break;
	    	}
	    	pintarTabla();
	    }

	    function desmarcar(letra){
	    	var miarray = ["todas", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

	    	for (var i = 0; i < miarray.length; i++) {
	    		if (letra == miarray[i]) {
	    			$("#btn-"+miarray[i]).addClass("btn-primary");
	    			$("#btn-"+miarray[i]).removeClass("btn-default");
	    		}else{
	    			$("#btn-"+miarray[i]).removeClass("btn-primary");
	    			$("#btn-"+miarray[i]).addClass("btn-default");
	    		}
	    	}
	    }

	    $("#txtClave").keyup(function() {
	    	$('#alerta').hide();
	    	var clave = $('#txtClave').val();
	    	var idEquipo = $('#txtEquipo').val();
	    	$.post("index.php?c=equipo&a=verificarClave", {clave: clave, idEquipo: idEquipo}, function(respuesta) {
	    		console.log(respuesta);
	    		if (respuesta == 0){
	    			$( "#btnGuardar" ).prop("disabled", false);
	    		}else if (respuesta == 1){
	    			$('#alerta').show();
	    			$("#alerta").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Esta clave ya existe </p>');
	    			$("#btnGuardar").prop("disabled", true);
	    		}
	    	})
	    });

	    //Metodo para traer el consecutivo según el equipo a registrar
	    function consecutivo(){
	    	$.post("index.php?c=equipo&a=Consecutivo", {}, function(numConsecutivo) {
	    		$("#txtConsecutivo").val(numConsecutivo);
	    	});
	    } 