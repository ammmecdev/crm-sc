var idCliente= <?php echo $_REQUEST['idCliente']; ?>;
	var idEmbudo= <?php echo $_SESSION['idEmbudo']; ?>;
	window.onload=function(){
		consulta();
	}
	consulta = function (valor){
		if (valor==null) {
			valor=0;
		}
		$.post("index.php?c=dealcontacto&a=Consultaas", {idCliente:idCliente,valor:valor}, function(resultado) {
			$("#resultadodiv").html(resultado); 
		});
	}
	function myFunctionVerNegocios(nombre) {
		$('#labTituloN').html(nombre);
		$.post("index.php?c=dealcontacto&a=ListarNegocios", {idCliente:idCliente}, function(mensaje) {
			$("#ResuVerN").html(mensaje);
		});
	}
	$('#form-negocios-eliminarNeg').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mEliminar').modal('toggle');
			$('#mNegocios').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);

			consulta();
		});
		return false;
	});

	$('#form-AgregarSeguidores').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Seguidor vinculado con exito</center></strong></div>');
			$('#mAgregarSeguidores').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);

			consulta();
		});
		return false;
	});

	$('#form_seguidoresdel').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Seguidor desvinculado con exito</center></strong></div>');
			$('#mVerSeguidores').modal('toggle');
			$('#mEliminarS').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consulta();
		});
		return false;
	});
	function ocultaActividades(idOrganizacion) {
		$('#checkTiempo').prop('checked',false);
		$('#divTiempo').hide();
		$('#divCruzadas').hide();
		$('#divFiltro').show();
		$('#txtIdActividad').val(0);
		$('#txtTipo').val(""); 
		$('#txtTituloNegocio').val("");
		$('#txtNombreOrganizacion').val("");
		$('#txtfechaActividad').val("");
		$('#txtHora').val("");
		$('#txtDuracion').val("");
		$('#txtNotasA').val("");
		$('#txtConfirmado').val("false");
		listarNegociosPorOrganizacion(idOrganizacion);
	}
	function myFunctionVerSeguidores(idUsuario) {
		var array = $('#txtArrayS').val();
		var array = array.split(",");
		$.post("index.php?c=dealcontacto&a=VerSeguidores", {idUsuario:idUsuario,idCliente:idCliente,array:JSON.stringify(array)}, function(mensaje) {
			$("#ResuVerS").html(mensaje);
		});
	}

	myFunctionEliminaregocio = function (idNegocio) {
		
		$('#txtIdNegocioE').val(idNegocio);
		$('#labTituloE').html("Eliminar Negocio");
	}
	function myFunctionAgregarSeguidores(idCliente) {
		$('#txtidClienteS').val(idCliente); 
		$.post("index.php?c=dealcontacto&a=NombreContacto", {idCliente: idCliente}, function(mensaje) {
			$("#ResuS").html(mensaje);
		});
		listarUsuarios();
	}
	function listarUsuarios(){
		var idUsuario = $('#txtidUsuario').val();
		var array = $('#txtArrayS').val();
		var array = array.split(",");
		datos = {array:JSON.stringify(array),idUsuario:idUsuario};
		$.ajax({
			url: "index.php?c=dealcontacto&a=ListarUsuarios",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			$("#selectIdUsuario").empty();
			var selector = document.getElementById("selectIdUsuario");
			selector.options[0] = new Option("Seleccionar usuario","");
			for (var i in respuesta) {
				console.log(Object.values(respuesta));
				var j = parseInt(i) + 1;
				selector.options[j] = new Option(respuesta[i].nombreUsuario,respuesta[i].idUsuario);
			}
			$('#selectIdUsuario').selectpicker('refresh');
		}); 
		$("#selectIdUsuario").empty();
		var selectPersona = document.getElementById("selectIdUsuario");
	}
	function myFunctionEliminaUsuario(idNegocio,idUsuario) {
		$('#txtidNegocioES').val(idNegocio);  
		$('#txtidUsuarioE').val(idUsuario);
	}
	$('#form-cambiaEstado').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mTerminar').modal('toggle');
			$('#completarActividad').modal('toggle');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consulta();
		});
		return false;
	});
	function myFunctionNuevo(nombrePersona,idOrganizacion,nombreOrganizacion) {
		$('#inputOrganizacion').val(idOrganizacion);
		$('#selectEtapa').val("");
		$('#selectIdOrganizacion').val(nombreOrganizacion);
		$('#inputCliente').val(idCliente);
		$('#selectIdPersonaF').val(nombrePersona);
		$('#txtClaveNegocio').val("");
		$('#txtTituloNegocio').val("");
		$('#txtValorNegocio').val("");
		$('#selectTipoMoneda').val("MXN");
		$('#txtFechaCierre').val("");
		$('#txtContadorActivo').val("");
		$('#txtStatus').val("");
		$('#selectPonderacion').val("");
		$('#txtXcambio').val("");
		$('#txtServicio').val("");
		$('#divPresupuesto').show();
		$('#labelPresupuesto').show();
		$('#btnGuardar').val("Guardar");  
		listarEquipos();
		listarPresupuestos();
		ConsultaClave(idOrganizacion);
		sumaConsecutivo(idOrganizacion);
		myFunctionCheckedNo();
		$.post("index.php?c=negocios&a=ConsultaEtapaPorEmbudo", {valorIdEmbudo: idEmbudo }, function(mensaje) {
			$("#ConsultaEtapa").html(mensaje);
		});
	}
	//Metodo para consultar la clave de organizacion segun el id seleccionado y imprimirlo en la caja de texto 
	function ConsultaClave(idOrganizacion){
		$.post("index.php?c=negocios&a=ConsultaClaveOrganizacion", { valorIdOrganizacion: idOrganizacion }, function(cOrganizacion) {
			$("#txtClaveOrganizacion").val(cOrganizacion);
		});

	}
	//Metodo para traer el consecutivo segun la clave de la organizacion y imprimirlo en la caja de texto
	function sumaConsecutivo(idOrganizacion){
		$.post("index.php?c=negocios&a=ConsultaConsecutivo", { valorIdOrganizacion: idOrganizacion }, function(numConsecutivo) {
			$("#txtConsecutivo").val(numConsecutivo);
		});
	}
	$(document).ready(function() {
		$('#form-negocios').submit(function() {
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(respuesta) {
					$('#mNegocio').modal('toggle');
					$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
					$('#mensajejs').show();
					$('#mensajejs').delay(1000).hide(600);
            consulta(); //Imprimir la lista al realizar el registro 
        }    
    })        
			return false;
		});

		$("#No").change(function(){
			myFunctionCheckedNo();
		});

		$("#Si").change(function(){
			myFunctionCheckedSi();
		});

	});
	function myFunctionCheckedNo(){
		$("#ActivarPresupuesto").hide();
		$('#No').prop('checked', true);
		$('#txtNotas').val("");
	}
	function myFunctionCheckedSi(){
		$("#ActivarPresupuesto").show();
		$('#Si').prop('checked', true);
	}
	function listarEquipos(){
		datos = {};
		$.ajax({
			url: "index.php?c=negocios&a=ListarEquipo",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			$("#selectIdEquipo").empty();
			var selectEquipo = document.getElementById("selectIdEquipo");
			selectEquipo.options[0] = new Option("Seleccione el equipo","");
			for (var i in respuesta) {
				var j = parseInt(i) + 1;
				selectEquipo.options[j] = new Option(respuesta[i].nombreEquipo,respuesta[i].idEquipo);
			}
			$('#selectIdEquipo').selectpicker('refresh');
		}); 
	}
	function listarPresupuestos(){
		datos = {idEmbudo:idEmbudo};
		$.ajax({
			url: "index.php?c=negocios&a=listarPresupuestos",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			$("#selectPresupuesto").empty();
			var selector = document.getElementById("selectPresupuesto");
			selector.options[0] = new Option("Seleccione presupuesto","");
			for (var i in respuesta) {
				var j=parseInt(i)+1;
				selector.options[j] = new Option(respuesta[i].nombrePresupuesto,respuesta[i].idPresupuestoGeneral);
			}
			$('#selectPresupuesto').selectpicker('refresh');
		});
	}
	function registrarNota() {
		var notas = $('#txtNotas').val();
		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota Registrada</center></strong></div>');
		$('#mensajejs').show();
		$('#mensajejs').delay(3500).hide(600);
		$.post("index.php?c=dealcontacto&a=RegistrarNota", {idCliente:idCliente,notas:notas}, function(resultado) {
		});
		consulta();
	}
	function listarNegociosPorOrganizacion(idOrganizacion){
		datos = {idOrganizacion:idOrganizacion};
		$.ajax({
			url: "index.php?c=actividades&a=ListarNegociosPorOrganizacion",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			$("#selectIdNegocioF").empty();
			var selector = document.getElementById("selectIdNegocioF");
			selector.options[0] = new Option("Seleccione el negocio de actividad","");
			for (var i in respuesta) {
				console.log(Object.values(respuesta));
				var j = parseInt(i) + 1;
				selector.options[j] = new Option(respuesta[i].tituloNegocio,respuesta[i].idNegocio);
			}
			$('#selectIdNegocioF').selectpicker('refresh');
		}); 
		$("#selectIdNegocioF").empty();
		var selectPersona = document.getElementById("selectIdNegocioF");
	}
	function irActividades(idOrganizacion) {
		$('#checkTiempo').prop('checked',false);
		$('#divTiempo').hide();
		$('#divCruzadas').hide();
		$('#divFiltro').show();
		$('#txtIdActividad').val(0);
		$('#txtTipo').val(""); 
		$('#txtTituloNegocio').val("");
		$('#txtNombreOrganizacion').val("");
		$('#txtfechaActividad').val("");
		$('#txtHora').val("");
		$('#txtDuracion').val("");
		$('#txtNotasA').val("");
		$('#txtConfirmado').val("false");
		listarNegociosPorOrganizacion(idOrganizacion);
		$('#Notas').hide();
		$('#nota').hide();
		checkTiempo.checked = false;
		$('#Actividades').hide();
		$('#divTiempo').hide();
		$('#actividad').show(); 
	}
	function irsubirArchivos() {
		$('#checkTiempo').prop('checked',false);
		$('#Actividades').hide();
		$('#actividad').hide();
		$('#Notas').hide();
		$('#nota').hide(); 
	}
	function irNotas() {
		$('#Actividades').hide();
		$('#checkTiempo').prop('checked',false);
		$('#actividad').hide();
		$('#Notas').hide();
		$('#nota').show(); 
	}
	function registrarArchivo() {
		var inputFileImage = document.getElementById("archivo");
		var file = inputFileImage.files[0];
		var data = new FormData();
		data.append('idCliente',idCliente);
		data.append('archivo',file);
		$.ajax({
                    url: "index.php?c=dealcontacto&a=SubirArchivo",        // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: data,               // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
               success: function(dat)   // A function to be called if request succeeds
               {
               	if (dat=="Peso") {
               		$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Archivo demasiado grande</center></strong></div>');
               		$('#mensajejs').show();
               		$('#mensajejs').delay(3500).hide(600);
               	}else if(dat=="Otro"){
               		$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Solo se permiten PDF</center></strong></div>');
               		$('#mensajejs').show();
               		$('#mensajejs').delay(3500).hide(600);
               	}else{
               		$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>El archivo se subio correctamente</center></strong></div>');
               		$('#mensajejs').show();
               		$('#mensajejs').delay(3500).hide(600);
               	}        	
               	
               	consulta();
               }
           });

	}

	camposEditar = function()
	{
		$('#labTitulo').html("Editar actividad");
		$('#btnEliminar').show();
		$('#divCruzadas').hide();
		$('#divFiltro').hide();
		$('#cruzadas').html('');
		$('#divFiltro').hide();
		$('#selectNegocios').hide();
		$('#selectOrganizaciones').hide();
		$("#txtIdOrganizacion").removeAttr("disabled");
		$('#divOrganizaciones').show();
		$('#selectNegociosF').show();
		$('#selectPersonas').show();
		$("#selectIdOrganizaciones").prop('disabled', true);
		$("#selectIdNegocio").prop('disabled', true);
	}
	function eliminarNota(id)
	{
		$.post("index.php?c=deal&a=EliminarNota", {id:id}, function(resultado) {
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota Eliminada</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consulta();
		});
	}
	function eliminarArchivo(id,nombre)
	{
		$.post("index.php?c=dealcontacto&a=EliminarArchivo", {id:id,nombre:nombre}, function(resultado) {
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Archivo Eliminado</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consulta();
		});
	}
	function quitarReadOnly(id)
	{
		$("#"+id).removeAttr("readonly");
		$( "#"+id ).blur(function() {
			$("#"+id).attr("readonly","readonly");
			var contenido = $('#'+id).val();
			patron = "txtNotas",
			nuevoValor    = "",
			id = id.replace(patron, nuevoValor);
			$.post("index.php?c=deal&a=ActualizarNota", {id:id,contenido,contenido}, function(resultado) {
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Nota Actualizada</center></strong></div>');
				$('#mensajejs').show();
				$('#mensajejs').delay(3500).hide(600);
			});
		});
	}
	function eliminarActividad(id)
	{
		$('#txtIdActividadE').val(id);  
	}
	//Metodo para cambiar el estado en la base de datos de acuerdo al idActividad
	cambiaEstado = function(idActividad,estado,fechaCompletado)
	{
		$('#txtIdActividad2').val(idActividad);
		$('#txtEstado').val(estado);
		if(estado==1){
			$('#labActComp').html("Actividad completada");
			$('#Guardar2').val('Desacompletar');
			$('#txtFechaCompletado').val(fechaCompletado);
		}
		else{
			$('#labActComp').html("Completar actividad");
			$('#Guardar2').val('Completar');
			$('#txtFechaCompletado').val('');
		}
	}
	$('#form-negocios-eliminar').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha eliminado el negocio con exito</center></strong></div>');
			$('#mNegocios').modal('toggle');
			$('#form-negocios-eliminar').modal('toggle');
			$('#mensajejs').delay(3500).hide(600);

			consulta();
		});
		return false;
	});
	$('#form-eliminar').submit(function(){
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
		}).done(function(respuesta){
			$('#mEliminarA').modal('toggle');
			$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
			$('#mensajejs').show();
			$('#mensajejs').delay(3500).hide(600);
			consulta();
		});
		return false;
	}); 
	editarActividad = function(datos){
		window.scrollTo(0,0);
		var elementN = document.getElementById("liNot");
		var elementA = document.getElementById("liAct");
		var elementAr = document.getElementById("liArc");
		elementN.classList.remove("active");
		elementA.classList.add("active");
		elementAr.classList.remove("active"); 

		var datos = datos.split(",");
		var idActividad = datos[0];
		var tipoActividad = datos[1];
		var notas = datos[2];
		var fechaActividad = datos[3];
		var duracion = datos[4];
		var horaInicio = datos [5];
		var idOrganizacion = datos[6];
		var nombreOrganizacion = datos[7];
		var idNegocio = datos[8];
		var tituloNegocio = datos[9];
		var nombrePersona = datos[10];
		irActividades(idOrganizacion);
		camposEditar();  

		$('#txtIdActividad').val(idActividad);  
		$('#txtTipo').val(tipoActividad);
		$('#txtNotasA').val(notas);  
		$('#txtfechaActividad').val(fechaActividad);
		var arregloDuracion = duracion.split(" ");
		$('#txtDuracion').val(arregloDuracion[0]);
		$('#txtHora').val(horaInicio)
		$('#txtIdOrganizacion').val(idOrganizacion);
		$('#txtNombreOrganizacion').val(nombreOrganizacion);
		$('#txtConfirmado').val('false');


		if( horaInicio != ""){
			$('#checkTiempo').prop('checked',true);
			$('#divTiempo').show();
			$('#btnGuardar').val('Guardar');
		} else {
			$('#checkTiempo').prop('checked',false);
			$('#divTiempo').hide();
			$('#btnGuardar').val('Guardar');
		}
		datos = {idOrganizacion:idOrganizacion};
		$.ajax({
			url: "index.php?c=actividades&a=ListarNegociosPorOrganizacion",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			$("#selectIdNegocioF").empty();
			var selectIdNegocioF = document.getElementById("selectIdNegocioF");
			if (tituloNegocio != "")
				selectIdNegocioF.options[0] = new Option(tituloNegocio,tituloNegocio);
			else
				selectIdNegocioF.options[0] = new Option("Seleccione la persona de contacto","");
			var j=0;
			for (var i in respuesta) {
				if(respuesta[i].tituloNegocio != tituloNegocio){
					selectIdNegocioF.options[++j] = new Option(respuesta[i].tituloNegocio,respuesta[i].tituloNegocio);
				}
			}
			$('#selectIdNegocioF').selectpicker('refresh');
		});
    //----- Select tipo de duración --------
    $('#selectTipoDuracion').empty(); 
    var selectTipoDuracion = document.getElementById("selectTipoDuracion");
    if(arregloDuracion[1] == "minuto" || arregloDuracion[1] == "minutos"){
    	selectTipoDuracion.options[0] = new Option("minutos","minutos");
    	selectTipoDuracion.options[1] = new Option("horas","horas");
    }
    else{
    	selectTipoDuracion.options[0] = new Option("horas","horas");
    	selectTipoDuracion.options[1] = new Option("minutos","minutos");
    }
    $('#txtTipo').val(tipoActividad);
    $("#ya").mouseover(function(){
    	$("#ya").removeClass("active");
    });
    $("#ya2").mouseover(function(){
    	$("#ya2").removeClass("active");
    });
}