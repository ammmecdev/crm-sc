/*Inicio de Validadores de Sector Industrial*/
$(document).ready(function() {
    $('#form-sector').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            clave: {
                validators:{
                    stringLength: {
                        min: 3,
                        max: 3,
                    },
                    regexp: {
                        regexp: /^([A-Z])*$/,
                        message: 'Sólo se permiten letras en mayusculas'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese una clave de sector'
                    }
                }
            },
            nombreSector: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z Ññáéíóú])*$/,
                        message: 'Sólo se permiten letras'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un nombre de sector'
                    }
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        $('#form-sector').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
})
/*Fin de Validadores de Sector Industrial*/



/*Inicio de Validadores de Actividades*/
$(document).ready(function() {
    $('#form-actividades').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        fields: {
            actividad: {
                validators: {
                    notEmpty: {
                        message: 'Por favor seleccione una actividad'
                    }
                }
            },
            notas: {
                validators: {
                    notEmpty: {
                        message: 'Por favor ingrese una descripción'
                    }
                }
            },
            fecha: {
                validators: {
                    notEmpty: {
                        message: 'Por favor seleccione una fecha'
                    }
                }
            },
            idOrganizacion: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione un cliente'
                    }
                }
            },
        }
    })
})
/*Fin de Validadores de Actividades*/

/*Inicio de Validadores de Negocio*/
$(document).ready(function() {
    $('#form-nego').bootstrapValidator({ //Estos validadores estan pendientes------------>
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        fields: {
            idOrganizacionN: {
                validators: {
                    notEmpty:{
                        message: 'Por favor seleccione una organización'
                    }
                }
            },
            idClienteF: {
                validators: {
                    notEmpty:{
                        message: 'Por favor seleccione una persona de contacto'
                    }
                }
            },
            idClienteN: {
                validators: {
                    notEmpty:{
                        message: 'Por favor seleccione una persona de contacto'
                    }
                }
            },
            idOrganizacionF: {
                validators: {
                    notEmpty:{
                        message: 'Por favor seleccione una organización'
                    }
                }
            },
            tituloNegocio: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z Ññáéíóú])*$/,
                        message: 'Sólo se permiten letras'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un título de negocio'
                    }
                }
            },
            servicio: {
                validators: {
                    notEmpty: {
                        message: 'Por favor seleccione un servicio'
                    }
                }
            },
            valorNegocio: {
                validators: {
                    regexp: {
                        regexp: /^[0-9]+([.])?([0-9]+)?$/,
                        message: 'Sólo números'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un valor de negocio'
                    }
                }
            },
            idEtapa: {
                validators: {
                    notEmpty: {
                        message:'Por favor seleccione una etapa de embudo'
                    }
                }
            },
            fechaCierre: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione una fecha de cierre'
                    }
                }
            },
            ponderacion:{
                validators: {
                    notEmpty: {
                        message: 'Seleccione una ponderacion'
                    }
                }
            },
        }
    })
    .on('success.form.bv', function(e) {
        $('#form-negocios').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});
/*Fin de Validadores de Negocio*/

/*Inicio de Validadores de Embudo
$(document).ready(function() {
    $('#form-embudo').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nombre: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z Ññáéíóú])*$/,
                        message: 'Sólo se permiten letras'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un título de embudo'
                    }
                }
            },
        }
    })
});
/*Fin de Validadores de Embudo*/

/*Inicio de Validadores de Usuarios*/
$(document).ready(function() {
    $('#form_Usuarios').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nombreUsuario: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z Ññáéíóú])*$/,
                        message: 'Sólo se permiten letras'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un nombre de persona'
                    }
                }
            },
            email: {
                validators: {
                    regexp: {
                        regexp: /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i,
                        message: 'Ingrese un correo válido'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un correo electrónico'
                    }
                }
            },
            idTipo: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione un tipo de usuario'
                    }
                }
            },
            usuario: {
                validators: {
                    regexp: {
                        regexp: /^([a-zA-Z Ññáéíóú])*$/,
                        message: 'Sólo se permiten letras'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese un nombre de usuario'
                    }
                }
            },
            pass: {
                validators: {
                    regexp:{
                        regexp: /^[0-9a-zA-Z]+$/,
                        message: 'Solo letras en mayusculas/minusculas y numeros sin espacios'
                    },
                    notEmpty: {
                        message: 'Por favor ingrese una contraseña temporal para el usuario'
                    }
                }
            },
        }
    })
    .on('success.form.bv', function(e) {
        $('#form_Usuarios').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});
/*Fin de Validadores de Usuarios*/