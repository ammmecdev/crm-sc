var socket = require( 'socket.io' );
var express = require( 'express' );
var http = require( 'http' );
var node_usuario = 'bruno'; 

var app = express();
var server = http.createServer( app );

var io = socket.listen( server );

app.get('/', function (req, res) {
	res.status(200).send("Servidor nodejs / Sistema de sockets / status: corriendo en http://localhost:8000 ");
})

io.sockets.on( 'connection', function( client ) {
	console.log( "New client" );

	//isco = Informacion para notificacion de solicitud del costo operativo al departamento de operaciones
	client.on( 'insco', function( data ) {
		console.log(data);
		io.sockets.emit( 'modulo 2 nivel 3 sco', { 
			idNotificacion: data.idNotificacion,
			usuarioEmisor: data.usuarioEmisor,
			moduloEmisor: data.moduloEmisor,
			timestamp: data.timestamp,
			ruta: data.ruta,
			evento: data.evento,
			idCostoOperativo: data.idCostoOperativo,
			fotoUsuario: data.fotoUsuario,
			registro : data.registro
		});
	});

	//isco = Informacion para notificacion del costo operativo al departamento de ventas
	client.on( 'inco', function( data ) {
		//console.log(data);
		io.sockets.emit( 'modulo 3 nivel 2 co', { 
			idNotificacion: data.idNotificacion,
			usuarioEmisor: data.usuarioEmisor,
			moduloEmisor: data.moduloEmisor,
			timestamp: data.timestamp,
			ruta: data.ruta,
			evento: data.evento,
			idCostoOperativo: data.idCostoOperativo,
			fotoUsuario: data.fotoUsuario
		});
	});

	//cepco : Cambio de estado en el proceso del costo operativo
	client.on( 'cepco', function() {
		io.sockets.emit( 'modulo 3 cepco' );
	});

	client.on('actualiza_permisos', function (data){
		console.log(data.idUsuario);
		io.sockets.emit('act_permisos user-'+data.idUsuario, {
			idUsuario:data.idUsuario
		});
	});

});

server.listen(8000, function(){
	console.log("Servidor corriendo en http://localhost:8000");
})