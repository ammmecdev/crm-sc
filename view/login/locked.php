
<link rel="stylesheet" href="../../assets/css/style2.css">
<link rel="stylesheet" href="../../assets/css/form-elements2.css">

<!-- Top content -->
<div class="altura">
	<div class="container">
		<div class="col-sm-9 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form id="form-lockscreen" action="?c=lockscreen&a=VerificarAdminRH&g=true" method="POST" class="form-signin">
				<div class="form-top">
					<div class="form-top-left">
						<h3 class="form-signin-heading">Acceso de Administrador</h3>
						<p>Ingresa tu contraseña para continuar:</p>
					</div>
					<div class="form-top-right">
						<i class="fa fa-lock"></i>
					</div>
				</div>
				<div class="form-bottom">
					<div class="form-group">
						<div id="error"></div>
					</div>
					<div class="form-group">
						<label for="inputPassword" class="sr-only">contraseña</label>
						<input type="password" name="password" id="txtPassword" class="form-control" placeholder="Contraseña" autofocus>
					</div>
					<button type="submit" class="btn btn-lg btn-block" id="btn-login">Acceder</button>
				</div>
			</form>			
		</div>
	</div> <!-- /container -->
</div>

<!--<script src="../../assets/js/scripts2.js"></script>-->
<script type="text/javascript">
	$('#form-lockscreen').bootstrapValidator({
		submitHandler: function(validator, form, submitButton) {
			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				data: form.serialize(),
			}).done(function(respuesta){
				if (respuesta=='ok') {
					window.location.href = "?c=usuarios&g=true"; 
				} else {
					$("#error").html('<div class="alert alert-danger alert-dismissible" role="alert"><strong><center><i class="material-icons mdi-spin">block</i> '+ respuesta +'</center></strong></div>');
				}
			});
			return false;
		},
		fields: {
			password: {
				validators: {
					notEmpty: {
						message: 'Campo Obligatorio'
					}
				}
			}
		}
	});

	window.onload=function(){
		$('#txtEmail').focus();
	}
</script>

