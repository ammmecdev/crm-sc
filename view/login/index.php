<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
	<title>Inicio de sesión</title>

	<link rel="icon" href="assets/imagenes/isotipo.ico">

	<!-- CSS -->
	<!-- <link rel="stylesheet" href="assets/font-awesome/css/fuentes.css"> -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css"><!-- 
	<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="assets/css/form-elements.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<script src="./assets/js/jquery-3.1.1.min.js"></script>

	<!-- Favicon and touch icons -->
	<link rel="shortcut icon" href="assets/ico/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

	<!-- Javascript -->
	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.backstretch.min.js"></script>
	<script src="assets/js/scripts.js"></script>
	<script src="assets/js/bootstrap-validator.js"></script>
	<style>
		.btn-primary{
			background-color: rgba(9, 85, 104, 1) !important;
		}
		.btn-secondary{
			background-color: rgba(87, 96, 102, 1) !important;
		}
		.btn-info{
			background-color: rgba(58, 163, 213, 1) !important;
		}
		.bg-primary{
			background-color: rgba(9, 85, 104, 1) !important;
		}
		.text-secondary{
			color: rgba(87, 96, 102, 1) !important;
		}
	</style>
</head>

<body id="body" class="bg-primary">

	<!-- Top content -->
	<div class="top-content" style="margin-top: 40px;">
		<div class="inner-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 form-box">
						<div class="form-top">
							<div class="form-top-left" id="encabezado-login">
								<h3 class="text-secondary">Inicio de Sesión</h3>
								<p class="text-secondary">Ingresa tu correo y contraseña para acceder:</p>
							</div>
							<div class="form-top-left" id="encabezado-recuperar">
								<h3 class="text-secondary">Recuperar contraseña</h3>
								<p class="text-secondary">Ingresa tu correo electronico para ayudarte a restructurar tu contraseña</p>
							</div>
							<div class="form-top-right">
								<i class="#"><img src="assets/imagenes/sc.png"></i>
							</div>
						</div>
						<div class="form-bottom" id="form-login">
							<form id="loginForm" role="form" action="?c=login&a=Verificar" method="POST" autocomplete="off">
								<div class="form-group">
									<div id="mensaje"></div>
									<div  id="error"></div>
								</div>
								<div class="form-group">
									<label class="sr-only" for="form-username">Correo</label>
									<input type="email" name="email" placeholder="Correo" class="form-username form-control" id="txtEmail" onkeypress="return enterCorreo(event)">
								</div>
								<div class="form-group">
									<label class="sr-only" for="form-password">Contraseña</label>
									<input type="password" name="password" placeholder="Contraseña" class="form-password form-control" id="txtPassword">
								</div>
								<div class="form-group" align="right">
									<a  href="#" onclick="recuperarPass()" class="olv">Olvidé mi contraseña</a>
								</div>
								<button type="submit" class="btn btn-lg btn-block btn-secondary" id="btn-login">Iniciar Sesión</button>
							</form>
						</div>
						<div class="form-bottom"  id="form-recuperar">  
							<form id="resPassForm" role="form" action="?c=login&a=VerificarCorreo" method="POST" autocomplete="off">
								<div class="form-group">
									<div  id="mensajePass"></div>
								</div>
								<div id="divFormRecupera">
									<div class="form-group">
										<label class="sr-only" for="form-username">Correo</label>
										<input type="email" name="email" placeholder="Ingresa tu correo electronico aquí" class="form-username form-control" id="txtEmailRecupera" required>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-lg btn-block btn-secondary" id="btn-confirmar">Confirmar</button>
									</div>
									<div class="form-group" align="center">
										<a href="#" onclick="login()" class="olv" id="linkVolver">Volver</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">

	$(document).ready(function(){
		$('#txtEmail').focus();
		login();

		$('#loginForm').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					url: form.attr('action'),
					type: "POST",
					data: form.serialize(),
				}).done(function(respuesta){
					console.log(respuesta);
					if (respuesta[0].mensaje=='ok') {
						$("#mensaje").hide();
						$("#btn-login").html('Accediendo ...');
						setTimeout(' window.location.href = "?c=modulos"; ',1000);
					} else if(respuesta[0].mensaje=='suspendida'){
						$("#mensaje").hide();
						$("#btn-login").html('Procesando ...');
						setTimeout('window.location.href = "index.php?c=login&a=RestablecerPass&idUsuario='+respuesta[0].idUsuario+'&token='+respuesta[0].token+'"',1000);
					}else {
						$("#mensaje").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center><i class="fa fa-warning"></i> '+ respuesta[0].mensaje +'</center></strong></div>');
					}
				});
				return false;
			},
			live: 'disabled',
			fields: {
				email: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				},
				password: {
					validators: {
						notEmpty: {
							message: 'Campo Obligatorio'
						}
					}
				}
			}
		});

		$('#resPassForm').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					url: form.attr('action'),
					type: "POST",
					data: form.serialize(),
				}).done(function(respuesta){
					if (respuesta=='error') {
						$("#mensajePass").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center><i class="fa fa-warning"></i> Ha ocurrido un error en el proceso de restablecer contraseña</center></strong></div>');
						$("#txtEmailRecupera").focus();
						$("#btn-confirmar").html('Volver a confirmar');
					} else if(respuesta=='denegado') {
						$("#mensaje").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center><i class="fa fa-warning"></i> No se puede recuperar contraseña para el correo electronico ingresado</center></strong></div>');
						$("#btn-confirmar").html('Confirmar');
						login();
					} else if(respuesta=='inexistente') {
						$("#mensajePass").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center><i class="fa fa-warning"></i> El correo electronico ingresado no existe en el sistema, vuelva a intentarlo.</center></strong></div>');
						$("#btn-confirmar").html('Volver a confirmar');
					} else {
						$("#mensajePass").html('<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center><i class="glyphicon glyphicon-info-sign"></i> '+respuesta+'</center></strong></div>');
						$('#divFormRecupera').hide();
					}
				});
				return false;
			},
			fields: {
				email: {
					validators: {
						notEmpty: {
							message: 'Por favor ingrese un correo electronico'
						}
					}
				}
			}
		});
	});


	function enterCorreo(e) {
		if (e.keyCode == 13) 
			$('#txtPassword').focus();
		tecla = (document.all) ? e.keyCode :e.which; 
		return (tecla!=13); 
	}

	function login(){
		$('#encabezado-recuperar').hide();
		$('#form-recuperar').hide();
		$('#encabezado-login').show();
		$('#form-login').show();
		$("#mensajePass").html('');
		var email = $('#txtEmailRecupera').val();
		if (!email==""){
			$('#txtEmail').val(email);
			$('#txtPassword').focus();
		}else{
			$('#txtEmail').focus();
		}
	}

	function recuperarPass(){
		$('#encabezado-recuperar').show();
		$('#form-recuperar').show();
		$('#encabezado-login').hide();
		$('#form-login').hide();
		$('#txtEmailRecupera').focus();
		$("#mensaje").html('');
	}

</script>

</html>
