<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
	<title>CRM SC Restablecer contraseña</title>

	<link rel="icon" href="assets/imagenes/isotipo.ico">
	
	<!-- CSS -->
	<link rel="stylesheet" href="assets/font-awesome/css/fuentes.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/form-elements.css">
	<link rel="stylesheet" href="assets/css/style.css">



	<!-- Favicon and touch icons -->
	<link rel="shortcut icon" href="assets/ico/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.backstretch.min.js"></script>
	<script src="assets/js/scripts.js"></script>
	<script src="./assets/js/bootstrap-validator.js"></script>
	<style>
		.btn-secondary{
			background-color: rgba(87, 96, 102, 1) !important;
		}
		.bg-primary{
			background-color: rgba(9, 85, 104, 1) !important;
		}
		.text-secondary{
			color: rgba(87, 96, 102, 1) !important;
		}
	</style>
</head>

<body id="body" class="bg-primary">

	<!-- Top content -->
	<div class="top-content" style="margin-top: -70px;">
		<div class="inner-bg">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2 text">
						<h1><strong>SMART CONDITION</strong></h1>
						<div class="description">
							<p>
								Proceso de restablecimiento de datos.
							</p>
						</div>
					</div>
				</div>

				<?php if($this->verificacion==1){ ?>
					<div class="row" id="divPass">
						<div class="col-sm-6 col-sm-offset-3 form-box">
							<div class="form-top">
								<div class="form-top-left" id="encabezado-login">
									<h3>Restablecer contraseña</h3>
									<p class="text-secondary">Ingresa una nueva contraseña para activar tu cuenta:</p>
								</div>
								<div class="form-top-right">
									<i class="#"><img src="assets/imagenes/sc.png"></i>
								</div>
							</div>
							<div class="form-bottom" id="form-login">
								<form id="resPassForm" role="form" action="?c=login&a=ActualizarPass" method="POST" autocomplete="off">
									<div class="form-group" id="divMensaje">
										<div id="mensaje" ></div>
									</div>
									<input type="text" name="idUsuario" id="txtIdUsuario" value="<?php echo $idUsuario; ?>" hidden>
									<input type="text" name="token" id="txtToken" value="<?php echo $token; ?>" hidden>

									<div class="form-group">
										<input type="password" name="password" placeholder="Contraseña nueva" class="form-username form-control" id="txtPassword" onkeypress="return enterPass(event)">
									</div>
									<div class="form-group">
										<input type="password" name="passworddos" placeholder="Confirmar contraseña" class="form-password form-control" id="txtPassword2">
									</div>
									<button type="submit" class="btn btn-lg btn-block btn-secondary" id="btn-guardar">Guardar cambios</button>
								</form>
							</div>
						</div>
					</div>
				<?php }else if($this->verificacion==0){ ?>
					<div class="row" id="divErr">
						<div class="col-sm-6 col-sm-offset-3 form-box">
							<div class="form-top">
								<div class="form-top-left" id="encabezado-login">
									<h2 style="color:#B71C1C; font-size: 35px;"><i class="fa fa-warning"></i> Error de verificación</h2>
								</div>
								<div class="form-top-right">
									<i class="#"><img src="assets/imagenes/sc.png"></i>
								</div>
								<center><a href="index.php" class="olv">Volver al login</a></center>
							</div>
						</div>
					</div>
				<?php } else if($this->verificacion==2){ ?>
					<div class="row" id="divSuccess">
						<div class="col-sm-6 col-sm-offset-3 form-box">
							<div class="form-top">
								<div class="form-top-left" id="encabezado-login">
									<h2 style="color:#388E3C; font-size: 35px;"><i class="fa fa-check"></i> Datos restablecidos</h2>

								</div>
								<div class="form-top-right">
									<i class="#"><img src="assets/imagenes/sc.png"></i>
								</div>
								<center><a href="index.php" class="olv">Iniciar sesión</a></center>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">

	$(document).ready(function() {

		$('#txtPassword').focus();
		$('#divMensaje').hide();

		$('#resPassForm').bootstrapValidator({
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			// submitHandler: function(validator, form, submitButton) {
			// 	$.ajax({
			// 		type: "POST",
			// 		url: form.attr('action'),
			// 		data: form.serialize(),
			// 	}).done(function(res){
			// 		if (res=='error') {
			// 			$('#divMensaje').show();
			// 			$("#mensaje").html('<div class="alert alert-danger alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center><i class="fa fa-warning"></i> Ha ocurrido un error en el proceso de restablecer contraseña, porfavor vuelve a intentarlo.</center></strong></div>');
			// 			$("#txtPassword").val('');
			// 			$("#txtPassword2").val('');
			// 			$("#txtPassword").focus();
			// 			return false;
			// 		}else{
			// 			return true;
			// 		}
			// 	});
			// },
			fields: {
				password: {
					validators: {
						identical: {
							field: 'passworddos',
							message: '<strong>Las contraseñas no coinciden</strong>'
						},
						regexp: {
							regexp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
							message: '<strong align="justify">Al menos una letra mayúscula (A - Z), una letra minúscula (a - z), un dígito (0 - 9), un caracter especial ( ? = . * ? [ # ? ! @ $ % ^ & * - ] ) y mínimo 8 de longitud.</strong>'
						},
						notEmpty: {
							message: '<strong>Ingrese una nueva contraseña</strong>'
						},

					}
				},
				passworddos: {
					validators: {
						identical: {
							field: 'password',
							message: '<strong>Las contraseñas no coinciden</strong>'
						},
						notEmpty: {
							message: '<strong>Confirme la contraseña</strong>'
						},
					}
				}
			} 
		});
	});

	function enterPass(e) {
		if (e.keyCode == 13) 
			$('#txtPassword2').focus();
		tecla = (document.all) ? e.keyCode :e.which; 
		return (tecla!=13); 
	}

</script>
</html>