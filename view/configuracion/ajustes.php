<!--Encabezado del contenido-->
<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
				<h3 style="margin-top: 16px;">Ajustes de Empresa</h3>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->


<div class="col-xs-12 col-sm-9 col-md-9 col-lg-12" style="padding-top: 15px;">
	<div class="row">
		<div class="col-md-9 col-lg-12">
			<?php if (isset($this->mensajeI) && !isset($this->warningI)){ ?>
				<div class="alert alert-success alert-dismissable">
					<a href="<?php echo $this->url; ?>" class="panel-close close" data-dismiss="alert">×</a>
					<span class="glyphicon glyphicon-ok"></span>
					<strong> <?php echo $this->mensajeI; ?> </strong>
				</div>
				<?php }if (isset($this->mensajeI) && isset($this->warningI)){?>
					<div class="alert alert-warning alert-dismissable">
						<a href="<?php echo $this->url; ?>" class="panel-close close" data-dismiss="alert">×</a>
						<span class="glyphicon glyphicon-warning-sign"></span>
						<strong> <?php echo $this->mensajeI; ?> </strong>
					</div>
					<?php } ?>
				</div>
				<!--Formulario-->
				<form class="" role="form" method="post" action="?c=configuracion&a=Guardar&g=true" enctype="multipart/form-data">
					<!-- left column -->
					<div class="col-md-3 col-md-offset-1" style="padding-top: 15px;">
						<div class="text-center">
							<img src="../../assets/imagenes/<?php echo $_SESSION['logo'];?>" class="img-rounded" alt="Usuario" style="max-width: 200px; max-height: 200px">
							<h5><strong>Logo Empresarial</strong></h5>
							<input type="file" name="logo" class="form-control">
						</div>
						<br>
					</div>

					<!-- edit form column -->
					<div class="col-md-6 col-md-offset-1">
						<br>
						<div class="form-group">
							<label class="control-label">Empresa:</label>
							<select class="form-control" onchange="cambiaVariable()" name="idEmbudo" aria-haspopup="true" aria-expanded="true" id="selectIdEmbudo">

								<option class="btn-default" value="<?php echo $_SESSION['idEmbudo']?>"><?php echo $_SESSION['nombre']; ?></option>
								<?php foreach($this->modelEmbudos->Listar() as $r): 
									if($r->idEmbudo != $_SESSION['idEmbudo']){ ?>

										<option class="btn-default" value="<?php echo $r->idEmbudo ?>"><?php echo $r->nombre; ?></option>

										<?php } endforeach; ?>
									</select>
								</div>
								<div class="form-group" align="right">
									<input type="submit" class="btn btn-sm btn-success" value="Guardar Cambios">
								</div>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
							<p class="help-block" align="center">Para una mejor visualización de tu logo empresarial utiliza imagenes con formato (.png)</p>
							<br>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					cambiaVariable = function(){
						var idEmbudo=$("#selectIdEmbudo").val();
						var nombre = $('#selectIdEmbudo option:selected').html();
						datos = {"idEmbudo":idEmbudo, "nombre":nombre};
						$.ajax({
							url: "?c=configuracion&a=CambiaVariables&g=true",
							type: "POST",
							data: datos
						}).done(function(){
							location.href = "?c=configuracion&n=ajustes&g=true"; 
						});                 
					} 
				</script>