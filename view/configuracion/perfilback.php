 <form class="form-horizontal" role="form" method="POST" action="?c=perfil&a=ActualizarFoto&g=true" enctype="multipart/form-data" id="form-foto">
            <input type="hidden" class="form-control" name="idUsuario" id="txtIdUsuario" value="<?php echo $_SESSION['idUsuario']; ?>">
          <div class="col-md-4 col-lg-3">
            <div class="text-center">
              <img src="../../assets/imagenes/<?php echo $_SESSION['foto']; ?>" class="img-circle" alt="Usuario" style="max-width: 170px; max-height: 170px" id="myImg">
              <h5><strong>Foto de Perfil</strong></h5>

              <div class="row">
                <div class="col-md-10" align="right">
                  <button type="button" onclick="showPerfil()" id="btnPerfil" class="btn btn-sm btn-link">Cambiar Foto de Perfil</button>
                </div>
                <div></div>
              </div>

              <div class="input-group" id="fotoPerfil">
                <label class="input-group-btn">
                  <span class="btn btn-sm btn-primary"  >
                    Abrir<input type="file" style="display: none;" id="imagenPerfil" multiple onchange="mostrarPalomita()" name="foto">
                  </span>
                </label>
                <input type="text" class="form-control input-sm" readonly>
              </div>

              <span class="help-block" id="labelImagen">
                Seleccionar Imagen
              </span>

              <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8" align="right">
                 <button type="submit" class="btn btn-sm btn-success" id="btnActualizarFoto"><span class="glyphicon glyphicon-ok"></span></button>
                 <button type="button" class="btn btn-sm btn-default" id="cancelF" onclick="ocultFoto()"><span class="glyphicon glyphicon-remove"></span></button>
                 <span></span>
               </div>
             </div>
             
           </div>
         </div> 
       </form>




       $('#form-foto').submit(function() {
    $.ajax({
      type: 'POST',
      url: $(this).attr('action'),
      data: $(this).serialize(),
      success: function(respuesta){
       $("#mensajejs").html('<div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>'); 
     }
   });   
    return false;
  }); 