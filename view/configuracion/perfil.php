<script src="../../assets/plugins/validation/parsley.min.js"></script>
<!--Encabezado del contenido--> 
<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 hidden-xs hidden-sm hidden-md">
				<h3 style="margin-top: 16px;">Tus ajustes personales</h3>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="row" style="padding-top: 10px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-offset-0">
			<form class="form-horizontal" id="form-foto" role="form" method="post" action="?c=perfil&a=ActualizarFoto&g=true" enctype="multipart/form-data" >
				<div class="form-group">
					<input type="hidden" class="form-control" name="idUsuario" id="txtIdUsuario" value="<?php echo $_SESSION['idUsuario']; ?>">
				</div>
				<div class="col-md-4 col-lg-3">
					<div class="text-center">
						<img src="../../assets/imagenes/<?php echo $_SESSION['foto']; ?>" class="img-circle" alt="Usuario" style="width: 170px; height: 170px; max-width: 170px; max-height: 170px; min-width: 170px; min-height: 170px;" id="myImg">
						<h5><strong>Foto de Perfil</strong></h5>

						<div class="row">
							<div class="col-sm-12 col-md-12" align="center" style="z-index: 1;">
								<button type="button" onclick="showPerfil()" id="btnPerfil" class="btn btn-sm btn-link">Cambiar Foto de Perfil</button>
							</div>
							<div></div>
						</div>

						<div class="input-group" id="fotoPerfil" style="z-index: 1;">
							<label class="input-group-btn">
								<span class="btn btn-sm btn-primary">
									Abrir<input type="file" style="display: none;" id="imagenPerfil" onchange="mostrarPalomita()" name="foto">
								</span>
							</label>
							<input type="text" class="form-control input-sm" readonly>
						</div>

						<span class="help-block" id="labelImagen">
							Seleccionar 
						</span>

						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-8" align="right" style="z-index: 1;">
								<button type="submit" class="btn btn-sm btn-success" id="btnActualizarFoto"><span class="glyphicon glyphicon-ok"></span></button>
								<button type="button" class="btn btn-sm btn-default" id="cancelF" onclick="ocultFoto()"><span class="glyphicon glyphicon-remove"></span></button>
								<span></span>
							</div>
						</div>
					</div>
				</div> 
			</form>
			<!--Formulario-->
			<form class="form-horizontal" id="form-perfil" role="form" method="post" action="?c=perfil&a=Actualizar&g=true" enctype="multipart/form-data">
				<!-- edit form column -->
				<div class="col-md-8 col-lg-9">
					<div class="form-group">
						<label class="col-md-8 control-label">Datos Generales</label>
					</div>
					<!--<div class="row">
						<div class="col-md-12" align="right" style="z-index: 1;">
							<button type="button" class="btn btn-sm btn-link" id="EditarDatos" onclick="quitarReadOnly()">Editar Datos</button>
						</div>
					</div>-->
					<div class="form-group">
						<!-- <input type="hidden" class="form-control" name="idUsuario" id="txtIdUsuario" value="<?php //echo $_SESSION['idUsuario']; ?>"> -->
						<input type="hidden" class="form-control" name="idEmpleado" id="txtIdEmpleado" value="<?php echo $_SESSION['idEmpleado']; ?>">

					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Tu Nombre:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" name="nombreUsuario" id="nombreUsuario" autofocus value="<?php echo $_SESSION['nombreUsuario']; ?>" placeholder="Nombre Completo" required readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">Tu Correo:</label>
						<div class="col-lg-8">
							<input class="form-control" type="text" name="email" id="email" placeholder="Correo Electrónico" value="<?php echo $_SESSION['email']; ?>" placeholder="Correo Electrónico" required readonly>
						</div>
						<div id="alertaEmail" class="col-lg-4 col-lg-offset-3"></div>
					</div>

					<div class="form-group">
						<label class="col-md-1 col-lg-3 control-label">Usuario:</label>
						<div class="col-md-12 col-lg-8">
							<input class="form-control" type="text" name="usuario" id="usuario" placeholder="Nombre de Usuario" value="<?php echo $_SESSION['usuario']; ?>" placeholder="Nombre de Usuario" required readonly>
						</div>
						<div id="alertaUsua" class="col-lg-4 col-lg-offset-3"></div>
					</div>
					<div class="form-group" id="botonesOcult">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-8" align="right" style="z-index: 1;">
							<input type="submit" class="btn btn-sm btn-success" id="GuardarDatos" value="Guardar">
							<button type="button" class="btn btn-sm btn-default" id="Cancelar1" onclick="verCosasReadO2()">Cancelar</button>
							<span></span>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!--Fin formulario-->

<!--Formulario Contraseña-->
<!--Formulario Contraseña-->
<div class="row">
	<div class="col-md-12 col-lg-12" align="right">
		<button type="button" onclick="showPass()" id="btnPass" class="btn btn-sm btn-link">Cambiar Contraseña</button>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 col-md-10 col-lg-12">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-md-10 col-lg-12">
				<div class="col-md-4 col-lg-3"></div>
				<div class="col-md-9">
					<form class="form-horizontal" role="form" method="post" id="formPass" action="?c=perfil&a=ActualizarPassword&g=true" enctype="multipart/form-data">
						<div class="form-group">
							<input type="hidden" class="form-control" name="idUsuario" id="idUsuario" value="<?php echo $_SESSION['idUsuario']; ?>">
						</div>
						<div class="form-group">
							<label class="col-md-8 col-md-offset-1 control-label">Cambiar Contraseña</label>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Contraseña actual:</label>
							<div class="col-md-4">

							</div>
							<div class="col-md-6">
								<div class="input-group">
									<input class="form-control" type="password" placeholder="Contraseña actual" onblur="cambiaCasilla()" name="pass" id="pass" required>
									<span class="input-group-btn">
										<button class="btn btn-default" type="button" data-toggle="tooltip" data-placement="bottom" title="Mostrar contraseña" onclick="Mostrar('x')">
											<span class="glyphicon glyphicon-eye-open"></span>
										</button>
									</span>                    
								</div>
								<div id="div-error-pass"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Nueva Contraseña:</label>
							<div class="col-md-6">
								<div class="input-group">
									<input class="form-control" type="password" placeholder="Nueva contraseña" id="passnueva" name="passnueva" required>
									<span class="input-group-btn">
										<button class="btn btn-default" type="button" data-toggle="tooltip" data-placement="bottom" title="Mostrar contraseña" onclick="Mostrar('a')">
											<span class="glyphicon glyphicon-eye-open"></span>
										</button>
									</span>                    
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Confirmar Contraseña:</label>
							<div class="col-md-6">
								<div class="input-group">
									<input class="form-control" type="password" placeholder="Confirmar contraseña" name="passconfirmar" id="passconfirmar" required>
									<span class="input-group-btn" name="ojo" id="ojo">
										<button class="btn btn-default" type="button" data-toggle="tooltip" data-placement="bottom" title="Mostrar contraseña" onclick="Mostrar('b')">
											<span class="glyphicon glyphicon-eye-open" ></span>
										</button>
									</span>                    
								</div>
							</div>
							<div id="div-error-pass2"></div>   
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-8" align="right">
								<input type="submit" class="btn btn-sm btn-success" id="btnActualizar" value="Actualizar">
								<button type="button" class="btn btn-sm btn-default" onclick="verCosasReadO1()">Cancelar</button>
								<span></span>
							</div>
						</div>
					</form>
					<!--Formulario Contrasela-->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- The Modal -->
<div id="myModal" class="modal externo">
	<span class="close" id="cerrar">&times;</span>
	<img class="interno" id="img01">
	<div id="caption"></div>
</div>

<?php if(isset($this->mensaje)){ if(!isset($this->error)){?>
	<div class="alert alert-success fade in alert-bottom2" align="center">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<strong><?php echo $this->mensaje; ?></strong>
	</div>  
<?php } if(isset($this->error)){ ?>
	<div class="alert alert-danger alert-bottom2" align="center">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		<strong><?php echo $this->mensaje; ?></strong>
	</div>
<?php } }?>


<script>

	$('#formPass, #form-perfil').submit(function() {
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(respuesta){
				$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"></button><strong><center>'+respuesta+'</center></strong></div>');
				$('#mensajejs').show();
				$('#mensajejs').delay(2500).hide(600);
				ocultFoto();
				verCosasReadO1();
				verCosasReadO2();
			}
		});   
		return false;
	}); 

	$("#email").keyup(function() {
		$('#alertaEmail').hide();
		var email = $('#email').val();
		var idUsuario = $('#idUsuario').val();
		$.post("index.php?c=perfil&a=verificarEmail&g=true", {email:email, idUsuario:idUsuario}, function(respuesta) {
			console.log(respuesta);
			if (respuesta == 0){
				$("#GuardarDatos").prop("disabled", false);
			}else if (respuesta == 1){
				$('#alertaEmail').show();
				$("#alertaEmail").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este correo electrónico ya existe </p>');
				$("#GuardarDatos").prop("disabled", true);
			}
		})
	});

	$("#usuario").keyup(function() {
		$('#alertaUsua').hide();
		var usuario = $('#usuario').val();
		var idUsuario = $('#txtIdUsuario').val();
		$.post("?c=usuarios&a=verificarUsuario&g=true", {usuario:usuario, idUsuario:idUsuario}, function(respuesta) {
			console.log(respuesta);
			if (respuesta == 0){
				$("#GuardarDatos").prop("disabled", false);
			}else if (respuesta == 1){

				$('#alertaUsua').show();
				$("#alertaUsua").html('<p style="color: #dd4b39; font-size: 12px; font-family: Helvetica; margin-top: 6px; margin-bottom: 0"> Este usuario ya existe </p>');
				$("#GuardarDatos").prop("disabled", true);
			}
		})
	});

	function Mostrar($id) {
		if ($id == "a") {
			var x = document.getElementById("passnueva");
		} else if ($id == "b") {
			var x = document.getElementById("passconfirmar");
		} else {
			var x = document.getElementById("pass");
		}
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}

	function quitarReadOnly($nombreUsuario,$email,$usuario,$imagen,$botonesOcult,$btnPass)
	{
		// Eliminamos el atributo de solo lectura
		$("#nombreUsuario,#email,#usuario,#imagen").removeAttr("readonly");
		$('#botonesOcult').show(); 
		$('#EditarDatos').hide();
		$("#btnPass").show();
	}
	window.onload=function(){
		verCosasReadO1();
	}
	function verCosasReadO1($nombreUsuario,$email,$usuario,$botonesOcult,$formPass,$btnPass)
	{
		// Ponemos el atributo de solo lectura
		$("#nombreUsuario,#email,#usuario").attr("readonly","readonly");
		$("#botonesOcult").hide();
		$("#EditarDatos").show();
		$("#formPass").hide();
		$("#btnPass").show();
		$("#labelImagen").hide();
		$("#fotoPerfil").hide();
		$("#btnActualizarFoto").hide();
		$("#cancelF").hide();
		
	}
	function verCosasReadO2($nombreUsuario,$email,$usuario,$imagen,$botonesOcult,$formPass,$btnPass)
	{
		// Ponemos el atributo de solo lectura
		$("#nombreUsuario,#email,#usuario,#imagen").attr("readonly","readonly");
		$("#botonesOcult").hide();
		$("#EditarDatos").show();
		$("#btnPass").show();
	}
	function showPass($formPass)
	{
		// Ponemos el atributo de solo lectura
		$("#formPass").show();
		$("#btnPass").hide();
	}
	function showPerfil()
	{
		// Ponemos el atributo de solo lectura
		$("#fotoPerfil").show();
		$("#labelImagen").show();
		$("#btnPerfil").hide();
		$("#cancelF").show();
		$("#btnActualizarFoto").hide();
	}
	function ocultFoto()
	{
		// Ponemos el atributo de solo lectura
		$("#fotoPerfil").hide();
		$("#labelImagen").hide();
		$("#btnPerfil").show();
		$("#fotoPerfil").hide();
		$("#btnActualizarFoto").hide();
		$("#cancelF").hide();
	}
	function mostrarPalomita(){
		// Ponemos el atributo de solo lectura
		$("#btnActualizarFoto").show();

	}
	myFunctionEditar = function (idUsuario, nombreUsuario, email, usuario, foto) {
		$('#txtIdUsuario').val(idUsuario);
		$('#txtNombreUsuario').val(nombreUsuario);
		$('#txtEmail').val(email);
		$('#txtUsuario').val(usuario); 
		$('#txtFoto').val(foto);
	}

	cambiaCasilla = function (){
		var password = $("#pass").val();
		datos = {"password":password};
		$.ajax({
			url: "index.php?c=perfil&a=VerificaPassword&g=true",
			type: "POST",
			data: datos
		}).done(function(respuesta){
			console.log(respuesta);
			if(respuesta!='ok'){
				$("#div-error-pass").html('<p style="color:red">La contraseña es incorrecta</p>');
			}else {
				$("#div-error-pass").html('');
			}
		});
	}
	 /* comparaContra = function (){
		var password = $("#passnueva").val();
		var password2 = $("#passconfirmar").val();
		datos = {"password":password,"password2":password2};
		$.ajax({
		  url: "index.php?c=perfil&a=VerificaPassword2",
		  type: "POST",
		  data: datos
		}).done(function(respuesta){
		  console.log(respuesta);
		  if(respuesta!='ok'){
			$("#div-error-pass2").html('<p style="color:red">Las contraseñas no coinciden</p>');

		  }else {
		  }
		});
	}*/
//Script para comparar las contraseñas
$(document).ready(function() {
  //variables
  var passnueva = $('[name=passnueva]');
  var passconfirmar = $('[name=passconfirmar]');
  var negacion = "Contraseñas no coinciden";
  //oculto por defecto el elemento span
  var span1 = $("<div></div>").html('<div style="color:red">Contraseñas no coinciden</div>').insertAfter(passconfirmar);
  span1.hide();
  //función que comprueba las dos contraseñas
  function coincidePassword(){
  	var valor1 = passnueva.val();
  	var valor2 = passconfirmar.val();
  //muestro el span
  span1.show().removeClass();
  //condiciones dentro de la función
  if(valor1 != valor2){
  	span1.text(negacion).addClass('negacion'); 
  }else{
  	span1.hide();
  }
}
  //ejecuto la función al soltar la tecla
  passconfirmar.keyup(function(){
  	coincidePassword();
  });
});

  //Script para input de fotografia
  $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
  	var input = $(this),
  	numFiles = input.get(0).files ? input.get(0).files.length : 1,
  	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  	input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
  	$(':file').on('fileselect', function(event, numFiles, label) {

  		var input = $(this).parents('.input-group').find(':text'),
  		log = numFiles > 1 ? numFiles + ' files selected' : label;

  		if( input.length ) {
  			input.val(log);
  		} else {
  			if( log ) alert(log);
  		}

  	});
  });
  
});

// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
	modal.style.display = "block";
	modalImg.src = this.src;
	//captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
	modal.style.display = "none";
}
</script>
<style type="text/css">
.negacion{color:#FF0000}
</style>