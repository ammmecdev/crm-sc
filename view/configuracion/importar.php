<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
				<h3 style="margin-top: 16px;">Importar Datos</h3>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->

<div class="row">
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-12">
		<br>
		<!--Inicio Panel de Importar Datos -->
		<div class="panel panel-default" style="padding-top: 0px; padding-left: 0px">
			<div class="panel-heading">
				<h3>Cargar archivo</h3>
			</div>
			<form action="" class="">
				<div class="panel-body">
					<!--Encabezado -->
					<div class="form-group">
						<label for="archivo" class="col-sm-3 control-label" style="padding-top: 7px;">Seleccionar Archivo:</label>
						<div class="col-sm-9">
							<input type="file" class="form-control" id="iarchivo">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<hr>
						</div>
					</div>
					<!--Encabezado -->

					<!--Contenido-->
					<ul class="nav nav-pills nav-justified">
						<li role="presentation"><a onclick="rutaOrganizaciones()" href="#Organizaciones" aria-controls="Actividades" role="tab" data-toggle="tab"><i class="mdi mdi-business mdi-lg"></i> Clientes</a></li>
						<li role="presentation"><a onclick="rutaPersonas()" href="#Personas" aria-controls="Actividades" role="tab" data-toggle="tab"><i class="fas fa-user" style="font-size: 15px;"></i> Contactos</a></li>
						<li role="presentation"><a onclick="rutaNegocios()" href="#Negocios" aria-controls="Actividades" role="tab" data-toggle="tab">
							<i class="fas fa-dollar-sign" style="font-size: 16px;"></i> Negocios</a>
						</li>
						<li role="presentation"><a onclick="rutaActividades()" href="#Actividades" aria-controls="Actividades" role="tab" data-toggle="tab"><i class="far fa-calendar-alt" style="font-size: 18px;"></i> Actividades</a></li>
						<!-- <li role="presentation"><a href="#Notas" aria-controls="Notas" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-comment"></span> Notas</a></li>-->
					</ul>
					<div class="row">
						<div class="col-lg-12">
							<hr>
						</div>
					</div>
					<p class="help-block" align="center">Solo se permiten archivos de Excel (.xls)</p>
					<!--Contenido-->
				</div>
				<div class="panel-footer" align="right">
					<button type="button" class="btn btn-sm btn-default">Cancelar Importación</button>
					<input type="submit" class="btn btn-sm btn-success" value="Guardar Cambios">
				</div>
			</form>
		</div>
		<!--Fin Panel de Importar Datos-->

		<br>

		<!--Inicio Panel de Historial de Importación -->
		<div class="panel panel-default" style="padding-top: 0px; padding-left: 0px">
			<div class="panel-heading">
				<h3>Historial de Importación</h3>
			</div>
			<div class="panel-body" style="padding-left: 0px; padding-right: 0px">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>              
							<tr>
								<td><strong>Fecha y hora</strong></td>
								<td><strong>Nombre de archivo</strong></td>
								<td><strong>Usuario</strong></td>
								<td><strong>Tipo</strong></td>
								<td><strong>Estado</strong></td>
							</tr> 
						</thead>
						<tbody>
							<tr>
								<td colspan="5" align="center">
									<p class="help-block">Todavía no has importado ningún archivo</p>
								</td>
							</tr>
						</tbody>         
					</table>
				</div>
			</div>
		</div>
		<!--Fin Panel de Historial de Importación-->
	</div>
</div>