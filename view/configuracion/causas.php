<!--Encabezado del contenido-->
<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
				<h3 style="margin-top: 16px;">Causas de perdida de negocios</h3>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->
<!--Contenido-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="right" style="margin-top: 16px; background: rgb(242, 243, 244);">
	<a href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#mCausa" onclick="myFunctionNuevo()">Añadir causa</a>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 16px;">
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr style="background-color: #FCF3CF;">
					<td style="width: 80%"><strong>Nombre de la causa</strong></td>
					<td align="center"><strong>Acción</strong></td>
				</tr>
			</thead>
			<tbody id="resultado">
			</tbody>
		</table>
	</div>
</div>
<!--Fin Contenido-->

<script>
	$(document).ready(function() {
		consulta();

		$('#form-causasp').bootstrapValidator({
			submitHandler: function(validator, form, submitButton) {
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					success: function(respuesta){
						if (respuesta=="error") {
							$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Error</center></strong></div>');
						}else{
							$("#mensajejs").html('<div class="alert alert-success alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
						}
						$('#mCausa').modal('toggle');
						$('#mensajejs').show();
						$('#mensajejs').delay(3600).hide(600);
						consulta();
					}
				})
				return false;
			},
			fields: {
				nombreCausa: {
					validators: {
						notEmpty: {
							message: 'Selección Obligatoria'
						}
					}
				}
			}
		})
		$('#mCausa')
		.on('shown.bs.modal', function() {	
		})

		.on('hidden.bs.modal', function () {
			$('#form-causasp').bootstrapValidator('resetForm', true);
		});

		$('#form-eliminarcp').submit(function(){
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: $(this).serialize(),
			}).done(function(respuesta){
				if (respuesta=="error") {
					$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>Se ha producido un error al eliminar la causa</center></strong></div>');
				}else{
					$("#mensajejs").html('<div class="alert alert-danger alert-dismissible alert-bottom" role="alert" style="margin-bottom: 0px;"><strong><center>'+respuesta+'</center></strong></div>');
				}
				$('#mEliminar').modal('toggle');
				$('#mCausa').modal('toggle');
				$('#mensajejs').show();
				$('#mensajejs').delay(3600).hide(600);
				consulta();
			})
			return false;
		}); 	

	});

	function myFunctionNuevo() {
		$('#btnEliminar').hide();
		$('#txtCausa').val(""); 
		$('#txtIdcausasPerdida').val(""); 
		$('#labTitulo').html("Añadir causa");
	}
	function myFunctionEditar(idcausasPerdida,nombreCausa) {
		$('#btnEliminar').show();
		$('#txtIdcausasPerdida').val(idcausasPerdida);
		$('#txtCausa').val(nombreCausa);  
		$('#labTitulo').html("Editar causa");
	}
	function myFunctionEliminar()
	{
		var idcausasPerdida = $('#txtIdcausasPerdida').val();
		$('#txtIdcausasPerdidaE').val(idcausasPerdida);  
	}
	consulta = function ()
	{
		$.get('?c=causas&a=ResultadoCausaPerdida&g=true',  
			function(causa) {
				console.log(causa);
				causas = "";
				for (var i in causa) { 
					causas=causas +`<tr>
					<td>${causa[i].nombreCausa}</td>
					<td align="center">
					<a data-toggle="modal" data-target="#mCausa" class="btn btn-xs btn-primary" onclick="myFunctionEditar('${causa[i].idcausasPerdida}','${causa[i].nombreCausa}');">
					<span class="glyphicon glyphicon-pencil"></span>
					</a>
					</td>
					</tr>`;
				}
				$('#resultado').html(causas);
			});
	}	
</script>


