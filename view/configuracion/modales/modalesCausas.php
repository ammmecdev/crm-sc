<div class="modal fade" id="mCausa" role="dialog">
	<div class="modal-dialog">
		<form  data-toggle="validator" method="post" action="?c=causas&a=Guardar&g=true" id="form-causasp" role="form">
			<div class="modal-content">
				<div class="modal-header bg-success">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><strong><label id="labTitulo"></label></strong></h4>
				</div>
				<div class="modal-body">
					<h5>Nombre de la causa</h5>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-question-sign"></span></span>
							<input type="hidden" class="form-control" name="idcausasPerdida" id="txtIdcausasPerdida" value="">
							<input type="text" id="txtCausa"  class="form-control"  name="nombreCausa" placeholder="Nombre de la causa">
						</div>
					</div>
				</div>
				<div class="modal-footer" style="margin-top: 0px;">
					<div class="col-xs-3 col-lg-3" align="left">
						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#mEliminar" data-toggle="tooltip" title="Eliminar organización" 
						id="btnEliminar" onclick="myFunctionEliminar()"><span class="glyphicon glyphicon-trash"></span></a>
					</div>
					<input type="submit" class="btn btn-success" id="btnGuardar" value="Guardar">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</form>
	</div>
</div>



<div class="modal fade" id="mEliminar" role="dialog">   
	<div class="modal-dialog">
		<form  method="post" action="?c=causas&a=Eliminar&g=true" id="form-eliminarcp"  role="form">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><strong>Eliminar causa</strong></h4>
				</div>
				<div class="modal-body"  style="margin-bottom: -30px;">  
					<!-- Cuerpo --> 
					<p style="font-size: 20px;">¿Esta seguro que desea eliminar la causa?</p>
					<input id="txtIdcausasPerdidaE" name="idcausasPerdida" hidden>
					<!-- fin cuerpo --> 
				</div>
				<div class="modal-footer">
					<div class="col-xs-12 col-sm-12 col-lg-12" align="right">
						<input type="submit" class="btn btn-danger" value="Eliminar">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>    
					</div>
				</div>
			</div>
		</form>
	</div>
</div>     
<!--fin modal-->