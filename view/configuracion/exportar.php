<!--Encabezado del contenido-->
<nav class="navbar navbar-default" id="navbar">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" id="sidebarCollapse" class="navbar-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
				<h3 style="margin-top: 16px;">Exportar Datos</h3>
			</div>
		</div>
	</div>
</nav>
<!--Encabezado del contenido-->

<div class="row">
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-12">
		<br>
		<!--Inicio Panel de Importar Datos -->
		<div class="panel panel-default" style="padding-top: 0px; padding-left: 0px">
			<div class="panel-body">
				<!--Encabezado -->
				<ul class="nav nav-pills nav-justified">
					<li role="presentation" class="active"><a onclick="rutaNegocios()" href="#Negocios" aria-controls="Actividades" role="tab" data-toggle="tab">
						<span><strong>$</strong></span> Negocios</a>
					</li>
					<li role="presentation"><a onclick="rutaOrganizaciones()" href="#Organizaciones" aria-controls="Actividades" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-briefcase"></span> Organizaciones</a></li>
					<li role="presentation"><a onclick="rutaPersonas()" href="#Personas" aria-controls="Actividades" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Personas</a></li>
					<li role="presentation"><a onclick="rutaActividades()" href="#Actividades" aria-controls="Actividades" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-earphone"></span> Actividades</a></li>
					<!-- <li role="presentation"><a href="#Notas" aria-controls="Notas" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-comment"></span> Notas</a></li>-->
				</ul>
				<!--Fin Enczbezado-->
				<div class="col-xs-12 col-lg-12">
					<hr size="2" style="color: blue;">
				</div>
				<!--Contenido -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="Negocios">
						<br>
						<div class="row">
							<div class="col-xs-12 col-lg-12" align="right">
								<p class="help-block">Clic para descargar <span class="glyphicon glyphicon-arrow-down"></span></p>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Organizaciones">
						<br>
						<div class="row">
							<div class="col-xs-12 col-lg-12" align="right">
								<p class="help-block">Clic para descargar <span class="glyphicon glyphicon-arrow-down"></span></p>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Personas">
						<br>
						<div class="row">
							<div class="col-xs-12 col-lg-12" align="right">
								<p class="help-block">Clic para descargar <span class="glyphicon glyphicon-arrow-down"></span></p>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Actividades">
						<br>
						<div class="row">
							<div class="col-xs-12 col-lg-12">
								<div class="form-group">
									<div class="form-group">
										<select name="periodo" id="periodo" class="form-control">
											<option value="">Seleccione periodo de exportación</option>
											<option value="vencimiento">Fecha de vencimiento</option>
											<option value="completada">Fecha marcada como completada</option>
										</select>
									</div>
									<div class="form-group">
										<h5><strong>De:</strong></h5>
										<div class="input-group">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
											<input type="date" class="form-control" name="fechaActividad1" id="fechaActividad1" />
										</div>
									</div>
									<div class="form-group">
										<h5><strong>Hasta:</strong></h5>
										<div class="input-group">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
											<input type="date" class="form-control" name="fechaActividad2" id="fechaActividad2" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
       <!--  <div role="tabpanel" class="tab-pane" id="Notas">
          <br>
          <div class="row">
            <div class="col-xs-12 col-lg-12">
              <div class="form-group">
                <div class="form-group">
                  <h5><strong>Fecha de creción</strong></h5>
                </div>
                <div class="form-group">
                  <h5><strong>De:</strong></h5>
                  <div class="input-group date form_datetime">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type="text" class="form-control" name="fechaHora" id="txtFechaHora" />
                  </div>
                </div>
                <div class="form-group">
                  <h5><strong>Hasta:</strong></h5>
                  <div class="input-group date form_datetime">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type="text" class="form-control" name="fechaHora" id="txtFechaHora" />
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>-->
  </div>
  <!--Fin Contenido -->
</div>
<!--Inicio Footer Panel-->
<div class="panel-footer" align="right">
	<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" onclick="ruta()" title="Descargar"><span class="glyphicon glyphicon-download-alt"></span> CSV</button>
</div>
<!--Fin Footer Panel-->
</div>
</div>



<script type="text/javascript">
	var apartado='';
	var url = '';

	function rutaNegocios()
	{
		apartado='negocios';
		url = '?c=negocios&a=Exportar';
	}
	function rutaOrganizaciones()
	{
		url = '?c=organizaciones&a=Exportar';
	}
	function rutaPersonas()
	{
		url = '?c=personas&a=Exportar';
	}
	function rutaActividades()
	{
		apartado='actividades';
	}
  /*function rutaNotas()
  {
    url = '?c=exportar&a=ExportarNotas';
}*/
function ruta(){
	if (apartado == 'actividades'){
		var fechaInicio = $('#fechaActividad1').val();
		var fechaFin = $('#fechaActividad2').val();
		var periodo = $('#periodo').val();
		url = '?c=actividades&a=ExportarPorFechas&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&periodo='+periodo;
	}
	location.href = url;
}
</script>