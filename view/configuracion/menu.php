<link rel="stylesheet" href="../../assets/css/configuracion.css">

<!--Contenedor-->
<div class="padding-nav padding-nav2">
	<div class="wrapper">
		<!-- Sidebar Holder -->
		<nav id="sidebar" class="navbar-default">
			<div class="sidebar-header">
				<h3 align="center">Configuración</h3>
			</div>

			<ul class="list-unstyled components">
				<li>
					<a <?php if(isset($perfil)){ ?> id="submenu" <?php } ?> href="?c=perfil&g=true" class="lista">Perfil de Usuario</a>
				</li>
				<li class="line"></li>
				<li>
					<a <?php if(isset($embudos)){ ?> id= "submenu" <?php } ?> href="?c=negocios&a=ConfigurarEmbudos" class="lista">Embudos</a>
				</li>
				<li class="line"></li>
				<li>
					<a <?php if(isset($ajustesEmpresa)){ ?> id="submenu" <?php } ?> href="?c=configuracion&g=true" class="lista">Ajustes de Empresa</a>
				</li>
				<li class="line"></li>
				<li>
					<a <?php if(isset($causasPerdida)){ ?> id= "submenu" <?php } ?> href="?c=causas&g=true" class="lista">Causas de Perdida</a>
				</li>
				<li class="line"></li>
				<!-- <li>
					<a <?php if(isset($exportar)){ ?> id= "submenu" <?php } ?> href="?c=exportar" class="lista">Exportar Datos</a>
				</li>
				<li class="line"></li> -->
			</ul>
		</nav>

		<!-- Page Content Holder -->
		<div id="content">
			<?php include ($contenedor); ?>
		</div>
	</div>
</div>

<?php if (isset($modales)): ?>
	<?php include ($modales); ?>
<?php endif ?>

<script>
	$(document).ready(function() {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
			$(this).toggleClass('active');
		})
	});
</script>