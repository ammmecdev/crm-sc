<?php
include_once '../../model/database.php';
include '../../model/inicio.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>CRM SC Ventas</title>
	<link rel="icon" href="../../assets/imagenes/isotipo.ico">
	<!-- Prueba de sortable etapas-->
	<!--link href="src/js/jquery-ui-1.10.2.custom/css/smoothness/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" /-->
	<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script> -->
	<!--script type="text/javascript" src="src/js/jquery-ui-1.10.2.custom/js/jquery-ui-1.10.2.custom.min.js"></script--> 
	<!-- <script src="../../assets/plugins/validation/parsley.min.js"></script> -->

	<!-- Prueba de sortable etapas-->

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap-validator.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/AdminLTE.min.css">
	<link rel="stylesheet" type="text/css" href="../../assets/font-awesome/css/fontawesome.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/negocios.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/timeline.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/informe1.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/administracion.css">
	<link href="../../assets/plugins/PHPExcel/Classes/PHPExcel.php">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap-3.3.7/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/modalImage.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap3-editable/css/bootstrap-editable.css">
	<!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">-->
	<!-- <link rel="stylesheet" type="text/css" href="../../assets/Datatables/datatables.min.css"> -->
	<link rel="stylesheet" type="text/css" href="../../assets/material-design-icons-master/css/material-icons.min.css">
	<!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->

	<!-- Color de los nodos -->
	<link href="../../assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<!-- Permisos -->
	<link href="../../assets/plugins/tree-view/jquery.treeview.css" rel="stylesheet" type="text/css" />
	<!-- Checkbox personalizados -->
	<!-- <link href="../../assets/plugins/checkbox/icheck.css" rel="stylesheet" type="text/css" /> -->

	<!-- <script src="http://192.168.1.94:8000/socket.io/socket.io.js"></script>  -->

	<!-- <script src="http://localhost:8000/socket.io/socket.io.js"></script> -->

	<script src="../../assets/font-awesome/js/all.js"></script>
	<script src="../../assets/js/jquery-3.1.1.min.js"></script>
	<script src="../../assets/js/jquery-ui.min.js"></script>
	<script src="../../assets/js/jquery.sortElements.js"></script>
	<script src="../../assets/js/funciones.js"></script>
	<script src="../../assets/js/bootstrap.min.js"></script>
	<script src="../../assets/js/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="../../assets/js/bootstrap-validator.js"></script>
	<script type="text/javascript" src="../../assets/js/autoExpand.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
	<!--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>-->
	<script type="" src="../../assets/DataTables/datatables.min.js"></script>
	<script type="text/javascript" src="../../assets/js/tooltip.js"></script>

	<!-- <script type="text/javascript" src="../../assets/js/bootstrap-treeview.js"></script> -->

<!-- 	<script src="../../assets/plugins/checkbox/icheck.js"></script>
	<script src="../../assets/js/icheck-init.js"></script> -->

	<!-- Color picker se usa para el color de los nodos -->
	<script src="../../assets/js/bootstrap-colorpicker.js"></script>

	<!-- Permisos -->

	<script src="../../assets/js/push.min.js"></script> 
	<script src="../../assets/js/main.js"></script> 

</head>
<style type="text/css">
	#seleccion{
		color: #367fa9;
	}
	.popover {
		background-color: #EEEEEE;
		min-width: 100px;
		width: 100%;
		max-width: 200px;
	}
	#submenu{
		background-color: #367fa9;
		color: #FDFEFE;
	}

	.alert-bottom{
		position: fixed;
		z-index: 3;
		bottom: 5px;
		left:1%;
		width: 98%;
	}
	.alert-bottom2{
		position: fixed;
		z-index: 3;
		bottom: 40px;
		left: 1%;
		width: 98%;
	}

	.bg-success {
    background-color: #d2d6de !important;
}
</style>

<body>

	<div id="mensajejs"></div>

	<!--Contenido menú-->

	<nav class="navbar navbar-default navbar-fixed-top" id="head">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Ventas" aria-expanded="false" style="padding-top: 17px;">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="../../?c=modulos" class="navbar-brand" style="padding-top: 1px;">
					<img src="../../assets/imagenes/sc.png" alt="" style="max-width: 80px; max-height: 50px; margin-top: 8px">
				</a>
				<!--<a href="../../?c=modulos" class="navbar-brand admin6" style=" title="AMMMEC"><?php echo $_SESSION['nombre']?></a>-->
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="Ventas">

				<!-- *********** CONTENIDO DE MENU DE CADA MODULO ************ -->

				<?php include './view/menu/menu.php'; ?>

				<!-- ********************************************************* -->

				<ul class="nav navbar-nav navbar-right" id="admin7">
					<li><a href="#" onclick="actualizarDolar();" data-toggle="popover" title="Valor del dolar" data-container="body" data-toggle="popover" data-placement="bottom"  data-content="$ <?php echo $dolar; ?> USD" id="popoverDolar"><i class="fas fa-dollar-sign" style="font-size: 17px;"></i></a></li>
					<!-- Notifications: style can be found in dropdown.less -->
					<li class="dropdown messages-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-bell" style="font-size: 17px;"></i> <span class="badge label label-danger"><?php if($contNot>0){ ?><div id="countNotification"><?php echo $contNot; ?></div> <?php } ?></span></a>
						<?php include ('../../view/notificaciones/notificaciones.php'); ?>
					</li>
					<li data-toggle="tooltip" data-placement="bottom" title="Acerca de"><a href="#" data-toggle="modal" data-target="#myModal"><i class="fas fa-question-circle" style="font-size: 19px;"></i></a></li>
					<li class="dropdown">
						<a href="#" class="nave" data-toggle="dropdown" id="admin8" style="padding-bottom: 9px;">
							<img src="../../assets/imagenes/<?php echo $_SESSION['foto']?>" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;" />
							<span>&nbsp;</span><?php echo $_SESSION['nombreUsuario']; ?> 
							<b class="caret"></b> 
						</a>
						<ul class="dropdown-menu" style="min-width: 250px;">
							<li>
								<div class="top_pointer"></div>
								<li> <a href="?c=perfil&g=true"><i class="fas fa-address-card" style="font-size: 18px;"></i>
								</span> Perfil de Usuario</a>
							</li>
							<li role="separator" class="divider"></li>
							<li><a  <?php if(isset($configuracion)){ ?> id="seleccion" <?php } ?> href="?c=perfil&g=true"><i class="fas fa-cog" style="font-size: 16px;"></i> Configuración</a></li>
							<li role="separator" class="divider"></li>
							<li> <a href="../../?c=login&a=Logout"><i class="fas fa-sign-out-alt" style="font-size: 16px;"></i> Cerrar Sesión</a> </li>
						</ul>
					</li>
				</ul>  
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>


	<!-- *********** CONTENIDO DE PÁGINA DE CADA MODULO ************ -->

	<?php include($page); ?>

	<!-- ********************************************************* -->

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<br>
					<div class="row">
						<div class="col-lg-12" align="center">
							<img src="../../assets/imagenes/sc.png" alt="" style="width: 250px; height:=200px;">
						</div>
						<div class="col-lg-12" align="center">
							<br>
							<h3><strong>CRM-SC</strong></h3>
							<br>
							<p class="help-block" align="center">
								Herramienta de software para la gestión de clientes de la empresa Smart Condition.
							</p>
							<hr>
							<span class="help-block" align="center" style="margin-bottom: 0px;">Versión 1.0</span>
							<span class="help-block" align="center" style="margin-top: 0px;">Copyright<i class="far fa-copyright" style="font-size: 9px;"></i> 2018 - 2019</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var idUsuarioSes = '<?php echo $idUsuario; ?>';
		var idEmpleadoSes = '<?php echo $idEmpleado; ?>';
		var idNivel =  '<?php echo $idNivel; ?>';
		var idModulo = '<?php echo $idModulo; ?>';

		// console.log(idUsuarioSes+" "+idEmpleadoSes+" "+idNivel+" "+idModulo);
		//var idTipo = '<?php //echo $tipo; ?>';

		var moduloNivel = 'modulo ' + idModulo + ' nivel ' + idNivel;
		//socketCO_Op : socket de costo operativo en el area de operaciones
		var socketCO_Op =  moduloNivel + ' sco';
		// console.log(socketCO_Op)
		//socketCO_Ven : socket de costo operativo en el area de ventas
		var socketCO_Ven = moduloNivel  + ' co';
		//socketCOCE_Ven : socket de cambio de estado en el costo operativo en el area de ventas
		var socketCOCE_Ven = 'modulo ' + idModulo + ' cepco';

		//alert(socketCOCE_Ven);

		$(document).ready(function(){
			$('[data-toggle="popover"]').popover();
			var contNot = '<?php echo $contNot; ?>';
			if (contNot==0){
				$('#divCountNot').hide();
			}
		});	

		// socket.on( socketCO_Op, function( data ) {
		// 	console.log(data);
		// 	idCostoOperativo = data.idCostoOperativo;
		// 	idNotificacion = data.idNotificacion;
		// 	evento = data.evento;
		// 	fotoUsuario = data.fotoUsuario;
		// 	contenido = data.moduloEmisor + ' - ' + data.usuarioEmisor;
		// 	ruta = data.ruta;
		// 	$.ajax({
		// 		url: "?c=costo&a=ObtenerCostoOperativo",
		// 		type : "POST",
		// 		data : {'idCostoOperativo' : idCostoOperativo},
		// 		success: function(sco){
		// 			if (data.registro=='nuevo'){
		// 				if( typeof renderTabCO_Op !== 'undefined' && jQuery.isFunction( renderTabCO_Op ) ) {
		// 					renderTabCO_Op(idCostoOperativo,sco);
		// 				}else{
		// 					countNot();
		// 				}
		// 			}else{
		// 				if( typeof consultasCO !== 'undefined' && jQuery.isFunction( consultasCO ) ) {
		// 					consultasCO();
		// 				}else{
		// 					countNot();
		// 				}
		// 			}
		// 			renderNotCO(data,sco);
		// 			notifyMe(idNotificacion, evento, fotoUsuario, contenido, ruta);
		// 		}
		// 	});
		// });

		// socket.on( socketCO_Ven, function( data ) {
		// 	idCostoOperativo = data.idCostoOperativo;
		// 	idNotificacion = data.idNotificacion;
		// 	evento = data.evento;
		// 	fotoUsuario = data.fotoUsuario;
		// 	contenido = data.moduloEmisor + ' - ' + data.usuarioEmisor;
		// 	ruta = data.ruta;
		// 	$.ajax({
		// 		url: "?c=costo&a=ObtenerCostoOperativo",
		// 		type : "POST",
		// 		data : {'idCostoOperativo' : idCostoOperativo},
		// 		success: function(sco){
		// 			if( typeof consultasCO_Ven !== 'undefined' && jQuery.isFunction( consultasCO_Ven ) ) {
		// 				consultasCO_Ven();
		// 			}else{
		// 				countNot();
		// 			}
		// 			renderNotCO(data,sco);
		// 			notifyMe(idNotificacion, evento, fotoUsuario, contenido, ruta);
		// 		}
		// 	});
		// });

		// socket.on( socketCOCE_Ven, function() {
		// 	if( typeof consultasCO_Ven !== 'undefined' && jQuery.isFunction( consultasCO_Ven ) ) {
		// 		consultasCO_Ven();
		// 	}
		// });

		// socket.on('act_permisos user-'+idUsuarioSes, function(data) {
		// 	$.ajax({
		// 		type: 'POST',
		// 		url: '?c=usuarios&a=ActualizaPermisosSesion&g=true',
		// 		success : function(res)
		// 		{
		// 			console.log(res);
		// 		}
		// 	});
		// });


		


		

		redireccionarPorNot = function(ruta, idNotificacion)
		{
			marcarNotLeida(idNotificacion);
			location.href = ruta;
		}

		marcarNotLeida = function(idNotificacion)
		{
			console.log('lelida');
			$.ajax({
				url: "?c=notificaciones&a=MarcarLeida&g=true",
				type: "POST",
				data: {'idNotificacion' : idNotificacion}
			});
		}

		countNot = function()
		{
			$.ajax({
				url:"?c=notificaciones&a=CountNotification&g=true",
				type:"POST",
				success: function (count) {
					$('#divCountNot').show();
					$('#countNotification').html(count);
				}
			});
		}

		renderNotCO = function(data, sco)
		{
			var actualContent = $( "#notificaciones" ).html();
			var newNotContent = ` <li>
			<a onclick="redireccionarPorNot(${data.ruta},${data.idNotificacion})" type="button" class="list-group-item-info">
			<div class="row">
			<div class="col-lg-6">
			<i class="fas fa-dollar-sign"></i>&nbsp;<small>${data.moduloEmisor}</small>
			</div>
			<div class="col-lg-6" align="right">
			<small style="font-size: 10px;"><i class="far fa-clock"></i> ${data.timestamp} </small>
			</div>
			<div class="col-lg-12">
			<h4 style="margin-bottom: 0px; margin-top: 0px;">
			<small>
			${data.evento}
			</small>
			</h4>
			</div>
			</div>
			<small style="font-size: 10px;">CUENTA: ${sco[0].claveOrganizacion + '-' + sco[0].claveConsecutivo + '-' + sco[0].claveServicio}</small><br>
			<small style="font-size: 10px;">USUARIO: ${data.usuarioEmisor}</small>
			</a>
			</li>`;
			var content = newNotContent + actualContent;
			$( "#notificaciones" ).html( content );
		}

		function actualizarDolar()
		{
			$.ajax({
				url: "index.php?c=negocios&a=actualizarDolar",
				type: "POST"
			}).done(function(respuesta){
				$("#valorDolar").val(respuesta);
			});
		}

	</script>

</body>
</html>
