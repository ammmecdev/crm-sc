 <?php
 /*----------N O T I F I C A C I O N E S-----------*/
  //qNotificacion = query notificación
 // $qNotificacion = $this->pdo->prepare("SELECT * FROM notificaciones AS n, 

 $qNotificacion = $this->pdo->prepare("SELECT n.idRelacion, n.idEvento, n.timestamp, e.evento, m.modulo, m.idModulo, n.idUsuarioEmisor, u.nombreUsuario, n.leido, n.ruta, n.idNotificacion FROM notificaciones AS n, eventos AS e, usuarios AS u, receptores_notificaciones r, modulos m WHERE n.idEvento = e.idEvento AND n.idUsuarioEmisor = u.idUsuario AND r.idNotificacion=n.idNotificacion AND m.idModulo = n.idModuloEmisor AND r.idUsuarioReceptor=? ORDER BY timestamp DESC LIMIT 15");
 $qNotificacion->execute(array($_SESSION['idUsuario']));
 $notificaciones = $qNotificacion->fetchAll(PDO::FETCH_OBJ);

 ?>
 <style type="text/css">
 .content-not{
 	margin-bottom: 0px; margin-top: 0px;
 }
</style>
<ul class="dropdown-menu" style="border: 1px solid #c4c4c4; box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19); width: 340px;">
	<li>
		<!-- inner menu: contains the actual data -->
		<ul class="menu" id="notificaciones">
			<!-- start message -->
			<?php if ($notificaciones==null){ ?>
			<li>
				<div class="row">
					<div class="col-lg-12">
						<br>
						<center><h4 style="color:#428bca">No hay notificaciones para mostrar</h4></center>
						<br>
					</div>
				</div>
			</li>
			<?php 
		} foreach($notificaciones as $r):
		if($r->idEvento<5){
			switch ($r->idEvento) { 
				/*PROCESOS*/  
				case 1: 

				break; 
				case 2: 
        // Costo operativo 
				$stm = $this->pdo
				->prepare("SELECT * FROM costo_operativo co, negocios n WHERE co.idNegocio = n.idNegocio AND co.idCostoOperativo = ?");
				$stm->execute(array($r->idRelacion));
				$not = $stm->fetch(PDO::FETCH_OBJ);

				break; 
				case 3:
        // Orden de trabajo

				break; 
				case 4:
        // Evaluacion de servicio

				break; 
			} ?>
			<li>
				<a class="list-group-item <?php if($r->leido==0){ ?> list-group-item-info <?php } ?>" onclick="redireccionarPorNot('<?php echo $r->ruta ?>', '<?php echo $r->idNotificacion; ?>')" style="cursor: pointer">
					<div class="row">
						<div class="col-lg-2">
							<img src="../../assets/imagenes/<?php echo $_SESSION['foto']?>" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;" />
						</div>
						<div class="col-lg-5" style="margin-left: -15px;">
							<div class="col-lg-12">
								<h4 class="content-not">
									<small>
										<i class="material-icons">monetization_on</i> <?php echo $r->modulo; ?>
									</small>
								</h4>
							</div>
							<div class="col-lg-12">
								<h5 class="content-not">
									<small>
										<?php echo $r->evento; ?>
									</small>
								</h5>
							</div>
							<div class="col-lg-12">
								<h5 class="content-not">
									<small>
										Cuenta: <?php echo $not->claveOrganizacion .'-'. $not->claveConsecutivo . '-' . $not->claveServicio; ?>
									</small>
								</h5>
							</div>
						</div>
						<div class="col-lg-5" align="right">
							<div class="row">
								<div class="col-md-12">
									<h5 class="content-not">
										<small>
											<?php echo $r->timestamp; ?>
										</small>
									</h5>
								</div>
								<div class="col-md-12">
									<h5 style="margin-bottom: 0px; max-width: 100%">
										<small>
											<?php echo $r->nombreUsuario; ?>
										</small>
									</h5>
								</div>
							</div>
						</div>
					</div>
				</a>
			</li>
			<?php }else{} ?>

		<?php endforeach; ?>
		<!-- end message -->
	</ul>
</li>
<?php if ($notificaciones!=null){ ?>
<li><a href="#" class="" align="center" style="font-size: 13px;">Ver todas</a></li>
<?php } ?>
</ul>